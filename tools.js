// ========
var request = require('request');
module.exports = {
	removeTildes: function(str) {
		str = str.replace(/á/g,"a");
		str = str.replace(/é/g,"e");
		str = str.replace(/í/g,"i");
		str = str.replace(/ó/g,"o");
		str = str.replace(/ú/g,"u");
		str = str.replace(/ü/g,"u");
		str = str.replace(/ñ/g,"n");
		return str;
	},
	
	numberFormat: function(num, isResponse) {
		
		if (isResponse) {
			return {to: num, valid: true}
		} else {
			if (num.length==9 && num.substring(0,1) == "9") {
				return {to: "593"+num, valid: true}
			}	
			if (num.length==10 && num.substring(0,2) == "09") {
				return {to: "593"+num.substring(1), valid: true}
			}
			if (num.length==12 && num.substring(0,4) == "5939") {
				return {to: "593"+num.substring(3), valid: true}
			}
			if (num.length==13 && num.substring(0,5) == "+5939") {
				return {to: "593"+num.substring(4), valid: true}
			}
			if (num.indexOf("-") > 0 ) {
				//is group
				return {to: num, valid: true}
			}
			if (num.length==12 && num.substring(0,2) == "54") {
				return {to: num, valid: true}
			}
			if (num.length==12 && num.substring(0,2) == "57") {
				return {to: num, valid: true}
			}
			return {to: num, valid: false}
		}
		
	},
	
	hash: function (str) {
		  var hash = 5381,
			  i    = str.length;

		  while(i) {
			hash = (hash * 33) ^ str.charCodeAt(--i);
		  }

		  return hash >>> 0;
	},
	
	isCedula: function(str) {
		str = str.replace(/-/g,"");
		if (str.length==10) {
			var reg = /^\d+$/;
			if ( reg.test(str) ) { //check is only digits
				return true;
			} else {
				return false;	
			}
		} else {
			return false;	
		} 	
	},
		
	isEmail: function(email) {
		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	},
	
	getRndMS: function () {
		var n = Math.floor(Math.random() * 1500) + 1000;
		return n;	
	},

	horaValid: function (str) {
		if (str.length == 5) {
			var parts = str.split(":");
			if (parts.length==2) {
				if ( isNaN(parts[0]) || isNaN(parts[1]) ) {
					return {valid: false, time: null}
				} else {
					var h = parseInt(parts[0]);
					var m = parseInt(parts[1]);				
					if (h < 9 || h > 18) {
						return {valid: false, time: null}
					} else {
						if (m > 59 || m < 0) {
							return {valid: false, time: null}
						} else {
							return {valid: true, time: str}
						}
					}
				}
			} else {
				return {valid: false, time: null}
			}
		} else {
			return {valid: false, time: null}	
		}	
	},
	
	makeid: function(length) {
		var result           = '';
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		var charactersLength = characters.length;
		for ( var i = 0; i < length; i++ ) {
		  result += characters.charAt(Math.floor(Math.random() * 
	 charactersLength));
	   }
	   return result;
	},

	dateValid: function(str) {
		var today = new Date();
		str = str.replace(/\-/g,"/");
		if (str.length == 10) {
			var parts = str.split("/");
			if (parts.length==3) {
				if ( isNaN(parts[0]) || isNaN(parts[1]) || isNaN(parts[2]) ) {
					return {valid: false, fecha: null}
				} else {
					var y = parseInt(parts[0]);
					var m = parseInt(parts[1]);
					var d = parseInt(parts[2]);
					if ( y>=today.getFullYear() ) {
						if (m > 12 || m < 1) {
							return {valid: false, fecha: null}
						} else {
							if (d > 31 || d < 1) {
								return {valid: false, fecha: null}
							} else {
								var cita = new Date(str);
								if (cita>today) {
									return {valid: true, fecha: str}
								} else {
									return {valid: false, fecha: null}
								}
							}
						}
					} else {
						return {valid: false, fecha: null}
					}
				}
			} else {
				return {valid: false, fecha: null}
			}
		} else {
			return {valid: false, fecha: null}	
		}	
	}
		
}
