module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' utPlugin', user.id, step, group)
    console.log(job)
    switch (step) {
      case "0":
        updateUser({ step: '1', module: "mainFirmas" }, job.contact);
        pushText(mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
        break;

      //!steps cuando ingresa directo por #
      case "techrevo-1":
        var kw = body.toLowerCase()
        if (kw === 'aceptar') {
          var nuevoProceso = {
            contacto: job.contact,
            st: 'new',
            tsc: Date.now(),
            type: 'solicitudFirma',
            notes: 'Solicitud de firma LTV por Whatsapp'
          }
          crearProceso(nuevoProceso, function (procesoId) {
            if (procesoId) {
              updateUser({ currentProcess: procesoId, status: 'new' }, job.contact)
              setUserParam('step', '2', job.contact, user.line)
              user.currentProcess = procesoId
              pushText("Revisa por favor el siguiente enlace con nuestros términos y condiciones de uso del servicio:\n\n https://uanataca.ec/terminos_y_condiciones.pdf \n\n*Aceptas términos y condiciones de uso?*", job.contact, user.line, null, "btn", [{ id: "aceptar-tyc", title: "SI" }, { id: "noaceptar-tyc", title: "NO" }])
            } else {
              pushText(msgErrorInterno(), job.contact, user.line);
              salir(job, user);
            }
          });
        } else if ((kw === 'rechazar')) {
          pushText("Listo, recuerda que esta promoción estará vigente todo el día de hoy.", job.contact, user.line);
          salir(job, user);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
      //!fin steps cuando ingresa directo por #


      case "1":
        var kw = body.toLowerCase()
        if (user.currentProcess) {
          if (kw === 'solicitar_firma') {
            setUserParam('step', '2', job.contact, user.line)
            pushText("Revisa por favor el siguiente enlace con nuestros términos y condiciones de uso del servicio:\n\n https://uanataca.ec/terminos_y_condiciones.pdf \n\n*Aceptas términos y condiciones de uso?*", job.contact, user.line, null, "btn", [{ id: "aceptar-tyc", title: "SI" }, { id: "noaceptar-tyc", title: "NO" }])
          } else if (kw === 'sac') {
            setUserParam("step", "servicioCliente", job.contact, user.line);
            pushText("*Servicio al cliente*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "SERVICIO AL CLIENTE", options: [{ id: "sc-ss", title: "Status de solicitud" }, { id: "sc-vf", title: "Vigencia de firma" }, { id: "sc-t", title: "Tutoriales" }, { id: "sc-ca", title: "Contacte con un asesor" }, { id: "sc-mp", title: "Menú Principal" }] })
          } else {
            pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
          }
        } else {
          if (kw === 'solicitar_firma') {
            var nuevoProceso = {
              contacto: job.contact,
              st: 'new',
              tsc: Date.now(),
              type: 'solicitudFirma',
              notes: 'Solicitud de firma LTV por Whatsapp'
            }
            crearProceso(nuevoProceso, function (procesoId) {
              if (procesoId) {
                updateUser({ currentProcess: procesoId, status: 'new' }, job.contact)
                setUserParam('step', '2', job.contact, user.line)
                user.currentProcess = procesoId
                pushText("Revisa por favor el siguiente enlace con nuestros términos y condiciones de uso del servicio:\n\n https://uanataca.ec/terminos_y_condiciones.pdf \n\n*Aceptas términos y condiciones de uso?*", job.contact, user.line, null, "btn", [{ id: "aceptar-tyc", title: "SI" }, { id: "noaceptar-tyc", title: "NO" }])
              } else {
                pushText(msgErrorInterno(), job.contact, user.line);
                salir(job, user);
              }
            });
          } else if (kw === 'sac') {
            setUserParam("step", "servicioCliente", job.contact, user.line);
            pushText("*Servicio al cliente*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "SERVICIO AL CLIENTE", options: [{ id: "sc-ss", title: "Status de solicitud" }, { id: "sc-vf", title: "Vigencia de firma" }, { id: "sc-t", title: "Tutoriales" }, { id: "sc-ca", title: "Contacte con un asesor" }, { id: "sc-mp", title: "Menú Principal" }] })
          } else {
            pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
          }
        }
        break;

      //!Inicio casos servicio al cliente
      case 'servicioCliente':
        var kw = body.toLowerCase()
        //* Status de solicitud
        if (["sc-ss"].indexOf(kw) >= 0) {
          setUserParam('step', 'sc-ss-1', job.contact, user.line)
          pushText(solicitarNumCedula(), job.contact, user.line)
          //*Vigencia firma
        } else if (["sc-vf"].indexOf(kw) >= 0) {
          setUserParam('step', 'sc-vf-1', job.contact, user.line)
          pushText(solicitarNumCedula(), job.contact, user.line)
          //*Tutoriales
        } else if (["sc-t"].indexOf(kw) >= 0) {
          setUserParam('step', '1', job.contact, user.line)
          pushText(tutoriales() + mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
          //*Contacte con un asesor
        } else if (["sc-ca"].indexOf(kw) >= 0) {
          //!Pruebas para abrir un servicio al cliente
          var msg = "Para servicio al cliente por favor utiliza el siguiente link: https://wa.me/message/U365MLTAPKDRM1\n\n";
          // updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          salir(job, user);
          // logStartTicket(job.contact, user.chn, user, "sac");
          //*Menu principal
        } else if (["sc-mp"].indexOf(kw) >= 0) {
          updateUser({ step: '1', module: "mainFirmas" }, job.contact);
          pushText(mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*Aqui me llega el numero de cedula para consultar el status de la solicitud
      case 'sc-ss-1':
        var kw = body.toLowerCase().replace(/\D/g, '')
        utapi.consultarEstado(kw).then((res) => {
          if (res.result) {
            setUserParam('step', '1', job.contact, user.line)
            pushText('El estado de la solicitud es: ' + res.data.solicitudes[res.data.solicitudes.length - 1].estado + '\n\n' + mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
          } else {
            utapi.consultarEstadoSolicitud(kw).then((res) => {
              if (res.result) {
                setUserParam('step', '1', job.contact, user.line)
                pushText('El estado de la solicitud es: ' + res.data.solicitudes[res.data.solicitudes.length - 1].estado + '\n\n' + mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
              } else {
                setUserParam('step', '1', job.contact, user.line)
                pushText('No existe una solicitud con ese numero de cédula\n\n' + mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
              }
            }).catch((err) => {
              console.log(err)
              pushText(msgErrorInterno(), job.contact, user.line);
              salir(job, user);

            })
          }
        }).catch((err) => {
          console.log(err)
          pushText(msgErrorInterno(), job.contact, user.line);
          salir(job, user);
        })
        break

      //* Aqui me llega el numero de cedula para consultar la vigencia de la firma
      case 'sc-vf-1':
        var kw = body.toLowerCase().replace(/\D/g, '')
        utapi.consultarEstado(kw).then((res) => {
          console.log(res)
          if (res.result) {
            setUserParam('step', '1', job.contact, user.line)
            pushText('El tiempo de validez que seleccionaste es de : ' + res.data.solicitudes[res.data.solicitudes.length - 1].validez + '\n\n' + mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
          } else {
            setUserParam('step', '1', job.contact, user.line)
            pushText('No existe una solicitud con ese numero de cédula\n\n' + mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
          }
        }).catch((err) => {
          console.log(err)
          pushText(msgErrorInterno(), job.contact, user.line);
          salir(job, user);
        })
        break
      //!Fin casos servicio al cliente

      //* Me llega si acepto o no terminos y condiciones
      case "2":
        var kw = body.toLowerCase()
        if (kw === 'aceptar-tyc') {
          pushText("Listo, toma en cuenta que por este canal solamente puedes solicitar firma de *persona natural* y necesitas tener contigo tu documento de identidad. Adicionalmente, si requieres la firma para facturación electrónica necesitas tener tu certificado de RUC en formato PDF.\n\nEl pago se realiza con tarjeta de crédito o débito Visa o Mastercard, o con tu código de promoción.", job.contact, user.line, "bot", "txt", null, null, null, function () {
            mods["utProds"].bot('Swtich Module', job, user, "0")
          });
        } else if (kw === 'noaceptar-tyc') {
          updateUser({ step: '1', module: "mainFirmas" }, job.contact);
          pushText(mainMenu(false), job.contact, user.line, null, "btn", [{ id: "solicitar_firma", title: "Solicitar firma" }, { id: "sac", title: "Servicio al cliente" }])
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo botPlugin")
    console.log(job)
    cb({ st: "done" })
  }
}

function mainMenu(greet) {
  if (greet) {
    var msg = '¡Hola! Bienvenido a *Uanataca*\n\n'
    msg += 'Por favor escoge una de las siguientes opciones:\n\n'
  } else {
    var msg = 'Por favor escoge una de las siguientes opciones:\n\n'
  }
  // msg += "1. Solicitar firma\n";
  // msg += "2. Servicio al cliente\n";
  return msg

}

function solicitarNumCedula() {
  var msg =
    'Por favor digita tu número de cédula.\n\n'
  return msg
}

function tutoriales() {
  var msg = 'Como firmar con tu firma electrónica de UANATACA:\n\n'
  msg += 'FIRMA EC: https://youtu.be/X3M4L0u57U4\n'
  msg += 'ADOBE READER: https://youtu.be/-txoPSL7Fus\n\n'
  msg += 'Como descargar tu firma electrónica:\n'
  msg += 'https://youtu.be/CJhXMDa8kUk\n\n'
  return msg
}
