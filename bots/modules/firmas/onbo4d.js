module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' onbo4d', user.id, step, group)
    console.log(job)
    switch (step) {
      case "0":
        if (user.currentProcess) {
          var pin = tools.generateNumberPin();
          var msg = "Envía un video selfie diciendo la siguiente frase:\n\n";
          msg += "_La clave es *" + (pin + "").split("").join(" ") + "*_";
          msg += "\n\nAsegura que el video sea claro, que tu rostro esté cerca de la cámara y pronuncia los números de *uno en uno*";
          updateUser({ step: "1", module: "onbo4d", pin: pin, tspin: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line, user.chn);
          break;
        } else {
          var nuevoProceso = {
            contacto: job.contact,
            st: 'new',
            tsc: Date.now(),
            type: 'onbo4d',
            notes: 'Validación de identidad mediante 4D'
          }
          crearProceso(nuevoProceso, function (pocesoId) {
            if (pocesoId) {
              updateUser({ step: "1", module: "utProds", currentProcess: pocesoId }, job.contact);
              var pin = tools.generateNumberPin();
              var msg = "Envía un video selfie diciendo la siguiente frase:\n\n";
              msg += "_La clave es *" + (pin + "").split("").join(" ") + "*_";
              msg += "\n\nAsegura que el video sea claro, que tu rostro esté cerca de la cámara y pronuncia los números de *uno en uno*";
              pushText(msg, job.contact, user.line, user.chn);
            } else {
              pushText(msgErrorInterno(), job.contact, user.line);
              salir(job, user);
            }
          });
        }
        break;


      case "1":
        var lapse = Date.now() - user.tspin
        //if (false) {
        if (lapse > (5 * 60 * 1000)) {
          var pin = tools.generateNumberPin();
          var msg = "Lo sentimos, el tiempo para enviar el video ha expirado :(\n\n";
          msg += "Recuerda pronuncia los números de tu clave de *uno en uno*:\n\n";
          msg += "Probemos una *nueva clave*. Envía un video selfie diciendo la siguiente frase:\n\n";
          msg += "_La clave es " + (pin + "").split("").join(" ") + "_";
          updateUser({ pin: pin, tspin: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line, user.chn);
        } else {
          if (job.msg['type'] === 'video') {
            //get video 
            updateUser({ step: "2" }, job.contact);
            pushText("Un momento por favor mientras procesamos tu video, puede tardar un par de minutos...", job.contact, user.line, user.chn);
            validateAudioPIN(envBot[account].TCAPIURL + "getmediaurl/" + user.line + "/" + job.msg.video.id, user)
          } else {
            pushText("Debes enviar un video selfie con la frase: _Mi clave segura es " + (user.pin + "").split("").join(" ") + "_", job.contact, user.line, user.chn);
          }
        }
        break;
      //end step 1


      case "2":
        pushText("Un momento por favor mientras procesamos tu video...", job.contact, user.line, user.chn);
        break;
      //end step 2

      //* Aqui me llega si escogio cedula o pasaporte
      case "3":
        var kw = body.toLowerCase()
        if (["cedula"].indexOf(kw) >= 0) {
          actualizarProceso(user.currentProcess, { tipoDocumento: 'CEDULA', tsu: Date.now() });
          updateUser({ step: "4" }, job.contact);
          pushText("Por favor ahora envíanos tu número de cédula, vamos a validar tu identidad.", job.contact, user.line);
        } else if (["pasaporte"].indexOf(kw) >= 0) {
          actualizarProceso(user.currentProcess, { tipoDocumento: 'PASAPORTE', tsu: Date.now() });
          updateUser({ step: "4-pasaporte" }, job.contact);
          pushText("Por favor ahora envíanos tu número de pasaporte, vamos a validar tu identidad.", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "4":
        //validate cédula
        var ced = body.replace(/\D/g, '') + "";
        console.log("validando ced", ced)
        if (ced.length == 10) {
          actualizarProceso(user.currentProcess, { numCedula: ced, tsu: Date.now() });
          updateUser({ step: "5", ced: ced }, job.contact);
          pushText("Listo, por favor envía tu código dactilar, está en el reverso de tu cédula (ej: E1234I1234)", job.contact, user.line, user.chn);
        } else {
          pushText(ced + " no parece un número de cédula correcto, por favor envía nuevamente", job.contact, user.line, user.chn);
        }
        break;
      //end step 3

      case "5":
        //get data from reg civil
        console.log(account)
        var dacti = body.toUpperCase().trim()
        if (dacti.length == 10) {
          getDataRegCivil(job.contact, user.ced, dacti, function (rcData) {
            if (rcData) {
              actualizarProceso(user.currentProcess, { nombresRegCivil: rcData.Nombre, fechaExpeRegCivil: rcData.FechaExpedicion, codigoDactilar: dacti, tsu: Date.now() });
              updateUser({ fechaNacimientoRC: rcData.FechaNacimiento }, job.contact);//! Pruebas
              //do compare faces
              //get screen shot from disk
              var source = fs.readFileSync('./tempVids/' + user.videoFile + '-1.jpg', { encoding: 'base64' });
              nxtapi.verificarCompareFacesImgs(source, rcData.Fotografia, job.contact).then((res) => {
                console.log(account)
                if (res.status == 200) {
                  if (res.details.FaceMatches.length > 0) {
                    console.log(res.details.FaceMatches)
                    //face match ok
                    //do liveness 2D test
                    checkLiveness2D(source, job.contact, function (resl) {
                      console.log(account)
                      if (resl.status == 200) {
                        //create a job to check analyze
                        if (resl.details.analyse_id) {
                          //crear job para revisar el resultado del analisis
                          var job2 = {
                            type: "check-liveness-status4D",
                            contact: job.contact,
                            fromJob: job.id,
                            analyse_id: resl.details.analyse_id,
                            api_evidence: resl.details.api_evidence,
                            FaceMatches: res.details.FaceMatches,
                            st: 'new',
                            ts: Date.now(),
                            runAfter: Date.now() + (requeueInt),
                            msg: {
                              line: user.line
                            }
                          }
                          firebase.database().ref('/accounts/' + account + '/botJobs/').push(job2);
                        } else {
                          updateUser({ step: "0", ced: '***', dacti: '***' }, job.contact);
                          pushText("Ha ocurrido un error y hemos notificado al administrador. Por favor intenta nuevamente más tarde", job.contact, user.line, user.chn);
                        }
                      } else {
                        updateUser({ step: "0", ced: '***', dacti: '***' }, job.contact);
                        pushText("Ha ocurrido un error y hemos notificado al administrador. Por favor intenta nuevamente más tarde", job.contact, user.line, user.chn);
                      }
                    });
                  } else {
                    var pin = tools.generateNumberPin();
                    var msg = "El rostro en el video no coincide con el rostro en el Registro Civil. \n\n"
                    msg = "Envía *nuevamente* un video selfie diciendo la siguiente frase:\n\n";
                    msg += "_La clave es *" + (pin + "").split("").join(" ") + "*_";
                    msg += "\n\nAsegura que el video sea claro, que tu rostro esté cerca de la cámara y pronuncia los números de *uno en uno*";
                    updateUser({ step: "1", module: "onbo4d", pin: pin, tspin: Date.now() }, job.contact);
                    pushText(msg, job.contact, user.line, user.chn);
                  }
                } else {
                  updateUser({ ced: '***', dacti: '***' }, job.contact);
                  pushText(msgErrorInterno(), job.contact, user.line);
                  salir(job, user);
                }
              }).catch((err) => {
                console.log(err)
                pushText(msgErrorInterno(), job.contact, user.line);
                salir(job, user);
                //error de la consulta de compare faces
              })

            } else {
              updateUser({ step: "4", ced: '***', dacti: '***' }, job.contact);
              pushText("No hemos podido validar los datos enviados, por favor intenta nuevamente, envía tu número de cédula", job.contact, user.line, user.chn);
            }
          });
        } else {
          pushText(dacti + " no parece un código dactilar correcto, por favor envía nuevamente", job.contact, user.line, user.chn);
        }
        break;
      //end step 5

      case "6-terceraEdad":
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'video') {
          nxtapi.convertirB64(envBot[account].TCAPIURL, user.line, job.msg.video.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { videoTerceraEdad: `evisImgs/b64${user.currentProcess}_videoTerceraEdad.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_videoTerceraEdad.txt`, resolve.b64)
              setUserParam('step', '6', job.contact, user.line)
              pushText("Listo, por favor envía una foto de la parte frontal de tu cédula", job.contact, user.line, user.chn);
            } else {
              setUserParam('step', 'cer-go-back', job.contact, user.line)
              pushText(msgErrorInterno() + GoBackMenu(), job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        } else {
          pushText("Lo que tu enviaste no es un video\n\n" + msgTerceraEdad(), job.contact, user.line)
        }
        break

      //*Aqui me llega la foto frontal de la cedula y pido la posterior
      case '6':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'image') {
          nxtapi.verificarDniOcr(envBot[account].TCAPIURL, user.line, job.msg.image.id).then((resolve) => {
            if (resolve.results.status === 200) {
              console.log(JSON.stringify(resolve.results.details.Blocks))
              if (JSON.stringify(resolve.results.details.Blocks).indexOf('CEDULA') !== -1) {
                obtenerProceso(user.currentProcess, function (evidencias) {
                  if (evidencias) {
                    if (JSON.stringify(resolve.results.details.Blocks).indexOf((evidencias.numCedula).substring(0, 9)) !== -1) {
                      actualizarProceso(user.currentProcess, { cedulaFront: `evisImgs/b64${user.currentProcess}_cedulaFront.txt`, resultadoCF: JSON.stringify(resolve.results.details.Blocks), tsu: Date.now() });
                      fs.writeFileSync(`evisImgs/b64${user.currentProcess}_cedulaFront.txt`, resolve.imagen)
                      setUserParam('step', '7', job.contact, user.line)
                      pushText('Por favor envía una foto de la parte posterior de tu cédula', job.contact, user.line)
                    } else {
                      pushText('La imagen no corresponde con el número de cédula que enviaste\n\nPor favor envía una foto de la parte frontal de tu cédula', job.contact, user.line)
                    }
                  } else {
                    pushText(msgErrorInterno(), job.contact, user.line)
                    salir(job, user);
                  }
                })
              } else {
                pushText('La imagen no corresponde a una cédula o la foto no es de buena calidad' + "\n" + 'Por favor envía una foto de la parte frontal de tu cédula', job.contact, user.line)
              }
            } else {
              pushText('La imagen no corresponde a una cédula o la foto no es de buena calidad\n\nPor favor envía una foto de la parte frontal de tu cédula', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error\n\nPor favor envía una foto de la parte frontal de tu cédula', job.contact, user.line)
          })
        } else {
          pushText("Lo que tu enviaste no es una fotografía\nPor favor envía una foto de la parte frontal de tu cédula", job.contact, user.line)
        }
        break

      //*Aqui me llega la foto posterior de la cedula y cambio de modulo
      case '7':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'image') {
          nxtapi.verificarDniOcr(envBot[account].TCAPIURL, user.line, job.msg.image.id).then((resolve) => {
            if (resolve.results.status === 200) {
              console.log(JSON.stringify(resolve.results.details.Blocks))
              if (JSON.stringify(resolve.results.details.Blocks).indexOf('LUGAR Y FECHA') !== -1) {
                obtenerProceso(user.currentProcess, function (evidencias) {
                  if (evidencias) {
                    var parts = evidencias.fechaExpeRegCivil.split("/");
                    var fechaExp = parts[2] + "-" + parts[1] + "-" + parts[0]
                    var meses = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"]
                    var fechaExp2 = parts[0] + " " + meses[Number(parts[1]) - 1] + " " + parts[2]
                    if (JSON.stringify(resolve.results.details.Blocks).indexOf(fechaExp) >= 0 || JSON.stringify(resolve.results.details.Blocks).indexOf(fechaExp2) >= 0) {
                      actualizarProceso(user.currentProcess, { cedulaBack: `evisImgs/b64${user.currentProcess}_cedulaBack.txt`, resultadoCB: JSON.stringify(resolve.results.details.Blocks), tsu: Date.now() });
                      fs.writeFileSync(`evisImgs/b64${user.currentProcess}_cedulaBack.txt`, resolve.imagen)
                      pushText("Listo, Hemos validado tu identidad", job.contact, user.line, "bot", "txt", null, null, null, function () {
                        mods["utSolicitud"].bot('Swtich Module', job, user, "0")
                      });
                    } else {
                      pushText('La fecha de expedición no coincide' + "\n" + 'Por favor envía una foto de la parte posterior de tu cédula', job.contact, user.line)
                    }
                  } else {
                    pushText(msgErrorInterno(), job.contact, user.line)
                    salir(job, user);
                  }
                })
              } else {
                pushText('La imagen no corresponde a una cédula o la foto no es de buena calidad' + "\n" + 'Por favor envía una foto de la parte posterior de tu cédula', job.contact, user.line)
              }
            } else {
              pushText('La imagen no corresponde a una cédula o la foto no es de buena calidad\n\nPor favor envía una foto de la parte posterior de tu cédula', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error\n\nPor favor envía una foto de la parte posterior de tu cédula', job.contact, user.line)
          })
        } else {
          pushText("Lo que tu enviaste no es una fotografía\nPor favor envía una foto de la parte posterior de tu cédula", job.contact, user.line)
        }
        break

      //*Aqui me llega el numero de pasaporte
      case "4-pasaporte":
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { numCedula: kw, tsu: Date.now() });
        updateUser({ step: "5-pasaporte", ced: ced }, job.contact);
        pushText("Listo, por favor envía una foto de tu pasaporte", job.contact, user.line, user.chn);
        break;

      //*Aqui me llega la foto del pasaporte
      case "5-pasaporte":
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'image') {
          nxtapi.convertirB64(envBot[account].TCAPIURL, user.line, job.msg.document.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { cedulaFront: `evisImgs/b64${user.currentProcess}_cedulaFront.txt`, cedulaBack: `evisImgs/b64${user.currentProcess}_cedulaBack.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_cedulaFront.txt`, resolve.b64)
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_cedulaBack.txt`, resolve.b64)
              mods["utSolicitud"].bot('Switch Module', job, user, "0")
            } else {
              pushText('Ocurrio un error', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        }
        actualizarProceso(user.currentProcess, { numCedula: kw, tsu: Date.now() });
        updateUser({ step: "5-pasaporte", ced: ced }, job.contact);
        pushText("Listo, por favor envía una foto de tu pasaporte", job.contact, user.line, user.chn);
        break;
    }
  },

  job: function (job, user, step, group, flow, cb) {
    switch (job.type) {
      case "check-liveness-status4D":
        firebase.database().ref('/accounts/' + account + '/chats/' + job.contact).once('value', function (snapshot) {
          var user = snapshot.val()
          console.log("check-liveness-status4D job")
          console.log(job)
          nxtapi.verificarLiveness(job.analyse_id, job.api_evidence).then((res) => {
            if (res.results.status === 200) {

              if (res.results.details.data.state == "FINISHED") {
                var score = (1 - res.results.details.data.results_media[0].results_data.confidence_spoofing) * 100
                console.log('SPOOFING', res.results.details.data.results_media[0].results_data.confidence_spoofing)
                console.log('SCORE', score)
                //termina prueba rapida de liveness
                if (score >= 0) {
                  console.log('RESULTADOS', res.results.details.data)
                  //liveness OK
                  //*Guardar Selfie
                  var source = fs.readFileSync('./tempVids/' + user.videoFile + '-1.jpg', { encoding: 'base64' });
                  actualizarProceso(user.currentProcess, { selfie: `evisImgs/b64${user.currentProcess}_selfie.txt`, tsu: Date.now() });
                  fs.writeFileSync(`evisImgs/b64${user.currentProcess}_selfie.txt`, source)
                  //Validar si es mayor de 64 años
                  if (calcularEdad(user.fechaNacimientoRC) < 64) {
                    updateUser({ step: "6" }, job.contact);
                    pushText("Listo, por favor envía una foto de la parte frontal de tu cédula", job.contact, user.line, user.chn);
                    actualizarProceso(user.currentProcess, { terceraEdad: 'no', tsu: Date.now() });
                  } else {
                    updateUser({ step: "6-terceraEdad" }, job.contact);
                    pushText(msgTerceraEdad(), job.contact, user.line, user.chn);
                    actualizarProceso(user.currentProcess, { terceraEdad: 'si', tsu: Date.now() });
                  }


                } else {
                  //liveness failed
                  sendNewPin(job, user, "1")
                }
                //job is completed
                cb({ st: "done" })
              } else {
                //requeue job	
                cb({ st: "requeue", ts: Date.now() + requeueInt })
              }
            } else {
              //requeue job	
              cb({ st: "requeue", ts: Date.now() + requeueInt })
            }
          })
        })
        break;

      case 'check-transcribe':
        aws.checkTranscribe(job.transJob, function (data) {
          console.log("check-transcribe job")
          console.log(data)
          if (data.TranscriptionJob.TranscriptionJobStatus == "COMPLETED") {
            aws.getObject('v3/coniski/demos/liveness3dplus/' + job.transJob, function (res) {
              console.log(res.file.Body.toString())
              var obj = JSON.parse(res.file.Body.toString())
              var valid = validateAlfaPIN(job, obj, user);
              if (valid) {
                //save evidences
                if (user.currentProcess) {
                  //to do: guardar resultado del transcribe
                  actualizarProceso(user.currentProcess, { videoOtp: user.pin, videoURL: "https://s3.amazonaws.com/wsp2crmcdn/v3/coniski/demos/liveness3dplus/" + job.transJob.replace("TJ_", ""), tsu: Date.now() });
                }
                //create biometry job
                var job2 = {
                  type: "extract-frame",
                  contact: user.id,
                  fromJob: job.id,
                  transJob: job.transJob,
                  st: 'new',
                  ts: Date.now(),
                  msg: {
                    line: user.line
                  }
                }
                firebase.database().ref("/accounts/" + account + "/botJobs/").push(job2);
              } else {
                console.log(new Date(), "alfa pin validation failed")
                sendNewPin(job, user, "1")
              }
              //job is completed
              cb({ st: "done" });
            })
          } else if (data.TranscriptionJob.TranscriptionJobStatus == "FAILED") {
            var pin = tools.generateNumberPin();
            var msg = "No podemos procesar el video que enviaste. Asegura que el video sea capturado como selfie. \n\n"
            msg = "Envía *nuevamente* un video selfie diciendo la siguiente frase:\n\n";
            msg += "_La clave es *" + (pin + "").split("").join(" ") + "*_";
            msg += "\n\nAsegura que el video sea claro, que tu rostro esté cerca de la cámara y pronuncia los números de *uno en uno*";
            updateUser({ step: "1", module: "onbo4d", pin: pin, tspin: Date.now() }, job.contact);
            pushText(msg, job.contact, user.line, user.chn);
            //job is completed
            cb({ st: "done" })
          } else {
            //requeue job	
            cb({ st: "requeue", ts: Date.now() + requeueInt })
          }
        })
        break;

      case 'extract-frame':
        firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
          var user = snapshot.val();
          console.log(job)
          //extract frames from video
          var fname = job.transJob.replace("TJ_", "")
          extractFrame({
            input: './tempVids/' + fname,
            output: './tempVids/' + fname + '-1.jpg',
            offset: tools.getRndMS(1234),
            quality: 2
          });
          updateUser({ step: '3', videoFile: fname }, job.contact);
          pushText("Listo, hemos validado correctamente tu clave segura\n\nPor favor elige el tipo de documento", job.contact, user.line, null, "btn", [{ id: "cedula", title: "Cédula" }, { id: "pasaporte", title: "Pasaporte" }])
          //job is completed
          cb({ st: "done" });
        });
        break;
    }
  }
}

//!Funciones para el onboard4D
function validateAudioPIN(url, user) {
  fetch(url, {
    method: 'get',
    headers: {}
  }).then(function (response) {
    if (response.status !== 200) {
      console.log(new Date(), 'Get Media url error', response.status);
      pushText(msgErrorInterno(), job.contact, user.line);
      salir(job, user);
    } else {
      console.log('URL', url)
      response.text().then(function (b64string) {
        console.log("video downloaded", b64string.length)
        var buffer = Buffer.from(b64string, 'base64');
        //save locally for liveness check
        var fname = user.id + "_" + Date.now()
        fs.writeFile('./tempVids/' + fname, buffer, {}, (err, res) => {
          if (err) {
            console.error(err)
            return
          }
          console.log('video saved')
        })
        //save buffer to AWS
        aws.putObject("v3/coniski/demos/liveness3dplus/" + fname + ".mp4", buffer, "video/mp4", function (res) {
          if (res.status == "ok") {
            console.log(new Date(), "Media saved on AWS for", fname);
            console.log("https://s3.amazonaws.com/wsp2crmcdn/v3/coniski/demos/liveness3dplus/" + fname);
            //create transcribe job
            aws.transcribeVideo("wsp2crmcdn", "v3/coniski/demos/liveness3dplus", fname, "mp4", function (data) {
              if (data) {
                console.log(data)
                //create check transcribe status job
                var job = {
                  type: "check-transcribe",
                  contact: user.id,
                  transJob: "TJ_" + fname,
                  st: 'new',
                  ts: Date.now(),
                  runAfter: Date.now() + (2 * requeueInt),
                  msg: {
                    line: user.line
                  }
                }
                firebase.database().ref("/accounts/" + account + "/botJobs/").push(job);
              } else {
                pushText(msgErrorInterno(), job.contact, user.line);
                salir(job, user);
              }
            })
          } else {
            pushText(msgErrorInterno(), job.contact, user.line);
            salir(job, user);
          }
        });
      });
    }
  }).catch(function (err) {
    console.log(new Date(), 'Get Media API request error', err.message);
    console.log(err)
  });
}

function checkLiveness2D(source, contact, cb) {
  //use Nexxit API to check 2D liveness
  var body = {
    "image": source,
    "userRef": contact
  }

  fetch(envBot[account].NexxitAPIURL + '/liveness/request', {
    method: 'post',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json', 'x-nexxit-key': envBot[account].NexxitApiKey }
  }).then(function (response) {
    if (response.status !== 200) {
      console.log(new Date(), '2d Liveness error', response.status);
      response.text().then(function (data) {
        console.log(data)
      });
      cb({ status: 502 });
    } else {
      response.json().then(function (data) {
        cb(data)
      });
    }
  }).catch(function (err) {
    console.log(new Date(), '2d Liveness error', err.message);
    console.log(err)
    cb({ status: 502 });
  });
}

const numberMapES = {
  'UNO': '1',
  'UN': '1',
  'DOC': '2',
  'DO': '2',
  'TREC': '3',
  'TREINTAI': '3',
  'TRE': '3',
  'CUATRO': '4',
  'CINCO': '5',
  'CEIC': '6',
  'CEI': '6',
  'CIETE': '7',
  'OCHO': '8',
  'NUEVE': '9',
  'DIECI': '1',
  'ONCE': '11',
  'DOCE': '12',
  'TRECE': '13',
  'CATORCE': '14',
  'CUINCE': '15',
  'DIECICEIC': '16',
  'DIECICEI': '16',
  'DIECICIETE': '17',
  'DIECIOCHO': '18',
  'DIECINUEVE': '19',
  'VEINTEI': '2',
  'VEINTE': '2',
  'VENTI': '2',
  'VEINTI': '2',
  'TREINTAI': '3',
  'TREINTA': '3',
  'TRENTI': '3',
  'TREINTI': '3',
  'CUARENTAI': '4',
  'CUARENTA': '4',
  'CUARENTI': '4',
  'CINCUENTAI': '5',
  'CINCUENTA': '5',
  'CINCUENTI': '5',
  'CECENTAI': '6',
  'CECENTA': '6',
  'CECENTI': '6',
  'CETENTAI': '7',
  'CETENTA': '7',
  'CETENTI': '7',
  'OCHENTAI': '8',
  'OCHENTA': '8',
  'OCHENTI': '8',
  'NOVENTAI': '9',
  'NOVENTA': '9',
  'NOVENTI': '9',
  'CIENTO': '1',
  'DOCIENTOC': '2',
  'DOCIENTO': '2',
  'TRECIENTOC': '3',
  'TRECIENTO': '3',
  'CUATROCIENTOC': '4',
  'CUATROCIENTO': '4',
  'CUINIENTOC': '5',
  'CUINIENTO': '5',
  'CEICIENTOC': '6',
  'CEICIENTO': '6',
  'CETECIENTOC': '7',
  'CETECIENTO': '7',
  'OCHOCIENTOC': '8',
  'OCHOCIENTO': '8',
  'NOVECIENTOC': '9',
  'NOVECIENTO': '9',
  'MIL': '1',
  'DOCMIL': '2',
  'DOMIL': '2',
  'TRECMIL': '3',
  'TREMIL': '3',
  'CUATROMIL': '4',
  'CINCOMIL': '5',
  'CEICMIL': '6',
  'CEIMIL': '6',
  'CIETEMIL': '7',
  'OCHOMIL': '8',
  'NUEVEMIL': '9',
}

function validateAlfaPIN(job, trans, user) {
  var arr = _.where(trans.results.items, { type: 'pronunciation' })
  var key = []
  var str = ""
  if (arr.length > 0) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].alternatives[0]) {
        str += arr[i].alternatives[0].content.toUpperCase()
      }
    }
    str = str.replace(/S/g, "C")
      .replace(/Z/g, "C")
      .replace(/Q/g, "C")
      .replace(/Ñ/g, "NI")
      .replace(/S/g, "C")
      .replace(/SC/g, "C")
      .replace(/B/g, "V")
      .replace(/Y/g, "I")
      .replace(/Á/g, "A")
      .replace(/É/g, "E")
      .replace(/Í/g, "I")
      .replace(/Ó/g, "O")
      .replace(/Ú/g, "U")
    console.log("TEXTO LEIDO", str)
    for (var prop in numberMapES) {
      const regex = new RegExp(`${prop}`, "g");
      str = str.replace(regex, numberMapES[prop])
    }
    //edge cases
    str = str.replace("3E", "13")
    console.log("TEXTO MAPEADO", str)

    //get number
    var num = str.replace(/\D/g, '');
    console.log("TEXTO SOLO NUMERO", num)
    console.log("pin", user.pin)
    if (num == user.pin) {
      return true
    } else {
      //TOOD: SEND EMAIL CON ERROR
      //enviar NUMERO DEL USER, y los datos de leido, pin, etc
      console.log(new Date(), "Transcribe job failed", job.contact)
      console.log(JSON.stringify(trans.results.items))
      return false
    }
  } else {
    //test wont pass we need at least one item
    return false;
  }
}

function sendNewPin(job, user, nextStep) {
  var pin = tools.generateNumberPin();
  var msg = "Lo sentimos, no hemos podido escuchar la clave correctamente\n\n";
  msg += "Recuerda pronuncia los números de tu clave de *uno en uno*:\n\n";
  msg += "Probemos una *nueva clave*. Envía un video selfie diciendo la siguiente frase:\n\n";
  msg += "_La clave es " + (pin + "").split("").join(" ") + "_";
  updateUser({ step: nextStep, pin: pin, tspin: Date.now() }, job.contact);
  pushText(msg, job.contact, user.line, user.chn);
}

function getDataRegCivil(contact, ced, dacti, cb) {
  nxtapi.verificarCedula(contact, ced, dacti).then((resolve) => {
    if (resolve.details) {
      cb(resolve.details)
    } else {
      console.log(resolve)
      cb(false)
    }
  });
}

function calcularEdad(fechaNacimiento) {
  var partesFecha = fechaNacimiento.split('/');
  var dia = parseInt(partesFecha[0], 10);
  var mes = parseInt(partesFecha[1], 10);
  var anio = parseInt(partesFecha[2], 10);

  var fechaNacimiento = new Date(anio, mes - 1, dia);
  var fechaActual = new Date();

  var edad = fechaActual.getFullYear() - fechaNacimiento.getFullYear();
  var mesActual = fechaActual.getMonth() + 1;
  var diaActual = fechaActual.getDate();

  // Si la fecha actual todavía no ha alcanzado el cumpleaños de este año
  if (mesActual < mes || (mesActual === mes && diaActual < dia)) {
    edad--;
  }

  return edad;
}

function msgTerceraEdad() {
  var msg = 'Por favor envía un video diciendo lo siguiente:\n\n'
  msg += 'Hoy (fecha actual) Yo (nombres completos) con CI. XXXXXX autorizo a emitir mi firma electrónica a Uanataca sabiendo que tiene la misma validez legal que la firma manuscrita y que me la envíen a mi correo electrónico (correo)'
  return msg
}
