global.fetch = require('node-fetch');
global.fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));

fetch('https://api.sandbox.ozforensics.com/api/authorize/auth', {
	method: 'post',
	body: JSON.stringify({"credentials":{"email":env.ozUser,"password":env.ozPwd}}),
	headers: { 'Content-Type': 'application/json'}
}).then(function(response) {
	if (response.status !== 200 ) {
		console.log(new Date(),'OZ Login Error',response.status);
		response.text().then(function(data) {
			console.log (data)
		});
	} else {
		response.json().then(function(data) {
			console.log (data)
		});
	}
}).catch(function(err) {
	console.log(new Date(),'OZ Login Error',err.message);
	console.log (err)
	
});		
