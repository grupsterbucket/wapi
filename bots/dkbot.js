var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "dk-bulk";
var booted = false;
var jobs = []


//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		loadAccountConfig();
		processJobQueue();
	}	
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/'+client+'/config').on('value', function(snapshot) {		  
		config = snapshot.val();
		console.log ("Config fetched for account",client);
	});
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+client+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var group = "main";
		if (user.step) {step = user.step};
		if (user.group) {group = user.group};
		
		console.log ("User "+job.contact+" is at step:", step, "group:", group);

			if (step != 'idle') {
					console.log ("Process MO ", job.msg.type);
					switch (job.msg.type) {
						case 'txt':
							var body = job.msg.m;
							if (body.length>0) {
								console.log ("Mo: ",body)
								//trim spaces
								body = body.trim();
								if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
									var msg = "Gracias por usar este canal. Adios!";
									pushText (msg, job.contact, user.line);
									resetUserParam("step", job.contact, user.line);
								} else {
									//process step
									switch (step) {
										case "0":
											var kw = body.toLowerCase()
											var msg = "Bienvenid@ al Quicentro Shopping. Para poder ofrecer una mejor atención, por favor digita una de las siguientes opciones:\n\n";
											msg += "*1* Servicio al Cliente\n";
											msg += "*2* Registra tus facturas\n";
											setUserParam("step","1", job.contact, user.line);
											pushText (msg, job.contact, user.line);
											break;
											//end step 0
										case "1":
											var kw = body.toLowerCase()
											var group = ""
											if (["1","1.","uno","one","ayuda","servicio al cliente", "servicio"].indexOf(kw)>=0) {
												if (isShopOpen()) {
													setUserParam("step","idle", job.contact, user.line);
													setUserParam("status","active", job.contact, user.line);
													setUserParam("group", "sac", job.contact, user.line); 
													var msg = "Enseguida uno de nuestros agentes le atenderá."
													pushText (msg, job.contact, user.line);	
													
												} else {
													setUserParam("step","idle", job.contact, user.line);
													setUserParam("status","active", job.contact, user.line);
													setUserParam("group", "sac", job.contact, user.line); 
													var msg = "Nuestro horario de atención por este canal es  de 09:30 a 17:30.\nUno de nuestros agentes le atenderá en nuestros siguiente horario disponible."
													pushText (msg, job.contact, user.line);	
												}
											} else if (["2","2.","dos","two","factura","compra","venta"].indexOf(kw)>=0) {
												var msg = "Por favor ingresa e este link y registra tu factura: link https://bit.ly/3bNLCtd"
												msg += "\n\nGracias por usar este servicio, adios!"
												pushText (msg, job.contact, user.line);
												resetUserParam("step", job.contact, user.line);
											} else {
												var msg = "Por favor responde solo con el número de la opción que deseas para poder continuar o responde *salir* para terminar la sesión";
												pushText (msg, job.contact, user.line);
											}
											
										break;
									}//end switch step
								} //end if salir
							} //end if body length > 0
							//job is completed
							finishJob(job);
							break; 
						default:
							
							//finish jobs
							finishJob(job);
							break;
					} //end switch mo type
			} else { //end idle check
				console.log ('user '+job.contact+'@'+user.line+' is idle');
				//job is completed
				finishJob(job);
			}

	});
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(1000));
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		console.log (new Date(), "Job completed");
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}


function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).set(val);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).remove();
}

global.isShopOpen = function() {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (t>=config.hourOpen && t<config.hourClose) {
		return true;
	} else {
		return false;		
	}	
}
