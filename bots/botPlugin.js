// Sample Bot as module
var qs = require('querystring');
module.exports = {
	cartBot: function (body, job, user, step, group, account) {
		console.log("Processing cart sub bot", step, group)
		switch (step) {
			case "c0":
				setUserParam("step", "c1", job.contact.msisdn, user.line, user.chn);
				pushText(mainMenuCartDemo(true), job.contact.msisdn, user.line, user.chn);
				break;
			//end step 0
			case "c1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno", "pedido", "comprar", "compra", "pedir", "domicilio"].indexOf(kw) >= 0) {
					var msg = "Excelente!\nSigue el siguiente enlace para realizar tu pedido en línea:\n\n";
					msg += "https://demo.factureasy.com/#shop?cr=" + job.contact.msisdn + "@wsp" + user.line;
					updateUser({ step: "c2", group: "ven" }, job.contact.msisdn, user.line, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					var msg = "Muy bien, sigue el siguiente enlace para ver nuestro menú:\n\n";
					msg += "https://demo.factureasy.com/#shop?cr=" + job.contact.msisdn + "@wsp" + user.line;
					pushText(msg + pageBreak() + mainMenuCartDemo(false), job.contact.msisdn, user.line, user.chn);
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					updateUser({ step: "idle", status: "active", group: "ser", tso: Date.now() }, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					updateUser({ step: "idle", status: "active", group: "ser", tso: Date.now() }, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else {
					pushText(mainMenuCartDemo(false), job.contact.msisdn, user.line, user.chn);
				}
				break;
			case "c2":
				var kw = body.toLowerCase()
				if (kw == "salir" || kw == "*salir*") {
					var str = "Gracias por usar este servicio. Adios!"
					pushText(str, job.contact.msisdn, user.line, user.chn);
					resetUserParam("step", job.contact.msisdn, user.line, user.chn);
					resetUserParam("status", job.contact.msisdn, user.line, user.chn);
					resetUserParam("pin", job.contact.msisdn, user.line, user.chn);
				} else {
					pushText("Por favor completa tu pedido en nuestro app o escribe *salir* para terminar  la sesión", job.contact.msisdn, user.line, user.chn);
				}
				break;
		}//end switch step

		function mainMenuCartDemo(greet) {
			if (greet) {
				var msg = "Hola!\nBienvenido a Burger Co.\n\n";
				msg += "¿En qué te puedo servir?\n";
				msg += "(digita una opción)\n\n";
			} else {
				var msg = "Por favor digita una opción:\n\n";
			}
			msg += "1. Deseo hacer un pedido a domicilio\n";
			msg += "2. Quiero ver su menú\n";
			msg += "3. Necesito ayuda con un pedido\n";
			msg += "4. Quiero hacer un reclamo\n";
			return msg;
		}
	},

	firmaBot: function (body, job, user, step, group, account) {
		console.log("Processing Firma sub bot", step, group)
		switch (step) {
			case "f0":
				if (user["x-dniValid"]) {
					var form = {
						ts: Date.now(),
						dni: user["x-dni"],
						model: 'sample',
						st: 'new',
						signed: false,
						contact: job.contact.msisdn,
						pin: Math.floor(Math.random() * 825500) + 100000
					}
					saveForm(form, function (key) {
						var nombre = user.persona.nombre;
						if (user.persona.nombre1) { nombre = user.persona.nombre1 }
						var msg = "Hola " + nombre + ", tu identidad ya fue validada, ahora solo debes llenar el contrato usando el siguiente link:\n\n";
						msg += "https://forms.textcenter.net/#form/" + key + "/edit\n\n"
						msg += "Utiliza este código para ingresar: *" + form.pin + "*"
						updateUser({ step: "f4" }, job.contact.msisdn, user.line, user.chn);
						pushText(msg, job.contact.msisdn, user.line, user.chn);
					});
				} else {
					var msg = "Coniski SmartContracts V1.0 Demo\n";
					msg += "Vamos a seguir un proceso de *tres* pasos:\n\n";
					msg += "- Validar tu identidad\n";
					msg += "- Llenar los datos de un contrato\n";
					msg += "- Obtener tu contrato PDF firmado electrónicamente\n\n";
					msg += "Es *importante* que respondas las preguntas del bot una por una y correctamente\n"
					msg += "Necesitas tener un *documento de identidad* a la mano para tomarle una foto\n\n"
					msg += "Comencemos!\n\n";
					msg += "Cuál es tu número de cédula?";
					setUserParam("step", "f1", job.contact.msisdn, user.line, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				}
				break;
			//end step 0
			case "f1":
				var kw = body.toLowerCase();
				kw = kw.replace(/-/g, "");
				//is cedula valid?
				if (tools.isCedula(kw)) {
					//call datofast and get data
					tools.datoFastHook(kw, function (str, ob) {
						if (ob) {
							//save in user
							updateUser({ persona: ob, step: "f2", "x-dni": kw }, job.contact.msisdn, user.line, user.chn);
							pushText("Listo, por favor envía una foto de un documento de identidad. Puede ser cédula, pasaporte o licencia.", job.contact.msisdn, user.line, user.chn);

						} else {
							pushText("Por favor revisa y envia tu cédula correctamente", job.contact.msisdn, user.line, user.chn);
						}
					});
				} else {
					pushText("Por favor revisa y envia tu cédula correctamente", job.contact.msisdn, user.line, user.chn);
				}
				break;
			case "f2":
				if (job.msg.content.image) {
					updateUser({ step: "f3", "x-dniPhoto": job.msg.content.image.url }, job.contact.msisdn, user.line, user.chn);
					var msg = "Listo, por favor envía un *video* donde se vea claramente tu rostro y debes decir lo siguiente:\n\n";
					msg += "_Mi nombre completo es " + user.persona.nombre + "_\n\n";
					msg += "_Mi número de identidad es " + user.persona.cedula + "_\n\n";
					msg += "_Hoy es  " + dateStr() + "_\n\n";
					msg += "_Autorizo a CONISKI a emitir una firma electrónica con mi identidad_\n\n";
					pushText(msg, job.contact.msisdn, user.line, user.chn);
					function dateStr() {
						var d = new Date()
						var ms = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"]
						return d.getDate() + " de " + ms[d.getMonth()] + " de " + d.getFullYear();
					}
				} else {
					pushText("Por favor revisa y envia una foto de tu identificación", job.contact.msisdn, user.line, user.chn);
				}
				break;
			case "f3":
				if (job.msg.content.video) {
					updateUser({ step: "f4", "x-dniVideo": job.msg.content.video.url, "x-dniValid": true }, job.contact.msisdn, user.line, user.chn);
					var msg = "Listo, espera un momento mientras validamos tu identidad";
					pushText(msg, job.contact.msisdn, user.line, user.chn);
					fakeAuth(job, user);
					function fakeAuth(job, user) {
						setTimeout(function () {
							//process to create p12, ask the user for PON
							//todo
							//process to create form
							var form = {
								ts: Date.now(),
								dni: user["x-dni"],
								model: 'sample',
								st: 'new',
								signed: false,
								contact: job.contact.msisdn,
								pin: Math.floor(Math.random() * 825500) + 100000
							}
							saveForm(form, function (key) {
								var msg = "Muy bien, ya hemos completado el primer paso. Por favor llena el contrato usando el siguiente link:\n\n";
								msg += "https://forms.textcenter.net/#form/" + key + "/edit\n\n"
								msg += "Utiliza este código para ingresar: *" + form.pin + "*"
								pushText(msg, job.contact.msisdn, user.line, user.chn);
							});

						}, 5000);
					}
				} else {
					pushText("Por favor revisa y envia un video con las instrucciones enviadas", job.contact.msisdn, user.line, user.chn);
				}
				break;
			case "f4":
				var kw = body.toLowerCase()
				if (kw == "salir" || kw == "*salir*") {
					var str = "Gracias por usar este servicio. Adios!"
					pushText(str, job.contact.msisdn, user.line, user.chn);
					resetUserParam("step", job.contact.msisdn, user.line, user.chn);
					resetUserParam("status", job.contact.msisdn, user.line, user.chn);
					resetUserParam("pin", job.contact.msisdn, user.line, user.chn);
				} else {
					pushText("Por favor completa el formulario en nuestro app o escribe *salir* para terminar la sesión", job.contact.msisdn, user.line, user.chn);
				}

				break;
		}//end switch step
	},

	citaBot: function (body, job, user, step, group, account) {
		console.log("Processing cart sub bot", step, group)
		switch (step) {
			case "a0":
				setUserParam("step", "a1", job.contact.msisdn, user.line, user.chn);
				pushText(mainMenuCitasDemo(true), job.contact.msisdn, user.line, user.chn);
				break;
			//end step 0
			case "a1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno", "agendar", "reserva", "cita"].indexOf(kw) >= 0) {
					var msg = "Excelente!\nSigue el siguiente enlace para agendar tu cita:\n\n";
					var pin = Math.floor(Math.random() * 625500) + 100000;
					updateUser({ step: "a2", group: "cit", pin: pin }, job.contact.msisdn, user.line, user.chn);
					var authStr = encr(job.contact.msisdn + "|coniski|res02|app|" + pin + "|" + Date.now())
					msg += "https://events.textcenter.net/#home/" + qs.escape(authStr);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					getCitas(job, user, account, function (arr) {
						arr = _.sortBy(arr, function (ob) { return arr.dateStart });
						//only report new and accepted and only future events
						var d = new Date();
						d.setHours(0);
						d.setMinutes(0);
						var ts = d.getTime();
						//console.log (calendars)
						evs = ""
						for (var i = 0; i < arr.length; i++) {
							var e = arr[i];
							if (e.dateStart >= ts) {
								if (e.st == "new" || e.st == "acc") {
									var cal = _.findWhere(calendars, { id: e.resource });
									if (cal) {
										var ed = new Date
										evs += "*" + cal.eventTitle + "*\n"
										evs += "" + cal.caption + "\n"
										evs += "Fecha: *" + e.ymd + "*\n"
										evs += "Hora: *" + formatTS(e.timeStart) + "*\n"
										evs += "Estado: *" + mapEventST(e.st) + "*\n"
										var pin = Math.floor(Math.random() * 625500) + 100000;
										var authStr = encr(job.contact.msisdn + "|coniski|res02|" + e.id + "|" + pin + "|" + Date.now())
										evs += "*Cancelar cita:*\nhttps://events.textcenter.net/#cancel/" + qs.escape(authStr);
										evs += "\n\n"
									}
								}
							}
						}
						if (evs.length > 0) {
							updateUser({ pin: pin }, job.contact.msisdn, user.line, user.chn);
							var msg = "A continuación tus citas pendientes.  Utiliza el enlace para cancelar la cita:\n\n";
							msg += evs;
						} else {
							var msg = "No tienes citas pendientes para cancelar.\n\n";
						}
						pushText(msg + pageBreak() + mainMenuCitasDemo(false), job.contact.msisdn, user.line, user.chn);
					});

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					var msg = "Te estamos direccionando con nuestro personal especializado";
					updateUser({ step: "idle", status: "active", group: "ser", tso: Date.now() }, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else {
					pushText(mainMenuCitasDemo(false), job.contact.msisdn, user.line, user.chn);
				}
				break;
			case "a2":
				var kw = body.toLowerCase()
				if (kw == "salir" || kw == "*salir*") {
					var str = "Gracias por usar este servicio. Adios!"
					pushText(str, job.contact.msisdn, user.line, user.chn);
					resetUserParam("step", job.contact.msisdn, user.line, user.chn);
					resetUserParam("status", job.contact.msisdn, user.line, user.chn);
					resetUserParam("pin", job.contact.msisdn, user.line, user.chn);
				} else {
					pushText("Por favor termina de agendar tu cita en nuestro app o escribe *salir* para terminar la sesión", job.contact.msisdn, user.line, user.chn);
				}
				break;
		}//end switch step

		function mainMenuCitasDemo(greet) {
			if (greet) {
				var msg = "Hola!\nBienvenido a Consultorios Bienestar\n\n";
				msg += "¿En qué te puedo servir?\n";
				msg += "(digita una opción)\n\n";
			} else {
				var msg = "Por favor digita una opción:\n\n";
			}
			msg += "1. Agendar una cita\n";
			msg += "2. Cancelar una cita\n";
			msg += "3. Reportar una empergencia\n";
			return msg;
		}

		function getCitas(job, user, account, cb) {
			firebase.database().ref("/accounts/" + account + "/events/").orderByChild("ref").equalTo(user.id).once('value', function (res) {
				if (res.val()) {
					cb(_.map(res.val(), function (ob, key) {
						ob.id = key;
						return (ob);
					}))
				} else {
					cb([]);
				}
			});
		}

		function formatTS(n) {
			if (n < 1000) {
				n += ""
				return n.substring(0, 1) + ":" + n.substring(1, 3);
			} else {
				n += ""
				return n.substring(0, 2) + ":" + n.substring(2, 4);
			}
		}

		function mapEventST(str) {
			switch (str) {
				case 'new':
					return "Por confirmar";
				case 'acc':
					return "Confirmado";
				default:
					return str.toUpperCase();
			}
		}
	},

	segurosBot: function (body, job, user, step, group, account) {
		console.log("Processing seguro sub bot", step, group)
		switch (step) {
			case "b0":
				var kw = body.toLowerCase()
				setUserParam("step", "b1", job.contact.msisdn, user.line, user.chn);
				pushText(mentaWelcome(true), job.contact.msisdn, user.line, user.chn);
				break;
			//end step 0
			case "b1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Ingresa al siguiente link donde podrás adquirir tu seguro en línea: \n\n";
					msg += "https://coniski.com/";
					msg += "\n\n";
					msg += "Gracias por usar este servicio. Adios!";
					resetUserParam("step", job.contact.msisdn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					var msg = "Qué tipo de asesoría requieres:\n\n";
					msg += "1. Conocer mis coberturas\n";
					msg += "2. Como usar mi seguro\n";
					msg += "3. Deseo presentar un requerimiento\n";
					msg += "4. Otros\n";
					var ob = { step: "b2", group: "sac" }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					var msg = "Estamos direccionado tu solicitud, un momento por favor";
					var ob = { step: "idle", group: "sin", status: "active", tso: Date.now() }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else {
					var msg = "Por favor digita una de las opciones";
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				}

				break;
			case "b2":
				var kw = body.toLowerCase()
				var msg = "Estamos direccionado tu solicitud, un momento por favor";
				updateUser({ step: "idle", status: "active", tso: Date.now() }, job.contact.msisdn, user.line, user.chn);
				logStartTicket(job.contact.msisdn, user.chn);
				pushText(msg, job.contact.msisdn, user.line, user.chn);
				break;

		}

		function mentaWelcome(greet) {
			var msg = "";
			if (greet) { msg += "¡Hola, bienvenid@ a Seguros Bienestar!\n\n"; }
			msg += "Por favor selecciona una opción:\n\n";
			msg += "1. Deseo adquirir un seguro.\n";
			msg += "2. Necesito asesoría.\n";
			msg += "3. Quiero reportar un siniestro.\n";
			return msg;

		}
	},

	//!Pruebas courierBot
	courrierBot: function (body, job, user, step, group, account) {
		console.log("Processing seguro sub bot", step, group)
		switch (step) {
			case "cr0":
				var kw = body.toLowerCase()
				setUserParam("step", "cr1", job.contact.msisdn, user.line, user.chn);
				pushText(Welcome(true), job.contact.msisdn, user.line, user.chn);
				break;
			//end step 0
			case "cr1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					setUserParam("step", "cr1-rastrear", job.contact.msisdn, user.line, user.chn);
					pushText('Por favor envíe su número de guia.', job.contact.msisdn, user.line, user.chn);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "cr1-centrega", job.contact.msisdn, user.line, user.chn);
					pushText('Por favor envíenos su ubicación', job.contact.msisdn, user.line, user.chn);
				} else {
					var msg = "Por favor digita una de las opciones";
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				}

				break;

			case "cr1-rastrear":
				var kw = body.toLowerCase()
				if (["123"].indexOf(kw) >= 0) {
					setUserParam("step", "cr1", job.contact.msisdn, user.line, user.chn);
					pushText('Listo, su paquete ya fue entregado\n\n' + Welcome(false) + '\n\n' + 'Escriba *salir* si desea terminar la sesión', job.contact.msisdn, user.line, user.chn);
				} else if (["456"].indexOf(kw) >= 0) {
					setUserParam("step", "cr1", job.contact.msisdn, user.line, user.chn);
					pushText('Listo, su paquete será entregado en el horario de: 10:00 a 14:00\n\n' + Welcome(false) + '\n\n' + 'Escriba *salir* si desea terminar la sesión', job.contact.msisdn, user.line, user.chn);
				}
				else if (["789"].indexOf(kw) >= 0) {
					setUserParam("step", "cr1", job.contact.msisdn, user.line, user.chn);
					pushText('Listo, su paquete será entregado en el transcurso de la siguiente semana\n\n' + Welcome(false) + '\n\n' + 'Escriba *salir* si desea terminar la sesión', job.contact.msisdn, user.line, user.chn);
				} else {
					pushText('Número de guia incorrecto, Ingrese su número de guia nuevamente.', job.contact.msisdn, user.line, user.chn);
				}
				break;

			//*Aqui me llega la ubicacion
			case "cr1-centrega":
				if (job.msg.type == 'location') {
					console.log('QUE ES', job.contact.msisdn)
					console.log('ENTRANDO A LA ENTREGA')
					setUserParam("step", "cr2-centrega", job.contact.msisdn, user.line, user.chn);
					pushText('Listo, hemos recibido la ubicación donde debemos entregar su paquete. Por favor agregue alguna referencia (color de la casa, lugares cercanos, etc).\n\n', job.contact.msisdn, user.line, user.chn);
				} else {
					console.log('QUE ES', job.contact.msisdn)
					pushText('Por favor enviénos su ubicación\n\n', job.contact.msisdn, user.line, user.chn);
				}

				break;

			case "cr2-centrega":
				var kw = body.toLowerCase()
				setUserParam("step", "cr1", job.contact.msisdn, user.line, user.chn);
				pushText('Gracias por la información.\n\n' + Welcome(false) + '\n\n' + 'Escriba *salir* si desea terminar la sesión', job.contact.msisdn, user.line, user.chn);
				break;

		}

		function Welcome(greet) {
			var msg = "";
			if (greet) { msg += "¡Hola, bienvenid@ a Servicios de courier!\n\n"; }
			msg += "Por favor seleccione una opción:\n\n";
			msg += "1. Rastrear mi pedido.\n";
			msg += "2. Confirmar entrega.\n";
			return msg;
		}
	},

	liberaBot: function (body, job, user, step, group, account) {
		console.log("Processing libera sub bot", step, group)
		switch (step) {
			case "l0":
				var kw = body.toLowerCase()
				setUserParam("step", "l1", job.contact.msisdn, user.line, user.chn);
				pushText(liberaWelcome(true), job.contact.msisdn, user.line, user.chn);
				break;
			//end step 0
			case "l1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Listo, en un momento uno de nuestros asesores se pondrá en contacto contigo por este canal para brindarte asistencia, espera un momento por favor.";
					var ob = { step: "idle", group: "ser", status: "active", tso: Date.now() }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					var msg = "Es posible que nos envíes por aquí una fotografía del respaldo de tu pago?\n\n";
					msg += "1. Si\n";
					msg += "2. No\n";
					var ob = { step: "l2", group: "sac" }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					var msg = "No hay problema, te pedimos disculpas si hemos enviado este mensaje sin tu autorización.\n\n";
					msg += "No dudes en escribirnos por este canal si requieres nuestra ayuda para cancelar tus obligaciones pendientes, adios!"
					pushText(msg, job.contact.msisdn, user.line, user.chn);
					resetUserParam("step", job.contact.msisdn, user.line, user.chn);
					resetUserParam("status", job.contact.msisdn, user.line, user.chn);
					resetUserParam("pin", job.contact.msisdn, user.line, user.chn);
					var ob = { group: "bl" }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
					var msg = "Te pedimos disculpas si hemos enviado este mensaje por equivocación, adios!"
					pushText(msg, job.contact.msisdn, user.line, user.chn);
					resetUserParam("step", job.contact.msisdn, user.line, user.chn);
					resetUserParam("status", job.contact.msisdn, user.line, user.chn);
					resetUserParam("pin", job.contact.msisdn, user.line, user.chn);
					var ob = { group: "wc" }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
				} else {
					var msg = "Por favor digita una de las opciones";
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				}

				break;
			case "l2":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno", "si", "ok", "yes", "bueno", "aja", "oki"].indexOf(kw) >= 0) {
					var msg = "Por favor envía por aquí una fotografía del recibo de tu pago, uno de nuestros asesores te ayudará a validarlo, gracias";
					var ob = { step: "idle", group: "valok", status: "active", tso: Date.now() }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else if (["2", "2.", "dos", "no", "nop", "no tengo"].indexOf(kw) >= 0) {
					var msg = "Espera un momento por favor";
					var ob = { step: "idle", group: "valnok", status: "active", tso: Date.now() }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else {
					var msg = "Por favor digita una de las opciones";
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				}
				break;

		}

		function liberaWelcome(greet) {
			var msg = "";
			if (greet) { msg += "¡Hola Juan Galindez, gracias por contactar a Libera, soluciones efectivas de cobranza.\n\n"; }
			msg += "Al momento registramos que mantienes una deuda pendiente con *Banco Equis Ye Zeta* por un valor de *USD $1,639.13*\n\n";
			msg += "Por favor selecciona una de las siguientes opciones:\n\n";
			msg += "1. Necesito ayuda para cancelar esta deuda.\n";
			msg += "2. La deuda ya fue cancelada.\n";
			msg += "3. No me envien más mensajes por favor.\n";
			msg += "4. Yo no soy Juan Galindez.\n";
			return msg;

		}
	},

	accessBot: function (body, job, user, step, group, account) {
		console.log("Processing access sub bot", step, group)
		switch (step) {
			case "q0":
				var kw = body.toLowerCase()
				getContact(job.contact.msisdn, function (contact) {
					if (contact) {
						console.log(job.contact.msisdn, "contact found", contact.id);
						var ob = { step: "q1", name: contact.name, account: contact.id }
						updateUser(ob, job.contact.msisdn, user.line);
						pushText(accessWelcome(true, contact.name), job.contact.msisdn, user.line, user.chn);
					} else {
						var msg = "Hola! No tenemos registrado este número, por favor contacta con administración para actualizar tu información.\nGracias!";
						pushText(msg, job.contact.msisdn, user.line, user.chn);
						salir(job, user);
					}
				});
				break;
			//end step 0
			case "q1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var fname = job.contact.msisdn + "_" + Date.now() + ".png"
					qr.generateCodeFromStringToFile(fname, "code:demo_123456", function (code) {
						if (code) {
							console.log(new Date(), "Media saved on server for", fname);
							var msg = "Listo, utiliza el siguiente QR Code para ingresar. Recuerda que este código expira en 15 minutos y solo puede ser utilizado una vez.";
							console.log("https://v3.textcenter.net/qrs/" + fname)
							pushText(msg, job.contact.msisdn, user.line, user.chn, null, "img", null, "https://v3.textcenter.net/qrs/" + fname, null);
							setTimeout(function () { salir(job, user); }, 3000);

							//~ var parts = dataURI.split(";base64,");
							//~ var contentType = parts[0].replace("data:","");
							//~ aws.putObject("v3/coniski/demos/qrs/" +fname, dataUriToBuffer(dataURI), contentType, function(res){
							//~ if (res.status=="ok") {
							//~ console.log (new Date(), "Media saved on AWS for", fname);
							//~ var msg = "Listo, utiliza el siguiente QR Code para ingresar. Recuerda que este código expira en 15 minutos y solo puede ser utilizado una vez.";
							//~ console.log (msg)
							//~ pushText (msg, job.contact.msisdn, user.line, user.chn, null, "img", null, "https://wsp2crmcdn.s3.amazonaws.com/v3/coniski/demos/qrs/" +fname, null);
							//~ setTimeout( function() { salir(job, user); }, 2000 );
							//~ } else {
							//~ var msg = "Te pedimos disculpas, este servicio está temporalmente fuera de servicio. Por favor intenta nuevamente en unos minutos.";
							//~ pushText (msg, job.contact.msisdn, user.line, user.chn);
							//~ salir(job, user);
							//~ }	
							//~ });

						} else {
							var msg = "Te pedimos disculpas, este servicio está temporalmente fuera de servicio. Por favor intenta nuevamente en unos minutos.";
							pushText(msg, job.contact.msisdn, user.line, user.chn);
							salir(job, user);
						}
					})

				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					var msg = "Listo, en un momento uno de nuestros asesores se pondrá en contacto contigo por este canal para brindarte asistencia, espera un momento por favor.";
					var ob = { step: "idle", group: "ser", status: "active", tso: Date.now() }
					updateUser(ob, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else {
					var msg = "Por favor digita una de las opciones";
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				}

				break;

		}

		function accessWelcome(greet, name) {
			var msg = "";
			if (greet) { msg += "¡Hola " + name + ", gracias por contactar a Demo Access Bot.\n\n"; }
			msg += "Por favor selecciona una de las siguientes opciones:\n\n";
			msg += "1. Generar QR Code de Acceso.\n";
			msg += "2. Contactar un agente de servicio.\n";
			return msg;

		}
	},

	l3dpBot: function (body, job, user, step, group, account) {
		console.log("Processing liveness 3d sub bot", step, group)
		switch (step) {
			case "3d0":
				var pin = tools.generateNumberPin();
				var msg = "Este es el demo de nuestra tecnología de validación de identidad *Liveness 3D Plus*.\n\n";
				msg += "Nuestra tecnología utiliza un video en vivo de la persona combinado con una clave de seguridad.\n\n";
				msg += "Utilizando AI validamos: la clave de acceso pronunciada, una biometría del rostro y control _anti-spoof_ del video.\n\n\n";
				msg += "Comencemos! Sigue estas instrucciones\n";
				msg += "Envía un video selfie diciendo la siguiente frase:\n\n";
				msg += "_Mi clave segura es *" + (pin + "").split("").join(" ") + "*_";
				msg += "\n\nAsegura que el video sea claro, que tu rostro esté cerca de la cámara y pronuncia los números de *uno en uno*";
				updateUser({ step: "3d1", pin: pin, tspin: Date.now() }, job.contact.msisdn, user.line, user.chn);
				pushText(msg, job.contact.msisdn, user.line, user.chn);
				break;
			//end step 0
			case "3d1":
				var lapse = Date.now() - user.tspin
				//if (false) {
				if (lapse > (5 * 60 * 1000)) {
					var pin = tools.generateNumberPin();
					var msg = "Lo sentimos, el tiempo para enviar el video ha expirado :(\n\n";
					msg += "Recuerda pronuncia los números de tu clave de *uno en uno*:\n\n";
					msg += "Probemos una *clave nueva*. Envía un video selfie diciendo la siguiente frase:\n\n";
					msg += "_Mi clave segura es " + (pin + "").split("").join(" ") + "_";
					updateUser({ pin: pin, tspin: Date.now() }, job.contact.msisdn, user.line, user.chn);
					pushText(msg, job.contact.msisdn, user.line, user.chn);
				} else {
					if (job.msg.content.video) {
						//get video 
						updateUser({ step: "3d2" }, job.contact.msisdn, user.line, user.chn);
						pushText("Un momento por favor mientras procesamos tu video, puede tardar un par de minutos...", job.contact.msisdn, user.line, user.chn);
						validateAudioPIN(job.msg.content.video.url, user)
					} else {
						pushText("Debes enviar un video selfie con la frase: _Mi clave segura es " + (user.pin + "").split("").join(" ") + "_", job.contact.msisdn, user.line, user.chn);
					}
				}
				break;
			//end step 1
			case "3d2":
				pushText("Un momento por favor mientras procesamos tu video, puede tardar un par de minutos...", job.contact.msisdn, user.line, user.chn);
				break;
			//end step 2
			case "3d3":
				//validate cédula
				var ced = body.replace(/\D/g, '') + "";
				console.log("validando ced", ced)
				if (ced.length == 10) {
					updateUser({ step: "3d4", ced: ced }, job.contact.msisdn, user.line, user.chn);
					pushText("Listo, por favor envía tu código dactilar, está en el reverso de tu cédula (ej: E1234I1234)", job.contact.msisdn, user.line, user.chn);
				} else {
					pushText(ced + " no parece un número de cédula correcto, por favor envía nuevamente", job.contact.msisdn, user.line, user.chn);
				}
				break;
			//end step 3
			case "3d4":
				//get data from reg civil
				var dacti = body.toUpperCase().trim()
				if (dacti.length == 10) {
					getDataRegCivil(user.ced, dacti, function (rcData) {
						if (rcData) {

							//do compare faces
							//get screen shot from disk
							var source = fs.readFileSync('./tempVids/' + user.videoFile + '-1.jpg', { encoding: 'base64' });
							compareFaces(source, rcData.Fotografia, job.contact.msisdn, function (res) {
								console.log(res)
								if (res.status == 200) {
									if (res.details.FaceMatches.length > 0) {
										//face match ok
										//do liveness 2D test
										checkLiveness2D(source, job.contact.msisdn, function (resl) {
											if (resl.status == 200) {
												//create a job to check analyze
												if (resl.details.analyse_id) {
													//crear job para revisar el resultado del analisis
													var job2 = {
														type: "check-liveness-status",
														contact: job.contact,
														fromJob: job.id,
														analyse_id: resl.details.analyse_id,
														api_evidence: resl.details.api_evidence,
														nextStep: 'finish',
														FaceMatches: res.details.FaceMatches,
														st: 'new',
														ts: Date.now(),
														runAfter: Date.now() + (requeueInt)
													}
													firebase.database().ref("/accounts/" + account + "/botJobs/").push(job2);
												} else {
													updateUser({ step: "0", ced: '***', dacti: '***' }, job.contact.msisdn, user.line, user.chn);
													pushText("Ha ocurrido un error y hemos notificado al administrador. Por favor intenta nuevamente más tarde", job.contact.msisdn, user.line, user.chn);
												}
											} else {
												updateUser({ step: "0", ced: '***', dacti: '***' }, job.contact.msisdn, user.line, user.chn);
												pushText("Ha ocurrido un error y hemos notificado al administrador. Por favor intenta nuevamente más tarde", job.contact.msisdn, user.line, user.chn);
											}
										});


									} else {
										updateUser({ step: "0" }, job.contact.msisdn, user.line, user.chn);
										var msg = "Resultado de prueba Liveness 3D Plus:\n\n";
										msg += "Clave segura por audio *✔*\n";
										msg += "Registro Civil *✔*\n";
										msg += "Biometría Facial *️❌*\n";
										msg += "Anti-spoof Liveness *❌*\n\n";
										msg += "La prueba ha terminado, gracias por usar este servicio. Adios!";
										pushText(msg, job.contact.msisdn, user.line, user.chn);
									}

								} else {
									updateUser({ step: "0", ced: '***', dacti: '***' }, job.contact.msisdn, user.line, user.chn);
									pushText("Ha ocurrido un error y hemos notificado al administrador. Por favor intenta nuevamente más tarde", job.contact.msisdn, user.line, user.chn);
								}
							});
						} else {
							updateUser({ step: "3d3", ced: '***', dacti: '***' }, job.contact.msisdn, user.line, user.chn);
							pushText("No hemos podido validar los datos enviados, por favor intenta nuevamente, envía tu número de cédula", job.contact.msisdn, user.line, user.chn);
						}
					});
				} else {
					pushText(dacti + " no parece un código dactilar correcto, por favor envía nuevamente", job.contact.msisdn, user.line, user.chn);
				}
				break;
			//end step 4
		}
	}

}
