module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' Pago Payphone', user.id, step, group)
    console.log(job)
    switch (step) {
      case "0":
        if (user.currentProcess) {
          obtenerProceso(user.currentProcess, function (evidencias) {
            if (evidencias) {
              updateUser({ step: "1", module: "pagoPayphone" }, job.contact);
              pushText(`El valor a pagar por tu firma es de USD $${evidencias.precio} + IVA\n\n` + datosEnviar(), job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
            } else {
              pushText("Ha ocurrido , por favor intenta nuevamente más tarde.", job.contact, user.line);
              salir(job, user);
            }
          })

        } else {
          var nuevoProceso = {
            contacto: job.contact,
            st: 'new',
            tsc: Date.now(),
            type: 'pago',
            notes: 'Módulo de pagos'
          }
          crearProceso(nuevoProceso, function (pocesoId) {
            if (pocesoId) {
              updateUser({ step: "1", module: "pagoPayphone" }, job.contact);
              pushText("El valor a pagar es de USD $XXX", job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
            } else {
              pushText(msgErrorInterno(), job.contact, user.line);
              salir(job, user);
            }
          });
        }
        break;

      case "1":
        var kw = body.toLowerCase()
        //* Tarjeta de credito
        if (kw === "do-pay") {
          obtenerProceso(user.currentProcess, function (evidencias) {
            if (evidencias) {
              makeLink(job, parseFloat(evidencias.precio), function (link) {
                if (link) {
                  pushText("Utiliza el siguiente enlace para realizar tu pago:\n\n" + link, job.contact, user.line);
                  updateUser({ step: "2" }, job.contact);
                } else {
                  pushText("Ha ocurrido un error con el procesador de pagos, por favor intenta nuevamente más tarde.", job.contact, user.line);
                  salir(job, user);
                }
              })
            } else {
              pushText("Ha ocurrido , por favor intenta nuevamente más tarde.", job.contact, user.line);
              salir(job, user);
            }
          })
        } else if (kw === "go-back") {
          utapi.consultarPrecios().then((res) => {
            if (res.result) {
              updateUser({ step: "cf-ap12-2", module: "utProds" }, job.contact);
              pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
            } else {
              pushText("Ha ocurrido un error con el procesador de pagos, por favor intenta nuevamente más tarde.", job.contact, user.line);
              salir(job, user);
            }
          }).catch((err) => {
            pushText("Ha ocurrido un error con el procesador de pagos, por favor intenta nuevamente más tarde.", job.contact, user.line);
            salir(job, user);
          })
        } else if (kw === "paycode") {
          setUserParam('step', '1-paycode', job.contact, user.line)
          pushText('Por favor escribe el código de compra', job.contact, user.line)
        } else if (kw === "transfer") {
          setUserParam('step', '1-transfer', job.contact, user.line)
          pushText('Por favor envíanos una foto de la transferencia', job.contact, user.line)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;
      case "2":
        var kw = body.toLowerCase()
        updateUser({ step: "2.1" }, job.contact);
        pushText(msgPayphone(), job.contact, user.line);
        break;

      case "2.1":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          utapi.consultarPrecios().then((res) => {
            if (res.result) {
              updateUser({ step: "cf-ap12-2", module: "utProds" }, job.contact);
              pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
            } else {
              pushText("Ha ocurrido un error con el procesador de pagos, por favor intenta nuevamente más tarde.", job.contact, user.line);
              salir(job, user);
            }
          }).catch((err) => {
            pushText("Ha ocurrido un error con el procesador de pagos, por favor intenta nuevamente más tarde.", job.contact, user.line);
            salir(job, user);
          })
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          updateUser({ step: "2" }, job.contact);
          pushText("Listo, esperemos a terminar el proceso de pago", job.contact, user.line);
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          salir(job, user);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "3":
        var kw = body.toLowerCase()
        pushText("Un momento por favor, estamos validando tu pago", job.contact, user.line);
        break;

      //!Casos transferencia
      //Me llega la foto de la transferencia
      case '1-transfer':
        if (job.msg['type'] === 'image') {
          //TODO: Abrir ticket y programar en el text center
        } else {
          pushText("Por favor envíanos una foto de la transferencia o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
      //!Fin casos transferencia


      //!Casos codigo de pago
      //Aqui me llega el codigo 
      case '1-paycode':
        var kw = body.toUpperCase()
        obtenerProceso(user.currentProcess, function (evidencias) {
          if (evidencias) {
            obtenerPaycodes(function (codes) {
              if (codes) {
                var codigoOb = _.findWhere(codes, { code: kw });
                if (codigoOb) {
                  if (codigoOb.vigencia_firma.indexOf(evidencias.vigencia_firma) >= 0) {
                    if (codigoOb.tipo_firma.indexOf(evidencias.tipo_firma) >= 0) {
                      if (Date.now() < codigoOb.tse) {
                        actualizarPaycode(codigoOb.update, { st: 'used'})
                        actualizarUsedby(codigoOb.update, { ref: user.id, tsu: Date.now() })
                        mods[envBot[account].postPagoModule].bot(envBot[account].postPagoBody, job, user, envBot[account].postPagoStep)
                      } else { //Codigo caducado
                        updateUser({ step: "1", module: "pagoPayphone" }, job.contact);
                        pushText(`El código que ingresaste se encuentra caducado\n\n El valor a pagar por tu firma es de USD $${evidencias.precio} + IVA\n\n`, job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
                      }
                    } else { //no coincide el tipo de firma
                      updateUser({ step: "no-tipo-firma-paycode", module: "pagoPayphone" }, job.contact);
                      pushText(`Este código no es válido para el tipo de firma que escogiste\nSelecciona una opción`, job.contact, user.line, null, "btn", [{ id: "tipo-firma", title: "Tipo de firma" }, { id: "metodo-pago", title: "Método de pago" }])
                    }
                  } else { //no coincide con las vigencias aceptadas
                    updateUser({ step: "no-vigencia-paycode", module: "pagoPayphone" }, job.contact);
                    pushText(`Este código no es válido para la vigencia de firma que escogiste\nSelecciona una opción`, job.contact, user.line, null, "btn", [{ id: "seleccionar-vigencia", title: "Seleccionar vigencia" }, { id: "metodo-pago", title: "Método de pago" }])
                  }
                } else { //Codigo no existe
                  updateUser({ step: "1", module: "pagoPayphone" }, job.contact);
                  pushText(`El codigo que ingresaste es inválido\n\n El valor a pagar por tu firma es de USD $${evidencias.precio} + IVA\n\n`, job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
                }
              } else { //No encontro codigos
                updateUser({ step: "1", module: "pagoPayphone" }, job.contact);
                pushText(`En este momento no existen códigos de compra\n\n El valor a pagar por tu firma es de USD $${evidencias.precio} + IVA\n\n`, job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
              }
            })
          } else {
            pushText("Ha ocurrido , por favor intenta nuevamente más tarde.", job.contact, user.line);
            salir(job, user);
          }
        })

        break

      case "no-vigencia-paycode":
        var kw = body.toLowerCase()
        if (kw === "metodo-pago") {
          obtenerProceso(user.currentProcess, function (evidencias) {
            if (evidencias) {
              updateUser({ step: "1", module: "pagoPayphone" }, job.contact);
              pushText(`El valor a pagar por tu firma es de USD $${evidencias.precio} + IVA\n\n` + datosEnviar(), job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
            } else {
              pushText("Ha ocurrido , por favor intenta nuevamente más tarde.", job.contact, user.line);
              salir(job, user);
            }
          })
        } else if (kw === "seleccionar-vigencia") {
          utapi.consultarPrecios().then((res) => {
            if (res.result) {
              updateUser({ step: "cf-ap12-2", module: "utProds" }, job.contact);
              pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
            } else {
              pushText("Ha ocurrido un error con el procesador de pagos, por favor intenta nuevamente más tarde.", job.contact, user.line);
              salir(job, user);
            }
          }).catch((err) => {
            pushText("Ha ocurrido un error con el procesador de pagos, por favor intenta nuevamente más tarde.", job.contact, user.line);
            salir(job, user);
          })
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      case "no-tipo-firma-paycode":
        var kw = body.toLowerCase()
        if (kw === "metodo-pago") {
          obtenerProceso(user.currentProcess, function (evidencias) {
            if (evidencias) {
              updateUser({ step: "1", module: "pagoPayphone" }, job.contact);
              pushText(`El valor a pagar por tu firma es de USD $${evidencias.precio} + IVA\n\n` + datosEnviar(), job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
            } else {
              pushText("Ha ocurrido , por favor intenta nuevamente más tarde.", job.contact, user.line);
              salir(job, user);
            }
          })
        } else if (kw === "tipo-firma") {
          updateUser({ step: "1", module: "utProds" }, job.contact);
          pushText("*Tipos de firma*\n" + "*Archivo P12:*\n" + "Recibes el certificado por email, puedes usarlo para facturación electrónica y firmar documentos.\n\n" + "*Nube:*\n" + "Utiliza nuestro aplicativo para firmar documentos en la nube desde tu teléfono o computador. *No sirve para facturación electrónica.*\n\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
      //!Fin casos codigo de pago
    }
  },

  job: function (job, user, step, group, flow, cb) {
    switch (job.type) {
      case "cancel-pf-trans":
        //ponemos la trans en cancel
        firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).update({ st: "cancel", tsu: Date.now() }); //todo: control if this fails
        //Le regresamos al step de pagar 
        obtenerProceso(user.currentProcess, function (evidencias) {
          if (evidencias) {
            updateUser({ step: "1" }, job.contact);
            pushText(`Has cancelado tu transacción de pago. Puedes intentar nuevamente.\n\nEl valor a pagar por tu firma es de USD $${evidencias.precio} + IVA`, job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
            cb({ st: "done" })
          } else {
            pushText("Ha ocurrido , por favor intenta nuevamente más tarde.", job.contact, user.line);
            salir(job, user);
          }
        })
        break;
      case "confirm-pf-trans":
        //obtenemos la trans par validar si ya fue confirmada
        updateUser({ step: "3" }, job.contact);
        firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).once('value', function (resp) {
          console.log(resp.val())
          var tx = resp.val();
          if (tx) {
            if (tx.st == "new") {
              pushText("Un momento por favor mientras validamos tu pago", job.contact, user.line);
              confirmPayment(job.tid, job.payphoneId, function (res) {
                switch (res.status) {
                  case 'fail':
                    obtenerProceso(user.currentProcess, function (evidencias) {
                      if (evidencias) {
                        updateUser({ step: "1" }, job.contact);
                        pushText(`Tu pago fue cancelado o expiró el tiempo para pagar. Puedes intentar nuevamente.\n\nEl valor a pagar por tu firma es de USD $${evidencias.precio} + IVA`, job.contact, user.line, null, "btn", [{ id: "do-pay", title: "Pagar con Payphone" }, { id: "paycode", title: "Pagar con código" }, { id: "go-back", title: "Regresar" }])
                        firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).update({ st: "cancel", tsu: Date.now() }); //todo: control if this fails
                        cb({ st: "done" });
                        updateUser({ step: "1" }, job.contact);
                      } else {
                        pushText("Ha ocurrido , por favor intenta nuevamente más tarde.", job.contact, user.line);
                        salir(job, user);
                      }
                    })
                    break;
                  case 'pending':
                    //not ready
                    cb({ st: "requeue", ts: Date.now() + requeueInt })
                    break;
                  case 'ok':
                    //payment completed
                    firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).update({ st: "ok", tsu: Date.now(), results: res.results, payphoneId: job.payphoneId }); //todo: control if this fails
                    firebase.database().ref("/accounts/" + account + "/procesos/" + user.currentProcess).update({ tx: res.results, txId: job.tid, payphoneId: job.payphoneId }); //todo: control if this fails
                    //move next
                    mods[envBot[account].postPagoModule].bot(envBot[account].postPagoBody, job, user, envBot[account].postPagoStep)
                    cb({ st: "done" });
                }
              })
            } else {
              console.log("Trans already confirmed");
              cb({ st: "done" });
            }
          } else {
            console.log("Trans not found", job.tid)
            cb({ st: "requeue", ts: Date.now() + requeueInt })
          }
        })
        break;

      default:
        cb({ st: "requeue", ts: Date.now() + requeueInt })
    }
  }
}

//setTimeout(function() { makeLink({contact:'593984252217'}, 10, function(link) { console.log ("link", link) } ) } , 3000)

function makeLink(job, pvp, cb) {
  var precio = Math.round(pvp * 100) / 100
  var iva = config.iva
  var tax = Math.round((precio * iva) * 100) / 100
  var transId = job.contact + "_" + Date.now()
  var payload = {
    amountWithTax: Math.round(precio * 10000) / 100,
    tax: Math.round(tax * 10000) / 100,
    amount: Math.round((precio + tax) * 10000) / 100,
    clientTransactionId: transId,
    currency: "USD",
    Reference: "Pago " + envBot[account].name,
    storeId: envBot[account].pf.store,
    responseUrl: envBot[account].TCAPIURL + 'paylink-ok',
    cancellationUrl: envBot[account].TCAPIURL + 'paylink-cancel'
  }

  payphone.prepareButton(payload, envBot[account].pf.token, function (data) {
    if (data) {
      var tx = {
        vendor: 'pf',
        data: data,
        contact: job.contact,
        st: 'new',
        tsc: Date.now()
      }
      firebase.database().ref("/accounts/" + account + "/tx/" + transId).set(tx); //todo: control if this fails
      //cb (data.payWithCard)
      cb("https://coniski.com/payphone/pago.html?ref=" + data.paymentId)
    } else {
      cb(false)
    }
  });
}

function confirmPayment(transId, paymentId, cb) {
  var paymentOb = {
    id: paymentId,
    clientTxId: transId
  }

  payphone.confirmPayment(paymentOb, envBot[account].pf.token, function (tx) {
    if (tx) {
      if (tx.statusCode == 2 || tx.statusCode == 3) {

        if (tx.statusCode == 2) { //pago cancelado o rechazado
          cb({ status: "fail" })
        }

        if (tx.statusCode == 3) { //pago completado
          cb({ status: "ok", results: tx })
        }
      } else {
        cb({ status: "pending" })
      }
    } else {
      cb({ status: "pending" })
    }
  });
}

function msgPayphone() {
  var msg = 'Aún está pendiente completar tu pago con PayPhone o es posible que haya expirado el link de pago, selecciona una opción:\n\n'
  msg += '1. Intentar nuevamente el pago\n'
  msg += '2. Esperar a completar el pago\n'
  msg += '3. Salir de la sesión (esta opción cancela tu proceso de solicitud)'
  return msg
}

function datosEnviar() {
  var msg = '¿Tienes listos tus documentos?, recuerda que deberás enviar:\n'
  msg += '- Foto del frente y reverso de tu cédula de identidad \n'
  msg += '- Video selfie sin lentes ni accesorios\n'
  msg += '- RUC en PDF (necesario si tu firma es para facturación electrónica)'
  return msg
}
