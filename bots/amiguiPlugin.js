// Amigui Bot as module
var qs = require('querystring');
module.exports = {
	bot: function (body, job, user, step, group, account) {
		console.log("Processing amigui sub bot", step, group)
		//only run if not a group chat
		if (job.contact.indexOf("-") < 0) {
			switch (step) {
				case "0":
					var kw = body.toLowerCase()
					setUserParam("step", "0.1", job.contact, user.line);
					pushText(amiguiCityMenu(), job.contact, user.line);
					break;
				//end step 0
				case "0.1":
					var kw = body.toLowerCase()
					setUserParam("x-city", kw, job.contact, user.line);
					setUserParam("step", "1", job.contact, user.line);
					pushText(amiguiMainMenu(true), job.contact, user.line);
					break;
				//end step 0.1
				case "1":
					var kw = body.toLowerCase()
					var group = ""
					if (["1", "1.", "*1*", "la uno", "uno", "vender", "vender ropa"].indexOf(kw) >= 0) {
						var msg = "*¡Qué buena decisión! Nosotros nos encargamos de todo 👍 de una manera muy rápida y segura*\n";
						msg += "haz tu cita, tráenos tus prendas y te pagamos en ese momento en efectivo.\n";
						msg += "En Amigui te evitamos tener que preocuparte por vender de prenda en prenda. El tiempo es oro…\n\n";
						msg += "Tráenos ropa actual en perfecto estado. Las prendas deben estar limpias, sin manchas, motas, rasgados, rotos, estirados ni muy gastadas. En el mismo estado que se los regalarías a tus amig@s o familiares.\n\n";
						msg += "Pagamos en promedio desde $0,50 hasta $5 en prendas de adultos y desde $0,50 hasta $3 por prendas de niños. Manejamos valores de $6.50 a $35 en chompas, carteras, zapatos deportivos, botas y ropa de hombre de marcas reconocidas y en modelos actuales.  Por el momento no estamos comprando ternos de mujer ni camisas de niños, hasta nuevo aviso.\n\n";
						msg += "Nos reservamos el derecho de comprar solo las prendas que consideremos adecuadas para ofrecer a nuestros clientes.\n"
						msg += "Más información: https://amigui.com.ec/pages/vende-tu-ropa\n"
						msg += "¡Gracias por unirte a este movimiento ecológico y social, pero sobretodo por hacer felices a muchas personas con tus prendas!\n\n"
						msg += "Digita una opción *Solo escribe el número*:\n\n";
						msg += "*1* Agendar una cita para vender ropa\n";
						msg += "*2* Horarios de atención y ubicaciones\n";
						msg += "*3* Regresar al menú principal\n";
						var ob = { step: "2", group: "ven" }
						updateUser(ob, job.contact, user.line);
						pushText(msg, job.contact, user.line);
					} else if (["2", "2.", "*2*", "la dos", "dos", "agendar", "cancelar", "agendar cita", "cancelar cita"].indexOf(kw) >= 0) {
						var msg = "Digita una opción *Solo escribe el número*:\n\n";
						msg += "*1* Agendar una cita para vender ropa\n"
						msg += "*2* Cancelar una cita\n"
						var ob = { step: "4", group: "ven" }
						updateUser(ob, job.contact, user.line);
						pushText(msg, job.contact, user.line);
					} else if (["3", "3.", "*3*", "la tres", "tres", "comprar", "comprar ropa"].indexOf(kw) >= 0) {
						var msg = "Utiliza el siguiente enlace www.amigui.com.ec para ir a nuestra tienda en línea, puedes comprar nuestra ropa y recibirla en casa!\nSi deseas hablar con un asesor de ventas, haz click aquí https://Wa.me/593983760038";
						pushText(msg + "\n\n" + amiguiMainMenu(false), job.contact, user.line)
					} else if (["4", "4.", "*4*", "cuatro", "la cuatro", "ayuda", "servicio", "servicio al cliente", "asesor"].indexOf(kw) >= 0) {
						if (isShopOpen()) {
							var msg = "En breve uno de nuestros asesores te atenderá.";
							var ob = { step: "idle", group: "sac", status: "active", tso: Date.now() }
							updateUser(ob, job.contact, user.line);
							logStartTicket(job.contact, user.chn, user.line);
							pushText(msg, job.contact, user.line);
						} else {
							var msg = config.horarioStr;
							pushText(msg + "\n\n" + amiguiMainMenu(false), job.contact, user.line)
						}
					} else if (["5", "5.", "*5*", "cinco", "la cinco", "experiencia", "queja", "reclamo"].indexOf(kw) >= 0) {
						var msg = "En Amigui nos gusta mejorar constantemente, cuéntanos tu experiencia.";
						var ob = { step: "idle", group: "exp", status: "active", tso: Date.now() }
						updateUser(ob, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user.line);
						pushText(msg, job.contact, user.line);

					} else {
						var msg = "Por favor digita el *número* de la opción que deseas o escribe *salir* para terminar la sesión";
						pushText(msg, job.contact, user.line);
					}

					break;
				case "2":
					var kw = body.toLowerCase()
					if (["1", "1.", "*1*", "uno", "la uno", "agendar", "cita", "agendar cita", "vender", "vender ropa"].indexOf(kw) >= 0) {
						makeCitaLinks(job, user);
					} else if (["2", "2.", "*2*", "dos", "la dos", "horarios", "locales", "ubicacion", "ubicación", "ubicaciones"].indexOf(kw) >= 0) {
						var msg = "Atendemos de *Lun a Vie de 9am a 6pm, Sáb de 10am a 2pm* en nuestros centros de compras:\n\n";
						msg += "*Quito Norte:*  Eloy Alfaro N39-313 y Gaspar de Villarroel Tel: 02-2443243\nhttps://maps.app.goo.gl/uTVQFf8q39bHVNEVA\n\n";
						msg += "*Cumbayá:* calle Bruneleschi E7199 Primavera 1. Frente a la PESEBRERA Tel:0979113502\nhttps://www.google.com/maps/place/0%C2%B012'40.9%22S+78%C2%B025'28.4%22W/@-0.2113606,-78.4271388,17z/data=!3m1!4b1!4m4!3m3!8m2!3d-0.2113606!4d-78.4245639?hl=es&entry=ttu\n\n";
						msg += "*Los Chillos:* Av. Ilaló y Río Tiputini, Quimbita's Plaza, local 9  Tel: 02-5139811\nhttps://maps.app.goo.gl/ooHKuBF1qq2jHy7y7\n\n";
						msg += "*Cuenca:* Calle Padre Matovelle 4-26 entre Manuel M. Palacios y Agustín Cueva\n https://goo.gl/maps/puLYqkgrfb3g5U7G9\n\n";
						msg += "*Ambato:* Avenida Miraflores 11-53 y las Retamas. Oficina Domus Business Lab, al lado de Cárnicos Berlín\n https://goo.gl/maps/STLQMvY4a6WnbkDZ9\n\n";
						msg += "*Ibarra:* Obando luna 5- 20 Entre Sucre y Jacinto Egas, diagonal a Neurocenter\n https://maps.app.goo.gl/Qwvi3ZY5UqjL7tjk9\n\n";
						var ob = { step: "1", group: "ven" }
						updateUser(ob, job.contact, user.line);
						pushText(msg + "\n\n" + amiguiMainMenu(false), job.contact, user.line);

					} else if (["3", "3.", "*3*", "tres"].indexOf(kw) >= 0) {
						var ob = { step: "1", group: "ven" }
						updateUser(ob, job.contact, user.line);
						pushText(amiguiMainMenu(false), job.contact, user.line);
					} else {
						var msg = "Por favor digita el *número* de la opción que deseas o escribe *salir* para terminar la sesión";
						pushText(msg, job.contact, user.line);
					}
					break;
				case "3":
					var msg = "Por favor utiliza uno de los enlaces para agendar tu cita o escribe *salir* para terminar la sesión";
					pushText(msg, job.contact, user.line);
					break;
				case "4":
					var kw = body.toLowerCase()
					if (["1", "1.", "*1*", "uno"].indexOf(kw) >= 0) {
						makeCitaLinks(job, user);

					} else if (["2", "2.", "*2*", "dos"].indexOf(kw) >= 0) {
						getEventListPerContact(user, function (arr) {
							if (arr.length > 0) {
								var activeRes = []
								var msg = "A continuación están tus citas pendientes, digita la opción que deseas cancelar *Solo escribe el número*\n\n";
								msg += "*Importante:* Las citas canceladas no se pueden reactivar. Escribe *salir* si no deseas cancelar una cita\n\n";
								for (var i = 0; i < arr.length; i++) {
									var cal = _.findWhere(calendars, { id: arr[i].resource });
									if (cal) {
										msg += (i + 1) + ". " + cal.caption + " Fecha: *" + arr[i].ymd + "* Hora: *" + formatHours(arr[i].timeStart) + "*";
										msg += '\n';
										activeRes.push(arr[i].id)
									}
								}
								updateUser({ step: "4.1", activeRes: activeRes }, job.contact, user.line);
							} else {
								var msg = "No tienes citas pendientes al momento\n\n";
								msg += amiguiMainMenu(false);
								updateUser({ step: "1" }, job.contact, user.line);
							}
							pushText(msg, job.contact, user.line);
						});

					} else {
						var msg = "Por favor digita el *número* de la opción que deseas o escribe *salir* para terminar la sesión";
						pushText(msg, job.contact, user.line);
					}
					break;
				case "4.1": //check if option is in list of active reservas
					var kw = body.toLowerCase().trim();

					if (isNaN(Number(kw))) {
						pushText("Por favor digita el *número* de la opción que deseas o escribe *salir* para terminar la sesión", job.contact, user.line);
					} else {
						var idx = Number(kw) - 1;
						if (user.activeRes[idx]) {
							updateEvent(user.activeRes[idx], { st: 'del', obs: 'Cancelado por el contacto', tsu: Date.now() }, function (updated) {
								if (updated) {
									var msg = "Listo! Tu cita fue cancelada\n\n";
									msg += amiguiMainMenu(false);
								} else {
									var msg = "Hubo un error al cancelar tu cita, por favor comunícante directamente con servicio al cliente\n\n";
									msg += amiguiMainMenu(false);
								}
								updateUser({ step: "1" }, job.contact, user.line);
								pushText(msg, job.contact, user.line);
							});

						} else {
							pushText("Por favor digita el *número* de la opción que deseas o escribe *salir* para terminar la sesión", job.contact, user.line);
						}
					}
					break;
				case "reminder": //event reminder sent
					var kw = body.toLowerCase()
					console.log('KWWWWWWWWWWWWW', kw)
					if (kw == 'si, confirmo asistencia') {
						var msg = "Listo, te esperamos!";
						var ob = { step: "0", group: "ven" }
						updateUser(ob, job.contact, user.line);
						pushText(msg, job.contact, user.line);
					} else if (kw == 'no, quiero reagendar') {
						var msg = "Digita una opción:\n\n";
						msg += "*1* Agendar una nueva cita para vender ropa\n"
						msg += "*2* Cancelar una cita\n"
						var ob = { step: "4", group: "ven" }
						updateUser(ob, job.contact, user.line);
						pushText(msg, job.contact, user.line);
					} else if (kw == 'no, cancelar la cita') {
						var msg = "Digita una opción:\n\n";
						msg += "*1* Agendar una nueva cita para vender ropa\n"
						msg += "*2* Cancelar una cita\n"
						var ob = { step: "4", group: "ven" }
						updateUser(ob, job.contact, user.line);
						pushText(msg, job.contact, user.line);
					} else {
						var msg = "Por favor digita el *número* de la opción que deseas o escribe *salir* para terminar la sesión";
						pushText(msg, job.contact, user.line);
					}
					break;

				case "999": //main menu
					setUserParam("step", "1", job.contact, user.line);
					resetUserParam('pin', job.contact);
					pushText(amiguiMainMenu(false), job.contact, user.line);
					break;
			} //send switch steps
		}//end check if group

		function amiguiCityMenu() {
			var msg = "¡Hola, bienvenid@ a Amigui! ¿De qué parte de Ecuador nos escribes?\n\n";
			msg += "*1* Quito\n";
			msg += "*2* Cuenca\n";
			msg += "*3* Ambato\n";
			msg += "*4* Ibarra\n";
			return msg;
		}

		function amiguiMainMenu(greet) {
			var msg = "";
			if (greet) { msg += "_¡Alarga la vida de la ropa y haz feliz a alguien más!_\n\n"; }
			msg += "Digita una opción *Solo escribe el número*:\n\n";
			msg += "*1* Vender la ropa que ya no usas\n";
			msg += "*2* Agendar/cancelar una cita\n";
			msg += "*3* Comprar ropa\n";
			msg += "*4* Comunicarse con un asesor\n";
			msg += "*5* Cuéntanos tu experiencia\n";
			return msg;
		}
	}
}
