var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var os = require('os');
var client = "hughes";
var booted = false;
var jobs = []
var months = ["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"];
var sDuration = 60*60*1000*3; //3 hours
//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		getSAN("HEC123") 
		getSAN("HEC2000061874")
		booted = true;
		listenToJobs();
		processJobQueue();
		 
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(100) );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+client+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var guess = 0;
		if (user.step) {step = user.step};
		if (user.guess) {guess = user.guess};
		
		console.log ("User "+job.contact+" is at step:", step, "guess", guess);
		
		//Reset session if last MO was over 3 hours ago
		var diff = Date.now() - lastMessageTS(user);
		console.log (diff)
		if (diff>sDuration) {
			console.log ("User "+job.contact+" session expired, do reset");
			updateUser({guess: 0, step: "0", status: "archived", sessionExpired: Date.now()}, job.contact, user.line);
			user.step = "0";
			user.guess = 0;
			step = "0";
			guess = 0;
		}
		
		if (step != 'idle') {
				var type = job.msg.type;
				if (type == "txt") { type = "chat"}
				console.log ("Process MO ", type);
				switch (type) {
					case 'chat':
						if (job.isBulk) {
							var body = job.msg.m;
						} else {
							var body = job.msg.body;	
						}
						if (body.length>0) {
							console.log ("Mo: ",body)
							//trim spaces
							body = body.trim();

							//catch operations
							if (body.toLowerCase().indexOf("#") == 0) {
								var parts = body.toLowerCase().split(" ");
								//match direct operation
								switch ( parts[0] ) {
									case "#ping":
										var msg = "#pong "+ new Date();
										pushText (msg, job.contact, user.line, job.isBulk);
										break;
									case "#salir":
										var msg = "Un placer haberte servido! Regresa cuando Necesites. Recuerda que te atendió Hugo el asistente virtual de HughesNet!\n";
										pushText (msg, job.contact, user.line, job.isBulk);
										resetUserParam("step", job.contact, user.line);
										break;
									case "#server":
										var msg = "Server data:\n";
										msg += "Datetime: "+ new Date() + "\n";
										msg += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
										msg += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
										pushText (msg, job.contact, user.line, job.isBulk);
										resetUserParam("step", job.contact, user.line);
										break;
								} //end switch parts
							} else {
								var kw = body.toLowerCase();
								if (kw == "salir" || kw == "*salir*") {
									var msg = "Un placer haberte servido! Regresa cuando Necesites. Recuerda que te atendió Hugo el asistente virtual de HughesNet!\n";
									setUserParam("step", "0", job.contact, user.line);
									pushText (msg, job.contact, user.line, job.isBulk)
								} else {
									//process step
									switch (step) {
										case "0":
											//welcome
											var msg = "Hola Soy Hugo! el Asesor virtual de HughesNet\n\nYa eres cliente de HughesNet?\n\n";
											msg += "1. Si\n";
											msg += "2. No\n";
											setUserParam("step", "1", job.contact, user.line);
											pushText (msg, job.contact, user.line, job.isBulk)
											break;
											//end step 0
										case "1":
											//ask if is client
											var kw = body.toLowerCase();											
											if (["1","1.","uno","si","sí","*si*","ok","yes","ya","sii", "siii","sip","yap","claro","aja", "okey","o.k."].indexOf(kw)>=0) {
												var msg = "Ingresa tu número de cliente SAN, que inicia con HEC seguido de 10 dígitos. Ejemplo: HEC1234567890\nO ingresa el numero de cédula o ruc del titular del contrato";
												setUserParam("step", "2", job.contact, user.line);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else if (["2","2.","dos","no","*no*", "nop" ].indexOf(kw)>=0) {
												var msg = "En un momento uno de nuestros asesores comerciales te atenderá";
												updateUser({step: "idle", status: "active", group: "ventas"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else {
												var msg = " No entendí tu respuesta, por favor intenta de nuevo.";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "2":
											//find client
											var kw = body.toUpperCase();
											if (kw.length == 13 && kw.substring(0,3) == 'HEC') {
												//find by SAN
												getSAN(kw, job.contact, user.line, job.isBulk, function(usr, contact, line, isBulk) {
													if (usr) {
														updateUser( {step: "4", san: usr}, job.contact, user.line );
														pushText (getMainMenu(), job.contact, user.line, job.isBulk)
													} else {
														console.log ("user NOT found with SAN", body.toUpperCase());
														if (guess >= 2) {
															var msg = "Al parecer tenemos la información incorrecta. Por favor espera unos minutos un asesor de servicio te atenderá"	
															updateUser({guess: 0, step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
															shopOpenAutoResponse(job, user);
															pushText (msg, contact, line, isBulk)
														} else {
															var msg = "Lo siento, no encontré contratos relacionados con este número. Por favor verifica tus datos y volvamos a intentarlo";
															setUserParam("guess", guess+1, job.contact, user.line);
															pushText (msg, contact, line, isBulk)
														}
													}
												});
											} else {
												//find by DNI	
												getAccount(kw, job.contact, user.line, job.isBulk, function(arr, contact, line, isBulk) {
													if (arr) {
														if (arr.length>1) {
															var msg = "Encontré mas de una cuenta asociada con ese número de cédula. Por favor elige el número de SAN sobre el que estás consultando:\n\n";
															var sans = []
															console.log (arr)
															for (var i=0; i < arr.length; i++) {
																msg += (i+1)+". "+arr[i].id+"\n";
																sans.push ( arr[i].id );
															}
															updateUser( {step: "3", san: arr, sanOpts: sans}, job.contact, user.line );
															pushText (msg, job.contact, user.line, job.isBulk)
														} else {
															updateUser( {step: "4", san: arr[0]}, job.contact, user.line );
															pushText (getMainMenu(), job.contact, user.line, job.isBulk)
														}
													} else {
														console.log ("user NOT found with SAN", body.toUpperCase());
														if (guess >= 3) {
															var msg = "Al parecer tenemos la información incorrecta. Por favor espera unos minutos un asesor de servicio te atenderá"	
															updateUser({guess: 0, step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
															shopOpenAutoResponse(job, user);
															pushText (msg, contact, line, isBulk)
														} else {
															var msg = "Lo siento, no encontré contratos relacionados con este número. Por favor verifica tus datos y volvamos a intentarlo";
															setUserParam("guess", guess+1, job.contact, user.line);
															pushText (msg, contact, line, isBulk)
														}
													}
												});
											}
											break;
											//end step 2
										case "3":
											//multi san account select
											var kw = body.toUpperCase();
											if (kw.length == 13 && kw.substring(0,3) == 'HEC') {
												var idx = user.sanOpts.indexOf(kw);
												if (idx>=0) {
													if (user.sanOpts[idx]) {
														updateUser( {step: "4", san: user.san[idx]}, job.contact, user.line );
														pushText (getMainMenu(), job.contact, user.line, job.isBulk)
													}
												}
											} else {
												var opt = Number(kw);
												if (!isNaN(opt)) {
													if (user.sanOpts[(opt-1)]) {
														updateUser( {step: "4", san: user.san[(opt-1)]}, job.contact, user.line );
														pushText (getMainMenu(), job.contact, user.line, job.isBulk)
													}
												}
											}
											break;
											
										case "4":
											//account info menu
											var kw = body.toLowerCase();
											if (["1","1.","uno","one","facturacion","facturación","pagos","pago","factura"].indexOf(kw)>=0) {
												setUserParam("step", "4.1", job.contact, user.line);
												pushText (getMenuFactura(), job.contact, user.line, job.isBulk)	
											} else if (["2","2.","dos","two","problemas","problema","técnicos","tecnicos","técnico","tecnico"].indexOf(kw)>=0) {
												setUserParam("step", "4.2", job.contact, user.line);
												pushText (getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)	
											} else if (["3","3.","tres","three","problemas","administración","administracion","cuenta"].indexOf(kw)>=0) {
												setUserParam("step", "4.3", job.contact, user.line);
												pushText (getMenuAdminCuenta(), job.contact, user.line, job.isBulk)	
											} else if (["4","4.","cuatro","four","promociones", "promo", "rifa", "sorteos", "sorteo"].indexOf(kw)>=0) {
												setUserParam("step", "4.4", job.contact, user.line);
												pushText (getMenuPromos(), job.contact, user.line, job.isBulk)	
											} else if (["5","5.","cinco","five","volver"].indexOf(kw)>=0) {
												var msg = "Ingresa tu número de cliente SAN, que inicia con HEC seguido de 10 dígitos. Ejemplo: HEC1234567890\nO ingresa el numero de cédula o ruc del titular del contrato";
												setUserParam("step", "2", job.contact, user.line);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else {
												var msg = "Por favor escribe el número de la opción con la que necesitas soporte o escribe *salir* para terminar la sesión";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "4.1":
											//account info menu
											var kw = body.toLowerCase();
											if (["1","1.","uno","one"].indexOf(kw)>=0) {
												setUserParam("step", "4.1.1", job.contact, user.line);
												pushText (getMenuFormaPago(), job.contact, user.line, job.isBulk)	
											} else if (["2","2.","dos","two"].indexOf(kw)>=0) {
												//get user san and build data
												var msg = "Tu última factura fue emitida el "+user.san.fe+" por el valor de "+user.san.vuf+". Tu factura vence el "+user.san.fv+".\n\n"
												var due = Number(user.san.due.replace(",","."))
												if (due>0) {
													msg += "Actualmente tienes un valor pendiente de pago de $"+user.san.due;
													
												} else {
													msg += "Actualmente no tienes valores pendientes de pago de pago. Tu último pago de $"+user.san.vup+" fue recibido el "+user.san.fup;
												}
												pushText (msg + separator() + getMenuFactura(), job.contact, user.line, job.isBulk)	
											} else if (["3","3.","tres","three"].indexOf(kw)>=0) {
												var msg = "Recuerda que tu factura se emite en el mismo día en que se activó tu servicio. Por ejemplo, si tu servicio fue activado el 5 de agosto, el día 5 de cada mes recibirás una nueva factura, lo que significa que el 5 de cada mes, se reestablecen tus datos porque empieza un nuevo periodo de consumo. Nuestro servicio se paga al inicio del periodo y tienes 15 días para hacerlo desde que se emite la factura.\n\n";
												msg += "El pago lo puedes realizar por los siguientes canales:\n\n";
												msg += "Para pagos en efectivo, utilizando tu número de cliente (SAN): "+user.san.id+" en:\n";
												msg += "- Banco de Guayaquil\n";
												msg += "- Ponle+\n";
												msg += "- Pagoágil\n";
												msg += "- Servipagos\n";
												msg += "Para pagos vía web:\n";
												msg += "- Con tarjeta de crédito o débito desde el botón de pagos de HughesNet: http://ech.edocnube.com/eDoc_Ecuador/ButtonPayEc/Login.aspx\n";
												msg += "- Si eres cliente de Banco Guayaquil lo puedes hacer utilizando su Web y App de banca virtual.\n\n";
												pushText (msg + separator() + getMenuFactura(), job.contact, user.line, job.isBulk)	
											} else if (["4","4.","cuatro","four"].indexOf(kw)>=0) {
												//~ var msg = "Te entiendo, las facturas a mi siempre me marean\n\n";
												//~ msg += "Dale clic en este enlace https://enlace.video para que puedas ver un breve video explicativo sobre los detalles de tu factura enlace video factura\n\n";
												//~ pushText (msg + separator() + getMenuFactura(), job.contact, user.line, job.isBulk)	
												var msg = "Por favor espera unos minutos enseguida un asesor se comunicará contigo";
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else if (["5","5.","cinco","five"].indexOf(kw)>=0) {
												var msg = "Por su puesto! Para esto voy a necesitar que un asesor de servicio me de una mano.\n\n";
												msg += "Por favor espera unos minutos enseguida un asesor se comunicará contigo";
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else if (["6","6.","seis","six"].indexOf(kw)>=0) {
												var msg = "Que raro! Tu factura se envió por correo electrónico a "+user.san.email+". Ese es tu correo?"
												setUserParam("step", "4.1.2", job.contact, user.line);
												pushText (msg, job.contact, user.line, job.isBulk)		
											} else if (["7","7.","siete","seven","asesor","ayuda"].indexOf(kw)>=0) {
												var msg = "No hay problema! Por favor espera unos minutos enseguida un asesor de servicio te atenderá.";
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else if (["8","8.","ocho","eight","volver"].indexOf(kw)>=0) {
												//~ var msg = "Ingresa tu número de cliente SAN, que inicia con HEC seguido de 10 dígitos. Ejemplo: HEC1234567890\nO ingresa el numero de cédula o ruc del titular del contrato";
												//~ setUserParam("step", "2", job.contact, user.line);
												//~ pushText (msg, job.contact, user.line, job.isBulk)
												updateUser( {step: "4"}, job.contact, user.line );
												pushText (getMainMenu(), job.contact, user.line, job.isBulk)
											} else {
												var msg = "Por favor escribe el número de la opción con la que necesitas soporte o escribe *salir* para terminar la sesión";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "4.1.1":
											//payment options
											var kw = body.toLowerCase();
											if (["1","1.","uno","one"].indexOf(kw)>=0) {
												var msg = "Genial! Sigue estos sencillos pasos:\n\n";
												msg += "- Dale clic a este enlace http://ech.edocnube.com/eDoc_Ecuador/ButtonPayEc/Login.aspx\n";
												msg += "- Ingresa con tu número de cliente SAN "+user.san.id+"\n";
												msg += "- Llena los datos solicitados de tu tarjeta y paga\n";
												msg += "- Listo!\n\n";
												setUserParam("step", "4.1", job.contact, user.line);
												pushText (msg + separator() + getMenuFactura(), job.contact, user.line, job.isBulk);	
											} else if (["2","2.","dos","two"].indexOf(kw)>=0) {
												var msg = "Genial! Sigue este link para ingresar a tu banca en línea:\n\n";
												msg += "https://bancavirtual.bancoguayaquil.com/BMultiPersonas/indexAlternativoP.html\n\n";
												setUserParam("step", "4.1", job.contact, user.line);
												pushText (msg + separator() + getMenuFactura(), job.contact, user.line, job.isBulk);
											} else if (["3","3.","tres","three"].indexOf(kw)>=0) {
												var msg = "Los pagos en efectivo los puedes realizar utilizando tu número de cliente (SAN): "+user.san.id+" en:\n\n";
												msg += "- Banco Guayaquil\n";
												msg += "- Ponle+\n";
												msg += "- Pagoágil\n";
												msg += "- Servipagos\n\n";
												setUserParam("step", "4.1", job.contact, user.line);
												pushText (msg + separator() + getMenuFactura(), job.contact, user.line, job.isBulk);	
											} else if (["4","4.","cuatro","four","volver"].indexOf(kw)>=0) {
												setUserParam("step", "4.1", job.contact, user.line);
												pushText (getMenuFactura(), job.contact, user.line, job.isBulk)	
											} else {
												var msg = "Por favor escribe el número de la opción con la que necesitas soporte o escribe *salir* para terminar la sesión";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "4.1.2":
											//valid email?
											var kw = body.toLowerCase();
											if (["si","sí","*si*","ok","yes"].indexOf(kw)>=0) {
												var msg = "Perfecto! No hay problema en este instante enviaremos nuevamente tu factura y en unos minutos te debe llegar. Si no la ves en tu bandeja de entrada, por favor revisa la bandeja de correos no deseados y/o spam";
												//setUserParam("step", "2", job.contact, user.line);
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else if (["no","*no*"].indexOf(kw)>=0) {
												var msg = "Con razón! Te pido disculpas al parecer tenemos la información incorrecta. Te voy a pasar con un asesor de servicio para que actualice tus datos y te envíe tu factura. Por favor espera unos minutos";
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else {
												var msg = " No entendí tu respuesta, por favor intenta de nuevo.";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "4.2":
											//tech support
											var kw = body.toLowerCase();
											if (["1","1.","uno","one"].indexOf(kw)>=0) {
												var msg = "Tu contraseña la puedes encontrar en la parte de atrás de tu módem\n";
												msg += "Da clic en este elnace para ver una imagen de referencia:\n\n";
												msg += "https://wsp2crmcdn.s3.amazonaws.com/hughes/contrasenaEnDispositivo.png";
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)	
											} else if (["2","2.","dos","two"].indexOf(kw)>=0) {
												var msg = "Muy simple:\n\n";
												msg += "- Ingresa a https://www.systemcontrolcenter.com\n";
												msg += "- Da clic en el enlace Ajustes de Wi-Fi, en el lado izquierdo de la pantalla\n";
												msg += "- Ingresa la contraseña *admin*\n";
												msg += "- Una vez iniciado sesión verás la contraseña\n";
												msg += "- Borra la contraseña actual y escribe una nueva. Elije algo fácil de recordar, pero que no sea predecible.\n";
												msg += "- Da clic en Guardar Ajustes\n";
												msg += "- Listo!\n\n";
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)	
											} else if (["3","3.","tres","three"].indexOf(kw)>=0) {
												var msg = "Recuerda que la velocidad de internet baja cuando has agotado los datos de tu plan\n\n";
												msg += "El tener muchos dispositivos conectados al mismo tiempo también afectará tu experiencia al navegar, ya que todos los dispositivos conectados compartirán la velocidad de la red\n\n";
												msg += "Para revisar cuantos GIGAS tienes disponibles:\n\n";
												msg += "- Ingresa a https://selfcare.hughesnet.com.ec \n";
												msg += "- Ingresa tu nombre de usuario y contraseña\n";
												msg += "- Listo! En la pantalla de inicio puedes ver la información sobre el *Uso Actual*\n\n";
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)	
											} else if (["4","4.","cuatro","four"].indexOf(kw)>=0) {
												//mejor usar un wizard en un web page
												var msg = "Oh No! Antes de ponerte en contacto con un asesor de servicio revisemos si aún tienes datos en tu plan:\n\n";
												msg += "Para revisar cuantos GIGAS tienes disponibles:\n\n";
												msg += "- Ingresa a https://selfcare.hughesnet.com.ec\n";
												msg += "- Ingresa tu Nombre de Usuario y contraseña\n";
												msg += "- Listo! En la pantalla de inicio puedes ver la información sobre el *Uso Actual*\n\n";
												msg += "Aún tienes datos en tu plan?";
												setUserParam("step", "4.2.1", job.contact, user.line);
												pushText (msg, job.contact, user.line, job.isBulk)	
											} else if (["5","5.","cinco","five"].indexOf(kw)>=0) {
												var msg = "Tener varios dispositivos conectados a Internet en forma simultánea pueden provocar un deterioro en la velocidad y un consumo acelerado de los datos de tu plan\n\n";
												msg += "HughesNet ofrece una variedad de planes de servicio para adaptarse a sus necesidades de uso de Internet\n\n";
												msg += "Plan 20 MEGAS - 1 o 2 dispositivos\n";
												msg += "Plan 30 MEGAS - 3 o 4 dispositivos\n";
											    msg += "Plan 40 MEGAS - Múltiples dispositivos\n";
												msg += "Si estás navegando a la velocidad mínima porque ya has agotado los datos de tu plan, recomendamos conectar únicamente 1 dispositivo a la vez.\n\n";
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)	
											} else if (["6","6.","seis","six"].indexOf(kw)>=0) {
												var msg = "Todas las actividades que realizas en internet consumen datos. Mira este video explicativo sobre el consumo de datos.\n";
												msg += "Da clic en el siguente enlace para ver el video explicativo:\n\n"
												msg += "https://wsp2crmcdn.s3.amazonaws.com/hughes/videoUsoDatos.mp4"
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)	
											} else if (["7","7.","siete","seven"].indexOf(kw)>=0) {
												var msg = "Para esto necesitamos la ayuda de un técnico. Espera unos minutos un asesor de servicio te ayudará con tu solicitud\n\n";
												msg += "Recuerda nunca mover la antena por tu cuenta ya que esto hará que pierdas la señal\n\n";
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk);
												
											} else if (["8","8.","ocho","eight","volver"].indexOf(kw)>=0) {
												//~ var msg = "Ingresa tu número de cliente SAN, que inicia con HEC seguido de 10 dígitos. Ejemplo: HEC1234567890\nO ingresa el numero de cédula o ruc del titular del contrato";
												//~ setUserParam("step", "2", job.contact, user.line);
												//~ pushText (msg, job.contact, user.line, job.isBulk)
												updateUser( {step: "4"}, job.contact, user.line );
												pushText (getMainMenu(), job.contact, user.line, job.isBulk)
											} else {
												var msg = "Por favor escribe el número de la opción con la que necesitas soporte o escribe *salir* para terminar la sesión";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "4.2.1":
											//has datos?
											var kw = body.toLowerCase();
											if (["si","sí","*si*","ok","yes"].indexOf(kw)>=0) {
												var msg = "Existen fuertes lluvias o nubosidad en tu zona?";
												setUserParam("step", "4.2.1.1", job.contact, user.line);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else if (["no","*no*"].indexOf(kw)>=0) {
												var msg = "Recuerda que la velocidad de tu internet baja cuando ya te has consumido el 100% de tus datos de uso a cualquier hora\n\n";
												msg += "Tus datos y tu velocidad se reestablecen al inicio de cada ciclo, o puedes comprar un token para recuperar la velocidad de tu plan\n\n";
												msg += "Quieres comprar un token?\n\n";
												setUserParam("step", "4.2.1.2", job.contact, user.line);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else {
												var msg = " No entendí tu respuesta, por favor intenta de nuevo.";
												pushText (msg, job.contact, user.line, job.isBulk)	
											}
											break;
										case "4.2.1.1":
											//llueve?
											var kw = body.toLowerCase();
											if (["si","sí","*si*","ok","yes"].indexOf(kw)>=0) {
												var msg = "Recuerda que las lluvias muy fuertes y alta nubosidad en la zona pueden afectar tu servicio. Por favor espera unos minutos a que el clima mejore e intenta nuevamente";
												setUserParam("step", "4.2", job.contact, user.line);
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)
											} else if (["no","*no*"].indexOf(kw)>=0) {
												var msg = "No tienes internet en un solo dispositivo?";
												setUserParam("step", "4.2.1.1.1", job.contact, user.line);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else {
												var msg = " No entendí tu respuesta, por favor intenta de nuevo.";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "4.2.1.1.1":
											//un solo dispositivo?
											var kw = body.toLowerCase();
											if (["si","sí","*si*","ok","yes"].indexOf(kw)>=0) {
												var msg = "Revisa la configuración de conexión de tu dispositivo, el dispositivo puede haberse desconfigurado. Asegúrate que esta bien conectado a tu red, reinicia tu dispositivo y vuelve a intentarlo";
												setUserParam("step", "4.2", job.contact, user.line);
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)
											} else if (["no","*no*"].indexOf(kw)>=0) {
												var msg = "Las luces del módem se encienden?";
												setUserParam("step", "4.2.1.1.1.1", job.contact, user.line);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else {
												var msg = " No entendí tu respuesta, por favor intenta de nuevo.";
												pushText (msg, job.contact, user.line, job.isBulk)	
											}
											break;
										case "4.2.1.1.1.1":
											//luces del módem?
											var kw = body.toLowerCase();
											if (["si","sí","*si*","ok","yes"].indexOf(kw)>=0) {
												var msg = "Listo! Por favor espera unos minutos y un asesor de servicio de atenderá";
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk)
											} else if (["no","*no*"].indexOf(kw)>=0) {
												var msg = "SI alguna de las luces no enciende. Desconecta tu módem del tomacorriente, espera 5 segundos, vuelve a conectarlo y vuelve a intentarlo";
												setUserParam("step", "4.2", job.contact, user.line);
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)
											} else {
												var msg = " No entendí tu respuesta, por favor intenta de nuevo.";
												pushText (msg, job.contact, user.line, job.isBulk)	
											}
											break;
										case "4.2.1.2":
											//buy token?
											var kw = body.toLowerCase();
											if (["si","sí","*si*","ok","yes"].indexOf(kw)>=0) {
												var msg = "Un token para nosotros es un paquete de datos adicionales. Tenemos paquetes de 1GB, 5GB y 10 GB. Para comprarlo:\n\n";
												msg += "- Ingresa al portal de cliente con tu nombre de usuario y contraseña. https://selfcare.hughesnet.com.ec\n";
												msg += "- Da clic en el botón de menú de las 2 barras horizontales ubicado en la parte superior derecha\n";
												msg += "- Da clic en administrar el servicio\n";
												msg += "- Da clic en comprar token\n";
												msg += "- Selecciona el token que quieres comprar\n";
												msg += "- Revisa que estás comprando el token que quieres y clic en comprar\n";
												msg += "- Listo! En 5 minutos aproximadamente se activará tú token y el valor será cargado a tu próxima factura.\n\n";
												updateUser( {step: "4.2", wantToken: true}, job.contact, user.line );
												pushText (msg + separator() + getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)
											} else if (["no","*no*"].indexOf(kw)>=0) {
												updateUser( {step: "4.2", wantToken: false}, job.contact, user.line );
												pushText (getMenuSoporteTecnico(), job.contact, user.line, job.isBulk)
											} else {
												var msg = " No entendí tu respuesta, por favor intenta de nuevo.";
												pushText (msg, job.contact, user.line, job.isBulk)	
											}
											break;
										case "4.3":
											//account admin
											var kw = body.toLowerCase();
											if (["1","1.","uno","one"].indexOf(kw)>=0) {
												setUserParam("step", "4.3.1", job.contact, user.line);
												pushText (getMenuPortal(), job.contact, user.line, job.isBulk)		
											} else if (["2","2.","dos","two"].indexOf(kw)>=0) {
												var msg = "Para cambiar los Datos de tu servicio:\n\n";
												msg += "- Ingresa al portal de cliente con tu nombre de usuario y contraseña. https://selfcare.hughesnet.com.ec\n";
												msg += "- Da clic en el botón de menú de las 3 barras horizontales ubicado en la parte superior derecha de tu pantalla\n";
												msg += "- Da clic en Mi Perfil\n";
												msg += "- Puedes cambiar correo electrónico y números de teléfonos\n";
												msg += "- Da clic en guardar y listo! tu información será actualizada en el sistema\n";
												pushText (msg + separator() + getMenuAdminCuenta(), job.contact, user.line, job.isBulk)	
											} else if (["3","3.","tres","three"].indexOf(kw)>=0) {
												var msg = "Genial! Voy a pedir que un asesor de servicio se comunique contigo para poder ver la mejor opción. Por favor espera unos minutos";
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk);
											} else if (["4","4.","cuatro","four"].indexOf(kw)>=0) {
												var msg = "No hay problema! Por favor espera unos minutos enseguida un asesor de servicio te atenderá";
												updateUser({step: "idle", status: "active", group: "servicio"}, job.contact, user.line);
												shopOpenAutoResponse(job, user);
												pushText (msg, job.contact, user.line, job.isBulk);
											} else if (["5","5.","cinco","five","volver"].indexOf(kw)>=0) {
												//~ var msg = "Ingresa tu número de cliente SAN, que inicia con HEC seguido de 10 dígitos. Ejemplo: HEC1234567890\nO ingresa el numero de cédula o ruc del titular del contrato";
												//~ setUserParam("step", "2", job.contact, user.line);
												//~ pushText (msg, job.contact, user.line, job.isBulk)
												updateUser( {step: "4"}, job.contact, user.line );
												pushText (getMainMenu(), job.contact, user.line, job.isBulk)
											} else {
												var msg = "Por favor escribe el número de la opción con la que necesitas soporte o escribe *salir* para terminar la sesión";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "4.3.1":
											//portal options
											var kw = body.toLowerCase();
											if (["1","1.","uno","one"].indexOf(kw)>=0) {
												var msg = "Simple! Para ingresar al portal de cliente sigue estos pasos:\n\n";
												msg += "- Ingresa a https://selfcare.hughesnet.com.ec\n";
												msg += "- Ingresa tu Nombre de Usuario y contraseña\n";
												msg += "- Listo! Ahora puedes administrar tu cuenta desde un solo lugar!\n\n";
												msg += "Si aún no te has registrado en portal no hay problema lo puedes hacer rápidamente:\n\n";
												msg += "- Ingresa a https://selfcare.hughesnet.com.ec\n";
												msg += "- Da clic en la pestaña que dice REGISTRO\n";
												msg += "- Completa los datos siguiendo las instrucciones que se muestran en pantalla. Pon tu mouse sobre la *i* para revisar las ayudas. Recuerda que tu SAN es "+user.san.id+" y el número de documento es el número de titular del contrato.\n";
												msg += "- Listo, ya estás registrado.\n\n"
												setUserParam("step", "4.3", job.contact, user.line);
												pushText (msg + separator() + getMenuAdminCuenta(), job.contact, user.line, job.isBulk);	
											} else if (["2","2.","dos","two"].indexOf(kw)>=0) {
												var msg = "Ingresando al portal de cliente puedes:\n\n";
												msg += "- Revisar tu consumo y saber cuantos datos tienes\n";
												msg += "- Ver y pagar tus facturas\n";
												msg += "- Pedir que te llamemos\n";
												msg += "- Hacer pruebas de velocidad\n";
												msg += "- Cambiar los datos de tu cuenta\n";
												msg += "- Comprar tokens\n";
												msg += "- Descargar tu contrato\n";
												msg += "En fin todo lo que necesitas para controlar tu servicio desde un solo lugar.\n\n";
												setUserParam("step", "4.3", job.contact, user.line);
												pushText (msg + separator() + getMenuAdminCuenta(), job.contact, user.line, job.isBulk);		
											} else if (["3","3.","tres","three"].indexOf(kw)>=0) {
												var msg = "Un token para nosotros es un paquete de datos adicionales. Tenemos paquetes de 1GB, 5GB y 10 GB. Para comprarlo debes:\n\n";
												msg += "- Ingresa al portal de cliente con tu nombre de usuario y contraseña. https://selfcare.hughesnet.com.ec\n";
												msg += "- Da clic en el botón de menú de las 2 barras horizontales ubicado en la parte superior derecha\n";
												msg += "- Da clic en administrar el servicio\n";
												msg += "- Da clic en comprar token\n";
												msg += "- Selecciona el token que quieres comprar\n";
												msg += "- Revisa que estás comprando el token que quieres y clic en comprar\n";
												msg += "- Listo! En 5 minutos aproximadamente se activará tú token y el valor será cargado a tu próxima factura.\n\n";
												updateUser( {step: "4.3", wantToken: true}, job.contact, user.line );
												pushText (msg + separator() + getMenuAdminCuenta(), job.contact, user.line, job.isBulk);		
											} else if (["4","4.","cuatro","four","volver"].indexOf(kw)>=0) {
												setUserParam("step", "4.3", job.contact, user.line);
												pushText (getMenuAdminCuenta(), job.contact, user.line, job.isBulk)	
											} else {
												var msg = "Por favor escribe el número de la opción con la que necesitas soporte o escribe *salir* para terminar la sesión";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										case "4.4":
											//promos
											var kw = body.toLowerCase();
											if (["1","1.","uno","one"].indexOf(kw)>=0) {
												var msg = "En Diciembre compra un paquete de datos adicional y recibe el doble\n\n";
												var thumb = '{"type":"b64Asset","client":"hughes","code":"hughes_1606346804099","path":"./assets/hughes_1606346804099.txt"}'
												pushText (msg + separator() + getMenuPromos(), job.contact, user.line, job.isBulk, 'bot', 'img', msg + separator() + getMenuPromos(), "https://s3.amazonaws.com/wsp2crmcdn/hughes/2629735957.png", thumb)
											} else if (["2","2.","dos","two"].indexOf(kw)>=0) {
												var msg = "No tenemos sorteos vigentes por el momento\n\n";
												pushText (msg + separator() + getMenuPromos(), job.contact, user.line, job.isBulk)	
											} else if (["3","3.","tres","three","volver"].indexOf(kw)>=0) {
												//~ var msg = "Ingresa tu número de cliente SAN, que inicia con HEC seguido de 10 dígitos. Ejemplo: HEC1234567890\nO ingresa el numero de cédula o ruc del titular del contrato";
												//~ setUserParam("step", "2", job.contact, user.line);
												//~ pushText (msg, job.contact, user.line, job.isBulk)
												updateUser( {step: "4"}, job.contact, user.line );
												pushText (getMainMenu(), job.contact, user.line, job.isBulk)
											} else {
												var msg = "Por favor escribe el número de la opción con la que necesitas soporte o escribe *salir* para terminar la sesión";
												pushText (msg, job.contact, user.line, job.isBulk)
											}
											break;
										
									}//end switch step
								}
							} //end if body is operation
						} //end if body length > 0
						//job is completeds
						finishJob(job);
						break; 
					default:
						//finish jobs
						finishJob(job);
						break;
				} //end switch mo type
		} else { //end idle check
			console.log ('user '+job.contact+'@'+user.line+' is idle');
			//job is completed
			finishJob(job);
		}
	});
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(100) );
	});
}


function pushText (body, contact, line, isBulk, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client or bulk MT queue
			var pushTo = client;
			if (isBulk) {
				pushTo = "wapi-bulk"	
			}
			firebase.database().ref("/accounts/"+pushTo+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK to", pushTo);
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(100));
}

function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).set( val );
}

function updateUser(ob, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/").update( ob );
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).remove();
}

function getSAN(kw, contact, line, isBulk, cb) {	
	firebase.database().ref("/accounts/"+client+"/sans/"+kw).once("value", function(snapshot) {
		console.log (snapshot.val())
		if ( snapshot.val() ) {
			var ob = snapshot.val();
			if (cb) { cb(ob, contact, line, isBulk); }
		} else {
			if (cb) { cb(undefined, contact, line, isBulk); }
		}
	});
}

function getAccount(kw, contact, line, isBulk, cb) {	
	firebase.database().ref("/accounts/"+client+"/sans/").orderByChild("dni").equalTo(kw).once("value", function(snapshot) {
		console.log (snapshot.val())
		if ( snapshot.val() ) {
			if (cb) { cb( _.map(snapshot.val(), function(v,k) {return v}), contact, line, isBulk); }
		} else {
			if (cb) { cb(undefined, contact, line, isBulk); }
		}
	});
}

function getMainMenu() {
	var str = "Escribe el número de la opción con la que necesitas soporte:\n\n";
	str += "1. Facturación y pagos\n";
	str += "2. Problemas Técnicos\n";
	str += "3. Administración de tu cuenta\n";
	str += "4. Promociones y Sorteos\n";
	str += "5. VOLVER\n\n";
	str += "Escribe *salir* si deseas terminar la sesión";
	return str;		
}

function getMenuFactura() {
	var str = "Escribe el número de la opción con la que necesitas soporte:\n\n";
	str += "1. Pagar factura\n";
	str += "2. Valor y fecha de pago\n";
	str += "3. Donde y cuando pagar\n";
	str += "4. No entiendo mi factura\n";
	str += "5. Necesito explicación de los valores de mi factura\n";
	str += "6. No me llegó mi factura\n";
	str += "7. Comuníquenme con un asesor\n";
	str += "8. VOLVER\n\n";
	str += "Escribe *salir* si deseas terminar la sesión";
	return str;		
}

function getMenuSoporteTecnico() {
	var str = "Escribe el número de la opción con la que necesitas soporte:\n\n";
	str += "1. Cuál es la contraseña de mi red\n";
	str += "2. Cómo cambiar la contraseña del mi red\n";
	str += "3. Mi internet está muy lento\n";
	str += "4. No tengo internet\n";
	str += "5. Cuántos dispositivos puedo conectar a mi red?\n";
	str += "6. Cómo se consumen mis datos\n";
	str += "7. Quiero reubicar la antena.\n";
	str += "8. VOLVER\n\n";
	str += "Escribe *salir* si deseas terminar la sesión";
	return str;		
}

function getMenuAdminCuenta() {
	var str = "Escribe el número de la opción con la que necesitas soporte:\n\n";
	str += "1. Consultas sobre el portal de cliente\n";
	str += "2. Cambiar datos de mi cuenta\n";
	str += "3. Quiero cambiar de plan\n";
	str += "4. Comuníquenme con un asesor\n";
	str += "5. VOLVER\n\n";
	str += "Escribe *salir* si deseas terminar la sesión";
	return str;		
}

function getMenuPromos() {
	var str = "Escribe el número de la opción con la que necesitas soporte:\n\n";
	str += "1. Promociones\n";
	str += "2. Sorteos\n";
	str += "3. VOLVER\n\n";
	str += "Escribe *salir* si deseas terminar la sesión";
	return str;		
}

function getMenuFormaPago() {
	var str = "Escribe el número de la opción con la que necesitas soporte:\n\n";
	str += "1. Pagar con tarjeta de crédito o débito\n";
	str += "2. Banca virtual Banco Guayaquil\n";
	str += "3. Pago en efectivo\n";
	str += "4. VOLVER\n\n";
	str += "Escribe *salir* si deseas terminar la sesión";
	return str;		
}

function getMenuPortal() {
	var str = "Escribe el número de la opción con la que necesitas soporte:\n\n";
	str += "1. Cómo Ingresar al portal de cliente\n";
	str += "2. Para qué sirve el portal de cliente\n";
	str += "3. Quiero comprar tokens\n";
	str += "4. VOLVER\n\n";
	str += "Escribe *salir* si deseas terminar la sesión";
	return str;		
}

function separator() {
	var str = "     ```------------<>------------```\n\n";
	return str;
}

function lastMessageTS(user) {
	var arr = _.sortBy(user.msgs, function(m) {return -m.ts});
	if (arr[1]) {
		return arr[1].ts;
	} else {
		return Date.now();
	}
}

function shopOpenAutoResponse(job, user) {
	if (!isShopOpen()) {
		var msg = " Nuestro horario de atención es de Lunes a Viernes desde las 08:00 hasta las 20:00. Dentro de este horario, estaremos gustosos de poder atenderte."
		pushText (msg, job.contact, user.line, job.isBulk);
	}
	//~ var msg = "Estimado (a), Por motivos de feriado nuestra atención se reanudará el día lunes 4 enero 2021 a partir de las 08:00. HUGHESNET INTERNET SATELITAL LES DESEA UN FELIZ AÑO!"
	//~ pushText (msg, job.contact, user.line, job.isBulk);
}

function isShopOpen() {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (t>=800 && t<2000) {
		return true;
	} else {
		return false;		
	}	
}
