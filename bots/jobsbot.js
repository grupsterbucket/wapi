var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var booted = false;
var jobs = []


//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/jobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job "+job.type+" queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {

	switch (job.type) {
		case 'request-assign':
			assignRep (job);
			break;
		default:
			finishJob(job, {st: 'unknown-type', tsu: Date.now()})
	}
}

function assignRep (job) {
	firebase.database().ref("/accounts/"+job.client+"/chats/"+job.contact).once('value', function(res) {
		var user = res.val();
		if (user) {
			if (user.rep) {
				finishJob(job, {st: 'already-assigned', tsu: Date.now()})   	
			} else {
				firebase.database().ref("/accounts/"+job.client+"/chats/"+job.contact+"/rep").set(job.rep, function(res) {
					finishJob(job, {st: 'assign-rep-ok', tsu: Date.now()})  
				}).catch(function(error) {
					finishJob(job, {st: 'cannot-assign-rep', tsu: Date.now()})  
				});	
			}
		} else {
			finishJob(job, {st: 'user-not-found', tsu: Date.now()})   	
		}
	}).catch(function(error) {
		finishJob(job, {st: 'cannot-fetch-user', tsu: Date.now()})   
	});
}

function finishJob(job, ob) {
	firebase.database().ref("jobs/"+job.id).update(ob, function(res) {
		console.log (new Date(), "Job completed", job.client, job.contact, job.rep, ob.st);
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}


