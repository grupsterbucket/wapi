var request = require('request');
global.tools = require('./tools');
var fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
global.main = require('./botsMain');
var bot = require('./demoPlugin');
var upBot = require('./urbaPlugin');
global._ = require('underscore');
global.firebase = require('firebase');
global.CryptoJS = require("crypto-js");
global.account = "demo";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 100;
global.config = {}
global.calendars = []
global.players = []
//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		loadAccountConfig();
		setTimeout(function(){
			listenToJobs();
			processJobQueue();
		}, 1000);
	}	
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/'+account+'/config/misc').on('value', function(snapshot) {		  
		config = snapshot.val();
		players = config.players;
		console.log ("Config fetched for account",account);
	});
	firebase.database().ref('accounts/'+account+'/config/calendars').on('value', function(snapshot) {		  
		calendars = _.map(snapshot.val(), function(ob, key){
			ob.id = key;
			return ob;
		});
		console.log ("Calendars fetched for account",account);
	});
}
function listenToJobs() {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	}
}

function processJob(job) {
	switch (job.type) {
		case 'send-notification':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				main.pushText (job.str, job.contact, user.line);
				//job is completed
				finishJob(job);
			});
			break;
		case 'reset-bot':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				main.resetBot (job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
	    case 'booking-completed':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				main.resetBot (job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
		case 'finish-order':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				//job is completed
				finishJob(job);
			});
			break;
		default:
		if (job.contact != "593968720200") {
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
					var user = snapshot.val();
					var step = "0";
					var group = "main";
					if (user.step) {step = user.step};
					if (user.group) {group = user.group};
					
					if (user.tsso) {
						console.log ("User "+job.contact+" is at step:", step, "group:", group);
					} else {
						main.startSession (job.contact, user);	
						console.log ("New Session for "+job.contact+"");
					}
					
					if (step != 'idle') {
							//console.log (job)
							console.log ("Process MO ", job.msg.type);
							var msgType = job.msg.type
							if (msgType == "chat") { msgType = "txt"}
							if (msgType == "") { msgType = "txt"}
							switch (msgType) {
								case 'txt':
									if (job.msg.m) {
										var body = job.msg.m;
									} else {
										var body = job.msg.body;
									}
									console.log ("Body", body);
									if (body.length>0) {
										//trim spaces
										body = body.trim();

										//catch operations
										if (body.toLowerCase().indexOf("#") == 0) {
											var parts = body.toLowerCase().split(" ");
											//match direct operation
											switch ( parts[0] ) {
												case "#salir":
													main.salir(job, user);
													break;
												case "#ping":
													var str = "#pong "+ new Date();
													main.pushText (str, job.contact, user.line);
													break;
												case "#up":
													main.updateUser({step:"u0"}, job.contact); 
													upBot.bot("na", job, user, "u0", group, account);
													break;
												case "#persona":
													var cedula = parts[1];
													if (tools.isCedula(cedula)) {
														//call datofast and get data
														tools.datoFastHook(cedula, function(str, raw) {
															main.pushText (str, job.contact, user.line)
														});
													} else {
														main.pushText ("No encontramos una persona con la cédula "+cedula, job.contact, user.line)
													}
													break;
												case "#empresa":
													var ruc = parts[1];
													//call datofast and get data
													tools.datoFastHookEmpresa(ruc, function(str, raw) {
														main.pushText (str, job.contact, user.line)
													});
													break;
												case "#desculeo":
													main.pushText ("No hay!", job.contact, user.line)
													break;
												case "#comoestoy":
													main.pushText ("Valiendo Verga!", job.contact, user.line)
													break;
												case "#elfrelon":
													main.pushText ("Los volvió a culear!", job.contact, user.line)
													break;
												case "#quiensoy":
													console.log (job)
													switch(job.msg.contact) {
														case "593984252217":
															main.pushText ("El Mejor Deck Builder del Ecuador!", job.contact, user.line)
															break;
														case "593998701679":
															main.pushText ("Net decker lechón!", job.contact, user.line)
															break;
														case "593979162082":
															main.pushText ("King of spite play!", job.contact, user.line)
															break;
														default:
															main.pushText ("N00b!", job.contact, user.line)
															break;
													}
													
													break;
												case "#placa":
													var placa = parts[1];
													//call datofast and get data
													tools.datoFastHookVehiculo(placa, function(str, raw) {
														main.pushText (str, job.contact, user.line)
													});
													break;
												case "#btc":
													request('https://blockchain.info/tobtc?currency=USD&value=1', function (error, resp, body) {
														var btc = Number(body);
														main.pushText ( "BTC AVG = USD" + (1/btc).toFixed(2) , job.contact, user.line )
													});
													break;
												case "#fact":
													main.pushText ( "Ricki es the best ever mtg player de este grupo" , job.contact, user.line )
													break;
												case "#ticker":
													if (parts[1]) {
														request('https://api.blockchain.com/v3/exchange/tickers', function (error, resp, body) {
															if (error) {
																console.log (error)
																main.pushText ( "Blockchain API error", job.contact, user.line )
															} else {
																var resp = _.sortBy(JSON.parse(body), function(ob) {return ob.symbol});
																if (parts[1].toUpperCase() == "ALL") {
																	var msg =  "*ALL TICKERS*\n\n";
																	for (var i=0; i<resp.length; i++) {
																		msg +=  "*" + resp[i].symbol + "*\n";
																		msg += "price_24h = " + resp[i].price_24h + "\n";
																		msg += "volume_24h = " + resp[i].price_24h + "\n";
																		msg += "last_trade_price = " + resp[i].price_24h + "\n\n";
																	}	
																	main.pushText ( msg , job.contact, user.line )
																} else {
																	var s = _.findWhere(resp, {symbol: parts[1].toUpperCase() })
																	if (s) {
																		//~ {
																			//~ "symbol": "DAI-USD",
																			//~ "price_24h": 1.0011,
																			//~ "volume_24h": 11471.9449,
																			//~ "last_trade_price": 1.0012
																		//~ }
																		var msg =  "*" + parts[1].toUpperCase() + "*\n";
																		msg += "price_24h = " + s.price_24h + "\n";
																		msg += "volume_24h = " + s.price_24h + "\n";
																		msg += "last_trade_price = " + s.price_24h + "\n";
																		main.pushText ( msg , job.contact, user.line )
																	} else {
																		main.pushText ( "*"+parts[1].toUpperCase()+"* ticker not found", job.contact, user.line )
																	}
																}
															}
														});
														
													} else {
														main.pushText ( "Invalid OP", job.contact, user.line )
													}
													break;
												case "#card":
													if (parts[1]) {
														var op = parts.shift();
														var searchFor = parts.join("+").toLowerCase();
														console.log ("Search for",searchFor);
														request('https://api.scryfall.com/cards/named?fuzzy='+searchFor, function (error, resp, body) {
															if (error) {
																console.log (error)
																main.pushText ( "Search card API error", job.contact, user.line )
															} else {
																var resp = JSON.parse(body);
																console.log (resp)
																if (resp.status == 404) {
																	main.pushText ( "Card not found", job.contact, user.line )
																} else {
																	var msg =  "*" + resp.name + " " + resp.mana_cost+"*\n";
																	if (resp.power) {
																		msg += "_" + resp.type_line + "_ "+resp.power+"/"+resp.toughness+"\n";
																	} else {
																		msg += "_" + resp.type_line + "_\n";
																	}
																	msg += "" + resp.oracle_text + "\n\n";
																	msg += "*Prices:*\n";
																	for (var p in resp.prices) {
																		if (resp.prices[p]) {
																			msg += p.toUpperCase().replace(/_/g," ") + " "+resp.prices[p]+ "\n";
																		}
																	}
																	if (resp.image_uris) {
																		msg += "\n";
																		msg += resp.image_uris.normal
																	} else {
																		if (resp.card_faces) {
																			msg += "\n";
																			msg += resp.card_faces[0].image_uris.normal;
																			msg += "\n";
																			msg += resp.card_faces[1].image_uris.normal;
																		}	
																	}
																	main.pushText ( msg , job.contact, user.line )
																}
																
															}
														});
														
													} else {
														main.pushText ( "Invalid OP", job.contact, user.line )
													}
													break;
												case "#server":
													var str = "Server data:\n";
													str += "Datetime: "+ new Date()+"\n";
													str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
													str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
													main.pushText (str, job.contact, user.line);
													break

												
												case "#on":
													var str = "Bot logger started";
													main.pushText (str, job.contact, user.line);
													main.setUserParam("step","3", job.contact, user.line);
													main.setUserParam("sub","#all", job.contact, user.line);
													break;
												case "#logs":
													var pin = Math.floor(Math.random() * 99988877) + 56774892  
													var str = "https://logger.textcenter.net/#login/"+job.contact;
													str += "\n\npin: *"+pin+"*";
													main.pushText (str, job.contact, user.line);
													main.setUserParam("pin",pin, job.contact, user.line);
													break;
												case "#off":
													var str = "Bot logger stopped";
													main.pushText (str, job.contact, user.line);
													main.setUserParam("step","0", job.contact, user.line);
													break;
												case "##":
													if (user.sub != "#all") {
														var str = user.sub + " subject closed";
														main.pushText (str, job.contact, user.line);
														main.setUserParam("step","3", job.contact, user.line);
														main.setUserParam("sub","#all", job.contact, user.line);
													}
													break;
												case "#tally":
													var pin = Math.floor(Math.random() * 625500) + 100000;
													var authStr = encr(job.contact+"|demo|"+pin+"|"+Date.now());												
													makeShortURL(authStr, function(str) {
														msg = "https://tally.textcenter.net/#home/SC@"+str;
														main.updateUser({pin: pin}, job.contact, user.line);	
														main.pushText (msg, job.contact, user.line);
													});	
													break;
												case "#lm":
													if (step == "0") {
														if (parts[1]) {
															var str = "CEDH game log started for date " + parts[1];
															main.setUserParam("matchDate", parts[1], job.contact, user.line);
														} else {
															var str = "CEDH game log started";
														}
														main.pushText (str, job.contact, user.line);
														main.setUserParam("step","4", job.contact, user.line);
														var d = new Date();
														var match = { id: d.getFullYear() + twoDigits(d.getMonth()+1) + twoDigits(d.getDate()) + twoDigits(d.getMinutes()) + (Math.floor(Math.random() * 100) + 1 ) }
														main.setUserParam("match", match, job.contact, user.line);
														
													}
													break;
												case "#vm":
													if (user.match) {
														var str = JSON.stringify(user.match,null,2);
													} else {
														var str = "Current match log not found"
													}
													main.pushText (str, job.contact, user.line);
													break;
												case "#fm":
													findMatch(parts[1], function(m) {
														if (m) {
															var str = JSON.stringify(m,null,2);
														} else {
															var str = "Match *"+parts[1]+"* not found"
														}
														main.pushText (str, job.contact, user.line);	
													});
													
													
													break;
												case "#plyrs":
													var msg = "CEDH Players:\n\n"
													for (var i=0; i<players.length; i++) {
														msg += players[i].code + " -> " + players[i].name + "\n"
													}
													main.pushText ( msg, job.contact, user.line);	
													break;
												case "#sm":
													//validate
													var valid = true;
													var err = ""
													if (user.match) {
														var match = user.match;
														if (!match.ps) {
															valid = false;
															err = "Players not set";
														} else if (match.ps.length<3 || match.ps.length>4) {
															valid = false;	
															err = "Must have 3 or 4 players";
														}
														
														if (!match.wd || !match.wp) {
															valid = false;	
															err = "Winner not set";
														}
														
														if (!match.d) {
															if (user.matchDate) {
																//set today as default
																match.d = user.matchDate
															} else {
																valid = false;	
																err = "Match date not set";
															}	
														}
														if (valid) {
															
															var str = "Match saved!\n\n"
															str += "Winner: *"+match.wd.toUpperCase()+"*\n";
															str += "Pilot: *"+match.wp.toUpperCase()+"*\n";
															//str += "Turn: *"+match.t+"*\n";
															str += "Date: *"+match.d+"*\n";
															str += "Ref: *"+match.id+"*\n";
															main.pushText (str, job.contact, user.line);
															main.setUserParam("step","0", job.contact, user.line);
															main.setUserParam("match",{}, job.contact, user.line);
															//save season
															user.match.ssn = 9; //todo: make this config
															saveMatch (user.match);
														} else {
															var str = "Cannot save: incomplete match data\n\n"+err+"\n\n"+JSON.stringify(match,null,2);
															main.pushText (str, job.contact, user.line);
														}
													}
													break;
												default:
													if (user.step == "3") {
														if (user.sub == "#all") {
															//start new subject
															var str = parts[0] + " subject open";
															main.pushText (str, job.contact, user.line);
															main.setUserParam("step","3", job.contact, user.line);
															main.setUserParam("sub",parts[0], job.contact, user.line);
															//find msg and label it
															setMsgSub (job.msg.id, job.contact, parts[0])
														}
													}
											} //end switch parts operation
										} else {
											if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
												main.salir(job, user);
											} else if(body.indexOf("#ts")>0) {
												console.log (new Date(), "Running TS");
												var str = "#tsback "+ new Date();
												main.pushText (str, job.contact, user.line);
											} else {
												//chat is not operation, run bot
												runMainBot (body.toLowerCase(),job, user, step, group)
											}
										} //end if operation
									} //body is empty
									//job is completed
									finishJob(job);
									break;
								case 'img':
									runMainBot ("[image]", job, user, step, group)
									//job is completed
									finishJob(job);
									break;
								case 'vid':
									runMainBot ("[video]", job, user, step, group)
									//job is completed
									finishJob(job);
									break;
								case 'btn':
									main.pushText ("Gracias por tu respuesta", job.contact, user.line);
									finishJob(job);
								default:
									console.log (job.msg)
									//job is completed
									finishJob(job);
									break;
							} //end switch mo type
					} else { //end idle check
						console.log ('user '+job.contact+'@'+user.line+' is idle');
						//is a reset operation?
						var body = job.msg.m;
						if (body.length>0) {
							//trim spaces
							body = body.trim();
							if (body.toLowerCase().indexOf("#") == 0) {
								if (body.toLowerCase() == "#reset") {
									main.resetBot(job.contact, user, "Reset Session OK");
								}
							}
						}
						//job is completed
						finishJob(job);
					}
				});
			} else {
				//ingnore bots talking
				//job is completed
				finishJob(job);		
			}
	} //end switch job type
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	});
}

function runMainBot(kw, job, user, step, group) {
	//do not run for groups
	if (job.contact.indexOf("-")<0 || (job.contact == "593984252217-1582926317" && step == "4") ) {
		//process step
		switch (step) {
			case "0":
				main.pushText (mainMenu(true), job.contact, user.line);
				main.setUserParam("step","1", job.contact);
				break;
				//end step 0
			case "1":
				if (["1","1.","uno","soporte","servicio"].indexOf(kw)>=0) {
					if (main.isShopOpen()) {
						main.pushText ("Listo, en un momento un agente de servicio le atenderá.", job.contact, user.line);	
						main.updateUser({step:"idle", group:"ser", status: "active", tso: Date.now()}, job.contact); 
						main.logStartTicket(job.contact, user.chn);
					} else {
						main.pushText ("Le recordamos que nuestro horario de atención por este canal es "+config.horarioStr+".\n\nGracias por su comprensión!", job.contact, user.line);
						main.salir(job, user);	
					}
					
				} else if (["2","2.","dos","productos"].indexOf(kw)>=0) {
					main.pushText ("Por favor visite nuestro website para mayor información: https://coniski.com/" + pageBreak() + mainMenu(false), job.contact, user.line);
				} else if (["3","3.","tres","demo"].indexOf(kw)>=0) {
					main.pushText (demosMenu(), job.contact, user.line);
					main.setUserParam("step","2", job.contact);	
				} else {
					main.pushText ("Por favor digite una de las opciones o escriba *salir* para terminar la sesión", job.contact, user.line);
				} 
				
				break;
				//end step 1
			case "2":
				if (["1","1.","uno","carrito","compras","pedido","orden"].indexOf(kw)>=0) {
					main.updateUser({step:"c0"}, job.contact); 
					bot.cartBot(kw, job, user, "c0", group, account);
				} else if (["2","2.","dos","cita","agenda"].indexOf(kw)>=0) {
					main.updateUser({step:"a0"}, job.contact); 
					bot.citaBot(kw, job, user, "a0", group, account);
				} else if (["3","3.","tres","banca"].indexOf(kw)>=0) {
					main.updateUser({step:"b0"}, job.contact); 
					bot.segurosBot(kw, job, user, "b0", group, account);
				} else {
					main.pushText ("Por favor digite una de las opciones o escriba *salir* para terminar la sesión", job.contact, user.line);
				} 
				
				break;
				//end step 2
			case "3":
				//logger is on, add MT to subject
				//find msg and label it
				setMsgSub (job.msg.id, job.contact, user.sub)
				break;
				//end step 3
			case "4":
				//logging liga match
				var match = user.match;
				var str = ""
				var save = false;
				//console.log ("CEDH bot input:", kw)
				
				if (kw.indexOf("@")>0) {
					console.log ("log player");
					if (!match.ps) {
						match.ps = []
						}
					if (match.ps.length<5) {
						if (kw.indexOf("*")>0) {
							//is winner
							var parts = kw.split("@");	
							var p = {
								d: parts[0].toUpperCase(),
								p: parts[1].replace("*","")
							}
							var player = _.find(players, {code: p.p});
							if (player) {
								match.ps.push(p)
								match.wd = parts[0]
								match.wp = parts[1].replace("*","")
								str = "Logged:\n\nDeck:"+ p.d+"\nPilot: "+player.name+" [*winner*]";
								save = true;
							} else {
								str = "Don't know player "+p.p;
							}
						} else {
							var parts = kw.split("@");	
							var p = {
								d: parts[0].toUpperCase(),
								p: parts[1]
							}
							var player = _.find(players, {code: p.p});
							if (player) {
								match.ps.push(p)
								str = "Logged:\n\nDeck: "+p.d+"\nPilot: "+player.name+"";
								save = true;
							} else {
								str = "Don't know player "+p.p;
							}
							
						}
					} else {
						str = "All seats filled";
					}
					
				}
				//console.log (kw)
				if (kw.indexOf("all")==0) {
					console.log ("log all players");
					if (!match.ps) {
						match.ps = []
					}
					var arr = kw.split("\n");
					//console.log (arr)
					if (arr.length == 4 || arr.length == 5) {

						for (var i=1; i<arr.length; i++) {
							var row = arr[i]
							console.log ("process row", row)
							if (row.indexOf("*")>0) {
								//is winner
								var parts = row.split(" ");	
								var p = {
									d: parts[0].toUpperCase(),
									p: parts[1].replace("*","")
								}
								var player = _.find(players, {code: p.p});
								if (player) {
									match.ps.push(p)
									match.wd = parts[0]
									match.wp = parts[1].replace("*","")
									str +=  "Logged:\n\nDeck:"+ p.d+"\nPilot: "+player.name+" [*winner*]\n";
									save = true;
								} else {
									str = "Don't know player "+p.p;
									save = false;
								}
							} else {
								var parts = row.split(" ");	
								var p = {
									d: parts[0].toUpperCase(),
									p: parts[1]
								}
								var player = _.find(players, {code: p.p});
								if (player) {
									match.ps.push(p)
									str += "Logged:\n\nDeck: "+p.d+"\nPilot: "+player.name+"\n";
									save = true;
								} else {
									str = "Don't know player "+p.p;
									save = false;
								}
							}
						}
					} else {
						str = "Only 3 or 4 players allowed";
						save = false;
					}
					
				}
				
				if (kw.indexOf("t:")==0) {
					console.log ("log turn");
					var parts = kw.split(":");
					match.t = parts[1];
					str = "Match ended in turn "+match.t;
					save = true;
				}
				
				if (kw.indexOf("d:")==0) {
					console.log ("log date");
					var parts = kw.split(":");
					if (parts[1].length == 8) {
						var dparts = parts[1].split("/")
						if (dparts.length == 3) {
							var dia = Number(dparts[0])
							var mes = Number(dparts[1])
							var ano = Number(dparts[2])
							if (isNaN(dia) || isNaN(mes) || isNaN(ano)) {
									str = "Match date incorrect, use format: dd/mm/yy";
							} else {	
								if ( (dia>0 && dia<32) && (mes>0 && mes<13) && (ano>21 && ano<25) ) {
									str = "Match date saved *"+ parts[1] + "*";
									match.d = parts[1];
									save = true;
								} else {
									str = "Match date incorrect, use format: dd/mm/yy";
								}
									
							}	
						} else {
							str = "Match date incorrect, use format: dd/mm/yy";
						}
					} else {
						str = "Match date incorrect, use format: dd/mm/yy";	
					}
				}
				
				if (kw.indexOf("rp:")==0) {
					
					var parts = kw.split(":");
					
					if (match.ps) {
						if (parts[1]=="all") {
							console.log ("remove players");
							match.ps = []
							str = "All players removed";
							save = true;
						} else {
							var idx = Number(parts[1]);
							if (match.ps[idx-1]) {
								console.log ("remove one player");
								match.ps.splice(idx-1,1);
								str = "Player removed\n\n"+JSON.stringify(match.ps,null,2);
								save = true;	
							}
						}
					}
					
				}
				
				if (kw.indexOf("draw")==0) {
					console.log ("log draw");
					str = "Draw logged";
					match.wd = "draw";
					match.wp = "draw";
					save = true;
				}
				
				if (str.length>0) {
					main.pushText (str, job.contact, user.line);	
					if (save) { main.setUserParam("match", match, job.contact, user.line); }
				}
				break;
				//end step 4
			default:
				if (step.indexOf("c")==0) {
					bot.cartBot(kw, job, user, step, group, account);	
				}
				if (step.indexOf("f")==0) {
					bot.firmaBot(kw, job, user, step, group, account);	
				}
				if (step.indexOf("a")==0) {
					bot.citaBot(kw, job, user, step, group, account);	
				}
				if (step.indexOf("b")==0) {
					bot.segurosBot(kw, job, user, step, group, account);	
				}
				if (step.indexOf("u")==0) {
					upBot.bot(kw, job, user, step, group, account);	
				}
		}//end switch step
	}
} 

global.mainMenu = function (greet) {
	if (greet) {
		var str = "Hola! Este es el demo chatbot de Coniski.\nPor favor digite una opción:\n\n";
	} else {
		var str = "Por favor digite una opción:\n\n";
	}
	str += "1. Servicio al cliente\n";
	str += "2. Información sobre nuestros productos\n";
	str += "3. Correr un demo\n\n";
	str += "Escriba *salir* si desea terminar la sesión";
	return str;		
}

global.demosMenu = function () {
	var str = "Por favor digite una opción para correr un demo:\n\n";
	str += "1. Carrito de compras\n";
	str += "2. Agendar una cita médica\n";
	str += "3. Servicios seguros\n\n";
	str += "Escriba *salir* si desea terminar la sesión";
	return str;		
}

global.pageBreak = function() {
	return '\n\n--------------------------oooo--------------------------\n\n'
}

function saveMatch (match) {
	var id = match.id;
	delete match.id;
	match.tsc = Date.now();
	match.st = "new"; //valid-1, valid, deleted, nulled
	firebase.database().ref("/accounts/"+account+"/matches/"+id).set(match);
}

function findMatch(mid, cb) {
	firebase.database().ref("/accounts/"+account+"/matches/"+mid).once('value', function(res) {
		if (res.val()) {
			cb(res.val())
		} else {
			cb(null)
		}	
	});
}
function twoDigits(n) {
	if (n<10) {
		return "0"+n;
	} else {
		return ""+n;	
	}
}

function setMsgSub (key, contact, val) {
	firebase.database().ref("/accounts/"+account+"/conves/"+contact+"/msgs/"+key+"/sub").set(val);
}

global.makeShortURL = function(authStr, cb) {
	firebase.database().ref('/shorts/').push(authStr).then(function(res) {
		console.log ("Short Pushed", res.key)
		cb(res.key)
	})
	.catch(function(error) {
		cb("err")
	});
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);	
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()	
}
