// Gamezone Bot as module
var qs = require('querystring');
module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing gamezone sub bot", step, group)
		//only run if not a group chat
		if (job.contact.indexOf("-")<0) {
			switch (step) {			
				case "0":
					var kw = body.toLowerCase()
					setUserParam("step","1", job.contact, user.line);
					pushText (mainMenu(true), job.contact, user.line);
					break;
					//end step 0
				case "1":
					var kw = body.toLowerCase()
					var group = ""
					if (["1","1.","*1*","uno"].indexOf(kw)>=0) {
						var msg = "Hola! A qué local quieres ir?\n\n";
						msg += "Por favor digita una de las siguientes opciones:\n\n";
						msg += "*1* GameZone Portal Shopping\n";
						msg += "*2* GameZone Quicentro Sur\n";
						msg += "*3* GameZone Manta Mall del Pacifico\n";
						msg += "*4* Regresar al menú principal\n";
						var ob = {step:"2", group:"cup"}
						updateUser(ob, job.contact, user.line);
						pushText (msg, job.contact, user.line);
					} else if (["2","2.","*2*","dos"].indexOf(kw)>=0) {
						var msg = "Visítanos en Instagram: https://instagram.com/gamezone.ecuador?r=nametag";
						pushText (msg + "\n\n" + mainMenu(false), job.contact, user.line)
					} else if (["3","3.","*3*","tres"].indexOf(kw)>=0) {
						var msg = "Visita nuestra página web: https://gamezone.com.ec ";
						pushText (msg + "\n\n" + mainMenu(false), job.contact, user.line)
					} else if (["4","4.","*4*","cuatro"].indexOf(kw)>=0) {
						salir(job, user);
					} else {
						var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
						pushText (msg, job.contact, user.line);
					}
					break;
				case "2":
					var kw = body.toLowerCase()
					if (["1","1.","*1*","uno"].indexOf(kw)>=0) {
						//portal
						var ob = {step:"0"}
						updateUser(ob, job.contact, user.line);
						pushText ("El Verano llegó a GameZone, pronto sabrás nuestras nuevas promociones 😃 Síguenos en nuestras redes sociales",  job.contact, user.line);
						//sendCupon(job, user, kw)
					} else if (["2","2.","*2*","dos"].indexOf(kw)>=0) {
						//quicentro
						var ob = {step:"0"}
						updateUser(ob, job.contact, user.line);
						pushText ("El Verano llegó a GameZone, pronto sabrás nuestras nuevas promociones 😃 Síguenos en nuestras redes sociales",  job.contact, user.line);
						//sendCupon(job, user, kw)
					} else if (["3","3.","*3*","tres"].indexOf(kw)>=0) {
						//manta
						var ob = {step:"0"}
						updateUser(ob, job.contact, user.line);
						pushText ("Pronto sabrás nuestras nuevas promociones 😃 Síguenos en nuestras redes sociales",  job.contact, user.line);
						//sendCupon(job, user, kw)
					} else if (["4","4.","*4*","cuatro"].indexOf(kw)>=0) {
						var ob = {step:"1", group:"cup"}
						updateUser(ob, job.contact, user.line);
						pushText (mainMenu(false),  job.contact, user.line);
					} else {
						var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
						pushText (msg, job.contact, user.line);
					}
					break;
			} //send switch steps
		}//end check if group
		
		function mainMenu(greet) {
			var msg = "";
			if (greet) { msg += "Hola, bienvenido a GameZone, los mejores parques de entretenimiento bajo techo del Ecuador\n\n"; }
			msg += "Por favor digita una de las siguientes opciones:\n\n";
			msg += "*1* Promociones\n";
			msg += "*2* Redes sociales\n";
			msg += "*3* Página web\n";
			msg += "*4* Salir\n";
			return msg;	
			
		}
	}
}
