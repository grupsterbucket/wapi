var request = require('request');
var _ = require('underscore');
var firebase = require('firebase');
var account = "demo";
var booted = false;
var itv;
var nanaITV = 315000;
var babies = ["593939498611"]


//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		itv = setInterval(function() { nana(); }, nanaITV); 
		getData(babies[0])
	}	
}

function nana() {
	for (var i=0; i<babies.length; i++) {
		getData(babies[i])
	}
}

function getData(line) {
	firebase.database().ref("/accounts/"+account+"/chats/"+line+"/tsMTQ").on('value', function(res) {
		var tsMTQ = res.val();
		if (tsMTQ) {
			firebase.database().ref("/accounts/"+account+"/chats/"+line+"/tsMOQ").on('value', function(res) {
				var tsMOQ = res.val();
				if (tsMOQ) {
					firebase.database().ref("/accounts/"+account+"/chats/"+line+"/tsMOR").on('value', function(res) {
						var tsMOR = res.val();
						if (tsMOR) {
							checkStatus(tsMTQ, tsMOQ, tsMOR, line)
						}
					});
				}
			});	
		}
	});	
}


function checkStatus (tsMTQ, tsMOQ, tsMOR, line) {
	console.log (new Date(), tsMTQ, tsMOQ, tsMOR, line)
	if (tsMOQ>0) {
		//mark is aging
		var diff = (Date.now() - tsMOQ) / 1000 / 60;
		if (diff>6) {//6 minutos old
			console.log (diff.toFixed(2),"m","ERROR");
			pushText("Linea "+line+" esta atorada. "+diff.toFixed(2)+" min ult MO.", "593984252217", "593939397007"); 
		} else {
			console.log (diff.toFixed(2),"m","we goochy");
		}
	} else {
		//mark is new
	}
}

global.pushText = function  (body, contact, line) {
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:body, rep: "nana", t:"mt", ts: Date.now(), type: "txt", st:'qed', lu: Date.now()};
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}
