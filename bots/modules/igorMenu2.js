
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' igorMenu2', user.id, step, group)
    console.log(job)
    switch (step) {
      // Me llega el saludo y el menu principal
      case "0":
        updateUser({ step: '1', module: "igorMenu2" }, job.contact);
        pushText(mainMenu2(), job.contact, user.line)
        break;

      // Me llega si selecciono hablar con un especialista
      case "1":
        var kw = body.toLowerCase()
        var msg = ''
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //Abrir ticket
          if (isShopOpen()) {
            msg = 'Pronto un especialista te contactará.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo igorMenu2")
    console.log(job)

    cb({ st: "done" })
  }
}

function mainMenu2() {
  var msg = "¡Genial! 😄\n";
  msg += "Para nuestra app:\n\n";
  msg += "1️⃣ Ve a tu tienda de aplicaciones (App Store o Google Play Store).\n";
  msg += '2️⃣ Busca "Igor".\n';
  msg += "3️⃣ Elige nuestra app.\n";
  msg += '4️⃣ Haz clic en "Descargar" o "Instalar".\n\n';
  msg += '¡Listo! Ahora disfruta de todas sus ventajas para un estacionamiento cómodo y rápido. 🅿️📲💨\n\n';
  msg += '¿Ayuda? Selecciona una opcion:\n\n';
  msg += '1. Hablar con un especialista';
  return msg

}

