var _ = require('underscore');
var firebase = require('firebase');
var c=1;
//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            boot();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function boot() {
	if (process.argv[2]) {
		console.log ("Start delete b64...");
		firebase.database().ref('/accounts/'+process.argv[2]+"/chats").limitToLast(1500).on('child_added', function(res) {
			var chat = res.val();
			chat.id = res.key;
			fixChat(chat);
		});
	}
}

function fixChat(chat){
	console.log (c, "FIX CHAT");
	c += 1;
	if (chat.msgs) {
		var msgs = _.map(chat.msgs, function(v,k) {
			v.id = k;
			return v;
		});
		for (var i=0; i<msgs.length; i++){
			var m = msgs[i];
			if (m.t == "mt" && m.type == "img") {
				if (m.thumb) {
					if (m.thumb != '[ad]') {
						console.log ("Fixing thumb for", m.id, chat.id);	
						firebase.database().ref('/accounts/'+process.argv[2]+"/chats/"+chat.id+"/msgs/"+m.id+"/thumb").set('[ad]');	
					}
					
				}	
			}
			
		}
	}
}
