//PROXY for DKManagement - DMOL
var _ = require('underscore');
var request = require('request');
var HttpCors = require('http-cors');
var http = require("http");
var fs = require('fs');
var qs = require('querystring');
var firebase = require('firebase');
var atob = require('atob');
var cats = {}
var cors = new HttpCors(); // configure (all allowed - see source for options)
const {Client, Status} = require("@googlemaps/google-maps-services-js");
const gclient = new Client({});

//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}


function bootApp() {
	 
	function processPost(req, res, callback) {
		var queryData = "";
		if(typeof callback !== 'function') return null;

		req.on('data', function(data) {
			queryData += data;
			if(queryData.length > 1e6) {
				queryData = "";
				res.writeHead(413, {'Content-Type': 'text/plain; ; charset=utf-8'}).end();
				req.connection.destroy();
			}
		});

		req.on('end', function() {
			if (req.method == 'GET') {
				req.query = qs.parse(req.url.replace(/^.*\?/, ''));
			} else {	
				req.post = queryData;
			}
			callback();
		});
	}
	
	function saveData(body) {
		var data = JSON.parse(body)
		var arr = _.map(data, function(ob, key) { 
			ob.id = key;
			return ob;
			});
		console.log (new Date(), arr.length, "records received");
		for (var i=0; i<arr.length; i++) {
			var t = arr[i]
			firebase.database().ref("/tiendas/"+t.id).update(JSON.parse(atob(t.data)));
		}
		console.log ("records saved");
	}

	
	//create server
	var app = function(req, res) {
		
		if ( cors.apply(req, res) ) { 
			res.end();  
		}   
		if(req.method == 'POST') {
			processPost(req, res, function() {
				//process route
				var segments = req.url.split("/")
				if (segments[1] == "push") {
					res.writeHead(200, "OK", {'Content-Type': 'text/plain; ; charset=utf-8'});
					res.write("Data Saved");
					res.end();
					saveData(req.post)
				} else {
					res.writeHead(404, {'Content-Type': 'text/plain'});
					res.write("Path Not Found "+req.url);
					res.end();
					
				}
					
			});//end process post
		} else {
			res.writeHead(405, {'Content-Type': 'text/plain; ; charset=utf-8'});
			res.end();	
		}//end method post check

	}
	
	http.createServer(app).listen(8888);
	console.log("API started on port 8888");
	
	geoCodeTiendas();

} //end boot app

function geoCodeTiendas() {
	console.log (new Date(),"GCODE STARTS");
	firebase.database().ref("/tiendas/").limitToLast(40000).once('value', function(res) {
		var arr = _.map(res.val(), function(ob, key) { 
			ob.id = key;
			return ob;
			});
		console.log (new Date(), arr.length, "records loaded");
		processTiendasGeoCode(arr);
		
	});
}

function processTiendasGeoCode(arr) {
	if (arr.length>0) {
		var t = arr.shift();
		console.log (new Date(),"Processing store "+t.id, arr.length);
		if (t.gcoded) {
			console.log ("Store already gcoded");
			//loop
			processTiendasGeoCode(arr);	
		} else {
			getGeoCodes(t.lat, t.lng, function(addrs) {
				var ob = {
					dirc: '',
					parr: '',
					cant: '',
					prov: '',
					gcoded: true
					}
				console.log ("found "+addrs.length+" results from gcode")
				if (addrs.length>0) {
					for ( var i=0; i<addrs.length; i++) {
						if (addrs[i].types.indexOf("street_address")>=0){
							ob.dirc = addrs[i].formatted_address;
						}
						if (addrs[i].types.indexOf("administrative_area_level_3")>=0){
							var comps = addrs[i]["address_components"]
							for ( var j=0; j<comps.length; j++) {
								if (comps[j].types.indexOf("administrative_area_level_3")>=0){
									ob.parr = comps[j]["long_name"]
								}
								if (comps[j].types.indexOf("administrative_area_level_2")>=0){
									ob.cant = comps[j]["long_name"]
								}
								if (comps[j].types.indexOf("administrative_area_level_1")>=0){
									ob.prov = comps[j]["long_name"]
								}
							}
						}
					}
					//save to cats file
					saveCat(ob)
					//save result to DB
					firebase.database().ref("/tiendas/"+t.id).update(ob);
					//save geocoding to DB
					firebase.database().ref("/gcodes/"+t.id).set(addrs);
					console.log ("Store gcode completed");
					//loop
					processTiendasGeoCode(arr);	
				} else {
					//loop
					processTiendasGeoCode(arr);	
				}
			});
		}
	} else {
		console.log (new Date(),"Reverse Geocoding is completed!");	
	}
}

function getGeoCodes(lat, lon, cb) {
	
	gclient
	  .geocode({
		params: {
		  latlng: lat+','+lon,
		  key: "AIzaSyBpW6iOiitetMndJgRrQ9V5DBfsU8DYU2Y",
		},
		timeout: 1000, // milliseconds
	  })
	  .then((r) => {
		if (r.data.results) {
			cb( r.data.results );
		} else {
			cb([])
		}
	  })
	  .catch((e) => {
		  console.log(e.response.data.error_message);
		  cb([])
	  });
}

function saveCat(ob) {
	if (!cats[ob.prov]) {
		cats[ob.prov] = {}	
	}
	if (!cats[ob.prov][ob.cant]) {
		cats[ob.prov][ob.cant] = []	
	}
	if (cats[ob.prov][ob.cant].indexOf(ob.parr) < 0) {
		cats[ob.prov][ob.cant].push(ob.parr)
	}
	//save to disk
	fs.writeFile("cats.json", JSON.stringify(cats), function(err) {
		if(err) {
			return console.log(err);
		}
		console.log("Cats file was saved!");
	}); 
}
