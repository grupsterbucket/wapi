module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' emailOTP', user.id, step, group)
    console.log(job)
    switch (step) {
      case "0":
        if (user.currentProcess) {
          updateUser({ step: "1", module: "emailOTP" }, job.contact);
          pushText('Por favor ingresa tu correo electrónico.', job.contact, user.line)
        } else {
          var nuevoProceso = {
            contacto: job.contact,
            st: 'new',
            tsc: Date.now(),
            type: 'emailOTP',
            notes: 'Validación de email por OTP'
          }
          crearProceso(nuevoProceso, function (pocesoId) {
            if (pocesoId) {
              updateUser({ step: "1", module: "utProds", currentProcess: pocesoId }, job.contact);
              pushText("*Tipo de usuario*\n" + "Selecciona una opción:\n", job.contact, user.line, null, "btn", [{ id: "firma-cf", title: "Cliente Final" }, { id: "firma-sm", title: "Socio Móvil" }, { id: "firma-mp", title: "Menú Principal" }])
            } else {
              pushText(msgErrorInterno(), job.contact, user.line);
              salir(job, user);
            }
          });
        }
        break;

      //*Aqui me llega el correo electronico
      case '1':
        var kw = body.toLowerCase()
        //*Enviar un mail con el OTP
        nxtapi.enviarCorreoOTP(kw).then((res) => {
          if (res.results.status === 200) {
            if (user.currentProcess) {
              actualizarProceso(user.currentProcess, { email: kw, mailOTP: res.pin, tries: 0, tsu: Date.now() });
              setUserParam('step', '2', job.contact, user.line)
              pushText("Se envió un mensaje a tu correo con un código\n" + "Por favor ingresa el código que te llegó al correo", job.contact, user.line)
            } else {
              setUserParam('step', '1', job.contact, user.line)
              pushText('Por favor ingresa tu correo electrónico.', job.contact, user.line)
            }
          } else {
            pushText("No se pudo enviar el mensaje a ese e-mail, por favor ingresa nuevamente tu correo electrónico\n", job.contact, user.line)
          }
        })
        break

      //* Aqui me llega el OTP y compruebo si es valido
      case '2':
        var kw = body.toLowerCase()
        obtenerProceso(user.currentProcess, function (evidencias) {
          if (evidencias) {
            if (evidencias.tries < 2) {
              if (evidencias.mailOTP === kw) {
                pushText("Listo, hemos validado tu correo.", job.contact, user.line, "bot", "txt", null, null, null, function () {
                  mods["onbo4d"].bot('Swtich Module', job, user, "0")
                });
              } else {
                pushText("Tu código es inválido, por favor envía el código nuevamente.\n\n", job.contact, user.line)
                let intentos = evidencias.tries + 1;
                actualizarProceso(user.currentProcess, { tries: intentos, tsu: Date.now() });
              }
            } else {
              setUserParam('step', '1', job.contact, user.line)
              pushText("Has superado el número máximo de intentos, por favor ingresa tu correo nuevamente", job.contact, user.line)
            }
          } else {
            setUserParam('step', '1', job.contact, user.line)
            pushText('Por favor ingresa tu correo electrónico.', job.contact, user.line)
          }
        })
        break
    }
  },

  job: function (job, user, step, group, flow, cb) {
    switch (job.type) {
      case "confirm-pf-trans":
        console.log("Trans already confirmed");
        cb({ st: "done" });
        break;
      default:
        cb({ st: "requeue", ts: Date.now() + requeueInt })
    }
  }
}
