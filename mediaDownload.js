var _ = require('underscore');
var firebase = require('firebase');
var fs = require('fs');
var path = require('path');
var booted = false;
var account = "xyz" //default
if (process.argv[2]) {
	account = process.argv[2];
}

var firebaseConfig = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };
  
firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function bootApp() {
	if (!booted) {
		booted = true;
		console.log ("Processing account ",account)
		downloadMedia();
	}
}

function downloadMedia() {
	var ref = firebase.database().ref('accounts/'+account+'/media')
	ref.limitToLast(1).once("value", function(res) {
		if (res.val()) {
			var ob = res.val();
			for (const key in ob) {
				console.log(key, ob[key].length);
				fs.writeFile(path.resolve(__dirname,'./media/'+account+'/'+key), ob[key], 'utf8', function(err) {
					if (err) {
							console.log (err)
							process.exit()
					} else {
						firebase.database().ref('accounts/'+account+'/media/'+key).remove();
						setTimeout(function(){ downloadMedia(); },250);
					}	
				});	
			}
			
		} else {
			console.log ("Done!")
			process.exit(0)	
		}
		
	});
}
