var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "demo";
var booted = false;
var jobs = []
var os = require('os');


//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(250) );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+client+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var sub = "#all";
		var group = "main";
		if (user.step) {step = user.step};
		if (user.group) {group = user.group};
		if (user.sub) {sub = user.sub};
		
		console.log ("User "+job.contact+" is at step:", step, "sub:", sub, "group:", group);
		
		if (step != 'idle') {
				console.log ("Process MO ", job.msg.type);
				switch (job.msg.type) {
					case 'txt':
						var body = job.msg.m;
						if (body.length>0) {
							//trim spaces
							body = body.trim();

							//catch operations
							if (body.toLowerCase().indexOf("#") == 0) {
								var parts = body.toLowerCase().split(" ");
								//match direct operation
								switch ( parts[0] ) {
									case "#etph":
										var str = "#etph "+ Date.now();
										pushText (str, job.contact, user.line);
										break;
									case "#ping":
										var str = "#pong "+ new Date();
										pushText (str, job.contact, user.line);
										break;
									case "#chat":
										updateUser({step:"idle", status: "active", group: "main", tso: Date.now()}, job.contact, user.line);
										pushText ("Hola! en que te puedo ayudar?", job.contact, user.line);
										break;
									case "#persona":
										var cedula = parts[1];
										if (tools.isCedula(cedula)) {
											//call datofast and get data
											tools.datoFastHook(cedula, function(str, raw) {
												pushText (str, job.contact, user.line)
											});
										} else {
											pushText ("No encontramos una persona con la cÃ©dula "+cedula, job.contact, user.line)
										}
										break;
									case "#empresa":
										var ruc = parts[1];
										//call datofast and get data
										tools.datoFastHookEmpresa(ruc, function(str, raw) {
											pushText (str, job.contact, user.line)
										});
										break;
									case "#placa":
										var placa = parts[1];
										//call datofast and get data
										tools.datoFastHookVehiculo(placa, function(str, raw) {
											pushText (str, job.contact, user.line)
										});
										break;
									case "#btc":
										request('https://blockchain.info/tobtc?currency=USD&value=1', function (error, resp, body) {
											var btc = Number(body);
											pushText ( "BTC AVG = USD" + (1/btc).toFixed(2) , job.contact, user.line )
										});
										break;
									case "#server":
										var str = "Server data:\n";
										str += "Datetime: "+ new Date()+"\n";
										str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
										str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
										pushText (str, job.contact, user.line);
										break
									case "#on":
										var str = "Bot logger started";
										pushText (str, job.contact, user.line);
										setUserParam("step","1", job.contact, user.line);
										setUserParam("sub","#all", job.contact, user.line);
										break;
									case "#logs":
										var pin = Math.floor(Math.random() * 99988877) + 56774892  
										var str = "https://logger.textcenter.net/#login/"+job.contact;
										str += "\n\npin: *"+pin+"*";
										pushText (str, job.contact, user.line);
										setUserParam("pin",pin, job.contact, user.line);
										break;
									case "#off":
										var str = "Bot logger stopped";
										pushText (str, job.contact, user.line);
										setUserParam("step","0", job.contact, user.line);
										break;
									case "##":
										if (user.sub != "#all") {
											var str = user.sub + " subject closed";
											pushText (str, job.contact, user.line);
											setUserParam("step","1", job.contact, user.line);
											setUserParam("sub","#all", job.contact, user.line);
										}
										break;
									case "#lm":
										if (step == "0") {
											var str = "Match log started";
											pushText (str, job.contact, user.line);
											setUserParam("step","2", job.contact, user.line);
											var d = new Date();
											var match = { id: d.getFullYear() + twoDigits(d.getMonth()+1) + twoDigits(d.getDate()) + twoDigits(d.getMinutes()) + (Math.floor(Math.random() * 100) + 1 ) }
											setUserParam("match", match, job.contact, user.line);
										}
										break;
									case "#vm":
										if (user.match) {
											var str = JSON.stringify(user.match,null,2);
										} else {
											var str = "Match not found"
										}
										pushText (str, job.contact, user.line);
										break;
									case "#sm":
										//validate
										var valid = true;
										var err = ""
										var match = user.match;
										if (!match.ps) {
											valid = false;
											err = "Players not set";
										} else if (match.ps.length<3 || match.ps.length>4) {
											valid = false;	
											err = "Must have 3 or 4 players";
										}
										
										if (!match.wd || !match.wp || !match.t) {
											valid = false;	
											err = "Winner and turn end not saved";
										}
										if (valid) {
											
											var str = "Match saved!\n\n\[link]\n\n"
											str += "Winner: *"+match.wd.toUpperCase()+"*\n";
											str += "Pilot: *"+match.wp.toUpperCase()+"*\n";
											str += "Ref: *"+match.id+"*\n";
											pushText (str, job.contact, user.line);
											setUserParam("step","0", job.contact, user.line);
											setUserParam("match",{}, job.contact, user.line);
											saveMatch (user.match);
										} else {
											var str = "Cannot save: incomplete match data\n\n"+err+"\n\n"+JSON.stringify(match,null,2);
											pushText (str, job.contact, user.line);
										}
										break;
									default:
										if (user.step == "1") {
											if (user.sub == "#all") {
												//start new subject
												var str = parts[0] + " subject open";
												pushText (str, job.contact, user.line);
												setUserParam("step","1", job.contact, user.line);
												setUserParam("sub",parts[0], job.contact, user.line);
												//find msg and label it
												setMsgSub (job.msg.id, job.contact, parts[0])
											}
										}
										
								} //end switch parts
							} else {
								//value of MO received
								var kw = body.toLowerCase();
								//process step
								switch (step) {
									case "0":
										//logger is off
										console.log ("Logger is off, nothing was labelled");
										break
										//end step 0
									case "1":
										//logger is on, add MT to subject
										//find msg and label it
										setMsgSub (job.msg.id, job.contact, user.sub)
										break;
										//end step 1
									case "2":
										//logging liga match
										var match = user.match;
										var str = ""
										var save = false;
										console.log ("Liga bot input:", kw)
										var players = ["fro","rim","raa","sed","anp","anj","fel","dao","ani","luc","juf","ben","jua","jab"]
										if (kw.indexOf("@")>0) {
											console.log ("log player");
											if (!match.ps) {
												match.ps = []
												}
											if (match.ps.length<5) {
												if (kw.indexOf("*")>0) {
													//is winner
													var parts = kw.split("@");	
													var p = {
														d: parts[0],
														p: parts[1].replace("*","")
													}
													if (players.indexOf(p.p)>=0) {
														match.ps.push(p)
														match.wd = parts[0]
														match.wp = parts[1].replace("*","")
														str = "D:"+p.d+" / P:"+p.p+" logged [*winner*]";
														save = true;
													} else {
														str = "Don't know player "+p.p;
													}
												} else {
													var parts = kw.split("@");	
													var p = {
														d: parts[0],
														p: parts[1]
													}
													if (players.indexOf(p.p)>=0) {
														match.ps.push(p)
														str = "D:"+p.d+" / P:"+p.p+" logged";
														save = true;
													} else {
														str = "Don't know player "+p.p;
													}
													
												}
											} else {
												str = "All seats filled";
											}
											
										}
										
										if (kw.indexOf("t:")==0) {
											console.log ("log turn");
											var parts = kw.split(":");
											str = "Turn end logged";
											match.t = parts[1];
											save = true;
										}
										
										if (kw.indexOf("rp:")==0) {
											console.log ("remove player");
											var parts = kw.split(":");
											var idx = Number(parts[1]);
											if (match.ps) {
												if (match.ps[idx-1]) {
													match.ps.splice(idx-1,1);
													str = "Player removed\n\n"+JSON.stringify(match.ps,null,2);
													save = true;	
												}
											}
											
										}
										
										if (kw.indexOf("draw")==0) {
											console.log ("log draw");
											str = "Draw logged";
											match.wd = "draw";
											match.wp = "draw";
											save = true;
										}
										
										if (str.length>0) {
											pushText (str, job.contact, user.line);	
											if (save) { setUserParam("match", match, job.contact, user.line); }
										}
										break;
										//end step 2
								}//end switch step
								
							} //end if operation
						}
						//job is completed
						finishJob(job);
						break;
					case 'img':
						finishJob(job);
						//ToDo: send image to AI for face recognition
						break;
					default:
						//finish jobs
						finishJob(job);
						break;
				} //end switch mo type
		} else { //end idle check
			console.log ('user '+job.contact+'@'+user.line+' is idle');
			//job is completed
			finishJob(job);
		}
	});
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed'};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(500));
}

function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).set(val);
}

global.updateUser = function(ob, contact) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/").update( ob );
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).remove();
}

function setMsgSub (key, contact, val) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/msgs/"+key+"/sub").set(val);
}

function saveMatch (match) {
	var id = match.id;
	delete match.id;
	match.tsc = Date.now();
	match.st = "new"; //valid-1, valid, deleted, nulled
	firebase.database().ref("/accounts/"+client+"/matches/"+id).set(match);
}
function twoDigits(n) {
	if (n<10) {
		return "0"+n;
	} else {
		return ""+n;	
	}
}
