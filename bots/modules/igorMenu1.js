
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' igorMenu1', user.id, step, group)
    console.log(job)
    switch (step) {
      // Me llega el saludo del menu1
      case "0":
        updateUser({ step: '1', module: "igorMenu1" }, job.contact);
        pushText(saludoMenu1(), job.contact, user.line)
        break;

      // Me llega el numero de cedula y mando al api
      case "1":
        var ced = body.replace(/[ -]/g, '');
        var tipoIdentidad = ''
        var documento = ''
        var msg = ''
        if (body.toLowerCase() == 'ayuda') {
          //Abre ticket
          if (isShopOpen()) {
            msg = 'Pronto un especialista te contactará.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else {
          if (ced.length == 10 && !isNaN(ced)) {
            tipoIdentidad = '2'
            documento = 'cédula'
          } else if (ced.length == 13 && !isNaN(ced)) {
            tipoIdentidad = '4'
            documento = 'RUC'
          } else {
            tipoIdentidad = '3'
            documento = 'pasaporte'
          }
          igorApi.autenticacion().then((res) => {
            igorApi.consultarUsuario(tipoIdentidad, ced, res.token).then((response) => {
              if (response.length > 0) {
                pushText(mainMenu1(response[0].datosPersonales[0].primerNombre, true), job.contact, user.line);
                updateUser({ step: '2', ced: ced, tipoIdentidad: tipoIdentidad }, job.contact);
              } else {
                pushText(`No pudimos encontrar ese número de ${documento}, por favor ingresa el número de ${documento} nuevamente o escribe *ayuda* para hablar con un especialista.`, job.contact, user.line);
              }
            }).catch((err) => {
              //catch de consultarUsuario
              console.log(err)
            })
          }).catch((err) => {
            //catch de autenticacion
            console.log(err)
          })

        }
        break;

      //Me llega la opcion del menu que selecciono
      case "2":
        var kw = body.toLowerCase()
        var msg = ''
        // Hecha un vistazo a tu saldo
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          igorApi.autenticacion().then((res) => {
            igorApi.consultarUsuario(user.tipoIdentidad, user.ced, res.token).then((response) => {
              pushText(menu21(response[0].tarjetasMonedero[0].saldo), job.contact, user.line);
              updateUser({ step: '2.1' }, job.contact);
            }).catch((err) => {
              //catch de consultarUsuario
              console.log(err)
            })
          }).catch((err) => {
            //catch de autenticacion
            console.log(err)
          })
          // Descubre tus placas registradas
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          igorApi.autenticacion().then((res) => {
            igorApi.consultarUsuario(user.tipoIdentidad, user.ced, res.token).then((response) => {
              pushText(menu22(response[0].vehiculos), job.contact, user.line);
              updateUser({ step: '2.2' }, job.contact);
            }).catch((err) => {
              //catch de consultarUsuario
              
              console.log(err)
            })
          }).catch((err) => {
            //catch de autenticacion
            console.log(err)
          })
          // Revisa los movimientos de tu cuenta
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          igorApi.autenticacion().then((res) => {
            igorApi.consultarConsumo(user.ced, res.token).then((response) => {
              pushText(menu23(response), job.contact, user.line);
              updateUser({ step: '2.3' }, job.contact);
            }).catch((err) => {
              //catch de consultarUsuario
              console.log(err)
            })
          }).catch((err) => {
            //catch de autenticacion
            console.log(err)
          })
          // Solicita una tarjeta o codigo QR 
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          //Abre ticket
          if (isShopOpen()) {
            msg = menu24()
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
          // Problemas con tu tarjeta de credito
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
          updateUser({ step: '2.5' }, job.contact);
          pushText(menu25(), job.contact, user.line);
          // Validar mis facturas de consumo
        } else if (["6", "6.", "seis"].indexOf(kw) >= 0) {
          //Abre ticket
          if (isShopOpen()) {
            msg = 'Pronto un especialista te contactará.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      // Hecha un vistazo a tu saldo
      case "2.1":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          pushText("Enlaza tu tarjeta de crédito iniciando sesión en la app o través del siguiente link: https://www.urbaparkpass.com/urbaweb/home recarga en en efectivo en cualquiera de los puntos de Urbapark. ", job.contact, user.line);
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          regresarMenu(job, user)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      // Descubre tus placas registradas
      case "2.2":
        var kw = body.toLowerCase()
        var msg = ''
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //abrir ticket
          if (isShopOpen()) {
            msg = 'Pronto un especialista te contactará.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          regresarMenu(job, user)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      // Revisa los movimientos de tu cuenta
      case "2.3":
        var kw = body.toLowerCase()
        var msg = ''
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //abrir ticket
          if (isShopOpen()) {
            msg = 'Pronto un especialista te contactará.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          regresarMenu(job, user)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      // Problemas con tu tarjeta de credito
      case "2.5":
        var kw = body.toLowerCase()
        var msg = ''
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //abrir ticket
          if (isShopOpen()) {
            msg = 'Pronto un especialista te contactará.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          regresarMenu(job, user)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo igorMenu1")
    console.log(job)

    cb({ st: "done" })
  }
}

function saludoMenu1() {
  var msg = "¡Bienvenido, cliente genial! ¡Como tu asistente personal, estoy aquí para ayudarte en lo que necesites! Por favor, ingresa tu número de cédula con el cual te registraste: \n";
  return msg
}

function mainMenu1(nombre, saludo) {
  if (saludo) {
    var msg = `¡Gracias ${nombre}! aquí tienes algunas opciones para comenzar:\n\n`;
  } else {
    var msg = 'Selecciona una de las opciones:\n\n'
  }
  msg += "1. Hecha un vistazo a tu saldo\n";
  msg += "2. Descubre tus placas registradas.\n";
  msg += "3. Revisa los movimientos de tu cuenta\n";
  msg += "4. Solicita una tarjeta o código QR\n";
  msg += "5. Problemas con tu Tarjeta de Crédito\n";
  msg += "6. Validar mis facturas de consumo\n";
  return msg
}

function menu21(saldo) {
  var msg = '¡Genial! Lo revisare en un abrir y cerrar de ojos 👀\n\n';
  msg += `¡Listo! He revisado tu cuenta y tu saldo actual es de $${saldo} \n\n`;
  msg += "Selecciona uno de los siguientes botones para continuar:\n\n";
  msg += "1. Recarga tu saldo\n";
  msg += "2. Regresa al menú anterior.\n";
  return msg
}

function menu22(vehiculos) {
  var msg = '¡Por supuesto! Puedo mostrarte las placas de tus vehículos registrados a continuación: \n\n';

  if (vehiculos.length > 0) {
    for (var i = 0; i < vehiculos.length; i++) {
      var vehiculo = vehiculos[i];
      msg += `🚗 ${vehiculo.placa}-${vehiculo.marca} ${vehiculo.modelo}\n`;
    }
    msg += '\n¡Vaya, tienes un par de bellezas en tu garaje! \n\n';
  } else {
    msg += 'No encontramos vehículos registrados.\n\n';
  }
  msg += "puedes seleccionar uno de los siguientes botones para continuar:\n\n";
  msg += "1. Agregar o eliminar un auto\n";
  msg += "2. Regresa al menú anterior.\n";
  return msg
}

function menu23(consumos) {
  var msg = '¡Claro, estás en control total! 🕵🏻 \n\n';
  msg += 'Aquí están los movimientos más recientes de tu cuenta:\n\n';

  if (consumos.length > 0) {
    for (var i = 0; i < consumos.length; i++) {
      var consumo = consumos[i];
      msg += `Fecha: ${consumo.fecha} - Descripción: ${consumo.concepto} - Monto: $${consumo.consumo}\n`;
    }
  } else {
    msg += 'No encontramos consumos recientes en tu cuenta.\n\n';
  }

  msg += "\n¡Puedes mantener un ojo en todo lo que sucede en tu cuenta y asegurarte que todo esté en orden desde la app! \n\n";
  msg += "1. Hablar con un especialista\n";
  msg += "2. Regresa al menú anterior.\n";
  return msg
}

function menu24() {
  var msg = '¡Arrasa en nuestros parqueaderos! 🚗💨\n\n';
  msg += 'Obtén tu tarjeta física para una entrada y salida cómoda. ¡Sin complicaciones! O usa tu código QR desde la app. ¡Sin tickets, sin tarjeta y sin efectivo! 😎\n\n';
  msg += "Pronto un especialista te contactará para la entrega.";
  return msg
}

function menu25() {
  var msg = '¡Oh no! Parece que tu tarjeta de crédito se tomó unas vacaciones 🏖️ ¡Pero no te preocupes! Te ayudaremos.\n\n';
  msg += 'Actualiza los datos de tu tarjeta aquí:  https://www.urbaparkpass.com/urbaweb/login o en nuestra app.\n\n';
  msg += "¿Más info?:\n\n";
  msg += "1. Hablar con especialista\n";
  msg += "2. Regresar al menú anterior.";

  return msg
}

function regresarMenu(job, user) {
  pushText(mainMenu1(' ', false), job.contact, user.line);
  updateUser({ step: '2' }, job.contact);
}

