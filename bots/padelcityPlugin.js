// Sample Bot as module
var qs = require('querystring');
module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing padel city bot", step, group)
		switch (step) {
			case "pc0":
				setUserParam("step","pc1", job.contact.msisdn, user.line, user.chn);
				pushText (mainMenuPadelCity(true), job.contact.msisdn, user.line, user.chn);
				break;
				//end step 0
			case "pc1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","agendar","reserva","cita","cancha"].indexOf(kw)>=0) {
					var msg = "Excelente!\nSigue el siguiente enlace para reservar tu espacio:\n\n";
					var pin = Math.floor(Math.random() * 625500) + 100000;
					updateUser({step:"pc2", group: "res", pin: pin}, job.contact.msisdn, user.line, user.chn);
					var authStr = encr(job.contact.msisdn+"|coniski|all|res|"+pin+"|"+Date.now())
					msg += "https://events.textcenter.net/#home/"+qs.escape(authStr);
					pushText (msg, job.contact.msisdn, user.line, user.chn);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					getCitas(job, user, account, function(arr) {
						arr = _.sortBy(arr, function(ob) { return arr.dateStart});
						//only report new and accepted and only future events
						var d = new Date();
						d.setHours(0);
						d.setMinutes(0);
						var ts = d.getTime();
						//console.log (calendars)
						evs = ""
						var pin = Math.floor(Math.random() * 625500) + 100000;
						for (var i=0; i<arr.length; i++) {
							var e = arr[i];
							if (e.dateStart>=ts) {
								if (e.st == "new" || e.st == "acc") {
									var cal = _.findWhere(calendars, {id: e.resource});
									if (cal) {
										var ed = new Date
										evs += "*Reserva "+cal.caption+"*\n"
										evs += "Fecha: *"+e.ymd+"*\n"
										evs += "Hora: *"+formatTS(e.timeStart)+"*\n"
										evs += "Estado: *"+mapEventST(e.st)+"*\n"
										var authStr = encr(job.contact.msisdn+"|coniski|"+e.resource+"|"+e.id+"|"+pin+"|"+Date.now())
										evs += "*Cancelar Reserva:*\nhttps://events.textcenter.net/#cancel/"+qs.escape(authStr);
										evs += "\n\n"
									}
								}
							}
						}
						if (evs.length>0) {
							updateUser({pin: pin}, job.contact.msisdn, user.line, user.chn);
							var msg = "A continuación tus reserva pendientes.  Utiliza el enlace para cancelar la reserva:\n\n";
							msg += evs;
						} else {
							var msg = "No tienes reservas pendientes para cancelar.\n\n";
						}
						pushText (msg + pageBreak() + mainMenuPadelCity(false), job.contact.msisdn, user.line, user.chn);
					});
					
				} else if (["3","3.","tres","info","informacion","información"].indexOf(kw)>=0) {
					pushText (infoPage() + "\n\n" + mainMenuPadelCity(false), job.contact.msisdn, user.line, user.chn);
				} else if (["4","4.","cuatro","servicio","cliente"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro personal especializado";
					updateUser({step:"idle", status: "active", group: "ser", tso: Date.now()}, job.contact.msisdn, user.line, user.chn);
					logStartTicket(job.contact.msisdn, user.chn);
					pushText (msg, job.contact.msisdn, user.line, user.chn);
				} else {												
					pushText (mainMenuPadelCity(false), job.contact.msisdn, user.line, user.chn);
				}
				break;
			case "pc2":
				var kw = body.toLowerCase();
				if (kw == "salir" || kw == "*salir*") {
					var str = "Gracias por usar este servicio. Adios!"
					pushText (str, job.contact.msisdn, user.line, user.chn);
					setTimeout(function() { resetBot (job.contact.msisdn, user); }, 250);
				} else {
					pushText ("Por favor termina de agendar tu reserva o escribe *salir* para terminar la sesión.", job.contact.msisdn, user.line, user.chn);
				}
				break;
			case "pc3":
				console.log (job)
				setUserParam("step","pc4", job.contact, user.line, user.chn);
				//crear transacción con status pending
				str = "El valor a pagar por tu reserva es de *$"+(job.event.tarifa*job.event.pax).toFixed(2)+" + IVA*.\n"
				pushText (str + "\n" + mainMenuPagosPadelCity(), job.contact, user.line, user.chn);
				break;
			case "pc4":
				var kw = body.toLowerCase()
				if (["1","1.","tarjeta"].indexOf(kw)>=0) {
					var msg = "Por favor utiliza el siguiente enlace para realizar tu pago con tarjeta de crédito\n\n";
					msg += "[link Payphone]"
					updateUser({step:"pc5"}, job.contact.msisdn, user.line, user.chn);
					pushText (msg, job.contact.msisdn, user.line, user.chn);
				} else if (["2","2.","dos","cupon","cupón"].indexOf(kw)>=0) {
					var msg = "Por favor ingresa el código del cupón de pago\n\n";
					updateUser({step:"pc5"}, job.contact.msisdn, user.line, user.chn);
					pushText (msg, job.contact.msisdn, user.line, user.chn);
				} else if (["3","3.","tres","transfer","transferencia"].indexOf(kw)>=0) {
					var msg = "Por favor realiza la transferencia a esta cuenta y envíanos una fotografía del recibo.\n\n";
					msg += "*Banco:* Produbanco\n";
					msg += "*Cuenta Corriente:* 200563212548\n";
					msg += "*Titular:* Padel City EC\n";
					msg += "*RUC:* 1796235662001";
					updateUser({step:"pc5"}, job.contact.msisdn, user.line, user.chn);
					pushText (msg, job.contact.msisdn, user.line, user.chn);
				} else if (["4","4.","cuatro","cash","efectivo"].indexOf(kw)>=0) {
					var msg = "Listo, debes realizar el pago el día de la reserva previo al acceso a la cancha.\n\n";
					msg += "Gracias por usar nuestro chat bot para agendar tu reserva.\n";
					msg += "Adios!";
					pushText (msg, job.contact.msisdn, user.line, user.chn);
					setTimeout(function() { resetBot (job.contact.msisdn, user); }, 250);
				} else {
					pushText ("Por favor selecciona una forma de pago", job.contact.msisdn, user.line, user.chn);
				}
				break;
			case "pc5":
				setUserParam("step","pc4", job.contact.msisdn, user.line, user.chn);
				str = "Listo! Hemos recibido tu pago, todo está listo para tu reserva. Nos vemos pronto!"
				pushText (str, job.contact.msisdn, user.line, user.chn);
				setTimeout(function() { resetBot (job.contact.msisdn, user); }, 250);
				break;
		}//end switch step
		
		function mainMenuPadelCity(greet) {
			if (greet) {
				var msg = "Hola!\nBienvenido a Padel City!\n\n";
				msg += "Por favor digita una opción:\n\n";
			} else {
				var msg = "*Menú Principal*\n\n";
			}
			msg += "1. Reservar una cancha\n";
			msg += "2. Cancelar una reserva\n";
			msg += "3. Información sobre Padel City\n";
			msg += "4. Servicio al cliente\n";
			return msg;
		}
		
		function mainMenuPagosPadelCity(greet) {
			var msg = "*Por favor escoge una forma de pago*\n";
			msg += "1. Tarjeta de crédito o débito\n";
			msg += "2. Ingresar cupón\n";
			msg += "3. Transferencia\n";
			msg += "4. Pago en efectivo\n";
			return msg;
		}
		
		function infoPage() {
			msg = "Padel City la mejor cancha del valle!\n\n";
			msg += "*Horarios:*\n";
			msg += "[Contenido de horarios]\n\n";
			msg += "*Ubicación:*\n";
			msg += "[Link a mapa]\n\n";
			msg += "*Website:*\n";
			msg += "[Link a website]\n\n";
			msg += "*Redes:*\n";
			msg += "[Link a RS]\n\n";
			msg += "*Términos y Condiciones:*\n";
			msg += "[Link a TyC]\n\n";
			return msg;
		}
		
		function getCitas(job, user, account, cb) {
			firebase.database().ref("/accounts/"+account+"/events/").orderByChild("account").equalTo(user.id).once('value', function(res) {
				if (res.val()) {
					cb(_.map(res.val(), function(ob, key) {
						ob.id = key;
						return (ob);
					}))
				} else {
					cb([]);	
				}
			});
		}
		
		function formatTS(n) {
			if (n<1000) {
				n+=""
				return n.substring(0,1) + ":" + n.substring(1,3);	
			} else {
				n+=""
				return n.substring(0,2) + ":" + n.substring(2,4);
			}
		}
		
		function mapEventST(str) {
			switch (str) {
				case 'new':
					return "Por confirmar";
				case 'acc':
					return "Confirmado";
				default:
					return str.toUpperCase();
			}
		}
	}
	
}
