// LATINUS API plugin
module.exports = {

	checkStatus: function(id, cb) {
		console.log ("Checking LATINUS Status", id);
		
		fetch(env.latinusApiURL+'/empresaConsulta/estadoSolicitud?idTramite='+id, {
			method: 'get',
			headers: { 'Authorization' : 'Bearer '+env.latinusApiKey+'' }
		}).then(function(response) {
			if (response.status !== 200 ) {
				console.log(new Date(),'LATINUS API error',response.status);
				response.json().then(function(data) {
					console.log(data);
				});
				cb (false);
			} else {
				response.text().then(function(data) {
					console.log ("LATINUS response length", data.length)
					if (tools.isJsonString(data)) {
						cb (JSON.parse(data))	
					} else {
						cb (false)	
					}
				});
			}
		}).catch(function(err) {
			console.log(new Date(),'LATINUS API request error',err.message);
			cb (false);
		});
	},
	
	checkStatusPago: function(id, cb) {
		console.log ("Checking LATINUS Status Pago", id);
		
		fetch(env.latinusApiURL+'/empresaConsulta/consultaPago?idTramite='+id, {
			method: 'get',
			headers: { 'Authorization' : 'Bearer '+env.latinusApiKey+'' }
		}).then(function(response) {
			if (response.status !== 200 ) {
				console.log(new Date(),'LATINUS API error',response.status);
				response.json().then(function(data) {
					console.log(data);
				});
				cb (false);
			} else {
				response.text().then(function(data) {
					console.log ("LATINUS response length", data.length)
					if (tools.isJsonString(data)) {
						cb (JSON.parse(data))	
					} else {
						cb (false)	
					}
				});
			}
		}).catch(function(err) {
			console.log(new Date(),'LATINUS API request error',err.message);
			cb (false);
		});
	}
}
