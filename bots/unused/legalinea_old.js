var request = require('request');
var tools = require('./tools');
var bot = require('./botPlugin');
var _ = require('underscore');
var firebase = require('firebase');
var account = "legalinea";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 250;


//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var sub = "#all";
		var group = "main";
		if (user.step) {step = user.step};
		if (user.group) {group = user.group};
		if (user.sub) {sub = user.sub};
		
		console.log ("User "+job.contact+" is at step:", step, "sub:", sub, "group:", group);
		
		if (step != 'idle') {
				console.log ("Process MO ", job.msg.type);
				switch (job.msg.type) {
					case 'chat':
						var body = job.msg.body;
						if (body.length>0) {
							//trim spaces
							body = body.trim();

							//catch operations
							if (body.toLowerCase().indexOf("#") == 0) {
								var parts = body.toLowerCase().split(" ");
								//match direct operation
								switch ( parts[0] ) {
									case "#salir":
										var str = "Gracias por contactar a Legalinea!\nSu sesión ha terminado."
										pushText (str, job.contact, user.line, user.cid);
										resetUserParam("step", job.contact, user.line);
										break;
									case "#ping":
										var str = "#pong "+ new Date();
										pushText (str, job.contact, user.line, user.cid);
										break;
									case "#server":
										var str = "Server data:\n";
										str += "Datetime: "+ new Date()+"\n";
										str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
										str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
										pushText (str, job.contact, user.line, user.cid);
										break;
										
								} //end switch parts
							} else {
								//value of MO received
								var kw = body.toLowerCase();
								if (kw =="salir" || kw =="*salir*") {
									var str = "Gracias por contactar a Legalinea!\nSu sesión ha terminado."
									pushText (str, job.contact, user.line, user.cid);
									resetUserParam("step", job.contact, user.line);
								} else { 
									//process step
									switch (step) {
										case "0":
											pushText (mainMenu(true), job.contact, user.line, user.cid);
											setUserParam("step","1", job.contact, user.line);
											break;
										case "1":
											kw = kw.trim();
											if (["1","1.","uno","one","consulta","consulta legal"].indexOf(kw)>=0) {
												pushText (serviceMenu(), job.contact, user.line, user.cid);
												setUserParam("step","2", job.contact, user.line);
											} else if (["2","2.","dos","two","tramite","trámite"].indexOf(kw)>=0) {
												var str = "A continuación una lista de los expedientes abiertos en su cuenta:\n\n";
												str += "Caso *223-1323*\n"
												str += "Estado Al momento su caso se encuentra en...\n\n"
												str += "Caso *223-1425*\n"
												str += "Estado Al momento su caso se encuentra en...\n\n"
												str += "Caso *223-1625*\n"
												str += "Estado Al momento su caso se encuentra en...\n\n"
												str += "++++++++++++++++++++++++++++++\n\n"
												str += mainMenu(false)
												pushText (str, job.contact, user.line, user.cid);
												setUserParam("step","1", job.contact, user.line);
											} else if (["3","3.","tres","three","subir","enviar","documento"].indexOf(kw)>=0) {
												var str = "Instrucciones:\n\n";
												str += "- Se acepta el envío de cualquier documento, video, foto o voice note.\n";
												str += "- Cada documento no puede pesar más de 10MB.\n";
												str += "- Si requiere enviar documentos más pesados, envíelos a nuestro correo: soporte@legalinea.com.\n";
												str += "- Usted puede enviar cualquier número de documentos, *uno a la vez*\n";
												str += "- Cuando haya terminado escriba *salir*\n\n";
												str += "Listo, envie los documentos por favor";
												pushText (str, job.contact, user.line, user.cid);
												setUserParam("step","2", job.contact, user.line);
											} else if (["4","4.","cuatro","four","hablar","abogado"].indexOf(kw)>=0) {
												var str = "Listo, en breve su abogado le atenderá. Espere un momento por favor.";
												//todo lookup rep and preassign
												pushText (str, job.contact, user.line, user.cid);
												updateUser({step:"idle", status: "active", group: "main", tso: Date.now()}, job.contact, user.line);
												logStartTicket(job.contact, user.chn);
												
											} else if (["5","5.","cinco","five","otro"].indexOf(kw)>=0) {
												var str = "Listo, en breve un agente de servicio al cliente le atenderá. Espere un momento por favor.";
												//todo assign group based on selected options 
												pushText (str, job.contact, user.line, user.cid);
												updateUser({step:"idle", status: "active", group: "main", tso: Date.now()}, job.contact, user.line);
												logStartTicket(job.contact, user.chn);
											} else if (["6","6.","seis","six","salir","exit","cerrar"].indexOf(kw)>=0) {
												
											} else {
												pushText ("Por favor, marque una de las opciones o escriba salir para terminar la sesión", job.contact, user.line, user.cid);
											}
											break;
										case "2":
											kw = kw.trim();
											if (["6","6.","seis","six","sei","salir"].indexOf(kw)>=0) {
												var str = "Gracias por contactar a Legalinea!\nSu sesión ha terminado."
												pushText (str, job.contact, user.line, user.cid);
												resetUserParam("step", job.contact, user.line);
											} else {
												var str = "Listo, en breve un abogado especialista le atenderá. Espere un momento por favor.";
												//todo assign group based on selected options 
												pushText (str, job.contact, user.line, user.cid);
												updateUser({step:"idle", status: "active", group: "main", tso: Date.now()}, job.contact, user.line);
												logStartTicket(job.contact, user.chn);
											}
											break;
										case "3":
											//user is sending docs, ignore anything else
											console.log ("user is sending docs");
											break;
											
									}//end switch step
								} // end kw is salir
							} //end if operation
						}
						//job is completed
						finishJob(job);
						break;
					default:
						//finish jobs
						console.log (job.msg.type)
						if (["document","image","video","audio","ptt"].indexOf(job.msg.type)>=0) {
							var str = "El archivo fue recibido y agregado a su expediente."
							pushText (str, job.contact, user.line, user.cid);
						}
						finishJob(job);
						break;
				} //end switch mo type
		} else { //end idle check
			console.log ('user '+job.contact+'@'+user.line+' is idle');
			//is a reset operation?
			var body = job.msg.body;
			if (body.length>0) {
				//trim spaces
				body = body.trim().toLowerCase();
				if ( ["#reset","#salir","salir","*salir*","exit"].indexOf(body)>=0 ) {
					console.log ('Session reset');
					var str = "Gracias por contactar a Legalinea!\nSu sesión ha terminado."
					pushText (str, job.contact, user.line, user.cid);
					resetUserParam("step", job.contact, user.line);
					resetUserParam("status", job.contact, user.line);
				}
			}
			//job is completed
			finishJob(job);
		}
	});
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	});
}

global.pushText = function  (body, contact, line, cid, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms

	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	if (cid) {mt.cid = cid; }
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.setUserParam = function(key, val, contact, line) {
	console.log (key, val, contact, line)
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).set(val);
}

global.updateUser = function(ob, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob );
}

global.logStartTicket = function(contact, chn) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).remove();
}

function setMsgSub (key, contact, val) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+key+"/sub").set(val);
}


function twoDigits(n) {
	if (n<10) {
		return "0"+n;
	} else {
		return ""+n;	
	}
}

function mainMenu(isNew) {
	if (isNew) {
		var str = "Hola! Gracias por contactar a Legalinea. Por favor marque una opción:\n\n";
	} else {
		var str = "Por favor nuevamente marque una opción:\n\n";	
	}
	str += "1. Hacer una consulta legal\n";
	str += "2. Averiguar sobre el estado de mis trámites\n";
	str += "3. Enviar un documento\n";
	str += "4. Hablar con mi abogado\n";
	str += "5. Otro\n";
	str += "6. Salir";
	return str;	
}

function serviceMenu() {
	var str = "Por favor escoja el área de servicio que más se ajuste a su necesidad:\n\n";
	str += "1. Familia o niñez\n";
	str += "2. Tributario o empresarial\n";
	str += "3. Propiedad intelectual\n";
	str += "4. Accidentes de tránsito\n";
	str += "5. Otro\n";
	str += "6. Salir";
	return str;	
}
