var request = require('request');
let env = require('./env.json');
var moment = require('moment');  
global.numberMapES = {
		'UN': '1',
		'UNO': '1',
		'DOS': '2',
		'TRES': '3',
		'TREZ': '3',
		'CUATRO': '4',
		'QUATRO': '4',
		'QUATTRO': '4',
		'CINCO': '5',
		'SINCO': '5',
		'ZINCO': '5',
		'SEIS': '6',
		'SIETE': '7',
		'CIETE': '7',
		'ZIETE': '7',
		'OCHO': '8',
		'OSHO': '8',
		'NUEVE': '9',
		'NUEFE': '9',
		'NUEBE': '9',
		'NUVE': '9',
		'NUBE': '9'
		}
global.letts = "ABCDEGKQUZ"
global.lettsMapES = {
		'AA': 'A',
		'AAA': 'A',
		'BE': 'B',
		'BEH': 'B',
		'VE': 'B',
		'VEH': 'B',
		'CE': 'C',
		'CEE': 'C',
		'CEH': 'C',
		'SE': 'C',
		'SEE': 'C',
		'SEH': 'C',
		'DE': 'D',
		'DEH': 'D',
		'DEE': 'D',
		'EE': 'E',
		'EH': 'E',
		'EEE': 'E',
		'GE': 'G',
		'GEH': 'G',
		'GEE': 'G',
		'JE': 'G',
		'JEH': 'G',
		'JEE': 'G',
		'KA': 'K',
		'KAA': 'K',
		'KAH': 'K',
		'CA': 'K',
		'CAA': 'K',
		'CAH': 'K',
		'PE': 'P',
		'PEH': 'P',
		'PEE': 'P',
		'CU': 'Q',
		'CUH': 'Q',
		'CUU': 'Q',
		'UU': 'U',
		'UN': 'U',
		'UM': 'U',
		'UH': 'U',
		'UHM': 'U',
		'UUU': 'U',
		'SETA': 'Z',
		'CETA': 'Z',
		'ZETA': 'Z',
		'A':'A',
		'B':'B',
		'C':'C',
		'D':'D',
		'E':'E',
		'G':'G',
		'K':'K',
		'P':'P',
		'Q':'Q',
		'U':'U',
		'Z':'Z'
		}
module.exports = {
	twoDigits: function(n) {
		if (n<10) {
			return "0"+n;
		} else {
			return ""+n;	
		}
	},

	removeTildes: function(str) {
		str = str.replace(/á/g,"a");
		str = str.replace(/é/g,"e");
		str = str.replace(/í/g,"i");
		str = str.replace(/ó/g,"o");
		str = str.replace(/ú/g,"u");
		str = str.replace(/ü/g,"u");
		str = str.replace(/ñ/g,"n");
		return str;
	},
	
	numberFormat: function(num) {
		
		if (num.length==9 && num.substring(0,1) == "9") {
			return {to: "593"+num, valid: true}
		}	
		if (num.length==10 && num.substring(0,2) == "09") {
			return {to: "593"+num.substring(1), valid: true}
		}
		if (num.length==11 && num.substring(0,2) == "51") { //Perú
			return {to: num, valid: true}
		}
		if (num.length==12 && num.substring(0,4) == "5939") {
			return {to: num, valid: true}
		}
		if (num.length==13 && num.substring(0,5) == "+5939") {
			return {to: num.substring(1), valid: true}
		}
		return {to: num, valid: false}
	},
	
	hash: function (str) {
		  var hash = 5381,
			  i    = str.length;

		  while(i) {
			hash = (hash * 33) ^ str.charCodeAt(--i);
		  }

		  return hash >>> 0;
	},
	
	isCedula: function(str) {
		if (str) {
			str = str.replace(/-/g,"");
			if (str.length==10) {
				var reg = /^\d+$/;
				if ( reg.test(str) ) { //check is only digits
					return true;
				} else {
					return false;	
				}
			} else {
				return false;	
			} 	
		} else {
			return false;	
		}
	},

	isRUC: function(str) {
		if (str) {
			str = str.replace(/-/g,"");
			if (str.length==13) {
				var reg = /^\d+$/;
				if ( reg.test(str) ) { //check is only digits
					return true;
				} else {
					return false;	
				}
			} else {
				return false;	
			} 	
		} else {
			return false;	
		}
	},
	
	isEmail: function(email) {
		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	},
		
	getRndMS: function (base) {
		if (!base) { base = 3500 }
		var n = Math.floor(Math.random() * 100) + base;
		return n;	
	},
	
	datoFastHook: function(cedula, cb) {
		var str = "";
		//do a datofast query for persona
		var url = "https://whatabotapi:dfst52112*5@datofast.com/api/personas/"+cedula;
		request(url, function (error, resp, body) {
			//console.log (body)
			if (body.indexOf("<!DOCTYPE")==0) {
				str = "Error al buscar registro de "+cedula;
			} else {	
				var ob = JSON.parse(body);
				if (ob.data) {
					var buffer = "";
					for (var p in ob.data) {
						if (ob.data.hasOwnProperty(p)) {
							buffer += "*"+p+":* "+ob.data[p]+"\n"
						}
					}
					str = buffer;
				} else {
					str = ob.error.message;
				}
			}
			if (cb) {
				cb (str, ob.data);
			}
		});	
	},
	
	datoFastHookEmpresa: function(ruc, cb) {
		var str = "";
		//do a datofast query for persona
		var url = "https://whatabotapi:dfst52112*5@datofast.com/api/empresas/"+ruc;
		request(url, function (error, resp, body) {
			if (body.indexOf("<!DOCTYPE")==0) {
				str = "Error al buscar registro de "+cedula;
			} else {	
				var ob = JSON.parse(body);
				if (ob.data) {
					var buffer = "";
					for (var p in ob.data) {
						if (ob.data.hasOwnProperty(p)) {
							buffer += "*"+p+":* "+ob.data[p]+"\n"
						}
					}
					str = buffer;
				} else {
					str = ob.error.message;
				}
			}
			if (cb) {
				cb (str, ob.data);
			}
		});	
	},
	
	datoFastHookVehiculo: function(placa, cb) {
		var str = "";
		//do a datofast query for persona
		var url = "https://whatabotapi:dfst52112*5@datofast.com/api/vehiculos/"+placa;
		request(url, function (error, resp, body) {
			if (body.indexOf("<!DOCTYPE")==0) {
				str = "Error al buscar registro de "+cedula;
			} else {	
				var ob = JSON.parse(body);
				if (ob.data) {
					var buffer = "";
					for (var p in ob.data) {
						if (ob.data.hasOwnProperty(p)) {
							buffer += "*"+p+":* "+ob.data[p]+"\n"
						}
					}
					str = buffer;
				} else {
					str = ob.error.message;
				}
			}
			if (cb) {
				cb (str, ob.data);
			}
		});	
	},
	
	isJsonString: function(str) {
		if (str.indexOf("[")==0 || str.indexOf("{")==0) {
			try {
				JSON.parse(str);
			} catch (e) {
				return false;
			}
			return true;
		} else {
			return false;	
		}
	},
	
	generatePin: function(){
		return Math.random().toString().substr(2,5);
	},
	
	generateALfaPin: function(){
		var nums = "123456789"
		function grn(min, max) { return Math.trunc((Math.random() * (max - min) + min)); }
		var a = grn(0, 9)
		var b = grn(0, 10)
		var c = grn(0, 9)
		var d = grn(0, 10)
		//var e = grn(0, 9)
		//return nums[a]+letts[b]+nums[c]+letts[d]+nums[e]
		return nums[a]+letts[b]+nums[c]+letts[d]
	},
	
	generateNumberPin: function(){
		var nums = "123456789"
		function grn(min, max) { return Math.trunc((Math.random() * (max - min) + min)); }
		var a = grn(0, 9)
		var b = grn(0, 9)
		var c = grn(0, 9)
		var d = grn(0, 9)
		
		return nums[a]+nums[b]+nums[c]+nums[d]
	},

	validarFecha: function (fecha) {
		const fechaSep = fecha.split('/')
		if (fechaSep[0].length != 4) {
			return false
		}
		else if (fechaSep[1].length != 2 || parseInt(fechaSep[1]) > 12 || parseInt(fechaSep[1]) < 0) {
			return false
		}
		else if (fechaSep[2].length != 2 || parseInt(fechaSep[2]) > 31 || parseInt(fechaSep[2]) < 0) {
			return false
		} else {
			return true
		}
	},
	
}
