var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "demo2";
var booted = false;
var jobs = []
var os = require('os');


//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+client+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var group = "main";
		if (user.step) {step = user.step};
		if (user.group) {group = user.group};
		
		console.log ("User "+job.contact+" is at step:", step, "group:", group);
		
		if (step != 'idle') {
				console.log ("Process MO ", job.msg.type);
				switch (job.msg.type) {
					case 'chat':
						var body = job.msg.body;
						if (body.length>0) {
							//trim spaces
							body = body.trim();

							//catch operations
							if (body.toLowerCase().indexOf("#") == 0) {
								var parts = body.toLowerCase().split(" ");
								//match direct operation
								switch ( parts[0] ) {
									case "#ping":
										var str = "#pong "+ new Date();
										pushText (str, job.contact, user.line);
										break;
									case "#salir":
										var str = "Estimado cliente, gracias por utilizar nuestros canales de comunicación....\n";
										str += "Hasta pronto";
										pushText (str, job.contact, user.line);
										resetUserParam("step", job.contact, user.line);
										break;
									case "#server":
										var str = "Server data:\n";
										str += "Datetime: "+ new Date() + "\n";
										str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
										str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
										pushText (str, job.contact, user.line);
										resetUserParam("step", job.contact, user.line);
										break
								} //end switch parts
							} else {
								//process step
								switch (step) {
									case "0":
										pushText ("Hola, bienvenido a Support Line, puedes contar con nosotros si tienes problemas de ansiedad, estrés... por favor ayúdame con tu nombre", job.contact, user.line);
										//open ticket
										setUserParam("step","1", job.contact, user.line);
										setUserParam("status","active", job.contact, user.line);
										break;
									case "1":
										pushText ("Gracias, en seguida te atenderá uno de nuestros especialistas", job.contact, user.line);
										//open ticket
										setUserParam("step","idle", job.contact, user.line);
										break;
								}//end switch step
								
							} //end if operation
						}
						//job is completed
						finishJob(job);
						break;
					default:
						//finish jobs
						finishJob(job);
						break;
				} //end switch mo type
		} else { //end idle check
			console.log ('user '+job.contact+'@'+user.line+' is idle');
			//job is completed
			finishJob(job);
		}
	});
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed'};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(500));
}

function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).set(val);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).remove();
}

function sendAd(contact, line, idx) {
	if (idx) {
		var i = parseInt(idx)-1;
	} else {
		var i = parseInt(Math.random() * (5 - 1) + 1);
		i = i-1;
	}
	if (i<0 || i>3) {i=0}
	console.log ("Sending demo ad", i)
	var ads = [
		{t: 'Banca Móvil', d: '	Nueva Banca Móvil, Tu Banco en tu celular, sin filas, sin perder tiempo', u: 'https://www.bancointernacional.com.ec/bancamovil/index.html'},
		{t: 'Tú eres más', d: 'Compra lo que quieras, cuando quieras', u: 'https://www.bancointernacional.com.ec/visamaster/index.html'},
		{t: 'Impuesto Predial', d: 'Sabemos que tu tiempo es valioso. Paga tu Impuesto Predial desde Banca Online', u: 'https://www.bancointernacional.com.ec/images/impuestosPrediales/mobile/index.html#p=1'},
		{t: 'Crédito Empresarial Banco Internacional', d: 'Conoce sobre nuestra oferta de Crédito para Empresas', u: 'https://youtu.be/FRsQTeunsG8'}
	]
	
	var b64 = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wgARCADcAoYDASIAAhEBAxEB/8QAGQABAQEBAQEAAAAAAAAAAAAAAAEDBAIF/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/2gAMAwEAAhADEAAAAtleD2ggAAAAUUSgAKRRFEogpFEURRFEURRFEURRFEUQAEURSxRFgABFEWAAQAAAAAAFFEoAFEogpFEUBRRFEUSgAAAAAAlEURRFEURYAJURRFEBFLFgBKCUQpBAABVACkoCpFAUUSgAKkURQAAAAAAAAABFEURRFEACpRFEWCVEUQEUsWAAEAoAKAqSgLUoAFJKAAAAABRFEVUURRFEVEURRFgAAAAAlEURYAsURYRURYRSwEoAKAqBRQAKgAB0c+oGaKRfNekJUFSHp59AKUkUsUkCgAAAARRFkAAFgAlCURYRSwEEAsAoCoApQAqAANseveWejpjkpx6gOXq59Y856adM5+b6R78+pp5e42HPYAAAAAAEURYAoCUSykWQAUQEUQKlgERR5C0IsoFUAJQCk6/Lrzz2w3A1nxfPvOuZ0c+NsOji1nVll0x9Ll34sa9uvh1NvHXwnvXfDNy9evOptl4xT6fNpy51383R887NOP2M9emub3MjXHx3pz68w99PzO9c+jh7c3n14td53eGNdEsxsIqKsIAgAWACPIWhFlAqgBKAD3tza7zN+fo1lZdZyudxueDGrxdvF0y9+N9Z5568az3cPdw433/N+lwJ711xXDz0+N41zesawuvNvPZ5y68b4fXvxvHb64+rj1y5ujPpi9fzu7Nz5fXveblvIy35feptz93LjWjP0dU8+uXSyxQhYoIgAWACPIWgWEoqpQBYSgAdfJ73noMN48Dl1AY7LMdPZPOPQJjuJzdSuX3uszaM6w96LJz9KOfoDOajnz7GpjdUuWHYMfftGful5nSuWekzrn89TU5emyUM0ABLAARQAPIlWChKlAqpQBYSpQDXIsqWUKAAqEoAAAAAAAAAACCyiBQAEIAAEAAVCAqKjylUCoKEqCilgoASoKABYKgooAACoSoKgqCoKgqAFAAAAIFQCACAAFlgEgAF8gqCgWCpQEqCilgqUAWEqCgAAqCoKgooAAAAiKgqCwAFgAJQmtZNYmbQZzUZNC5vfoxajGxnQEEALFVKALBUoCVBUoFVBUoACVBUFQUAAAAAAAhUFQWAACgNcVnTeVc9EwL0OYb++UasbL0+ecBnQEEAAALFVKAAVBUoCVBUFFAVBUFQVKAAAAAACFQVBUAABKERUFQWIVCgASwBH//aAAwDAQACAAMAAAAhyDDDPf8AcZDCMNryww8888884wwzz/8ALCQwzHX099cMMN/3GQzC8vP8xDDSwwwwwwxzjDCAQx/OM/qQyRDn8/8AxEYmb3MwkMbzzDHPPPPPPLDzzzzNMcwEfjPy0EB9hcibnYgMzjHPf++yywwwwyyy2++PPLTz3I4Eznw8hcm7FwM7DPc+6wBFb38xwx88sMMIw2+/HPTbXwI/CUi7Ggc7Df6sgM/07IfBBBBBBBBx18MQo2+CP/MUDz8HrcA9DT9q+y0hRNGMLs7ZJxQ0tD8WM+U+Maz/ACPA/F6/APQ0zIa2HwFLfA1CWzhNivL3O/ev11P7PvaPA9Ar1CNLw17e0PPM444QUgYw40Ucg3Mx0lvv41/ZNDELEy9CNLy83ykNPDQQQQQQQQQQQTUfCAHv4x+UGlMyUDEz8bENTw87jgMNPDTTTTTTfOMAADJO4z/yIJ8wvTULGtz8LCMJz089jjjkMMMIDjjnu48+yN/39AeLPM/7ScLCsz28DCMMTzzw888884xzz38cEHb8bJWTsAQw9/7ScNDGszz8cBDDGMMMMMMJDDDMMWTzw1wEALf/AP/aAAwDAQACAAMAAAAQwDHPzmMUpH7GYlJzzOOOOOObzzNNx8KXvB8kMzmLDHfjMQp/Wh5OydhN6iCGOOOCS2+NR1NSuTJ0Wr7ZYOzGwZqopaVFqWpV9MIQwyygAEM99BB/qSu1Sap8GZMcknkpqpuWRlIymPfzz1999zzznPP6wEchlGml+TyPkngacV2ZMwpP72DzsAMDCDOMc4gF53PGyycmlBlPMvkRbli5cgcUlQDXxJwf/wD/AM888vOczBm2e/O6DI07FZGrVZcG4L99I7aD/DWQ6KafuDicr07tZX/Fcp2Yk5YHpXZdCzP6pdRRULyiBmTmSFrVwskANZVUK3/taH55SaFgq3CrDCVD2FU2w2yz9135x4kFsa6T8JhVM43GebVploqVg5EuVjI8PPP/AP8A/wD/ADzDP7KkwbyckniPh+YvG1WElibFwOjfxM4yHPPPPPHw0IxZNlGYNtmteNwwMlioZWVh6Wp9gO7D/wD4QQQUf7w05qoiqug+VyK5sz0pGB4uFQqtQehvwdfMAiiigkNPaUfytqCwm+GIh8E9y0rGAexqFQ/gPSVPqggghgglvqgfQPKio1fp4ZP/AP/EADgRAAEEAAQCBQcNAQAAAAAAAAEAAgMRBBIhMRATFDBBgbEgIkBRcaHwBSMyM0JQUmFykcHR8TT/2gAIAQIBAT8A6qlSpUqVKlSpUqVKlSpUqVKlSpV6FSrhSpUqVKuNeRSpUqVKlSpVwpV1lcaVej1wr7qpV180LoiGu9vFrS40AsrrqllPq3QY4iwNEWOGpHoR6+CNroZHEaiqUkbXynMNmccGakPsPgnStjIkveh66HamGNpZETtqD3nxCJssc0+YKsX++ilLgx/MddnTW+3f9uuHHRaKh1gFmlBhZY7zxZu9AUyYZcu2iP1rv0KOEuiFRXY3v3qWCSIgPFLAxtkmDXCwo8JE+KqokmvesLC0tk5g1aP7UPR5I3P5f0fzUPIka+TJoBt+6Y+CaRrBHWvrVQOnMBj71DhIixzXb2QCsLhxzHskGwKwMLHZnyDQKTCXieU3QHwUkuGhdkazNXaSuUySAvY2iTQ9ylEGFphbmPajhonlksY80miFj8K1nzke2xWMia3JkG4WIwkbYCGjzm1aETOh8ytb42rV9bBKzXmk9xTC0xTFl1pvuj9a79CbLCI6t2au60XF25Xyb9eO9TPLMO1zdw4/yvNfE+Zv2m6+0LB/88vs/gr5PcWxSFu/+pksss7DIKo+pNnBxLosoB9Y3VOjwrx2h39KEtmHPG9EFSfM4Nre12vx7lE8O5cx7RXx3hYjDyRyEELDnl4a3djh/C+UInc3OBYKhBhhYx25IQlHSHwP2d4p8Fzxg/ZHgsNOJcS8HZ3x4J0Tm4MsrUFOY5v0hXHVa9dFM1kT2Hc0ppmxSHN2trjHK6N2Zhop00jm5CdN0yeRjSxp0KZM9jS1p0O6ixEkV5DVp2NndVu8Fzn5+ZeqdiJHtLXHQ6qOeSIEMNWpJnyVnN0hM8M5d6JuOnaKDk7ESOaWk6HVR4yaMZWu0Rnkc8SE6hOe97s53RxmIO7vcEx7mODm7hDHYgbO9wU2IkmrObrydFfCj1b5HPNuN+hHqbRXZ9waeTRVFX9w2rVpjC/ZGCQOqlyJfwlHDyfhKEMhOgQglP2Sjh5AMxHW35F+hMkLPzXTZbRxbyCF0t/x+f8Aqbi5Bddq6Q/U+v8AxOxkjrvt9ItX1dq1avyf/8QANxEAAQQABAMDCgQHAAAAAAAAAQACAxEEEiExEzBBECBAFCIzUWFxgbHR8EJScpEjMlChweHx/9oACAEDAQE/AOTatWrVq1atWrVq1atWrVq1atWrVq1fOtWrVq1atWrVq1atWrVq1atWrVq1atWrVq1avkX22rVq1avsvnX22rVq+y1f9ItXz4pRKCR2kgalZhV2swReAaJQcDse/avlDnzPc2VjQdDaY9zIxlO7u3FC2fEIRueMnvPq9yeHuDpAN9/2HyKAoOaR5xutP21UYBezIKrfTwOq1V8wmtVNiY31kkr4Im3xG731Q9G39afKGyG5eu1KOZkgthtYt7mRW06p+IkZJd6AC1iJXWzIdCfopeMx7WZ91LxmOazPv/pOZLGxzi+9Fcwh42f4KXEyBzXN2oGliJjw2uYdysXK5tMZuVHiaw/EO4TI55Rnc+rXEeyYNcbFaqPjYi3h2UdEJ5GZo3nUDQrBzud5j991hpHOz5jsVBiXmUFx803S4jvKcl6V3K500TtOEB8U8OEkQdvrsh6MfrTo5S+6Ff3QaBsFjvQlRNDpi0/lH+F5zJGxO6FYn00fvWMAdIwHZOjjjieGHcetOhIgEl37OisPxDfUWqUGI8I7XYTP4mJLujfv6qRpbniHvUMzHsBBUwzz0OrfqsFI3h5TuFKRLK5zdgEYzwWSt3CbLUTyOp+aniMcDSN2/fzTZAcSHesIOadj26LTnSxF0jHDpaiidIzTo6+18bZBlcNE2JjXZgNU6Fj3BzhqE6JjnBxGoUkLJP5xaGFhGwXCZkyVomwRtIIGyfEyQguGyjiZHeUboxML89ap2EhcbLUIWNIcBqE/DRPNuCELA3IBomta1uUbIYaEdE5rXDK7ZHCQn8PzUULIryCuRY5bWNYKaPBDlBdfBVyq5NrTwNeBpUqTnBqEzKu1xo/zBCeP1oysrUrjRj8QQnjvLfKrtpUq7KVKlSrv0qVdyk+MOXkkdUhhmAgryVn39+xHCsNX0XAZp7P+puEjbt0VeCrl13qVKlS69z//xABNEAABAwIBBgkHCgIJAwUAAAABAAIRAwQFBhASITFxEyAiMkFRgZGxMEJSYXKhwRQWNDU2VHOy0eEVkiMkM0BTk8Lw8SZigkNQY4Si/9oACAEBAAE/APJRxoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFCjyLdvYfDM/ndg8PKRnhQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFHFbt7D4Zn7eweCjyMKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUIsaNr9e5OaAAQZTdvYfDM/ndg8EBqkmOJHFhQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChPHKKI5I3pu3sPhmdt7B4LzDvzRxoUKFChQoUKFChQoUKFChQoUKFChQozQoUZoUKFChRmhQoUZoUKFChQoUKFChQoUKFChQoUKFChQoUJ45RRHJG9NGvsPgoT+d2DwXmHfxoUKFCAUKFChQoUKFChQoUKFChQoUKOPGaM0KM0KFChQo4kKFGaFChQoUKFChQoUKFChQoUKE8coojkjegNfYVCeNfYPBeYd/FhQoUKEAoUKFChQoUKFChRmjiRxI/ucKFGeM8cSFChQoEDaoEGJUKFChQoUJ45RRHJG9Aa+wqE8a+weCjkdvEhQozQoUKFChQoUKFCjyEcaFChQoUKFChQoUKFChQoUKFHFhRxoUZiNQUbVChBpOwErQd6J7loO9E9yLSNoIUJ45RRHJG9Aa+wqE/b2BeZ28SM4ChQoUKFCjNCjPHkYUKFCjyEKFHEhRmjNChRmhR5I9C61ChGRTbB6SpPWVJ6ymySdZ2FQnjlFHmjegNfYUQn7ewLze3iRmAQzAKPJ4hhlnQyfo3VOlo1nBhLtImZGvVMcWFCr3trauDa9xTpuIkBzoMKhc0LppdQrMqAbdB0wnXVuyu2g+tTbVdzWFwk9irXVvbua2tWp0y/mhzgJVavSt6fCVqjabJjScYCqVqVGkalWoxjB5zjAVDEbK6qcHQuaT3+iHayquJ2FGqaVS7oteNoLhq39SaQ5oc0gg6wR0qFChQoUKFChQoUKM0KFChQoUZo4oAgk9CAaTEnuzHYF0FQiApaWAGdXUoZ1u7lDOt3cuSJidmZw5RRHJG9Db2FEJ23sC8zt4wQQzDyPSsZwajhtClUp1HvL3QQ6OpYr9lbf2afgm4NRdgJv8AhH8Jol2jqjbGaFGfHH0qeUVk6vSNWkKfKYG6U7ejpWDGhXyhuK9q0W9BrI4E6i7/AMepXZrXNa6xqk6W0LhraZ9Q6fy96yhYMQr4W1h1VwdE74hXd+66yafQr6rm3qtZUB2nqKdTGL5Sm1uCTb21IOFMGATq/X3LF7e2wvDK1zaW9OjX0dBr2NgiSAVh2BWBwqiK1BlSpVphz3nbJE6j0LJipUpvvbBzi5lvUhhO8g+ChQoUKFChQoUcaFHkIzjmuQ5w3o7UdgQ6cx2ldHFdzijzRvQ29hzO29gXm9ucZhmHlBtCyq+hW3tnwWK/ZW39mn4Kn9jT+GfzcbF6dzTx6zvKVrVrspM18G2dev8AVXFHErq9rYkyxfR0aRpsZ5ziQRPZM9issK0cmjZObo1KtMl09Djsndq7la2l+52ENq2tVvyaq4OJGxsgg/76llFhFw+4+U2VNz+GAbWYwdI1g+7/AHKvLG9scUbidjS4bTYG1aXTsGzuCq1bjG7arZVcOrWzXtkVamwOGsdCtb/FbGzbZ1MKrVa1MaDKjdbSOiSsBw2rY0ata5j5TcO0nwdnq958vCjiR5Ac1yHOG9HajsCaSJIJB9S4R/pu70ajxq03d64R+3SPeuEf6bu9cI/03d6DnOkFxOo9OZ3OKPNG9Db2FFOGvsC83tzhDOPJtyVtHMDuHraxPR+ixnC6WGVqLKVR79MEnSjUsqvoVt7fwWK/ZW39mn4Kn9jT+GfzLCMDoYhZcPUq1Gu0y2GxCxHJ+3s7CrcMrVXOYBAdEbQOriY7jtfCrunSpUqbw5mkS+es+tYhjte0wuyumUqbn3DZcDMDVOpYHlCcSuH29wynTqRLNHzusa+nNbZT3NfEHW7qFENGnrEzyQT1+pfPG8+70Pf+q+eN793oe/8AVYZlNdXt6KD6NFrS1zpbM6mk9fqXzxvPu9D3/qvnje/d6Hv/AFT8oK7Mn6eIcDTNR9XQ0dcDb+iblZiVTXTtKLgNsMcfirbLGoKobd2rdGYJpkgjsKxHFPk2Dm/ttCoDolulMEErCsqX3d+y3uqdKm2pqa5s87o25rrKK4oY4bFtGkafCNZpGZgx6/XmxfKarYYg+2oUqbwwDSL527f0WCYmcUsOGe1rajXlrmt2J7202Oe9wa1okkmAAr7K9lOoWWVEVAP/AFKmoHcF87MT5xpUNH2DHimZQXLsBqYgaFMPbUDAJMHZrWA4rVxW3q1KtNjCx+iAyepYvlFQw1xo028NcdLZgN3lHKvE3TUbQohg/wCxxHfKw3KyncVRSvKbaLnahUaeT29SxnEKmG4ebikxrnaQbDtmtYTlO+9vm29zTp0w8QxzZ53VrzVMobhuO/IBRpcHwwp6WuYnfmxXKWrZYhUtqFKm9tOAXOnasHxE4nYCu5rWvDi1wbsB/wCIWN4jUwyzZWpMY9zqgbDtmw/osDxGpilhVrVWMY5tTQhs9QPxTecN6O1HYE2NcmAtFnpn+VQz0z3KGeke5Qz0z3KGeke5ckSQSTHVmdzijzRvQ29hRTtvYF5vbnCCGYIIcejT4avTpTGm4Nnqkr5o1Pvjf5P3XzSf97b/ACfusUwt2F1aTDVFTTEyBELKr6Fbe38Fiv2Wt/Zp+Cp/Y0/hn8yw3AnYja8OK4YNItgtlfNR/wB7b/J+6r5MPo0KlX5U06DS6NDbA358sfrGh+F8Ssb+zuE+x/pCNKth7bK+pEjhBptd1OBgj/fWsPvaeIWVO5p7HDWOo9IWCa8qKYP+I/wcrqjSFpW/o2f2bvNHUsmGh2OUg4AjRdqI9SrUqbbeqWsaDoHWB6lku1rsbphwBGg7UR6lwNL/AAmfyhZWtDcFYGgAcM3UB6isj/qut+MfALLGjQbTt6oDRXc4gwNbmx0+7vTOE+Y1TTmOF5E9WkPjKFq/+HC9ZMNqmm4jo1Aj4rAsTGJYe1zj/TU+TUHr6+1Yj9rz+Oz4KtVbQovqvMNY0uJ9QVlbvxjELp75c806lXt6PeQskLoU7+rbOOqs2W7x+0rK+/extKyYYDxpvjpEwB4rJvBKLbRl7c0w+rU1sa4SGt6DHWVAiIEdSympsp4BUaxjWt02mGiOlZP3ZscAxC5EaTHcmeuIHvWA2H8VxRzriX02f0lSfOPQE1jGMDGtDWgQGgQAsqsKp2tSnd0GBlOodF4GwO6O/X3KreOu8jYqOl9Gs2nPq6PH3IW1Rtgy9YSAKppkg80gAjxPcsFxIYnh7ahI4VnJqD19faq32w/+0PEKvVbb29Ss88ljS49is7apitzeVXy54pvqmOl3QskbrQuq1s46qjdJu8fsfcsrvqql+MPArJD6nr/jn8rUOcEdqPRuQ2HMdpR2ZjnfzijzRvQ29hRI9FP2rze3MMwQQzDyDXFrg5pIcDII6F/FL/77X/zCm4blA5oIu3wdf9uViltfW9WkL6qajiOSS/SgLKr6Fbe38Fiv2Wt/Zp+Cp/Y0/hn8yw2zxSva6dnXcylpEQKhbr3K5tcbtLd9erd1NBm2KxKdiN69pa67rFpEEF51jPlj9Y0PwviVjf2ewn2P9IVnhwxPI6hSAHCt03Uz6w53/CyXxA2eIOs6p0adYxB81/R37O5YMNHKtg6qlQe5yuvodf8ADd4LJb69pey7wVx9Gq+wfBYfZ1767FC3cG1CCQSYTMmcWa9pNanAM/2p/RZXfU7Pxm+BWF1MZZbP/hwqGlpcrRYDrj1oVDd4m0YtWrN16LyRrb6vUFlDSp0Mmn0qLQ2m3QDQOqQsmrVl7gl5bVdbH1I3GBBWGXNXA8bNOtyW6XB1R6uv4rEftfPXXpnwWVV38nwk0hzq7tHs2n9O1ZH2uhZ1rlzddR2i0+ofufcq2lg+UZIEClW0hHS06/ArKsk4zPmmk0tPq1rDyDhtqW7OBZHcM2VP1FU9tvirUO+al9o7BWZO7V+yyLI/rg6eR8c2VpH8GE9NVsdxVuHfNu8PmmvTA36/2WTtmy/yeu7apsfVMHqOiIKwi7qYLjJpV5awu4OqPA/76FXH/WUDXNy3xCysuTb4WKGx1d0dg1n4LJCzcywqXJZrrOhp6wP3lHTwbKIzyRSrf/g/sVleCMLpT/jDwKyQ+p6/45/K1DnDejtR2BDpzHaUHuaNRhcK/wBIo1H+kUHkyHGRClOMkwjze1DadxRTtvYvN7c4QQQ8oMosSAAFdsDVzB+iu8QuL97HXDw4t1CAAsqvoVt7fwWK/Za39mn4Kn9jT+GfzK0xa8sqPBUKgayZgtB1q4xq+uqDqNWqCx20aIHEyx+saH4XxKxv7PYT7H+kLJr7P2v/AJ/nKypw02t22/oAhlQ8ojzX9fasBqurZS0aro0nve4x1lpV19Dr/hu8Fkt9e0vZd4K4+jVfYPgsnK9K2xhlStUbTYGuGk4wNi/jGG/frf8AzAsp7mjdYG2pQqsqM4cDSYZEwVkf9V1vxj4BZY2zWXdC5aINRpa71kf8+5XFz8pyHa6ZczRYexwHhCyN+gXH4vwCyswzhKQv6TeUzk1PW3oPYrSq+vitq95l3CU2zugDwWVV2bjFhQa6W0W6Mf8AcdZ+HcsNtfkeHW9v0sYJ37T71lja6F3QuhsqN0Hbx+x9yvaLr/ArS+py51u3gKwG0AbD7/esnsoKNtbizvHFjWn+jqbRHUV/EbHR0vllvo9fCj9VlDc0bvJ2pVoVA+nwgGkPUVk5ai9wK/tpA4R0AnoMavesJvX4Lix4djg3XTqt6R6+xU8SsalEVW3dHQImS8CN/Uso8XZiVxToWxLqNM6j6bj0wr2wdh+R7adQRUfVa946iejuhZH/AFXW/GP5QsrMM0mNxCk3W3k1d3Qfh3LDar62O2b6hlxqsBPXEBZT3JusYNFh0m0gGAD0un9OxWNt8jsKFvIPBsAJHSen3rK624O+pXA2VWQd4/YhYndC7ySsXTLmPFN28B3whZL31pa4XWp17ilTeaxIa9wBiGoYth0j+u0P5wpB1jYjsCHTmO1AAjW6OxFrfTHctFvpjuQ0RJ0p1dWboR5vaht7CinbewLze3OEM4QQzDyA2hZVfQrb2z4LFfstb+zT8FT+xp/DP5uNi+ANxa4ZWNwaWizRgMnp3q9wBt5h9paG4LBbiNLQnS1R1rDbMYdYUrUP0wyeUREySfiry1p3tpUt6oljxG49BWHZMNw+/pXQuy8055PBxMgjr9aqs4Wi+nMaTS2eqVheTTcNvm3IujULQRolkbRvVRunTcyY0gRK+ZbPvzv8r918y2ffnf5X7r5tN/hHyD5UY4bhdPQ9URErCMLGE2z6IqmrpP0pLY6B+ixbC2YtatoufwZa7SDgJhU8nODwmth5uyWVHh4dwfNiPX6lg+EjCaFSkKxq6btKS2I1J7G1KbmPaHMcCCD0hUskadG7ZWbduhjw8NLOozEym5LM/iIu6t2ah4ThHN4OAdcxtzYthjMVtBQe80yHBzXATCwjBxhVKtSNfhqdQg6LmRHX0q9ySs7h5fb1HW5PmgaTe5DIuppcq9bHqp/uhk8wYM7Dm3L4c/TLy2YOro7Fg+EjCaNSmKxq6btKS2I1LE8CtMTOm8GnW/xGbTv60ci6mlqvW6PXwevxWG5N2eH1BVdNesNjnjUD1gLFcPGJ2RtjVNOXB2kBOxYRhYwm1fRFU1dJ+nJbHQB8FVpsrUn06jQ5jwWuB6QrXJSna31K4bducKbw8NLNsHZMqjkuxmINu6t06qRU4Qt0IkzPX1qWeie9YthlLFbVtFzjTc12kHxMIZMRYOtDeEsNQVAeD2GCOv8A3C+ZrIJ+XO1f/H+6GRzJH9dd/l/umt0WhvUIR6NyGw5jtXRxNaOi3VB70SNEQOlDb2HM7b2Lze3iBBBDygV5id1fMay4qBzWmRDQFiv2Vt/Zp+CGKXYsfkYqDgIjR0Rs27f/AGMMcRIae5aD/Qd3LQf6Du5aD/Rd3LQd6J7lou9E9yMg6xCHNcm84b0dqOwbkNhzHaV0Z5RITyNI6wiRo7elA6+wopzHkyGOIgdC4N+jzHberijiT5S4xmvc4eyyfTphjA0BwmdXHnNPEn+5zxH7RuHhxnbG7kOa5N5w3o7UdgQ2HMdq6OK/nFHm9qKKq88eyPALze3jTxp4s8aVPGnPPElSp8jKniznLmmJBmI2qWdTu9Szqd3qWdTu9S3qPepb1HvTjMR0BDmuQ5w3o7UdgQ2HMdpXQp4j50ijOj2rbme5hIlrpgbD6tymno81230v24855UqVKnPPEniSpUqc055zSpU+VlTnnNOac0HqKg9RUHqK2Ic1yHOG9HaUdgXQcx2ldGeVKeDpbEZDRvQOvsKJT9vYF5vbxpUqUFOaVKlSpUqVKlTnlTmlSpUqc0qc0qVOeVKlSpzypU55U5pzSmc9u9aTvSPetI9Z71pHrPenmQ3chzXIc4b0dpR2BA7VKO0oNLhtA3mEWEec3+YLQPpN/mCLDBMtO45tI9ZRMtGvpQOvsKlO29gUw3t8hKlSpUqVKlSpzSpUqVKlTxZUqVKlSpUqVKlTxpUqVKlSpzSpzSmmHAnoXI9J3d+65PpHuXJ6z3JxBiOgQhzHJvOG9E60TqG5TqK6UYk60dgUqU06zuKJTtRhE8kb03b2FSnnX2Bead/kJUqVKlSpUqVKlSpUqVKlSpUqeJKlSpUqVKlSpUqVKlSpUqVKlTxZUqVOaVKB5LkDyhvVK1taFi28vRUfwriKVJjo0gNpJV/hYpU/lNsSbbgmVeWRpDSJAHr2JuCXhqOYeDZBa2XPAGkRIbvhDAr4ta7QYC46ml4BOuD3Qjgd4XOLTRdTDdPhBUGjExtVXBbu3pVHPZSfwYBLQ+SQTqIA6E/BrxrRFCi92k1jmMqS5hOzS16kcOqm9baMFvUqOBJ0KkhsbZPRsVPCbis9zaRttGQwVBU5LnEbAekqvhN1b2za9UMa1xhrS4aTjMagquT9811TSNAcGQHzVHIB2E9QVDA6/wAtNK5DW0qVQCo/TABmCACdpKu8IrUDXruFGjSa5zWMNWS6NoHXCKcdfYF5h38ac0qc0qVKlSpUqVKlSpUqVKlSpUqVKniSpUqVKlSpUqVKlSpUqc0qVKlSpUqVKlSgeS5A8ob1QurC6tLe1vnVaRoudo1KYkFpMkFXeKWL6b7WppOotcw0zQMhzGjU0kwnZQ0K4qNBr0TwhcxzGMJLdQ6dh9YVLGGMv6VVwe6k214AklpcD0uAJg61eYkytaVqDataoarmS5+g0BrZ1AA6tqONUm3d1XbSceEdS4NpIgNYQSDr9XrRxO0Y+4+TvuGG6q8LUq8nSZBkACev3KpiFvUxkXjHVaAawDTptZpPd0kiY160cetjSqNptq2oFQvbwLGOJB380+sK8xOlc39lUioaFuym0hwGkYMuO3aVdYpRq07xgFUOubkVHmB/ZjYNu2VWx7D7l9KpWo3AFG44RrGRDxqgu17RCucYtatjdUWmvVdWeXsbVY0CkSZJBmVpu9I96fzuwLzDv8lKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqUDyHdiaeUN6J1lGNFsmNS1QYM6kDrCceUd6J5LUNcrV1pvOcPUczzyiieQN6bt7D4KU/ndg8F5h3+SnNKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpQPId2Jp5Q3onWU48lm74lMIGlIkRsWmyf7Md5Tns0j/AEfvKL2QOR71ptg8j3rTZ/h+9F4BIDdeyZUp/PKJ5A3pu3sPhmfzuweC8w7/AO/zxZU8afISpU5pU5pzznHMd2Jp5Q3o7Snc1m74lDmu3fFDaE7nHejzWobDuXSnc4781TnlHmjem7ew5nc7sHgvMO9f//4AAwD/2Q==";
	pushText (ads[i].t, contact, line, 'bot', 'img', ads[i].d, ads[i].u, b64)
											
}
