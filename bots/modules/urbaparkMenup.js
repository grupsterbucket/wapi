
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' urbaparkMenup', user.id, step, group)
    console.log(job)
    switch (step) {
      // Me llega el saludo y el menu principal
      case "0":
          updateUser({ step: '1', module: "main" }, job.contact);
          pushText(mainMenu(true), job.contact, user.line)
        break;

      case "0.1":
        updateUser({ step: '1', module: "main" }, job.contact);
        pushText(mainMenu(false), job.contact, user.line)
        break;

      // Me llega que opcion del menu principal selecciono y se redirije
      case "1":
        var kw = body.toLowerCase()
        var msg = ''
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          mods["urbaparkMenu1"].bot('Swtich Module', job, user, "0")
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          mods["urbaparkMenu2"].bot('Swtich Module', job, user, "0")
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          mods["urbaparkMenu3"].bot('Swtich Module', job, user, "0")
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          //Abrir ticket
          if (isShopOpen()) {
            msg = 'Gracias por permitirnos escucharte, en unos momentos uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
          mods["urbaparkMenu5"].bot('Swtich Module', job, user, "0")
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo igorMenup")
    console.log(job)

    cb({ st: "done" })
  }
}

function mainMenu(greet) {
  if (greet) {
    var msg = '¡Hola! Bienvenido al canal de atención de *Urbapark*. Para ayudarte, selecciona entre las siguientes opciones:\n\n'
  } else {
    var msg = 'Por favor, selecciona una de las opciones:\n\n'
  }
  msg += "1. Servicio de Valet Parking\n";
  msg += "2. Planes de parqueo  \n";
  msg += "3. Conoce nuestra red: Ubicaciones, Tarifas y Horarios\n";
  msg += "4. Cuéntanos tu experiencia\n";
  msg += "5. Asistencia Igor\n";
  return msg
}

