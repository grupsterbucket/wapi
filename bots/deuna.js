module.exports = {
  linkPagoDEUNA: async function (monto, internalTransactionReference, detalle) {

    var options = {
      method: 'post',
      headers: {
        "Content-Type": "application/json",
        "x-api-key": envBot[account].deuna.apiKeyDeuna,
        "x-api-secret": envBot[account].deuna.apiSecretDeuna
      },
      body: JSON.stringify({
        pointOfSale: "32802",
        qrType: "dynamic",
        amount: parseFloat(monto),
        detail: detalle,
        internalTransactionReference: internalTransactionReference,
        format: "2",
        "Accept-Charset": "utf-8"
      })
    }
    //amount: parseFloat(monto),
    console.log('OPTIONS DEUNA', options.body)

    const respuesta = await fetch('https://deuna-prod.apigee.net/merchant/api/v1/payment/request', options)

    const respuestaFinal = await respuesta.text()

    if (isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: 502 }
    }
  }
}

function isJsonString(str) {
  if (str.indexOf("[") == 0 || str.indexOf("{") == 0) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  } else {
    return false;
  }
}
