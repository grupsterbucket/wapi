var request = require('request');
global.tools = require('./tools');
var path = require('path');
global.qr = require('./qr');
global.aws = require('../aws');
global.dataUriToBuffer = require('data-uri-to-buffer');
var fs = require('fs');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
var plugin = require('./cuponfiPlugin');
global.geo = require('./geoPlugin');
global._ = require('underscore');
global.firebase = require('firebase');
var CryptoJS = require("crypto-js");
var account = "cuponfi";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 250;
global.config = {}
global.calendars = []

//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		loadAccountConfig();
		initAWS(function() {
			listenToJobs();
			processJobQueue();
			geo.init();
			//~ setTimeout(function() {
				//~ geo.findCupons(-0.1653949, -78.4833768, 0.75)
			//~ },3000);
		});
	}	
}

function initAWS(cb) {
	firebase.database().ref('/accounts/demo/config/creds/aws').once('value', function(creds) {
		if (creds.val()) {
			aws.init(creds.val().accessKeyId, creds.val().secretAccessKey);
			cb();
		} else {
			console.log ("AWS credentials not found");	
		}
	});	
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/'+account+'/config/misc').on('value', function(snapshot) {		  
		config = snapshot.val();
		console.log ("Config fetched for account",account);
	});
	firebase.database().ref('accounts/'+account+'/config/calendars').on('value', function(snapshot) {		  
		calendars = _.map(snapshot.val(), function(ob, key){
			ob.id = key;
			return ob;
		});
		console.log ("Calendars fetched for account",account);
	});
}
function listenToJobs() {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued", job.id);
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	}
}

function processJob(job) {
	switch (job.type) {
		
		case 'reset-bot':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				resetBot (job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;

			
		default:

			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				var step = "0";
				var lastStep = "0";
				var group = "main";
				if (user.step) {step = user.step};
				if (user.lastStep) {lastStep = user.lastStep};
				if (user.group) {group = user.group};
				
				if (user.tsso) {
					console.log ("User "+job.contact+" is at step:", step, "group:", group);
				} else {
					startSession (job.contact, user);	
					console.log ("New Session for "+job.contact+"");
				}
				
				if (step != 'idle') {
					
						console.log ("Process MO ", job.msg.type);
						
						var msgType = job.msg.type
						if (msgType == "chat") { msgType = "txt"}
						if (msgType == "img") { msgType = "image"}
						if (msgType == "loc") { msgType = "location"}
						if (msgType == "") { msgType = "txt"}
						switch (msgType) {
							case 'txt':
								if (job.msg.m) {
									var body = job.msg.m;
								} else {
									var body = job.msg.body;
								}
								console.log ("Body", body);
								if (body.length>0) {
									//trim spaces
									body = body.trim();

									//catch operations
									if (body.toLowerCase().indexOf("#") == 0) {
										var parts = body.toLowerCase().split(" ");
										//match direct operation
										switch ( parts[0] ) {
											case "#salir":
												salir(job, user);
												break;
											case "#ping":
												var str = "#pong "+ new Date();
												pushText (str, job.contact, user.line);
												break;
											case "#server":
												var str = "Server data:\n";
												str += "Datetime: "+ new Date()+"\n";
												str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
												str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
												pushText (str, job.contact, user.line);
												break;
											default:
												var str = "Operación desconocida";
												pushText (str, job.contact, user.line);
										} //end switch parts operation
									} else {
										if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
											salir(job, user);
										} else if(body.indexOf("#ts")>0) {
											console.log (new Date(), "Running TS");
											var str = "#tsback "+ new Date();
											pushText (str, job.contact, user.line);
										} else if (body.toLowerCase() == "volver" || body.toLowerCase() == "*volver*") {
											setUserParam("step", lastStep, job.contact);
											step = lastStep;
											plugin.bot(body.toLowerCase(), job, user, step, group, account);
										} else {
											//save step as last step to implement volver
											setUserParam("lastStep", step, job.contact);
											//bot is enabled?
											if (config.botEnabled) {
												//chat is not operation, run bot
												plugin.bot(body.toLowerCase(), job, user, step, group, account);
											} else {
												if (config.autoMessage) {
													pushText (config.autoMessage, job.contact, user.line);
												} else {
													pushText ("Servicio en mantenimiento", job.contact, user.line);
												}
												//job is completed
												finishJob(job);
											}
											
										}
									} //end if operation
								} //body is empty
								//job is completed
								finishJob(job);
								break;
							case 'image':
								console.log (job.msg)
								if (job.msg.url) {
									//check if user is a contact before scanning code
									getContact(job.contact, function(contact) {
										if (contact) {
											qr.scanImage(job.msg.url, function(str) {
												plugin.bot(str + ":" + job.msg.m, job, user, "r1", group, account, contact);	
											})	
										} else {
											pushText ("La lectura de cupones está disponible para contactos registrados.", job.contact, user.line);
										}	
									});	
								}
								//job is completed
								finishJob(job);
								break;
							case 'location':
								plugin.bot(job.msg.lat+"@"+job.msg.lng, job, user, "c1", group, account);
								//job is completed
								finishJob(job);
								break;
							default:
								//job is completed
								finishJob(job);
								break;
						} //end switch mo type
				} else { //end idle check
					console.log ('user '+job.contact+'@'+user.line+' is idle');
					//is a reset operation?
					if (job.msg.body) { 
						var body = job.msg.body;
						if (body.length>0) {
							//trim spaces
							body = body.trim();
							if (body.toLowerCase().indexOf("#") == 0) {
								if (body.toLowerCase() == "#reset") {
									resetBot(job.contact, user, "Reset Session OK");
								}
							}
						}
					}
					//job is completed
					finishJob(job);
				}
			});

		}//end switch job type
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	});
}

global.pageBreak = function() {
	return '\n\n--------------------------oooo--------------------------\n\n'
	}

global.pushText = function  (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}


global.setUserParam = function(key, val, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).set(val);
}

global.updateUser = function(ob, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob );
}

function startSession (contact, user) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/tsso").set( Date.now() );
	logStartSession(contact, user.chn);
}

global.logStartSession = function(contact, chn) {
	var log = {
		type: "session-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logEndSession = function(contact, chn, tsso, op) {
	var log = {
		type: "session-end",
		tssx: Date.now(),
		ref: contact+"",
		ts: Date.now(),
		op: op
	}
	if (tsso) { log.tsso = tsso; }
	if (chn) { log.chn = chn; }
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logStartTicket = function(contact, chn, user, group) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn,
		group: group
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
	//send notification to agents
	if (user.condos && group == "admin") {
		var condo = _.findWhere(user.condos, {id: user.group});
		if (condo) {
			if (condo.config) {
				if (condo.config.notif) {
					var sendTos = condo.config.notif.split(",");
					for (var i=0; i<sendTos.length; i++) {
						var to = sendTos[i].trim();
						if (to.length == 10 || to.length == 12) {
							if (to.length==10) { to = to.replace("09","5939") }
							pushTemplate(["https://v3.textcenter.net/#inbox?c="+contact], "new_ticket_created", to, "593997520494");	
						}
					}
				}
			}
		}
	} else {
		if ( config.notifyTo[group] ) {
			var sendTos = config.notifyTo[group].split(",");	
			for (var i=0; i<sendTos.length; i++) {
				var to = sendTos[i].trim();
				if (to.length == 10 || to.length == 12) {
					if (to.length==10) { to = to.replace("09","5939") }
					pushTemplate(["https://v3.textcenter.net/#inbox?c="+contact], "new_ticket_created", to, "593997520494");
				}
			}
		}
	}
}

global.pushTemplate = function  (params, templateName, contact, line) {
	
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:"Nuevo ticket en Textcenter", rep: "bot", t:"mt", ts: Date.now(), type: "txt", st:"qed", lu: Date.now()};
	mt.isTemplate = true	
	mt.lang = "es"; //todo read from DB
	mt.params = params.join("|");
	mt.templateName = templateName;
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "Template Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.resetUserParam = function(key, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).remove();
}

global.saveForm = function(ob, cb) {
	firebase.database().ref("/accounts/"+account+"/forms").push(ob).then(function(res){
		cb(res.key);
	});
}

global.salir = function(job, user) {
	var self = this;
	logEndSession(job.contact, user.chn, user.tsso, "s");	
	var str = "Gracias por usar este servicio. Adios!"
	pushText (str, job.contact, user.line);
	resetUserParam('step', job.contact);
	resetUserParam('lastStep', job.contact);
	resetUserParam('pin', job.contact);
	resetUserParam('group', job.contact);
	resetUserParam('account', job.contact);
	resetUserParam('cupones', job.contact);
	resetUserParam('page', job.contact);
	resetUserParam('tsso', job.contact);
	setUserParam('status', 'reset', job.contact);
	
}

global.resetBot = function(contact, user, msg) {
	logEndSession(contact, user.chn, user.tsso, "r");
	
	if (msg) { pushText (msg, contact, user.line); }
	resetUserParam('step', contact);
	resetUserParam('lastStep', contact);
	resetUserParam('pin', contact);
	resetUserParam('group', contact);
	resetUserParam('account', contact);
	resetUserParam('cupones', contact);
	resetUserParam('page', contact);
	resetUserParam('tsso', contact);
	setUserParam('status', 'reset', contact);
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);	
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()	
}

global.getContact = function(phone, cb) {
	console.log ("Searching for",phone);
	firebase.database().ref("/accounts/"+account+"/contacts").orderByChild("phone").equalTo(phone+'').once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			console.log ("Contact found", arr.length);
			cb (arr[0])
		} else {
			cb (null);
		}
	}).catch(function(error) {
		console.log (error)
		cb (null);
	});
}

global.getCuprint = function(cid, cb) {
	console.log ("Searching for cuprint", cid);
	firebase.database().ref("/accounts/"+account+"/cuprints/"+cid).once("value", function(res) {
		if (res.val()) {
			cb (res.val());
		} else {
			cb (null);
		}
	}).catch(function(error) {
		console.log (error)
		cb (null);
	});
}

global.updateCuprint = function(cid, ob, cb) {
	console.log ("Updating cuprint", cid);
	firebase.database().ref("/accounts/"+account+"/cuprints/"+cid).update(ob);
}

global.getCupon = function(cid, cb) {
	console.log ("Searching for cupon", cid);
	firebase.database().ref("/accounts/"+account+"/cupones/"+cid).once("value", function(res) {
		if (res.val()) {
			cb (res.val());
		} else {
			cb (null);
		}
	}).catch(function(error) {
		console.log (error)
		cb (null);
	});
}

global.getUser = function(phone, cb) {
	console.log ("Searching user",phone);
	firebase.database().ref("users").orderByChild("phone").equalTo(phone+"").once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			console.log ("User found", arr.length);
			var ob = arr[0];
			if (ob.group) {
				firebase.database().ref("/accounts/"+account+"/props/"+ob.group).once("value", function(res) {
					if (res.val()) {
						ob.condo = res.val();
						cb (ob)
					} else {
						console.log ("User found but prop not found");
						cb (null);	
					}
				});
			} else {
				console.log ("User found but has no groups");
				cb (null);	
			}
		} else {
			console.log ("User not found");
			cb (null);
		}
	}).catch(function(error) {
		console.log ("User not found. Error:");
		console.log (error)
		cb (null);
	});
}

global.isShopOpen = function() {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (t>=config.officeOpen && t<config.officeClose) {
		return true;
	} else {
		return false;		
	}	
}
