// Habitat Bot as module
var qs = require('querystring');
var mailer = require('./mailer');
var fs = require('fs');

module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing legalinea sub bot", step, group)
		switch (step) {			
			case "0":
				var kw = body.toLowerCase()
				getContact(job.contact.msisdn, function(contact) {
					if (contact) {
						var ob = {step:"1", account: contact.id, name: contact.name}
						updateUser(ob, job.contact.msisdn, user.line);
						pushText (legalineaMainMenu(true, contact.name), job.contact.msisdn, user.line);		
					} else {
						var ob = {step:"1"}
						updateUser(ob, job.contact.msisdn, user.line);
						pushText (legalineaMainMenu(true), job.contact.msisdn, user.line);		
					}
				});
				break;
				
			case "1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var ob = {step:"2"}
					updateUser(ob, job.contact.msisdn, user.line);
					pushText (legalineaProductsMenu(), job.contact.msisdn, user.line);
					
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "Ya tienes una firma electrónica? Puedes usar el siguiente enlace para aprender como firmar un documento:\n\n";
					msg += "https://legalinea.com/como_firmar_electronicamente_documento.html\n\n";
					var ob = {step:"1"}
					updateUser(ob, job.contact.msisdn, user.line);
					pushText (msg + legalineaMainMenu(false), job.contact.msisdn, user.line);
					
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = "Nuestro *Centro de Ayuda* en este enlace https://legalinea.com/ayuda tiene información completa que puede ayudarte a resolver tus inquietudes\n\n";
					msg += "Pudiste resolver tus inquietudes?\n";
					msg += "1. Si pude\n";
					msg += "2. No, necesito contactar un asesor\n";
					msg += "3. Salir\n";
					var ob = {step:"1a", group:"sac", tso: Date.now()}
					updateUser(ob, job.contact.msisdn, user.line);
					pushText (msg, job.contact.msisdn, user.line);
					
				} else if (["4","4.","cuatro"].indexOf(kw)>=0) {
					salir(job, user);
				} else {
					var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión\n\n";
					msg += "1. Obtener una firma electrónica\n";
					msg += "2. Firmar electrónicamente un documento\n";
					msg += "3. Ayuda\n";
					msg += "4. Salir\n";
					pushText (msg, job.contact.msisdn, user.line);
				}
				
				break;
			
			case "1a":
				var kw = body.toLowerCase()
				if (["1","1.","uno","si","sip","aja","ok","si pude"].indexOf(kw)>=0) {
					salir(job, user);					
				} else if (["2","2.","dos","no","nop","no pude"].indexOf(kw)>=0) {
					if (isShopOpen()) {
						var msg = "Estamos direccionado tu solicitud, un momento por favor";
						var ob = {step:"idle", group:"sac", status: "active", tso: Date.now()}
						updateUser(ob, job.contact.msisdn, user.line);
						logStartTicket(job.contact.msisdn, user.chn);
						pushText (msg, job.contact.msisdn, user.line);
					} else {
						var msg = config.horarioStr;
						updateUser({step: "1"}, job.contact.msisdn, user.line);
						pushText (msg + "\n\n" + legalineaMainMenu(true), job.contact.msisdn, user.line);
					}
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					salir(job, user);
				} else {
					salir(job, user);
				}
				break;
			case "2":
				var kw = Number(body.toLowerCase().trim());
				if (isNaN(kw)) {
					var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión\n\n";
					msg += legalineaProductsMenu();
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					if (kw == rubros.length+1) {//regresar	
						var ob = {step:"1"}
						updateUser(ob, job.contact.msisdn, user.line);
						pushText (legalineaMainMenu(false), job.contact.msisdn, user.line);
					} else {
						var rubro = rubros[kw-1];
						if (rubro) {
							var group = ""
							//to do este menu tamb puede venir de config
							if (rubro.code == "f1" || rubro.code == "f1-2") {
								var msg = "Estos son los requerimientos para obtener tu firma digital:\n\n";
								msg += "- Una foto tuya sosteniendo tu cédula a la altura del mentón\n";
								msg += "- Foto de la cédula (frente y reverso)\n";
								//msg += "- Video solicitando la emisión del certificado\n";
								msg += "- Pago de *$"+rubro.value+"* (incluye IVA) con tarjeta o transferencia\n";
								msg += "- Validez *"+rubro.validez+"*\n";
								//~ if(isShopOpen()){
									//~ msg += "Nuestro horario de trabajo es de 09:00 a 18:00, fuera de este horario se agregará una tarifa express de 15 + IVA por procesar su firma.\n";
								//~ }
						
								msg += "\nDeseas comenzar el proceso?\n";
								msg += "1. Si\n";
								msg += "2. No\n";
								var ob = {step:"3", 'x-prod': rubro.code}
								updateUser(ob, job.contact.msisdn, user.line);
								pushText (msg, job.contact.msisdn, user.line);
								
							} else if (rubro.code == "f2" || rubro.code == "f2-2") {
								var msg = "Estos son los requerimientos para obtener tu firma digital:\n\n";
								msg += "- Una foto tuya sosteniendo tu cédula a la altura del mentón\n";
								msg += "- Foto de la cédula (frente y reverso)\n";
								//msg += "- Video solicitando la emisión del certificado\n";
								msg += "- Copia de tu RUC (PDF)\n";
								msg += "- Pago de *$"+rubro.value+"* (incluye IVA) con tarjeta o transferencia\n";
								msg += "- Validez *"+rubro.validez+"*\n";
								//~ if(isShopOpen()){
									//~ msg += "Nuestro horario de trabajo es de 09:00 a 18:00, fuera de este horario se agregará una tarifa express de 15 + IVA por procesar su firma.\n";
								//~ }
								
								msg += "\nDeseas comenzar el proceso?\n";
								msg += "1. Si\n";
								msg += "2. No\n";
								var ob = {step:"3", 'x-prod': rubro.code}
								updateUser(ob, job.contact.msisdn, user.line);
								pushText (msg, job.contact.msisdn, user.line);
								
							} else if (rubro.code == "f3" || rubro.code == "f3-2") {
								var msg = "Estos son los requerimientos para obtener tu firma digital:\n\n";
								msg += "- Una foto tuya sosteniendo tu cédula a la altura del mentón\n";
								msg += "- Foto de la cédula (frente y reverso)\n";
								//msg += "- Video solicitando la emisión del certificado\n";
								msg += "- Copia del RUC de la empresa (PDF)\n";
								msg += "- Copia del nombramiento de representante legal (PDF)\n";
								msg += "- Copia del documento de constitución de la empresa (PDF)\n";
								msg += "- Pago de *$"+rubro.value+"* (incluye IVA) con tarjeta o transferencia\n";
								msg += "- Validez *"+rubro.validez+"*\n";
								if(isShopOpen()){
									msg += "Nuestro horario de trabajo es de 09:00 a 18:00, fuera de este horario se agregará una tarifa express de 15 + IVA por procesar su firma.\n";
								}

								msg += "\nDeseas comenzar el proceso?\n";
								msg += "1. Si\n";
								msg += "2. No\n";
								var ob = {step:"3", 'x-prod': rubro.code}
								updateUser(ob, job.contact.msisdn, user.line);
								pushText (msg, job.contact.msisdn, user.line);
								
							}
						} else {
							var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
							pushText (msg, job.contact.msisdn, user.line);
						}	
					}
				}
				break;
			case "3":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","si","ok","bueno"].indexOf(kw)>=0) {
					var ob = {step:"fpre"}
					updateUser(ob, job.contact.msisdn, user.line);
					this.firmaBot(body, job, user, "fpre", group, account);
					
				} else if (["2","2.","dos","no","salir"].indexOf(kw)>=0) {
					salir(job, user);
				} else {
					var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
					pushText (msg, job.contact.msisdn, user.line);
				}
				
				break;
				
			default:
				if ( step.indexOf("f") == 0 ) {
					this.firmaBot(body, job, user, step, group, account);
				}
			
		}
	},
	
	firmaBot: function(body, job, user, step, group, account) {
		console.log ("Processing Firma sub bot", step, group)
		switch (step) {
			case "fpre":
				var msg = "Muy bien, lee detenidamente estas instrucciones, comencemos el proceso\n\n";
				msg += "- Al continuar con este proceso aceptas los términos y condiciones del servicio, puedes revisar los detalles en este enlace: https://legalinea.com/tyc.html\n";
				msg += "- Esta línea *"+formatNumber(job.contact.msisdn)+"* será asociada a tu identidad, no podrás solicitar firmas a nombre de terceros por esta línea\n";
				msg += "- Ten lista tu tarjeta de crédito para poder realizar el pagos, aceptamos Visa o Mastercard. También aceptamos pagos por transferencia bancaria.\n";
				msg += "- Si tienes un código promocional puedes ingresarlo al momento de hacer el pago\n";
				msg += "- Es *importante* que respondas todas las preguntas *UNA A LA VEZ* y correctamente\n"
				msg += "- No te preocupes si ingresas mal un dato tendrás oportunidad de corregirlo\n\n"
				msg += "Cuál es tu nombre completo (nombres y apellidos)?";
				setUserParam("step","f0", job.contact.msisdn, user.line);					
				pushText (msg, job.contact.msisdn, user.line); 
				break;
			
			case "f0":
				var kw = body.toUpperCase();
				if (kw.length > 0) {
					updateUser({ step:"f1", "x-full-name": kw}, job.contact.msisdn, user.line);
					pushText ("Cuál es tu número de cédula?", job.contact.msisdn, user.line);
					
				} else {
					pushText ("Por favor asegúrate de escribir tu nombre correctamente, asegúrate de ingresar tus *2 nombres* y *2 apellidos*", job.contact.msisdn, user.line);
				}
				break;
			case "f1":
				var kw = body.toLowerCase();
				kw = kw.replace(/-/g,"");
				//is cedula valid?
				if (tools.isCedula(kw)) {
					console.log ("kw has 10 digs");
					//call datofast and get data
					tools.datoFastHook(kw, function(str, ob) {

						if (ob) {
							//save in user
							let attempt = 1;
							if(user["x-email-attempt"]){
								attempt = user["x-email-attempt"];
							}
							updateUser({ persona: ob, step:"f1.1", "x-dni": kw, "x-email-attempt": attempt }, job.contact.msisdn, user.line);
							pushText ("Cuál es tu correo electrónico?", job.contact.msisdn, user.line);
							
						} else {
							console.log (str)
							pushText ("Por favor revisa y envia tu cédula correctamente", job.contact.msisdn, user.line);
						}
					});
				} else {
					pushText ("Por favor revisa y envia tu cédula nuevamente", job.contact.msisdn, user.line);
				}
				break;
				
			case "f1.1" : 
				var kw = body.toLowerCase();
				//is correo valid?
				if (tools.isEmail(kw)) {
					//save in user
					let pin = tools.generatePin();
					updateUser({ step:"f1.1.1", "x-email": kw ,"x-email-pin": pin }, job.contact.msisdn, user.line);
					var details = {
						"img": "https://storage.googleapis.com/pointalk/mail-logo-1590088504037.png",
						"title": "Hola! Tu código de verificacón Legalinea es: " + pin
					  }
			
					mailer.sendTemplateEmail(kw,"Verifica tu correo electrónico", "blank", details);
					var msg = "Para verificar tu correo electrónico se te ha enviado un código de 5 dígitos, por favor responde con el código a continuación.\n";
					msg += "Si deseas cambiar el correo electrónico responde *cambiar correo*\n";
					msg += "Si no te llegó el correo responde *reenviar*."
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor revisa y envia tu correo electrónico correctamente", job.contact.msisdn, user.line);
				}
				break;
			case "f1.1.1" : 
				var kw = body.toLowerCase();
				let pin = user['x-email-pin'];

				if(["reenviar"].indexOf(kw)>=0){
					var details = {
						"img": "https://storage.googleapis.com/pointalk/mail-logo-1590088504037.png",
						"title": "Hola! Tu código de verificacón Legalinea es: " + pin
					  }
			
					mailer.sendTemplateEmail(user["x-email"],"Verifica tu correo electrónico", "blank", details);
					var msg = "Para verificar tu correo electrónico se te ha enviado un código de 5 dígitos, por favor responde con el código a continuación.\n";
					msg += "Si deseas cambiar el correo electrónico responde *cambiar correo*\n";
					msg += "Si no te llegó el correo responde *reenviar*."
					pushText (msg, job.contact.msisdn, user.line);
				}
				else if (pin == kw) {
					//save in user
					console.log("Prod", user['x-prod']);
					if (user['x-prod'] == 'f1' || user['x-prod'] == 'f1-2'){
						pushText ("Cuál es tu dirección de domicilio?", job.contact.msisdn, user.line);
						updateUser({ step:"f1.1.2", "x-email-valid": true }, job.contact.msisdn, user.line);
					}
					else if(user['x-prod'] == 'f2' || user['x-prod'] == 'f2-2'){
						pushText ("Cuál es tu ruc?", job.contact.msisdn, user.line);
						updateUser({ step:"f1.1.2-ruc", "x-email-valid": true }, job.contact.msisdn, user.line);
					}else if(user['x-prod'] == 'f3' || user['x-prod'] == 'f3-2'){
						pushText ("Cuál es el nombre de la empresa?", job.contact.msisdn, user.line);
						updateUser({ step:"f1.1.2-empresa", "x-email-valid": true }, job.contact.msisdn, user.line);
					}
					
				} else {
					let emailAttempt = user['x-email-attempt'] + 1;
					//4 intentos para validar
					if(emailAttempt <= 4){
						if(kw == "cambiar correo"){
							updateUser({ step:"f1.1", "x-email-attempt": emailAttempt}, job.contact.msisdn, user.line);
							pushText ("Cuál es tu correo electrónico?", job.contact.msisdn, user.line);
						}else{
							updateUser({ "x-email-attempt": emailAttempt}, job.contact.msisdn, user.line);
							pushText ("El código no coincide, por favor revisa tu correo electrónico e ingresa el código nuevamente.", job.contact.msisdn, user.line);
						}
					} else {
						pushText ("Has superado los intentos de validación", job.contact.msisdn, user.line);
						salir(job, user);
					}
				}
				break;

			case "f1.1.2-empresa":
				var kw = body.toUpperCase();
				if (kw.length > 0) {
					updateUser({ step:"f1.1.2-cargo", "x-empresa": kw}, job.contact.msisdn, user.line);
					pushText ("Cuál es tu cargo en la empresa?", job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor envía el nombre de la empresa.", job.contact.msisdn, user.line);
				}
				break;
			case "f1.1.2-cargo":
				var kw = body.toUpperCase();
				if (kw.length > 0) {
					updateUser({ step:"f1.1.2-ruc", "x-cargo": kw}, job.contact.msisdn, user.line);
					pushText ("Cuál es el ruc de la empresa?", job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor envía el nombre de tu cargo en la empresa.", job.contact.msisdn, user.line);
				}
				break;
			case "f1.1.2-ruc":
				var kw = body.toUpperCase();
				if (tools.isRUC(kw)) {
					if(user['x-prod'] == 'f3' || user['x-prod'] == 'f3-2'){
						pushText ("Cuál es la dirección de la empresa?", job.contact.msisdn, user.line);
					}else{
						pushText ("Cuál es la dirección de tu domicilio?", job.contact.msisdn, user.line);
					}
					updateUser({ step:"f1.1.2", "x-ruc": kw}, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa el ruc correctamente.", job.contact.msisdn, user.line);
				}
				break;
			case "f1.1.2" : 
				var kw = body.toUpperCase();
				//save in user
				if (kw.length > 0) {
					if(user['x-prod'] == 'f3'|| user['x-prod'] == 'f3-2'){
						pushText ("En qué provincia se encuentra la empresa?", job.contact.msisdn, user.line);
					}else{
						pushText ("Cuál es tu provincia de residencia?", job.contact.msisdn, user.line);
					}
					updateUser({ step:"f1.1.3", "x-dir": kw }, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la dirección correctamente.", job.contact.msisdn, user.line);
				}
				
				break;
			case "f1.1.3" : 
				var kw = body.toUpperCase();
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.2", "x-province": kw }, job.contact.msisdn, user.line);
				
					if(user['x-prod'] == 'f3'|| user['x-prod'] == 'f3-2'){
						pushText ("En qué ciudad se encuentra la empresa?", job.contact.msisdn, user.line);
					}else{
						pushText ("Cuál es tu ciudad de residencia?", job.contact.msisdn, user.line);
					}
				} else {
					pushText ("Por favor ingresa la provincia correctamente.", job.contact.msisdn, user.line);
				}
				
				break;
			case "f1.2" : 
				var kw = body.toUpperCase();
				//save in user
				updateUser({ step:"f1.3", "x-city": kw }, job.contact.msisdn, user.line);
				pushText ("Cuál es tu teléfono fijo incluido el código de provincia (si no tienes uno responde _no tengo_)?", job.contact.msisdn, user.line);
				break;
				
			case "f1.3" : 
				var kw = body.toLowerCase();
				//save in user
				
				var msg = "Revisa la información a continuación y digita una opción:\n\n";
				console.log("Producto", user['x-prod']);
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-fijo": kw }, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa el teléfono correctamente.", job.contact.msisdn, user.line);
				}

				if(user['x-prod'] == 'f1'|| user['x-prod'] == 'f1-2'){
					msg += "*Nombre Completo*: "+user["x-full-name"] + "\n";
					msg += "*Cédula*: "+user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+user["x-email"] + "\n";
					msg += "*Dirección Domicilio*: "+user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ kw + "\n\n";
				}else if(user['x-prod'] == 'f2'|| user['x-prod'] == 'f2-2'){
					msg += "*Nombre Completo*: "+user["x-full-name"] + "\n";
					msg += "*Cédula*: "+user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+user["x-email"] + "\n";
					msg += "*RUC*: "+user["x-ruc"] + "\n";
					msg += "*Dirección Domicilio*: "+user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ kw + "\n\n";
				}else if(user['x-prod'] == 'f3'|| user['x-prod'] == 'f3-2'){
					msg += "*Nombre Completo*: "+user["x-full-name"] + "\n";
					msg += "*Cédula*: "+user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+user["x-email"] + "\n";
					msg += "*Nombre Empresa*: "+user["x-empresa"] + "\n";
					msg += "*Cargo Empresa*: "+user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ kw + "\n\n";
				}
				msg += "1. Es correcta, continuar\n";
				msg += "2. Corregir\n";
				pushText (msg, job.contact.msisdn, user.line);
				break;
				
			case "f1.4" :
				var kw = body.toLowerCase()
				if (["1","1.","si","correcto","continuar","seguir","ok"].indexOf(kw)>=0) {
					updateUser({ step:"f2.1" }, job.contact.msisdn, user.line);
					pushText (
						"Listo, por favor envía una foto tuya *sin lentes* , *gafas*, *sombreros* o *gorras* sosteniendo tu cédula a la *altura del mentón* mostrando el *frente de tu cédula* como se muestra en la imagen. Pon atención de no tapar la cédula con tus dedos o mano, ni estar en contra luz.", 
						job.contact.msisdn, 
						user.line, 
						null, 
						"img", 
						null, 
						"https://s3.amazonaws.com/wsp2crmcdn/v3/legalinea/3124865265.png"
						);
				} else if (["2","2.","no","corregir"].indexOf(kw)>=0) {
					//start correct loop
					updateUser({ step:"f1.5"}, job.contact.msisdn, user.line);
					var msg = "Digita la opción que deseas corregir:\n\n"
					if(user['x-prod'] == 'f1'|| user['x-prod'] == 'f1-2'){
						msg += "1. *Nombre Completo*: "+user["x-full-name"] + "\n";
						msg += "2. *Cédula*: "+user["x-dni"] + "\n";
						msg += "3. *Correo Electrónico*: "+user["x-email"] + "\n";
						msg += "4. *Dirección Domicilio*: "+user["x-dir"] + "\n";
						msg += "5. *Provincia de Domicilio*: "+user["x-province"] + "\n";
						msg += "6. *Ciudad de Domicilio*: "+user["x-city"] + "\n";
						msg += "7. *Teléfono Fijo*: "+user["x-fijo"] + "\n";
						msg += "8. Ninguna, continuar\n\n";
					}else if(user['x-prod'] == 'f2'|| user['x-prod'] == 'f2-2'){
						msg += "1. *Nombre Completo*: "+user["x-full-name"] + "\n";
						msg += "2. *Cédula*: "+user["x-dni"] + "\n";
						msg += "3. *Correo Electrónico*: "+user["x-email"] + "\n";
						msg += "4. *RUC*: "+user["x-ruc"] + "\n";
						msg += "5. *Dirección Domicilio*: "+user["x-dir"] + "\n";
						msg += "6. *Provincia de Domicilio*: "+user["x-province"] + "\n";
						msg += "7. *Ciudad de Domicilio*: "+user["x-city"] + "\n";
						msg += "8. *Teléfono Fijo*: "+user["x-fijo"] + "\n";
						msg += "9. Ninguna, continuar\n\n";
					}else if(user['x-prod'] == 'f3'|| user['x-prod'] == 'f3-2'){
						msg += "1. *Nombre Completo*: "+user["x-full-name"] + "\n";
						msg += "2. *Cédula*: "+user["x-dni"] + "\n";
						msg += "3. *Correo Electrónico*: "+user["x-email"] + "\n";
						msg += "4. *Nombre Empresa*: "+user["x-empresa"] + "\n";
						msg += "5. *Cargo Empresa*: "+user["x-cargo"] + "\n";
						msg += "6. *RUC Empresa*: "+user["x-ruc"] + "\n";
						msg += "7. *Dirección Empresa*: "+user["x-dir"] + "\n";
						msg += "8. *Provincia de Empresa*: "+user["x-province"] + "\n";
						msg += "9. *Ciudad de Empresa*: "+user["x-city"] + "\n";
						msg += "10. *Teléfono Fijo*: "+user["x-fijo"] + "\n";
						msg += "11. Ninguna, continuar\n\n";
					}
					pushText (msg, job.contact.msisdn, user.line);			
				} else {
					var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
					pushText (msg, job.contact.msisdn, user.line);
				}
				
				break;
				
			case "f1.5":
				var kw = body.toLowerCase()
				if(user['x-prod'] == 'f1'|| user['x-prod'] == 'f1-2'){
					if (["1","1.","uno","nombre"].indexOf(kw)>=0) {
						updateUser({ step:"f1.5.1" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu nombre completo", job.contact.msisdn, user.line);
					} else if (["2","2.","dos","cédula","cedula"].indexOf(kw)>=0) {
						updateUser({ step:"f1.5.2" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu cédula", job.contact.msisdn, user.line);
					} else if (["3","3.","tres","correo","email"].indexOf(kw)>=0) {
						updateUser({ step:"fs.email" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu correo electrónico", job.contact.msisdn, user.line);
					} else if (["4","4.","direccion","dirección"].indexOf(kw)>=0) {
						updateUser({ step:"f1.5.4" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu dirección de domicilio", job.contact.msisdn, user.line);
					} else if (["5","5.","provincia"].indexOf(kw)>=0) {
						updateUser({ step:"f1.5.5" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu provincia de domicilio", job.contact.msisdn, user.line);			
					} else if (["6","6.","ciudad"].indexOf(kw)>=0) {
						updateUser({ step:"f1.5.6" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu ciudad de domicilio", job.contact.msisdn, user.line);			
					} else if (["7","7.","telefono","teléfono","telefono fijo","teléfono fijo"].indexOf(kw)>=0) {
						updateUser({ step:"f1.5.7" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu teléfono fijo incluido el código de provincia (si no tienes uno envía _no tengo_)", job.contact.msisdn, user.line);			
					} else if (["8","8.","ok","seguir","ninguna","continuar"].indexOf(kw)>=0) {
						updateUser({ step:"f2.1" }, job.contact.msisdn, user.line);
						pushText (
							"Listo, por favor envía una foto tuya *sin lentes* , *gafas*, *sombreros* o *gorras* sosteniendo tu cédula a la *altura del mentón* mostrando el *frente de tu cédula* como se muestra en la imagen. Pon atención de no tapar la cédula con tus dedos o mano, ni estar en contra luz.", 
							job.contact.msisdn, 
							user.line, 
							null,
							"img", 
							null, 
							"https://s3.amazonaws.com/wsp2crmcdn/v3/legalinea/3124865265.png"
							);
					} else {
						var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
						pushText (msg, job.contact.msisdn, user.line);
					}
				} else if( user['x-prod'] == 'f2'|| user['x-prod'] == 'f2-2'){
					if (["1","1.","uno","nombre"].indexOf(kw)>=0) {
						updateUser({ step:"f2.5.1" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu nombre completo", job.contact.msisdn, user.line);
					} else if (["2","2.","dos","cédula","cedula"].indexOf(kw)>=0) {
						updateUser({ step:"f2.5.2" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu cédula", job.contact.msisdn, user.line);
					} else if (["3","3.","tres","correo","email"].indexOf(kw)>=0) {
						updateUser({ step:"fs.email" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu correo electrónico", job.contact.msisdn, user.line);
					} else if (["4","4.", "cuatro", "ruc"].indexOf(kw)>=0) {
						updateUser({ step:"f2.5.4" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu ruc", job.contact.msisdn, user.line);
					} else if (["5","5.","direccion","dirección"].indexOf(kw)>=0) {
						updateUser({ step:"f2.5.5" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu dirección de domicilio", job.contact.msisdn, user.line);
					} else if (["6","6.","provincia"].indexOf(kw)>=0) {
						updateUser({ step:"f2.5.6" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu provincia de domicilio", job.contact.msisdn, user.line);			
					} else if (["7","7.","ciudad"].indexOf(kw)>=0) {
						updateUser({ step:"f2.5.7" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu ciudad de domicilio", job.contact.msisdn, user.line);			
					} else if (["8","8.","telefono","teléfono","telefono fijo","teléfono fijo"].indexOf(kw)>=0) {
						updateUser({ step:"f2.5.8" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu teléfono fijo incluido el código de provincia (si no tienes uno envía _no tengo_)", job.contact.msisdn, user.line);			
					} else if (["9","9.","ok","seguir","ninguna","continuar"].indexOf(kw)>=0) {
						updateUser({ step:"f2.1" }, job.contact.msisdn, user.line);
						pushText (
							"Listo, por favor envía una foto tuya *sin lentes* , *gafas*, *sombreros* o *gorras* sosteniendo tu cédula a la *altura del mentón* mostrando el *frente de tu cédula* como se muestra en la imagen. Pon atención de no tapar la cédula con tus dedos o mano, ni estar en contra luz.", 
							job.contact.msisdn, 
							user.line, 
							null,
							"img", 
							null, 
							"https://s3.amazonaws.com/wsp2crmcdn/v3/legalinea/3124865265.png"
							);
					} else {
						var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
						pushText (msg, job.contact.msisdn, user.line);
					}
				} else if(user['x-prod'] == 'f3'|| user['x-prod'] == 'f3-2'){

					if (["1","1.","uno","nombre"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.1" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu nombre completo", job.contact.msisdn, user.line);
					} else if (["2","2.","dos","cédula","cedula"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.2" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu cédula", job.contact.msisdn, user.line);
					} else if (["3","3.","tres","correo","email"].indexOf(kw)>=0) {
						updateUser({ step:"fs.email" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu correo electrónico", job.contact.msisdn, user.line);
					} else if (["4","4.","cuatro","empresa"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.4" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente el nombre de la empresa", job.contact.msisdn, user.line);
					} else if (["5","5.","cinco","cargo"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.5" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu cargo en la empresa", job.contact.msisdn, user.line);
					} else if (["6","6.","seis","ruc"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.6" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente el ruc de la empresa", job.contact.msisdn, user.line);
					} else if (["7","7.","direccion","dirección"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.7" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente la dirección de la empresa", job.contact.msisdn, user.line);
					} else if (["8","8.","provincia"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.8" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente la provincia de la empresa", job.contact.msisdn, user.line);			
					} else if (["9","9.","ciudad"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.9" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente la ciudad de la empresa", job.contact.msisdn, user.line);			
					} else if (["10","10.","telefono","teléfono","telefono fijo","teléfono fijo"].indexOf(kw)>=0) {
						updateUser({ step:"f3.5.10" }, job.contact.msisdn, user.line);
						pushText ("Envía nuevamente tu teléfono fijo incluido el código de provincia (si no tienes uno envía _no tengo_)", job.contact.msisdn, user.line);			
					} else if (["11","1!.","ok","seguir","ninguna","continuar"].indexOf(kw)>=0) {
						updateUser({ step:"f2.1" }, job.contact.msisdn, user.line);
						pushText (
							"Listo, por favor envía una foto tuya *sin lentes* , *gafas*, *sombreros* o *gorras* sosteniendo tu cédula a la *altura del mentón* mostrando el *frente de tu cédula* como se muestra en la imagen. Pon atención de no tapar la cédula con tus dedos o mano, ni estar en contra luz.", 
							job.contact.msisdn, 
							user.line, 
							null,
							"img", 
							null, 
							"https://s3.amazonaws.com/wsp2crmcdn/v3/legalinea/3124865265.png"
							);
					} else {
						var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
						pushText (msg, job.contact.msisdn, user.line);
					}
				}
				break;
			
			case "f1.5.1":
				var kw = body.toUpperCase();
				if (tools.lettersOnly(kw) && tools.countSpaces(kw) > 1) {
					//save in user
					updateUser({ step:"f1.4", "x-full-name": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n";
					msg += "*Nombre Completo*: "+ kw + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor asegúrate de escribir tu nombre correctamente, asegúrate de ingresar tus *2 nombres* y *2 apellidos*", job.contact.msisdn, user.line);
				}
				break;
			case "f1.5.2":
				var kw = body.toLowerCase();
				if (tools.isCedula(kw)) {
					//call datofast and get data
					tools.datoFastHook(kw, function(str, ob) {
						//save in user
						updateUser({ step:"f1.4", "x-dni": kw }, job.contact.msisdn, user.line);
						var msg = "Revisa la información a continuación y digita una opción:\n\n"
						msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
						msg += "*Cédula*: "+ kw + "\n";
						msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
						msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
						msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
						msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
						msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
						msg += "1. Es correcta, continuar\n";
						msg += "2. Corregir\n";
						pushText (msg, job.contact.msisdn, user.line);
					});
				} else {
					pushText ("Por favor revisa y envia tu cédula nuevamente", job.contact.msisdn, user.line);
				}
				break;
			case "f1.5.3":
				var kw = body.toLowerCase();
				//is correo valid?
				if (tools.isEmail(kw)) {
					//save in user
					updateUser({ step:"f1.4", "x-email": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ kw + "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor revisa y envia tu correo electrónico correctamente", job.contact.msisdn, user.line);
				}
				break;
			case "f1.5.4":
				var kw = body.toUpperCase();				
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-dir": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n";
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Dirección Domicilio*: "+ kw + "\n";
					msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la dirección correctamente.", job.contact.msisdn, user.line);
				}
				break;
			
			case "f1.5.5":
				var kw = body.toUpperCase();				
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-province": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ kw + "\n";
					msg += "*Ciudad de Domicilio*: "+ user['x-city'] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la provincia correctamente.", job.contact.msisdn, user.line);
				}
				break;
			case "f1.5.6":
				var kw = body.toUpperCase();				
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-city": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user['x-province'] + "\n";
					msg += "*Ciudad de Domicilio*: "+ kw + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la ciudad correctamente.", job.contact.msisdn, user.line);
				}
				break;	
			case "f1.5.7":
				var kw = body.toLowerCase();				
				//save in user
				if (kw.length > 0) {
					//save in user
					updateUser({ step:"f1.4", "x-fijo": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ kw + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa el teléfono correctamente.", job.contact.msisdn, user.line);
				}

				break;

			case "f2.5.1":
				var kw = body.toUpperCase();
				if (tools.lettersOnly(kw) && tools.countSpaces(kw) > 1) {
					//save in user
					updateUser({ step:"f1.4", "x-full-name": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n";
					msg += "*Nombre Completo*: "+ kw + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
					msg += "*RUC*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor asegúrate de escribir tu nombre correctamente, asegúrate de ingresar tus *2 nombres* y *2 apellidos*", job.contact.msisdn, user.line);
				}
				break;
			case "f2.5.2":
				var kw = body.toLowerCase();
				if (tools.isCedula(kw)) {
					//call datofast and get data
					tools.datoFastHook(kw, function(str, ob) {
						//save in user
						updateUser({ step:"f1.4", "x-dni": kw }, job.contact.msisdn, user.line);
						var msg = "Revisa la información a continuación y digita una opción:\n\n"
						msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
						msg += "*Cédula*: "+ kw + "\n";
						msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
						msg += "*RUC*: "+ user["x-ruc"] + "\n";
						msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
						msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
						msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
						msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
						msg += "1. Es correcta, continuar\n";
						msg += "2. Corregir\n";
						pushText (msg, job.contact.msisdn, user.line);
					});
				} else {
					pushText ("Por favor revisa y envia tu cédula nuevamente", job.contact.msisdn, user.line);
				}
				break;
			case "f2.5.3":
				var kw = body.toLowerCase();
				//is correo valid?
				if (tools.isEmail(kw)) {
					//save in user
					updateUser({ step:"f1.4", "x-email": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ kw + "\n";
					msg += "*RUC*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor revisa y envia tu correo electrónico correctamente", job.contact.msisdn, user.line);
				}
				break;
			case "f2.5.4":
				var kw = body.toUpperCase();				
				//save in user
				if (tools.isRUC(kw)) {
					updateUser({ step:"f1.4", "x-ruc": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n";
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*RUC*: "+ kw + "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa el ruc correctamente.", job.contact.msisdn, user.line);
				}
				break;
			
			case "f2.5.5":
				var kw = body.toUpperCase();				
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-dir": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*RUC*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Domicilio*: "+ kw + "\n";
					msg += "*Provincia de Domicilio*: "+ user['x-province'] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user['x-city'] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la dirección correctamente.", job.contact.msisdn, user.line);
				}
				break;
			case "f2.5.6":
				var kw = body.toUpperCase();				
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-province": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*RUC*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ kw + "\n";
					msg += "*Ciudad de Domicilio*: "+ user['x-city'] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la provincia correctamente.", job.contact.msisdn, user.line);
				}
				break;	
			case "f2.5.7":
				var kw = body.toUpperCase();				
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-city": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*RUC*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user['x-province'] + "\n";
					msg += "*Ciudad de Domicilio*: "+ kw + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la ciudad correctamente.", job.contact.msisdn, user.line);
				}
				break;
			case "f2.5.8":
				var kw = body.toUpperCase();				
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-fijo": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*RUC*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Domicilio*: "+ user['x-province'] + "\n";
					msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ kw + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa el teléfono correctamente.", job.contact.msisdn, user.line);
				}
				break;	
			case "f3.5.1":
				var kw = body.toUpperCase();
				if (tools.lettersOnly(kw) && tools.countSpaces(kw) > 1) {
					//save in user
					updateUser({ step:"f1.4", "x-full-name": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n";
					msg += "*Nombre Completo*: "+ kw + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
					msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
					msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor asegúrate de escribir tu nombre correctamente, asegúrate de ingresar tus *2 nombres* y *2 apellidos*", job.contact.msisdn, user.line);
				}
				break;
			case "f3.5.2":
				var kw = body.toLowerCase();
				if (tools.isCedula(kw)) {
					//call datofast and get data
					tools.datoFastHook(kw, function(str, ob) {
						//save in user
						updateUser({ step:"f1.4", "x-dni": kw }, job.contact.msisdn, user.line);
						var msg = "Revisa la información a continuación y digita una opción:\n\n"
						msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
						msg += "*Cédula*: "+ kw + "\n";
						msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
						msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
						msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
						msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
						msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
						msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
						msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
						msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
						msg += "1. Es correcta, continuar\n";
						msg += "2. Corregir\n";
						pushText (msg, job.contact.msisdn, user.line);
					});
				} else {
					pushText ("Por favor revisa y envia tu cédula nuevamente", job.contact.msisdn, user.line);
				}
				break;
			case "f3.5.3":
				var kw = body.toLowerCase();
				//is correo valid?
				if (tools.isEmail(kw)) {
					//save in user
					updateUser({ step:"f1.4", "x-email": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ kw + "\n";
					msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
					msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor revisa y envia tu correo electrónico correctamente", job.contact.msisdn, user.line);
				}
				break;
			case "f3.5.4":
				var kw = body.toUpperCase();				
				//save in user
				if (kw.length > 0) {
					
					updateUser({ step:"f1.4", "x-empresa": kw}, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n";
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Nombre Empresa*: "+ kw + "\n";
					msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa el ruc correctamente.", job.contact.msisdn, user.line);
				}
				
				break;
			case "f3.5.5":
				var kw = body.toUpperCase();
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-cargo": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
					msg += "*Cargo en Empresa*: "+ kw + "\n";
					msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor envía el nombre de tu cargo en la empresa.", job.contact.msisdn, user.line);
				}
				
				break;
			case "f3.5.6":
				var kw = body.toUpperCase();				
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-ruc": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
					msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+ kw + "\n";
					msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor envía el nombre de la empresa.", job.contact.msisdn, user.line);
				}
				break;
			case "f3.5.7":
				var kw = body.toUpperCase();
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-dir": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
					msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+ kw + "\n";
					msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la dirección correctamente.", job.contact.msisdn, user.line);
				}
				
				break;	
			case "f3.5.8":
				var kw = body.toUpperCase();
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-province": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
					msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+ kw + "\n";
					msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la provincia correctamente.", job.contact.msisdn, user.line);
				}
				
				break;
			case "f3.5.9":
				var kw = body.toUpperCase();
				//save in user
				if (kw.length > 0) {
					updateUser({ step:"f1.4", "x-city": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
					msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+ kw + "\n";
					msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa la ciudad correctamente.", job.contact.msisdn, user.line);
				}
				
				break;
			case "f3.5.10":
				var kw = body.toLowerCase();	
				if (kw.length > 0) {
					//save in user
					updateUser({ step:"f1.4", "x-fijo": kw }, job.contact.msisdn, user.line);
					var msg = "Revisa la información a continuación y digita una opción:\n\n"
					msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
					msg += "*Cédula*: "+ user["x-dni"] + "\n";
					msg += "*Correo Electrónico*: "+ user["x-email"]+ "\n";
					msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
					msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
					msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
					msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
					msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
					msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
					msg += "*Teléfono Fijo*: "+ kw + "\n\n";
					msg += "1. Es correcta, continuar\n";
					msg += "2. Corregir\n";
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor ingresa el teléfono correctamente.", job.contact.msisdn, user.line);
				}			
				
				break;	
			case "f2.1":
				if (job.msg.content.image) {
					if (job.msg.content.image) { var url = job.msg.content.image.url }
					updateUser({ step:"f2.2", "x-dniPhoto1": url }, job.contact.msisdn, user.line);
					pushText (
						"Listo, por favor una foto enfocando el *frente de tu cédula*, asegurate que la foto se vea claramente y todos los datos sean legibles.", 
						job.contact.msisdn, 
						user.line,
						null, 
						"img", 
						null, 
						"https://media.messagebird.com/v1/media/9f485d72-4b6c-4835-a9d1-2bc073d70c2d"
						);
				} else {
					pushText ("Por favor revisa y envia la foto solicitada", job.contact.msisdn, user.line);
				}
				break;
			case "f2.2":
				if (job.msg.content.image) {
					console.log (user)
					if (job.msg.content.image) { var url = job.msg.content.image.url }
					updateUser({ step:"f3", "x-dniPhoto2": url }, job.contact.msisdn, user.line);
					pushText (
						"Listo, por favor envía una foto enfocando el *posterior de tu cédula*, asegurate que todos los datos sean legibles.", 
						job.contact.msisdn, 
						user.line,
						null, 
						"img", 
						null, 
						"https://s3.amazonaws.com/wsp2crmcdn/v3/legalinea/2134679662.jpeg"
						);
				} else {
					pushText ("Por favor revisa y envia la foto solicitada", job.contact.msisdn, user.line);
				}
				break;	
			case "f3":
				if (job.msg.content.image) {
					if (job.msg.content.image) { var url = job.msg.content.image.url }
					//updateUser({ step:"f4", "x-dniPhoto3": url }, job.contact.msisdn, user.line);
					/*var msg = "Listo, por favor envía un *video* donde se vea claramente tu rostro y debes decir lo siguiente:\n\n";
					msg += "_Mi nombre completo es "+user.persona.nombre+"_\n\n";
					msg += "_Mi cédula de identidad es "+user.persona.cedula+"_\n\n";
					msg += "_Hoy es  "+dateStr()+"_\n\n";
					msg += "_Autorizo a CONISKI a emitir una firma electrónica con mi identidad_\n\n";
					pushText (msg, job.contact.msisdn, user.line);
					function dateStr() {
						var d = new Date()
						var ms = ["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"]
						return d.getDate() + " de " + ms[d.getMonth()] + " de " + d.getFullYear();	
					}*/
					switch (user["x-prod"]) {
						case "f1":
						case "f1-2":
							updateUserCB({ step:"idle", status:"active", group:"val", "x-dniPhoto3": url, "x-dniValid": false }, job.contact.msisdn, function(saved) {
								console.log("Saved", saved);
								
								if (saved) {
									var clone = JSON.parse(JSON.stringify(user));
									clone["x-dniPhoto3"] = url
									clone["x-dniValid"] = false
									createContact(job.contact.msisdn, clone);	
								}
							});
							var msg = "Listo, espera un momento mientras validamos tu identidad y los documentos enviados";
							pushText (msg, job.contact.msisdn, user.line);
							logStartTicket(job.contact.msisdn, user.chn);
							
							break;
						case "f2":
						case "f2-2":	
							updateUser({ step:"f5",  "x-dniPhoto3": url }, job.contact.msisdn, user.line);
							var msg = "Listo, por favor envía tu RUC en formato PDF\n"
							pushText (msg, job.contact.msisdn, user.line);
							break;
						case "f3":
						case "f3-2":
							updateUser({ step:"f5",  "x-dniPhoto3": url }, job.contact.msisdn, user.line);
							var msg = "Listo, por favor envía el RUC de la empresa en formato PDF\n"
							pushText (msg, job.contact.msisdn, user.line);
							break;
					}
				} else {
					pushText ("Por favor revisa y envia la foto solicitada", job.contact.msisdn, user.line);
				}
				break;
				
			case "f4":
				if (job.msg.content.video) {
					switch (user["x-prod"]) {
						case "f1":
						case "f1-2":
							updateUser({ step:"idle", status:"active", "x-dniValid": false,  group:"val", "x-dniVideo": job.msg.content.video.url }, job.contact.msisdn, user.line);
							var msg = "Listo, espera un momento mientras validamos tu identidad y los documentos enviados";
							pushText (msg, job.contact.msisdn, user.line);
							logStartTicket(job.contact.msisdn, user.chn);
							break;
						case "f2":
						case "f2-2":
							updateUser({ step:"f5",  "x-dniVideo": job.msg.content.video.url}, job.contact.msisdn, user.line);
							var msg = "Listo, por favor envía tu RUC en formato PDF\n"
							pushText (msg, job.contact.msisdn, user.line);
							break;
						case "f3":
						case "f3-2":
							updateUser({ step:"f5",  "x-dniVideo": job.msg.content.video.url}, job.contact.msisdn, user.line);
							var msg = "Listo, por favor envía el RUC de la empresa en formato PDF\n"
							pushText (msg, job.contact.msisdn, user.line);
							break;
					}
					
				} else {
					pushText ("Por favor revisa y envia un video con las instrucciones descritas.", job.contact.msisdn, user.line);
				}
				break;
			
			case "f5":
				if (job.msg.content.file) {
					var url = job.msg.content.file.url
					switch (user["x-prod"]) {
						case "f2":
						case "f2-2":
							updateUserCB({ step:"idle", status:"active", group:"val", "x-rucDoc": url, "x-dniValid": false }, job.contact.msisdn, function(saved) {
								if (saved) {
									var clone = JSON.parse(JSON.stringify(user));
									clone["x-rucDoc"] = url;
									clone["x-dniValid"] = false;
									createContact(job.contact.msisdn, clone);	
								}
							});
							var msg = "Listo, espera un momento mientras validamos tu identidad y los documentos enviados";
							pushText (msg, job.contact.msisdn, user.line);
							logStartTicket(job.contact.msisdn, user.chn);
							break;
						case "f3":
						case "f3-2":
							updateUser({ step:"f6", "x-rucDoc": url }, job.contact.msisdn, user.line);
							var msg = "Listo, por favor envía el nombramiento del representante legal en formato PDF\n"
							pushText (msg, job.contact.msisdn, user.line);
							break;
					}
				} else {
					pushText ("Por favor revisa y envia el documento solicitado en formato PDF", job.contact.msisdn, user.line);
				}
				break;

			case "f6":
				if (job.msg.content.file) {
					var url = job.msg.content.file.url
					updateUser({ step:"f7", "x-nombramiento": url}, job.contact.msisdn, user.line);
					var msg = "Listo, por favor envía la constitución de la empresa en formato PDF\n"
					pushText (msg, job.contact.msisdn, user.line);
					break;
				} else {
					pushText ("Por favor revisa y envia el documento solicitado en formato PDF", job.contact.msisdn, user.line);
				}
				break;
			case "f7":
				if (job.msg.content.file) {
					var url = job.msg.content.file.url;
					updateUserCB({ step:"idle", status:"active", group:"val", "x-constitucion": url, "x-dniValid": false }, job.contact.msisdn, function(saved) {
						if (saved) {
							var clone = JSON.parse(JSON.stringify(user));
							clone["x-constitucion"] = url;
							clone["x-dniValid"] = false;

							createContact(job.contact.msisdn, clone);	
						}
					});
					var msg = "Listo, espera un momento mientras validamos tu identidad y los documentos enviados";
					pushText (msg, job.contact.msisdn, user.line);
					logStartTicket(job.contact.msisdn, user.chn);
				} else {
					pushText ("Por favor revisa y envia el documento solicitado en formato PDF", job.contact.msisdn, user.line);
				}
				break;
				
			case "f9":
				var kw = body.toLowerCase();
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					let retryInterval;
					let attempt = 1;
					resetPaymentMethod(job.contact.msisdn);
					legalineaGeneratePagoLink(user, function (err,res) {

						if(err){
							retryInterval = setInterval(function(){
								legalineaGeneratePagoLink(user, function (err,link) {
									attempt++;
									if(attempt == 3){
										clearInterval(retryInterval);
										updateUser({ step:"idle", status:"active", group:"val" }, job.contact.msisdn, user.line);
										var msg = "Hubo un error al solicitar tu link de pago, tu solicitud pasará a manos de un asesor para ayudarte con otras opciones de pago. ";
										pushText (msg, job.contact.msisdn, user.line);
									}
									if(!link) return;
									
									clearInterval(retryInterval);
									var msg = "Por favor utiliza el siguiente enlace para realizar el pago en línea:\n\n http://wsppayphone.coniski.com/paylink/" + env.account + "/redirect/" + res.details.clientTransactionI + "\n\nSi el link ha expirado responde *LINK* para crear un nuevo link."
									var ob = {step:"f10", "x-pagoLink": res.link, "x-pagoId": res.details.clientTransactionId}
									updateUser(ob, job.contact.msisdn, user.line);
									resetPaymentMethod(job.contact.msisdn);
									pushText (msg, job.contact.msisdn, user.line);
								});
							},20000)
							return;
						}

						var msg = "Por favor utiliza el siguiente enlace para realizar el pago en línea:\n\n http://wsppayphone.coniski.com/paylink/" + env.account + "/redirect/" + res.details.clientTransactionId + "\n\nSi el link ha expirado responde *LINK* para crear un nuevo link."
						var ob = {step:"f10", "x-pagoLink": res.link, "x-pagoId": res.details.clientTransactionId}
						updateUser(ob, job.contact.msisdn, user.line);
						resetPaymentMethod(job.contact.msisdn);
						pushText (msg, job.contact.msisdn, user.line);

					});
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var ob = {step:"f12"}
					updateUser(ob, job.contact.msisdn, user.line);
					resetPaymentMethod(job.contact.msisdn);
					pushText ("Envía tu código de promoción", job.contact.msisdn, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {

					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							var ob = {step:"ftransfer"}
							updateUser(ob, job.contact.msisdn, user.line);
							let rubro = getRubro(user);
							let m = "Listo, realiza la transferencia *EN LINEA* de *$" + rubro.value.toFixed(2) + "* a la siguiente cuenta. En la referencia puedes poner LEGALINEA y tu número de cédula:";
							m += "\n\n*BANCO DEL PACÍFICO*";
							m += "\n\n*N° CUENTA:* 5177081 Cuenta Corriente";
							m += "\n*NOMBRE:* RM SIT";
							m += "\n*RUC:* 1792041228001";
							m += "\n*EMAIL:* info@coniski.com";
							m += "\n\nUna vez ralizada la transferencia envía por aquí la foto del comprobante de la transferencia o responde *regresar* para elegir otra forma de pago."
							pushText (m, job.contact.msisdn, user.line);
						}
					});
					
				} else {
					var msg = "Por favor digita una de las opciones o escribe *salir* para terminar la sesión";
					pushText (msg, job.contact.msisdn, user.line);
				}
				
				break;
			case "ftransfer":
				if (job.msg.content.image) {
					console.log (user)
					if (job.msg.content.image) { var url = job.msg.content.image.url }
					updateUser({ step:"idle", "x-transferPhoto": url }, job.contact.msisdn, user.line);
					getContact(job.contact.msisdn, function(contact) {
						updateContact({"x-transferPhoto": url, "x-transferValid": false}, contact.id)
						var msg = "Listo, por favor espera mientras validamos el pago por transferencia, esto puede tardar unos minutos.\n";
						console.log("contact", msg, job.contact.msisdn, user.line);
						
						pushText (msg, job.contact.msisdn, user.line);	
					});
				} else if (["regresar"].indexOf(kw)>=0) {
					var msg = "Selecciona una forma de pago para continuar:\n\n"
					msg += "1. Pagar con tarjeta de crédito\n";
					msg += "2. Ingresar un código de promoción\n";
					msg += "3. Pagar con transferencia bancaria";
					updateUser({ step:"f9"}, job.contact.msisdn, user.line);
					pushText (msg, job.contact.msisdn, user.line);
				} else {
					//TODO regresar a otra forma de pago
					pushText ("Por favor revisa y envia la foto solicitada o responde *regresar* para elegir otra forma de pago.", job.contact.msisdn, user.line);
				}
				break;
			case "f10":
				var kw = body.toLowerCase();
				var group = ""
				if (["link"].indexOf(kw)>=0) {
					let retryInterval;
					let attempt = 1;
					legalineaGeneratePagoLink(user, function (err,res) {
						if(err){
							retryInterval = setInterval(function(){
								legalineaGeneratePagoLink(user, function (err,link) {
									attempt++;
									if(attempt == 3){
										clearInterval(retryInterval);
										updateUser({ step:"idle", status:"active", group:"val" }, job.contact.msisdn, user.line);
										var msg = "Hubo un error al solicitar tu link de pago, tu solicitud pasará a manos de un asesor para ayudarte con otras opciones de pago. ";
										pushText (msg, job.contact.msisdn, user.line);
									}
									if(!link) return;
									
									clearInterval(retryInterval);
									var msg = "Por favor utiliza el siguiente enlace para realizar el pago en línea:\n\n http://wsppayphone.coniski.com/paylink/legalinea/redirect/" + res.details.clientTransactionId + "\n\nSi el link ha expirado responde *LINK* para crear un nuevo link."
									var ob = {step:"f10", "x-pagoLink": res.link, "x-pagoId": res.details.clientTransactionId}
									updateUser(ob, job.contact.msisdn, user.line);
									pushText (msg, job.contact.msisdn, user.line);
								});
							},20000)
							return;
						}

						var msg = "Por favor utiliza el siguiente enlace para realizar el pago en línea:\n\n http://wsppayphone.coniski.com/paylink/legalinea/redirect/" + res.details.clientTransactionId + "\n\nSi el link ha expirado responde *LINK* para crear un nuevo link."
						var ob = {step:"f10", "x-pagoLink": res.link, "x-pagoId": res.details.clientTransactionId}
						updateUser(ob, job.contact.msisdn, user.line);
						pushText (msg, job.contact.msisdn, user.line);
					});
				}  else {
					var msg = "Por favor responde *LINK* para crear nuevamente el enlace de pago o escribe *salir* para terminar la sesión";
					pushText (msg, job.contact.msisdn, user.line);
				}
				
				break;
			case "f12":
				var kw = body.toLowerCase();
				if (kw.length>=6) {
					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							legalineaValidatePromoCode(kw,  job.contact.msisdn, contact['x-dni'], user['x-prod'], function (promo) {
								console.log(promo);
								if (promo) {
									console.log(promo.type);
									if(promo.type == "free_sign") {
										let retryInterval;
										let attempt = 1;	
										pushText ("Listo, el pago fue exitoso. Tu firma está siendo procesada, por favor espera un momento...", job.contact.msisdn, user.line);
										updateUserCB({"x-pagoStatus": "redeemed-coupon", "x-coupon": kw.toUpperCase()}, job.contact.msisdn, function(saved) {
											user["x-pagoStatus"] =  "redeemed-coupon";
											user["x-coupon"] = kw.toUpperCase();
											requestFirma(user, function(res, data){
												pushText (successMessage(), job.contact.msisdn, user.line);
												if(!res) res = {result: false};;
												if(!res.result){
													retryInterval = setInterval(function(){
														requestFirma(user, function (res, data) {
															attempt++;
															if(attempt == 3){
																var msg = "Hubo un error al solicitar la firma de " + job.contact.msisdn + " " + JSON.stringify(res) + " " + "https://v3.textcenter.net/#inbox";
																pushTemplate([msg], "new_ticket_created", "593995453544", botLine);	
																clearInterval(retryInterval);
															}
															if(!res.result) return;
															
															clearInterval(retryInterval);
															
															delete data.f_cedulaFront;
															delete data.f_cedulaBack;
															delete data.f_selfie;
															delete data.f_copiaruc;
															delete data.f_nombramiento;
															delete data.f_constitucion;

															updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
																if (saved) {
																	let rubro = _.findWhere(rubros, {code: user['x-prod']})
																	let value = rubro.value;
																	let ref = rubro.name;
																	user["x-uanatacaReq"] =  {data:data, res: res}
																	createTrans(contact.id, user, "cup", value, ref, promo);
																	resetBot(job.contact.msisdn, user);
																} 	
															});
														});
													},20000)
													return;
												}

												delete data.f_cedulaFront;
												delete data.f_cedulaBack;
												delete data.f_selfie;
												delete data.f_copiaruc;
												delete data.f_nombramiento;
												delete data.f_constitucion;

												updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
													if (saved) {
														let rubro = _.findWhere(rubros, {code: user['x-prod']})
														let value = rubro.value;
														let ref = rubro.name;
														user["x-uanatacaReq"] =  {data:data, res: res}
														createTrans(contact.id, user, "cup", value, ref, promo);
														resetBot(job.contact.msisdn, user);
													} 	
												});
											});
											
										});
									}
								} else {
									updateUser({ step:"f9"}, job.contact.msisdn, user.line);
									var msg = "El código ingresado no es válido. Selecciona una opción:\n\n"
									msg += "1. Pagar con tarjeta de crédito\n";
									msg += "2. Ingresar un código de promoción\n";
									msg += "3. Pagar con transferencia bancaria";
									pushText (msg, job.contact.msisdn, user.line);	
								}
							});
						}
					});
					
				} else {
					updateUser({ step:"f9"}, job.contact.msisdn, user.line);
					var msg = "Ese no es un código válido. Selecciona una opción:\n\n"
					msg += "1. Pagar con tarjeta de crédito\n";
					msg += "2. Ingresar un código de promoción\n";
					msg += "3. Pagar con transferencia bancaria";
					pushText (msg, job.contact.msisdn, user.line);
				}
				break;
			case "f13":
				var kw = body.trim();
				pushText ("Utiliza el link de pago para completar el proceso. Si deseas cancelar el proceso envía salir", job.contact.msisdn, user.line);
				break;
				
			case "f14":
				//To Do: API LINK
				pushText (successMessage(), job.contact.msisdn, user.line);
				resetBot(job.contact.msisdn, user);
				break;
			
			case "fs.dniPhoto1":
				if (job.msg.content.image) {
					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							if (job.msg.content.image) { var url = job.msg.content.image.url }
							updateUser({ step:"idle", "x-dniPhoto1": url}, job.contact.msisdn, user.line);
							updateContact({"x-dniPhoto1": url, "x-dniValid": false, "docValidation": {"dniPhoto1": false}}, contact.id)
							var msg = "Listo, por favor espera un momento mientras validamos tu identidad.\n";
							pushText (msg, job.contact.msisdn, user.line);	
						}
					});
				} else {
					pushText ("Por favor revisa y envia la foto solicitada", job.contact.msisdn, user.line);
				}
				break;
			case "fs.dniPhoto2":
				if (job.msg.content.image) {
					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							if (job.msg.content.image) { var url = job.msg.content.image.url }
							updateUser({ step:"idle", "x-dniPhoto2": url}, job.contact.msisdn, user.line);
							updateContact({"x-dniPhoto2": url, "x-dniValid": false, "docValidation": {"dniPhoto2": false}}, contact.id)
							var msg = "Listo, por favor espera un momento mientras validamos tu identidad.\n";
							pushText (msg, job.contact.msisdn, user.line);	
						}
					});
				} else {
					pushText ("Por favor revisa y envia la foto solicitada", job.contact.msisdn, user.line);
				}
				break;
			case "fs.dniPhoto3":
				if (job.msg.content.image) {
					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							if (job.msg.content.image) { var url = job.msg.content.image.url }
							updateUser({ step:"idle", "x-dniPhoto3": url}, job.contact.msisdn, user.line);
							updateContact({"x-dniPhoto3": url, "x-dniValid": false, "docValidation": {"dniPhoto3": false}}, contact.id)
							var msg = "Listo, por favor espera un momento mientras validamos tu identidad.\n";
							pushText (msg, job.contact.msisdn, user.line);	
						}
					});
				} else {
					pushText ("Por favor revisa y envia la foto solicitada", job.contact.msisdn, user.line);
				}
				break;
			case "fs.dniVideo":
				if (job.msg.content.video) {
					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							var url = job.msg.content.video.url
							updateUser({ step:"idle", "x-dniVideo": url}, job.contact.msisdn, user.line);
							updateContact({"x-dniVideo": url, "x-dniValid": false, "docValidation": {"dniVideo": false}}, contact.id)
							var msg = "Listo, por favor espera un momento mientras validamos tu identidad.\n";
							pushText (msg, job.contact.msisdn, user.line);	
						}
					});
				} else {
					pushText ("Por favor revisa y envia un video con las instrucciones descritas.", job.contact.msisdn, user.line);
				}
				break;
			case "fs.rucDoc":
				if (job.msg.content.file) {
					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							var url = job.msg.content.file.url
							updateUser({ step:"idle", "x-rucDoc": url}, job.contact.msisdn, user.line);
							updateContact({"x-rucDoc": url, "x-dniValid": false, "docValidation": {"rucDoc": false}}, contact.id)
							var msg = "Listo, por favor espera un momento mientras validamos tu identidad.\n";
							pushText (msg, job.contact.msisdn, user.line);	
						}
					});
				} else {
					pushText ("Por favor revisa y envia el documento solicitado en formato PDF", job.contact.msisdn, user.line);
				}
				break;
			case "fs.nombramiento":
				if (job.msg.content.file) {
					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							var url = job.msg.content.file.url;
							updateUser({ step:"idle", "x-nombramiento": url}, job.contact.msisdn, user.line);
							updateContact({"x-nombramiento": url, "x-dniValid": false, "docValidation": {"nombramiento": false}}, contact.id)
							var msg = "Listo, por favor espera un momento mientras validamos tu identidad.\n";
							pushText (msg, job.contact.msisdn, user.line);	
						}
					});
				} else {
					pushText ("Por favor revisa y envia el documento solicitado en formato PDF", job.contact.msisdn, user.line);
				}
				break;
			case "fs.constitucion":
				if (job.msg.content.file) {
					getContact(job.contact.msisdn, function(contact) {
						if (contact) {
							var url = job.msg.content.file.url;
							updateUser({ step:"idle", "x-constitucion": url}, job.contact.msisdn, user.line);
							updateContact({"x-constitucion": url, "x-dniValid": false, "docValidation": {"constitucion": false}}, contact.id);
							var msg = "Listo, por favor espera un momento mientras validamos tu identidad.\n";
							pushText (msg, job.contact.msisdn, user.line);	
						}
					});
				} else {
					pushText ("Por favor revisa y envia el documento solicitado en formato PDF", job.contact.msisdn, user.line);
				}
				break;
			case "fs.email" :
				var kw = body.toLowerCase();
				//is correo valid?
				if (tools.isEmail(kw)) {
					//save in user
					let pin = tools.generatePin(4);
					updateUser({ step:"fs.email-ver", "x-email": kw ,"x-email-pin": pin }, job.contact.msisdn, user.line);
					var details = {
						"img": "https://storage.googleapis.com/pointalk/mail-logo-1590088504037.png",
						"title": "Hola! Tu código de verificacón Legalinea es: " + pin
					  }
			
					mailer.sendTemplateEmail(kw,"Verifica tu correo electrónico", "blank", details);
					pushText ("Para verificar tu correo electrónico se te ha enviado un código de 5 dígitos, por favor responde con el código a continuación.\nSi deseas cambiar el correo electrónico responde *cambiar correo*. \nSi no te llegó el correo responde *reenviar*.", job.contact.msisdn, user.line);
				} else {
					pushText ("Por favor revisa y envia tu correo electrónico correctamente", job.contact.msisdn, user.line);
				}
				break;
			case "fs.email-ver" : 
				var kw = body.toLowerCase();
				let pin2 = user['x-email-pin'];

				console.log(pin2, kw);

				if(["reenviar"].indexOf(kw)>=0){
					mailer.sendEmail(user["x-email"], "Verifica tu correo electrónico", "Tu código de verificacón es: <br>" + pin2);
					pushText ("Se te ha enviado nuevamente un código de 5 dígitos, por favor responde con el código a continuación.\nSi deseas cambiar el correo electrónico responde *cambiar correo*\nSi no te llegó el correo responde *reenviar*.", job.contact.msisdn, user.line);
				} else if (pin2 == kw) {
					//save in user
					console.log("Prod", user['x-prod']);
					
					if(user['x-prod'] == 'f1' || user['x-prod'] == 'f1-2'){
						//save in user
						updateUser({ step:"f1.4", "x-email": kw }, job.contact.msisdn, user.line);
						var msg = "Revisa la información a continuación y digita una opción:\n\n"
						msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
						msg += "*Cédula*: "+ user["x-dni"] + "\n";
						msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
						msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
						msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
						msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
						msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
						msg += "1. Es correcta, continuar\n";
						msg += "2. Corregir\n";
						pushText (msg, job.contact.msisdn, user.line);
					} else if(user['x-prod'] == 'f2' || user['x-prod'] == 'f2-2'){
						updateUser({ step:"f1.4", "x-email": kw }, job.contact.msisdn, user.line);
						var msg = "Revisa la información a continuación y digita una opción:\n\n"
						msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
						msg += "*Cédula*: "+ user["x-dni"] + "\n";
						msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
						msg += "*RUC*: "+ user["x-ruc"] + "\n";
						msg += "*Dirección Domicilio*: "+ user["x-dir"] + "\n";
						msg += "*Provincia de Domicilio*: "+ user["x-province"] + "\n";
						msg += "*Ciudad de Domicilio*: "+ user["x-city"] + "\n";
						msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
						msg += "1. Es correcta, continuar\n";
						msg += "2. Corregir\n";
						pushText (msg, job.contact.msisdn, user.line);
					}else if(user['x-prod'] == 'f3' || user['x-prod'] == 'f3-2'){
						updateUser({ step:"f1.4", "x-email": kw }, job.contact.msisdn, user.line);
						var msg = "Revisa la información a continuación y digita una opción:\n\n"
						msg += "*Nombre Completo*: "+ user["x-full-name"] + "\n";
						msg += "*Cédula*: "+ user["x-dni"] + "\n";
						msg += "*Correo Electrónico*: "+ user["x-email"] + "\n";
						msg += "*Nombre Empresa*: "+ user["x-empresa"] + "\n";
						msg += "*Cargo en Empresa*: "+ user["x-cargo"] + "\n";
						msg += "*RUC Empresa*: "+ user["x-ruc"] + "\n";
						msg += "*Dirección Empresa*: "+ user["x-dir"] + "\n";
						msg += "*Provincia de Empresa*: "+ user["x-province"] + "\n";
						msg += "*Ciudad de Empresa*: "+ user["x-city"] + "\n";
						msg += "*Teléfono Fijo*: "+ user["x-fijo"] + "\n\n";
						msg += "1. Es correcta, continuar\n";
						msg += "2. Corregir\n";
						pushText (msg, job.contact.msisdn, user.line);
					}
					
				} else {
					let emailAttempt = user['x-email-attempt'] + 1;
					//4 intentos para validar
					if(emailAttempt <= 4){
						if(kw == "cambiar correo"){
							updateUser({ step:"fs.email", "x-email-attempt": emailAttempt}, job.contact.msisdn, user.line);
							pushText ("Cuál es tu correo electrónico?", job.contact.msisdn, user.line);
						}else{
							updateUser({ "x-email-attempt": emailAttempt}, job.contact.msisdn, user.line);
							pushText ("El código no coincide, por favor revisa tu correo electrónico e ingresa el código nuevamente.", job.contact.msisdn, user.line);
						}
					} else {
						updateUser({ step:"f1.1.1.1", "x-email-attempt": emailAttempt}, job.contact.msisdn, user.line);
						pushText ("Has superado los intentos de validación por favor, contáctate directamente con nuestros canales de soporte vía email a soporte@legalinea.com para mayor información.", job.contact.msisdn, user.line);
					}
				}
				break;

		}//end switch step bot
	}
}

function legalineaMainMenu(greet, name) {
	var msg = "";
	if (greet) { 
		if (name) {
			parts = name.split(",");
			msg += "¡Hola "+parts[0]+", bienvenid@ a *Legalinea*!\n\n"; 
		} else {	
			msg += "¡Hola, bienvenid@ a *Legalinea*!\n\n";

		}
		msg += "Soy *Arthur* 🤖 el chatbot de Legalinea y te voy a asistir para obtener tu Firma Electrónica desde tu Whatsapp en pocos minutos. Comencemos....\n\n"
	}
	msg += "Por favor digita una opción:\n\n";
	msg += "1. Obtener una firma electrónica\n";
	msg += "2. Firmar electrónicamente un documento\n";
	msg += "3. Ayuda\n";
	msg += "4. Salir\n";
	return msg;		
}

function getRubro(user){
	let rubro = _.findWhere(rubros, {code: user['x-prod']})
	if(!rubro){
		return null;
	}
	return rubro;
}
function legalineaGeneratePagoLink(user, cb) {
	
	let rubro = _.findWhere(rubros, {code: user['x-prod']})

	if(!rubro){
		cb("No product found", null);
		return;
	}
	let value = rubro.value;
	let ref = rubro.name;

	generateLink(ref, value * 100, user, function(err,res){
		cb (err, res);
	})
}

function validatePagoLink(user, cb) {
	getLinkStatus(user['x-pagoId'], function(err,res){
		console.log(res);
		
		if(err){
			cb(false);
			return;
		}

		if(!res) {
			cb(false);
			return;
		}
		if(!res.transactionStatus) {
			cb(false);
			return;
		}
		if(res.transactionStatus == "Approved") {
			cb(true);
			return;
		}else{
			cb(false);
			return;
		}
	})
}

function legalineaValidatePromoCode(code, phone, dni, prod, cb) {
	validateCoupon(code, phone, dni, prod, function(coupon){
		cb(coupon)
	});
}

function formatNumber(num) {
	num +=""
	return num.substring(0,3) + "-" + num.substring(3)	
}
