var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "izo";
var booted = false;
var jobs = []
var os = require('os');


//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	//we catch MOs from bulk messages and forward to clients bots if required
	//we only process txt MOs
	if (job.msg.type == "txt") {		
		if (job.msg.m.toLowerCase() != "ok") {
			var msg = "Recuerda que, si tienes alguna duda, puedes contactarnos al 1800 - EQUINOCCIAL (378466) o escribir a nuestro WhatsApp +593-96-2996666";
			pushText(msg, job.contact, job.line)
		}
		finishJob(job);	
	} else {
		var msg = "Recuerda que, si tienes alguna duda, puedes contactarnos al 1800 - EQUINOCCIAL (378466) o escribir a nuestro WhatsApp +593-96-2996666";
		pushText(msg, job.contact, job.line)
		finishJob(job);
	}
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(1000));
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		console.log (new Date(), "Job completed");
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}
