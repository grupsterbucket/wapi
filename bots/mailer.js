// MAILER WRAPPER
const sgMail = require('@sendgrid/mail')
const fs = require('fs');
const handlebars = require('handlebars');

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

module.exports = {

	sendEmail: function(to,subject,body) {

		const msg = {
		  to: to,
		  from: 'Soporte Legalinea <soporte@legalinea.com>',
		  subject: subject,
		  html: body,
		}
		console.log ("Sending email to", to);
		sgMail
		  .send(msg)
		  .then(() => {
			console.log('Email sent')
		  })
		  .catch((error) => {
			console.error(JSON.stringify(error))
		})    
		
		return true;
	},

	sendTemplateEmail: function(to,subject,template_id,data) {
		console.log(to,subject,template_id,data);
		this.renderHTMLTemplate(template_id,data,function(err, html){
			const msg = {
				to: to,
				from: 'Soporte Legalinea <soporte@legalinea.com>',
				subject: subject,
				html: html.toString()
			  }
			  console.log ("Sending email to", to);
			  sgMail
				.send(msg)
				.then(() => {
				  console.log('Email sent')
				})
				.catch((error) => {
					console.log(error);
				  console.error(JSON.stringify(error))
			  }) 
		})
		  
		return true;
	},

	renderHTMLTemplate: function(template, replacements, callback) {
		this.readFile('./templates/' + template + '.html', function(err, html) {
			var template = handlebars.compile(html);
			var htmlToSend = template(replacements);

			if (err) callback(err);
			callback(null, htmlToSend);
		})
	},

	readFile(path, callback) {
		fs.readFile(path, {
			encoding: 'utf-8'
		}, function(err, html) {
			if (err) {
			callback(err);
			} else {
			callback(null, html);
			}
		});
	}
}
