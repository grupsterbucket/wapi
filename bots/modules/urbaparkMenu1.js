
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' urbaparkMenu1', user.id, step, group)
    console.log(job)
    switch (step) {
      // Me llega el saludo y el menu principal
      case "0":
        updateUser({ step: '1', module: "urbaparkMenu1" }, job.contact);
        pushText(mainMenu(), job.contact, user.line)
        break;

      case "1":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          updateUser({ step: '1.1' }, job.contact);
          pushText(menu11(), job.contact, user.line)
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          updateUser({ step: '1.2' }, job.contact);
          pushText(menu12(), job.contact, user.line)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "1.1":
        var kw = body.toLowerCase()
        var msg = ''
        //abrir ticket
        if (isShopOpen()) {
          msg = 'Uno de nuestros especialistas se comunicará contigo.'
        } else {
          msg = config.horarioStr
        }
        updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
        pushText(msg, job.contact, user.line)
        logStartTicket(job.contact, user.chn, user, "sac");
        break;

      case "1.2":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos121(), job.contact, user.line)
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos122(), job.contact, user.line)
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos123(), job.contact, user.line)
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos124(), job.contact, user.line)
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos125(), job.contact, user.line)
        } else if (["6", "6.", "seis"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos126(), job.contact, user.line)
        } else if (["7", "7.", "siete"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos127(), job.contact, user.line)
        } else if (["8", "8.", "ocho"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos128(), job.contact, user.line)
        } else if (["9", "9.", "nueve"].indexOf(kw) >= 0) {
          updateUser({ step: '2' }, job.contact);
          pushText(datos129(), job.contact, user.line)
        } else if (["0", "0.", "cero"].indexOf(kw) >= 0) {
          mods["main"].bot('Volver menu principal', job, user, "0.1")
        } else {
          pushText("Por favor selecciona una de las opciones del menú, escribe *0* para regresar al menú principal o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "2":
        var kw = body.toLowerCase()
        if (["0", "0.", "cero"].indexOf(kw) >= 0) {
          mods["main"].bot('Volver menu principal', job, user, "0.1")
        } else {
          pushText("Por favor escribe *0* para regresar al menu principal o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo igorMenu2")
    console.log(job)

    cb({ st: "done" })
  }
}

function mainMenu() {
  var msg = "Descubre nuestro exclusivo Valet Parking: la opción perfecta para estacionar tu vehículo de manera conveniente y segura. Nuestro equipo profesional de Valet Parkers cuidará de tu automóvil, estacionándolo en un espacio designado y devolviéndotelo cuando lo necesites.\n\n";
  msg += "Selecciona una de nuestras opciones:\n\n";
  msg += "1. Cotiza nuestro servicio de Valet para tu evento.\n";
  msg += '2. Conoce los puntos en donde encontrarás nuestro servicio fijo.\n';

  return msg
}

function menu11() {
  var msg = "Por favor, proporciona la siguiente información para poder brindarte una cotización precisa para el servicio de Valet Parking en tu evento:\n\n";
  msg += "•	Fecha del evento:\n\n";
  msg += "•	Aforo del evento:\n";
  msg += '•	Horario en el cual requieres el servicio:\n';
  msg += '•	Dirección en donde se va a realizar el evento:\n';
  msg += '•	Correo electrónico de contacto:\n\n';
  msg += 'Una vez que nos proporciones estos detalles, estaremos encantados de enviarte una cotización personalizada para el servicio de Valet Parking en tu evento. ¡Esperamos poder asistirte y hacer de tu evento una experiencia sin preocupaciones en cuanto al estacionamiento de tus invitados!';

  return msg
}

function menu12() {
  var msg = "Nuestro servicio de Valet Parking está disponible en los siguientes puntos, selecciona el punto que deseas conocer.\n\n";
  msg += "1. Restaurante Somos\n";
  msg += '2. Restaurante Tributo \n';
  msg += '3. Restaurante Nubori \n';
  msg += '4. Restaurante Bhoga \n';
  msg += '5. Restaurante Osaka \n';
  msg += '6. Restaurante Vía Parténope\n';
  msg += '7. Restaurante Alimar\n';
  msg += '8. JW Marriot\n';
  msg += '9. Swissotel\n\n';
  msg += 'Si deseas regresar al Menú principal, ingresa 0 en cualquier momento.\n\n';

  return msg
}

function datos121() {
  var msg = "Dirección: Av. Eloy Alfaro N34-421\n";
  msg += "Horario de atención: \n\n";
  msg += "• Lunes 19h00 a 22h00\n";
  msg += "• Martes a viernes de 13h00 a 16:00 y 19h00 a 22h00 \n";
  msg += "• Sábado de 12h00 a 16h00 y 19:00-22:00   \n";
  msg += "• Domingo de 12h00 a 16h00 \n";

  return msg
}

function datos122() {
  var msg = "Dirección: Isabela Católica N24-54 entre Luis Cordero y, Francisco Salazar\n";
  msg += "Horario de atención: \n\n";
  msg += "• Lunes a sábado de 12h00 a 16h00 y 19h00 a 23h00\n";
  msg += "• Domingo de 12h00 a 16h00 \n";

  return msg
}

function datos123() {
  var msg = "Dirección: Catalina Aldaz y Portugal y esquina, Edificio Bristol Parc \n";
  msg += "Horario de atención: \n\n";
  msg += "•	Lunes a viernes de 12h30 a 16h30 y 18h30 a 22h30\n";

  return msg
}

function datos124() {
  var msg = "Dirección: Av. González Suarez y Orellana\n";
  msg += "Horario de atención: \n\n";
  msg += "•	Jueves a sábado de 19h00 a 24h00\n";

  return msg
}

function datos125() {
  var msg = "Dirección: Av. Isabel la católica E12-17\n";
  msg += "Horario de atención: \n\n";
  msg += "•	Miércoles a sábado de 19h00 a 23h00\n";

  return msg
}

function datos126() {
  var msg = "Dirección: Av. República de El Salvador N34-349\n";
  msg += "Horario de atención: \n\n";
  msg += "•	Miércoles a viernes de 18h00 a 23h00 \n";
  msg += "•	Sábado y Domingo de 12h00 a 17h00 \n";

  return msg
}

function datos127() {
  var msg = "Dirección: Calle J y Alonso de Torres\n";
  msg += "Horario de atención: \n\n";
  msg += "• Viernes de 12h00 a 15h30 y 19h00 a 23h00 \n";
  msg += "•	Sábado de 12h30 a 14h30  \n";

  return msg
}

function datos128() {
  var msg = "Dirección: Av. Francisco de Orellana 1172\n";
  msg += "Horario de atención: \n\n";
  msg += "•	Lunes a domingo de 12h00 a 15h00 y 18h00 a 23h00\n";

  return msg
}

function datos129() {
  var msg = "Dirección: Av. 12 de octubre 1820\n";
  msg += "Horario de atención: \n\n";
  msg += "•	Martes a sábado de 15h00 a 23h00\n";

  return msg
}

