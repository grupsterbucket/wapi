var request = require('request');
var tools = require('./tools');
var ads = require('./hornero-ads');
var _ = require('underscore');
var firebase = require('firebase');
var os = require('os');
var client = "hornero";
var booted = false;
var jobs = []
	//firebase init
var config = {
	apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
	authDomain: "whatabot-ac0d8.firebaseapp.com",
	databaseURL: "https://whatabot-ac0d8.firebaseio.com",
	projectId: "whatabot-ac0d8",
	storageBucket: "whatabot-ac0d8.appspot.com",
	messagingSenderId: "288379073650"
};

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function(user) {
	if (user) {
		// User is signed in.
		console.log("Logged in to firebase as uid " + user.uid);
		setTimeout(function() {
			bootApp();
		}, 2000);

	} else {
		// User is signed out login again
		console.log("Session expired");
		loginTofirebase();
	}
	// ...
});

function loginTofirebase() {
	console.log("Login to firebase");
	firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function(error) {
		var errorCode = error.code;
		var errorMessage = error.message;
		console.log(error.code + ": " + error.message);
	});
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}
}

function listenToJobs() {
	firebase.database().ref("/accounts/" + client + "/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log(new Date(), "New job queued");
		console.log({
			contact: job.contact,
			st: job.st,
			msg: job.msg.body,
			id: job.id
		});
	});
}

function processJobQueue() {
	console.log("Processing job");
	if (jobs.length > 0) {
		processJob(jobs.shift());
	} else {
		setTimeout(function() {
			processJobQueue();
		}, 3000);
	}
}

function processJob(job) {
	//get user step in bot
	console.log("Here job.contact: ", job.contact);
	if (job.contact == '593984252217' || job.contact == '593995453544' || job.contact == '593999703430' || job.contact == '593995901693') {
		console.log("Here");
		firebase.database().ref("/accounts/" + client + "/chats/" + job.contact).once("value", function(snapshot) {
			var user = snapshot.val();
			var step = "0";
			var group = "main";
			if (user.step) {
				step = user.step
			};
			if (user.group) {
				group = user.group
			};

			console.log("User " + job.contact + " is at step:", step, "group:", group);

			if (step != 'idle') {
				console.log("Process MO ", job.msg.type);
				switch (job.msg.type) {
					case 'chat':
						var body = job.msg.body;
						if (body.length > 0) {
							console.log("Mo: ", body)
								//trim spaces
							body = body.trim();

							//catch operations
							if (body.toLowerCase().indexOf("#") == 0) {
								var parts = body.toLowerCase().split(" ");
								//match direct operation
								switch (parts[0]) {
									case "#ping":
										var msg = "#pong " + new Date();
										pushText(msg, job.contact, user.line);
										break;
									case "#salir":
										var msg = "Gracias por comunicarse con nosotros, adios!\n";
										pushText(msg, job.contact, user.line);
										resetUserParam("step", job.contact, user.line);
										break;
									case "#ad":
										var msg = "Prueba Ads\n";
										if (parts[1]) {
											pushAd(job.contact, user.line, msg, parts[1]);
										} else {
											pushAd(job.contact, user.line, msg);
										}
										break;
									case "#server":
										var msg = "Server data:\n";
										msg += "Datetime: " + new Date() + "\n";
										msg += "Ram: " + Math.round(os.totalmem() / 1000000) + "MB\n";
										msg += "Free: " + Math.round(os.freemem() / 1000000) + "MB\n";
										pushText(msg, job.contact, user.line);
										resetUserParam("step", job.contact, user.line);
										break;
								} //end switch parts
							} else {
								//process step
								switch (step) {
									case "0":
										if (isShopOpen()) {
											var msg = "Gracias por comunicarte con *Pizzeria el Hornero*. Por favor digita una de las siguientes opciones:\n\n";
											msg += "1. Realizar un pedido por Whatsapp\n";
											msg += "2. Realizar un pedido online\n";
											msg += "3. Consultar sobre el estado de tu pedido\n";
											msg += "4. Consultar Puntos de venta/ Horarios\n";
											msg += "5. Sugerencias\n";
											setUserParam("step", "1", job.contact, user.line);
											setUserParam("status", "new", job.contact, user.line);
										} else {
											var msg = "Gracias por comunicarte con Pizzeria el Hornero, este es un mensaje automático; nuestro horario de atención es de 10AM hasta las 21H30, solo domicilios. Visita nuestro menú en http://www.pizzeriaelhornero.com.ec";
										}
										pushText(msg, job.contact, user.line)
										break;
										//end step 1
									case "1":
										var kw = body.toLowerCase();
										if (["1", "1.", "uno"].indexOf(kw) >= 0) {
											var msg = "";
											msg += "En breve un agente se comunicará contigo, mientras esperas te invitamos a ver nuestro menú y promociones en: \n\n";
											msg += "http://www.pizzeriaelhornero.com.ec/promociones/\n\n";
											setUserParam("step", "idle", job.contact, user.line);
											setUserParam("status", "active", job.contact, user.line);
											pushText(msg, job.contact, user.line)
										} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
											var msg = "Para realizar un pedido visita nuestros canales digitales y encuentra promociones exclusivas.\n\n";
											msg += "Web: https://tienda.pizzeriaelhornero.com.ec/\n";
											msg += "Android: https://play.google.com/store/apps/details?id=com.hornero.ec\n";
											msg += "IOS: https://apps.apple.com/ec/app/hornero-ecuador/id1506256777\n\n";
											msg += "Responde *volver* si deseas regresar al menú de inicio.";
											setUserParam("step", "1", job.contact, user.line);
											pushText(msg, job.contact, user.line)
										} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
											msg += "En breve un agente se comunicará contigo. También puedes comunicarte al 1700 005 005 y rastrear tu pedido.\n\n";
											setUserParam("step", "idle", job.contact, user.line);
											setUserParam("status", "active", job.contact, user.line);
											pushText(msg, job.contact, user.line)
										} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
											msg += "Puedes encontrar tu local más cercano aquí: \n\n";
											msg += "https://pizzeriaelhornero.com.ec/locales/\n\n";
											msg += "Responde *volver* si deseas regresar al menú de inicio.";
											setUserParam("step", "1", job.contact, user.line);
											pushText(msg, job.contact, user.line)
										} else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
											var msg = "En breve un asesor se comunicará contigo.\n\n";
											setUserParam("step", "idle", job.contact, user.line);
											setUserParam("status", "active", job.contact, user.line);
											pushText(msg, job.contact, user.line);
										} else if (["volver"].indexOf(kw) >= 0) {
											if (isShopOpen()) {
												var msg = "Gracias por comunicarte con *Pizzeria el Hornero*. Por favor digita una de las siguientes opciones:\n\n";
												msg += "1. Realizar un pedido por Whatsapp\n";
												msg += "2. Realizar un pedido online\n";
												msg += "3. Consultar sobre el estado de tu pedido\n";
												msg += "4. Consultar Puntos de venta/ Horarios\n";
												msg += "5. Sugerencias\n";
												setUserParam("step", "1", job.contact, user.line);
											} else {
												var msg = "Gracias por comunicarte con Pizzeria el Hornero, este es un mensaje automático; nuestro horario de atención es de 10AM hasta las 21H30, solo domicilios. Visita nuestro menú en http://www.pizzeriaelhornero.com.ec";
											}
											pushText(msg, job.contact, user.line);
										} else{
											if (isShopOpen()) {
												var msg = "Por favor digita una de las siguientes opciones:\n\n";
												msg += "1. Realizar un pedido por Whatsapp\n";
												msg += "2. Realizar un pedido online\n";
												msg += "3. Consultar sobre el estado de tu pedido\n";
												msg += "4. Consultar Puntos de venta/ Horarios\n";
												msg += "5. Sugerencias\n";
												setUserParam("step", "1", job.contact, user.line);
											} else {
												var msg = "Gracias por comunicarte con Pizzeria el Hornero, este es un mensaje automático; nuestro horario de atención es de 10AM hasta las 21H30, solo domicilios. Visita nuestro menú en http://www.pizzeriaelhornero.com.ec";
											}
										}
										break;
								} //end switch step

							} //end if body is operation
						} //end if body length > 0
						//job is completed
						finishJob(job);
						break;
					default:
						//for any other post type we respond with menu
						if (isShopOpen()) {
							var msg = "Gracias por comunicarte con *Pizzeria el Hornero*. Por favor digita una de las siguientes opciones:\n\n";
							msg += "1. Realizar un pedido por Whatsapp\n";
							msg += "2. Realizar un pedido online\n";
							msg += "3. Consultar sobre el estado de tu pedido\n";
							msg += "4. Consultar Puntos de venta/ Horarios\n";
							msg += "5. Sugerencias\n";
							setUserParam("step", "1", job.contact, user.line);
							setUserParam("status", "new", job.contact, user.line);
						} else {
							var msg = "Gracias por comunicarte con Pizzeria el Hornero, este es un mensaje automático; nuestro horario de atención es de 10AM hasta las 21H30, solo domicilios. Visita nuestro menú en http://www.pizzeriaelhornero.com.ec";
						}
						pushText(msg, job.contact, user.line)
							//finish jobs
						finishJob(job);
						break;
				} //end switch mo type
			} else {
				console.log('user ' + job.contact + '@' + user.line + ' is idle');
				//job is completed
				finishJob(job);
			}
		});
	}
}

function finishJob(job) {
	firebase.database().ref("/accounts/" + client + "/botJobs/" + job.id + "/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(1000));
	});
}

function pushAd(contact, line, msg, code) {
	var b64 = "";
	if (code) {
		if (ads.getAd(code)) {
			var b64 = JSON.stringify({
				type: 'b64Asset',
				client: 'hornero',
				code: 'hornero_' + code,
				path: ads.getAd(code)
			});
		}
	} else {
		var d = new Date();
		var day = d.getDay();
		if (day == 2) { //martes
			var b64 = JSON.stringify({
				type: 'b64Asset',
				client: 'hornero',
				code: 'hornero_martes',
				path: ads.getAd("martes")
			});
		} else {
			var rnd = Math.floor(Math.random() * 2) + 1;
			switch (rnd) {
				case 1:
					var b64 = JSON.stringify({
						type: 'b64Asset',
						client: 'hornero',
						code: 'hornero_promo',
						path: ads.getAd("promo")
					});
					break;
				case 2:
					var b64 = JSON.stringify({
						type: 'b64Asset',
						client: 'hornero',
						code: 'hornero_congelados',
						path: ads.getAd("congelados")
					});
					break;
				default:
					var b64 = JSON.stringify({
						type: 'b64Asset',
						client: 'hornero',
						code: 'hornero_promo',
						path: ads.getAd("promo")
					});
			}
		}
	}
	if (b64.length > 0) {
		//send after random ms
		setTimeout(function() {
			var msgId = Date.now() + "-" + line + "-" + contact;
			var mt = {
				thumb: b64,
				g: 0,
				m: msg,
				rep: 'bot',
				t: "mt",
				ts: Date.now(),
				type: 'img',
				st: 'qed',
				lu: Date.now()
			};
			firebase.database().ref("/accounts/" + client + "/chats/" + contact + "/msgs/" + msgId).set(mt).then(function() {
				var ob = _.clone(mt);
				ob.line = line;
				ob.to = contact;
				ob.client = client;
				//push to client MT queue
				firebase.database().ref("/accounts/" + client + "/mtq/" + msgId).set(ob).then(function() {
					console.log(new Date(), "MT Image Response POSTED OK");
				}).catch(function(error) {
					console.log(new Date(), "Push MTQ Error", error);
				});
			}).catch(function(error) {
				console.log(new Date(), "Push MTQ Error", error);
			});
		}, tools.getRndMS(1000));
	}

}

function pushText(body, contact, line, rep, type, desc, url, b64) {
	if (!rep) {
		rep = 'bot';
	}
	if (!type) {
		type = 'txt';
	}
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now() + "-" + line + "-" + contact;
		var mt = {
			g: 0,
			m: body,
			rep: rep,
			t: "mt",
			ts: Date.now(),
			type: type,
			st: 'qed',
			lu: Date.now()
		};
		if (desc) {
			mt.desc = desc
		}
		if (url) {
			mt.url = url
		}
		if (b64) {
			mt.thumb = b64;
		}
		firebase.database().ref("/accounts/" + client + "/chats/" + contact + "/msgs/" + msgId).set(mt).then(function() {
			var ob = _.clone(mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/" + client + "/mtq/" + msgId).set(ob).then(function() {
				console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
				console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
			console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(1000));
}

function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/" + client + "/chats/" + contact + "/" + key).set(val);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/" + client + "/chats/" + contact + "/" + key).remove();
}

function isShopOpen() {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (t >= 1000 && t < 2130) {
		return true;
	} else {
		return false;
	}

}
