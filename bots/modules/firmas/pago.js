module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' Pago', user.id, step, group)
    console.log(job)
    switch (step) {
      case "0":
        if (user.currentProcess) {
          updateUser({ step: "1", module: "pago" }, job.contact);
          pushText("*Método de Pago*\n" + "Por favor, selecciona tu método de pago", job.contact, user.line, null, "btn", [{ id: "cf-ap12-20-credeb", title: "Crédito/Débito" }, { id: "cf-ap12-20-deptran", title: "Depósito/Transfer" }, { id: "cf-ap12-20-deuna", title: "Deuna" }])
          // pushText("*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
        } else {
          var nuevoProceso = {
            contacto: job.contact,
            st: 'new',
            tsc: Date.now(),
            type: 'pago',
            notes: 'Módulo de pagos'
          }
          crearProceso(nuevoProceso, function (pocesoId) {
            if (pocesoId) {
              updateUser({ step: "1", module: "utProds", currentProcess: pocesoId }, job.contact);
              pushText("*Tipo de usuario*\n" + "Selecciona una opción:\n", job.contact, user.line, null, "btn", [{ id: "firma-cf", title: "Cliente Final" }, { id: "firma-sm", title: "Socio Móvil" }, { id: "firma-mp", title: "Menú Principal" }])
            } else {
              pushText(msgErrorInterno(), job.contact, user.line);
              salir(job, user);
            }
          });
        }
        break;

      //* Aqui me llega el metodo de pago seleccionado
      case "1":
        var kw = body.toLowerCase()
        //* Tarjeta de credito
        if (kw === "cf-ap12-20-credeb") {
          console.log('Pago con tarjeta de credito/debito')
          pushText("Listo, validaremos tu pago", job.contact, user.line, "bot", "txt", null, null, null, function () {
            mods["emailOTP"].bot('Swtich Module', job, user, "0")
          });
        }
        //* Deposito o transferencia
        else if (kw === "cf-ap12-20-deptran") {
          console.log('Pago con deposito o transferencia')
          pushText("Listo, validaremos tu pago", job.contact, user.line, "bot", "txt", null, null, null, function () {
            mods["emailOTP"].bot('Swtich Module', job, user, "0")
          });
        }
        //* De una
        else if (kw === "cf-ap12-20-deuna") {
          console.log('Pago con deuna')
          pushText("Listo, validaremos tu pago", job.contact, user.line, "bot", "txt", null, null, null, function () {
            mods["emailOTP"].bot('Swtich Module', job, user, "0")
          });
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log(job)
    cb(false)
  }
}