var tools = require('./tools');
var http = require("http");
var https = require("https");
var fs = require('fs');
var qs = require('querystring');
var _ = require('underscore');
var request = require('request');
var HttpCors = require('http-cors');
var cors = new HttpCors();
var accountOb = {}
var mtq = {}
var booted = false;
var ssl = false;
var tokens = []
var assetsCache = {}
var messagebird = {}

var mtITV = 150; //Interval to post MT to MB

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs

//global scope to access from modules
global.lines = [];
global.firebase = require('firebase');

//read port and account from command line
var port = 9601 //default
var account = "demo2" //default
if (process.argv[2]) {
	var portNumber = parseInt(process.argv[2]);
	if (portNumber >= 9000 && portNumber <= 9999) { port = portNumber; }
}

if (process.argv[3]) {
	account = process.argv[3];	
}

if (process.argv[4]) {
	if (process.argv[4] == "sslon") {
		ssl = true;	
	}	
}

//firebase init
var firebaseConfig = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };
firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            boot();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function boot() {
  if (!booted) {
    booted = true;
    //load lines
    loadAccount(function (ob) {
		if (ob) {
			accountOb = JSON.parse( JSON.stringify(ob) );
			//init MB SDK
			messagebird = require('messagebird')(accountOb.mb.apiKey);
			messagebird.balance.read(function (err, response) {
				if (err) {
					return console.log(err);
				}
				console.log ("Message Bird Balance", response.amount)
				loadLines(function () {
					  //start API
					  if (!ssl) {
						http.createServer(app).listen(port);
						console.log("MBRIDGE HTTP Server started on port", port)
					  } else {
						var httpsOptions = {
							cert: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/fullchain.pem'),
							ca: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/chain.pem'),
							key: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/privkey.pem'),
							requestCert: false,
							rejectUnauthorized: false
						};

						https.createServer(httpsOptions, app).listen(port);
						console.log("MBRIDGE HTTPS Server started on port", port)
					  }
					  //start mt worker
					  setTimeout(function() { 
						  for (var i=0; i<lines.length; i++) {
							mtWorker(lines[i].id);
						  }
					  },2000)
				});
			});
			
		} else {
			console.log ("ERROR",account,"account not found")	
		}
	});
  }
}

function loadAccount(cb) {
	firebase.database().ref('/accounts/'+account+'/creds').once('value', function(snapshot) {
		if (snapshot.val()) {
			cb( JSON.parse( JSON.stringify(snapshot.val()) ) );	
		} else {
			cb(null);	
		}
	});
}
function loadLines(cb) {
	firebase.database().ref('/lines/').orderByChild("client").equalTo(account).once('value', function(snapshot) {		
		var arr = _.map (snapshot.val(), function(value, key) {
			value.id = key;
			return value;
		});
		for (var i=0; i<arr.length; i++) {
			if (arr[i].isWAPIMB) {
				lines.push( arr[i] );
				tokens.push(arr[i].apiToken);
				MBHookSetup(arr[i]);
			}
		}
		console.log (lines.length + " line(s) loaded for "+account);
		//initializq mtq arrays
		for (var i=0; i<lines.length; i++) {
			mtq[lines[i].id] = [];
		}
		//load mtq
		loadMTQ(account);
		if (cb) { cb(); }
	});
}

function MBHookSetup(line) {
	//setup MB hooks
	var paramsmb = {
	  'events': [
	  'conversation.created',
	  'conversation.updated',
	  'message.created',
	  'message.updated'
	  ],
	  'channelId': line.mbID,
	  'url': 'https://textcenter.net:'+port+'/mbhook'
	}
	messagebird.conversations.webhooks.create(paramsmb, function (err, response) {
	  if (err) {
	  return console.log(err);
	  }
	  console.log(response);
	  messagebird.conversations.webhooks.list(100, 0, function (err, response) {
		  if (err) {
		  return console.log(err);
		  }
		  console.log(response);
		});
	});
	//~ messagebird.conversations.webhooks.delete('a7c62bd857d4485d9130d3b3b8ee3f96', function (err, response) {
	  //~ if (err) {
	  //~ return console.log(err);
	  //~ }
	  //~ console.log(response);
	//~ });
}

function loadMTQ(account) {
	firebase.database().ref("/accounts/"+account+"/mtq/").orderByChild("st").equalTo("qed").on('child_added', function(res) {
		var msg = res.val();
		msg.id = res.key;
		if (mtq[msg.line]) {			
			var door = Date.now() - accountOb.misc.purgeAge;
			if (msg.ts>door) {					
					mtq[msg.line].push(msg);
					console.log (new Date(), "NEW MT FOR", account, msg.line, mtq[msg.line].length);					
			} else {
				console.log (new Date(), "Purge", msg.id)	
				purgeMT(msg);
			}
		} else {
		    console.log (new Date(), "Line NOT VALID", msg);
		}
	});
}

function mtWorker(lineId) {
	
	if (mtq[lineId]) {
		if (mtq[lineId].length>0) {
			console.log (new Date(), "Run MT Worker for", lineId, " l =", mtq[lineId].length	)	
			var mt = mtq[lineId].shift();
			postToMB(mt, function(res) {
				console.log (res)
				if (res.status == "error") {
					//requeue wait and call again
					mtq[lineId].unshift(mt);
					setTimeout (function() { mtWorker(lineId) }, 5000)
				} else {
					//Update mtq
					firebase.database().ref("/accounts/"+mt.client+"/mtq/"+mt.id).update({ line: lineId, lu: Date.now(), st: 'snt', mid: res.id }).then(function() {
						console.log ("MT Sent to MB");
					}).catch(function(error) {
						console.log(new Date(), "Error updating MTQ", error);
					});
					//save ACK Ref
					var ack = { client: mt.client, contact:mt.to , msgId: mt.id }
					firebase.database().ref("/ack/"+res.id).set(ack, function() {
						console.log (new Date(), "ACK Ref saved");
					}).catch(function(error) {
						console.log(new Date(), "Error saving ACK", error);
					});
					setTimeout (function() { mtWorker(lineId) }, mtITV);
				}
			})
		} else {
			setTimeout (function() { mtWorker(lineId) }, mtITV);
		}
	} else {
		setTimeout (function() { mtWorker(lineId) }, mtITV);
	}
}

function postToMB(mt, cb) {
	var lineOb = _.findWhere( lines, {id: mt.line} );
	if (lineOb) {
		var params = {
		  'to': '+'+mt.to,
		  'from': lineOb.mbID,
		  'reportUrl': 'https://textcenter.net:'+port+'/mback',
		  'content': {}
		};
		switch (mt.type) {
			case "txt":
				params.type = "text";
				params.content.text = mt.m;
				break;
			case "img":
				params.type = "image";
				params.content.image = { url: mt.url, caption: mt.m}
				if (mt.desc) {params.content.image.caption += "\n" + mt.desc}
				break;
			case "doc":
				params.type = "file";
				params.content.file = { url: mt.url, caption: mt.url}
				break;
			default:
				params.type = "text";
				params.content.text = "[mt "+mt.type+"]";
		}
		console.log (params)
		messagebird.conversations.send(params, function (err, response) {
		  if (err) {
			console.log(err);
			cb ({status: 'error'});
		  } else {
			cb (response)
		  }
		});
	} else {
		cb ({status: 'error'});
	}
}

function runOp(post) {
	var line = post.line;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
		switch (post.op) {
			case "reset-bot":
				firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+post.contact+"/step").remove();
				firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+post.contact+"/status").remove();
				firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+post.contact+"/pin").remove();
				console.log (new Date(), "Run OP OK", post.op, post.contact);
				break;
			default:
				console.log (new Date(), "Unknown OP", post.op, post.contact);
		}
	} else {
		console.log (new Date(), "Error running op line not found", line);
	}
}

function pushMT(post) {
	
	var type = 'txt';
	if ( post.type ) { type = post.type }
	var rep = 'wapi';
	if ( post.rep ) { rep = post.rep }
	
	var line = post.line;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
		var msgId = Date.now()+"-"+line+"-"+post.to;
		var mt = { g:0, m:post.body, to: post.to, rep: rep, t:"mt", ts: Date.now(), type: type, st: 'qed', lu: Date.now()};
		if (post.desc) { mt.desc = post.desc; }
		if (post.url) { mt.url = post.url; }
		if (post.thumb) { mt.thumb = post.thumb; }
		if (post.cid) {mt.cid = post.cid; }
		firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+post.to+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = post.to;
			ob.client = lineOb.client;
			//push to account MT queue
			firebase.database().ref("/accounts/"+lineOb.client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT POSTED OK TO MTQ");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Save MT to chat Error", error);
		});
	} else {
		console.log (new Date(), "Error pushing line not found", line);
	}
}

function processMO( post ) {
	console.log (new Date(), "Processing MO", post.message.type);
	var msg = post.message;
	var line = msg.to.replace("+","");
	var chat = msg.chat;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
	
		//build mo from type
		switch (msg.type) {
			case 'text':
				var mo = { g:3, m: msg.content.text, rep: 'wapimb', t:"mo", ts: Date.now(), type: 'txt', cid: post.conversation.id };
				break;
			case 'image':
				var strBody = "[imagen]";
				if (msg.content.image.caption) { strBody = msg.content.image.caption }
				var mo = { g:3, m: strBody, rep: 'wapimb', t:"mo", ts: Date.now(), type: 'img', url: msg.content.image.url , cid: post.conversation.id};
				break;
			case 'file':
				var strBody = "[document]";
				if (msg.content.file.caption) { strBody = msg.content.file.caption }
				var mo = { g:3, m: strBody, rep: 'wapimb', t:"mo", ts: Date.now(), type: 'doc', url: msg.content.file.url , cid: post.conversation.id};
				break;
			case 'location':
				var mo = { g:3, m:'[location]', rep: 'wapimb', t:"mo", ts: Date.now(), type: 'loc', lng: msg.content.location.longitude, lat: msg.content.location.latitude, cid: post.conversation.id};
				break;
			case 'video':
				var strBody = "[video]";
				if (msg.content.video.caption) { strBody = msg.content.video.caption }
				var mo = { g:3, m: strBody, rep: 'wapimb', t:"mo", ts: Date.now(), type: 'vid', url: msg.content.video.url , cid: post.conversation.id};
				break;
			case 'audio':
				var mo = { g:3, m: '[audio]', rep: 'wapimb', t:"mo", ts: Date.now(), type: 'aud', url: msg.content.audio.url , cid: post.conversation.id};
				break;
			default:
				var mo = { g:3, m: msg.type, rep: 'wapimb', t:"mo", ts: Date.now(), type: msg.type, cid: post.conversation.id, dets: msg};
		}
		var contact = post.contact;
		var senderId = msg.from.replace("+","");
		var name = senderId;
		if (contact.displayName) {
			name = contact.displayName;	
		}

		var user = { chn: 'mb', id: senderId, line: line, nick: name, lu: Date.now(), cid: post.conversation.id }
		
		if ( false) { //is group
			mo.contact = sender.id.user;
			mo.nick = sender.id.user;
			if (sender.pushname) {
				mo.nick = sender.pushname;
			}
			if (chat.formattedTitle) {
				user.nick = chat.formattedTitle;
			}
		}

		//create or update user
		firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+senderId).update(user).then(function() {
			firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+senderId+"/msgs/"+msg.id).set(mo).then(function() {
			   //run bot
			   if (lineOb.hasBot) {
				   delete contact.attributes;
				   delete contact.customDetails;
				   var job = {
					   msg: msg,
					   contact: contact,
					   st: 'new',
					   ts: Date.now()
					   }
				   firebase.database().ref("/accounts/"+lineOb.client+"/botJobs/"+msg.id).set(job);
			   } else {
				   //start ticket
				   firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+senderId).once("value", function(u) {
					   var usr = u.val();
					   if (usr.tso) {
						   //ticket already started
					   } else {
						   firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+senderId).update({tso: Date.now(), status: "active"});
						   console.log (new Date(), "Ticket started");
					   }
                   });					
			   }
			   console.log (new Date(), "MO Processed OK");
			}).catch(function(error) {
			   console.log (new Date(), "Error posting MO to Firebase", line);
			   console.log (error)
			   console.log (mo)
			   //TODO: persist this msg in local file to resend MOs
			});
		}).catch(function(error) {
			console.log (new Date(), "Error posting User to Firebase", line);
			console.log (error)
			console.log (sender)
			console.log (user)
			//TODO: persist this msg in local file to resend MOs
		});
	} else {
		console.log (new Date(), "Error processing MO line not found", line);
	}

}

function processACK(post) {
	var ob = {g: 0, lu: Date.now()}
	switch (post.message.status) {
		case "sent":
			ob.g = 1
			break;
		case "delivered":
			ob.g = 2
			break;
		case "read":
			ob.g = 3
			break;
		default:
			ob.g = post.message.status
			if (post.error) {
				if (post.error.description) {
					ob.err = post.error.description;
				}	
			}
	}
	if (post.message.id) {
		//get ack ref
		firebase.database().ref("/ack/"+post.message.id).once('value', function(res) {
			if (res.val()){
				var ref = res.val();
				firebase.database().ref("/accounts/"+ref.client+"/chats/"+ref.contact+"/msgs/"+ref.msgId).update(ob).then(function() {
					console.log (new Date(), "ACK updated for", post.message.id);
				});
			}
		});
		
	}
	
}

function purgeMT(msg) {
	var lineOb = _.findWhere( lines, {id: msg.line} );
	if (lineOb) {
			//set mtq status to purged
			firebase.database().ref("/accounts/"+lineOb.client+"/mtq/"+msg.id).update({ lu: Date.now(), st: 'pur' })
	} else {
		console.log (new Date(), "Error purgeMT line not found", msg.line);
	}
}

function processPost(req, res, callback) {
	var queryData = "";
	if(typeof callback !== 'function') return null;

	req.on('data', function(data) {
		queryData += data;
		if(queryData.length > 10000000) {
			queryData = "";
			res.writeHead(413, {'Content-Type': 'text/plain'}).end();
			req.connection.destroy();
		}
	});

	req.on('end', function() {
		if (req.method == 'GET') {
			req.post = qs.parse(req.url.replace(/^.*\?/, ''));
		} else {
			req.post = queryData;
		}
		callback();
	});
}


var app = function(req, res) {

     if(cors.apply(req, res)) {
		 res.end();
	 }

	 processPost(req, res, function() {
		console.log ("URL", req.url);
		var parts = req.url.split("/");
		//router
        switch (parts[1]) {
			case 'send':
                //process MT received from API call
				   console.log (new Date(), "MT received");
				   if (req.method == "POST") {
	
						if (typeof req.post === "string") {
							var post = _.clone ( qs.parse(req.post)	);
						} else {
							var post = _.clone ( req.post );
						}
						if (tokens.indexOf(post.apiToken)>=0 ) {
							var numberCheck = tools.numberFormat(post.to, post.isResponse)
							if (numberCheck.valid) {
								post.to = numberCheck.to;
								if (post.body) {
									if (post.body.length > 0) {
										pushMT (post)
										res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
										res.write("mt-pushed-ok");
										res.end();
									} else {
										res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
										res.write("invalid-message");
										res.end();
									}
								} else {
									res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
									res.write("invalid-message");
									res.end();
								}
							} else {
								res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
								res.write("invalid-number");
								res.end();
							}
						} else {
							res.writeHead(401, "ERROR", {'Content-Type': 'text/plain'});
							res.write("invalid-token");
							res.end();
						}

				   } else {
					   res.writeHead(405, {'Content-Type': 'text/plain'});
					   res.write("Invalid request method");
					   res.end();
				   }

                break;
            case 'run-op':
                
				   console.log (new Date(), "OP received");
				   if (req.method == "POST") {
	
						if (typeof req.post === "string") {
							var post = _.clone ( qs.parse(req.post)	);
						} else {
							var post = _.clone ( req.post );
						}
						if (tokens.indexOf(post.apiToken)>=0 ) {
							runOp(post);
							res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
							res.write("run-op-ok");
							res.end();
							
						} else {
							res.writeHead(401, "ERROR", {'Content-Type': 'text/plain'});
							res.write("invalid-token");
							res.end();
						}

				   } else {
					   res.writeHead(405, {'Content-Type': 'text/plain'});
					   res.write("Invalid request method");
					   res.end();
				   }

                break;
            case 'mbhook?':
				console.log (new Date(), "MB Hook received");
			    console.log (req.post);
			    var post = JSON.parse(req.post);
			    switch (post.type) {
					case "message.created":
						if (post.message.direction == "received") {
							processMO(post)	
						}
						break;
					default:
						console.log ("Cannot process hook",post.type);  
				}
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("hook-ok");
			    res.end();
				break;
			case 'mback?':
				console.log (new Date(), "MB ACK received");
			    console.log (req.post);
			    var post = JSON.parse(req.post);
				switch (post.type) {
					case "message.status":
						if (post.message.id && post.message.status) {
							processACK(post);
						} else {
							console.log ("This is MT", post.message.direction);	
						}
						break;
					default:
						console.log ("Cannot process ack",post.type);  
				}
			
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("ack-ok");
			    res.end();
				break;
            
            case "ping":
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("pong");
				res.end();
				break;

            default:
			   console.log(new Date(), "unknown path "+req.url);
			   res.writeHead(404, {'Content-Type': 'text/plain'});
			   res.write("unknown path "+req.url);
			   res.end();
        }
    });
}
