// Bot as module
var qs = require('querystring');
module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing feasy sub bot", step, group)
		switch (step) {
			case "0":
				setUserParam("step","1", job.contact, user.line);
				pushText (mainMenu(true), job.contact, user.line);
				break;
				//end step 0
			case "1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","sri"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText (SRI()+GoBackMenu(), job.contact, user.line);
				} else if (["2","2.","dos","firma"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText (Firma()+GoBackMenu(), job.contact, user.line);
				} else if (["3","3.","tres","factura","facturas","facturacion","facturación"].indexOf(kw)>=0) {
					setUserParam("step","3.1", job.contact, user.line);
					pushText (Facturas(), job.contact, user.line);
				} else if (["4","4.","cuatro","recarga","saldo","recargas","creditos","créditos"].indexOf(kw)>=0) {
					setUserParam("step","4.1", job.contact, user.line);
					pushText (Recargas(), job.contact, user.line);
				} else if (["5","5.","cinco","asesoria","asesoría","cita","asesor","ayuda","help"].indexOf(kw)>=0) {
					var pin = Math.floor(Math.random() * 625500) + 100000;
					var authStr1 = encr(job.contact+"|feasy|res01|cit|"+pin+"|"+Date.now()+"|blank.png");
					makeShortURL(authStr1, function(str1) {
						var link = "https://events.textcenter.net/#home/SC@" + str1 ;
						pushText (Cita(link)+GoBackMenu(), job.contact, user.line);
						var ob = {pin:pin, step:"go-back", group:"ven"}
						updateUser(ob, job.contact, user.line);
					});
			    } else if (["6","6.","seis","contador","contadora"].indexOf(kw)>=0) {
					setUserParam("step","6.1", job.contact, user.line);
					pushText (Contador(), job.contact, user.line);
			    } else if (["7","7.","siete","otro"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText (Otro()+GoBackMenu(), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "3.1":
				//facturas
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText (emitirFactura()+GoBackMenu(), job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText (anularFactura()+GoBackMenu(), job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText (erroresSRI()+GoBackMenu(), job.contact, user.line);
				} else if (["4","4.","cuatro"].indexOf(kw)>=0) {
					setUserParam("step","1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "4.1":
				//recargas
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Puedes comprar créditos con tarjeta de crédito o con transferencia bancaria. Ingresa a tu cuenta en https://factureasy.com/app/#shop y compra una recarga.\n\n";
					setUserParam("step","go-back", job.contact, user.line);
					pushText (msg+GoBackMenu(), job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					if (isShopOpen()) {
						var msg = "Hiciste un pago por transferencia? Por favor envíanos la foto/captura del recibo y el código de transacción. En unos minutos validamos el pago y acreditamos tu saldo.";
						updateUser({step:"idle", status: "active", group: "sac", tso: Date.now()}, job.contact, user.line);
						pushText (msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "sac");
					} else {
						pushText (config.horarioStr, job.contact, user.line);
						setUserParam("step","0", job.contact, user.line);
					}
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					if (isShopOpen()) {
						var msg = "Tienes un problema con una recarga? Por favor envíanos los detalles y te responderemos lo antes posible. Gracias!\n";
						updateUser({step:"idle", status: "active", group: "sac", tso: Date.now()}, job.contact, user.line);
						pushText (msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "sac");
					} else {
						pushText (config.horarioStr, job.contact, user.line);
						setUserParam("step","0", job.contact, user.line);
					}
				} else if (["4","4.","cuatro"].indexOf(kw)>=0) {
					setUserParam("step","1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "6.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					if (isShopOpen()) {
						var msg = "Un momento por favor, en un momento te atendemos\n";
						updateUser({step:"idle", status: "active", group: "con", tso: Date.now()}, job.contact, user.line);
						pushText (msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "con");
					} else {
						pushText (config.horarioStr, job.contact, user.line);
						setUserParam("step","0", job.contact, user.line);
					}
				} else {											
					setUserParam("step","1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				}
				break;
			case "go-back":
				var kw = body.toLowerCase()
				var group = ""
				if (["salir","*salir*"].indexOf(kw)>=0) {
					salir(job, user);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					salir(job, user);
				} else {											
					setUserParam("step","1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				}
				break;
		}//end switch step
		
		function mainMenu(greet) {
			if (greet) {
				var msg = "¡Hola! Bienvenido al canal de atención *Factureasy*. Para ayudarte, selecciona una de las siguientes opciones:\n\n";
			} else {
				var msg = "Por favor selecciona una de las siguientes opciones:\n\n";
			}
			msg += "1. Configuración SRI\n";
			msg += "2. Firma Electrónica\n";
			msg += "3. Facturación Electrónica\n";
			msg += "4. Recargas\n";
			msg += "5. Agendar cita con Asesor\n";
			msg += "6. Soy contador/a\n";
			msg += "7. Otro\n";
			return msg;
		}
		
		function SRI() {
		    var msg = "Sigue estos pasos en tu *SRI En Línea* para poder emitir facturas electrónicas:\n\n";
			msg += "Ingresa al SRI En línea con tu *RUC*, no con tu cédula\n\n";
			msg += "Ponte al día en tus obligaciones\n\n";
			msg += "Si no lo has hecho aún, actualiza tu correo electrónico en el SRI:\nhttps://srienlinea.sri.gob.ec/sri-en-linea/contribuyente/perfil\n\n";
			msg += "Ingresa los datos para el convenio de débito automático en:\nMenú Principal > Pagos > Registro convenio de débito bancario\n\n";
			msg += "Solicita la autorización para emitir facturas electrónicas en:\nMenú Principal > Facturación Electrónica > Producción > Autorización\n\n";
			msg += "Todos los pasos anteriores son obligatorios para poder emitir facturas electrónicas\n\n";
			return msg;
		}
		
		function Otro() {
		    var msg = "No has podido resolver tu inquietud por este canal? Por favor escríbenos los detalles a soporte@factureasy.com, responderemos lo antes posible\n\nGracias\n\n";
			return msg;
		}
		
		function Firma() {
		    var msg = "Para poder emitir facturas electrónicas es necesario que saques tu firma electrónica. Si no la tienes aún sigue estos pasos:\n\n";
			msg += "Obtén tu firma con una autoridad autorizada por el Arcotel.\n\n";
			msg += "Te recomendamos sacar tu firma con nuestro partner *UANATACA Ecuador*\nUtiliza el siguiente enlace: https://store.uanataca.ec/?ref=CNKCL1 y pon el código *CNKCL1* en el formulario de registro para obtener un descuento.\n\n";
			msg += "Por seguridad, recomendamos sacar una firma a un año y renovarla anualmente\n\n";
			//msg += "Asegúrate de sacar tu firma con tu *RUC*, si sacas una firma solamente para firmar documentos no te servirá para facturación electrónica\n\n";
			msg += "Saca una firma en *archivo* y memoriza bien tu PIN (la contraseña de la firma). El archivo y el PIN tendrás que subir en Factureasy para poder firmar las facturas electrónicas. Para tu seguridad, ambos serán cifrados con tu clave antes de subirlos a la nube\n\n";
			msg += "Si ya tienes una firma vigente de otro proveedor, si puedes usarla en Factureasy\n\n";
			return msg;
		}
		
		function GoBackMenu() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Regresar al menú principal\n";
			msg += "2. Salir";
			return msg;
		}
		
		function Facturas() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Emitir una factura\n";
			msg += "2. Anular una factura\n";
			msg += "3. Errores de validación del SRI\n";
			msg += "4. Regresar al menú anterior\n";
			return msg;
		}
		
		function emitirFactura() {
			var msg = "Para poder emitir una factura debes cumplir con los siguientes requisitos:\n\n";
			msg += "Realizar la configuración de facturación electrónica en el *SRI En Línea*\n\n";
			msg += "Obtener tu firma digital. Te recomendamos sacar tu firma con nuestro partner *UANATACA Ecuador*\nUtiliza el siguiente enlace: https://store.uanataca.ec/?ref=CNKCL1 y pon el código *CNKCL1* en el formulario de registro para obtener un descuento\n\n";
			msg += "Crear una cuenta gratuita en https://factureasy.com\n\n";
			msg += "Puedes revisar todos los detalles paso a paso en nuestro centro de ayuda al usuario en este enlace: https://factureasy.com/ayuda\n\n";
			return msg;
		}
		
		function anularFactura() {
			var msg = "Para poder anular una factura debes realizar dos pasos:\n\n";
			msg += "Primero debes buscar y anular la factura en Factureasy https://factureasy.com/app/#facturas\n\n";
			msg += "Segundo debes ingresar a tu cuenta del *SRI En Línea* y anular el comprobante.\n";
			msg += "Esto lo puedes hacer en:\n Menú Principal > Facturación Electrónica > Producción > Autorización > Solicitud de anulación comprobantes\n\n";
			msg += "Los detalles de comprobante como clave de acceso, fecha de autorización, etc los puedes encontrar en el RIDE de la factura que te llegó al correo o en Factureasy\n\n";
			return msg;
		}
		
		function erroresSRI() {
			var msg = "Puedes revisar la lista completa de errores comunes del SRI en nuestro centro de ayuda al usuario en este enlace: https://factureasy.com/ayuda/index.html#faq4\n\n";
			
			return msg;
		}
		
		
		function Recargas() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Comprar créditos\n";
			msg += "2. Enviar recibo de transferencia\n";
			msg += "3. Contactar servicio al cliente\n";
			msg += "4. Regresar al menú anterior\n";
			return msg;
		}
		
		function Cita(link) {
			var msg = "En Factureasy te brindamos el servicio de asesoría personalizada para ayudarte a completar todos los requisitos necesarios para emitir tus facturas\n\n";
			msg += "Nuestro servicio es uno a uno con asesores especialistas. La asesoría personalizada tiene un costo de *USD 25 + IVA* y puedes realizar el pago con tarjeta de crédito o transferencia bancaria\n\n";
			msg += "El servicio consiste en una sesión de chat por este canal con un asesor quién te guiará paso a paso y te ayudará a solventar errores comunes.\n\n";
			msg += "Puedes utilizar el siguiente enlace para agendar tu cita:\n\n";
			msg += link+"\n\n";
			return msg;
		}
		
		function Contador() {
			var msg = "Trae tus clientes a Factureasy y recibe una comisión de ventas ¡Tenemos un programa de referidos para ti!\n\n";
			msg += "1. Me interesa\n";
			msg += "2. Regresar al menú principal";
			return msg;
		}
	}
}
