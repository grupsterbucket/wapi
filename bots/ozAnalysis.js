global.fetch = require('node-fetch');
global.fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));

var folderId = '20c251ed-6687-4fd0-b634-93335a8c5d5b'
var mediaId = '246dfddb-c281-46b7-a561-40e2e956572e'

var body = {
  "analyses": [
    {
      "source_media": [
        mediaId
      ],
      "type": "QUALITY"
    }
  ],
  "params": {
    "extract_best_shot": true,
    "threshold_max": 0,
    "threshold_min": 0,
    "threshold_spoofing": 0
  }
}


fetch('https://api.sandbox.ozforensics.com/api/folders/'+folderId+'/analyses', {
	method: 'post',
	body: JSON.stringify( body ),
	headers: { 'Content-Type': 'application/json', 'X-Forensic-Access-Token': env.ozAccessToken}
}).then(function(response) {
	if (response.status !== 200 ) {
		console.log(new Date(),'OZ Analy Error',response.status);
		response.text().then(function(data) {
			console.log (data)
		});
	} else {
		response.json().then(function(data) {
			console.log (data)
		});
	}
}).catch(function(err) {
	console.log(new Date(),'OZ Analy Error',err.message);
	console.log (err)
	
});		
