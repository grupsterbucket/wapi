var _ = require('underscore');
var firebase = require('firebase');
var admin = require("firebase-admin");
var fs = require('fs');
var path = require('path');
//~ //firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

//~ var config = {
	//~ "apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    //~ "authDomain": "ubiku3.firebaseapp.com",
    //~ "databaseURL": "https://ubiku3.firebaseio.com",
    //~ "projectId": "ubiku3",
    //~ "storageBucket": "ubiku3.appspot.com",
    //~ "messagingSenderId": "946813430025"
    //~ }
 
firebase.initializeApp(config);


//Initialize Firebase Admin
admin.initializeApp({
  credential: admin.credential.cert( JSON.parse(fs.readFileSync(path.resolve(__dirname, './whatabot-sa.json'), 'utf8')) ),
  databaseURL: "https://whatabot-ac0d8.firebaseio.com"
});


firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            boot();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
	//firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function boot() {
	firebase.database().ref('/users').orderByChild("client").equalTo("hornero").once('value', function(snapshot) {
		if (snapshot.val()) {
			var arr = _.map(snapshot.val(), function(ob, key) {
				ob.id = key;
				return ob
			});
			for (var i=0; i<arr.length; i++) {
				var pwd = "h0rn3"+ (
				Math.floor(Math.random() * 9999) + 1111  )
				console.log ("Nueva clave para ", arr[i].email, pwd);
				//updateUserPassword(pwd, arr[i].id);
			}
		}
	});
}

//admin functions
function updateUserPassword(pwd, uid) {
	admin.auth().updateUser(uid, {password: pwd}).then(function(userRecord) {
		console.log("Successfully updated user password for ", uid);
	})
	.catch(function(error) {
		console.log("Error updating user password:", error);
	});
}
