// Bot as module
var qs = require('querystring');
module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing sis sub bot", step, group)
		switch (step) {
			case "0":
				setUserParam("step","1", job.contact, user.line);
				pushText (mainMenu(true), job.contact, user.line);
				break;
				//end step 0
			case "1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					setUserParam("step","1.1", job.contact, user.line);
					pushText (Shop(), job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText (Cats()+GoBackMenu(), job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					setUserParam("step","3.1", job.contact, user.line);
					pushText (Promos(), job.contact, user.line);
				} else if (["4","4.","cuatro","otro"].indexOf(kw)>=0) {
					var msg = "Hola! Soy Diana de Sisteryl Nuts. En qué puedo ayudarte?";
					updateUser({step:"idle", status: "active", group: "sac", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "sac");
					pushText (msg, job.contact, user.line);
				} else if (["5","5.","cinco"].indexOf(kw)>=0) {
					setUserParam("step","5.1", job.contact, user.line);
					pushText (Tiendas(), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "1.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Utiliza este enlace para hacer tu pedido online: https://www.sisterlynuts.com/tienda\n\n";
					setUserParam("step","go-back", job.contact, user.line);
					pushText (msg+GoBackMenu(), job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "Hola! Soy Diana de Sisteryl Nuts. En qué puedo ayudarte?";
					updateUser({step:"idle", status: "active", group: "com", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "com");
					pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = "Utiliza este enlace para encontrar el punto de venta más cercano: https://sisterlynuts.com/puntos-de-venta/\n\n";
					setUserParam("step","go-back", job.contact, user.line);
					pushText (msg+GoBackMenu(), job.contact, user.line);
				} else if (["4","4.","cuatro"].indexOf(kw)>=0) {
					setUserParam("step","1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "3.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","2","2.","dos","3","3.","tres","4","4.","cuatro"].indexOf(kw)>=0) {
					var msg = "Hola! Soy Diana de Sisteryl Nuts. En un momento te ayudo.";
					updateUser({step:"idle", status: "active", group: "pro", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "pro");
					pushText (msg, job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "5.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "En Quito puedes visitar las tiendas físicas que llevan nuestros productos en el siguiente link https://sisterlynuts.com/puntos-de-venta\n\n";
					setUserParam("step","go-back", job.contact, user.line);
					pushText (msg+GoBackMenu(), job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "En Guayaquil puedes llamar para hacer tu pedido a este numero: Distribuidora SisterlyNuts en Guayaquil 099 177 7668 (no tiene lugar fisico) o hacer click en este link https://wa.me/593991777668  o visitar las tiendas fisicas que llevan nuestros productos en su tienda.\nEn el siguiente link puedes revisar que tienda en Guayquil te quda mas cerca https://sisterlynuts.com/puntos-de-venta\n\n";
					setUserParam("step","go-back", job.contact, user.line);
					pushText (msg+GoBackMenu(), job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = "En Cuenca puedes hacer tu pedido en éste enlace https://wa.me/message/S3FMRDF22JLBC1 o visitar las tiendas fisicas que llevan nuestros productos en el siguiente enlace  https://sisterlynuts.com/puntos-de-venta\n\n";
					setUserParam("step","go-back", job.contact, user.line);
					pushText (msg+GoBackMenu(), job.contact, user.line);
				} else if (["4","4.","cuatro","otro"].indexOf(kw)>=0) {
					var msg = "En Manta puedes llamar para hacer tu pedido a este numero   Distribuidora SisterlyNuts en Manta https://wa.me/message/FOVHTFLT6YEND1  Manta: Plaza del Sol, Vía Barbasquillo, Local 1B Local se llama: Kaws 0 99 177 7668   o visitar las tiendas fisicas que llevan nuestros productos en su tienda En el siguiente link puedes revisar que tienda en MANTA te queda más cerca https://sisterlynuts.com/puntos-de-venta/\n\n";
					setUserParam("step","go-back", job.contact, user.line);
					pushText (msg+GoBackMenu(), job.contact, user.line);
				} else if (["5","5.","cinco"].indexOf(kw)>=0) {
					var msg = "En Loja  puedes visitar las tiendas fisícas que llevan nuestros productos en su tienda en el siguiente enlace https://sisterlynuts.com/puntos-de-venta\n\n";
					setUserParam("step","go-back", job.contact, user.line);
					pushText (msg+GoBackMenu(), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "go-back":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					setUserParam("step","1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					salir(job, user);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
		}//end switch step
		
		function mainMenu(greet) {
			if (greet) {
				var msg = "¡Hola! Bienvenido al canal de atención *SisterlyNuts*. Para ayudarte, selecciona del 1 al 5 entre las siguientes opciones:\n\n";
			} else {
				var msg = "Por favor selecciona del 1 al 5 entre las siguientes opciones:\n\n";
			}
			msg += "1. Hacer un pedido\n";
			msg += "2. Catálogo de productos\n";
			msg += "3. Promociones del mes\n";
			msg += "4. Servicio al cliente\n";
			msg += "5. Donde están ubicados?\n";
			return msg;
		}
		
		function Shop(t) {
		    var msg = "1. Hacer un pedido online\n";
			msg += "2. Hacer un pedido por chat\n";
			msg += "3. Visitar un punto de venta\n";
			msg += "4. Regresar al menú anterior\n";
			return msg;
		}
		
		function Promos() {
			var msg = "Promociones del mes! Selecciona una opción:\n\n";
			msg += "1. 3 mantequillas x $27 edición especial Avellana Almendra y Cacao  (precio unitario normal $10.50) incluye envío en Quito\n\n";
			msg += "2. 3 mantequillas de 200g (puedes elegir dentro de los 4 sabores) $21 incluye envío en Quito\n\n";
			msg += "3. 2 mantequillas de 200g cualquier sabor $15 incluye envío en Quito\n\n";
			msg += "4. Envío gratis si compras dos mantequillas o mínimo 18 USD\n\n";
			return msg;
		}
		
		function GoBackMenu() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Regresar al menú anterior\n";
			msg += "2. Salir";
			return msg;
		}
		
		function About() {
			var msg = "*¿Cómo nace SisterlyNuts?*\n";
			msg += "SisterlyNuts nació en el año 2015 a partir de que dos hermanas apasionadas por la comida saludable buscaban opciones de snacks, que no existían en ese entonces en Quito: Mantequillas Saludables de frutos secos.\n\n";
			msg += "Decidieron hacer sus propias mantequillas en casa y lograron desarrollar productos realmente deliciosos. Sus amigos y familiares los probaron y les encantó, por esta razón, empezaron a venderlas al público en general.\n\n";	
			return msg;
		}
		
		function Tiendas() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Quito\n";
			msg += "2. Guayaquil\n";
			msg += "3. Cuenca\n";
			msg += "4. Manta\n";
			msg += "5. Loja\n";
			return msg;
		}
		
		function Cats() {
			var msg = "Utiliza el siguiente enlace para descargar el catálogo de nuestros productos en PDF:\n";
			msg += "https://sisterlynuts.com/wp-content/uploads/2021/02/Catalogo-Sisterly-Nuts.pdf\n\n";
			msg += "También puedes ver este catálogo en línea: https://sisterlynuts.com/tienda\n\n";
			return msg;
		}
	}
}
