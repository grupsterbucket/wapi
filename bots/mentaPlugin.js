// Menta Bot as module
var qs = require('querystring');
module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing menta sub bot", step, group)
		//only run if not a group chat
		if (job.contact.indexOf("-")<0) {
			switch (step) {			
				case "0":
					var kw = body.toLowerCase()
					setUserParam("step","1", job.contact, user.line);
					pushText (mentaWelcome(true), job.contact, user.line);
					break;
					//end step 0
				case "1":
					var kw = body.toLowerCase()
					var group = ""
					if (["1","1.","uno"].indexOf(kw)>=0) {
						if (isShopOpen()) {
							var msg = "Estamos direccionado tu solicitud, un momento por favor";
						} else {
							var msg = config.horarioStr;
						}
						var ob = {step:"idle", group:"com", status: "active", tso: Date.now()}
						updateUser(ob, job.contact, user.line);
						logStartTicket(job.contact, user.chn);
						pushText (msg, job.contact, user.line);
					} else if (["2","2.","dos"].indexOf(kw)>=0) {
						if (isShopOpen()) {
							var msg = "Estamos direccionado tu solicitud, un momento por favor";
						} else {
							var msg = config.horarioStr;
						}
						var ob = {step:"idle", group:"sac", status: "active", tso: Date.now()}
						updateUser(ob, job.contact, user.line);
						logStartTicket(job.contact, user.chn);
						pushText (msg, job.contact, user.line);
						
					} else if (["3","3.","tres"].indexOf(kw)>=0) {
						var msg = "¡¡Te queremos ayudar lo más pronto posible¡¡ por favor contáctate a los siguientes números:"
						msg += "\n\n";
						msg += "*VEHICULOS:*\n";
						msg += "*AIG Metropolitana*\n";
						msg += "*Telf.* 1800 AIG AIG (1800 244 244)\n";
						msg += "*ASISTENCIA AIG*\n";
						msg += "Movil 0992431655\n";
						msg += "         0988573307\n";
						msg += "\n\n";
						msg += "*VIDA:*\n";
						msg += "*Seguros del Pichincha*\n";
						msg += "*Telf.* 1800 400 400\n";
						msg += "*Whatsapp* 0999667779\n";
						msg += "\n";
						//~ msg += "*BMI*\n";
						//~ msg += "*Telf.* 22941400\n";
						msg += "*Chubb Seguros*\n";
						msg += "*Telf.* 1700 111 999\n";
						msg += "\n\n";
						msg += "*ASISTENCIA MEDICA:*\n";
						msg += "*CONFIAMED*\n";
						msg += "*Telf.* 1800 306 030\n";
						msg += "        1700 303 030\n";
						msg += "*Cel.* 0996007733\n";
						var ob = {step:"idle", group:"sin", status: "active", tso: Date.now()}
						updateUser(ob, job.contact, user.line);
						logStartTicket(job.contact, user.chn);
						pushText (msg, job.contact, user.line);
					} else {
						var msg = "Por favor digita una de las opciones";
						pushText (msg, job.contact, user.line);
					}
					
					break;
				case "2":
					var kw = body.toLowerCase()
					if (["5","5.","cinco","regresar","back"].indexOf(kw)>=0) {
						setUserParam("step","1", job.contact, user.line);
						pushText (mentaWelcome(false), job.contact, user.line);
					} else {
						if (isShopOpen()) {
							var msg = "Estamos direccionado tu solicitud, un momento por favor";
						} else {
							var msg = config.horarioStr;
						}
						updateUser({step:"idle", status: "active", tso: Date.now()}, job.contact, user.line);
						logStartTicket(job.contact, user.chn);
						pushText (msg, job.contact, user.line);
					}
					break;
				
			} //send switch steps
		}//end check if group
		
		function mentaWelcome(greet) {
			var msg = "";
			if (greet) { msg += "¡Hola, bienvenid@ a Menta!\n\n"; }
			msg += "Por favor selecciona una opción:\n\n";
			msg += "1. Tengo dudas para comprar un seguro.\n";
			msg += "2. Quiero que un ejecutivo me contacte.\n";
			msg += "3. Quiero reportar un siniestro.\n";
			return msg;	
			
		}
	}
}
