// Habitat Bot as module
var qs = require('querystring');
module.exports = {
	bot: function (body, job, user, step, group, account) {
		console.log("Processing taec sub bot", step, group, job.contact, "job ID", job.id)
		getContact(job.contact, function (contact) {
			if (contact) {
				//contact exists we open new ticket
				if (isShopOpen()) {
					var msg = "En breve un asesor se contactará contigo"
				} else {
					var msg = config.officeClosedMsg;
				}
				var ob = { step: "idle", status: "active", group: "com", tso: Date.now() }
				updateUser(ob, job.contact, user.line);
				pushText(msg, job.contact, user.line);
				logStartTicket(job.contact, user.chn, user, "com");
			} else {
				switch (step) {
					case "0":
						var ob = { step: "0.1" }
						updateUser(ob, job.contact, user.line);
						pushText(welcomeMessage(true), job.contact, user.line);
						break;
					//end step 0
					case "0.1":
						var ob = { step: "1", "x-apellido": body.substring(0, 255) }
						updateUser(ob, job.contact, user.line);
						pushText("¿Cuál es tu nombre?", job.contact, user.line);
						break;
					//end step 0.1
					case "1":
						var ob = { step: "2", "x-nombre": body.substring(0, 255), name: body.substring(0, 255) }
						updateUser(ob, job.contact, user.line);
						pushText("Gracias! Me ayudas por favor con tu correo electrónico en caso de que se corte la conexión", job.contact, user.line);
						break;
					//end step 1
					case "2":
						//todo: validate email
						if (validateEmail(body.substring(0, 255))) {
							var ob = { step: "3", "x-correo": body.substring(0, 255) }
							updateUser(ob, job.contact, user.line);
							pushText(statusSelect(), job.contact, user.line);
						} else {
							pushText("Por favor escribe un email válido o escribe *salir* para terminar la sesión", job.contact, user.line);
						}
						break;
					//end step 2
					case "3":
						body = body.toLowerCase();
						body = body.replace("nuevo", 1)
						body = body.replace("new", 1)
						body = body.replace("cero km", 1)
						body = body.replace("0 km", 1)
						body = body.replace("0km", 1)
						body = body.replace("usado", 1)
						body = body.replace("used", 2)
						var kw = Number(body.toLowerCase());
						if (!isNaN(kw)) {
							var opts = ["Nuevo", "Usado"]
							if (opts[(kw - 1)]) {
								if (kw == 1) {
									var ob = { step: "4", "x-estado": opts[(kw - 1)] }
									updateUser(ob, job.contact, user.line);
									pushText(typeSelect(), job.contact, user.line);
								} else {
									var ob = { step: "0", "x-estado": opts[(kw - 1)] }
									updateUser(ob, job.contact, user.line);
									pushText("Al momento no disponemos de autos usados, pero mantente atento porque tendremos novedades. Recuerda que en *tuautoencasa.com* " + emoji("house") + " podrás encontrar las mejores marcas de autos *CERO kilómetros* del país " + emoji("ec") + "", job.contact, user.line);
								}
							} else {
								pushText("Por favor digita una de las opciones de la lista o escribe *salir* para terminar la sesión", job.contact, user.line);
							}
						} else {
							pushText("Por favor digita una de las opciones de la lista o escribe *salir* para terminar la sesión", job.contact, user.line);
						}
						break;
					//end step 3
					case "4":
						body = body.toLowerCase();
						var kw = Number(body.toLowerCase());
						if (!isNaN(kw)) {
							var opts = tipoVehs;
							if (opts[(kw - 1)]) {
								var ob = { step: "5", "x-tipo": opts[(kw - 1)] }
								updateUser(ob, job.contact, user.line);
								pushText(PagoSelect(), job.contact, user.line);
							} else {
								pushText("Por favor digita una de las opciones de la lista o escribe *salir* para terminar la sesión", job.contact, user.line);
							}
						} else {
							pushText("Por favor digita una de las opciones de la lista o escribe *salir* para terminar la sesión", job.contact, user.line);
						}
						break;
					//end step 4
					case "5":
						var kw = Number(body.toLowerCase());
						if (!isNaN(kw)) {
							var opts = ["Efectivo", "Crédito"]
							if (opts[(kw - 1)]) {
								//var ob = {step:"6", "x-presupuesto": opts[(kw-1)]}
								if (kw == 1) {
									var ob = { step: "6", "x-pago": opts[(kw - 1)] }
									updateUser(ob, job.contact, user.line);
									pushText(chnSelect(), job.contact, user.line);
								} else {
									var ob = { step: "5.1", "x-pago": opts[(kw - 1)] }
									updateUser(ob, job.contact, user.line);
									pushText('Por favor ayúdame con tu número de cédula y te ofreceremos la mejor opción de financiamiento.', job.contact, user.line);
								}
							} else {
								pushText("Por favor digita una de las opciones de la lista o escribe *salir* para terminar la sesión", job.contact, user.line);
							}
						} else {
							pushText("Por favor digita una de las opciones de la lista o escribe *salir* para terminar la sesión", job.contact, user.line);
						}
						break;
					//end step 5
					case "5.1":
						var ced = body.replace(/\D/g, '') + ""
						if (ced.length == 10) {
							var ob = { step: "6" }
							updateUser(ob, job.contact, user.line);
							pushText(chnSelect(), job.contact, user.line);
						} else {
							pushText(ced + " no parece un número de cédula correcto, Por favor, ingresa tu número de cédula nuevamente", job.contact, user.line, user.chn);
						}
						break
					//end step 5.1
					case "6":
						body = body.toLowerCase();
						body = body.replace("whatsapp", 1)
						body = body.replace("chat", 1)
						body = body.replace("por aqui", 1)
						body = body.replace("por aquí", 1)
						body = body.replace("llamada", 2)
						body = body.replace("tele", 2)
						body = body.replace("teléfono", 2)
						body = body.replace("telefono", 2)
						body = body.replace("llameme", 2)
						var kw = Number(body.toLowerCase());
						if (!isNaN(kw)) {
							var opts = ["Whatsapp", "Teléfono"]
							if (opts[(kw - 1)]) {
								if (isShopOpen()) {
									if (kw == 1) {
										var msg = "En breve un asesor se contactará contigo por este canal " + emoji("chat") + " Recuerda que en *tuautoencasa.com* " + emoji("house") + " podrás encontrar las mejores marcas de autos *CERO kilómetros* del país " + emoji("ec") + ""
									} else {
										var msg = "En breve un asesor se contactará por teléfono " + emoji("phone") + " Recuerda que en *tuautoencasa.com* " + emoji("house") + " podrás encontrar las mejores marcas de autos *CERO kilómetros* del país " + emoji("ec") + ""
									}
								} else {
									var msg = config.officeClosedMsg;
								}


								var ob = { step: "idle", status: "active", group: "com", tso: Date.now(), "x-canal": opts[(kw - 1)] }
								updateUser(ob, job.contact, user.line);
								pushText(msg, job.contact, user.line);
								logStartTicket(job.contact, user.chn, user, "com");

								//~ //asign rep
								//~ getRepRoundRobin(function(repId) {
								//~ if (repId) {
								//~ var ob = {step:"idle", status: "active", group: "com", tso: Date.now(), "x-canal": opts[(kw-1)], tsa: Date.now(), rep: repId}
								//~ logAssignedTicket(job.contact, repId, "com") 
								//~ } else {
								//~ var ob = {step:"idle", status: "active", group: "com", tso: Date.now(), "x-canal": opts[(kw-1)]}
								//~ }
								//~ updateUser(ob, job.contact, user.line);

								//~ pushText (msg, job.contact, user.line);
								//~ logStartTicket(job.contact, user.chn, user, "com");
								//~ });

							} else {
								pushText("Por favor digita una de las opciones de la lista o escribe *salir* para terminar la sesión", job.contact, user.line);
							}
						} else {
							pushText("Por favor digita una de las opciones de la lista o escribe *salir* para terminar la sesión", job.contact, user.line);
						}
						break;
					//end step 6
				}//end switch step
			} //end check if contact exists
		});
	}
}

function welcomeMessage(greet) {
	var msg = ""
	if (greet) {
		msg += "Gracias por contactarte con *tuautoencasa.com* 🏠\nSomos el portal web donde podrás encontrar las mejores marcas de autos *CERO kilómetros* del país 🇪🇨\nTenemos el vehículo que estás buscando 🔎 pero con beneficios exclusivos.\nAl proporcionarnos voluntariamente tus datos personales aceptas su uso de acuerdo con nuestra Política de privacidad 👉🏼 https://tuautoencasa.com/politicas-condiciones\n\n";
	}
	msg += "Antes de empezar, ¿Cuál es tu apellido?";
	return msg;
}

function statusSelect() {
	var msg = ""
	msg += "¿Qué tipo de auto estás buscando? Escribe el número de la opción que quieres para continuar.\n\n";
	msg += emoji("1") + " Nuevo\n";
	msg += emoji("2") + " Usado";
	return msg;
}

function typeSelect() {
	var msg = ""
	msg += "¿Qué tipo de vehículo " + emoji("car") + " *0 kilómetros* estás buscando? Escribe el número de la opción que quieres para continuar.\n\n";
	for (var i = 0; i < tipoVehs.length; i++) {
		msg += emoji((i + 1) + "") + " " + tipoVehs[i] + "\n";
	}
	return msg;
}


function budgetSelect() {
	var msg = ""
	msg += "Listo, cuál es tu  presupuesto a invertir? Escribe el número de la opción que quieres para continuar.\n\n";
	msg += emoji("1") + " $15mil a $20mil\n";
	msg += emoji("2") + " $21mil a $25mil\n";
	msg += emoji("3") + " $26mil a $35mil\n";
	msg += emoji("4") + " $35mil en adelante";
	return msg;
}

function PagoSelect() {
	var msg = ""
	msg += "Con gusto, cuál sería tu forma de pago? Escribe el número de la opción que quieres para continuar.\n\n";
	msg += emoji("1") + " Efectivo\n";
	msg += emoji("2") + " Crédito\n";
	return msg;
}

function chnSelect() {
	var msg = ""
	msg += "¡Excelente!  Escoge el canal que prefieres para que nuestro asesor se contacte contigo. Escribe el número de la opción que quieres para continuar. \n\n";
	msg += emoji("1") + " Whatsapp\n";
	msg += emoji("2") + " Llamada";
	return msg;
}

function validateEmail(email) {
	var re = /\S+@\S+\.\S+/;
	return re.test(email);
}

function emoji(key) {
	opts = {
		"car": "🚗",
		"house": "🏠",
		"search": "🔎",
		"right": "👉🏼",
		"down": "👇🏼",
		"ec": "🇪🇨",
		"1": "1️⃣",
		"2": "2️⃣",
		"3": "3️⃣",
		"4": "4️⃣",
		"5": "5️⃣",
		"6": "6️⃣",
		"7": "7️⃣",
		"8": "8️⃣",
		"9": "9️⃣",
		"10": "🔟",
		"phone": "☎️",
		"chat": "💬"
	}
	if (opts[key]) {
		return opts[key];
	} else {
		return key;
	}
}






