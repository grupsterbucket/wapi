var tools = require('./tools');
var http = require("http");
var https = require("https");
var fs = require('fs');
var qs = require('querystring');
var _ = require('underscore');
var request = require('request');
var HttpCors = require('http-cors');
var cors = new HttpCors();
var accountOb = {}
var mtq = {}
var booted = false;
var ssl = false;
var tokens = []
var assetsCache = {}
var messagebird = {}
var tries = {}

var mtITV = 10; //Interval to post MT to MB

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs

//global scope to access from modules
global.lines = [];
global.firebase = require('firebase');

//read port and account from command line
var port = 9301 //default
var account = "demo" //default
if (process.argv[2]) {
	var portNumber = parseInt(process.argv[2]);
	if (portNumber >= 9000 && portNumber <= 9999) { port = portNumber; }
}

if (process.argv[3]) {
	account = process.argv[3];	
}

if (process.argv[4]) {
	if (process.argv[4] == "sslon") {
		ssl = true;	
	}	
}

//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            boot();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function boot() {
  if (!booted) {
    booted = true;
    //load lines
    loadAccount(function (ob) {
		if (ob) {
			accountOb = JSON.parse( JSON.stringify(ob) );
			//init MB SDK
			messagebird = require('messagebird')(accountOb.creds.mb.apiKey);
			messagebird.balance.read(function (err, response) {
				if (err) {
					return console.log(err);
				}
				console.log ("Message Bird Balance", response.amount)
				loadLines(function () {
					  //start API
					  if (!ssl) {
						http.createServer(app).listen(port);
						console.log("MBRIDGE HTTP Server started on port", port)
					  } else {
						var httpsOptions = {
							cert: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/fullchain.pem'),
							ca: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/chain.pem'),
							key: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/privkey.pem'),
							requestCert: false,
							rejectUnauthorized: false
						};

						https.createServer(httpsOptions, app).listen(port);
						console.log("MBRIDGE HTTPS Server started on port", port)
					  }
					  //start mt worker
					  setTimeout(function() { 
						  for (var i=0; i<lines.length; i++) {
							mtWorker(lines[i].id);
						  }
					  },2000)
				});
			});
			
		} else {
			console.log ("ERROR",account,"account not found")	
		}
	});
  }
}

function loadAccount(cb) {
	firebase.database().ref('/accounts/'+account+'/config').once('value', function(snapshot) {
		if (snapshot.val()) {
			cb( JSON.parse( JSON.stringify(snapshot.val()) ) );	
		} else {
			cb(null);	
		}
	});
}
function loadLines(cb) {
	firebase.database().ref('/lines/').orderByChild("account").equalTo(account).once('value', function(snapshot) {		
		var arr = _.map (snapshot.val(), function(value, key) {
			value.id = key;
			return value;
		});
		for (var i=0; i<arr.length; i++) {
			if (arr[i].isWAPIMB) {
				lines.push( arr[i] );
				tokens.push(arr[i].apiToken);
				MBHookSetup(arr[i]);
			}
		}
		console.log (lines.length + " line(s) loaded for "+account);
		//initializq mtq arrays
		for (var i=0; i<lines.length; i++) {
			mtq[lines[i].id] = [];
		}
		//load mtq
		loadMTQ(account);
		if (cb) { cb(); }
	});
}

function MBHookSetup(line) {
	//setup MB hooks
	var paramsmb = {
	  'events': [
	  'conversation.created',
	  'conversation.updated',
	  'message.created',
	  'message.updated'
	  ],
	  'channelId': line.mbID,
	  'url': 'https://textcenter.net:'+port+'/mbhook'
	}
	messagebird.conversations.webhooks.create(paramsmb, function (err, response) {
	  if (err) {
	  return console.log(err);
	  }
	  console.log(response);
	  messagebird.conversations.webhooks.list(100, 0, function (err, response) {
		  if (err) {
		  return console.log(err);
		  }
		  console.log(response);
		});
	});
}

function loadMTQ(account) {
	firebase.database().ref("/accounts/"+account+"/mtq/").orderByChild("st").equalTo("qed").on('child_added', function(res) {
		var msg = res.val();
		msg.id = res.key;
		if (mtq[msg.line]) {			
			var door = Date.now() - accountOb.misc.purgeAge;
			if (msg.ts>door) {					
					mtq[msg.line].push(msg);
					console.log (new Date(), "NEW MT FOR", account, msg.line, mtq[msg.line].length);					
			} else {
				console.log (new Date(), "Purge", msg.id)	
				purgeMT(msg);
			}
		} else {
		    console.log (new Date(), "Line NOT VALID", msg);
		    purgeMT(msg);
		}
	});
}

function mtWorker(lineId) {
	
	if (mtq[lineId]) {
		if (mtq[lineId].length>0) {
			console.log (new Date(), "Run MT Worker for", lineId, " l =", mtq[lineId].length	)	
			var mt = mtq[lineId].shift();
			postToMB(mt, function(res) {
				console.log (res)
				if (res.status == "error") {
					if (!tries[mt.id]) { tries[mt.id] = 0; }
					tries[mt.id] += 1;
					if (tries[mt.id] >=10) {
						//Update mtq
						firebase.database().ref("/accounts/"+mt.account+"/mtq/"+mt.id).update({ line: lineId, lu: Date.now(), st: 'err', mid: "na" }).then(function() {
							console.log ("ERR Sent to MB");
						}).catch(function(error) {
							console.log(new Date(), "Error updating MTQ", error);
						});
						setTimeout (function() { mtWorker(lineId) }, mtITV);
					} else {
						//requeue wait and call again
						console.log ("Retry MT", mt.id, tries[mt.id]);
						mtq[lineId].push(mt);
						setTimeout (function() { mtWorker(lineId) }, 5000)
					}
				} else {
					//Update mtq
					firebase.database().ref("/accounts/"+mt.account+"/mtq/"+mt.id).update({ line: lineId, lu: Date.now(), st: 'snt', mid: res.id }).then(function() {
						console.log ("MT Sent to MB");
					}).catch(function(error) {
						console.log(new Date(), "Error updating MTQ", error);
					});
					//save ACK Ref
					var ack = { account: mt.account, contact:mt.to , msgId: mt.id }
					firebase.database().ref("/ack/"+res.id).set(ack, function() {
						console.log (new Date(), "ACK Ref saved");
					}).catch(function(error) {
						console.log(new Date(), "Error saving ACK", error);
					});
					setTimeout (function() { mtWorker(lineId) }, mtITV);
				}
			})
		} else {
			setTimeout (function() { mtWorker(lineId) }, mtITV);
		}
	} else {
		setTimeout (function() { mtWorker(lineId) }, mtITV);
	}
}

function postToMB(mt, cb) {
	var lineOb = _.findWhere( lines, {id: mt.line} );
	if (lineOb) {
		if (mt.isTemplate) {
			var arr = []
			if (mt.params.length>0) {
				if (mt.params.indexOf("|")>=0) {
					var params = mt.params.split("|");
					for  (var i=0; i<params.length; i++) {
						arr.push({ 'default': params[i] })	
					}
				} else {
					arr.push({ 'default': mt.params })	
				}	
			}
			var chn = "wsp";
			if (mt.chn) { chn = mt.chn }
			var sendTo = mt.to
			if ( chn == "wsp") { sendTo = '+'+sendTo }
			var params = { 
			  'to': sendTo,
			  'from': lineOb.mbID,
			  'reportUrl': 'https://textcenter.net:'+port+'/mback',
			  'type': 'hsm',
			  content: {
			  'hsm': {
				'namespace': lineOb.namespace,
				'templateName': mt.templateName,
				'language': {
				  'policy': 'deterministic',
				  'code': mt.lang
				},
				'params': arr
			  }
			  }
			};
			console.log (JSON.stringify(params))
			messagebird.conversations.send(params, function (err, response) {
			  if (err) {
				console.log(err);
				cb ({status: 'error'});
			  } else {
				  console.log ("Template Posted to MB")
				cb (response)
			  }
			});
		} else {
			var chn = "wsp";
			if (mt.chn) { chn = mt.chn }
			var sendTo = mt.to
			if ( chn == "wsp") { sendTo = '+'+sendTo }
			var params = { 
			  'to': sendTo,
			  'from': lineOb.mbID,
			  'reportUrl': 'https://textcenter.net:'+port+'/mback',
			  'content': {}
			};
			switch (mt.type) {
				case "txt":
					params.type = "text";
					params.content.text = mt.m;
					break;
				case "btn":
					params.type = "interactive";
					params.content.interactive = mt.buttons;
					break;
				case "img":
					params.type = "image";
					var caption = mt.url;
					if (mt.m) { caption = mt.m }
					params.content.image = { url: mt.url, caption: caption}
					if (mt.desc) {params.content.image.caption += "\n" + mt.desc}
					break;
				case "doc":
					params.type = "file";
					var caption = mt.url;
					if (mt.m) { caption = mt.m }
					params.content.file = { url: mt.url, caption: caption}
					break;
				default:
					params.type = "text";
					params.content.text = "[mt "+mt.type+"]";
			}
			console.log (params)
			messagebird.conversations.send(params, function (err, response) {
				if (err) {
					console.log(err);
					cb ({status: 'error'});
				} else {
					console.log ("MT Posted to MB")
					cb (response)
				}
			});
		}
	} else {
		cb ({status: 'error'});
	}
}

function runOp(post) {
	var line = post.line;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
		switch (post.op) {
			case "reset-bot":
				console.log (new Date(), "Run OP OK", post.op, post.contact);
				var job = {
				   type: post.op,
				   contact: post.contact,
				   st: 'new',
				   ts: Date.now()
				   }
			    firebase.database().ref("/accounts/"+lineOb.account+"/botJobs/").push(job);
				break;
			case "finish-order":
				console.log (new Date(), "Run OP OK", post.op, post.contact);
				var job = {
				   type: post.op,
				   contact: post.contact,
				   st: 'new',
				   ts: Date.now()
				   }
			    firebase.database().ref("/accounts/"+lineOb.account+"/botJobs/").push(job);
				break;
			case "payment-success":
				console.log (new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.contact,
					st: 'new',
					ts: Date.now()
					}
				firebase.database().ref("/accounts/"+lineOb.account+"/botJobs/").push(job);
				break;
			case "payment-error":
				console.log (new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.contact,
					st: 'new',
					ts: Date.now()
					}
				firebase.database().ref("/accounts/"+lineOb.account+"/botJobs/").push(job);
				break;
			default:
				console.log (new Date(), "Unknown OP", post.op, post.contact);
		}
	} else {
		console.log (new Date(), "Error running op line not found", line);
	}
}

function pushMT(post) {
	
	var type = 'txt';
	if ( post.type ) { type = post.type }
	var rep = 'wapi';
	if ( post.rep ) { rep = post.rep }
	
	var line = post.line;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
		var msgId = Date.now()+"-"+line+"-"+post.to;
		var chn = "wsp"
		if (post.chn) { chn = post.chn}
		var mt = { g:0, chn: chn, m:post.body, to: post.to, rep: rep, t:"mt", ts: Date.now(), type: type, st: 'qed', lu: Date.now()};
		if (post.desc) { mt.desc = post.desc; }
		if (post.url) { mt.url = post.url; }
		if (post.thumb) { mt.thumb = post.thumb; }
		if (post.cid) {mt.cid = post.cid; }
		if (chn == "wsp" && post.isTemplate) {
			mt.isTemplate = post.isTemplate;	
			mt.lang = post.lang;
			mt.params = post.params;
			mt.templateName = post.templateName;
		}
		firebase.database().ref("/accounts/"+lineOb.account+"/conves/"+post.to+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = post.to;
			ob.account = lineOb.account;
			//push to account MT queue
			firebase.database().ref("/accounts/"+lineOb.account+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT POSTED OK TO MTQ");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Save MT to chat Error", error);
		});
	} else {
		console.log (new Date(), "Error pushing line not found", line);
	}
}

function processMO( post ) {
	console.log (new Date(), "Processing MO", post.message.type);
	var msg = post.message;
	var line = msg.to.replace("+","");
	var chat = msg.chat;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
	
		//build mo from type
		switch (msg.type) {
			case 'text':
				var mo = { g:3, m: msg.content.text, rep: 'wapi', t:"mo", ts: Date.now(), type: 'txt', cid: post.conversation.id };
				break;
			case 'image':
				var strBody = "[imagen]";
				if (msg.content.image.caption) { strBody = msg.content.image.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'img', url: msg.content.image.url , cid: post.conversation.id};
				break;
			case 'file':
				var strBody = "[document]";
				if (msg.content.file.caption) { strBody = msg.content.file.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'doc', url: msg.content.file.url , cid: post.conversation.id};
				break;
			case 'location':
				var mo = { g:3, m:'[location]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'loc', lng: msg.content.location.longitude, lat: msg.content.location.latitude, cid: post.conversation.id};
				break;
			case 'video':
				var strBody = "[video]";
				if (msg.content.video.caption) { strBody = msg.content.video.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'vid', url: msg.content.video.url , cid: post.conversation.id};
				break;
			case 'audio':
				var mo = { g:3, m: '[audio]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'aud', url: msg.content.audio.url , cid: post.conversation.id};
				break;
			default:
				var mo = { g:3, m: msg.type, rep: 'wapi', t:"mo", ts: Date.now(), type: msg.type, cid: post.conversation.id, dets: msg};
		}
		var contact = post.contact;
		var senderId = msg.from.replace("+","");
		var name = senderId;
		if (contact.displayName) {
			name = contact.displayName;	
		}
		var chn = "wsp";
		if (lineOb.chn) { chn = lineOb.chn }
		var user = { chn: chn, id: senderId, line: line, nick: name, lu: Date.now(), cid: post.conversation.id}
		
		//if chat is from FB messenger, number is sender ID
		if (chn == "fm") { contact.msisdn = senderId}

		//create or update user
		firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+senderId).update(user).then(function() {
			firebase.database().ref("/accounts/"+lineOb.account+"/conves/"+senderId+"/msgs/"+msg.id).set(mo).then(function() {
				//run bot
				if (lineOb.hasBot) {

				   delete contact.attributes;
				   delete contact.customDetails;
				   var job = {
					   msg: msg,
					   contact: contact,
					   st: 'new',
					   ts: Date.now()
					   }
				   firebase.database().ref("/accounts/"+lineOb.account+"/botJobs/"+msg.id).set(job);

				} else {
				   firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+senderId+"/tso").once("value", function(uat) {
					   var tso = uat.val();				   
					   //start ticket					   
					   if (tso) {
						   //ticket already started
					   } else {
						   firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+senderId).update({tso: Date.now(), status: "active"});
						   console.log (new Date(), "Ticket started");
						   logStartTicket(lineOb.account, senderId, 'wsp')
					   }
				   });
				}

				firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+senderId+"/wsc").once("value", function(uat) {
					   var wsc = uat.val();	
					   //reset whastapp 24h clock
					   if (wsc) {
						   //has 24h passed?
						   if ( (Date.now() - wsc) > (24*60*60*1000) ) {
							   firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+senderId).update({wsc: Date.now()});
						   } 
					   } else {
						   firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+senderId).update({wsc: Date.now()});
					   }
				});
				console.log (new Date(), "MO Processed OK");
			}).catch(function(error) {
			   console.log (new Date(), "Error posting MO to Firebase", line);
			   console.log (error)
			   console.log (mo)
			   //TODO: persist this msg in local file to resend MOs
			});
		}).catch(function(error) {
			console.log (new Date(), "Error posting User to Firebase", line);
			console.log (error)
			console.log (sender)
			console.log (user)
			//TODO: persist this msg in local file to resend MOs
		});
	} else {
		console.log (new Date(), "Error processing MO line not found", line);
	}

}

function logStartTicket(account, contact, chn) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

function processACK(post) {
	var ob = {g: 0, lu: Date.now()}
	switch (post.message.status) {
		case "sent":
			ob.g = 1
			break;
		case "delivered":
			ob.g = 2
			break;
		case "read":
			ob.g = 3
			break;
		default:
			ob.g = post.message.status
			if (post.error) {
				if (post.error.description) {
					ob.err = post.error.description;
				}	
			}
	}
	if (post.message.id) {
		//get ack ref
		firebase.database().ref("/ack/"+post.message.id).once('value', function(res) {
			if (res.val()){
				var ref = res.val();
				firebase.database().ref("/accounts/"+ref.account+"/conves/"+ref.contact+"/msgs/"+ref.msgId).update(ob).then(function() {
					console.log (new Date(), "ACK updated for", post.message.id);
				});
			} else {
				console.log (new Date(), "ACK not found for", post.message.id);
			}
		});
		
	}
	
}

function purgeMT(msg) {
	var lineOb = _.findWhere( lines, {id: msg.line} );
	if (lineOb) {
			//set mtq status to purged
			firebase.database().ref("/accounts/"+lineOb.account+"/mtq/"+msg.id).update({ lu: Date.now(), st: 'pur' })
	} else {
		console.log (new Date(), "Error purgeMT line not found", msg.line);
	}
}

function processPost(req, res, callback) {
	var queryData = "";
	if(typeof callback !== 'function') return null;

	req.on('data', function(data) {
		queryData += data;
		if(queryData.length > 10000000) {
			queryData = "";
			res.writeHead(413, {'Content-Type': 'text/plain'}).end();
			req.connection.destroy();
		}
	});

	req.on('end', function() {
		if (req.method == 'GET') {
			req.post = qs.parse(req.url.replace(/^.*\?/, ''));
		} else {
			req.post = queryData;
		}
		callback();
	});
}


var app = function(req, res) {

     if(cors.apply(req, res)) {
		 res.end();
	 }

	 processPost(req, res, function() {
		console.log ("URL", req.url);
		var parts = req.url.split("/");
		//router
        switch (parts[1]) {
			case 'send':
                //process MT received from API call
				   console.log (new Date(), "MT received");
				   if (req.method == "POST") {
	
						if (typeof req.post === "string") {
							var post = _.clone ( qs.parse(req.post)	);
						} else {
							var post = _.clone ( req.post );
						}
						if (tokens.indexOf(post.apiToken)>=0 ) {
							var numberCheck = tools.numberFormat(post.to, post.isResponse)
							if (numberCheck.valid) {
								post.to = numberCheck.to;
								if (post.body) {
									if (post.body.length > 0) {
										pushMT (post)
										res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
										res.write("mt-pushed-ok");
										res.end();
									} else {
										res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
										res.write("invalid-message");
										res.end();
									}
								} else {
									res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
									res.write("invalid-message");
									res.end();
								}
							} else {
								res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
								res.write("invalid-number");
								res.end();
							}
						} else {
							res.writeHead(401, "ERROR", {'Content-Type': 'text/plain'});
							res.write("invalid-token");
							res.end();
						}

				   } else {
					   res.writeHead(405, {'Content-Type': 'text/plain'});
					   res.write("Invalid request method");
					   res.end();
				   }

                break;
            case 'run-op':
                
				   console.log (new Date(), "OP received");
				   if (req.method == "POST") {
	
						if (typeof req.post === "string") {
							var post = _.clone ( qs.parse(req.post)	);
						} else {
							var post = _.clone ( req.post );
						}
						if (tokens.indexOf(post.apiToken)>=0 ) {
							runOp(post);
							res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
							res.write("run-op-ok");
							res.end();
							
						} else {
							res.writeHead(401, "ERROR", {'Content-Type': 'text/plain'});
							res.write("invalid-token");
							res.end();
						}

				   } else {
					   res.writeHead(405, {'Content-Type': 'text/plain'});
					   res.write("Invalid request method");
					   res.end();
				   }

                break;
            case 'mbhook':
				//console.log (new Date(), "MB Hook received");
			    var post = JSON.parse(req.post);
			    //console.log (JSON.stringify(post))
			    switch (post.type) {
					case "message.created":
						console.log (req.post);
						if (post.message.direction == "received") {
							//check if super fast ping
							if (post.message.type == "text") {
								if (post.message.content.text == "#fast") {
									var mt = {
										line: post.message.to.replace("+",""),
										isTemplate: false,
										to:  post.message.from.replace("+",""),
										type: 'txt',
										m: '#fast ' + new Date()
									}
									postToMB(mt, function(){});
									
								} else {
									processMO(post)	
								}
							} else {
								processMO(post)	
							}
						}
						break;
					default:
						//console.log ("Cannot process hook", post.type);  
				}
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("hook-ok");
			    res.end();
				break;
			case 'mback':
				//console.log (new Date(), "MB ACK received");
			    var post = JSON.parse(req.post);
				switch (post.type) {
					case "message.status":
						console.log (req.post);
						if (post.message.id && post.message.status) {
							processACK(post);
						} else {
							//console.log ("This is MT", post.message.direction);	
						}
						break;
					default:
						//console.log ("Cannot process ack",post.type);  
				}
			
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("ack-ok");
			    res.end();
				break;
            
            case "ping":
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("pong");
				res.end();
				break;

            default:
			   console.log(new Date(), "unknown path "+req.url);
			   res.writeHead(404, {'Content-Type': 'text/plain'});
			   res.write("unknown path "+req.url);
			   res.end();
        }
    });
}
