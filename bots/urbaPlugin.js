// Bot as module
var qs = require('querystring');
module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing up sub bot", step, group)
		switch (step) {
			case "u0":
				main.setUserParam("step","u1", job.contact, user.line);
				main.pushText (mainMenuUP(true), job.contact, user.line);
				break;
				//end step 0
			case "u1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","urbapark","pass"].indexOf(kw)>=0) {
					var msg = "Urbapark Pass es una tarjeta de movilidad inteligente que te permite utilizar toda la red de estacionamientos Urbapark en el país, sin tomar ticket, ni pasar por caja.\n\n";
					msg += "Para obtener tu tarjeta UrbaPark Pass, es necesario crear una cuenta en el siguiente enlace: https://www.urbaparkpass.com/urbaweb/registro\n\n";
					msg += "1. Coordina la entrega de tu tarjeta\n";
					msg += "2. Regresar al Menú principal."
					main.setUserParam("step","u1.1", job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos","asistencia","ayuda"].indexOf(kw)>=0) {
					main.setUserParam("step","u1.2", job.contact, user.line);
					main.pushText (asistenciaMenuUP(), job.contact, user.line);
				} else if (["3","3.","tres","puntos"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "pnt", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
					//~ var msg = "[imagen puntos]";
					//~ main.pushText (msg + pageBreak() + mainMenuUP(false), job.contact, user.line);
				} else if (["4","4.","cuatro","otro"].indexOf(kw)>=0) {
					var msg = "Por favor, selecciona del 1 al 3 entre las siguientes opciones:\n\n"
					msg += "1. Mensualidades\n";
					msg += "2. Valet Parking\n"
					msg += "3. Hablar con un especialista"
					main.setUserParam("step","u1.4", job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["5","5.","cinco","experiencia"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "exp", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
			case "u1.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "ent", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos","usuario"].indexOf(kw)>=0) {
					main.setUserParam("step","u1", job.contact, user.line);
					main.pushText (mainMenuUP(false), job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
			case "u1.2":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","contraseña","recupera","clave","password"].indexOf(kw)>=0) {
					var msg = "Para recuperar tu contraseña haz clic en el siguiente enlace de recuperación: https://www.urbaparkpass.com/urbaweb/resetPwd\n\n";
					msg += "Ingresa tu correo electrónico y haz clic en el botón “Restablecer”. Las instrucciones llegaran a tu correo electrónico, no olvides revisar en correos no deseados o spam.\n\n"
					msg += "1. Regresar al menú anterior";
					main.setUserParam("step","u1.2.1", job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos","usuario"].indexOf(kw)>=0) {
					var msg = "Tu usuario es el correo electrónico que ingresaste al momento de generar tu cuenta. Si no recuerdas tu correo, selecciona la opción 1 y uno de nuestros especialistas te asistirá\n\n";
					msg += "1. Hablar con un especialista.\n";
					msg += "2. Regresar al menú anterior";
					main.setUserParam("step","u1.2.2", job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres","movimientos"].indexOf(kw)>=0) {
					var msg = "Para revisar tus movimientos debes ingresar a https://www.urbaparkpass.com/urbaweb/login con tu usuario y contraseña.\n\n";
					msg += "Una vez dentro haz clic en el botón “Tarjeta UrbaPark Pass” y luego en el botón “Transacciones”, de inmediato se desplegara la información con todos tus movimientos.\n\n";
					msg += "1. Regresar al menú anterior";
					main.setUserParam("step","u1.2.3", job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["4","4.","cuatro","tarjeta"].indexOf(kw)>=0) {
					var msg = "Si tu tarjeta de crédito se encuentra inactiva o caducada no podrás usar el servicio. Para solucionarlo debes ingresar a tu perfil de usuario y actualizar los datos de tu tarjeta en el siguiente enlace: https://www.urbaparkpass.com/urbaweb/login\n";
					msg += "O puedes seleccionar entre las siguientes opciones:\n\n"
					msg += "1. Hablar con un especialista\n";
					msg += "2. Regresar al menú anterior";
					main.setUserParam("step","u1.2.4", job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["5","5.","cinco","menu"].indexOf(kw)>=0) {
					main.setUserParam("step","u1", job.contact, user.line);
					main.pushText (mainMenuUP(false), job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
		    case "u1.2.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					main.setUserParam("step","u1.2", job.contact, user.line);
					main.pushText (asistenciaMenuUP(), job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
			 case "u1.2.2":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "usr", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					main.setUserParam("step","u1.2", job.contact, user.line);
					main.pushText (asistenciaMenuUP(), job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
			 case "u1.2.3":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					main.setUserParam("step","u1.2", job.contact, user.line);
					main.pushText (asistenciaMenuUP(), job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
			case "u1.2.4":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "tc", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					main.setUserParam("step","u1.2", job.contact, user.line);
					main.pushText (asistenciaMenuUP(), job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;	
			case "u1.4":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","mensual"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "men", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos","valet"].indexOf(kw)>=0) {
					var msg = "1. Coordina una reserva de Valet Parking\n";
					msg += "2. Coordina el servicio de Valet Parking para tu evento";
					main.setUserParam("step","u1.4.1", job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "ser", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
			case "u1.4.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","mensual"].indexOf(kw)>=0) {
					var msg = "Puedes coordinar tu valet en uno de los siguientes estacionamientos:\n\n";
					msg += "1. Hospital Metropolitano\n";
					msg += "2. Restaurantes";
					main.setUserParam("step","u1.4.1.1", job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos","valet"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "eve", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
			case "u1.4.1.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "vmt", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "vrs", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else {											
					main.pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
			case "u2":
				var kw = body.toLowerCase()
				if (kw == "salir" || kw == "*salir*") {
					var str = "Gracias por usar este servicio. Adios!"
					main.pushText (str, job.contact, user.line);
					main.resetUserParam("step", job.contact, user.line);
					main.resetUserParam("status", job.contact, user.line);
					main.resetUserParam("pin", job.contact, user.line);
				} else {
					main.pushText ("Por favor completa tu pedido en nuestro app o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
		}//end switch step
		
		function mainMenuUP(greet) {
			if (greet) {
				var msg = "¡Hola! Bienvenido al canal de atención Urbapark. Para ayudarte, selecciona del 1 al 5 entre las siguientes opciones:\n\n";
			} else {
				var msg = "Por favor selecciona del 1 al 5 entre las siguientes opciones:\n\n";
			}
			msg += "1. Adquiere tu tarjeta UrbaPark Pass\n";
			msg += "2. Asistencia con tu tarjeta UrbaPark Pass\n";
			msg += "3. Conoce nuestra red: Puntos, Tarifas y Horarios\n";
			msg += "4. Otros servicios\n";
			msg += "5. Cuéntanos tu experiencia";
			return msg;
		}
		
		function asistenciaMenuUP() {
			var msg = "Por favor selecciona del 1 al 5 entre las siguientes opciones:\n\n";
			msg += "1. Recupera tu contraseña\n";
			msg += "2. Recupera tu nombre de usuario\n";
			msg += "3. Revisa tus movimientos Urbapark Pass\n";
			msg += "4. Problemas con tu tarjeta de crédito\n";
			msg += "5. Regresar al menú principal";
			return msg;
		}
	}
}
