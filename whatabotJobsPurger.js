var fs = require('fs');
var path = require('path');
var _ = require('underscore');
var firebase = require('firebase');
var accounts = {};
var booted = false;
var os = require('os');
var accsq = [
"andy",
"baninter",
"demo",
"demo2",
"gigo",
"hornero",
"hughes",
"moderna",
"nexxit",
"nova",
"ricki",
"skyluft",
"terrasol",
"tventas",
"udla",
"udla-bulk",
"unicef",
"wapi",
"wapi-bulk"]

var accs = ["hornero"]

//firebase init
var firebaseConfig = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };
firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function bootApp() {
	if (!booted) {
		booted = true;
		//load api accounts 
		loadAccounts();
		
	}	
}

function loadAccounts() {
	var self = this;
	var arr = accs
	for (var i=0; i<arr.length; i++) {
		if (accounts[arr[i]]) {
			console.log ("Listeners already started for", arr[i])	
		} else {
			if (fs.existsSync(path.resolve(__dirname, './dumps/jobs2/'+arr[i]))) {
				console.log ("Directory",path.resolve(__dirname, './dumps/jobs2/'+arr[i]),"exists")
			} else {
				fs.mkdirSync(path.resolve(__dirname, './dumps/jobs2/'+arr[i]));
				console.log ("Directory",path.resolve(__dirname, './dumps/jobs2/'+arr[i]),"created")
			}
			accounts[arr[i]] = {started: true, q: []}	
			listenToJobs(arr[i]);
			console.log ("Listeners started for", arr[i])
			//start worker
			startWorker(arr[i]);
		}
	}

}

function startWorker(account) {
	setTimeout(function() {
		worker(account);
		console.log("Worker started for",account)
	}, 2000)	
}
function listenToJobs(account) {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("done").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		console.log (new Date(), "New "+account+" job queued", accounts[account].q.length);
		accounts[account].q.push(job)
	});
}

function worker(account) {
	if (accounts[account].q.length<=0) {
		setTimeout(function() {
			worker(account);
		}, 10000)	
	} else {
		console.log ("Processing", account, accounts[account].q.length)
		processJob(accounts[account].q.shift(), account);
	}	
}
function processJob(job, account) {
	dumpJSON('./dumps/jobs2/'+account+'/'+job.id+'.json', job, function(dumped){
		if (dumped) {
			firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id).remove();	
			setTimeout(function() {worker(account)}, 10)
		}	
	});	
}

function dumpJSON(url, ob, cb) {
	fs.writeFile(path.resolve(__dirname, url), JSON.stringify(ob), 'utf8', function(err) {
		if (!err) {
			cb(true);
		} else {
			console.log (err);
			process.exit(0)
			cb(false)
		}	
	});
}


