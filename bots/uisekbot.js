var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "uisek";
var booted = false;
var jobs = []
var config = {}

//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		loadAccountConfig();
		listenToJobs();
		processJobQueue();
	}	
}



function loadAccountConfig() {
	firebase.database().ref("/accounts/"+client+"/config/").on('value', function(res) {
		config = res.val()
		console.log (new Date(), "Config loaded");
	});	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+client+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var group = "main";
		if (user.step) {step = user.step};
		if (user.group) {group = user.group};
		
		console.log ("User "+job.contact+" is at step:", step, "group:", group);
		

		//check if user has a campusers entry
		firebase.database().ref("/backroute/").orderByChild("to").equalTo(job.contact).once('value', function(results) {
			var campId = "0"
			var clienteFinal = "na"
			if (results.val()) {
				var arr = _.map(results.val(), function(val, key){
					val.id = key
					return val
					})
				arr = _.sortBy(arr, function(ob) { return -ob.tsc });
				var lastCamp = arr[0];
				//get last sent	camp ID
				campId = lastCamp.campId;
				clienteFinal = lastCamp.clienteFinal;
			} 

			console.log ("Process MO ", job.msg.type);
			setUserParam("step","idle", job.contact, user.line);
			setUserParam("status","active", job.contact, user.line);
			setUserParam("group", "main", job.contact, user.line); 
			if (campId) {
				setUserParam("x-camp", campId, job.contact, user.line);
			} else {
				setUserParam("x-camp", "0", job.contact, user.line);
			}
			//send autoMsg if shop is closed and is step 0
			if (step == "0" && !isShopOpen()) {
				console.log ("Shop is closed, user is step 0, send autoresponse")
				pushText(config.autoMsg, job.contact, job.line)
			}
			finishJob(job);							

		}); //end campusers check

	});
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(1000));
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		console.log (new Date(), "Job completed");
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}

function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).set(val);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).remove();
}

function isShopOpen () {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	var dow = d.getDay()
	if (config.schedule[dow].isOpen) {
		if ( t >= config.schedule[dow].hourStart && t < config.schedule[dow].hourEnd ) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;	
	}
}



