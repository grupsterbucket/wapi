const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
var firebase = require('firebase');
var moment = require('moment');
var fs = require('fs');
var path = require('path');
var envConfig = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
var config = envConfig[envConfig.env];
var size = [];

//firebase init
var firebaseConfig = {
  "apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
  "authDomain": "ubiku3.firebaseapp.com",
  "databaseURL": "https://ubiku3.firebaseio.com",
  "projectId": "ubiku3",
  "storageBucket": "ubiku3.appspot.com",
  "messagingSenderId": "946813430025",
  "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(function(user) {

  if (user) {
    // User is signed in.
    console.log("Logged in to firebase as uid " + user.uid, "\n");
    promptValues();
  } else {
    // User is signed out login again
    console.log("Session expired");
    loginTofirebase();
  }
  // ...
});

function loginTofirebase() {
  console.log("Login to firebase");
  firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    console.log(error.code + ": " + error.message);
  });
}


function promptValues() {
  rl.question("Quantity: ", function(qty) {
    size = qty;
    rl.question("Client ID: ", function(clientId) {
      rl.question("Expiration(days): ", function(exp) {
        rl.question("Product ID: ", function(product) {
          rl.question("\nCoupons: " + qty + " coupons\nClient ID: " + clientId + "\nExpiring in: " + exp + " days\nProduct: " + product + "\n\nGenerate? (Yes/No) ", function(generate) {

            let kw = generate.toLowerCase();
            let res = false;
            [
              'y', // true
              'yes', // true
            ].forEach(q => {
              let find = wordInString(kw, q);
              if (find) {
                res = true;
              }
            })
  
            if (!res) {
              console.log("\nEnter coupon values: \n");
              promptValues();
            } else {
              console.log("\nGenerating coupons... \n");
  
              createCoupons(qty, clientId, exp, product);
              rl.close();
            }
          });
        });

      });
    });
  });
}


const wordInString = (s, word) => new RegExp('\\b' + word + '\\b', 'i').test(s);


function createCoupons(qty, clientId, exp, product) {

  var coupons = [];
  var now = moment();
  var later = now.clone().add(exp, 'days');

  for (let index = 0; index < qty; index++) {

    let coupon = {
      clientId: clientId,
      st: 'new',
      tsc: now.unix() * 1000,
      tse: later.unix() * 1000,
      id: generateRandomCode(6),
      uses: 1,
      type: 'free_sign',
      product: product
    }
    coupons.push(coupon);
  }

  writeCoupons(coupons)
}


function writeCoupons(coupons) {

  let coupon = coupons.shift();
  firebase.database().ref("/accounts/" + config.account + "/coupons/" + coupon.id).update(coupon).then(function() {
    console.log("Generated ", (size - coupons.length), " of ", size, coupon)
    if (coupons.length >= 1) {
      writeCoupons(coupons);
    }
    if (coupons.length == 0) {
      console.log("\nFinished coupon creation");
      process.exit(0)
    }
  });
}

function generateRandomCode(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKMNPQRSTUVWXYZ23456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
