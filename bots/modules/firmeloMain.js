
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' firmeloMain', user.id, step, group)
    console.log(job)
    switch (step) {
      case "0":
        updateUser({ step: '1', module: "main" }, job.contact);
        pushText(menu(true), job.contact, user.line)
        break;

      case "1":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText('Listo, en un momento te atendemos', job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
          //~ updateUser({ step: '5.1', module: "main" }, job.contact);
          //~ pushText("Este canal de soporte solamente sirve para consultar el enlace de acceso al proceso de firma electrónica.\nSi usted es titular o garante puede ingresar su cédula.\nPara cualquier otra inquietud por favor contacte directamente a su Coordinador en el ISSPOL.\n\nCuál es su número de cédula?", job.contact, user.line)
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          updateUser({ step: '2.1', module: "main" }, job.contact);
          pushText(recargas(), job.contact, user.line)
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          updateUser({ step: '1', module: "main" }, job.contact);
          pushText(preguntasFrecuentes() + menu(false), job.contact, user.line)
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          salir(job, user)
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
          updateUser({ step: '5.1', module: "main" }, job.contact);
          pushText("Este canal de soporte solamente sirve para consultar el enlace de acceso al proceso de firma electrónica.\nSi usted es titular o garante puede ingresar su cédula.\nPara cualquier otra inquietud por favor contacte directamente a su Coordinador en el ISSPOL.\n\nCuál es su número de cédula?", job.contact, user.line)
          
           
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "2.1":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          updateUser({ step: '2.1.1', module: "main" }, job.contact);
          pushText('Listo, envía por favor la foto del recibo de la transferencia.', job.contact, user.line)
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          updateUser({ step: '1', module: "main" }, job.contact);
          pushText(menu(false), job.contact, user.line)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "2.1.1":
        if (job.msg['type'] === 'image') {
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText('Un momento por favor mientras validamos la transferrencia.', job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else {
          pushText('Debes enviar una foto de la transferencia o escribe *salir* para terminar la sesión', job.contact, user.line);
        }
        break;

	  case "5.1":
        var kw = body.replace(/\D/g,'');
        console.log ("Searching flows for",kw)
        if (kw.length==10) {
			//search flows
			wfapi.searchFlows(kw, 'isspol', function(flows) {
				if (flows) {
					if (flows.length>0) {
						var news = _.where(flows, {st: 'new'})
						var opens = _.where(flows, {st: 'in_progress'})
						buildSSOArr(_.union(news, opens), [], function(ssos) {
							if (ssos.length>0) {
								var str = "Se encontraron los siguientes procesos de firma activos, utilice el enlace para acceder al proceso. Recuerde que este enlace es personal y no debe compartirlo:\n\n"
								for (var i=0; i<ssos.length; i++) {
									var flow = ssos[i]
									//console.log (JSON.stringify(flow.details))
									str += (i+1) + ". "+(flow.details.customData.subject?flow.details.customData.subject:"Proceso de firma ISSPOL") + "\n"+flow.details.type.link_sso + "" + flow.details.sso + "\n"
								}
								pushText(str, job.contact, user.line);
								salirQuiet(job, user)
							} else {
								pushText("No se encontraron procesos de firma activos para la cédula "+kw+"\n\nSi usted es garante, espere hasta que culmine la validación del titular para que le llegue a usted el enlace para firmar.\n\n*El proceso de firma lo puede hacer hasta el mismo día que realizó la solicitud hasta las 17h00*\n\nPara cualquier otra inquietud por favor contacte directamente a su Coordinador en el ISSPOL.", job.contact, user.line);
								salirQuiet(job, user)	
							}
						})
					} else {
						pushText("No se encontraron procesos de firma activos para la cédula "+kw+"\n\nSi usted es garante, espere hasta que culmine la validación del titular para que le llegue a usted el enlace para firmar.\n\n*El proceso de firma lo puede hacer hasta el mismo día que realizó la solicitud hasta las 17h00*\n\nPara cualquier otra inquietud por favor contacte directamente a su Coordinador en el ISSPOL.", job.contact, user.line);
						salirQuiet(job, user)
					}
				} else {
					pushText("Ha ocurrido un error, por favor intente nuevamente más tarde", job.contact, user.line);
					salirQuiet(job, user)
				}	
			}) 
		} else {
			pushText("Por favor ingrese un número de cédula válido.", job.contact, user.line);
		}
        break;
        
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo call2bMain")
    console.log(job)

    cb({ st: "done" })
  }
}

function buildSSOArr(arr, ssos, cb) {
	if (arr.length>0) {
		var el = arr.shift()
		console.log ("getFlow dets", el.id)
		wfapi.getFlow(el.id, 'isspol', function(flow) {
			if (flow) {
				ssos.push(flow)	
			}
			buildSSOArr(arr, ssos, cb)
		})
	} else {
		cb (ssos)	
	}
}

function menu(greet) {
  if (greet) {
    var msg = 'Hola bienvenido al canal de servicio al cliente de Firmelo. Por favor selecciona una opción:\n\n';
  } else {
    var msg = 'Selecciona una opción:\n\n';
  }
  msg += "1. Servicio al cliente\n";
  msg += "2. Recargas\n";
  msg += "3. Preguntas frecuentes\n";
  msg += "4. Salir\n";
  msg += "5. Firma Créditos ISSPOL\n";
	
  return msg
}

function recargas() {
  var msg = 'Debes realizar las recargas de créditos ingresando a tu cuenta aquí:\n';
  msg += "https://firmelo.com/app/#shop\n\n";
  msg += "Puedes pagar con tarjeta de crédito o por transferencia bancaria.\n\n";
  msg += "Realizaste una recarga con transferencia bancaria y quieres enviar el recibo de la transferencia?\n";
  msg += "1. Si\n";
  msg += "2. No\n";

  return msg
}

function preguntasFrecuentes() {
  var msg = 'Puedes revisar nuestro manual de ayuda y preguntas frecuentes en:\n';
  msg += "https://firmelo.com/ayuda/\n\n";

  return msg
}


