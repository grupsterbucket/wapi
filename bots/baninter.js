var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "baninter";
var booted = false;
var jobs = []
var os = require('os');


//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+client+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		if (user.step) {step = user.step};
		
		console.log ("User "+job.contact+" is at step:", step);
		
		if (step != 'idle') {
				console.log ("Process MO ", job.msg.type);
				switch (job.msg.type) {
					case 'chat':
						var body = job.msg.body;
						if (body.length>0) {
							//trim spaces
							body = body.trim();

							//catch operations
							if (body.toLowerCase().indexOf("#") == 0) {
								var parts = body.toLowerCase().split(" ");
								//match direct operation
								switch ( parts[0] ) {
									case "#ping":
										var msg = "#pong "+ new Date();
										pushText (msg, job.contact, user.line);
										break;
									case "#salir":
										var msg = "Estimado cliente, gracias por utilizar este canale de comunicación.\n";
										msg += "Hasta pronto";
										pushText (msg, job.contact, user.line);
										resetUserParam("step", job.contact, user.line);
										break;
									case "#server":
										var msg = "Server data:\n";
										msg += "Datetime: "+ new Date() + "\n";
										msg += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
										msg += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
										pushText (msg, job.contact, user.line);
										resetUserParam("step", job.contact, user.line);
										break
								} //end switch parts
							} else {
								//process step
								switch (step) {
									case "0":
										var kw = body.toLowerCase();
										if (kw == "ok" || kw == "gracias" || kw == "muchas gracias") {
											var msg = "Contamos contigo!\n"
											pushText (msg, job.contact, user.line);
										}
										if (kw == "si" || kw == "sí") {
											var msg = "Gracias, nos mantendremos en contacto contigo por este medio. Agréganos a tus contactos. Que tengas un excelente día!\n"
											pushText (msg, job.contact, user.line);
										}
										if (kw == "no") {
											var msg = "Por favor responde con el número celular donde quieres recibir esta información, tu nombre y tu cédula, para actualizarlo. \nGracias"
											pushText (msg, job.contact, user.line);
											//open ticket
											setUserParam("step","idle", job.contact, user.line);
											setUserParam("status","active", job.contact, user.line);
											//send notification and autoassign
											setUserParam("rep","BtHT4FEs2lMOvnDLXgQxiDkmCbF3", job.contact, user.line);
											pushText ("Hola José, tienes un ticket nuevo en Textcenter.","593999724466", user.line);
											//pushText ("Hola Ricki, tienes un ticket nuevo en Textcenter.","593984252217", user.line);
										}
										if (kw == "ayuda") {
											var msg = "Hola, gracias por comunicarte con nosotros. Por favor cuéntanos en qué te podemos ayudar?";
											if ( user["x-nick"] ) {
												msg = "Hola, " + user["x-nick"] + ", en qué te podemos ayudar?";
											}
											pushText (msg, job.contact, user.line);
											//open ticket
											setUserParam("step","idle", job.contact, user.line);
											setUserParam("status","active", job.contact, user.line);
											//send notification and autoassign
											setUserParam("rep","BtHT4FEs2lMOvnDLXgQxiDkmCbF3", job.contact, user.line);
											pushText ("Tienes un ticket nuevo en Textcenter.","593995402373", user.line);
											pushText ("Tienes un ticket nuevo en Textcenter.","593984251603", user.line);
											//pushText ("Hola Ricki, tienes un ticket nuevo en Textcenter.","593984252217", user.line);
										}
										break;
									
								}//end switch step
								
							} //end if operation
						}
						//job is completed
						finishJob(job);
						break;
					default:
						//finish jobs
						finishJob(job);
						break;
				} //end switch mo type
		} else { //end idle check
			console.log ('user '+job.contact+'@'+user.line+' is idle');
			//job is completed
			finishJob(job);
		}
	});
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed'};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(500));
}

function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).set(val);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).remove();
}
