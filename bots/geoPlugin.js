// Cuponfi Bot as module
var cupones = []
var locales = []
module.exports = {
	init: function() {
		console.log ("Init Geo Plugin. Listen to cupones col.")
		var path = "accounts/cuponfi/cupones"
		firebase.database().ref(path).on('child_added', function(res) {
			var reg = res.val();
			reg.id = res.key;
			cupones.push(reg);
			//console.log (cupones)	
		});	
		
		firebase.database().ref(path).on('child_changed', function(res) {
			var reg = res.val();
			reg.id = res.key;
			var idx = _.findIndex(cupones, {id: reg.id});
			if (idx>=0) {
				cupones[idx] = JSON.parse(JSON.stringify(reg))	
			}
		});
		
		firebase.database().ref(path).on('child_removed', function(res) {
			var reg = res.val();
			reg.id = res.key;
			var idx = _.findIndex(cupones, {id: reg.id});
			if (idx>=0) {
				cupones.splice(idx,1);
			}
		});
		console.log ("Init Geo Plugin. Listen to locales col.")
		var path = "accounts/cuponfi/locales"
		firebase.database().ref(path).on('child_added', function(res) {
			var reg = res.val();
			reg.id = res.key;
			locales.push(reg);
			//console.log (locales)	
		});	
		
		firebase.database().ref(path).on('child_changed', function(res) {
			var reg = res.val();
			reg.id = res.key;
			var idx = _.findIndex(locales, {id: reg.id});
			if (idx>=0) {
				locales[idx] = JSON.parse(JSON.stringify(reg))	
			}
		});
		
		firebase.database().ref(path).on('child_removed', function(res) {
			var reg = res.val();
			reg.id = res.key;
			var idx = _.findIndex(locales, {id: reg.id});
			if (idx>=0) {
				locales.splice(idx,1);
			}
		});
	},
	
	findCupons: function (lat, lon, minDist) {
		console.log (new Date(), "Finding cupons", lat, lon, minDist)
		
		//loop active locales and find close ones
		var arr = []
		for (var i=0; i<locales.length;i++) {
			var local = locales[i];
			var dist = calcCrow(lat, lon, local.lat, local.lon);
			if (local.status == "active" && dist<=minDist) {
				var lMatch = JSON.parse(JSON.stringify(local));
				delete lMatch.createdBy;
				delete lMatch.updataedBy;
				delete lMatch.tsc;
				delete lMatch.tsu;
				delete lMatch.status;
				lMatch.dist = dist;
				arr.push(lMatch);
			}
		}
		console.log (new Date(), "Found "+arr.length+" locales");
		var matches = []
		for (var i=0; i<arr.length;i++) {
			var cps = _.where(cupones, {local: arr[i].id});
			if (cps.length>0) {
				for (var j=0; j<cps.length;j++) {
					var cupon = JSON.parse(JSON.stringify(cps[j]));
					delete cupon.createdBy;
					delete cupon.updataedBy;
					delete cupon.tsc;
					delete cupon.tsu;
					//to do: check max run
					var ts = Date.now()
					if (cupon.status == "active" && ts >= cupon.activeDate && ts <= cupon.expireDate) {
						cupon.local = arr[i];
						cupon.dist = arr[i].dist;
						matches.push(cupon);
					}
				}	
			}
		}
		console.log (new Date(), "Found "+matches.length+" cupones");
		return matches;
		
		function calcCrow(lat1, lon1, lat2, lon2)  {
			var R = 6371; // km
			var dLat = toRad(lat2-lat1);
			var dLon = toRad(lon2-lon1);
			var lat1 = toRad(lat1);
			var lat2 = toRad(lat2);

			var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
			var d = R * c;
			return d;
		}

		// Converts numeric degrees to radians
		function toRad(Value) {
			return Value * Math.PI / 180;
		}
	}
}
