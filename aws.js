// AWS PUT/GET WRAPPERS

module.exports = {
	init: function (k,p) {
		global.AWS = require('aws-sdk');
		AWS.config.logger = 'console';
		AWS.config.update({accessKeyId: k, secretAccessKey: p});
		global.s3 = new AWS.S3({region: 'us-east-1', maxRetries: 3});
		global.transcribe = new AWS.TranscribeService({region: 'us-east-1'});
		global.s3Bucket = 'wsp2crmcdn';
		console.log ("AWS initialized");
	},
	
	getObject: function(key, cb) {
		console.log (new Date(), "AWS Getting "+key);
		var params = {
		  Bucket: s3Bucket,
		  Key: key
		};
		
		s3.getObject(params, function(err, data) {
			if (err) {
				  console.log (new Date(), "AWS GET Object Error");
				  console.log (err)
				  if (cb) { cb({ status: "err", msg: err.message }); }
			} else {
				 if (cb) { cb({ status: "ok", file: data }); }
			}
		}); 
	},
	
	putObject: function(key, buffer, contentType, cb) {
		var params = {
			Bucket: s3Bucket,
			Key: key,
			Body: buffer,
			ACL:"public-read",
			CacheControl: "no-cache, no-store, must-revalidate",
			StorageClass: "STANDARD",
			ContentType: contentType
		};
		
		s3.putObject(params).send(function(err, data) {
			if (!err) {
				if (cb) { cb({ status: "ok", msg: 'AWS Put Object Success' }); } 
			} else {
				console.log (new Date(), "AWS PUT Object Error");
				console.log (err);
				if (cb) { cb({ status: "err", msg: err.message }); }

			}
		});
	},
	
	transcribeVideo: function(bucket, folder, fname, type, cb) {

		var params = {
		  TranscriptionJobName: "TJ_"+fname,
		  LanguageCode: "es-US",
		  Media: {
			MediaFileUri: "s3://"+bucket+"/"+folder+"/"+fname+"."+type
		  },
		  OutputBucketName: bucket,
		  OutputKey: folder+"/TJ_"+fname
		};
		console.log (new Date(), "TV started")
		transcribe.startTranscriptionJob(params, function(err, data) {
		  console.log (new Date(), "TV completed")
		  if (err) {
			  console.log(err, err.stack); // an error occurred
			  cb(false) 
		  } else {
			  cb(data);           // successful response
		  }
		});
	},
	
	checkTranscribe: function(jobName, cb) {

		var params = {
		  TranscriptionJobName: jobName
		};
		transcribe.getTranscriptionJob(params, function(err, data) {
		  if (err) {
			  console.log(err, err.stack); // an error occurred
			  cb(false)
		  } else {
			  cb(data);           // successful response
		  }
		});
	}
		
}
