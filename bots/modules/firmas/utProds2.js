const { consultarPrecios } = require('../../uanatacaAPI')

module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' utProds2', user.id, step, group)
    console.log(job)
    switch (step) {
      case "0":
        if (user.currentProcess) {
          updateUser({ step: "1", module: "utProds" }, job.contact);
          pushText("*Tipos de firma*\n" + "*Archivo P12:*\n" + "Recibes el certificado por email, puedes usarlo para facturación electrónica y firmar documentos.\n\n" + "*Nube:*\n" + "Utiliza nuestro aplicativo para firmar documentos en la nube desde tu teléfono o computador. *No sirve para facturación electrónica.*\n\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
        } else {
          var nuevoProceso = {
            contacto: job.contact,
            st: 'new',
            tsc: Date.now(),
            type: 'utProds2',
            notes: 'Selección de firma LTV UT'
          }
          crearProceso(nuevoProceso, function (pocesoId) {
            if (pocesoId) {
              updateUser({ step: "1", module: "utProds", currentProcess: pocesoId }, job.contact);
              pushText("*Tipos de firma*\n" + "*Archivo P12:*\n" + "Recibes el certificado por email, puedes usarlo para facturación electrónica y firmar documentos.\n\n" + "*Nube:*\n" + "Utiliza nuestro aplicativo para firmar documentos en la nube desde tu teléfono o computador. *No sirve para facturación electrónica.*\n\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
            } else {
              pushText(msgErrorInterno(), job.contact, user.line);
              salir(job, user);
            }
          });
        }
        break;

      //!Aqui me llega el tipo de firma (step 1)
      case '1':
        var kw = body.toLowerCase()
        if (["cf-tipos-ap12"].indexOf(kw) >= 0) {
          actualizarProceso(user.currentProcess, { codigoDeDescuento: 'no', tipo_usuario: 'cliente_final', tipo_firma: '0', codigoVendedor: '', tsu: Date.now() });
          setUserParam('step', 'cf-ap12-0', job.contact, user.line)
          pushText("*Detalles*\n" + "Diseñada para firmar documentos electrónicos y facturación electrónica.\n\n" + "Selecciona una opción:", job.contact, user.line, null, "btn", [{ id: "cf-ap12-continuar", title: "Continuar" }, { id: "cf-ap12-mp", title: "Menú Principal" }])
        } else if (["cf-tipos-nube"].indexOf(kw) >= 0) {
          utapi.consultarPrecios().then((res) => {
            if (res.result) {
              actualizarProceso(user.currentProcess, { codigoDeDescuento: 'no', tipo_usuario: 'cliente_final', tipo_firma: '2', codigoVendedor: '', perfil_suscriptor: '1', tsu: Date.now() });
              setUserParam('step', 'cf-ap12-2', job.contact, user.line)
              pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
            } else {
              setUserParam('step', '1', job.contact, user.line)
              pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
            }
          }).catch((err) => {
            console.log(err)
            setUserParam('step', '1', job.contact, user.line)
            pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
          })
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      case 'cf-ap12-0':
        var kw = body.toLowerCase()
        if (["cf-ap12-continuar"].indexOf(kw) >= 0) {
          utapi.consultarPrecios().then((res) => {
            if (res.result) {
              actualizarProceso(user.currentProcess, { codigoDeDescuento: 'no', perfil_suscriptor: '1', tsu: Date.now() });
              setUserParam('step', 'cf-ap12-2', job.contact, user.line)
              pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
            } else {
              setUserParam('step', '1', job.contact, user.line)
              pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
            }
          }).catch((err) => {
            console.log(err)
            setUserParam('step', '1', job.contact, user.line)
            pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
          })
          //!Paso eliminado: Poner cuando haya persona natural, miembro de empresa y representante legal
          // setUserParam('step', 'cf-ap12-1', job.contact, user.line)
          // pushText("*Perfil suscriptor*\n" + "Selecciona una opción:\n\n", job.contact, user.line, null, "btn", [{ id: "cf-ap12-pn", title: "Persona Natural" }, { id: "cf-ap12-rl", title: "Representante Legal" }, { id: "cf-ap12-me", title: "Miembro de Empresa" }])
        } else if (["cf-ap12-mp"].indexOf(kw) >= 0) {
          setUserParam('step', '1', job.contact, user.line)
          pushText("*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*Aqui me llega cuando hay persona natural, representante de emresa o miembro legal
      // case 'cf-ap12-1':
      //   var kw = body.toLowerCase()
      //   //perfil_suscriptor || Persona natural:"1", Representante legal:"2", Miembro de empresa:"3"
      //   //* Persona Natural
      //   if (["cf-ap12-pn"].indexOf(kw) >= 0) {
      //     consultarPrecios().then((res) => {
      //       if (res.result) {
      //         actualizarProceso(user.currentProcess, { codigoDeDescuento: 'no', perfil_suscriptor: '1', tsu: Date.now() });
      //         setUserParam('step', 'cf-ap12-2', job.contact, user.line)
      //         pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
      //       } else {
      //         setUserParam('step', '1', job.contact, user.line)
      //         pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //       }
      //     }).catch((err) => {
      //       console.log(err)
      //       setUserParam('step', '1', job.contact, user.line)
      //       pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //     })
      //     //* Representante legal
      //   } else if (["cf-ap12-rl"].indexOf(kw) >= 0) {
      //     consultarPrecios().then((res) => {
      //       if (res.result) {
      //         actualizarProceso(user.currentProcess, { codigoDeDescuento: 'no', perfil_suscriptor: '2', tsu: Date.now() });
      //         setUserParam('step', 'cf-ap12-2', job.contact, user.line)
      //         pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
      //       } else {
      //         setUserParam('step', '1', job.contact, user.line)
      //         pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //       }
      //     }).catch((err) => {
      //       console.log(err)
      //       setUserParam('step', '1', job.contact, user.line)
      //       pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //     })
      //     //* Miembro de empresa
      //   } else if (["cf-ap12-me"].indexOf(kw) >= 0) {
      //     consultarPrecios().then((res) => {
      //       if (res.result) {
      //         actualizarProceso(user.currentProcess, { codigoDeDescuento: 'no', perfil_suscriptor: '3', tsu: Date.now() });
      //         setUserParam('step', 'cf-ap12-2', job.contact, user.line)
      //         pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
      //       } else {
      //         setUserParam('step', '1', job.contact, user.line)
      //         pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //       }
      //     }).catch((err) => {
      //       console.log(err)
      //       setUserParam('step', '1', job.contact, user.line)
      //       pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //     })
      //     //* Si no escoge ninguna de las opciones
      //   } else {
      //     pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
      //   }
      //   break

      //*Aqui me llega si tiene un codigo de descuento si o no
      // case 'cf-ap12-codigo-descuento':
      //   var kw = body.toLowerCase()
      //   if (kw === 'si_cod') {
      //     setUserParam('step', 'cf-ap12-codigo-descuento-1', job.contact, user.line)
      //     pushText('Ingresa tu código de descuento por favor.', job.contact, user.line)
      //   } else if (kw === 'no_cod') {
      //     consultarPrecios().then((res) => {
      //       console.log('CONSULTAR PRECIOS', res)
      //       if (res.result) {
      //         actualizarProceso(user.currentProcess, { tipo_firma: '0', codigoDeDescuento: 'no', codigo: ' ', porcentaje: ' ', tsu: Date.now() });
      //         setUserParam('step', 'cf-ap12-2', job.contact, user.line)
      //         pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
      //       } else {
      //         setUserParam('step', '1', job.contact, user.line)
      //         pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //       }
      //     }).catch((err) => {
      //       console.log(err)
      //       setUserParam('step', '1', job.contact, user.line)
      //       pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //     })
      //   } else {
      //     pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
      //   }
      //   break

      //*Aqui me llega el codigo de descuento
      // case 'cf-ap12-codigo-descuento-1':
      //   var kw = job.msg.text.body
      //   consultarCodDescuento(kw).then((res) => {
      //     if (res.result) {
      //       actualizarProceso(user.currentProcess, { codigoDeDescuento: 'si', porcentaje: res.porcent_descuento, codigo: kw, tsu: Date.now() });
      //       consultarPrecios().then((result) => {
      //         if (result.result) {
      //           let porcentajeDescuento = res.porcent_descuento
      //           let precio1 = Math.round((parseFloat(result.precios[0].precio1) - (parseFloat(result.precios[0].precio1) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
      //           let precio2 = Math.round((parseFloat(result.precios[1].precio2) - (parseFloat(result.precios[1].precio2) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
      //           let precio3 = Math.round((parseFloat(result.precios[2].precio3) - (parseFloat(result.precios[2].precio3) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
      //           let precio4 = Math.round((parseFloat(result.precios[3].precio4) - (parseFloat(result.precios[3].precio4) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
      //           let precio5 = Math.round((parseFloat(result.precios[4].precio5) - (parseFloat(result.precios[4].precio5) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
      //           setUserParam('step', 'cf-ap12-2', job.contact, user.line)
      //           pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + precio5 + ' + ' + 'IVA' }] })
      //         } else {
      //           setUserParam('step', '1', job.contact, user.line)
      //           pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //         }
      //       }).catch((err) => {
      //         console.log(err)
      //         setUserParam('step', '1', job.contact, user.line)
      //         pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //       })
      //     } else {
      //       setUserParam('step', 'cf-ap12-codigo-descuento-2', job.contact, user.line)
      //       pushText("*No encontramos el código que ingresaste.*\n" + "Deseas continuar sin aplicar un descuento?\n", job.contact, user.line, null, "btn", [{ id: "si_continuar", title: "SI" }, { id: "no_continuar", title: "NO" }])
      //       // pushText('No encontramos el código que ingresaste.\n\nDeseas continuar sin aplicar un descuento?\n1. Si\n2. No', job.contact, user.line)
      //     }
      //   }).catch((err) => {
      //     console.log(err)
      //     setUserParam('step', '1', job.contact, user.line)
      //     pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //   })
      //   break


      //*Aqui me llega si desea o no continuar con un codigo de descuento
      // case 'cf-ap12-codigo-descuento-2':
      //   var kw = body.toLowerCase()
      //   if (kw === 'si_continuar') {
      //     consultarPrecios().then((res) => {
      //       if (res.result) {
      //         actualizarProceso(user.currentProcess, { codigoDeDescuento: 'no', codigo: ' ', porcentaje: ' ', tsu: Date.now() });
      //         setUserParam('step', 'cf-ap12-2', job.contact, user.line)
      //         pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + res.precios[0].precio1 + ' + ' + 'IVA' }, { id: "vigencia2", title: "2 años", desc: '$' + res.precios[1].precio2 + ' + ' + 'IVA' }, { id: "vigencia3", title: "3 años", desc: '$' + res.precios[2].precio3 + ' + ' + 'IVA' }, { id: "vigencia4", title: "4 años", desc: '$' + res.precios[3].precio4 + ' + ' + 'IVA' }, { id: "vigencia5", title: "5 años", desc: '$' + res.precios[4].precio5 + ' + ' + 'IVA' }] })
      //       } else {
      //         setUserParam('step', '1', job.contact, user.line)
      //         pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //       }
      //     }).catch((err) => {
      //       console.log(err)
      //       setUserParam('step', '1', job.contact, user.line)
      //       pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
      //     })
      //   } else if (kw === 'no_continuar') {
      //     setUserParam('step', 'cf-ap12-codigo-descuento-1', job.contact, user.line)
      //     pushText('Ingresa tu código de descuento por favor.', job.contact, user.line)
      //   }
      //   break


      //*Aqui me llega el tiempo de vigencia que escogio
      case 'cf-ap12-2':
        var kw = body.toLowerCase()
        if (kw.includes('vigencia')) {
          obtenerProceso(user.currentProcess, function (evidencias) {
            console.log('EVIDENCIAS', evidencias)
            if (evidencias) {
              //*Si es que tiene codigo de descuento
              if (evidencias.codigoDeDescuento === 'si') {
                utapi.consultarPrecios().then((result) => {
                  if (result.result) {
                    porcentajeDescuento = evidencias.porcentaje;
                    let precio1 = Math.round((parseFloat(result.precios[0].precio1) - (parseFloat(result.precios[0].precio1) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
                    let precio2 = Math.round((parseFloat(result.precios[1].precio2) - (parseFloat(result.precios[1].precio2) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
                    let precio3 = Math.round((parseFloat(result.precios[2].precio3) - (parseFloat(result.precios[2].precio3) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
                    let precio4 = Math.round((parseFloat(result.precios[3].precio4) - (parseFloat(result.precios[3].precio4) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
                    let precio5 = Math.round((parseFloat(result.precios[4].precio5) - (parseFloat(result.precios[4].precio5) * ((parseFloat(porcentajeDescuento)) / 100))) * 100) / 100
                    //*ifs para comprobar que vigencia eligio
                    if (kw === 'vigencia1') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '1 año', precio: precio1, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    }
                    else if (kw === 'vigencia2') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '2 años', precio: precio2, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    }
                    else if (kw === 'vigencia3') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '3 años', precio: precio3, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    }
                    else if (kw === 'vigencia4') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '4 años', precio: precio4, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    }
                    else if (kw === 'vigencia5') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '5 años', precio: precio5, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    } else {
                      pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
                    }
                  } else {
                    setUserParam('step', '1', job.contact, user.line)
                    pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
                  }
                }).catch((err) => {
                  console.log(err)
                  setUserParam('step', '1', job.contact, user.line)
                  pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
                })
              } else {
                utapi.consultarPrecios().then((result) => {
                  if (result.result) {
                    //*ifs para comprobar que vigencia eligio
                    if (kw === 'vigencia1') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '1 año', precio: result.precios[0].precio1, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    }
                    else if (kw === 'vigencia2') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '2 años', precio: result.precios[1].precio2, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    }
                    else if (kw === 'vigencia3') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '3 años', precio: result.precios[2].precio3, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    }
                    else if (kw === 'vigencia4') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '4 años', precio: result.precios[3].precio4, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    }
                    else if (kw === 'vigencia5') {
                      actualizarProceso(user.currentProcess, { vigencia_firma: '5 años', precio: result.precios[4].precio5, tsu: Date.now() });
                      mods[envBot[account].modPago].bot('Swtich Module', job, user, "0")
                    } else {
                      pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
                    }
                  } else {
                    setUserParam('step', '1', job.contact, user.line)
                    pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
                  }
                }).catch((err) => {
                  console.log(err)
                  setUserParam('step', '1', job.contact, user.line)
                  pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
                })
              }
            } else {
              setUserParam('step', '1', job.contact, user.line)
              pushText("Ocurrio un error\n\n*Tipos de firma*\n" + "Selecciona:\n", job.contact, user.line, null, "btn", [{ id: "cf-tipos-ap12", title: "Archivo P12" }, { id: "cf-tipos-nube", title: "Nube" }])
            }
          })
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo utProds2")
    console.log(job)
    cb({ st: "done" })
  }
}

