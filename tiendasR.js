//PROXY for DKManagement - DMOL
var _ = require('underscore');
var fs = require('fs');
var atob = require('atob');
var tiendas = [];
var csv = "";
loadTiendas( bootApp );

function bootApp() {
		console.log ("Creating dump");
		console.log ("Create Header");
		var sample = {
			"address" : "Ramon Vicente Cl 15 De Febrero Y En A 24 De Mayo",
			"cant" : "24 de Mayo",
			"code" : "11802882",
			"country_code" : "+593",
			"data" : "eyJwcm9tb3MiOltdfQ==",
			"delivery" : 1,
			"dirc" : "",
			"gcoded" : true,
			"id" : 1741308,
			"lat" : -1.2719200000000002,
			"lng" : -80.419573,
			"parr" : "Sucre",
			"phone" : "985256524",
			"prov" : "Manabí",
			"title" : "Kiosko-Chancay Cesar",
			"type" : "DISTRIBUIDOR LICORES"
		}
		var header = ["Dir", "Tel", "Delivery", "Nombre", "Tipo", "Provincia", "Cantón", "Parroquia", "LAT", "LON"]
		var rows = []
		rows.push (header.join("\t"))
		for (var i=0; i<tiendas.length; i++) {
			var obj = tiendas[i]
			
			var row = []
				
			if (obj["address"]) {
				row.push(obj["address"])
			} else {
				row.push("");
			}
			
			if (obj["phone"]) {
				row.push(obj["phone"])
			} else {
				row.push("");
			}
			
			if (obj["delivery"]) {
				row.push(obj["delivery"])
			} else {
				row.push("");
			}
			
			if (obj["title"]) {
				row.push(obj["title"])
			} else {
				row.push("");
			}
			
			if (obj["type"]) {
				row.push(obj["type"])
			} else {
				row.push("");
			}
			
			if (obj["prov"]) {
				row.push(obj["prov"])
			} else {
				row.push("");
			}
			
			if (obj["cant"]) {
				row.push(obj["cant"])
			} else {
				row.push("");
			}
			
			if (obj["parr"]) {
				row.push(obj["parr"])
			} else {
				row.push("");
			}
			
			if (obj["lat"]) {
				row.push(obj["lat"])
			} else {
				row.push("");
			}
			
			if (obj["lng"]) {
				row.push(obj["lng"])
			} else {
				row.push("");
			}
			
			rows.push(row.join("\t").replace(/\n/g," / n"))
		}
		csv = rows.join("\n")
		fs.writeFile('tiendasR.csv', csv, function (err) {
		  if (err) return console.log(err);
		  console.log (csv.length)
		  console.log ("Done!");
		});
		

} //end boot app


function loadTiendas(cb) {
	
	//load tiendas json
	fs.readFile('tiendasR.json', 'utf8', function (err, data) {
		if (err) {
			console.log("Cannot load tiendas file") 
		    console.log(err)
		    cb();
		} else {
			tiendas = _.map(JSON.parse(data), function(ob, key) { 
				ob.id = key;
				return ob;
				});
			console.log (tiendas.length, "tiendas loaded");
			cb();
	    }
	});
}
