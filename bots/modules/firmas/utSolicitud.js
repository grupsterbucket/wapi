const { convertirB64 } = require('../../nexxitAPI')

module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' utSolicitud', user.id, step, group)
    console.log(job)
    switch (step) {
      case "0":
        if (user.currentProcess) {
          updateUser({ step: "3", module: "utSolicitud" }, job.contact);
          pushText(solicitarPrimerApellido(), job.contact, user.line)
        } else {
          var nuevoProceso = {
            contacto: job.contact,
            st: 'new',
            tsc: Date.now(),
            type: 'utSolicitud',
            notes: 'Petición de datos para la solicitud de la firma'
          }
          crearProceso(nuevoProceso, function (pocesoId) {
            if (pocesoId) {
              updateUser({ step: "3", module: "utSolicitud", currentProcess: pocesoId }, job.contact);
              pushText(solicitarPrimerApellido(), job.contact, user.line)
            } else {
              pushText(msgErrorInterno(), job.contact, user.line);
              salir(job, user);
            }
          });
        }
        break;

      //* Aqui me llega el apellido y solicito el segundo apellido si es que lo tiene
      case '3':
        var kw = body.toUpperCase()

        obtenerProceso(user.currentProcess, function (evidencias) {
          if (evidencias) {
            if (evidencias.nombresRegCivil.indexOf(kw) >= 0 || kw.indexOf("TEST") == 0) {
              setUserParam('step', '4', job.contact, user.line)
              pushText(solicitarSegundoApellido(), job.contact, user.line)
              actualizarProceso(user.currentProcess, { apellido1: kw, tsu: Date.now() });
            } else {
              pushText('El apellido que acabas de ingresar no coincide con los datos de tu cédula\n\n' + solicitarPrimerApellido(), job.contact, user.line)
            }
          } else {
            setUserParam('step', '0', job.contact, user.line)
            pushText(msgErrorInterno(), job.contact, user.line)
          }
        })
        break

      //* Aqui me llega el segundo apellido o la palabra continuar si es que no lo tiene
      //*  y solicito los nombres completos
      case '4':
        var kw = body.toUpperCase()
        if (kw === 'CONTINUAR') {
          setUserParam('step', '5', job.contact, user.line)
          pushText(solicitarNombres(), job.contact, user.line)
          actualizarProceso(user.currentProcess, { apellido2: '', tsu: Date.now() });
        } else {
          // en este caso guardar en evidencias el apellido
          obtenerProceso(user.currentProcess, function (evidencias) {
            if (evidencias) {
              if (evidencias.nombresRegCivil.indexOf(kw) >= 0 || kw.indexOf("TEST") == 0) {
                setUserParam('step', '5', job.contact, user.line)
                pushText(solicitarNombres(), job.contact, user.line)
                actualizarProceso(user.currentProcess, { apellido2: kw, tsu: Date.now() });
              } else {
                pushText('El apellido que acabas de ingresar no coincide con los datos de tu cédula\n\n' + solicitarSegundoApellido(), job.contact, user.line)
              }
            } else {
              setUserParam('step', '0', job.contact, user.line)
              pushText(msgErrorInterno(), job.contact, user.line)
            }
          })
        }
        break

      //* Aqui me llegan los nombres completos y solicito la fecha de nacimiento
      case '5':
        var kw = body.toUpperCase()

        obtenerProceso(user.currentProcess, function (evidencias) {
          if (evidencias) {
            if (evidencias.nombresRegCivil.indexOf(kw) >= 0 || kw.indexOf("TEST") == 0) {
              setUserParam('step', '6', job.contact, user.line)
              pushText(solicitarFechaNacimiento(), job.contact, user.line)
              actualizarProceso(user.currentProcess, { nombres: kw, nombreCompleto: evidencias.apellido1 + ' ' + evidencias.apellido2 + ' ' + kw, tsu: Date.now() });
            } else {
              pushText('Los nombres que acabas de ingresar no coinciden con los datos de tu cédula\n\n' + solicitarNombres(), job.contact, user.line)
            }
          } else {
            setUserParam('step', '0', job.contact, user.line)
            pushText(msgErrorInterno(), job.contact, user.line)
          }
        })
        break

      //* Aqui me llega la fecha de nacimiento
      //* y solicito la nacionalidad con un menu de opciones 
      case '6':
        var kw = body.toLowerCase()
        if (tools.validarFecha(kw)) {
          setUserParam('step', '7', job.contact, user.line)
          pushText("*Nacionalidad*\n" + "Selecciona una opción:", job.contact, user.line, null, "btn", [{ id: "cf-ap12-10-ecuatoriana", title: "Ecuatoriana" }, { id: "cf-ap12-10-otra", title: "Otra" }])
          actualizarProceso(user.currentProcess, { fecha_nacimiento: kw, tsu: Date.now() });
        } else {
          pushText('El formato de la fecha que ingresaste es inválida\n\n' + solicitarFechaNacimiento(), job.contact, user.line)
        }
        break

      //* Aqui me llega la nacionalidad y solicito el sexo
      case '7':
        var kw = body.toLowerCase()
        if (kw === 'cf-ap12-10-ecuatoriana') {
          setUserParam('step', '8', job.contact, user.line)
          pushText("*Sexo*\n" + "Selecciona una opción:", job.contact, user.line, null, "btn", [{ id: "cf-ap12-9-hombre", title: "HOMBRE" }, { id: "cf-ap12-9-mujer", title: "MUJER" }])
          actualizarProceso(user.currentProcess, { nacionalidad: 'ECUATORIANA', tsu: Date.now() });
        } else if (kw === 'cf-ap12-10-otra') {
          setUserParam('step', '7-otranacionalidad', job.contact, user.line)
          pushText("Por favor especifica tu nacionalidad", job.contact, user.line)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //!Caso para especificar si es otra nacionalidad
      //*Aqui me llega la nacionalidad si no es ecuatoriana
      //* y vuelvo al flujo normal
      case '7-otranacionalidad':
        var kw = body.toUpperCase()
        setUserParam('step', '8', job.contact, user.line)
        pushText("*Sexo*\n" + "Selecciona una opción:", job.contact, user.line, null, "btn", [{ id: "cf-ap12-9-hombre", title: "HOMBRE" }, { id: "cf-ap12-9-mujer", title: "MUJER" }])
        actualizarProceso(user.currentProcess, { nacionalidad: kw, tsu: Date.now() });
        break
      //!Fin caso para especificar si es otra nacionalidad

      //* Aqui me llega el sexo y pregunto si tiene ruc personal activo
      case '8':
        var kw = body.toLowerCase()
        if (kw === 'cf-ap12-9-hombre') {
          setUserParam('step', '9', job.contact, user.line)
          pushText(solicitarProvincia(), job.contact, user.line)
          actualizarProceso(user.currentProcess, { sexo: 'HOMBRE', tsu: Date.now() });
        } else if (kw === 'cf-ap12-9-mujer') {
          setUserParam('step', '9', job.contact, user.line)
          pushText(solicitarProvincia(), job.contact, user.line)
          actualizarProceso(user.currentProcess, { sexo: 'MUJER', tsu: Date.now() });
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //* Aqui me llega la provincia y solicito la ciudad
      case '9':
        var kw = body.toUpperCase().replace('BOLIVAR', 'BOLÍVAR').replace('CANAR', 'CAÑAR').replace('GALAPAGOS', 'GALÁPAGOS').replace('RIOS', 'RÍOS').replace('MANABI', 'MANABÍ').replace('TSACHILAS', 'TSÁCHILAS').replace('SUCUMBIOS', 'SUCUMBÍOS')
        if (config.provincias.indexOf(kw) >= 0) {
          actualizarProceso(user.currentProcess, { provincia: kw, tsu: Date.now() });
          setUserParam('step', '11', job.contact, user.line)
          pushText(solicitarCiudad(), job.contact, user.line)
        } else {
          pushText('esa provincia no se encuentra registrada.\n\n' + solicitarProvincia(), job.contact, user.line)
        }
        break

      //* Aqui me llega la ciudad  y solicito la direccion
      case '11':
        var kw = body.toUpperCase()
        actualizarProceso(user.currentProcess, { ciudad: kw, tsu: Date.now() });
        setUserParam('step', '12', job.contact, user.line)
        pushText('Por favor escribe la dirección completa de tu domicilio.', job.contact, user.line)
        break

      //*Aqui me llega la direccion y es el ultimo paso de informacion general
      case '12':
        var kw = body.toUpperCase()
        //valido que la dir tenga al menos 10 chars
        if (kw.length >= 10) {
          //*ultimo paso de los datos que todos necesitan
          //*Aqui se sepra en tres caminos(persona natural, representante legal, miembro de empresa)
          obtenerProceso(user.currentProcess, function (evidencias) {
            if (evidencias) {
              //*Persona natural
              if (evidencias.perfil_suscriptor === '1') {
                actualizarProceso(user.currentProcess, { direccion: kw, tsu: Date.now() });
                setUserParam('step', 'pn-0', job.contact, user.line)
                pushText("*¿Tiene RUC personal ACTIVO?*\n" + "Selecciona una opción:", job.contact, user.line, null, "btn", [{ id: "ruc-activo", title: "Si" }, { id: "noruc-activo", title: "No" }])
              }
              //*Representante legal
              if (evidencias.perfil_suscriptor === '2') {
                actualizarProceso(user.currentProcess, { direccion: kw, tsu: Date.now() });
                setUserParam('step', 'rl-0', job.contact, user.line)
                pushText('Por favor envíanos el nombre de la empresa tal y como consta en el documento RUC', job.contact, user.line)
              }
              //*Miembro de empresa
              if (evidencias.perfil_suscriptor === '3') {
                actualizarProceso(user.currentProcess, { direccion: kw, tsu: Date.now() });
                setUserParam('step', 'me-0', job.contact, user.line)
                pushText('Por favor envíanos el nombre de la empresa tal y como consta en el documento RUC', job.contact, user.line)
              }
            } else {
              pushText("Ha ocurrido , por favor intenta nuevamente más tarde.", job.contact, user.line);
              salir(job, user);
            }
          })
        } else {
          pushText("Debes enviar la dirección completa de tu domicilio", job.contact, user.line)
        }
        break

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //!Inicio persona natural
      //* Aqui me llega si o no en la pregunta del RUC
      //* Solicito numero de RUC
      case 'pn-0':
        var kw = body.toLowerCase()
        if (kw === 'noruc-activo') {
          actualizarProceso(user.currentProcess, { rucActivo: 'no', tsu: Date.now() });
          //crear Job syncFiles
          obtenerProceso(user.currentProcess, function (evidencias) {
            if (evidencias) {
              if (evidencias.terceraEdad === 'no') {
                syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack'])
              } else {
                syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack', 'videoTerceraEdad'])
              }
            } else {
              setUserParam('step', '0', job.contact, user.line)
              pushText(msgErrorInterno(), job.contact, user.line)
            }
          })

          //Finalizar proceso con st: sent
          finishProcess(job, user);
          mensajeFinSolicitud(job, user);
        } else if (kw === 'ruc-activo') {
          actualizarProceso(user.currentProcess, { rucActivo: 'si', tsu: Date.now() });
          setUserParam('step', 'pn-1', job.contact, user.line)
          pushText(solicitarNumRuc(), job.contact, user.line)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //!Caso cuando tiene RUC activo
      //* Aqui me llega el numero de RUC
      //*Preguntar si se requiere que la firma contenga los datos del ruc, para facturación electrónica.
      case 'pn-1':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { numRUC: kw, rucActivo: 'si', tsu: Date.now() });
        setUserParam('step', 'pn-2', job.contact, user.line)
        pushText("*Esta firma sirve también para facturación electrónica*\n" + "Deseas que la firma contenga los datos del RUC, para facturación electrónica?", job.contact, user.line, null, "btn", [{ id: "rucactivo-si", title: "Si" }, { id: "rucactivo-no", title: "No" }])
        break

      //* Aqui me llega si desea o no que la firma contenga los datos del ruc
      case 'pn-2':
        var kw = body.toLowerCase()
        if (kw === 'rucactivo-no') {
          actualizarProceso(user.currentProcess, { st: 'sent', tsu: Date.now() });
          //crear Job syncFiles
          obtenerProceso(user.currentProcess, function (evidencias) {
            if (evidencias) {
              if (evidencias.terceraEdad === 'no') {
                syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack'])
              } else {
                syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack', 'videoTerceraEdad'])
              }
            } else {
              setUserParam('step', '0', job.contact, user.line)
              pushText(msgErrorInterno(), job.contact, user.line)
            }
          })
          //Finalizar proceso con st: sent
          finishProcess(job, user);
          mensajeFinSolicitud(job, user);
        } else if (kw === 'rucactivo-si') {
          setUserParam('step', 'pn-3', job.contact, user.line)
          pushText(solicitarDocumentoRucPDF(), job.contact, user.line)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //* Aqui me llega el documento del RUC en PDF y creo el job para hacer el post
      case 'pn-3':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'document') {
          convertirB64(envBot[account].TCAPIURL, user.line, job.msg.document.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { documentoPdfRuc: `evisImgs/b64${user.currentProcess}_documentoPdfRuc.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_documentoPdfRuc.txt`, resolve.b64)
              //crear Job syncFiles
              obtenerProceso(user.currentProcess, function (evidencias) {
                if (evidencias) {
                  if (evidencias.terceraEdad === 'no') {
                    syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack', 'documentoPdfRuc'])
                  } else {
                    syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack', 'documentoPdfRuc', 'videoTerceraEdad'])
                  }
                } else {
                  setUserParam('step', '0', job.contact, user.line)
                  pushText(msgErrorInterno(), job.contact, user.line)
                }
              })

              //Finalizar proceso con st: sent
              finishProcess(job, user);
              mensajeFinSolicitud(job, user);
            } else {
              pushText('Ocurrio un error', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        } else {
          pushText('Lo que usted envió no es un documento' + solicitarDocumentoRucPDF(), job.contact, user.line)
        }
        break
      //!Fin de casos cuando tiene RUC activo
      //!Fin persona natural
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //!Inicio representante legal
      //*Aqui me llega el nombre de la empresa y solicito el numero de RUC
      case 'rl-0':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { nombreEmpresa: kw, tsu: Date.now() });
        setUserParam('step', 'rl-1', job.contact, user.line)
        pushText('Por favor envíanos el numero de RUC de la empresa', job.contact, user.line)
        break

      //*Aqui me llega el numero de ruc de la empresa y pido el cargo del representante legal
      case 'rl-1':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { numRucEmpresa: kw, tsu: Date.now() });
        setUserParam('step', 'rl-2', job.contact, user.line)
        pushText('Por favor envíanos el cargo del representante legal en la empresa', job.contact, user.line)
        break

      //*Aqui me llega el cargo del representante legal y pido el nombramiento
      case 'rl-2':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { cargoRL: kw, tsu: Date.now() });
        setUserParam('step', 'rl-3', job.contact, user.line)
        pushText('Por favor envíanos el nombramiento en formato PDF', job.contact, user.line)
        break

      //* Aqui me llega el nombramiento en PDF y pido constitucion de la empresa en formato PDF
      case 'rl-3':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'document') {
          convertirB64(envBot[account].TCAPIURL, user.line, job.msg.document.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { nombramientoPdf: `evisImgs/b64${user.currentProcess}_nombramientoPdf.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_nombramientoPdf.txt`, resolve.b64)
              setUserParam('step', 'rl-4', job.contact, user.line)
              pushText('Por favor envíanos el documento PDF de la constitución de la empresa', job.contact, user.line)
            } else {
              pushText('Ocurrio un error', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        } else {
          pushText('Lo que nos enviaste no es un documento, por favor envíanos el documento solicitado', job.contact, user.line)
        }
        break

      //* Aqui me llega constitucion de la empresa en formato PDF y creo el job para postear
      case 'rl-4':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'document') {
          convertirB64(envBot[account].TCAPIURL, user.line, job.msg.document.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { constitucionPdf: `evisImgs/b64${user.currentProcess}_constitucionPdf.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_constitucionPdf.txt`, resolve.b64)
              //crear Job syncFiles
              obtenerProceso(user.currentProcess, function (evidencias) {
                if (evidencias) {
                  if (evidencias.terceraEdad === 'no') {
                    syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack', 'nombramientoPdf', 'constitucionPdf'])
                  } else {
                    syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack', 'nombramientoPdf', 'constitucionPdf', 'videoTerceraEdad'])
                  }
                } else {
                  setUserParam('step', '0', job.contact, user.line)
                  pushText(msgErrorInterno(), job.contact, user.line)
                }
              })
              //Finalizar proceso con st: sent
              finishProcess(job, user);
              mensajeFinSolicitud(job, user);
            } else {
              pushText('Ocurrio un error', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        } else {
          pushText('Lo que nos enviaste no es un documento, por favor envíanos el documento solicitado', job.contact, user.line)
        }
        break
      //!Fin representante legal
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //!Inicio miembro de empresa
      //*Aqui me llega el nombre de la empresa y pido el numero de RUC de la empresa
      case 'me-0':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { nombreEmpresa: kw, tsu: Date.now() });
        setUserParam('step', 'rl-1', job.contact, user.line)
        pushText('Por favor envíanos el numero de RUC de la empresa', job.contact, user.line)
        break

      //*Aqui me llega el numero de ruc de la empresa y pido el cargo del representante legal
      case 'me-1':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { numRucEmpresa: kw, tsu: Date.now() });
        setUserParam('step', 'me-3', job.contact, user.line)
        pushText('Por favor envíanos el cargo del representante legal en la empresa', job.contact, user.line)
        break

      //*Aqui me llega el cargo del representante legal y pido el motivo
      case 'me-3':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { cargoRL: kw, tsu: Date.now() });
        setUserParam('step', 'me-4', job.contact, user.line)
        pushText('Por favor especifica para que uso se necesita está firma electrónica.', job.contact, user.line)
        break

      //*Aqui me llega el motivo y pido la unidad
      case 'me-4':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { motivo: kw, tsu: Date.now() });
        setUserParam('step', 'me-5', job.contact, user.line)
        pushText('Por favor especifica en qué departamento o unidad trabajas.', job.contact, user.line)
        break

      //*Aqui me llega la unidad y pido nombres RL
      case 'me-5':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { unidadTrabajo: kw, tsu: Date.now() });
        setUserParam('step', 'me-6', job.contact, user.line)
        pushText('Por favor envíanos los nombres del representante legal de la empresa.', job.contact, user.line)
        break

      //*Aqui me llega nombres RL y pido apellidos RL
      case 'me-6':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { nombresRL: kw, tsu: Date.now() });
        setUserParam('step', 'me-7', job.contact, user.line)
        pushText('Por favor envíanos los apellidos del representante legal de la empresa.', job.contact, user.line)
        break

      //*Aqui me llega apellidos RL y pido el tipo de documento de RL
      case 'me-7':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { apellidosRL: kw, tsu: Date.now() });
        setUserParam('step', 'me-8', job.contact, user.line)
        pushText("Tipo de documento de identidad del Representante Legal de la empresa", job.contact, user.line, null, "btn", [{ id: "cedula", title: "Cédula" }, { id: "pasaporte", title: "Pasaporte" }])
        break


      //*Aqui me llega tipo de documento de RL y pido Numero del documento del RL
      case 'me-8':
        var kw = body.toLowerCase()
        if (["cedula"].indexOf(kw) >= 0) {
          actualizarProceso(user.currentProcess, { tipoDocumentoRL: 'CEDULA', tsu: Date.now() });
          setUserParam('step', 'me-8-num', job.contact, user.line)
          pushText('Por favor envíanos el número de cédula del representante legal.', job.contact, user.line)
        } else if (["pasaporte"].indexOf(kw) >= 0) {
          actualizarProceso(user.currentProcess, { tipoDocumentoRL: 'PASAPORTE', tsu: Date.now() });
          setUserParam('step', 'me-8-num', job.contact, user.line)
          pushText('Por favor envíanos el número de pasaporte del representante legal.', job.contact, user.line)
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*Aqui me llega el numero de documento del RL pido copia del documento de RL
      case 'me-8-num':
        var kw = body.toLowerCase()
        actualizarProceso(user.currentProcess, { numDocumentoRL: kw, tsu: Date.now() });
        setUserParam('step', 'me-9', job.contact, user.line)
        pushText("Por favor envíanos una copia de ambos lados del documento del representante legal.", job.contact, user.line, null, "btn", [{ id: "cedula", title: "Cédula" }, { id: "pasaporte", title: "Pasaporte" }])
        break

      //* Aqui me llega copia documento de identidad del representante legal y pido nombramiento
      case 'me-9':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'document') {
          convertirB64(envBot[account].TCAPIURL, user.line, job.msg.document.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { identidadPdfRL: `evisImgs/b64${user.currentProcess}_identidadPdfRL.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_identidadPdfRL.txt`, resolve.b64)
              setUserParam('step', 'me-10', job.contact, user.line)
              pushText('Por favor envíanos el nombramiento en formato PDF', job.contact, user.line)
            } else {
              pushText('Ocurrio un error', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        } else {
          pushText('Lo que nos enviaste no es un documento, por favor envíanos el documento solicitado', job.contact, user.line)
        }
        break

      //* Aqui me llega nombramiento y pido constitucion
      case 'me-10':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'document') {
          convertirB64(envBot[account].TCAPIURL, user.line, job.msg.document.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { nombramientoPdf: `evisImgs/b64${user.currentProcess}_nombramientoPdf.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_nombramientoPdf.txt`, resolve.b64)
              setUserParam('step', 'me-11', job.contact, user.line)
              pushText('Por favor envíanos el documento PDF de la constitución de la empresa', job.contact, user.line)
            } else {
              pushText('Ocurrio un error', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        } else {
          pushText('Lo que nos enviaste no es un documento, por favor envíanos el documento solicitado', job.contact, user.line)
        }
        break

      //*Aqui me llega la constitucion y pido la autorizacion
      case 'me-11':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'document') {
          convertirB64(envBot[account].TCAPIURL, user.line, job.msg.document.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { nombramientoPdf: `evisImgs/b64${user.currentProcess}_constitucionPdf.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_constitucionPdf.txt`, resolve.b64)
              setUserParam('step', 'me-12', job.contact, user.line)
              pushText('Por favor envíanos el documento PDF la autorización del representante legal para que el firmante solicite la firma electrónica', job.contact, user.line)
            } else {
              pushText('Ocurrio un error', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        } else {
          pushText('Lo que nos enviaste no es un documento, por favor envíanos el documento solicitado', job.contact, user.line)
        }
        break

      //* Aqui me llega la autorizacion y creo el job para el post
      case 'me-12':
        var kw = body.toLowerCase()
        if (job.msg['type'] === 'document') {
          convertirB64(envBot[account].TCAPIURL, user.line, job.msg.document.id).then((resolve) => {
            if (resolve.estado.status === 200) {
              actualizarProceso(user.currentProcess, { autorizacionPdf: `evisImgs/b64${user.currentProcess}_autorizacionPdf.txt`, tsu: Date.now() });
              fs.writeFileSync(`evisImgs/b64${user.currentProcess}_autorizacionPdf.txt`, resolve.b64)
              //crear el job syncFiles
              obtenerProceso(user.currentProcess, function (evidencias) {
                if (evidencias) {
                  if (evidencias.terceraEdad === 'no') {
                    syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack', 'identidadPdfRL', 'nombramientoPdf', 'constitucionPdf', 'autorizacionPdf'])
                  } else {
                    syncFilesJob(user, ['selfie', 'cedulaFront', 'cedulaBack', 'identidadPdfRL', 'nombramientoPdf', 'constitucionPdf', 'autorizacionPdf', 'videoTerceraEdad'])
                  }
                } else {
                  setUserParam('step', '0', job.contact, user.line)
                  pushText(msgErrorInterno(), job.contact, user.line)
                }
              })

              //Finalizar proceso con st: sent
              finishProcess(job, user);
              mensajeFinSolicitud(job, user);
            } else {
              pushText('Ocurrio un error', job.contact, user.line)
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ocurrio un error', job.contact, user.line)
          })
        } else {
          pushText('Lo que nos enviaste no es un documento, por favor envíanos el documento solicitado', job.contact, user.line)
        }
        break
      //!Fin miembro de empresa
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log(job)
    cb(false)
  }
}

function solicitarPrimerApellido() {
  var msg =
    'Por favor escribe tu primer apellido.\n\n'
  return msg
}

function solicitarSegundoApellido() {
  var msg =
    'Por favor escribe tu segundo apellido (En caso de no tenerlo escribe la palabra *continuar*).\n\n'
  return msg
}

function solicitarNombres() {
  var msg =
    'Por favor escribe tus nombres.\n\n'
  return msg
}

function solicitarFechaNacimiento() {
  var msg = 'Por favor digita tu fecha de nacimiento en formato *aaaa/mm/dd*.\n\n'
  return msg
}

function solicitarNumRuc() {
  var msg =
    'Por favor digita tu número RUC.\n\n'
  return msg
}

function solicitarCiudad() {
  var msg = 'Por favor indicanos tu *ciudad*.\n\n'
  return msg
}

function solicitarProvincia() {
  var msg = 'Por favor indicanos tu *provincia*.\n\n'
  return msg
}

function solicitarDocumentoRucPDF() {
  var msg = 'Por favor envíanos el documento PDF del RUC.\n\n'
  return msg
}

function mensajeFinSolicitud(job, user) {
  var str = 'Listo hemos terminado de llenar tu solicitud de firma, para seguir con el proceso revisa tu correo electrónico. Gracias por usar este servicio!'
  pushText(str, job.contact, user.line)
}
