var request = require('request');
var tools = require('./tools');
var bot = require('./botPlugin');
var _ = require('underscore');
var firebase = require('firebase');
var account = "demo";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 250;


//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+account+"/chats/"+job.contact.msisdn).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var sub = "#all";
		var group = "main";
		if (user.step) {step = user.step};
		if (user.group) {group = user.group};
		if (user.sub) {sub = user.sub};
		
		console.log ("User "+job.contact.msisdn+" is at step:", step, "sub:", sub, "group:", group);
		
		if (step != 'idle') {
				console.log ("Process MO ", job.msg.type);
				switch (job.msg.type) {
					case 'text':
						var body = job.msg.content.text;
						if (body.length>0) {
							//trim spaces
							body = body.trim();

							//catch operations
							if (body.toLowerCase().indexOf("#") == 0) {
								var parts = body.toLowerCase().split(" ");
								//match direct operation
								switch ( parts[0] ) {
									case "#salir":
										var str = "Gracias por usar el chatbot de Coniski!"
										pushText (str, job.contact.msisdn, user.line, user.cid);
										resetUserParam("step", job.contact.msisdn, user.line);
										break;
									case "#ping":
										var str = "#pong "+ new Date();
										pushText (str, job.contact.msisdn, user.line, user.cid);
										break;
									case "#persona":
										var cedula = parts[1];
										if (tools.isCedula(cedula)) {
											//call datofast and get data
											tools.datoFastHook(cedula, function(str, raw) {
												pushText (str, job.contact.msisdn, user.line)
											});
										} else {
											pushText ("No encontramos una persona con la cédula "+cedula, job.contact.msisdn, user.line)
										}
										break;
									case "#empresa":
										var ruc = parts[1];
										//call datofast and get data
										tools.datoFastHookEmpresa(ruc, function(str, raw) {
											pushText (str, job.contact.msisdn, user.line)
										});
										break;
									case "#placa":
										var placa = parts[1];
										//call datofast and get data
										tools.datoFastHookVehiculo(placa, function(str, raw) {
											pushText (str, job.contact.msisdn, user.line)
										});
										break;
									case "#btc":
										request('https://blockchain.info/tobtc?currency=USD&value=1', function (error, resp, body) {
											var btc = Number(body);
											pushText ( "BTC AVG = USD" + (1/btc).toFixed(2) , job.contact.msisdn, user.line )
										});
										break;
									case "#server":
										var str = "Server data:\n";
										str += "Datetime: "+ new Date()+"\n";
										str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
										str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
										pushText (str, job.contact.msisdn, user.line, user.cid);
										break
									case "#on":
										var str = "Bot logger started";
										pushText (str, job.contact.msisdn, user.line, user.cid);
										setUserParam("step","1", job.contact.msisdn, user.line);
										setUserParam("sub","#all", job.contact.msisdn, user.line);
										break;
									case "#menu":
										var str = "Haz click en una opción y luego enter:\n\n";
										str += "1. Consulta tu saldo\n"
										str += "https://wa.me/"+user.line+"?text=1\n\n";
										str += "2. Chatear con un agente\n"
										str += "https://wa.me/"+user.line+"?text=2\n\n";
										pushText (str, job.contact.msisdn, user.line, user.cid);
										setUserParam("step","3", job.contact.msisdn, user.line);
										break;
									case "#login":
										var pin = Math.floor(Math.random() * 99988877) + 56774892  
										var str = "https://logger.textcenter.net/"+pin;
										pushText (str, job.contact.msisdn, user.line, user.cid);
										setUserParam("pin",pin, job.contact.msisdn, user.line);
										break;
									case "#off":
										var str = "Bot logger stopped";
										pushText (str, job.contact.msisdn, user.line, user.cid);
										setUserParam("step","0", job.contact.msisdn, user.line);
										break;
									case "##":
										if (user.sub != "#all") {
											var str = user.sub + " subject closed";
											pushText (str, job.contact.msisdn, user.line, user.cid);
											setUserParam("step","1", job.contact.msisdn, user.line);
											setUserParam("sub","#all", job.contact.msisdn, user.line);
										}
										break;
									case "#lm":
										if (step == "0") {
											var str = "Match log started";
											pushText (str, job.contact.msisdn, user.line, user.cid);
											setUserParam("step","2", job.contact.msisdn, user.line);
											var d = new Date();
											var match = { id: d.getFullYear() + twoDigits(d.getMonth()+1) + twoDigits(d.getDate()) + twoDigits(d.getMinutes()) + (Math.floor(Math.random() * 100) + 1 ) }
											setUserParam("match", match, job.contact.msisdn, user.line);
										}
										break;
									case "#vm":
										if (user.match) {
											var str = JSON.stringify(user.match,null,2);
										} else {
											var str = "Match not found"
										}
										pushText (str, job.contact.msisdn, user.line, user.cid);
										break;
									case "#gm":
										if (parts[1]) {
											getMatch(parts[1], function(match) {
												pushText (match, job.contact.msisdn, user.line, user.cid);
											});
										}
										
										break;
									case "#sm":
										//validate
										var valid = true;
										var err = ""
										var match = user.match;
										if (!match.ps) {
											valid = false;
											err = "Players not set";
										} else if (match.ps.length<3 || match.ps.length>4) {
											valid = false;	
											err = "Must have 3 or 4 players";
										}
										
										if (!match.wd || !match.wp || !match.t) {
											valid = false;	
											err = "Winner and turn end not saved";
										}
										if (valid) {
											
											var str = "Match saved!\n\n\[link]\n\n"
											str += "Winner: *"+match.wd.toUpperCase()+"*\n";
											str += "Pilot: *"+match.wp.toUpperCase()+"*\n";
											str += "Ref: *https://wa.me/"+user.line+"?text=#gm%20"+match.id+"*\n";
											pushText (str, job.contact.msisdn, user.line, user.cid);
											setUserParam("step","0", job.contact.msisdn, user.line);
											setUserParam("match",{}, job.contact.msisdn, user.line);
											saveMatch (user.match);
										} else {
											var str = "Cannot save: incomplete match data\n\n"+err+"\n\n"+JSON.stringify(match,null,2);
											pushText (str, job.contact.msisdn, user.line, user.cid);
										}
										break;
									default:
										if (user.step == "1") {
											if (user.sub == "#all") {
												//start new subject
												var str = parts[0] + " subject open";
												pushText (str, job.contact.msisdn, user.line, user.cid);
												setUserParam("step","1", job.contact.msisdn, user.line);
												setUserParam("sub",parts[0], job.contact.msisdn, user.line);
												//find msg and label it
												setMsgSub (job.msg.id, job.contact.msisdn, parts[0])
											}
										}
									case "#nova":
										setUserParam("step","n0", job.contact.msisdn, user.line);
										bot.novaBot(body, job, user, "n0", group, sub);								
										break;
									case "#cart":
										setUserParam("step","c0", job.contact.msisdn, user.line);
										bot.cartBot(body, job, user, "c0", group, sub);								
										break;
									case "#habitat":
										setUserParam("step","u0", job.contact.msisdn, user.line);
										bot.usBot(body, job, user, "u0", group, sub);								
										break;
									case "#chat":
										updateUser({step:"idle", status: "active", group: "main", tso: Date.now()}, job.contact.msisdn, user.line);
										logStartTicket(job.contact.msisdn, user.chn);
										break;
										
								} //end switch parts
							} else {
								//value of MO received
								var kw = body.toLowerCase();
								//process step
								switch (step) {
									case "0":
										//logger is off
										console.log ("Logger is off, nothing was labelled");
										break
										//end step 0
									case "1":
										//logger is on, add MT to subject
										//find msg and label it
										setMsgSub (job.msg.id, job.contact.msisdn, user.sub)
										break;
										//end step 1
									case "2":
										//logging liga match
										var match = user.match;
										var str = ""
										var save = false;
										console.log ("Liga bot input:", kw)
										var players = ["fro","rim","raa","sed","anp","anj","fel","dao","ani","luc","juf","ben","jua","jab"]
										if (kw.indexOf("@")>0) {
											console.log ("log player");
											if (!match.ps) {
												match.ps = []
												}
											if (match.ps.length<5) {
												if (kw.indexOf("*")>0) {
													//is winner
													var parts = kw.split("@");	
													var p = {
														d: parts[0],
														p: parts[1].replace("*","")
													}
													if (players.indexOf(p.p)>=0) {
														match.ps.push(p)
														match.wd = parts[0]
														match.wp = parts[1].replace("*","")
														str = "D:"+p.d+" / P:"+p.p+" logged [*winner*]";
														save = true;
													} else {
														str = "Don't know player "+p.p;
													}
												} else {
													var parts = kw.split("@");	
													var p = {
														d: parts[0],
														p: parts[1]
													}
													if (players.indexOf(p.p)>=0) {
														match.ps.push(p)
														str = "D:"+p.d+" / P:"+p.p+" logged";
														save = true;
													} else {
														str = "Don't know player "+p.p;
													}
													
												}
											} else {
												str = "All seats filled";
											}
											
										}
										
										if (kw.indexOf("t:")==0) {
											console.log ("log turn");
											var parts = kw.split(":");
											str = "Turn end logged";
											match.t = parts[1];
											save = true;
										}
										
										if (kw.indexOf("rp:")==0) {
											console.log ("remove player");
											var parts = kw.split(":");
											var idx = Number(parts[1]);
											if (match.ps) {
												if (match.ps[idx-1]) {
													match.ps.splice(idx-1,1);
													str = "Player removed\n\n"+JSON.stringify(match.ps,null,2);
													save = true;	
												}
											}
											
										}
										
										if (kw.indexOf("draw")==0) {
											console.log ("log draw");
											str = "Draw logged";
											match.wd = "draw";
											match.wp = "draw";
											save = true;
										}
										
										if (str.length>0) {
											pushText (str, job.contact.msisdn, user.line, user.cid);	
											if (save) { setUserParam("match", match, job.contact.msisdn, user.line); }
										}
										break;
										//end step 2
									case "3":
										kw = kw.trim();
										if (["1","1.","uno","one","saldo"].indexOf(kw)>=0) {
											var str = "Tu saldo es *$" + Math.floor(Math.random() * 100) +"*\n\n";
											str += "Gracias por usar este servicio.\n"
											str += "Adios!.\n"
											pushText (str, job.contact.msisdn, user.line, user.cid);
											setUserParam("step","0", job.contact.msisdn, user.line);
										}
										if (["2","2.","dos","two","ayuda","help","chat","chatear"].indexOf(kw)>=0) {
											var str = "Listo, en breve te atendemos";
											pushText (str, job.contact.msisdn, user.line, user.cid);
											updateUser({step:"idle", status: "active", group: "main", tso: Date.now()}, job.contact.msisdn, user.line);
											logStartTicket(job.contact.msisdn, user.chn);
										}
										break;
									default:
										if (step.indexOf("n")==0) {
											bot.novaBot(body, job, user, step, group, sub);	
										}
										if (step.indexOf("u")==0) {
											bot.usBot(body, job, user, step, group, sub);	
										}
										if (step.indexOf("c")==0) {
											bot.cartBot(body, job, user, step, group, sub);	
										}
										
										
								}//end switch step
								
							} //end if operation
						}
						//job is completed
						finishJob(job);
						break;
					default:
						//finish jobs
						finishJob(job);
						break;
				} //end switch mo type
		} else { //end idle check
			console.log ('user '+job.contact.msisdn+'@'+user.line+' is idle');
			//is a reset operation?
			var body = job.msg.content.text;
			if (body.length>0) {
				//trim spaces
				body = body.trim();
				if (body.toLowerCase().indexOf("#") == 0) {
					if (body.toLowerCase() == "#reset") {
						var str = "Session reset OK"
						pushText (str, job.contact.msisdn, user.line, user.cid);
						resetUserParam("step", job.contact.msisdn, user.line);
					}
				}
			}
			//job is completed
			finishJob(job);
		}
	});
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	});
}

global.pushText = function  (body, contact, line, cid, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms

	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	if (cid) {mt.cid = cid; }
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.setUserParam = function(key, val, contact, line) {
	console.log (key, val, contact, line)
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).set(val);
}

global.updateUser = function(ob, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob );
}

global.logStartTicket = function(contact, chn) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).remove();
}

function setMsgSub (key, contact, val) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+key+"/sub").set(val);
}

function saveMatch (match) {
	var id = match.id;
	delete match.id;
	match.tsc = Date.now();
	match.st = "new"; //valid-1, valid, deleted, nulled
	firebase.database().ref("/accounts/"+account+"/matches/"+id).set(match);
}
function getMatch (mid, cb) {
	firebase.database().ref("/accounts/"+account+"/matches/"+mid).once('value', function(res) {
		if (res.val()) {
			var m = res.val()
			m.id = res.key;
			cb ( JSON.stringify(m,null,4) );
		} else {
			cb ( "Match not found" )	
		}
	});
}
function twoDigits(n) {
	if (n<10) {
		return "0"+n;
	} else {
		return ""+n;	
	}
}
