var request = require('request');
global.tools = require('./tools');
var fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
var plugin = require('./amiguiPlugin');
global._ = require('underscore');
global.firebase = require('firebase');
var CryptoJS = require("crypto-js");
var account = "amigui";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 500;
global.config = {}
global.calendars = []

//firebase init
var firebaseConfig = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
	"authDomain": "ubiku3.firebaseapp.com",
	"databaseURL": "https://ubiku3.firebaseio.com",
	"projectId": "ubiku3",
	"storageBucket": "ubiku3.appspot.com",
	"messagingSenderId": "946813430025",
	"appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(function (user) {
	if (user) {
		// User is signed in.
		console.log("Logged in to firebase as uid " + user.uid);
		setTimeout(function () {
			bootApp();
		}, 2000);

	} else {
		// User is signed out login again
		console.log("Session expired");
		loginTofirebase();
	}
	// ...
});

function loginTofirebase() {
	console.log("Login to firebase");
	firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
		var errorCode = error.code;
		var errorMessage = error.message;
		console.log(error.code + ": " + error.message);
	});
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		loadAccountConfig();
		setTimeout(function () {
			listenToJobs();
			processJobQueue();
		}, 1000);
	}
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/' + account + '/config/misc').on('value', function (snapshot) {
		config = snapshot.val();
		console.log("Config fetched for account", account);
	});
	firebase.database().ref('accounts/' + account + '/config/calendars').on('value', function (snapshot) {
		calendars = _.map(snapshot.val(), function (ob, key) {
			ob.id = key;
			return ob;
		});
		console.log("Calendars fetched for account", account);
	});
}
function listenToJobs() {
	firebase.database().ref("/accounts/" + account + "/botJobs/").orderByChild("st").equalTo("new").on('child_added', function (res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log(new Date(), "New job queued", job.id);
	});
}

function processJobQueue() {
	if (jobs.length > 0) {
		processJob(jobs.shift());
	} else {
		setTimeout(function () {
			processJobQueue();
		}, qitv);
	}
}

function processJob(job) {
	switch (job.type) {
		case 'send-notification':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				pushText(job.str, job.contact, user.line);
				//job is completed
				finishJob(job);
			});
			break;
		case 'reset-bot':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				resetBot(job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
		case 'booking-completed':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				//return main menu to user
				plugin.bot("", job, user, "999", user.group, account);
				//job is completed
				finishJob(job);
			});
			break;
		default:
			if (job.contact == "593991684677") {
				//job is completed
				finishJob(job);
			} else {

				firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
					var user = snapshot.val();
					var step = "0";
					var group = "main";
					if (user.step) { step = user.step };
					if (user.group) { group = user.group };

					if (user.tsso) {
						console.log("User " + job.contact + " is at step:", step, "group:", group);
					} else {
						startSession(job.contact, user);
						console.log("New Session for " + job.contact + "");
					}

					if (step != 'idle') {
						console.log("Process MO ", job.msg.type);
						var msgType = job.msg.type
						if (msgType == "chat") { msgType = "text" }
						if (msgType == "") { msgType = "text" }
						switch (msgType) {
							case 'text':
								if (job.msg.m) {
									var body = job.msg.m;
								} else {
									var body = job.msg.text.body;
								}
								console.log("Body", body);
								if (body.length > 0) {
									//trim spaces
									body = body.trim();

									//catch operations
									if (body.toLowerCase().indexOf("#") == 0) {
										var parts = body.toLowerCase().split(" ");
										//match direct operation
										switch (parts[0]) {
											case "#salir":
												salir(job, user);
												break;
											case "#ping":
												var str = "#pong " + new Date();
												pushText(str, job.contact, user.line);
												break;
											case "#server":
												var str = "Server data:\n";
												str += "Datetime: " + new Date() + "\n";
												str += "Ram: " + Math.round(os.totalmem() / 1000000) + "MB\n";
												str += "Free: " + Math.round(os.freemem() / 1000000) + "MB\n";
												pushText(str, job.contact, user.line);
												break;
											default:
												var str = "Operación desconocida";
												pushText(str, job.contact, user.line);
										} //end switch parts operation
									} else {
										if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
											salir(job, user);
										} else if (body.indexOf("#ts") > 0) {
											console.log(new Date(), "Running TS");
											var str = "#tsback " + new Date();
											pushText(str, job.contact, user.line);
										} else if (body.indexOf("Bot disabled") >= 0) {
											console.log(new Date(), "Ignore");
										} else {
											if (config.sessionExpire) {
												//check age of last MO
												var lapse = 0;
												if (user.lastMTts) {
													lapse = Date.now() - user.lastMTts;
												}

												if (lapse > config.sessionExpire) {
													console.log("Session is expired");
													console.log("lapse", lapse)
													resetBot(job.contact, user, null, function () {
														plugin.bot(body.toLowerCase(), job, user, "0", group, account);
													});
												} else {
													plugin.bot(body.toLowerCase(), job, user, step, group, account);
												}
											} else {
												//chat is not operation, run bot
												plugin.bot(body.toLowerCase(), job, user, step, group, account);
											}
										}
									} //end if operation
								} //body is empty
								//job is completed
								finishJob(job);
								break;


							case 'button':
								plugin.bot(job.msg.button.payload, job, user, step, group, account);
								//job is completed
								finishJob(job)
								break


							default:
								plugin.bot("[" + job.msg.type + "]", job, user, step, group, account);
								//job is completed
								finishJob(job);
								break;
						} //end switch mo type
					} else { //end idle check
						console.log('user ' + job.contact + '@' + user.line + ' is idle');
						//is a reset operation?
						if (job.msg.text) {
							var body = job.msg.text.body;
							if (body) {
								if (body.length > 0) {
									//trim spaces
									body = body.trim();
									if (body.toLowerCase().indexOf("#") == 0) {
										if (body.toLowerCase() == "#reset") {
											resetBot(job.contact, user, "Reset Session OK");
										}
									}
								}
							}
						}
						//job is completed
						finishJob(job);
					}
				});
			} //end block lines
	}//end switch job type
}

function finishJob(job) {
	firebase.database().ref("/accounts/" + account + "/botJobs/" + job.id + "/st/").set('done', function (res) {
		setTimeout(function () {
			processJobQueue();
		}, qitv);
	});
}

global.pageBreak = function () {
	return '\n\n--------------------------oooo--------------------------\n\n'
}

global.pushTemplate = function (params, templateName, contact, line) {

	var msgId = Date.now() + "-" + line + "-" + contact;
	var mt = { g: 0, m: "Nuevo ticket en Textcenter", rep: "bot", t: "mt", ts: Date.now(), type: "txt", st: "qed", lu: Date.now() };
	mt.isTemplate = true
	mt.lang = "es"; //todo read from DB
	mt.params = params.join("|");
	mt.templateName = templateName;
	firebase.database().ref("/accounts/" + account + "/conves/" + contact + "/msgs/" + msgId).set(mt).then(function () {
		var ob = _.clone(mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).set(ob).then(function () {
			console.log(new Date(), "Template Response POSTED OK");
		}).catch(function (error) {
			console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function (error) {
		console.log(new Date(), "Push MTQ Error", error);
	});

}

global.pushText = function (body, contact, line, rep, type, desc, url, b64, cb) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	var msgId = Date.now() + "-" + line + "-" + contact;
	var mt = { g: 0, m: body, rep: rep, t: "mt", ts: Date.now(), type: type, st: 'qed', lu: Date.now() };
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	firebase.database().ref("/accounts/" + account + "/conves/" + contact + "/msgs/" + msgId).set(mt).then(function () {
		if (cb) { cb(); }
		var ob = _.clone(mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).set(ob).then(function () {
			console.log(new Date(), "MT Response POSTED OK");
		}).catch(function (error) {
			console.log(new Date(), "Push MTQ Error", error);
		});
		//update timestamp of last mt sent
		firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/lastMTts").set(Date.now())
	}).catch(function (error) {
		console.log(new Date(), "Push MTQ Error", error);
	});

}

global.setUserParam = function (key, val, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/" + key).set(val);
}

global.updateUser = function (ob, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/").update(ob);
}

function startSession(contact, user) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/tsso").set(Date.now());
	logStartSession(contact, user.chn);
}

global.logStartSession = function (contact, chn) {
	var log = {
		type: "session-start",
		ts: Date.now(),
		ref: contact + "",
		chn: chn
	}
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
}

global.logEndSession = function (contact, chn, tsso, op) {
	var log = {
		type: "session-end",
		tssx: Date.now(),
		ref: contact + "",
		ts: Date.now(),
		op: op
	}
	if (tsso) { log.tsso = tsso; }
	if (chn) { log.chn = chn; }
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
}

global.logStartTicket = function (contact, chn, line) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact + "",
		chn: chn
	}
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
	//notify of new ticket
	var nts = config.notifyTickets;
	if (nts) {
		var arr = nts.split(",");
		for (var i = 0; i < arr.length; i++) {
			pushTemplate(["https://v3.textcenter.net/#inbox?c=" + contact], "new_ticket_created", arr[i].trim(), line);
		}

	}
}

global.resetUserParam = function (key, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/" + key).remove();
}

global.saveForm = function (ob, cb) {
	firebase.database().ref("/accounts/" + account + "/forms").push(ob).then(function (res) {
		cb(res.key);
	});
}

global.salir = function (job, user) {
	var self = this;
	logEndSession(job.contact, user.chn, user.tsso, "s");
	var str = "Gracias por contactarnos 🙂"
	pushText(str, job.contact, user.line);
	resetUserParam('step', job.contact);
	resetUserParam('pin', job.contact);
	resetUserParam('condos', job.contact);
	resetUserParam('group', job.contact);
	resetUserParam('account', job.contact);
	resetUserParam('tsso', job.contact);
	setUserParam('status', 'reset', job.contact);

}

global.resetBot = function (contact, user, msg, cb) {
	if (user.tsso) { logEndSession(contact, user.chn, user.tsso, "r"); }
	if (msg) { pushText(msg, contact, user.line); }
	resetUserParam('step', contact);
	resetUserParam('pin', contact);
	resetUserParam('condos', contact);
	resetUserParam('group', contact);
	resetUserParam('account', contact);
	resetUserParam('tsso', contact);
	setUserParam('status', 'reset', contact);
	if (cb) { cb(); }
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()
}

global.getContact = function (phone, cb) {
	console.log("Searching for", phone);
	firebase.database().ref("/accounts/" + account + "/contacts").orderByChild("phone").equalTo(phone + '').once("value", function (res) {
		if (res.val()) {
			var arr = _.map(res.val(), function (ob, key) {
				ob.id = key;
				return ob;
			});
			console.log("Contact found", arr.length);
			var ob = arr[0];
			if (ob.group) {
				cb(ob)
			} else {
				cb(null);
			}
		} else {
			cb(null);
		}
	}).catch(function (error) {
		console.log(error)
		cb(null);
	});
}

global.isShopOpen = function () {
	var d = new Date();
	var strDate = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear()
	var day = d.getDay();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (config.holidays.indexOf(strDate) >= 0) {
		return false;
	} else {
		if (config.workDays[day]) {
			if (t >= config.officeOpen && t < config.officeClose) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}

global.getEventListPerContact = function (user, cb) {
	console.log("Searching events ", user.id);
	firebase.database().ref("/accounts/" + account + "/events").orderByChild("account").equalTo(user.id).once("value", function (res) {
		if (res.val()) {
			var arr = _.map(res.val(), function (ob, key) {
				ob.id = key;
				return ob;
			});
			var activas = [];
			for (var i = 0; i < arr.length; i++) {
				if ((arr[i].st == "acc" || arr[i].st == "new") && arr[i].dateStart >= (Date.now() - 86400000)) {
					activas.push(arr[i])
				}
			}
			console.log("Events found", activas.length);
			cb(activas);
		} else {
			cb([]);
		}
	}).catch(function (error) {
		console.log(error)
		cb([]);
	});
}

global.updateEvent = function (evId, ob, cb) {
	firebase.database().ref('accounts/' + account + '/events/' + evId).update(ob).then(function () {
		console.log(new Date(), "Event updated OK", evId);
		if (cb) { cb(true); }
	}).catch(function (error) {
		console.log(new Date(), "Update event error");
		console.log(error)
		if (cb) { cb(false); }
	});
}

global.makeCitaLinks = function (job, user) {
	var pin = Math.floor(Math.random() * 625500) + 100000;
	var authStr1 = encr(job.contact + "|amigui|res01|cit|" + pin + "|" + Date.now() + "|blank.png");
	var authStr2 = encr(job.contact + "|amigui|res02|cit|" + pin + "|" + Date.now() + "|blank.png");
	var authStr3 = encr(job.contact + "|amigui|res03|cit|" + pin + "|" + Date.now() + "|blank.png");
	var authStr4 = encr(job.contact + "|amigui|res04|cit|" + pin + "|" + Date.now() + "|blank.png");
	var authStr5 = encr(job.contact + "|amigui|res05|cit|" + pin + "|" + Date.now() + "|blank.png");
	var authStr6 = encr(job.contact + "|amigui|res06|cit|" + pin + "|" + Date.now() + "|blank.png");
	makeShortURL(authStr1, function (str1) {
		makeShortURL(authStr2, function (str2) {
			makeShortURL(authStr3, function (str3) {
				makeShortURL(authStr4, function (str4) {
					makeShortURL(authStr5, function (str5) {
						makeShortURL(authStr6, function (str6) {
							var msg = "*Haz click* en el enlace del centro de compras de tu preferencia, de esa forma te enviaremos al calendario para agendar tu cita:\n\n";
							msg += "*Quito Norte*\n";
							msg += "Eloy Alfaro N39-313 y Gaspar de Villarroel Tel: 02-2443243\n";
							msg += "https://events.textcenter.net/#home/SC@" + str1 + "\n\n";
							msg += "*Cumbayá*\n";
							msg += "Calle Bruneleschi E7199 Primavera 1. Frente a la PESEBRERA Tel:0979113502\n";
							msg += "https://events.textcenter.net/#home/SC@" + str2 + "\n\n";
							msg += "*Los Chillos*\n";
							msg += "Av. Ilaló y Río Tiputini, Quimbita's Plaza, local 9  Tel: 02-5139811\n";
							msg += "https://events.textcenter.net/#home/SC@" + str3 + "\n\n";
							msg += "*Cuenca*\n";
							msg += "Calle Padre Matovelle 4-26 entre Manuel M. Palacios y Agustín Cueva\n";
							msg += "https://events.textcenter.net/#home/SC@" + str4 + "\n\n";
							msg += "*Ambato*\n";
							msg += "Avenida Miraflores 11-53 y las Retamas. Oficina Domus Business Lab, al lado de Cárnicos Berlín\n";
							msg += "https://events.textcenter.net/#home/SC@" + str5 + "\n\n";
							msg += "*Ibarra*\n";
							msg += "Obando luna 5- 20 Entre Sucre y Jacinto Egas, diagonal a Neurocenter\n";
							msg += "https://events.textcenter.net/#home/SC@" + str6 + "\n\n";
							var ob = { pin: pin, step: "3", group: "ven" }
							updateUser(ob, job.contact, user.line);
							pushText(msg, job.contact, user.line);
						});
					});
				});
			});
		});
	});
}

global.makeShortURL = function (authStr, cb) {
	firebase.database().ref('/shorts/').push(authStr).then(function (res) {
		console.log("Short Pushed", res.key)
		cb(res.key)
	})
		.catch(function (error) {
			cb("err")
		});
}

global.formatHours = function (n) {
	if (n < 1000) {
		n += ""
		return n.substring(0, 1) + "h" + n.substring(1, 3);
	} else {
		n += ""
		return n.substring(0, 2) + "h" + n.substring(2, 4);
	}
}
