
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' urbaparkMenu3', user.id, step, group)
    console.log(job)
    switch (step) {
      // Me llega el saludo y el menu principal
      case "0":
        updateUser({ step: '1', module: "urbaparkMenu3" }, job.contact);
        pushText(mainMenu3(true), job.contact, user.line)
        break;

      case "0.1":
        updateUser({ step: '1', module: "urbaparkMenu3" }, job.contact);
        pushText(mainMenu3(false), job.contact, user.line)
        break;

      case "1":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          updateUser({ step: '1.1' }, job.contact);
          pushText(menu11(), job.contact, user.line)
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          updateUser({ step: '1.2' }, job.contact);
          pushText(menu12(), job.contact, user.line)
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          updateUser({ step: '1.3' }, job.contact);
          pushText(menu13(), job.contact, user.line)
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0 || ["0", "0.", "cero"].indexOf(kw) >= 0) {
          mods["main"].bot('Volver menu principal', job, user, "0.1")
        } else {
          pushText("Por favor selecciona una de las opciones del menú, escribe *0* para regresar al menú principal o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "1.1":
        var kw = body.toLowerCase()
        var msg = ''
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //Foto centros comerciales Quito
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          //Foto hospitales Quito
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          //Foto hoteles Quito
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          //Foto aeropuertos Quito
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
          //Foto otros puntos interes Quito
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["6", "6.", "seis"].indexOf(kw) >= 0) {
          mods["urbaparkMenu3"].bot('Volver atras', job, user, "0.1")
        } else if (["0", "0.", "cero"].indexOf(kw) >= 0) {
          mods["main"].bot('Volver menu principal', job, user, "0.1")
        } else {
          pushText("Por favor selecciona una de las opciones del menú, escribe *0* para regresar al menú principal o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "1.2":
        var kw = body.toLowerCase()
        var msg = ''
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //Foto centros comerciales Guayaquil
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          //Foto hospitales Guayaquil
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          //Foto hoteles Guayaquil
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          //Foto otros puntos interes Guayaquil
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
          mods["urbaparkMenu3"].bot('Volver atras', job, user, "0.1")
        } else if (["0", "0.", "cero"].indexOf(kw) >= 0) {
          mods["main"].bot('Volver menu principal', job, user, "0.1")
        } else {
          pushText("Por favor selecciona una de las opciones del menú, escribe *0* para regresar al menú principal o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

        case "1.3":
        var kw = body.toLowerCase()
        var msg = ''
        //Foto Lima
        if (isShopOpen()) {
          msg = 'Uno de nuestros especialistas se comunicará contigo.'
        } else {
          msg = config.horarioStr
        }
        updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
        pushText(msg, job.contact, user.line)
        logStartTicket(job.contact, user.chn, user, "sac");
        break;

      case "2":
        var kw = body.toLowerCase()
        if (["0", "0.", "cero"].indexOf(kw) >= 0) {
          mods["main"].bot('Volver menu principal', job, user, "0.1")
        } else {
          pushText("Por favor escribe *0* para regresar al menu principal o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo igorMenu2")
    console.log(job)

    cb({ st: "done" })
  }
}

function mainMenu3(greet) {
  if (greet) {
    var msg = "¡Claro! Te brindaremos toda la información que necesitas sobre nuestra red de parqueaderos, incluyendo puntos de ubicación, tarifas y horarios. Por favor, selecciona una de las siguientes opciones:\n\n";
  } else {
    var msg = "Por favor, selecciona una de las siguientes opciones:\n\n"
  }
  msg += "1. Quito\n";
  msg += "2. Guayaquil\n";
  msg += '3. Lima \n';
  msg += '4. Volver al Menú Principal\n\n';
  msg += 'Si deseas regresar al Menú Principal en cualquier momento, simplemente ingresa *0*\n';

  return msg
}

function menu11() {
  var msg = 'Perfecto, has seleccionado la opción "Quito". A continuación, te presento los puntos disponibles en Quito:\n\n';
  msg += "1. Centros Comerciales\n";
  msg += "2. Hospitales\n";
  msg += '3. Hoteles\n';
  msg += '4. Aeropuerto\n';
  msg += '5. Otros puntos de interés \n';
  msg += '6. Volver atrás';

  return msg
}

function menu12() {
  var msg = 'Perfecto, has seleccionado la opción "Guayaquil". A continuación, te presento los puntos disponibles en Guayaquil:\n\n';
  msg += "1. Centros Comerciales\n";
  msg += "2. Hospitales\n";
  msg += '3. Hoteles\n';
  msg += '4. Otros puntos de interés \n';
  msg += '5. Volver atrás';

  return msg
}

function menu13() {
  var msg = 'Perfecto, has seleccionado la opción "Lima". A continuación, te presento los puntos disponibles en Lima:\n\n';

  return msg
}



