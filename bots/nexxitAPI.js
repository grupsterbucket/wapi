module.exports = {
  verificarCedula: async function (numTelf, numCedula, codigoDactilar) {
    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'x-nexxit-key': envBot[account].NexxitApiKey
      },
      body: JSON.stringify({
        userRef: numTelf,
        cedula: numCedula,
        codigoDactilar: codigoDactilar
      })
    }
    const respuestaVerificarCedula = await fetch(envBot[account].NexxitAPIURL + '/dni-validate', options)
    const respuestaVerificarCedulaFinal = await respuestaVerificarCedula.text()

    if (tools.isJsonString(respuestaVerificarCedulaFinal)) {
      return JSON.parse(respuestaVerificarCedulaFinal)
    } else {
      return { status: 502 }
    }
  },

  verificarDniOcr: async function (TCAPIURL, linea, id) {
    const response = await fetch(TCAPIURL + "getmediaurl/" + linea + "/" + id)
    let data = await response.text()

    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'x-nexxit-key': envBot[account].NexxitApiKey
      },
      body: JSON.stringify({
        userRef: 'bot',
        image: data,
      })
    }

    const respuestaDniOcr = await fetch(envBot[account].NexxitAPIURL + '/dni-ocr', options)
    const respuestaFinalDniOcrObj = await respuestaDniOcr.text()

    if (tools.isJsonString(respuestaFinalDniOcrObj)) {
      return respuesta = {
        results: JSON.parse(respuestaFinalDniOcrObj),
        imagen: data
      }
    } else {
      return respuesta = {
        results: { status: 502 },
        imagen: data
      }
    }
  },

  faceMenton: async function (TCAPIURL, linea, id, contact, cb) {
    console.log(new Date(), 'Download URL', TCAPIURL + "getmediaurl/" + linea + "/" + id)
    const response = await fetch(TCAPIURL + "getmediaurl/" + linea + "/" + id)
    let data = await response.text()
	console.log(new Date(), 'Download completed', data.length)
    

    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'x-nexxit-key': envBot[account].NexxitApiKey
      },
      body: JSON.stringify({
        userRef: contact,
        image: data
      })
    }

    const respuestaFaceMenton = await fetch(envBot[account].NexxitAPIURL + '/validate-face-menton', options)
    const respuestaFinalFaceMentonObj = await respuestaFaceMenton.text()

    console.log(respuestaFinalFaceMentonObj)
    if (tools.isJsonString(respuestaFinalFaceMentonObj)) {
      cb({
        results: JSON.parse(respuestaFinalFaceMentonObj),
        imagen: data
      })
    } else {
      cb({
        results: { status: 502, details: { msg: 'Error interno.'} },
        imagen: data
      })
    }
  },
  
  verificarCompareFaces: async function (TCAPIURL, linea1, id1, linea2, id2) {

    const response = await fetch(TCAPIURL + "getmediaurl/" + linea1 + "/" + id1)
    let data1 = await response.text()

    const res = await fetch(TCAPIURL + "getmediaurl/" + linea2 + "/" + id2)
    let data2 = await res.text()

    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'x-nexxit-key': envBot[account].NexxitApiKey
      },
      body: JSON.stringify({
        source: data1,
        target: data2,
        userRef: 'bot'
      })
    }
    const respuestaCompareFaces = await fetch(envBot[account].NexxitAPIURL + '/compare-faces', options)
    const respuestaFinal = await respuestaCompareFaces.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: 502 }
    }
  },

  verificarCompareFacesImgs: async function (img1B64, img2B64, userRef) {

    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'x-nexxit-key': envBot[account].NexxitApiKey
      },
      body: JSON.stringify({
        source: img1B64,
        target: img2B64,
        userRef: userRef
      })
    }
    const respuestaCompareFaces = await fetch(envBot[account].NexxitAPIURL + '/compare-faces', options)
    const respuestaFinal = await respuestaCompareFaces.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: 502 }
    }
  },


  requestLiveness: async function (TCAPIURL, linea, id) {
    const response = await fetch(TCAPIURL + "getmediaurl/" + linea + "/" + id)
    var data = await response.text()

    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'x-nexxit-key': envBot[account].NexxitApiKey
      },
      body: JSON.stringify({
        userRef: 'bot',
        image: data
      })
    }

    const respuesta = await fetch(envBot[account].NexxitAPIURL + '/liveness/request', options)
    const respuestaFinal = await respuesta.text()
    if (tools.isJsonString(respuestaFinal)) {
      return respuestaLiveness = {
        results: JSON.parse(respuestaFinal),
        imagen: data
      }
    } else {
      return respuestaLiveness = {
        results: { status: 502 },
        imagen: data
      }
    }
  },

  verificarLiveness: async function (analyse_id, api_evidence) {

    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'x-nexxit-key': envBot[account].NexxitApiKey
      },
      body: JSON.stringify({
        analyze_id: analyse_id,
        api_evidence: api_evidence
      })
    }

    const respuesta = await fetch(envBot[account].NexxitAPIURL + '/liveness/check', options)
    const respuestaFinal = await respuesta.text()
    if (tools.isJsonString(respuestaFinal)) {
      return respuestaLiveness = {
        results: JSON.parse(respuestaFinal)
      }
    } else {
      return respuestaLiveness = {
        results: { status: 502 },
        imagen: data
      }
    }
  },

  getWSPImageB64: async function (TCAPIURL, linea, id) {
    console.log(new Date(), 'Descargar URL', TCAPIURL + "getmediaurl/" + linea + "/" + id)
    const response = await fetch(TCAPIURL + "getmediaurl/" + linea + "/" + id)
    var data = await response.text()
    console.log (new Date(), "Descarga terminada")
    return data
  },
  
  convertirB64: async function (TCAPIURL, linea, id) {
    const response = await fetch(TCAPIURL + "getmediaurl/" + linea + "/" + id)
    console.log('URL', TCAPIURL + "getmediaurl/" + linea + "/" + id)
    var data = await response.text()

    return datos = {
      b64: data,
      estado: response
    }
  },

  OneShot: async function (opciones) {
    const respuestaOneShot = await fetch(envBot[account].NexxitAPIURL + '/create-oneshot-request', opciones)
    const respuestaFinalOneShot = await respuestaOneShot.text()
    console.log (respuestaFinalOneShot)
    if (tools.isJsonString(respuestaFinalOneShot)) {
      return JSON.parse(respuestaFinalOneShot)
    } else {
      return { status: 502 }
    }
  },

  RetrieveContract: async function (opciones) {
    const respuestaOneShot = await fetch(envBot[account].NexxitAPIURL + '/get-oneshot-contract', opciones)
    const respuestaFinalOneShot = await respuestaOneShot.text()
    console.log (respuestaFinalOneShot)
    if (tools.isJsonString(respuestaFinalOneShot)) {
      return JSON.parse(respuestaFinalOneShot)
    } else {
      return { status: 502 }
    }
  },
  
  UploadDoc: async function (opciones) {
    const respuestaOneShot = await fetch(envBot[account].NexxitAPIURL + '/upload-oneshot-document', opciones)
    const respuestaFinalOneShot = await respuestaOneShot.text()
    console.log (respuestaFinalOneShot)
    if (tools.isJsonString(respuestaFinalOneShot)) {
      return JSON.parse(respuestaFinalOneShot)
    } else {
      return { status: 502 }
    }
  },
  
  GenerateQR: async function (opciones) {
    const respuestaOneShot = await fetch(envBot[account].NexxitAPIURL + '/oneshot-generateQR', opciones)
    const respuestaFinalOneShot = await respuestaOneShot.text()
    console.log (respuestaFinalOneShot)
    if (tools.isJsonString(respuestaFinalOneShot)) {
      return JSON.parse(respuestaFinalOneShot)
    } else {
      return { status: 502 }
    }
  },
  
  RequestOTP: async function (opciones) {
    const respuestaOneShot = await fetch(envBot[account].NexxitAPIURL + '/oneshot-requestOTP', opciones)
    const respuestaFinalOneShot = await respuestaOneShot.text()
    console.log (respuestaFinalOneShot)
    if (tools.isJsonString(respuestaFinalOneShot)) {
      return JSON.parse(respuestaFinalOneShot)
    } else {
      return { status: 502 }
    }
  },
  
  OneShotSignPDF: async function (opciones) {
    const respuestaOneShot = await fetch(envBot[account].NexxitAPIURL + '/oneshot-signdocs', opciones)
    const respuestaFinalOneShot = await respuestaOneShot.text()
    console.log (respuestaFinalOneShot)
    if (tools.isJsonString(respuestaFinalOneShot)) {
      return JSON.parse(respuestaFinalOneShot)
    } else {
      return { status: 502 }
    }
  },
  
  OneShotGetSignedPDF: async function (opciones) {
    const respuestaOneShot = await fetch(envBot[account].NexxitAPIURL + '/retrieve-document', opciones)
    const respuestaFinalOneShot = await respuestaOneShot.text()
    if (tools.isJsonString(respuestaFinalOneShot)) {
      return JSON.parse(respuestaFinalOneShot)
    } else {
      return { status: 502 }
    }
  },
  
  enviarCorreoOTP: async function (correo) {

    let pin = tools.generatePin()
    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'x-nexxit-key': envBot[account].NexxitApiKey
      },
      body: JSON.stringify({
        to: correo,
        from: 'soporte@nexxit.dev',
        subject: 'Código de Seguridad '+envBot[account].name,
        body: 'Hola, este es tu código de seguridad para validar tu correo:' + ' ' + pin,
        channel: 'mailgun'
      })
    }

    const respuestaMail = await fetch(envBot[account].NexxitAPIURL + '/send-otp', options)
    const respuestaMailFinal = await respuestaMail.text()
	console.log (respuestaMailFinal)
    if (tools.isJsonString(respuestaMailFinal)) {
      return resultados = {
        results: JSON.parse(respuestaMailFinal),
        pin: pin
      }
    } else {
      return { status: 502 }
    }
  }
}
