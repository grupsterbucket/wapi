//PROXY for DKManagement - DMOL
var _ = require('underscore');
var fs = require('fs');
var atob = require('atob');
var tiendas = [];
var csv = "";
loadTiendas( bootApp );

function bootApp() {
		console.log ("Creating dump");
		console.log ("Create Header");
		var obj = JSON.parse(atob(tiendas[0].data));
		var header = []
		for (var prop in obj) {
			//console.log (prop)
			if (Object.prototype.hasOwnProperty.call(obj, prop)) {
				header.push(prop)
			}
		}
		var rows = []
		rows.push (header.join("\t"))
		for (var i=0; i<tiendas.length; i++) {
			var t = tiendas[i]
			var obj = JSON.parse(atob(t.data));
			var row = []
			for (var prop in obj) {
				if (Object.prototype.hasOwnProperty.call(obj, prop)) {
					if (obj[prop]) {
						row.push(obj[prop])
					} else {
						row.push("");
					}
				}
			}
			rows.push(row.join("\t").replace(/\n/g," / n"))
		}
		csv = rows.join("\n")
		fs.writeFile('tiendas.csv', csv, function (err) {
		  if (err) return console.log(err);
		  console.log (csv.length)
		  console.log ("Done!");
		});
		

} //end boot app


function loadTiendas(cb) {
	
	//load tiendas json
	fs.readFile('tiendas.json', 'utf8', function (err, data) {
		if (err) {
			console.log("Cannot load tiendas file") 
		    console.log(err)
		    cb();
		} else {
			tiendas = _.map(JSON.parse(data), function(ob, key) { 
				ob.id = key;
				return ob;
				});
			console.log (tiendas.length, "tiendas loaded");
			cb();
	    }
	});
}
