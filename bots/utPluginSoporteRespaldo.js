// SOP Bot as module
module.exports = {
  bot: function (body, job, user, step, group, account) {
    console.log("Processing uanataca sop bot", step, group)
    switch (step) {
      case "0":
        if (config.whiteList.includes(user.id) || config.whiteListPiccolos.includes(user.id)) {
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(mainMenu(true), job.contact, user.line);
        } else {
          pushText('Tu número telefónico no tiene acceso a este chat.\nMayor información contacta a tu asesor.', job.contact, user.line);
          salir(job, user);
        }
        break;
      //end step 0
      case "sop1":
        var kw = body.toLowerCase()
        var group = ""
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          if (config.whiteList.includes(user.id)) {
            setUserParam("step", "sop1.1", job.contact, user.line);
            pushText(distri(), job.contact, user.line);
          } else if (config.whiteListPiccolos.includes(user.id)) {
            setUserParam("step", "sop1.1.piccolos", job.contact, user.line);
            pushText(piccolos(), job.contact, user.line);
          } else {
            pushText('Tu número telefónico no tiene acceso a este chat.\nMayor información contacta a tu asesor.', job.contact, user.line);
            salir(job, user);
          }
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          setUserParam("step", "sop3.1", job.contact, user.line);
          pushText(oneShot(), job.contact, user.line);

        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;
      case "sop1.1":
        var kw = body.toLowerCase()
        var group = ""
        //reenvio de email de descarga automatico
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {

          var msg = "El mail de descarga se reenvía solo en status *APROBADO*\n\nIngrese el número de cédula sin guiones ni espacios."
          pushText(msg, job.contact, user.line);
          setUserParam("step", "sop-rcorreo-descarga", job.contact, user.line);

          //cancelación automatica
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {

          var msg = "Solo en los siguientes status podrá cancelar una solicitud,\nescoja una opcion:\n\n1. Nuevo\n2. Aprobado\n3. Regresar al Menú anterior"
          pushText(msg, job.contact, user.line);
          setUserParam("step", "sop-cancel", job.contact, user.line);

          //reenvio de credenciales automatico
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {

          var msg = "Solo en status *EMITIDO* se puede reenviar credenciales:\n\nIngrese el número de cédula sin guiones ni espacios."
          pushText(msg, job.contact, user.line);
          setUserParam("step", "sop1.1.1", job.contact, user.line);

          //Revocar
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {

          setUserParam("step", "sop1.1.3", job.contact, user.line);
          pushText(revocar(), job.contact, user.line);

          //Estado solicitud
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {

          var msg = "Ingrese el número de cédula o pasaporte, sin guiones ni espacios."
          pushText(msg, job.contact, user.line);
          setUserParam("step", "sop-estado-solicitud", job.contact, user.line);

          //Contactar asesor
        } else if (["6", "6.", "seis"].indexOf(kw) >= 0) {

          if (isShopOpen()) {
            var msg = "Por favor indícanos cuál es tu requerimiento"
            updateUser({ step: "idle", status: "active", group: "sod", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sod");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }
        } else if (["7", "7.", "siete"].indexOf(kw) >= 0) {

          // main menu
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(mainMenu(false), job.contact, user.line);

        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      // !Reenvio de credenciales automatico
      //*Llega numero de cedula
      case "sop1.1.1":
        var kw = body.toLowerCase()
        pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
        updateUser({ step: "sop1.1.1-0", ced: kw }, job.contact, user.line);
        break

      //*Llega el tipo de perfil
      case "sop1.1.1-0":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          utapi.consultarFirmas(user.ced, '1', user.id).then((res) => {
            if (res.result) {
              pushText(fechasFirmas(res.firmas, 'Persona Natural', [], job, user), job.contact, user.line);
              updateUser({ step: "sop1.1.1-1", tipo_perfil: "1", perfil: 'Persona Natural' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-rcredenciales-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          utapi.consultarFirmas(user.ced, '2', user.id).then((res) => {
            if (res.result) {
              pushText(fechasFirmas(res.firmas, 'Representante Legal', [], job, user), job.contact, user.line);
              updateUser({ step: 'sop1.1.1-1', tipo_perfil: '2', perfil: 'Representante Legal' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-rcredenciales-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          utapi.consultarFirmas(user.ced, '3', user.id).then((res) => {
            if (res.result) {
              pushText(fechasFirmas(res.firmas, 'Miembro de empresa', [], job, user), job.contact, user.line);
              updateUser({ step: "sop1.1.1-1", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-rcredenciales-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*llega la firma escogida
      case "sop1.1.1-1":
        var kw = body.toLowerCase()
        if (!isNaN(Number(kw))) {
          if (Number(kw) < 1 || Number(kw) > user.firmas.length + 1) {
            pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
          } else {
            if (Number(kw) == user.firmas.length + 1) {
              // main menu
              setUserParam("step", "sop1", job.contact, user.line);
              pushText(mainMenu(false), job.contact, user.line);
            } else {
              //reenvio de credenciales
              utapi.reenvioCredenciales(user.ced, user.firmas[Number(kw) - 1], user.id).then((res) => {
                if (res.result) {
                  //credenciales reenviadas
                  setUserParam("step", "sop-rcredenciales-menu-final", job.contact, user.line);
                  pushText(res.resultado + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
                } else {
                  //resultado negativo para la cosulta
                  pushText(res.resultado, job.contact, user.line);
                  salir(job, user);
                }
              }).catch((err) => {
                console.log(err)
                pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
                salir(job, user);
              })
            }
          }
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break

      case 'sop-rcredenciales-wait':
        var kw = body.toLowerCase()
        //volver a ingresar cedula
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          var msg = "Ingrese el número de cédula sin guiones ni espacios."
          pushText(msg, job.contact, user.line);
          setUserParam("step", "sop1.1.1", job.contact, user.line);
          //regresar al menu principal
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1.1", job.contact, user.line);
          pushText(distri(), job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break

      case 'sop-rcredenciales-menu-final':
        var kw = body.toLowerCase()
        //Regresar al menu principal
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1.1", job.contact, user.line);
          pushText(distri(), job.contact, user.line);
          //Salir
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          salir(job, user);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break
      //! Fin reenvio de credenciales automatico


      // !Cancelacion de solicitud
      //*LLega estado de la solicitud 1.Nuevo / 2.Aprobado / 3.Regresar al menu principal
      case "sop-cancel":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          pushText("Ingrese el número de cédula o pasaporte sin guiones ni espacios", job.contact, user.line);
          updateUser({ step: "sop-cancel-0", statusSolicitud: "1" }, job.contact, user.line);
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          pushText("Ingrese el número de cédula o pasaporte sin guiones ni espacios", job.contact, user.line);
          updateUser({ step: "sop-cancel-0", statusSolicitud: "3" }, job.contact, user.line);
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          // main menu
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(mainMenu(false), job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*Llega numero de cedula
      case "sop-cancel-0":
        var ced = body.toLowerCase()
        utapi.consultarSolicitudes(ced, user.statusSolicitud, user.id).then((res) => {
          // console.log(res)
          if (res.result) {
            //si hay mas una solicitud no se pide el perfil
            if (res.solicitudes.length == 1) {
              pushText(fechasSolicitudes(res.solicitudes, [], job, user), job.contact, user.line);
              updateUser({ step: "sop-cancel-2" }, job.contact, user.line);

              // si hay mas de una solicitud se pide el perfil para solo mostrar dichas solicitudes
            } else {
              pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
              updateUser({ step: "sop-cancel-1", ced: ced }, job.contact, user.line);
            }
          } else {
            setUserParam("step", "sop-cancel-wait", job.contact, user.line);
            pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
          }
        }).catch((err) => {
          console.log(err)
        })
        break

      //*Llega el tipo de perfil
      case "sop-cancel-1":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) { // Persona natural
          utapi.consultarSolicitudes(user.ced, user.statusSolicitud, user.id).then((res) => {
            if (res.result) {
              pushText(fechasSolicitudes2(res.solicitudes, 'Persona Natural', [], 0, job, user), job.contact, user.line);
              updateUser({ step: "sop-cancel-2", tipo_perfil: "1", perfil: 'Persona Natural' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-cancel-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) { //Representante legal
          utapi.consultarSolicitudes(user.ced, user.statusSolicitud, user.id).then((res) => {
            if (res.result) {
              pushText(fechasSolicitudes2(res.solicitudes, 'Representante legal', [], 0, job, user), job.contact, user.line);
              updateUser({ step: 'sop-cancel-2', tipo_perfil: '2', perfil: 'Representante Legal' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-cancel-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) { //Miembro de empresa
          utapi.consultarSolicitudes(user.ced, user.statusSolicitud, user.id).then((res) => {
            if (res.result) {
              pushText(fechasSolicitudes2(res.solicitudes, 'Miembro de empresa', [], 0, job, user), job.contact, user.line);
              updateUser({ step: "sop-cancel-2", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-cancel-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*llega la firma escogida
      case "sop-cancel-2":
        var kw = body.toLowerCase()
        var msg = ''
        if (!isNaN(Number(kw))) {
          if (Number(kw) < 1 || Number(kw) > user.solicitudes.length + 1) {
            pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
          } else {
            if (Number(kw) == user.solicitudes.length + 1) {
              // main menu
              setUserParam("step", "sop1", job.contact, user.line);
              pushText(mainMenu(false), job.contact, user.line);
            } else {
              updateUser({ step: "sop-cancel-3", solicitudCancelar: user.solicitudes[Number(kw) - 1] }, job.contact, user.line);
              if (user.statusSolicitud == '1') {//Nuevo
                msg = 'Escoja un motivo para cancelar la solicitud:\n\n'
                msg += '1. No cumple requisitos\n'
                msg += '2. Duplicada\n'
                msg += '3. Cliente desiste solicitud\n'
                msg += '4. Otro\n'
              }
              if (user.statusSolicitud == '3') {//Aprobado
                msg = 'Escoja un motivo para cancelar la solicitud:\n\n'
                msg += '1. Correo Incorrecto\n'
                msg += '2. Celular incorrecto\n'
                msg += '3. Duplicada\n'
                msg += '4. Perfil Incorrecto\n'
                msg += '5. Vigencia incorrecta\n'
                msg += '6. Formato incorrecto\n'
                msg += '7. Otro\n'
              }
              pushText(msg, job.contact, user.line);
            }
          }
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break

      //En este paso se cancela luego de haber escogido el motivo de cancelacion
      case 'sop-cancel-3':
        var kw = body.toLowerCase()
        if (user.statusSolicitud == '1') { //Nuevo
          if (["1", "2", "3", "4"].indexOf(kw) >= 0) {
            //cancelar solicitud
            utapi.cancelarSolicitud(user.solicitudCancelar, user.statusSolicitud, user.id).then((res) => {
              if (res.result) {
                //solicitud cancelada
                setUserParam("step", "sop-cancel-menu-final", job.contact, user.line);
                pushText(res.message + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
              } else {
                //resultado negativo para la consulta
                pushText(res.message, job.contact, user.line);
                salir(job, user);
              }
            }).catch((err) => {
              console.log(err)
              pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
              salir(job, user);
            })
          } else {
            pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
          }
        }

        if (user.statusSolicitud == '3') { //Aprobado
          if (["1", "2", "3", "4", "5", "6", "7"].indexOf(kw) >= 0) {
            //cancelar solicitud
            utapi.cancelarSolicitud(user.solicitudCancelar, user.statusSolicitud, user.id).then((res) => {
              if (res.result) {
                //solicitud cancelada
                setUserParam("step", "sop-cancel-menu-final", job.contact, user.line);
                pushText(res.message + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
              } else {
                //resultado negativo para la consulta
                pushText(res.message, job.contact, user.line);
                salir(job, user);
              }
            }).catch((err) => {
              console.log(err)
              pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
              salir(job, user);
            })
          } else {
            pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
          }
        }
        break

      case 'sop-cancel-wait':
        var kw = body.toLowerCase()
        //volver a ingresar cedula
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          var msg = "Ingrese el número de cédula sin guiones ni espacios."
          pushText(msg, job.contact, user.line);
          setUserParam("step", "sop-cancel-0", job.contact, user.line);
          //regresar al menu principal
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1.1", job.contact, user.line);
          pushText(distri(), job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break

      case 'sop-cancel-menu-final':
        var kw = body.toLowerCase()
        //Regresar al menu principal
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1.1", job.contact, user.line);
          pushText(distri(), job.contact, user.line);
          //Salir
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          salir(job, user);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break
      //! Fin cancelacion de solicitud

      //! Reenvio de correo de descarga
      //*Llega numero de cedula
      case 'sop-rcorreo-descarga':
        var kw = body.toLowerCase()
        pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
        updateUser({ step: "sop-rcorreo-descarga-0", ced: kw }, job.contact, user.line);
        break

      case 'sop-rcorreo-descarga-0':
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          utapi.consultarFirmasAprobadas(user.ced, user.id, '1').then((res) => {
            if (res.result) {
              pushText(fechasFirmas2(res.firmas, 'Persona Natural', [], job, user), job.contact, user.line);
              updateUser({ step: "sop-rcorreo-descarga-1", tipo_perfil: "1", perfil: 'Persona Natural' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-rcorreo-descarga-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          utapi.consultarFirmasAprobadas(user.ced, user.id, '2').then((res) => {
            if (res.result) {
              pushText(fechasFirmas2(res.firmas, 'Representante Legal', [], job, user), job.contact, user.line);
              updateUser({ step: 'sop-rcorreo-descarga-1', tipo_perfil: '2', perfil: 'Representante Legal' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-rcorreo-descarga-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          utapi.consultarFirmasAprobadas(user.ced, user.id, '3').then((res) => {
            if (res.result) {
              pushText(fechasFirmas2(res.firmas, 'Miembro de empresa', [], job, user), job.contact, user.line);
              updateUser({ step: "sop-rcorreo-descarga-1", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-rcorreo-descarga-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break


      case "sop-rcorreo-descarga-1":
        var kw = body.toLowerCase()
        if (!isNaN(Number(kw))) {
          if (Number(kw) < 1 || Number(kw) > user.firmas.length + 1) {
            pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
          } else {
            if (Number(kw) == user.firmas.length + 1) {
              // main menu
              setUserParam("step", "sop1", job.contact, user.line);
              pushText(mainMenu(false), job.contact, user.line);
            } else {
              //reenvio de mail de descarga
              utapi.reenvioCorreoDescarga(user.firmas[Number(kw) - 1], user.id).then((res) => {
                if (res.result) {
                  //email de descarga reenviado
                  setUserParam("step", "sop-rcorreo-descarga-menu-final", job.contact, user.line);
                  pushText(res.resultado + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
                } else {
                  //resultado negativo para la consulta
                  pushText(res.resultado, job.contact, user.line);
                  salir(job, user);
                }
              }).catch((err) => {
                console.log(err)
                pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
                salir(job, user);
              })
            }
          }
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break

      case 'sop-rcorreo-descarga-wait':
        var kw = body.toLowerCase()
        //volver a ingresar cedula
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          var msg = "El mail de descarga se reenvía solo en status *APROBADO*\n\nIngrese el número de cédula sin guiones ni espacios."
          pushText(msg, job.contact, user.line);
          setUserParam("step", "sop-rcorreo-descarga", job.contact, user.line);
          //regresar al menu principal
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1.1", job.contact, user.line);
          pushText(distri(), job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break

      case 'sop-rcorreo-descarga-menu-final':
        var kw = body.toLowerCase()
        //Regresar al menu principal
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1.1", job.contact, user.line);
          pushText(distri(), job.contact, user.line);
          //Salir
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          salir(job, user);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break
      //! Fin reenvio de correo de descarga

      //!consultar estado de solicitud
      //*Llega numero de cedula
      case 'sop-estado-solicitud':
        var kw = body.toLowerCase()
        pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
        updateUser({ step: "sop-estado-solicitud-0", ced: kw }, job.contact, user.line);
        break

      case 'sop-estado-solicitud-0':
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          utapi.consultarSolicitudesConEstado(user.ced, user.id, '1').then((res) => {
            console.log(res.result)
            if (res.result) {
              pushText(fechasSolicitudesConEstado(res.solicitudes, [], job, user, 0), job.contact, user.line);
              updateUser({ step: "sop-estado-solicitud-1", tipo_perfil: "1", perfil: 'Persona Natural' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-estado-solicitud-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          utapi.consultarSolicitudesConEstado(user.ced, user.id, '2').then((res) => {
            if (res.result) {
              pushText(fechasSolicitudesConEstado(res.solicitudes, [], job, user, 0), job.contact, user.line);
              updateUser({ step: 'sop-estado-solicitud-1', tipo_perfil: '2', perfil: 'Representante Legal' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-estado-solicitud-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          utapi.consultarSolicitudesConEstado(user.ced, user.id, '3').then((res) => {
            if (res.result) {
              pushText(fechasSolicitudesConEstado(res.solicitudes, [], job, user, 0), job.contact, user.line);
              updateUser({ step: "sop-estado-solicitud-1", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
            } else {
              setUserParam("step", "sop-estado-solicitud-wait", job.contact, user.line);
              pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
            }
          }).catch((err) => {
            console.log(err)
            pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
            salir(job, user);
          })
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break


      case "sop-estado-solicitud-1":
        var kw = body.toLowerCase()
        if (!isNaN(Number(kw))) {
          if (Number(kw) < 1 || Number(kw) > user.solicitudesConEstado.length + 1) {
            pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
          } else {
            if (Number(kw) == user.solicitudesConEstado.length + 1) {
              // main menu
              setUserParam("step", "sop1", job.contact, user.line);
              pushText(mainMenu(false), job.contact, user.line);
            } else {
              utapi.consultarSolicitudesConEstado(user.ced, user.id, user.tipo_perfil).then((res) => {
                if (res.result) {
                  const ob = res.solicitudes.find(ob => ob.token_uid === user.solicitudesConEstado[Number(kw) - 1]);
                  pushText(`El status del cliente ${user.ced} con perfil ${ob.tipo} creado el ${Object.values(ob)[0]} es: ${ob.estado}` + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
                  setUserParam("step", "sop-estado-solicitud-menu-final", job.contact, user.line);
                } else {
                  setUserParam("step", "sop-estado-solicitud-wait", job.contact, user.line);
                  pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
                }
              }).catch((err) => {
                console.log(err)
              })
            }
          }
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break

      case 'sop-estado-solicitud-wait':
        var kw = body.toLowerCase()
        //volver a ingresar cedula
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          var msg = "Ingrese el número de cédula sin guiones ni espacios."
          pushText(msg, job.contact, user.line);
          setUserParam("step", "sop-estado-solicitud", job.contact, user.line);
          //regresar al menu principal
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1.1", job.contact, user.line);
          pushText(distri(), job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break

      case 'sop-estado-solicitud-menu-final':
        var kw = body.toLowerCase()
        //Regresar al menu principal
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1.1", job.contact, user.line);
          pushText(distri(), job.contact, user.line);
          //Salir
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          salir(job, user);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
        }
        break
      //!Fin consultar estado de solicitud

      case "sop1.1.3":
        //REVOCAR
        var kw = body.toLowerCase()
        var group = ""
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          pushText("En el siguiente link puede descargar el formato y enviar firmado por el titular con firma electrónica o manuscrita mediante correo a info@uanataca.ec\n\nhttps://uanataca.ec/descargas/SOLICITUD_REVOCATORIA_UT.docx", job.contact, user.line);
          salir(job, user);
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          // main menu
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(mainMenu(false), job.contact, user.line);
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          // salir
          salir(job, user);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "sop3.1":
        var kw = body.toLowerCase()
        var group = ""
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          if (isShopOpen()) {
            var msg = "Por favor coméntanos como podemos ayudarte, en un momento un asistente se comunicará contigo\n";
            updateUser({ step: "idle", status: "active", group: "sos", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sos");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }

        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          if (isShopOpen()) {
            var msg = "Por favor coméntanos como podemos ayudarte, en un momento un asistente se comunicará contigo\n";
            updateUser({ step: "idle", status: "active", group: "sos", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sos");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }

        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          if (isShopOpen()) {
            var msg = "Por favor coméntanos como podemos ayudarte, en un momento un asistente se comunicará contigo\n";
            updateUser({ step: "idle", status: "active", group: "sos", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sos");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(mainMenu(false), job.contact, user.line);
        } else {

          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "go-back":
        var kw = body.toLowerCase()
        var group = ""
        if (["salir", "*salir*"].indexOf(kw) >= 0) {
          salir(job, user);
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          salir(job, user);
        } else {
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(mainMenu(false), job.contact, user.line);
        }
        break;

      //!Inicio flujo Piccolos
      case "sop1.1.piccolos":
        var kw = body.toLowerCase()
        var group = ""
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {

          //reenvio de credenciales original
          if (isShopOpen()) {
            pushText('Por favor ingresa el número de cédula y el email del solicitante', job.contact, user.line);
            updateUser({ step: "idle", status: "active", group: "sod", tso: Date.now() }, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sod");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }

        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {

          //cancelación
          setUserParam("step", "sop1.1.2.piccolos", job.contact, user.line);
          pushText(estadoMenu(), job.contact, user.line);

        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {

          //revocar
          setUserParam("step", "sop1.1.3.piccolos", job.contact, user.line);
          pushText(estadoMenu(), job.contact, user.line);

        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {

          //estado solicitud
          var msg = "Por favor ingresa el numero de cédula de la solicitud que quieres revisar?\n\n"
          setUserParam("step", "sop1.1.4.piccolos", job.contact, user.line);

          pushText(msg, job.contact, user.line);
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {

          //hablar operador
          if (isShopOpen()) {
            var msg = "Por favor indícanos cuál es tu requerimiento"
            updateUser({ step: "idle", status: "active", group: "sod", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sod");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }
        } else if (["6", "6.", "seis"].indexOf(kw) >= 0) {

          // main menu
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(mainMenu(false), job.contact, user.line);

        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "sop1.1.2.piccolos":
        //cancelar
        var kw = body.toLowerCase()
        var group = ""
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          var msg = "Si el estado de la solicitud es NUEVO, se puede cancelar la solicitud en su portal dentro del menú de solicitudes. \n\n"
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(msg + mainMenu(false), job.contact, user.line);
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          if (isShopOpen()) {
            var msg = "Por favor ayúdanos con la fecha de la solicitud y el número de cédula, la cancelación se hará lo más pronto posible \n";
            updateUser({ step: "idle", status: "active", group: "sod", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sod");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }

        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          if (isShopOpen()) {
            var msg = "Por favor ayúdanos con la fecha de la solicitud y el número de cédula, la revocación se hará lo más pronto posible \n";
            updateUser({ step: "idle", status: "active", group: "sod", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sod");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "sop1.1.3.piccolos":
        //REVOCAR
        var kw = body.toLowerCase()
        var group = ""
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          var msg = "Si el estado de la solicitud es NUEVO, se puede cancelar la solicitud en su portal dentro del menú de solicitudes. \n\n"
          setUserParam("step", "sop1", job.contact, user.line);
          pushText(msg + mainMenu(false), job.contact, user.line);
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          if (isShopOpen()) {
            var msg = "Por favor ayúdanos con la fecha de la solicitud y el número de cédula, la cancelación se hará lo más pronto posible\n";
            updateUser({ step: "idle", status: "active", group: "sod", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sod");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }

        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          if (isShopOpen()) {
            var msg = "Por favor ingresar el número de cédula del solicitante, la fecha de emisión del certificado y las razones por las cuales se está requiriendo esta revocación\n";
            updateUser({ step: "idle", status: "active", group: "sod", tso: Date.now() }, job.contact, user.line);
            pushText(msg, job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sod");
          } else {
            pushText(config.horarioStr, job.contact, user.line);
            setUserParam("step", "0", job.contact, user.line);
          }
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;

      case "sop1.1.4.piccolos":
        //consulta estado solicitud en el API
        var kw = body.toLowerCase()
        utapi.checkStatus(kw.substring(0, 13), function (arr) {
          if (arr) {
            if (arr.length == 1) {
              var reg = arr[0]
            } else {
              //sort and use last
              arr = _.sortBy(arr, function (item) { return item.fecha_registro });
              var reg = arr[arr.length - 1]
            }
            msg = "El estado de tu solicitud es *" + reg.estado + "*\nObservaciones: _" + mapaEstadoFirma(reg.estado.toUpperCase(), reg.observacion) + "_"
            setUserParam("step", "sop1", job.contact, user.line);
            pushText(msg + "\n\n" + mainMenu(false), job.contact, user.line);
          } else {
            setUserParam("step", "sop1", job.contact, user.line);
            pushText("No hemos podido validar tu solicitud con el número de cédula ingresado\n\n" + mainMenu(false), job.contact, user.line);
          }
        });
        break
      //!Fin flujo Piccolos

      //!Inicio flujo Correcaminos
      case "sop.correcaminos":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          pushText('Digite la cédula', job.contact, user.line);
          setUserParam("step", "sop.correcaminos.1", job.contact, user.line);
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          pushText("Seleccione el formato:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "archivo", title: "Archivo" }, { id: "nube", title: "Nube" }, { id: "token", title: "Token" }, { id: "combo", title: "Combo" }, { id: "mp", title: "Menú principal" }] })
          setUserParam("step", "sop.correcaminos.2", job.contact, user.line);
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          pushText(recargaCyT(), job.contact, user.line);
          setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {

        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*Captar Tienda
      //llega la cedula
      case "sop.correcaminos.1":
        var kw = body.toLowerCase()
        if (!user.payloadCT) { user.payloadCT = {} }
        user.payloadCT.cedula = kw
        pushText('Digite el nombre completo', job.contact, user.line);
        updateUser({ step: "sop.correcaminos.1.1", payloadCT: user.payloadCT }, job.contact, user.line);
        break

      //llega el nombre
      case "sop.correcaminos.1.1":
        var kw = body.toLowerCase()
        user.payloadCT.nombre = kw
        pushText('Digite el Email', job.contact, user.line);
        updateUser({ step: "sop.correcaminos.1.2", payloadCT: user.payloadCT }, job.contact, user.line);
        break

      //llega el email
      case "sop.correcaminos.1.2":
        var kw = body.toLowerCase()
        user.payloadCT.email = kw
        pushText('Digite el número de celular', job.contact, user.line);
        updateUser({ step: "sop.correcaminos.1.3", payloadCT: user.payloadCT }, job.contact, user.line);
        break

      //llega el numero de celular
      case "sop.correcaminos.1.3":
        var kw = body.toLowerCase()
        user.payloadCT.celular = kw
        pushText('Digite el número de celular', job.contact, user.line);
        updateUser({ step: "sop.correcaminos.1.4", payloadCT: user.payloadCT }, job.contact, user.line);
        break

      //Envío de proceso firma de contrato
      case "sop.correcaminos.1.4":
        var kw = body.toLowerCase()
        //TODO: Proceso firmelo
        break
      //*Fin Captar Tienda


      //*Venta Nueva
      //llega el formato
      case "sop.correcaminos.2":
        var kw = body.toLowerCase()
        //TODO: consultar precios
        if (kw == 'archivo') {

        } else if (kw == 'nube') {

        } else if (kw == 'token') {

        } else if (kw == 'combo') {

        } else if (kw == 'mp') {
          pushText(correcaminos(), job.contact, user.line);
          setUserParam("step", "sop.correcaminos", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //llega la vigencia
      case "sop.correcaminos.2.1":
        var kw = body.toLowerCase()
        if (kw == 'vigencia1') {

        } else if (kw == 'vigencia2') {

        } else if (kw == 'vigencia3') {

        } else if (kw == 'vigencia4') {

        } else if (kw == 'mp') {
          pushText(correcaminos(), job.contact, user.line);
          setUserParam("step", "sop.correcaminos", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
      //*Fin Venta Nueva


      //*Recargar Saldo
      //llega metodo de pago
      case "sop.correcaminos.3":
        var kw = body.toLowerCase()
        //De UNA
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //TODO: DEUNA
          //Tarjeta de crédito o débito
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          //TODO: Tarjeta de crédito o débito
          //Regresar al menu principal
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          pushText(correcaminos(), job.contact, user.line);
          setUserParam("step", "sop.correcaminos", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }

        break
      //*Fin Recargar Saldo


      //*Consulta Saldo
      //llega metodo de pago
      case "sop.correcaminos.4":
        var kw = body.toLowerCase()
        distriapi.getBalance(user.partnerUid).then((res) => {
          console.log(res)
          if (res.success) {
            pushText(`Tu saldo actual es ${res.data.balance}\n\n1. Recargar\n2. Regresar al menú principal`, job.contact, user.line);
            setUserParam("step", "sop.correcaminos-wait", job.contact, user.line);
          }
        }).catch((err) => {
          console.log(err)
        })
        break
      //*Fin Consulta Saldo


      case "sop.correcaminos-wait":
        var kw = body.toLowerCase()
        //Recargar
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          pushText(recargaCyT(), job.contact, user.line);
          setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
          //Regresar al menu principal
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          pushText(correcaminos(), job.contact, user.line);
          setUserParam("step", "sop.correcaminos", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
      //!Fin flujo Correcaminos


      //!Inicio flujo Tienda
      case "sop.tienda":
        var kw = body.toLowerCase()
        //Venta nueva
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          pushText(vigenciaCyT(), job.contact, user.line);
          setUserParam("step", "sop.tienda.1", job.contact, user.line);
          //Recargar saldo
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          pushText(recargaCyT(), job.contact, user.line);
          setUserParam("step", "sop.tienda.2", job.contact, user.line);
          //Consulta saldo
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {

        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*Venta Nueva
      //llega la vigencia
      case "sop.tienda.1":
        var kw = body.toLowerCase()
        //1 año
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          pushText(formatoCyT(), job.contact, user.line);
          updateUser({ step: "sop.tienda.1.1", vigenciaCorrecaminos: "1 año" }, job.contact, user.line);
          //2 años
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          pushText(formatoCyT(), job.contact, user.line);
          updateUser({ step: "sop.tienda.1.1", vigenciaCorrecaminos: "2 años" }, job.contact, user.line);
          //3 años
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          pushText(formatoCyT(), job.contact, user.line);
          updateUser({ step: "sop.tienda.1.1", vigenciaCorrecaminos: "3 años" }, job.contact, user.line);
          //4 años
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          pushText(formatoCyT(), job.contact, user.line);
          updateUser({ step: "sop.tienda.1.1", vigenciaCorrecaminos: "4 años" }, job.contact, user.line);
          //Regresar al menu principal
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
          pushText(tienda(), job.contact, user.line);
          setUserParam("step", "sop.tienda", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //llega el formato
      case "sop.tienda.1.1":
        var kw = body.toLowerCase()
        //Archivo
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //TODO: Consulta saldo
          //Nube
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          //TODO: Consulta saldo
          //Token
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          //TODO: Consulta saldo
          //Combo
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          //TODO: Consulta saldo
          //Regresar al menu principal
        } else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
          pushText(tienda(), job.contact, user.line);
          setUserParam("step", "sop.tienda", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      case "sop.tienda.1.2":
        var kw = body.toLowerCase()
        //TODO: continuar con el proceso de venta
        break
      //*Fin Venta Nueva


      //*Recargar Saldo
      //llega metodo de pago
      case "sop.tienda.2":
        var kw = body.toLowerCase()
        //De UNA
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //TODO: DEUNA
          //Tarjeta de crédito o débito
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          //TODO: Tarjeta de crédito o débito
          //Regresar al menu principal
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          pushText(tienda(), job.contact, user.line);
          setUserParam("step", "sop.tienda", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break

      //*Fin Recargar Saldo


      //*Consulta Saldo
      case "sop.tienda.3":
        var kw = body.toLowerCase()
        distriapi.getBalance(user.partnerUid).then((res) => {
          console.log(res)
          if (res.success) {
            pushText(`Tu saldo actual es ${res.data.balance}\n\n1. Recargar\n2. Regresar al menú principal`, job.contact, user.line);
            setUserParam("step", "sop.tienda-wait", job.contact, user.line);
          }
        }).catch((err) => {
          console.log(err)
        })
        break
      //*Fin Consulta Saldo


      case "sop.tienda-wait":
        var kw = body.toLowerCase()
        //Recargar
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          pushText(recargaCyT(), job.contact, user.line);
          setUserParam("step", "sop.correcaminos.2", job.contact, user.line);
          //Regresar al menu principal
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          pushText(tienda(), job.contact, user.line);
          setUserParam("step", "sop.tienda", job.contact, user.line);
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break
      //!Fin flujo Tienda
    }//end switch step

    function mainMenu(greet) {
      if (greet) {
        var msg = "Gracias por comunicarte con el canal de soporte de UANATACA EC por favor selecciona la opción de tu interés, será un gusto poder ayudarte 🙂\n\n";
      } else {
        var msg = "Por favor selecciona la opción de tu interés, será un gusto poder ayudarte 🙂\n\n";
      }
      msg += "1. Distribuidores\n";
      msg += "2. OneShot\n";
      return msg;
    }

    function GoBackMenu() {
      var msg = "Selecciona una opción:\n\n";
      msg += "1. Regresar al menú principal\n";
      msg += "2. Salir";
      return msg;
    }


    function oneShot() {
      var msg = "Selecciona una opción:\n\n";
      msg += "1. El documento no obtiene la firma OneShot\n";
      msg += "2. No tengo acceso al servidor\n";
      msg += "3. Necesito soporte directo\n";
      msg += "4. Regresar al menú anterior\n";
      return msg;
    }

    // function distri() {
    //      var msg = "Selecciona una opción:\n\n";
    //      msg += "1. Reenvío de credenciales\n";
    //      msg += "2. Cancelación de una solicitud\n";
    //      msg += "3. Revocar una solicitud\n";
    //      msg += "4. Cuál es el estado de una solicitud?\n";
    //      msg += "5. Necesito hablar con un operador\n";
    //      msg += "6. Regresas al menú anterior\n";
    //      return msg;
    // }

    function piccolos() {
      var msg = "Selecciona una opción:\n\n";
      msg += "1. Reenvío de credenciales\n";
      msg += "2. Cancelación de una solicitud\n";
      msg += "3. Revocar una solicitud\n";
      msg += "4. Cuál es el estado de una solicitud?\n";
      msg += "5. Necesito hablar con un operador\n";
      msg += "6. Regresas al menú anterior\n";
      return msg;
    }

    function distri() {
      var msg = "Selecciona una opción:\n\n";
      msg += "1. Reenvío email para descarga de firma\n";
      msg += "2. Cancelar una solicitud\n";
      msg += "3. Reenvío de credenciales\n";
      msg += "4. Revocar una solicitud\n";
      msg += "5. Cuál es el estado de una solicitud?\n";
      msg += "6. Contactar con un asesor\n";
      msg += "7. Regresar al menú principal\n";
      return msg;
    }

    function correcaminos() {
      var msg = "Menú principal\n\n"
      msg += "1. Captar Tienda\n"
      msg += "2. Venta Nueva\n"
      msg += "3. Recargar Saldo\n"
      msg += "4. Consulta tu saldo\n"
      return msg;
    }

    function vigenciaCyT() {
      var msg = "Seleccione Vigencia:\n\n"
      msg += "1. Un año\n"
      msg += "2. Dos años\n"
      msg += "3. Tres años\n"
      msg += "4. Cuatro años\n"
      msg += "5. Regresar al menú principal\n"
      return msg;
    }

    function formatoCyT() {
      var msg = "Seleccione Formato\n\n"
      msg += "1. Archivo\n"
      msg += "2. Nube\n"
      msg += "3. Token\n"
      msg += "4. Combo\n"
      msg += "5. Regresar al menú principal\n"
      return msg;
    }

    function recargaCyT() {
      var msg = "Escoja como recargar:\n"
      msg += "1. De UNA\n"
      msg += "2. Tarjeta de crédito o débito\n"
      msg += "3. Regresar al menú principal\n"
      return msg;
    }

    function tienda() {
      var msg = "Menú principal\n\n"
      msg += "1. Venta nueva\n"
      msg += "2. Recargar saldo\n"
      msg += "3. Consulta tu saldo\n"
      return msg;
    }

    function estadoMenu() {
      var msg = "El estado de la solicitud es:\n\n";
      msg += "1. Nuevo\n";
      msg += "2. Aprobado\n";
      msg += "3. Emitido\n";
      return msg;
    }

    function revocar() {
      var msg = "Para revocar una solicitud puede seguir dos pasos:\n";
      msg += '- El titular puede revocar en línea mediante el mail que recibió de Uanataca luego de descargar la firma electrónica en la opción "REVOCAR" (Servicio 24/7)\n';
      msg += "- Enviar la carta de solicitud de revocatoria firmada por el cliente por mail.\n";
      msg += "1. Descargar formato de revocatoria\n";
      msg += "2. Regresar al menu principal\n";
      msg += "3. Salir\n";
      return msg;
    }


    function fechasFirmas(firmas, perfil, arreglo, job, user) {
      var msg = `Escoja el certificado del que desea recuperar la clave en el perfil: ${perfil}\n\n`;

      firmas.map((p, index) => {
        for (let i = 0; i <= firmas.length; i++) {
          if (p[`firma_${i}`]) {
            msg += `${i}. ` + 'Aprobada: ' + p[`firma_${i}`].replace(/-/g, "/") + '\n';
            arreglo.push(p[`firma_${i}`])
            if (index === firmas.length - 1) {
              msg += `${i + 1}.` + ' Menú Principal';
            }
          }
        }
      })
      updateUser({ firmas: arreglo }, job.contact, user.line);

      return msg;
    }


    function fechasFirmas2(firmas, perfil, arreglo, job, user) {
      var msg = `Escoja el certificado del que desea reenviar el email de descarga en el perfil: ${perfil}\n\n`;

      firmas.map((p, index) => {
        for (let i = 0; i <= firmas.length; i++) {
          if (p[`solicitud_${i}`]) {
            msg += `${i}. ` + 'Creada: ' + p[`solicitud_${i}`].replace(/-/g, "/") + '\n';
            arreglo.push(p.uid)
            if (index === firmas.length - 1) {
              msg += `${i + 1}.` + ' Menú Principal';
            }
          }
        }
      })
      updateUser({ firmas: arreglo }, job.contact, user.line);

      return msg;
    }

    function fechasSolicitudes(solicitudes, arreglo, job, user) {
      var msg = `Escoja el certificado del que desea cancelar\n\n`;

      solicitudes.map((p, index) => {
        for (let i = 0; i <= solicitudes.length; i++) {
          if (p[i]) {
            msg += `${i}. ` + 'Creada: ' + p[i].replace(/-/g, "/") + '\n';
            arreglo.push(p.token_uid)
            if (index === solicitudes.length - 1) {
              msg += `${i + 1}.` + ' Menú Principal';
            }
          }
        }
      })
      updateUser({ solicitudes: arreglo }, job.contact, user.line);

      return msg;
    }

    function fechasSolicitudes2(solicitudes, perfil, arreglo, count, job, user) {
      var msg = `Escoja el certificado del que desea cancelar en el perfil: ${perfil}\n\n`;

      solicitudes.map((p, index) => {
        if (p.tipo === perfil.toUpperCase()) {
          msg += `${count + 1}. ` + 'Creada: ' + p[index + 1].replace(/-/g, "/") + '\n';
          arreglo.push(p.token_uid);
          count++;
        }
      });

      msg += `${count + 1}. Menú Principal\n`;

      updateUser({ solicitudes: arreglo }, job.contact, user.line);

      return msg;
    }

    function fechasSolicitudesConEstado(solicitudes, arreglo, job, user, count) {
      var msg = `Escoja el certificado que desea conocer el status por fecha de creación:\n\n`;

      solicitudes.map((p, index) => {
        msg += `${count + 1}. ` + 'Creada: ' + p[index + 1].replace(/-/g, "/") + '\n';
        arreglo.push(p.token_uid);
        count++;
      });

      msg += `${count + 1}. Menú Principal\n`;

      updateUser({ solicitudesConEstado: arreglo }, job.contact, user.line);

      return msg;
    }

  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job de uanatacaPluginSoporte")
    console.log(job)

    cb({ st: "done" })
  }
}