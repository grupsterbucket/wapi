var request = require('request');
global.tools = require('./toolsnode10');
global.mailer = require('./mailer');
var moment = require('moment');  
var fs = require('fs');
var path = require('path');
var envConfig = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
global.env = envConfig[envConfig.env];
console.log("Loading", envConfig.env, "environment", global.env);
var plugin = require('./legalineaPlugin');
global._ = require('underscore');
global.firebase = require('firebase');
var CryptoJS = require("crypto-js");
var account = global.env.account;
if (account == "legalinea") {
	botLine = "593995945725"
} else {
	botLine = "593991684677";
}

var booted = false;
var jobs = []
var lines =   ["593986019740","593993548064","593987724332","593990940426","593939498611"]
var players = ["593984252217","593999730011","16616186329" ,"593998981482","593998111761","593998529420","593984170713","593959187680"]
var testerITV = 1000*60*60;
var tests = {}
var os = require('os');
const { prepareDataForUanataca } = require('./toolsnode10');
var qitv = 250;
global.config = {};
global.calendars = [];
global.rubros = [];

//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

var wspCredentials = {
	"email": "api@wsppaylink.com",
	"password": "pinguino"
}

var wsppaylinkToken;
var wsppaylinkUrl = "http://wsppayphone.coniski.com"
var apiUser;
firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {

    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
		apiUser = user.uid;
        setTimeout(function () {
			//~ wspLinkLogin(wspCredentials, function(err, res) {
				//~ wsppaylinkToken = res.token;
				//~ console.log("wsppaylink token: ", wsppaylinkToken);
				
            	//~ bootApp();
			//~ });
			wsppaylinkToken = "none"
			bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//mailer.sendEmail("moncayo74@gmail.com","Legalinea Bot Booted", new Date());
		//load account config
		loadAccountConfig();
		setTimeout(function(){
			listenToJobs();
			processJobQueue();
			
			//pushTemplate(["Test Template"], "new_ticket_created", "593984252217", botLine);
		}, 1000);
		if (envConfig.env == "dev") {
			//~ console.log ("Start Interval for ping tester");
			//~ var tester = setInterval(function(){
				//~ runBotPinger();	
			//~ }, testerITV)
		}
	}	
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/'+account+'/config/misc').on('value', function(snapshot) {		  
		config = snapshot.val();
		console.log ("Config fetched for account",account);
	});
	firebase.database().ref('accounts/'+account+'/config/calendars').on('value', function(snapshot) {		  
		calendars = _.map(snapshot.val(), function(ob, key){
			ob.id = key;
			return ob;
		});
		console.log ("Calendars fetched for account",account);
	});
	firebase.database().ref('accounts/'+account+'/config/params/-LMSyLO5XRdroJ_hmrKr').on('value', function(snapshot) {		  
		rubros = snapshot.val().obj.rubros;
		//~ console.log (rubros)
		console.log ("Rubros fetched for account",account);
	});
	
}
function listenToJobs() {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	}
}

function processJob(job) {
	console.log(job);
	
	switch (job.type) {
		case 'pause':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				if(user.lastStep) {
					updateUser({ step:"idle", lastStep: user.step }, job.contact, user.line);
				}else{
					updateUser({ step:"idle"}, job.contact, user.line);
				}
				finishJob(job);
			});
			break;
		case 'resume':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				if(user.lastStep) {
					updateUser({ step: user.lastStep}, job.contact, user.line);
				}else{
					updateUser({ step: "0"}, job.contact, user.line);
				}
				
				resetUserParam("lastStep", job.contact);
				finishJob(job);
			});
			break;
		case 'update-step':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				updateUser({ step: job.step}, job.contact, user.line);
				resetUserParam("lastStep", job.contact);
				finishJob(job);
			});
			break;
		case 'send-notification':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				pushText (job.str, job.contact, user.line);
				//job is completed
				finishJob(job);
			});
			break;
		case 'send-alert':
			switch (job.alert.type) {
				case 'alert:login:failed':
					pushTemplate(["Session login failed for line "+job.alert.name], "task_completed", "593984252217", "593991684677");
					break;
				case 'alert:process:init':
					pushTemplate(["Process Init "+job.alert.name], "task_completed", "593984252217", "593991684677");
					break;
			}
			//job is completed
			finishJob(job);
			
			break;
		case 'reset-bot':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				resetBot (job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
		case 'identity-valid':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				updateUser({ step:"f9", "x-dniValid": true, "x-dniValidTs": Date.now()}, job.contact, user.line);
				resetUserParam('status', job.contact);
				var msg = "Excelente! Hemos validado correctamente tu identidad y los documentos. Selecciona una opción:\n\n"
				msg += "1. Pagar con tarjeta de crédito\n";
				msg += "2. Ingresar un código de promoción\n";
				msg += "3. Pagar con transferencia bancaria";
				pushText (msg, job.contact, user.line);
				//job is completed
				finishJob(job);
			});
			break;

		case 'request-firma':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				getContact(job.contact, function(contact) {
					if(!contact){
						//Todo uanataca error logic
						var msg = "Hubo un error al solicitar la firma de " + job.contact + " No contact found " + "https://v3.textcenter.net/#inbox";
						pushTemplate([msg], "new_ticket_created", "593995453544", botLine);		
					}
					let retryInterval;
					let attempt = 1;
					pushText ("Listo, el pago fue exitoso. Tu firma está siendo procesada, por favor espera un momento...", job.contact, user.line);
					requestFirma(user, function(res, data){
						pushText (successMessage(), job.contact, user.line);

						if(!res) res = {result: false};

						if(!res.result){
							retryInterval = setInterval(function(){
								requestFirma(user, function (res, data) {
									attempt++;
									if(attempt == 3){
										var msg = "Hubo un error al solicitar la firma de " + job.contact + " " + JSON.stringify(res) + " " + "https://v3.textcenter.net/#inbox";
										pushTemplate([msg], "new_ticket_created", "593995453544", botLine);	
										clearInterval(retryInterval);
									}
									if(!res.result) return;
									clearInterval(retryInterval);
									
									delete data.f_cedulaFront;
									delete data.f_cedulaBack;
									delete data.f_selfie;
									delete data.f_copiaruc;
									delete data.f_nombramiento;
									delete data.f_constitucion;

									updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
										if (saved) {
											user["x-uanatacaReq"] =  {data:data, res: res}
											resetBot(job.contact, user);
										} 	
									});
								});
							},20000)
							return;
						}
						
						delete data.f_cedulaFront;
						delete data.f_cedulaBack;
						delete data.f_selfie;
						delete data.f_copiaruc;
						delete data.f_nombramiento;
						delete data.f_constitucion;

						updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
							if (saved) {
								user["x-uanatacaReq"] =  {data:data, res: res}
								resetBot(job.contact, user);
							}
						});
						
					});
				});
			});

			finishJob(job);
			break;
		case 'validate-transfer':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				var ob = {"x-pagoStatus": "approved"}
				user['x-pagoStatus'] = "approved";
				updateUserCB(ob, user.id, function(saved){
					if(saved){
						getContact(job.contact, function(contact) {
							if(!contact){
								//Todo uanataca error logic
								var msg = "Hubo un error al solicitar la firma de " + job.contact + " No contact found " + "https://v3.textcenter.net/#inbox";
								pushTemplate([msg], "new_ticket_created", "593995453544", botLine);		
							}
							let retryInterval;
							let attempt = 1;
							pushText ("Listo, el pago fue exitoso. Tu solicitud de firma está siendo procesada, por favor espera un momento...", job.contact, user.line);
							requestFirma(user, function(res, data){
								pushText (successMessage(), job.contact, user.line);

								if(!res) res = {result: false};
		
								if(!res.result){
									retryInterval = setInterval(function(){
										requestFirma(user, function (res, data) {
											attempt++;
											if(attempt == 3){
												var msg = "Hubo un error al solicitar la firma de " + job.contact + " " + JSON.stringify(res) + " " + "https://v3.textcenter.net/#inbox";
												pushTemplate([msg], "new_ticket_created", "593995453544", botLine);	
												clearInterval(retryInterval);
											}
											if(!res.result) return;
											
											clearInterval(retryInterval);
											
											delete data.f_cedulaFront;
											delete data.f_cedulaBack;
											delete data.f_selfie;
											delete data.f_copiaruc;
											delete data.f_nombramiento;
											delete data.f_constitucion;

											updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
												if (saved) {
													let rubro = _.findWhere(rubros, {code: user['x-prod']})
													let value = isShopOpen() ? rubro.value : rubro.extendedValue;
													let ref = rubro.name;
													user["x-uanatacaReq"] =  {data:data, res: res}
													createTrans(contact.id, user, "trans", value, ref, user["x-transferPhoto"]);
													resetBot(job.contact, user);
												}	
											});
										});
									},20000)
									return;
								}
								
								delete data.f_cedulaFront;
								delete data.f_cedulaBack;
								delete data.f_selfie;
								delete data.f_copiaruc;
								delete data.f_nombramiento;
								delete data.f_constitucion;
		
								updateUserCB({"x-uanatacaReq": {foo:'bar'}, }, user.id, function(saved) {
									if (saved) {
										let rubro = _.findWhere(rubros, {code: user['x-prod']})
										let value = isShopOpen() ? rubro.value : rubro.extendedValue;
										let ref = rubro.name;
										user["x-uanatacaReq"] =  {foo:'bar'}
										createTrans(contact.id, user, "trans", value, ref, user["x-transferPhoto"]);
										resetBot(job.contact, user);
									}
								});
								
							});
						});
					}
				});
				finishJob(job);
			});
			break;
		case 'payment-success':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				var ob = {"x-pagoStatus": "approved"}
				user['x-pagoStatus'] = "approved";
				updateUserCB(ob, user.id, function(saved){
					if(saved){
						getContact(job.contact, function(contact) {
							if(!contact){
								//Todo uanataca error logic
								var msg = "Hubo un error al solicitar la firma de " + job.contact + " No contact found " + "https://v3.textcenter.net/#inbox";
								pushTemplate([msg], "new_ticket_created", "593995453544", botLine);		
							}
							let retryInterval;
							let attempt = 1;
							pushText ("Listo, el pago fue exitoso. Tu firma está siendo procesada, por favor espera un momento...", job.contact, user.line);
							requestFirma(user, function(res, data){
								pushText (successMessage(), job.contact, user.line);

								if(!res) res = {result: false};
		
								if(!res.result){
									retryInterval = setInterval(function(){
										requestFirma(user, function (res, data) {
											attempt++;
											if(attempt == 3){
												var msg = "Hubo un error al solicitar la firma de " + job.contact + " " + JSON.stringify(res) + " " + "https://v3.textcenter.net/#inbox";
												pushTemplate([msg], "new_ticket_created", "593995453544", botLine);	
												clearInterval(retryInterval);
											}
											if(!res.result) return;
											
											clearInterval(retryInterval);
											
											delete data.f_cedulaFront;
											delete data.f_cedulaBack;
											delete data.f_selfie;
											delete data.f_copiaruc;
											delete data.f_nombramiento;
											delete data.f_constitucion;

											updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
												if (saved) {
													let rubro = _.findWhere(rubros, {code: user['x-prod']})
													let value = isShopOpen() ? rubro.value : rubro.extendedValue;
													let ref = rubro.name;
													user["x-uanatacaReq"] =  {data:data, res: res}
													createTrans(contact.id, user, "pay", value, ref, user["x-pagoId"]);
													resetBot(job.contact, user);
												} 	
											});
										});
									},20000)
									return;
								}
								
								delete data.f_cedulaFront;
								delete data.f_cedulaBack;
								delete data.f_selfie;
								delete data.f_copiaruc;
								delete data.f_nombramiento;
								delete data.f_constitucion;
		
								updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
									if (saved) {
										let rubro = _.findWhere(rubros, {code: user['x-prod']})
										let value = isShopOpen() ? rubro.value : rubro.extendedValue;
										let ref = rubro.name;
										user["x-uanatacaReq"] =  {data:data, res: res}
										createTrans(contact.id, user, "pay", value, ref, user["x-pagoId"]);
										resetBot(job.contact, user);
									}
								});
								
							});
						});
					}
				});
				finishJob(job);
			});
			break;
		case 'payment-error':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				var ob = {"x-pagoStatus": "error", step:"f9"}
				updateUser(ob, job.contact, user.line);
				var msg = "Hubo un error procesando tu pago. Selecciona una opción para continuar e inténtalo nuevamente:\n\n"
				msg += "1. Pagar con tarjeta de crédito\n";
				msg += "2. Ingresar un código de promoción\n";
				msg += "3. Pagar con transferencia bancaria";
				pushText (msg, job.contact, user.line);
		
				finishJob(job);
			});
			break;
		case 'payment-retry':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				var ob = {"x-pagoStatus": "retry", step:"f9"}
				updateUser(ob, job.contact, user.line);
				var msg = "Selecciona una opción para continuar:\n\n"
				msg += "1. Pagar con tarjeta de crédito\n";
				msg += "2. Ingresar un código de promoción\n";
				msg += "3. Pagar con transferencia bancaria";
				pushText (msg, job.contact, user.line);
		
				finishJob(job);
			});
			break;
		case 'support': 
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				//job is completed
				console.log(job.mediaRequested);
				
				switch (job.mediaRequested) {
					case "dniPhoto1":
						console.log("Here");
						getContact(job.contact, function(contact){	
							updateUser({ step:"fs.dniPhoto1" }, job.contact, user.line);
							console.log(contact);
							contact.docValidation[job.mediaRequested] = false;
							console.log(contact);
							updateContact({"docValidation": contact.docValidation}, contact.id)
							pushText (
								"Por favor envía una foto tuya sosteniendo tu cédula a la *altura del mentón* mostrando el *frente de tu cédula* como se muestra en la imagen. Pon atención de no tapar la cédula con tus dedos o mano", 
								job.contact, 
								user.line, 
								null, 
								"img", 
								null, 
								"https://s3.amazonaws.com/wsp2crmcdn/v3/legalinea/3124865265.png"
								);
							})
						
						break;
					case "dniPhoto2":
						updateUser({ step:"fs.dniPhoto2" }, job.contact, user.line);
						pushText (
							"Por favor una foto enfocando el *frente de tu cédula*, asegurate que la foto se vea claramente y todos los datos sean legibles.", 
							job.contact, 
							user.line, 
							null, 
							"img", 
							null, 
							"https://media.messagebird.com/v1/media/9f485d72-4b6c-4835-a9d1-2bc073d70c2d"
							);
						break;
					case "dniPhoto3":
						updateUser({ step:"fs.dniPhoto3" }, job.contact, user.line);
						pushText (
							"Por favor envía una foto enfocando el *posterior de tu cédula*, asegurate que todos los datos sean legibles.", 
							job.contact, 
							user.line, 
							null, 
							"img", 
							null, 
							"https://s3.amazonaws.com/wsp2crmcdn/v3/legalinea/2134679662.jpeg"
							);
						break;
					case "dniVideo":
						updateUser({ step:"fs.dniVideo" }, job.contact, user.line);
						var msg = "Listo, por favor envía un *video* donde se vea claramente tu rostro y debes decir lo siguiente:\n\n";
						msg += "_Mi nombre completo es "+user.persona.nombre+"_\n\n";
						msg += "_Mi cédula de identidad es "+user.persona.cedula+"_\n\n";
						msg += "_Hoy es  "+dateStr()+"_\n\n";
						msg += "_Autorizo a CONISKI a emitir una firma electrónica con mi identidad_\n\n";
						pushText (msg, job.contact, user.line);
						break;
					case "rucDoc":
						updateUser({ step:"fs.rucDoc" }, job.contact, user.line);
						var msg = "Por favor favor envía nuevamente el RUC de la empresa en formato PDF";
						pushText (msg, job.contact, user.line);
						break;
					case "nombramiento":
						updateUser({ step:"fs.nombramiento" }, job.contact, user.line);
						var msg = "Por favor envía nuevamente el nombramiento del representante legal en formato PDF";
						pushText (msg, job.contact, user.line);
						break;
					case "constitucion":
						updateUser({ step:"fs.constitucion" }, job.contact, user.line);
						var msg = "Por favor envía nuevamente la constitución de la empresa en formato PDF";
						pushText (msg, job.contact, user.line);
						break;
					default:
						break;
				}

				finishJob(job);
			});
			break;
			
		default:
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact.msisdn).once("value", function(snapshot) {
				var user = snapshot.val();
				var step = "0";
				var group = "main";
				if (user.step) {step = user.step};
				if (user.group) {group = user.group};
				
				if (user.tsso) {
					console.log ("User "+job.contact.msisdn+" is at step:", step, "group:", group);
				} else {
					startSession (job.contact.msisdn, user);	
					console.log ("New Session for "+job.contact.msisdn+"");
				}
				
				if (step != 'idle') {
						console.log ("Process MO ", job.msg.type);
						//pushText ("Gracias por escribirnos! En este momento estamos actualizando nuestro sistema, estaremos pronto en línea. Si requieres ayuda nos puedes escribir a soporte@legalinea.com", job.contact.msisdn, user.line);
						switch (job.msg.type) {
							case 'text':
								console.log ("Body",job.msg.content.text);
								var body = job.msg.content.text;
								if (body.length>0) {
									//trim spaces
									body = body.trim();

									//catch operations
									if (body.toLowerCase().indexOf("#") == 0) {
										var parts = body.toLowerCase().split(" ");
										//match direct operation
										switch ( parts[0] ) {
											case "#salir":
												salir(job, user);
												break;
											case "#ping":
												var str = "#pong "+ new Date();
												pushText (str, job.contact.msisdn, user.line);
												break;
											case "#prods":
												pushText (legalineaProductsMenu(), job.contact.msisdn, user.line);
												break;
											case "#server":
												var str = "Server data:\n";
												str += "Datetime: "+ new Date()+"\n";
												str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
												str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
												pushText (str, job.contact.msisdn, user.line);
												break;
											case "#test":
												if (parts[1]) {
													saveTest (parts[1], job.contact.msisdn);
												}
												break;
											case "#w":
												getWordleStats([], function(ps) {
													console.log (new Date(), "Wordle Stats Built")
													var d = new Date();
													pushText (buildWordleStats(ps,  d.getFullYear()), job.contact.msisdn, user.line, user.cid);
												});
												break;	
											case "#tsback":
												console.log ("TS Back returned for", job.contact.msisdn)
												if (tests[job.contact.msisdn]) {
													//test found
													tests[job.contact.msisdn].tsr = Date.now();	
												}
												break;
											default:
												var str = "Operación desconocida";
												pushText (str, job.contact.msisdn, user.line);
										} //end switch parts operation
									} else {
										if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
											salir(job, user);
										} else {
											if (body.toLowerCase().indexOf("wordle")==0 || body.toLowerCase().indexOf("wordlefr")>=0  || body.toLowerCase().indexOf("la palabra del")>=0 ) {
												saveWordleResult(body.toLowerCase(), user, function(msg) {
													pushText (msg, job.contact.msisdn, user.line, user.cid);
												});
											} else {
												//chat is not operation, run bot
												plugin.bot(body.toLowerCase(), job, user, step, group, account);
											}
										}
									} //end if operation
								} //body is empty
								//job is completed
								finishJob(job);
								break;
							case 'image':
								plugin.bot("[image]", job, user, step, group, account);
								//job is completed
								finishJob(job);
								break;
							case 'video':
								plugin.bot("[video]", job, user, step, group, account);
								//job is completed
								finishJob(job);
								break;
							case 'audio':
								plugin.bot("[audio]", job, user, step, group, account);
								//job is completed
								finishJob(job);
								break;
							case 'file':
								plugin.bot("[file]", job, user, step, group, account);
								//job is completed
								finishJob(job);
								break;
							case 'location':
								plugin.bot("[location]", job, user, step, group, account);
								//job is completed
								finishJob(job);
								break;
							default:
								//job is completed
								finishJob(job);
								break;
						} //end switch mo type
				} else { //end idle check
					console.log ('user '+job.contact.msisdn+'@'+user.line+' is idle');
					//is a reset operation?
					var body = job.msg.content.text;
					if(body){
						if (body.length>0) {
							//trim spaces
							body = body.trim();
							if (body.toLowerCase().indexOf("#") == 0) {
								if (body.toLowerCase() == "#reset") {
									resetBot(job.contact.msisdn, user, "Reset Session OK");
								}
							}
						}
					}
					//job is completed
					finishJob(job);
				}
			});
		}//end switch job type
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	});
}

global.pageBreak = function() {
	return '\n\n--------------------------oooo--------------------------\n\n'
	}

global.pushText = function  (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	
	firebase.database().ref("/accounts/"+account+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		console.log(ob);
		
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.setUserParam = function(key, val, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).set(val);
}

global.updateUser = function(ob, contact, line) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob );
}

global.updateUserCB = function(ob, contact, cb) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob ).then(function(res) {
		if (cb) { cb (true) }
	})
	.catch(function(error) {
		if (cb) { cb (false) }
	});
}

global.createTrans = function(contactId, ob, tpay, value, ref, ref2) {
	
	var trans = {
		account: contactId,
		createdBy: apiUser,
		group: 'val',
		ref: ref,
		rubro: ob["x-prod"],
		status: "active",
		tsc: Date.now(),
		type: "deb",
		value: value,
		data: {}
	}
	//copia los params x-
	var obj = ob;
	for (var prop in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, prop)) {
			if (prop.indexOf("x-")==0) { trans['data'][prop] = obj[prop] }
		}
	}
	//guarda cupon
	if (tpay == "cup") {
		trans.ref2 = ref2.id;
	}

	if(tpay == "pay" || tpay == "trans"){
		trans.ref2 = ref2;
	}
	
	firebase.database().ref("/accounts/"+account+"/trans/").push( trans, function() {
	
		if (tpay == "cup") {
			var trans2 = JSON.parse(JSON.stringify(trans));
			trans2.type = "cre";
			trans2.tpay = tpay;
			trans2.caja = "cupones"
			trans2.ref = "Pago con cupón " + ref2.id;
			delete trans2.data;
			firebase.database().ref("/accounts/"+account+"/trans/").push( trans2 );
		}

		if (tpay == "pay") {
			var trans2 = JSON.parse(JSON.stringify(trans));
			trans2.type = "cre";
			trans2.tpay = tpay;
			trans2.caja = "payphone"
			trans2.ref = "Pago con payphone " + ref2;
			delete trans2.data;
			firebase.database().ref("/accounts/"+account+"/trans/").push( trans2 );
		}

		if (tpay == "trans") {
			var trans2 = JSON.parse(JSON.stringify(trans));
			trans2.type = "cre";
			trans2.tpay = tpay;
			trans2.caja = "transferencias"
			trans2.ref = "Pago con transferencia " + ref2;
			delete trans2.data;
			firebase.database().ref("/accounts/"+account+"/trans/").push( trans2 );
		}
	});
}
global.createContact = function(msisdn, obj) {
	console.log("Searching contact");
	
	getContact(msisdn, function(contact) {
		//~ console.log("Res: ", contact);
		
		if (!contact) {
			let newUser = {
				createdBy: apiUser,
				email: obj["x-email"],
				group: 'val',
				name: obj["x-full-name"],
				phone: obj.id,
				status: "active",
				tsc: Date.now(),
				tsu: Date.now(),
				updatedBy: apiUser,
				"x-dniValid": false,
				docValidation: {dniPhoto1: false, dniPhoto2: false, dniPhoto3: false}
			}

			if(obj['x-prod'] == 'f2' || obj['x-prod'] == 'f2-2'){
				newUser.docValidation.rucDoc = false;
			}

			if(obj['x-prod'] == 'f3' || obj['x-prod'] == 'f3-2'){
				newUser.docValidation.rucDoc = false;
				newUser.docValidation.nombramiento = false;
				newUser.docValidation.constitucion = false;
			}

			//copy x attributes
			for (var prop in obj) {
				if (Object.prototype.hasOwnProperty.call(obj, prop)) {
					if (prop.indexOf("x-")==0) { newUser[prop] = obj[prop] }
				}
			}
			
			
			//patch for email
			delete newUser["x-email"]
			console.log(newUser);
			
			firebase.database().ref("/accounts/"+account+"/contacts/").push( newUser );
		} else {
			console.log ("else")
			delete obj.msgs

			let updatedContact = {
				email: obj["x-email"],
				group: 'val',
				name: obj["x-full-name"],
				status: "active",
				tsu: Date.now(),
				updatedBy: apiUser,
				"x-dniValid": false,
				docValidation: {dniPhoto1: false, dniPhoto2: false, dniPhoto3: false}
			}
			//copy x attributes
			if(obj['x-prod'] == 'f2' || obj['x-prod'] == 'f2-2'){
				updatedContact.docValidation.rucDoc = false;
			}

			if(obj['x-prod'] == 'f3' || obj['x-prod'] == 'f3-2'){
				updatedContact.docValidation.rucDoc = false;
				updatedContact.docValidation.nombramiento = false;
				updatedContact.docValidation.constitucion = false;
			}
			
			for (var prop in obj) {
				if (Object.prototype.hasOwnProperty.call(obj, prop)) {
					if (prop.indexOf("x-")==0) { updatedContact[prop] = obj[prop] }
				}
			}
		
			//patch for email
			delete updatedContact["x-email"]
			
			firebase.database().ref("/accounts/"+account+"/contacts/"+contact.id).update( updatedContact );
		}
	});
}

global.updateContact = function(ob, contact) {
	console.log("/accounts/"+account+"/contacts/"+contact+"/", ob);
	firebase.database().ref("/accounts/"+account+"/contacts/"+contact+"/").update( ob );
}

global.updateContactDocValidation = function(ob, contact) {
	firebase.database().ref("/accounts/"+account+"/contacts/"+contact+"/docValidation").update( ob );
}


function startSession (contact, user) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/tsso").set( Date.now() );
	logStartSession(contact, user.chn);
}

global.successMessage = function() {
	var msg = "Hemos enviado toda tu información a ARGOSDATA quien se encargará de emitir tu firma electrónica.\n\n";
	msg += "Te enviarán un correo desde *noreply@argosdata.com.ec* en el transcurso de *10 minutos* con un link para poder ingresar la clave de tu firma (el PIN) y poder descargarla. Si el correo no llega revisa tu carpeta de SPAM\n\n";
	msg += "*No compartas el PIN de tu firma con nadie!* Te recomendamos revisar esta información sobre la seguridad de uso de una firma electrónica: https://legalinea.com/index.html#tips\n\n"
	msg += "*Importante* Recuerda descargar tu firma en tu PC, no la descargues en tu teléfono celular. Revisa este enlace donde tendrás instrucciones de como usar tu firma para firmar digitalmente un documento:\n https://legalinea.com/preguntas-frecuentes.html\n\n"
	msg += "Para usar tu firma debes usar *Firma EC versión 2.9.0 o superior*. En caso de no tenerla puedes descargarla en https://www.firmadigital.gob.ec/descargar-firmaec/\n\n"
	msg += "Gracias por usar nuestros servicios, ha sido un placer atenderte. No dudes en contactarnos si requieres ayuda\n\n";
	msg += "Nos vemos pronto!";
	return msg
}

global.logStartSession = function(contact, chn) {
	var log = {
		type: "session-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logEndSession = function(contact, chn, tsso, op) {
	var log = {
		type: "session-end",
		tssx: Date.now(),
		ref: contact+"",
		ts: Date.now(),
		op: op
	}
	if (tsso) { log.tsso = tsso; }
	if (chn) { log.chn = chn; }
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logStartTicket = function(contact, chn) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
	//send notification 
	pushTemplate(["https://v3.textcenter.net/#inbox"], "new_ticket_created", "593984252217", botLine);
}



global.pushTemplate = function  (params, templateName, contact, line) {
	
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:"Nuevo ticket", rep: "bot", t:"mt", ts: Date.now(), type: "txt", st:"qed", lu: Date.now()};
	mt.isTemplate = true	
	mt.lang = "es"; //todo read from DB
	mt.params = params.join("|");
	mt.templateName = templateName;
	firebase.database().ref("/accounts/"+account+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}


global.resetUserParam = function(key, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).remove();
}

global.resetContactParam = function(key, contact) {
	firebase.database().ref("/accounts/"+account+"/contacts/"+contact+"/"+key).remove();
}

global.saveForm = function(ob, cb) {
	firebase.database().ref("/accounts/"+account+"/forms").push(ob).then(function(res){
		cb(res.key);
	});
}
global.saveDoc = function(ob, cb) {
	firebase.database().ref("/accounts/"+account+"/docs").push(ob).then(function(res){
		cb(res.key);
	});
}
global.salir = function(job, user) {
	var self = this;
	logEndSession(job.contact.msisdn, user.chn, user.tsso, "s");	
	var str = "Muchas gracias por usar nuestro servicio. Hasta pronto!"
	pushText (str, job.contact.msisdn, user.line);
	resetUserParam('step', job.contact.msisdn);
	resetUserParam('pin', job.contact.msisdn);
	resetUserParam('group', job.contact.msisdn);
	resetUserParam('account', job.contact.msisdn);
	resetUserParam('tsso', job.contact.msisdn);
	var obj = user;
	for (var prop in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, prop)) {
			if (prop.indexOf("x-")==0) { resetUserParam(prop, job.contact.msisdn); }
		}
	}
	setUserParam('status', 'reset', job.contact.msisdn);
}

global.resetPaymentMethod = function(msisdn){

	getContact(msisdn, function(contact) {
		resetUserParam("x-transferPhoto", msisdn);
		resetUserParam("x-transferValid", msisdn);
		resetUserParam("x-transferValidated", msisdn);

		resetContactParam("x-transferPhoto", contact.id);
		resetContactParam("x-transferValid", contact.id);
		resetContactParam("x-transferValidated", contact.id);
	});
}
global.resetBot = function(contact, user, msg) {
	
	logEndSession(contact, user.chn, user.tsso, "r");
	
	if (msg) { pushText (msg, contact, user.line); }
	resetUserParam('step', contact);
	resetUserParam('pin', contact);
	resetUserParam('group', contact);
	resetUserParam('account', contact);
	resetUserParam('tsso', contact);
	var obj = user;
	for (var prop in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, prop)) {
			if (prop.indexOf("x-")==0) { resetUserParam(prop, contact); }
		}
	}
	setUserParam('status', 'reset', contact);
}

global.resetContact = function(contactId, obj) {
	resetContactParam("docValidation", contactId);
	for (var prop in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, prop)) {
			if (prop.indexOf("x-")==0) { resetContactParam(prop, contactId); }
		}
	}
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);	
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()	
}

global.getContact = function(phone, cb) {
	console.log ("Searching for",phone);
	firebase.database().ref("/accounts/"+account+"/contacts").orderByChild("phone").equalTo(phone+'').once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			console.log ("Contact found", arr.length);
			var ob = arr[0];
			if (ob.group) {
				cb (ob)
			} else {
				cb (null);	
			}
		} else {
			cb (null);
		}
	}).catch(function(error) {
		console.log (error)
		cb (null);
	});
}

global.getBalance = function(accId, cb) {
	console.log ("Checking balance for",accId);
	firebase.database().ref("/accounts/"+account+"/trans").orderByChild("account").equalTo(accId).once("value", function(res) {
		if (res.val()) {
			var b = 0;
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			for (var i=0; i<arr.length; i++) {
				if (arr[i].type=="deb") { b -= arr[i].value; } 
				if (arr[i].type=="cre") { b += arr[i].value; }
			}
			cb(b);
		} else {
			cb(0);	
		}	
	});
}

global.validateCoupon = function(coupon, phone, dni, prod, cb) {
	console.log("/accounts/"+account+"/coupons/" + coupon.toUpperCase());
	
	firebase.database().ref("/accounts/"+account+"/coupons/" + coupon.toUpperCase()).once("value", function(res) {
		if(!res.val()){
			if (cb) { cb(null); }
			return;
		}
		let val = res.val();
		val.uses = val.uses - 1;

		if(val.product != prod){
			if (cb) { cb(null); }
			return;
		}


		if(val.uses >= 0){
			val.tsu = Date.now();
			val.phone = phone;
			val.dni = dni;
			val.st = "used";
			
			firebase.database().ref("/accounts/"+account+"/coupons/" + coupon.toUpperCase()).update(val).then(function(res) {
				if (cb) { cb(val); }
			}).catch(function(error){
				console.log (error)
				console.log (task);
				if (cb) { cb(null); }
			});
		}else{
			val.st = "exp";
			val.uses = 0;
			firebase.database().ref("/accounts/"+account+"/coupons/" + coupon.toUpperCase()).update(val).then(function(res) {
				if (cb) { cb(null); }
			}).catch(function(error){
				console.log (error)
				console.log (task);
				if (cb) { cb(null); }
			});
		}
	});
}


global.isShopOpen = function() {
	var d = new Date();
	var strDate = d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear()
	var day = d.getDay();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (config.holidays.indexOf(strDate)>=0) {
		return false;
	} else {
		if (config.workDays[day]) {
			if (t>=config.officeOpen && t<config.officeClose) {
				return true;
			} else {
				return false;		
			}
		} else {
			return false;
		}
	}
}

global.isExtendedSchedule = function() {
	var d = new Date();	
	var strDate = d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear()
	var day = d.getDay();
	var t = (d.getHours() * 100) + d.getMinutes();

	if (config.holidays.indexOf(strDate)>=0) {
		return false;
	} else {
		if (config.workDays[day].available) {
			if (t >= config.workDays[day].open && t < config.workDays[day].close) {
				return true;
			} else {
				return false;		
			}
		} else {
			return false;
		}
	}
}



global.wspLinkLogin = function (credentials, callback) {
	request.post({
		url: wsppaylinkUrl + '/login',
		body: credentials,
		json: true
	}, function(error, response, body) {
		callback(error, body);
	});
}
  
  
global.generateLink =  function (reference, amount, user, callback) {
	
	let untaxedAmount = amount / 1.12;
	//round tax to 0 decimals
	untaxedAmount = Math.round(untaxedAmount * 100) / 100;
	untaxedAmount = parseInt(untaxedAmount.toString())
	let tax = amount - untaxedAmount;

	let data = {
		"amount": amount, //Integer
		"tax": tax, //Integer
		"amount_with_tax": untaxedAmount, //Integer
		"currency": "USD",
		"reference": reference, //Max 50 characters
		"phone_number": user.id
	}
	console.log(data);
	var options = {
		method: 'POST',
		url: wsppaylinkUrl + '/paylink/' + env.account + '/button',
		headers: {
			'x-access-token': wsppaylinkToken,
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	};

	request(options, function (error, response, body) {
		if (error) {
			callback("Error creating link", null);
		}else{
			console.log(body);
			try {
				let res = JSON.parse(body);
				console.log(res);
				callback(null, res.results);
			} catch (error) {
				console.log(error)
				callback("Error creating link", null);
			}
			
		}
	});
}
  
global.getLinkStatus = function (transactionId, callback) {
  
	request.get({
		headers: {
		'x-access-token': wsppaylinkToken
		},
		url: wsppaylinkUrl + '/paylink/status/' + transactionId
	}, function(error, response, body) {
		if (body) {
			callback(body.results);
		} else {
			callback("Error");
		}
	});
}

global.requestFirma = async function(user, callback){

	console.log("Requesting firma");
	
	var data = await prepareDataForUanataca(user);
	
	//console.log (data)
	
	setTimeout(function() {
		callback({result: true}, user)
		},10000)
	//~ var options = {
		//~ 'method': 'POST',
		//~ 'url': 'https://api.uanataca.ec/v4/solicitud',
		//~ 'headers': {
			//~ 'Content-Type': 'application/json'
		//~ },
		//~ 'body': JSON.stringify(data),
	//~ };
	
	
	//~ request(options, function (error, response) {
		//~ if (error){
			//~ console.log("ERROR Request Uanataca", user.line, error);
			//~ callback(false);
		//~ } else {
			//~ console.log("SUCCESS Request Uanataca", JSON.parse(response.body));
			//~ callback(JSON.parse(response.body), data);
		//~ }
	//~ });
}

global.legalineaProductsMenu = function() {
	var msg = "Por favor escoge la firma que necesitas:\n\n";
	for (var i=0; i<rubros.length; i++) {
		if(isShopOpen()){
			msg += (i+1)+". " + rubros[i].caption	
		}else{
			msg += (i+1)+". " + rubros[i].extendedCaption	
		}
	}
	msg += (rubros.length+1)+". Regresar";
	return msg;	
}

//test MD functions
function saveTest(ts, contact) {
	var test = {
		ts: Number(ts),
		tsr: Date.now(),
		contact: contact+""
	}
	firebase.database().ref("/accounts/"+account+"/tests").push(test);	
	console.log ("Test saved from", contact)
}

function saveWordleResult(kw, user, cb) {
	var msg = ""
	var parts = kw.split(" ");
	console.log ("parts", parts.length);
	var d = new Date();
	switch (parts.length) {
		case 3:
			
			var r = parts[2].split("/")[0];
			if (isNaN(r)) {
				r=7
			} else {
				r = Number(r)	
			} 
			
			var result = {
				ts: Date.now(),
				r: r
			}
			console.log (Date.now(), user.id, "Save Wordle",parts[1],r);
			var lan = getLan(kw);
			firebase.database().ref("/accounts/"+account+"/chats/"+user.id+"/wordle/"+lan+"/"+d.getFullYear()+"/"+parts[1]).set(result, function(res) {
				cb ("Listo! Ya guardé el resultado #"+parts[1]+" en "+lan.toUpperCase()+" :)");
			}).catch(function(error) {
				cb ("Soy un tonto robot y me equivoqué :(")
			});
			
			break; 
		case 5:
			//console.log (parts)
			if (parts[3].indexOf("#")==0) {
				var r = parts[4].split("/")[0];
				if (isNaN(r)) {
					r=7
				} else {
					r = Number(r)	
				} 
				
				var result = {
					ts: Date.now(),
					r: r
				}
				console.log (Date.now(), user.id, "Save Wordle",parts[3],r);
				var lan = getLan(kw);
				firebase.database().ref("/accounts/"+account+"/chats/"+user.id+"/wordle/"+lan+"/"+d.getFullYear()+"/"+parts[3].replace("#","")).set(result, function(res) {
					cb ("Listo! Ya guardé el resultado "+parts[3]+" en "+lan.toUpperCase()+" :)");
				}).catch(function(error) {
					cb ("Soy un tonto robot y me equivoqué :(")
				});
			} else {
				var r = parts[3].split("/")[0];
				if (isNaN(r)) {
					r=7
				} else {
					r = Number(r)	
				} 
				
				var result = {
					ts: Date.now(),
					r: r
				}
				console.log (Date.now(), user.id, "Save Wordle",parts[2],r);
				var lan = getLan(kw);
				firebase.database().ref("/accounts/"+account+"/chats/"+user.id+"/wordle/"+lan+"/"+d.getFullYear()+"/"+parts[2].replace("#","")).set(result, function(res) {
					cb ("Listo! Ya guardé el resultado "+parts[2]+" en "+lan.toUpperCase()+" :)");
				}).catch(function(error) {
					cb ("Soy un tonto robot y me equivoqué :(")
				});	
			}
			
			break;
		case 4:
			var r = parts[3].split("/")[0];
			if (isNaN(r)) {
				r=7
			} else {
				r = Number(r)	
			} 
			
			var result = {
				ts: Date.now(),
				r: r
			}
			console.log (Date.now(), user.id, "Save Wordle",parts[2],r);
			var lan = getLan(kw);
			firebase.database().ref("/accounts/"+account+"/chats/"+user.id+"/wordle/"+lan+"/"+d.getFullYear()+"/"+parts[2].replace("#","")).set(result, function(res) {
				cb ("Listo! Ya guardé el resultado "+parts[2]+" en "+lan.toUpperCase()+" :)");
			}).catch(function(error) {
				cb ("Soy un tonto robot y me equivoqué :(")
			});
			break
		case 6:
			var r = parts[4].split("/")[0];
			if (isNaN(r)) {
				r=7
			} else {
				r = Number(r)	
			} 
			
			var result = {
				ts: Date.now(),
				r: r
			}
			console.log (Date.now(), user.id, "Save Wordle",parts[3],r);
			var lan = getLan(kw);
			firebase.database().ref("/accounts/"+account+"/chats/"+user.id+"/wordle/"+lan+"/"+d.getFullYear()+"/"+parts[3].replace("#","")).set(result, function(res) {
				cb ("Listo! Ya guardé el resultado "+parts[3]+" en "+lan.toUpperCase()+" :)");
			}).catch(function(error) {
				cb ("Soy un tonto robot y me equivoqué :(")
			});
			break
		case 7:
			var r = parts[5].split("/")[0];
			if (isNaN(r)) {
				r=7
			} else {
				r = Number(r)	
			} 
			
			var result = {
				ts: Date.now(),
				r: r
			}
			console.log (Date.now(), user.id, "Save Wordle",parts[4],r);
			var lan = getLan(kw);
			firebase.database().ref("/accounts/"+account+"/chats/"+user.id+"/wordle/"+lan+"/"+d.getFullYear()+"/"+parts[4].replace("#","")).set(result, function(res) {
				cb ("Listo! Ya guardé el resultado "+parts[4]+" en "+lan.toUpperCase()+" :)");
			}).catch(function(error) {
				cb ("Soy un tonto robot y me equivoqué :(")
			});
			break
		default:
			console.log (parts);
			cb ("Soy un tonto robot y no entiendo este resultado:\n"+kw)
	}
	
}
function getLan(kw) {
	var lan = "en";
	var kw = kw.toLowerCase();
	if (kw.indexOf("wordlefr")>=0) {
		lan = "fr"	
	}
	if (kw.indexOf("(es)")>=0) {
		lan = "sp"	
	}
	if (kw.indexOf("palabra")>=0) {
		lan = "sp"	
	}
	return lan	
}
function buildWordleStats(ps, yr) {
	var stats = {
		en:{
			bingos:{},
			fails: {},
			rank: []
		},
		sp:{
			bingos:{},
			fails: {},
			rank: []
		},
		fr:{
			bingos:{},
			fails: {},
			rank: []
		}
	}
	var str = "*--Wordle Stats--*\n\n";
	//build them
	for (var i=0; i<ps.length; i++) {
		var p = ps[i]
		//console.log ("Build stats for", p.id)
		//en
		if (p.w) {
		
			if (p.w.en) {
				var w = p.w.en[yr]
				var c = 0;
				var t = 0;
				for (var r in w) {
					c += 1;
					if (r!=7) {
						t += w[r].r;
					}
					if (w[r].r == 1) {
						if (!stats.en.bingos["1"]) { stats.en.bingos["1"] = {} }
						if (!stats.en.bingos["1"][p.id]) { stats.en.bingos["1"][p.id] = 0 }
						stats.en.bingos["1"][p.id] += 1;
					}
					if (w[r].r == 2) {
						if (!stats.en.bingos["2"]) { stats.en.bingos["2"] = {} }
						if (!stats.en.bingos["2"][p.id]) { stats.en.bingos["2"][p.id] = 0 }
						stats.en.bingos["2"][p.id] += 1;
					}
					if (w[r].r == 3) {
						if (!stats.en.bingos["3"]) { stats.en.bingos["3"] = {} }
						if (!stats.en.bingos["3"][p.id]) { stats.en.bingos["3"][p.id] = 0 }
						stats.en.bingos["3"][p.id] += 1;
					}
					if (w[r].r == 7) {
						if (!stats.en.fails[p.id]) { stats.en.fails[p.id] = 0 }
						stats.en.fails[p.id] += 1;
					}
				}
				stats.en.rank.push({
					id: p.id,
					c: c,
					t: t,
					avg: Math.round((t/c)*100)/100
				});	
			}	
			//sp
			if (p.w.sp) {
				w = p.w.sp[yr]
				c = 0;
				t = 0;
				for (var r in w) {
					c += 1;
					if (r!=7) {
						t += w[r].r;
					}
					if (w[r].r == 1) {
						if (!stats.sp.bingos["1"]) { stats.sp.bingos["1"] = {} }
						if (!stats.sp.bingos["1"][p.id]) { stats.sp.bingos["1"][p.id] = 0 }
						stats.sp.bingos["1"][p.id] += 1;
					}
					if (w[r].r == 2) {
						if (!stats.sp.bingos["2"]) { stats.sp.bingos["2"] = {} }
						if (!stats.sp.bingos["2"][p.id]) { stats.sp.bingos["2"][p.id] = 0 }
						stats.sp.bingos["2"][p.id] += 1;
					}
					if (w[r].r == 3) {
						if (!stats.sp.bingos["3"]) { stats.sp.bingos["3"] = {} }
						if (!stats.sp.bingos["3"][p.id]) { stats.sp.bingos["3"][p.id] = 0 }
						stats.sp.bingos["3"][p.id] += 1;
					}
					if (w[r].r == 7) {
						if (!stats.sp.fails[p.id]) { stats.sp.fails[p.id] = 0 }
						stats.sp.fails[p.id] += 1;
					}
				}
				stats.sp.rank.push({
					id: p.id,
					c: c,
					t: t,
					avg: Math.round((t/c)*100)/100
				});
			}
			//fr
			if (p.w.fr) {
				w = p.w.fr[yr]
				c = 0;
				t = 0;
				for (var r in w) {
					c += 1;
					if (r!=7) {
						t += w[r].r;
					}
					if (w[r].r == 1) {
						if (!stats.fr.bingos["1"]) { stats.fr.bingos["1"] = {} }
						if (!stats.fr.bingos["1"][p.id]) { stats.fr.bingos["1"][p.id] = 0 }
						stats.fr.bingos["1"][p.id] += 1;
					}
					if (w[r].r == 2) {
						if (!stats.fr.bingos["2"]) { stats.fr.bingos["2"] = {} }
						if (!stats.fr.bingos["2"][p.id]) { stats.fr.bingos["2"][p.id] = 0 }
						stats.fr.bingos["2"][p.id] += 1;
					}
					if (w[r].r == 3) {
						if (!stats.fr.bingos["3"]) { stats.fr.bingos["3"] = {} }
						if (!stats.fr.bingos["3"][p.id]) { stats.fr.bingos["3"][p.id] = 0 }
						stats.fr.bingos["3"][p.id] += 1;
					}
					if (w[r].r == 7) {
						if (!stats.fr.fails[p.id]) { stats.fr.fails[p.id] = 0 }
						stats.fr.fails[p.id] += 1;
					}
				}
				stats.fr.rank.push({
					id: p.id,
					c: c,
					t: t,
					avg: Math.round((t/c)*100)/100
				});
			}
		} else { //end check new plauer
			stats.en.rank.push({
				id: p.id,
				c: 0,
				t: 0,
				avg: 0
			});
			stats.sp.rank.push({
				id: p.id,
				c: 0,
				t: 0,
				avg: 0
			});
			stats.fr.rank.push({
				id: p.id,
				c: 0,
				t: 0,
				avg: 0
			});
		}
	}
	
	//write report
	str += "*INGLÉS*\n\n"
	var subt = ["Bingos", "Doses", "Treses" ]
	for (var i=1; i<=3; i++) {
		str += "*"+subt[i-1]+":*\n"
		var bs = stats.en.bingos[i];
		if (bs) {
			var arr = []
			for (var p in bs) {
				var player = _.findWhere(ps, {id: p});
				arr.push({n:player.n, c:bs[p]})	
			}
			arr = _.sortBy(arr, function(ob) {return -ob.c});
			for (var j=0; j<arr.length; j++) {	
				str += arr[j].n +": *"+arr[j].c+"*\n"
			}
		}
		str += "\n";
	}
	str += "*Rankign EN:*\n"
	arr = _.sortBy(stats.en.rank, function(ob) {return ob.avg});
	for (var j=0; j<arr.length; j++) {
		var player = _.findWhere(ps, {id: arr[j].id});
		if (player) {
			str += player.n +": *"+arr[j].avg+"* ("+arr[j].t+"/"+arr[j].c+")\n"
		}
	}
	
	str += "\n*Fails EN:*\n"
	var fs = stats.en.fails;
	if (fs) {
		var arr = []
		for (var p in fs) {
			var player = _.findWhere(ps, {id: p});
			arr.push({n:player.n, f:fs[p]})	
		}
		arr = _.sortBy(arr, function(ob) {return -ob.f});
		for (var j=0; j<arr.length; j++) {	
			str += arr[j].n +": *"+arr[j].f+"*\n"
		}
	}
			
	str += "\n______________________________________________\n\n";
	str += "*ESPAÑOL*\n\n"
	var subt = ["Bingos", "Doses", "Treses" ]
	for (var i=1; i<=3; i++) {
		str += "*"+subt[i-1]+":*\n"
		var bs = stats.sp.bingos[i];
		if (bs) {
			var arr = []
			for (var p in bs) {
				var player = _.findWhere(ps, {id: p});
				arr.push({n:player.n, c:bs[p]})	
			}
			arr = _.sortBy(arr, function(ob) {return -ob.c});
			for (var j=0; j<arr.length; j++) {	
				str += arr[j].n +": *"+arr[j].c+"*\n"
			}
		}
		str += "\n";
	}
	str += "*Rankign SP:*\n"
	arr = _.sortBy(stats.sp.rank, function(ob) {return ob.avg});
	for (var j=0; j<arr.length; j++) {
		var player = _.findWhere(ps, {id: arr[j].id});	
		if (player) {
			str += player.n +": *"+arr[j].avg+"* ("+arr[j].t+"/"+arr[j].c+")\n"
		}
	}
	str += "\n*Fails SP:*\n"
	var fs = stats.sp.fails;
	if (fs) {
		var arr = []
		for (var p in fs) {
			var player = _.findWhere(ps, {id: p});
			arr.push({n:player.n, f:fs[p]})	
		}
		arr = _.sortBy(arr, function(ob) {return -ob.f});
		for (var j=0; j<arr.length; j++) {	
			str += arr[j].n +": *"+arr[j].f+"*\n"
		}
	}
	str += "\n______________________________________________\n\n";
	str += "*FRANCES*\n\n"
	var subt = ["Bingos", "Doses", "Treses" ]
	for (var i=1; i<=3; i++) {
		str += "*"+subt[i-1]+":*\n"
		var bs = stats.fr.bingos[i];
		if (bs) {
			var arr = []
			for (var p in bs) {
				var player = _.findWhere(ps, {id: p});
				arr.push({n:player.n, c:bs[p]})	
			}
			arr = _.sortBy(arr, function(ob) {return -ob.c});
			for (var j=0; j<arr.length; j++) {	
				str += arr[j].n +": *"+arr[j].c+"*\n"
			}
		}
		str += "\n";
	}
	str += "*Rankign FR:*\n"
	arr = _.sortBy(stats.fr.rank, function(ob) {return ob.avg});
	for (var j=0; j<arr.length; j++) {
		var player = _.findWhere(ps, {id: arr[j].id});	
		if (player) {
			str += player.n +": *"+arr[j].avg+"* ("+arr[j].t+"/"+arr[j].c+")\n"
		}
	}
	str += "\n*Fails FR:*\n"
	var fs = stats.fr.fails;
	if (fs) {
		var arr = []
		for (var p in fs) {
			var player = _.findWhere(ps, {id: p});
			arr.push({n:player.n, f:fs[p]})	
		}
		arr = _.sortBy(arr, function(ob) {return -ob.f});
		for (var j=0; j<arr.length; j++) {	
			str += arr[j].n +": *"+arr[j].f+"*\n"
		}
	}
	//console.log (stats)
	return str;
}

function getWordleStats(ps, cb0) {
	var arr = _.clone(players);
	loopLoad(ps, arr, function() {
		cb0 (ps);
	});
}

function loopLoad(ps, arr, cb) {
	if (arr.length <=0) {
		cb();
	} else {		
		loadPlayer(arr.shift(), function(p) {
			ps.push(p);
			loopLoad(ps, arr, cb);	
		});
	}	
}

function loadPlayer(id, cb) {
	firebase.database().ref("/accounts/"+account+"/chats/"+id).once('value', function(res) {
		if (res.val()) {
			var ob = res.val();
			//console.log (ob.nick);
			var  p = {
				id: ob.id,
				n: ob.nick,
				w: ob.wordle
				}
			cb(p);
		} else {
			cb ({})	
		}		
	});	
}

function runBotPinger() {
	console.log (new Date(), "Running bot pinger");
	tests = {}
	var ts = Date.now();
	for (i=0; i<lines.length; i++) {
		tests[lines[i]] = {
			tss: ts,
			tsr: 0
		}
		//push template
		pushTemplate(["#ts"+ts], "task_completed", lines[i], "593991684677");
		
	}
	//wait and check tests
	setTimeout(function() {
		console.log (new Date(), "Checking tests");
		var valid = true;
		for (i=0; i<lines.length; i++) {
			if (tests[lines[i]]) {
				if (tests[lines[i]].tsr>0) {
					//answer received
					console.log ("test OK for", lines[i])
					lines[i].ok = true;
					lines[i].msg = "OK";
					
				} else {
					console.log ("Test not returned for",lines[i])	
					lines[i].ok = false;
					lines[i].msg = "No return";
					valid = false;
				}
			} else {
				console.log ("Test not run for",lines[i])	
				lines[i].ok = false;
				lines[i].msg = "Not run";
				valid = false;
			}
		}
		//save test
		var test = {
			ts: Date.now(),
			contact: "na",
			type: "botPingBack",
			test: tests,
			hasError: !valid
		}
		firebase.database().ref("/accounts/"+account+"/tests").push(test);
		
		//notify Ricki
		if (!valid) {
			//push template
		pushTemplate(["Error en test ping back"], "task_completed", "593984252217", "593991684677");
		}
	}, 120000);
}

