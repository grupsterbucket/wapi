var request = require('request');
global.tools = require('./tools');
var fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
var bot = require('./botPlugin');
global._ = require('underscore');
global.firebase = require('firebase');
var CryptoJS = require("crypto-js");
var account = "coniski";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 250;
global.config = {}
global.calendars = []

//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		loadAccountConfig();
		setTimeout(function(){
			listenToJobs();
			processJobQueue();
		}, 1000);
	}	
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/'+account+'/config/misc').on('value', function(snapshot) {		  
		config = snapshot.val();
		console.log ("Config fetched for account",account);
	});
	firebase.database().ref('accounts/'+account+'/config/calendars').on('value', function(snapshot) {		  
		calendars = _.map(snapshot.val(), function(ob, key){
			ob.id = key;
			return ob;
		});
		console.log ("Calendars fetched for account",account);
	});
}
function listenToJobs() {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	}
}

function processJob(job) {
	switch (job.type) {
		case 'send-notification':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				pushText (job.str, job.contact, user.line, user.chn);
				//job is completed
				finishJob(job);
			});
			break;
		case 'reset-bot':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				resetBot (job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
		case 'booking-completed':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				//return main menu to user
				resetBot (job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
		case 'finish-order':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				//job is completed
				finishJob(job);
			});
			break;
		case 'payment-success':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				var ob = {"x-pagoStatus": "approved"}
				user['x-pagoStatus'] = "approved";
				updateUserCB(ob, user.id, function(saved){
					if(saved){
						getContact(job.contact, function(contact) {
							if(!contact){
								//Todo uanataca error logic
								var msg = "Hubo un error al solicitar la firma de " + job.contact + " No contact found " + "https://v3.textcenter.net/#inbox";
								pushTemplate([msg], "new_ticket_created", "593995453544", "593995945725");		
							}
							let retryInterval;
							let attempt = 1;
							pushText ("Tu firma está siendo procesada, por favor espera un momento...", job.contact, user.line);
							requestFirma(user, function(res, data){
								pushText (successMessage(), job.contact, user.line);

								if(!res) res = {result: false};
		
								if(!res.result){
									retryInterval = setInterval(function(){
										requestFirma(user, function (res, data) {
											attempt++;
											if(attempt == 3){
												var msg = "Hubo un error al solicitar la firma de " + job.contact + " " + JSON.stringify(res) + " " + "https://v3.textcenter.net/#inbox";
												pushTemplate([msg], "new_ticket_created", "593995453544", "593995945725");	
											}
											if(!res.result) return;
											
											clearInterval(retryInterval);
											
											delete data.f_cedulaFront;
											delete data.f_cedulaBack;
											delete data.f_selfie;
											delete data.f_copiaruc;
											delete data.f_nombramiento;
											delete data.f_constitucion;

											updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
												if (saved) {
													let rubro = _.findWhere(rubros, {code: user['x-prod']})
													let value = rubro.value;
													let ref = rubro.name;
													user["x-uanatacaReq"] =  {data:data, res: res}
													createTrans(contact.id, user, "pay", value, ref, promo);
													resetBot(job.contact, user);
												} 	
											});
										});
									},20000)
									return;
								}
								
								delete data.f_cedulaFront;
								delete data.f_cedulaBack;
								delete data.f_selfie;
								delete data.f_copiaruc;
								delete data.f_nombramiento;
								delete data.f_constitucion;
		
								updateUserCB({"x-uanatacaReq": {data:data, res: res}, }, user.id, function(saved) {
									if (saved) {
										let rubro = _.findWhere(rubros, {code: user['x-prod']})
										let value = rubro.value;
										let ref = rubro.name;
										user["x-uanatacaReq"] =  {data:data, res: res}
										createTrans(contact.id, user, "pay", value, ref, user["x-pagoId"]);
										resetBot(job.contact, user);
									}
								});
								
							});
						});
					}
				});
				finishJob(job);
			});
			break;
		default:
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact.msisdn).once("value", function(snapshot) {
				var user = snapshot.val();
				var step = "0";
				var group = "main";
				if (user.step) {step = user.step};
				if (user.group) {group = user.group};
				
				if (user.tsso) {
					console.log ("User "+job.contact.msisdn+" is at step:", step, "group:", group);
				} else {
					startSession (job.contact.msisdn, user);	
					console.log ("New Session for "+job.contact.msisdn+"");
				}
				
				if (step != 'idle') {
						console.log ("Process MO ", job.msg.type);
						switch (job.msg.type) {
							case 'text':
								console.log ("Body",job.msg.content.text);
								var body = job.msg.content.text;
								if (body.length>0) {
									//trim spaces
									body = body.trim();

									//catch operations
									if (body.toLowerCase().indexOf("#") == 0) {
										var parts = body.toLowerCase().split(" ");
										//match direct operation
										switch ( parts[0] ) {
											
											case "#padel":
												updateUser({step:"p0"}, job.contact.msisdn);
												bot.padelBot("na", job, user, "p0", group, account);
												break;
										} //end switch parts operation
									}
								} //body is empty
								//job is completed
								finishJob(job);
								break;
							default:
								//job is completed
								finishJob(job);
								break;
						} //end switch mo type
				} else { //end idle check
					console.log ('user '+job.contact.msisdn+'@'+user.line+' is idle');
					//is a reset operation?
					if (job.msg.content.text) {
						var body = job.msg.content.text;
						if (body.length>0) {
							//trim spaces
							body = body.trim();
							if (body.toLowerCase().indexOf("#") == 0) {
								if (body.toLowerCase() == "#reset") {
									resetBot(job.contact.msisdn, user, "Reset Session OK");
								}
							}
						}
					}
					//job is completed
					finishJob(job);
				}
			});
	} //end switch job type
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	});
}

function runMainBot(kw, job, user, step, group) {
	
	//process step
	switch (step) {
		case "0":
			pushText (mainMenu(true), job.contact.msisdn, user.line, user.chn);
			setUserParam("step","1", job.contact.msisdn);
			break;
			//end step 0
		case "1":
			if (["1","1.","uno","soporte","servicio"].indexOf(kw)>=0) {
				if (isShopOpen()) {
					pushText ("Listo, en un momento un agente de servicio le atenderá.", job.contact.msisdn, user.line, user.chn);	
				} else {
					pushText ("Hemos recibido su solicitud.\n\nLe recordamos que nuestro horario de atención por este canal es "+config.horarioStr+". Un agente de servicio le responderá apenas retornemos a las actividades. Gracias por su comprensión!", job.contact.msisdn, user.line, user.chn);
				}
				updateUser({step:"idle", group:"ser", status: "active", tso: Date.now()}, job.contact.msisdn); 
				logStartTicket(job.contact.msisdn, user.chn);
			} else if (["2","2.","dos","contacto"].indexOf(kw)>=0) {
				if (isShopOpen()) {
					pushText ("Listo, en un momento un especialista le atenderá.", job.contact.msisdn, user.line, user.chn);	
				} else {
					pushText ("Hemos recibido su solicitud.\n\nLe recordamos que nuestro horario de atención por este canal es "+config.horarioStr+". Un agente de servicio le responderá apenas retornemos a las actividades. Gracias por su comprensión!", job.contact.msisdn, user.line, user.chn);
				}
				updateUser({step:"idle", group:"com", status: "active", tso: Date.now()}, job.contact.msisdn); 
				logStartTicket(job.contact.msisdn, user.chn);
			} else if (["3","3.","tres","productos"].indexOf(kw)>=0) {
				pushText ("Por favor visite nuestro website para mayor información: https://coniski.com/" + pageBreak() + mainMenu(false), job.contact.msisdn, user.line, user.chn);
			} else if (["4","4.","cuatro","demo"].indexOf(kw)>=0) {
				pushText (demosMenu(), job.contact.msisdn, user.line, user.chn);
				setUserParam("step","2", job.contact.msisdn);	
			} else {
				pushText ("Por favor digite una de las opciones o escriba *salir* para terminar la sesión", job.contact.msisdn, user.line, user.chn);
			} 
			
			break;
			//end step 1
		case "2":
			if (["1","1.","uno","carrito","compras","pedido","orden"].indexOf(kw)>=0) {
				updateUser({step:"c0"}, job.contact.msisdn); 
				bot.cartBot(kw, job, user, "c0", group, account);
			} else if (["2","2.","dos","firma"].indexOf(kw)>=0) {
				updateUser({step:"f0"}, job.contact.msisdn); 
				bot.firmaBot(kw, job, user, "f0", group, account);
			} else if (["3","3.","tres","cita","agenda"].indexOf(kw)>=0) {
				updateUser({step:"a0"}, job.contact.msisdn); 
				bot.citaBot(kw, job, user, "a0", group, account);
			} else if (["4","4.","cuatro","banca"].indexOf(kw)>=0) {
				updateUser({step:"b0"}, job.contact.msisdn); 
				bot.segurosBot(kw, job, user, "b0", group, account);
			} else if (["5","5.","cinco","condo"].indexOf(kw)>=0) {
				updateUser({step:"h0"}, job.contact.msisdn); 
				habitat.bot(kw, job, user, "h0", group, account);
			} else {
				pushText ("Por favor digite una de las opciones o escriba *salir* para terminar la sesión", job.contact.msisdn, user.line, user.chn);
			} 
			
			break;
			//end step 1
		default:
			if (step.indexOf("c")==0) {
				bot.cartBot(kw, job, user, step, group, account);	
			}
			if (step.indexOf("f")==0) {
				bot.firmaBot(kw, job, user, step, group, account);	
			}
			if (step.indexOf("a")==0) {
				bot.citaBot(kw, job, user, step, group, account);	
			}
			if (step.indexOf("b")==0) {
				bot.segurosBot(kw, job, user, step, group, account);	
			}
			if (step.indexOf("l")==0) {
				bot.liberaBot(kw, job, user, step, group, account);	
			}
	}//end switch step
} 

global.mainMenu = function (greet) {
	if (greet) {
		var str = "Gracias por contactar al chatbot de Coniski.\nPor favor digite una opción:\n\n";
	} else {
		var str = "Por favor digite una opción:\n\n";
	}
	str += "1. Servicio al cliente\n";
	str += "2. Contactarnos\n";
	str += "3. Información sobre nuestros productos\n";
	str += "4. Correr un demo\n\n";
	str += "Escriba *salir* si desea terminar la sesión";
	return str;		
}

global.demosMenu = function () {
	var str = "Por favor digite una opción para correr un demo:\n\n";
	str += "1. Carrito de compras\n";
	str += "2. Firma electrónica\n";
	str += "3. Agendar una cita médica\n";
	str += "4. Servicios seguros\n\n";
	str += "Escriba *salir* si desea terminar la sesión";
	return str;		
}

global.pageBreak = function() {
	return '\n\n--------------------------oooo--------------------------\n\n'
	}

global.pushText = function  (body, contact, line, chn, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, chn: chn, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.pushTemplate = function  (params, templateName, contact, line) {
	
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:"Nuevo ticket", rep: "bot", t:"mt", ts: Date.now(), type: "txt", st:"qed", lu: Date.now()};
	mt.isTemplate = true	
	mt.lang = "es"; //todo read from DB
	mt.params = params.join("|");
	mt.templateName = templateName;
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.setUserParam = function(key, val, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).set(val);
}

global.updateUser = function(ob, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob );
}

function startSession (contact, user) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/tsso").set( Date.now() );
	logStartSession(contact, user.chn);
}

global.logStartSession = function(contact, chn) {
	var log = {
		type: "session-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logEndSession = function(contact, chn, tsso, op) {
	var log = {
		type: "session-end",
		tssx: Date.now(),
		ref: contact+"",
		ts: Date.now(),
		op: op
	}
	if (tsso) { log.tsso = tsso; }
	if (chn) { log.chn = chn; }
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logStartTicket = function(contact, chn) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
	//send notification 
	  pushTemplate(["Elias","http://v3.textcenter.net/"], "new_task_update", "593999703430", "593939397007");
	pushTemplate(["Ricardo","http://v3.textcenter.net/"], "new_task_update", "593984252217", "593939397007");
}

global.resetUserParam = function(key, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).remove();
}

global.saveForm = function(ob, cb) {
	firebase.database().ref("/accounts/"+account+"/forms").push(ob).then(function(res){
		cb(res.key);
	});
}

global.salir = function(job, user) {
	var self = this;
	logEndSession(job.contact.msisdn, user.chn, user.tsso, "s");	
	var str = "Gracias por usar este servicio. Adios!"
	pushText (str, job.contact.msisdn, user.line, user.chn);
	resetUserParam('step', job.contact.msisdn);
	resetUserParam('pin', job.contact.msisdn);
	resetUserParam('condos', job.contact.msisdn);
	resetUserParam('group', job.contact.msisdn);
	resetUserParam('account', job.contact.msisdn);
	resetUserParam('tsso', job.contact.msisdn);
	setUserParam('status', 'reset', job.contact.msisdn);
	
}

global.resetBot = function(contact, user, msg) {
	logEndSession(contact, user.chn, user.tsso, "r");
	
	if (msg) { pushText (msg, contact, user.line, user.chn); }
	resetUserParam('step', contact);
	resetUserParam('pin', contact);
	resetUserParam('condos', contact);
	resetUserParam('group', contact);
	resetUserParam('account', contact);
	resetUserParam('tsso', contact);
	setUserParam('status', 'reset', contact);
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);	
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()	
}

global.createTask = function(task, cb) {
	firebase.database().ref("/accounts/"+account+"/tasks").push(task).then(function(res) {
		task.id = res.key;
		if (cb) { cb(task); }
	}).catch(function(error){
		console.log (error)
		console.log (task);
		if (cb) { cb(null); }
	});
}

global.isShopOpen = function() {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (t>=config.officeOpen && t<config.officeClose) {
		return true;
	} else {
		return false;		
	}	
}
