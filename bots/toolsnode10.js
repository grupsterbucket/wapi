var request = require('request');
let envConfig = require('./env.json');
let env = envConfig[envConfig.env];
var moment = require('moment');  

module.exports = {
	twoDigits: function(n) {
		if (n<10) {
			return "0"+n;
		} else {
			return ""+n;	
		}
	},

	removeTildes: function(str) {
		str = str.replace(/á/g,"a");
		str = str.replace(/é/g,"e");
		str = str.replace(/í/g,"i");
		str = str.replace(/ó/g,"o");
		str = str.replace(/ú/g,"u");
		str = str.replace(/ü/g,"u");
		str = str.replace(/ñ/g,"n");
		return str;
	},
	
	numberFormat: function(num) {
		
		if (num.length==9 && num.substring(0,1) == "9") {
			return {to: "593"+num, valid: true}
		}	
		if (num.length==10 && num.substring(0,2) == "09") {
			return {to: "593"+num.substring(1), valid: true}
		}
		if (num.length==11 && num.substring(0,2) == "51") { //Perú
			return {to: num, valid: true}
		}
		if (num.length==12 && num.substring(0,4) == "5939") {
			return {to: num, valid: true}
		}
		if (num.length==13 && num.substring(0,5) == "+5939") {
			return {to: num.substring(1), valid: true}
		}
		return {to: num, valid: false}
	},
	
	hash: function (str) {
		  var hash = 5381,
			  i    = str.length;

		  while(i) {
			hash = (hash * 33) ^ str.charCodeAt(--i);
		  }

		  return hash >>> 0;
	},
	
	isCedula: function(str) {
		if (str) {
			str = str.replace(/-/g,"");
			if (str.length==10) {
				var reg = /^\d+$/;
				if ( reg.test(str) ) { //check is only digits
					return true;
				} else {
					return false;	
				}
			} else {
				return false;	
			} 	
		} else {
			return false;	
		}
	},

	isRUC: function(str) {
		if (str) {
			str = str.replace(/-/g,"");
			if (str.length==13) {
				var reg = /^\d+$/;
				if ( reg.test(str) ) { //check is only digits
					return true;
				} else {
					return false;	
				}
			} else {
				return false;	
			} 	
		} else {
			return false;	
		}
	},
	
	isEmail: function(email) {
		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	},
		
	getRndMS: function (base) {
		if (!base) { base = 3500 }
		var n = Math.floor(Math.random() * 100) + base;
		return n;	
	},
	
	datoFastHook: function(cedula, cb) {
		var str = "";
		//do a datofast query for persona
		var url = "https://whatabotapi:dfst52112*5@datofast.com/api/personas/"+cedula;
		request(url, function (error, resp, body) {
			//console.log (body)
			if (body.indexOf("<!DOCTYPE")==0) {
				str = "Error al buscar registro de "+cedula;
			} else {	
				var ob = JSON.parse(body);
				if (ob.data) {
					var buffer = "";
					for (var p in ob.data) {
						if (ob.data.hasOwnProperty(p)) {
							buffer += "*"+p+":* "+ob.data[p]+"\n"
						}
					}
					str = buffer;
				} else {
					str = ob.error.message;
				}
			}
			if (cb) {
				cb (str, ob.data);
			}
		});	
	},
	
	datoFastHookEmpresa: function(ruc, cb) {
		var str = "";
		//do a datofast query for persona
		var url = "https://whatabotapi:dfst52112*5@datofast.com/api/empresas/"+ruc;
		request(url, function (error, resp, body) {
			if (body.indexOf("<!DOCTYPE")==0) {
				str = "Error al buscar registro de "+cedula;
			} else {	
				var ob = JSON.parse(body);
				if (ob.data) {
					var buffer = "";
					for (var p in ob.data) {
						if (ob.data.hasOwnProperty(p)) {
							buffer += "*"+p+":* "+ob.data[p]+"\n"
						}
					}
					str = buffer;
				} else {
					str = ob.error.message;
				}
			}
			if (cb) {
				cb (str, ob.data);
			}
		});	
	},
	
	datoFastHookVehiculo: function(placa, cb) {
		var str = "";
		//do a datofast query for persona
		var url = "https://whatabotapi:dfst52112*5@datofast.com/api/vehiculos/"+placa;
		request(url, function (error, resp, body) {
			if (body.indexOf("<!DOCTYPE")==0) {
				str = "Error al buscar registro de "+cedula;
			} else {	
				var ob = JSON.parse(body);
				if (ob.data) {
					var buffer = "";
					for (var p in ob.data) {
						if (ob.data.hasOwnProperty(p)) {
							buffer += "*"+p+":* "+ob.data[p]+"\n"
						}
					}
					str = buffer;
				} else {
					str = ob.error.message;
				}
			}
			if (cb) {
				cb (str, ob.data);
			}
		});	
	},

	generatePin: function(){
		return Math.random().toString().substr(2,5);
	},

	lettersOnly: function(text){
		var regex = new RegExp(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g);
        if (regex.test(text)) {
           return true;
        } else {
            return false;
        }
	},

	countSpaces: function(text){
		var spaceCount = (text.split(" ").length - 1);
		return spaceCount;
	},

	getBase64FromUrlFile: function(url){
		return new Promise(resolve => {
			request.get({url : url, encoding: null}, (err, res, body) => {
				if (!err) {
					const type = res.headers["content-type"];
					const prefix = "data:" + type + ";base64,";
					const base64 = body.toString('base64');
					const dataUri = prefix + base64;
					resolve(dataUri);
				}
			});
		});
	},

	prepareDataForUanataca: async function(user){
		
		let dniPhoto1 = await this.tools.getBase64FromUrlFile(user['x-dniPhoto1']);
		let dniPhoto2 = await this.tools.getBase64FromUrlFile(user['x-dniPhoto2']);
		let dniPhoto3 = await this.tools.getBase64FromUrlFile(user['x-dniPhoto3']);
		let dateTransformation = this.tools.changeDateToUanatacaFormat(user.persona.fecha_nacimiento);
		let duration = "1 año";
		if(user['x-prod'] == "f1-2" || user['x-prod'] == "f2-2" || user['x-prod'] == "f3-2") duration = "2 años";

		if(user['x-prod'] == "f1" || user['x-prod'] == "f1-2"){
			return {
				"apikey": env.uanatacaApiKey,
				"uid": env.uanatacaUID,
				"tipo_solicitud": "1",
				"contenedor": "0",
				"nombres": user.persona.nombre1 + " " + user.persona.nombre2,
				"apellido1": user.persona.apellido1,
				"apellido2": user.persona.apellido2,
				"tipodocumento": "CEDULA",
				"numerodocumento": user['x-dni'],
				"sexo": user.persona.genero == "M" ? "HOMBRE" : "MUJER",
				"fecha_nacimiento": dateTransformation,
				"nacionalidad": user.persona.nacionalidad,
				"telfCelular": user.id.replace('593', '0'),
				"eMail": user['x-email'],
				"provincia": "PICHINCHA",
				"ciudad": "QUITO",
				"direccion": user['x-dir'],
				"vigenciafirma": duration,
				"f_cedulaFront": dniPhoto2,
				"f_cedulaBack": dniPhoto3,
				"f_selfie": dniPhoto1
			  }
		}

		if(user['x-prod'] == "f2" || user['x-prod'] == "f2-2"){
			let rucDoc = await this.tools.getBase64FromUrlFile(user['x-rucDoc']);
			return {
				"apikey": env.uanatacaApiKey,
				"uid": env.uanatacaUID,
				"tipo_solicitud": "1",
				"contenedor": "0",
				"nombres": user.persona.nombre1 + " " + user.persona.nombre2,
				"apellido1": user.persona.apellido1,
				"apellido2": user.persona.apellido2,
				"tipodocumento": "CEDULA",
				"numerodocumento": user['x-dni'],
				"ruc_personal": user['x-ruc'],
				"sexo": user.persona.genero == "M" ? "HOMBRE" : "MUJER",
				"fecha_nacimiento": dateTransformation,
				"nacionalidad": user.persona.nacionalidad,
				"telfCelular": user.id.replace('593', '0'),
				"eMail": user['x-email'],
				"provincia": "PICHINCHA",
				"ciudad": "QUITO",
				"direccion": user['x-dir'],
				"vigenciafirma": duration,
				"f_cedulaFront": dniPhoto2,
				"f_cedulaBack": dniPhoto3,
				"f_selfie": dniPhoto1,
				"f_copiaruc": rucDoc
			  }
		}

		if(user['x-prod'] == "f3" || user['x-prod'] == "f3-2"){
			let rucDoc = await this.tools.getBase64FromUrlFile(user['x-rucDoc']);
			let nombramiento = await this.tools.getBase64FromUrlFile(user['x-nombramiento']);
			let constitucion = await this.tools.getBase64FromUrlFile(user['x-constitucion']);
			return {
				"apikey": env.uanatacaApiKey,
				"uid": env.uanatacaUID,
				"tipo_solicitud": "2",
				"nombres": user.persona.nombre1 + " " + user.persona.nombre2,
				"apellido1": user.persona.apellido1,
				"apellido2": user.persona.apellido2,
				"tipodocumento": "CEDULA",
				"numerodocumento": user['x-dni'],
				"sexo": user.persona.genero == "M" ? "HOMBRE" : "MUJER",
				"fecha_nacimiento": dateTransformation,
				"nacionalidad": user.persona.nacionalidad,
				"telfCelular": user.id.replace('593', '0'),
				"eMail": user['x-email'],
				"empresa": user['x-empresa'],
				"ruc_empresa": user['x-ruc'],
				"cargo": user['x-cargo'],
				"provincia": "PICHINCHA",
				"ciudad": "QUITO",
				"direccion": user['x-dir'],
				"vigenciafirma": duration,
				"f_cedulaFront": dniPhoto2,
				"f_cedulaBack": dniPhoto3,
				"f_selfie": dniPhoto1,
				"f_copiaruc": rucDoc,
				"f_nombramiento": nombramiento,
				"f_constitucion": constitucion,
			  }
		}
		  
	},

	changeDateToUanatacaFormat: function(date){
		return moment(date, "DD/MM/YYYY").format('YYYY/MM/DD')
	}

}
