var fetch = require('node-fetch');
var fs = require('fs');
var path = require('path');

var url = "http://132.145.174.7/api/v1/document/134506/signed/1e757b32-0c19-4341-8944-ae446d0a28ec"
fetch(url, {
	method: 'get',
	headers: { }
}).then(function(response) {
	if (response.status !== 200 ) {
		console.log(new Date(),'Get PDF error',response.status);
		
	} else {
		response.buffer().then(function(buffer) {
			console.log (buffer)
			//save locally 
			fs.writeFile('./'+Date.now()+".pdf", buffer, {}, (err, res) => {
				if(err){
					console.error(err)
					return
				}
				console.log('PDF saved')
				process.exit(0);
			})
		});
	}
}).catch(function(err) {
	console.log(new Date(),'Get PDF request error',err.message);
	console.log (err)
});	
