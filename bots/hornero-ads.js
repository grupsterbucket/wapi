// ========
var ads = {
		'congelados': './bots/assets/ads/hornero/congelados.txt',
		'promo': './bots/assets/ads/hornero/promo.txt',
		'martes': './bots/assets/ads/hornero/martes.txt',
		'nino2020': './bots/assets/ads/hornero/nino2020.txt'
		
	}
	
module.exports = {
	getAd: function(code) {
		if ( ads[code] ) {
			return ads[code];
		} else {
			return '';
		}
	}		
}
