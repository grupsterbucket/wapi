// Bot as module
var qs = require('querystring');
module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing up sub bot", step, group)
		switch (step) {
			case "u0":
				setUserParam("step","u1", job.contact, user.line);
				pushText (mainMenuUP(true), job.contact, user.line);
				break;
				//end step 0
			case "u1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","compra","adquiere"].indexOf(kw)>=0) {
					updateUser({step:"idle", status: "active", group: "com", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "com");
					pushText (newTicketMessage(), job.contact, user.line);
				} else if (["2","2.","urbapark","pass"].indexOf(kw)>=0) {
					var msg = "Urbapark Pass es una tarjeta de movilidad inteligente que te permite utilizar dentro de toda la red de estacionamientos Urbapark en el país, sin tomar ticket, ni pasar por caja.\n\n";
					msg += "Para obtener tu tarjeta UrbaPark Pass, es necesario crear una cuenta en el siguiente enlace: https://www.urbaparkpass.com/urbaweb/registro\n\n";
					msg += asistenciaMenuUP()
					setUserParam("step","u1.1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres","puntos"].indexOf(kw)>=0) {
					var msg = newTicketMessage();
					updateUser({step:"idle", status: "active", group: "pnt", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "pnt");
					pushText (msg, job.contact, user.line);
				} else if (["4","4.","cuatro","otro"].indexOf(kw)>=0) {
					var msg = "Por favor, selecciona del 1 al 3 entre las siguientes opciones:\n\n"
					msg += "1. Mensualidades\n";
					msg += "2. Valet Parking\n";
					msg += "3. Comunícate con un asesor\n";
					msg += "4. Regresar al menú principal\n";
					setUserParam("step","u1.3", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["5","5.","cinco"].indexOf(kw)>=0) {
					var msg = newTicketMessage();
					updateUser({step:"idle", status: "active", group: "exp", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "exp");
					pushText (msg, job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;

			case "u1.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","compra","adquiere"].indexOf(kw)>=0) {
					updateUser({step:"idle", status: "active", group: "com", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "com");
					pushText (newTicketMessage(), job.contact, user.line);
				} else if (["2","2.","dos","entrega"].indexOf(kw)>=0) {
					updateUser({step:"idle", status: "active", group: "ent", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "ent");
					pushText (newTicketMessage(), job.contact, user.line);
				} else if (["3","3.","tres","contraseña","recupera","clave","password"].indexOf(kw)>=0) {
					var msg = "Para recuperar tu contraseña haz clic en el siguiente enlace de recuperación: https://www.urbaparkpass.com/urbaweb/resetPwd\n\n";
					msg += "Las instrucciones llegaran a tu correo electrónico, no olvides revisar en correos no deseados o spam.\n\n"
					msg += backOrHelp();
					updateUser({step:"boh", group: "usr"}, job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["4","4.","cuatro","usuario"].indexOf(kw)>=0) {
					var msg = "Tu usuario es el correo electrónico que ingresaste al momento de generar tu cuenta. Si no recuerdas tu correo, selecciona la opción 1 y uno de nuestros especialistas te asistirá\n\n";
					msg += backOrHelp();
					updateUser({step:"boh", group: "usr"}, job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["5","5.","cinco","movimientos"].indexOf(kw)>=0) {
					var msg = "Para revisar tus movimientos debes ingresar a https://www.urbaparkpass.com/urbaweb/login con tu usuario y contraseña.\n\n";
					msg += "Una vez dentro haz clic en el botón *Tarjeta UrbaPark Pass > Transacciones*\n\n";
					msg += backOrHelp();
					updateUser({step:"boh", group: "usr"}, job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["6","6.","seis","tarjeta"].indexOf(kw)>=0) {
					var msg = "Si tu tarjeta de crédito se encuentra inactiva o caducada no podrás usar el servicio. Para solucionarlo debes ingresar a tu perfil de usuario y actualizar los datos de tu tarjeta en el siguiente enlace: https://www.urbaparkpass.com/urbaweb/login\n";
					msg += "O puedes seleccionar entre las siguientes opciones:\n\n"
					msg += backOrHelp();
					updateUser({step:"boh", group: "tc"}, job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["7","7.","siete"].indexOf(kw)>=0) {
					updateUser({step:"idle", status: "active", group: "con", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "con");
					pushText (newTicketMessage(), job.contact, user.line);
				} else if (["8","8.","ocho","menu"].indexOf(kw)>=0) {
					setUserParam("step","u1", job.contact, user.line);
					pushText (mainMenuUP(false), job.contact, user.line);
				} else if (["9","9.","nueve",].indexOf(kw)>=0) {
					updateUser({step:"idle", status: "active", group: "sac", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "sac");
					pushText (newTicketMessage(), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
		    case "boh":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					setUserParam("step","u1.1", job.contact, user.line);
					pushText (asistenciaMenuUP(), job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					updateUser({step:"idle", status: "active", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, user.group);
					pushText (newTicketMessage(), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			 
			case "u1.3":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","mensual"].indexOf(kw)>=0) {
					var msg = newTicketMessage();
					updateUser({step:"idle", status: "active", group: "men", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "men");
					pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos","valet"].indexOf(kw)>=0) {
					var msg = newTicketMessage();
					updateUser({step:"idle", status: "active", group: "val", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "val");
					pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = newTicketMessage();
					updateUser({step:"idle", status: "active", group: "sac", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "sac");
					pushText (msg, job.contact, user.line);
				} else if (["4","4.","cuatro","menu"].indexOf(kw)>=0) {
					setUserParam("step","u1", job.contact, user.line);
					pushText (mainMenuUP(false), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
		}//end switch step
		
		function mainMenuUP(greet) {
			if (greet) {
				var msg = "Bienvenido a nuestro canal de Gestión al Cliente. Nuestro horario de atención es, de lunes a viernes de 8:00 a 18:00.\n\n";
				msg += "Para ayudarte, selecciona una de las siguientes opciones:\n\n";
			} else {
				var msg = "Por favor selecciona una de las siguientes opciones:\n\n";
			}
			msg += "1. Adquiere tu tarjeta\n";
			msg += "2. Asistencia con tu tarjeta UrbaPark Pass\n";
			msg += "3. Conoce nuestra red: Puntos, Tarifas y Horarios\n";
			msg += "4. Otros servicios\n";
			msg += "5. Cuéntanos tu experiencia";
			return msg;
		}
		
		function asistenciaMenuUP() {
			var msg = "Por favor selecciona una de las siguientes opciones:\n\n";
			msg += "1. Adquiere tu tarjeta\n";
			msg += "2. Coordina la entrega de tu tarjeta\n";
			msg += "3. Recupera tu contraseña\n";
			msg += "4. Recupera tu nombre de usuario\n";
			msg += "5. Revisa tus movimientos\n";
			msg += "6. Problemas con tu tarjeta de crédito\n";
			msg += "7. Valida tus facturas de consumos\n";
			msg += "8. Regresar al menú principal\n";
			msg += "9. Comunícate con un asesor\n";
			return msg;
		}
		
		function backOrHelp() {
			var msg = "";
			msg += "1. Regresar al menú anterior\n";
			msg += "2. Comunícate con un asesor\n";
			return msg;
		}
	}
}
