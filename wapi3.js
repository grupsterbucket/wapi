var tools = require('./tools');
var aws = require('./aws');
var http = require("http");
var https = require("https");
var fs = require('fs');
var qs = require('querystring');
var _ = require('underscore');
var request = require('request').defaults({ encoding: null });
var HttpCors = require('http-cors');
var dataUriToBuffer = require('data-uri-to-buffer');
var cors = new HttpCors();
var mtq = {};
var mtqLoaded = false;
var clientQLoaded = {}
var booted = false;
var ssl = true;
var tokens = []
var assetsCache = {}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs
//global scope to access from modules
global.lines = [];
global.firebase = require('firebase');

//read port from command line
var port = 9302 //default
if (process.argv[2]) {
	var portNumber = parseInt(process.argv[2]);
	if (portNumber >= 9000 && portNumber <= 9999) { port = portNumber; }
}

//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootWAPI();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function bootWAPI() {
  if (!booted) {
    booted = true;
    //load lines
    initAWS( function() {
		loadLines(function () {
			  //start API
			  if (!ssl) {
				http.createServer(app).listen(port);
				console.log("WAPI V3 HTTP Server started on port", port)
			  } else {
				var httpsOptions = {
					cert: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/fullchain.pem'),
					ca: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/chain.pem'),
					key: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/privkey.pem'),
					requestCert: false,
					rejectUnauthorized: false
				};

				https.createServer(httpsOptions, app).listen(port);
				console.log("WAPI HTTPS Server started on port", port)
			 }
		});
	});
  }
}

function initAWS(cb) {
	firebase.database().ref('/accounts/demo/config/creds/aws').once('value', function(creds) {
		if (creds.val()) {
			aws.init(creds.val().accessKeyId, creds.val().secretAccessKey);
			cb();
		} else {
			console.log ("AWS credentials not found");	
		}
	});	
}

function loadLines(cb) {
	firebase.database().ref('/lines/').once('value', function(snapshot) {
		var arr = _.map (snapshot.val(), function(value, key) {
			value.id = key;
			return value;});
		for (var i=0; i<arr.length; i++) {
			if (arr[i].isWAPI) {
				lines.push( arr[i] );
			}
		}
		console.log (lines.length + " line(s) loaded");
		//initializq mtq arrays
		for (var i=0; i<lines.length; i++) {
			mtq[lines[i].id] = [];
			tokens.push(lines[i].apiToken);
			//load MTs queue from database if not loaded already
			if (clientQLoaded[lines[i].account]) {
				console.log ("mtq already loaded for "+lines[i].account) 	
			} else {
				console.log ("loading mtq for "+lines[i].account) 	
				clientQLoaded[lines[i].account] = true;
				loadMTQ(lines[i].account);
			}
		}
		if (cb) { cb(); }
	});
}


function loadMTQ(client) {
	firebase.database().ref("/accounts/"+client+"/mtq/").orderByChild("st").equalTo("qed").on('child_added', function(res) {
		var msg = res.val();
		msg.id = res.key;
		if (mtq[msg.line]) {			
			var door = Date.now() - (3*60*60*1000); //only log if recent as last 3 hours
			if (msg.ts>door) {					
					mtq[msg.line].push(msg);
					console.log (new Date(), "NEW MT FOR", client, msg.line, mtq[msg.line].length);					
			} else {
				console.log (new Date(), "Purge", msg.id)	
				purgeMT(msg);
			}
		} else {
		    console.log (new Date(), "Line NOT VALID", msg);
		}
	});
}

function pushMT(post) {
	
	var type = 'txt';
	if ( post.type ) { type = post.type }
	var rep = 'wapi';
	if ( post.rep ) { rep = post.rep }
	
	var line = post.line;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
		var msgId = Date.now()+"-"+line+"-"+post.to;
		var mt = { g:0, m:post.body, to: post.to, rep: rep, t:"mt", ts: Date.now(), type: type, st: 'qed', lu: Date.now()};
		if (post.desc) { mt.desc = post.desc; }
		if (post.url) { mt.url = post.url; }
		if (post.cid) {mt.cid = post.cid; }
		firebase.database().ref("/accounts/"+lineOb.account+"/conves/"+post.to+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = post.to;
			ob.account = lineOb.account;
			//push to account MT queue
			firebase.database().ref("/accounts/"+lineOb.account+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT POSTED OK TO MTQ");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Save MT to chat Error", error);
		});	
		
	} else {
		console.log (new Date(), "Error pushing line not found", line);
	}
}

function mapForTunnel(mt, cb) {
	//get URI for media MTs
	if ( (mt.type=="img" || mt.type=="vid") && mt.url ) {
		getURI(mt.url,function(res) {			
			if (res.st == "ok") {
				mt.thumb = res.uri	
			} else {
				mt.type = "doc";
				mt.m += "\n\n"+mt.url;
			}
			cb(mt)
		});
	} else { 
		if (mt.type=="doc" && mt.url) { mt.m += "\n\n"+mt.url;}  
		cb(mt);
	}	
}
function getURI(url,cb) {
	request(url, function(err, res, body) {			
		if (!err && res.statusCode == 200) {
			var data_uri_prefix = "data:" + res.headers["content-type"] + ";base64,";
			var image = new Buffer.from(body).toString('base64');
			var data_uri = data_uri_prefix + image;
			cb({st: "ok", uri: data_uri});	
		} else {
			cb({st: "err"});
		}
	})	
}

function processMO( post ) {
	var line = post.me.user;
	var msg = post.msg.msg;
	var sender = msg.senderObj;
	var chat = msg.chat;
	var lineOb = _.findWhere( lines, {id: line} );
	if (chat.id.user) {
		var contact = chat.id.user;
	} else {
		var contact = sender.id.user;
	}
		
	if (lineOb) {
	
		//build mo from type
		switch (msg.type) {
			case 'chat':
				var mo = { g:3, m: msg.body, rep: 'wapi', t:"mo", ts: Date.now(), type: 'txt', cid: contact};
				break;
			case 'image':
				var strBody = "[imagen]";
				if (msg.caption) { strBody = msg.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'img', url: post.msg.fn, cid: contact };
				break;
			case 'document':
				var strBody = "[document]";
				if (msg.caption) { strBody = msg.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'doc', url: post.msg.fn, cid: contact};
				break;
			case 'location':
				var mo = { g:3, m:'[location]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'loc', lng: msg.lng, lat: msg.lat, cid: contact };
				break;
			case 'vcard':
				var mo = { g:3, m: msg.body, rep: 'wapi', t:"mo", ts: Date.now(), type: 'vcd', cid: contact };
				break;
			case 'video':
				var strBody = "[video]";
				if (msg.caption) { strBody = msg.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'vid', url: post.msg.fn, cid: contact };
				break;
			case 'audio':
				var mo = { g:3, m: '[audio]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'aud', url: post.msg.fn ,cid: contact};
				break;
			case 'ptt':
				var mo = { g:3, m: '[audio]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'aud', url: post.msg.fn ,cid: contact};
				break;
			default:
				var mo = { g:3, m: msg.type, rep: 'wapi', t:"mo", ts: Date.now(), type: msg.type };
		}
		var name = sender.id.user;
		if ( sender.pushname ) { name = sender.pushname; }
		if ( sender.name ) { name = sender.name; }
		var user = { chn: 'wsp', id: contact, line: line, nick: name, lu: Date.now()}

		
		if ( contact.indexOf ("-")>0 ) { //is group
			mo.contact = sender.id.user;
			mo.nick = sender.id.user;
			if (sender.pushname) {
				mo.nick = sender.pushname;
			}
			if (chat.formattedTitle) {
				user.nick = chat.formattedTitle;
			}
		}

		//create or update user
		firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+contact).update(user).then(function() {
			firebase.database().ref("/accounts/"+lineOb.account+"/conves/"+contact+"/msgs/"+msg.id.id).set(mo).then(function() {
			   //run bot
			   if (lineOb.hasBot) {
				   var job = {
					   msg: msg,
					   contact: contact,
					   st: 'new',
					   ts: Date.now()
					   }
				   firebase.database().ref("/accounts/"+lineOb.account+"/botJobs").push(job);
			   } else {
				   //start ticket
				   firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+contact).once("value", function(u) {
					   var usr = u.val();
					   if (usr.tso) {
						   //ticket already started
					   } else {
						   firebase.database().ref("/accounts/"+lineOb.account+"/chats/"+contact).update({tso: Date.now(), status: "active"});
						   console.log (new Date(), "Ticket started");
						   logStartTicket(lineOb.account, contact, 'wsp')
					   }
                   });
			   }
			}).catch(function(error) {
			   console.log (new Date(), "Error posting MO to Firebase", line);
			   console.log (error)
			   console.log (mo)
			   //TODO: persist this msg in local file to resend MOs
			});
		}).catch(function(error) {
			console.log (new Date(), "Error posting User to Firebase", line);
			console.log (error)
			console.log (sender)
			console.log (user)
			//TODO: persist this msg in local file to resend MOs
		});
	} else {
		console.log (new Date(), "Error processing MO line not found", line);
	}

}

function logStartTicket(account, contact, chn) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

function runOp(post) {
	var line = post.line;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
		switch (post.op) {
			case "reset-bot":
				console.log (new Date(), "Run OP OK", post.op, post.contact);
				var job = {
				   type: post.op,
				   contact: post.contact,
				   st: 'new',
				   ts: Date.now()
				   }
			    firebase.database().ref("/accounts/"+lineOb.account+"/botJobs/").push(job);
				break;
			case "finish-order":
				console.log (new Date(), "Run OP OK", post.op, post.contact);
				var job = {
				   type: post.op,
				   contact: post.contact,
				   st: 'new',
				   ts: Date.now()
				   }
			    firebase.database().ref("/accounts/"+lineOb.account+"/botJobs/").push(job);
				break;
			default:
				console.log (new Date(), "Unknown OP", post.op, post.contact);
		}
	} else {
		console.log (new Date(), "Error running op line not found", line);
	}
}

function pushWatcher (line, watch ) {
	firebase.database().ref("/lines/"+line+"/watch").set( watch  );
}

function getTest (account, ts, cb) {
	firebase.database().ref("/accounts/"+account+"/tests").orderByChild("ts").equalTo(Number(ts)).once('value', function(res) {
		var arr = []
		if (res.val()) {
			arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});	
		}
		cb(arr)
	});
}

function processACK(post) {
	var lineOb = _.findWhere( lines, {id: post.me.user} );
	
	if (lineOb) {
		//update ACK and LU of MT
		if (post.msg.muid) {
			var parts = post.msg.muid.split("-");
			firebase.database().ref("/accounts/"+lineOb.account+"/conves/"+parts[2]+"/msgs/"+post.msg.muid).update({g: post.msg.ack, lu: Date.now()});
			console.log (new Date(), "ACK updated for |"+post.msg.muid+"|");
		}
	} else {
		console.log (new Date(), "Error process ACK line not found", post.me.user);
	}
	
}

function ackMTSent(msgId, line) {
	var lineOb = _.findWhere( lines, {id: line} );
	firebase.database().ref("/accounts/"+lineOb.account+"/mtq/"+msgId).update({ line: line, lu: Date.now(), st: 'snt' }).then(function() {
		var parts = msgId.split("-");
		if (parts.length==4) {
			//group	
			firebase.database().ref("/accounts/"+lineOb.account+"/conves/"+parts[2]+"-"+parts[3]+"/msgs/"+msgId).update({g:1, st: 'tun', lu: Date.now()});
		} else {
			firebase.database().ref("/accounts/"+lineOb.account+"/conves/"+parts[2]+"/msgs/"+msgId).update({g:1, st: 'tun', lu: Date.now()});	
		}
		
		console.log ("MT Sent to Device, graded 1", lineOb.account, msgId);
	}).catch(function(error) {
		console.log(new Date(), "Error setting ACK MT SENT", error);
	});
}

function purgeMT(msg) {
	var lineOb = _.findWhere( lines, {id: msg.line} );
	if (lineOb) {
			//set mtq status to purged
			firebase.database().ref("/accounts/"+lineOb.account+"/mtq/"+msg.id).update({ lu: Date.now(), st: 'pur' })
	} else {
		console.log (new Date(), "Error purgeMT line not found", msg.line);
	}
}

function saveMedia(post) {
	if (post.client) {
		var parts = post.blob.split(";base64,");
		var contentType = parts[0].replace("data:","");
		aws.putObject("v3/" + post.client + "/" +post.fn, dataUriToBuffer(post.blob), contentType, function(res){
			if (res.status=="ok") {
				console.log (new Date(), "Media saved un AWS for", post.client);	
			} else {
				//save locally
				fs.writeFileSync('./assets/media/v3/'+post.client+'_'+post.fn, post.blob, 'utf8');
				console.log (new Date(), "Local media saved for", post.client);	
			}	
		})
	}
}

function processPost(req, res, callback) {
	var queryData = "";
	if(typeof callback !== 'function') return null;

	req.on('data', function(data) {
		queryData += data;
		if(queryData.length > 10000000) {
			queryData = "";
			res.writeHead(413, {'Content-Type': 'text/plain'}).end();
			req.connection.destroy();
		}
	});

	req.on('end', function() {
		if (req.method == 'GET') {
			req.post = qs.parse(req.url.replace(/^.*\?/, ''));
		} else {
			req.post = queryData;
		}
		callback();
	});
}


var app = function(req, res) {

     if(cors.apply(req, res)) {
		 res.end();
		 return;
	 }

	 processPost(req, res, function() {
		//console.log ("URL", req.url);
		var parts = req.url.split("/");
		//router
        switch (parts[1]) {
			case 'send':
                //process MT received from API call
				   console.log (new Date(), "MT received");
				   if (req.method == "POST") {
						if (typeof req.post === "string") {
							try {
								var post = _.clone ( JSON.parse(req.post)	);
							} catch {
								var post = _.clone ( qs.parse(req.post)	);
							}
						} else {
							var post = _.clone ( req.post );
						}
						if (tokens.indexOf(post.apiToken)>=0 ) {
							var numberCheck = tools.numberFormat(post.to, post.isResponse)
							if (numberCheck.valid) {
								post.to = numberCheck.to;
								if (post.body) {
									if (post.body.length > 0) {
										pushMT (post)
										res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
										res.write("mt-pushed-ok");
										res.end();
										return;
									} else {
										res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
										res.write("invalid-message");
										res.end();
										return;
									}
								} else {
									res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
									res.write("invalid-message");
									res.end();
									return;
								}
							} else {
								res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
								res.write("invalid-number");
								res.end();
								return;
							}
						} else {
							res.writeHead(401, "ERROR", {'Content-Type': 'text/plain'});
							res.write("invalid-token");
							res.end();
							return;
						}

				   } else {
					   res.writeHead(405, {'Content-Type': 'text/plain'});
					   res.write("Invalid request method");
					   res.end();
					   return;
				   }

                break;
            case 'run-op':
                
				console.log (new Date(), "OP received");
				if (req.method == "POST") {

					if (typeof req.post === "string") {
						var post = _.clone ( qs.parse(req.post)	);
					} else {
						var post = _.clone ( req.post );
					}
					if (tokens.indexOf(post.apiToken)>=0 ) {
						runOp(post);
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write("run-op-ok");
						res.end();
						return;
						
					} else {
						res.writeHead(401, "ERROR", {'Content-Type': 'text/plain'});
						res.write("invalid-token");
						res.end();
						return;
					}

				} else {
				   res.writeHead(405, {'Content-Type': 'text/plain'});
				   res.write("Invalid request method");
				   res.end();
				   return;
				}

                break;
            case 'pushMO':                
			    console.log (new Date(), "MO received");
			    //console.log (req.post);
			    if (req.post) {
					var postObj = JSON.parse(req.post);
					if (postObj.me) {
						processMO ( postObj );
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write("mo-pushed-ok");
						res.end();
						return;
					} else {
						//~ res.writeHead(500, "ERROR", {'Content-Type': 'text/plain'});
						//~ res.end();
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write("mo-pushed-ok");
						res.end();
						return;
					}
				}
                break;
            case 'pullMT':
				var line = parts[2];
				//console.log ("pullMT", line)
				if (line) {
					if ( mtq[line] ) {
						if ( mtq[line].length>0 ) {
							console.log (new Date(), "MT PULL OK LENGTH = " + mtq[line].length, line);
							var mt = mtq[line].shift();
							mapForTunnel(mt, function(mt) {
								ackMTSent(mt.id, line);
								res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
								res.write( JSON.stringify(mt) );
								res.end();
								return;
							});
						} else {
							res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
							res.write( "mtq-empty" );
							res.end();
							return;
						}
					} else {
						console.log (new Date(), "MT PULL INVALID LINE", line);
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write( "invalid-line" );
						res.end();
						return;
					}
				} else {
					console.log (new Date(), "MT PULL INVALID LINE", line);
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write( "invalid-line" );
					res.end();
					return;
				}
			    
                break;
            case 'upload':                
			    console.log (new Date(), "UPLOAD received");
			    //console.log (req.post);
			    if (IsJsonString(req.post)) {
					saveMedia ( JSON.parse(req.post) ); 
				} else {
					console.log (new Date(), "Upload Error")	
				}
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("upload-ok");
			    res.end();
			    return;
                break;
            case 'ack':                
			    console.log (new Date(), "ACK received");
			    //console.log (req.post)
			    if (req.post) {
					var postObj = JSON.parse(req.post);
					processACK ( postObj );
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write("ack-ok");
					res.end();
					return;
				}
                break;
            case 'watcher':                
			    //console.log (new Date(), "Watcher received");
			    //console.log (req.post)
			    if (req.post) {
					var obj = JSON.parse(req.post); //To Do catch error SyntaxError: Unexpected end of JSON input
					if (obj.me) {
						var line = obj.me.user;
						var watch = {
							phoneAuthed: obj.msg.stream.phoneAuthed,
							info: obj.msg.stream.info,
							mode: obj.msg.stream.mode,
							connected: obj.msg.conn.connected,
							battery: obj.msg.conn.battery,
							plugged: obj.msg.conn.plugged,
							ts: Date.now(),
							ql: 0
						}
						if (mtq[line]) { watch.ql = mtq[line].length }
						if (!watch.connected) { watch.connected = false }
						if (!watch.plugged) { watch.plugged = false }
						if (!watch.battery) { watch.battery = false }
						if (obj.phone) { watch.phone = obj.phone }
						//push watcher
						pushWatcher(line, watch );
					}
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write("watch-ok");
					res.end();
					return;
				}
				break;
			    
            case 'err':                
			    console.log (new Date(), "ERR received");
			    console.log (req.post)
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("err-ok");
			    res.end();
			    return;
                break;
            
            case "ping":
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("pong");
				res.end();
				return;
				break;
			
			case "test":
				getTest(parts[2], parts[3], function(arr) {
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write(JSON.stringify(arr));
					res.end();
				});
				return;
				break;
				
            default:
			   console.log(new Date(), "unknown path "+req.url);
			   res.writeHead(404, {'Content-Type': 'text/plain'});
			   res.write("unknown path "+req.url);
			   res.end();
			   return;
        }
    });
}

function IsJsonString(str) {
	if (str.indexOf("[")==0 || str.indexOf("{")==0) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	} else {
		return false;	
	}
}
