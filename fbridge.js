//https://developers.facebook.com/apps/745136353572824/whatsapp-business/wa-settings/?business_id=514393120290055
var fetch = require('node-fetch');
var tools = require('./tools');
var http = require("http");
var https = require("https");
var fs = require('fs');
var qs = require('querystring');
var _ = require('underscore');
var HttpCors = require('http-cors');
var cors = new HttpCors();
var accountOb = {}
var mtq = {}
var purgeQ = []
var booted = false;
var ssl = false;
var apiTokens = []
var fbTokens = {}
var graphURL = 'https://graph.facebook.com/v14.0/' //todo make this an env argument
var tries = {}
var moIDs = []
var mtITV = 10; //Interval to post MT to MBFB
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs
var notRetryCodes = [132012, 131026]
//global scope to access from modules
global.lines = [];
global.firebase = require('firebase');

//read port and account from command line
var port = 9701 //default
var account = "demo" //default
if (process.argv[2]) {
	var portNumber = parseInt(process.argv[2]);
	if (portNumber >= 9000 && portNumber <= 9999) { port = portNumber; }
}

if (process.argv[3]) {
	account = process.argv[3];
}

if (process.argv[4]) {
	if (process.argv[4] == "sslon") {
		ssl = true;
	}
}

var serverDomain = "textcenter.net"
if (process.argv[5]) {
	serverDomain = process.argv[5]
}

//firebase init
var firebaseConfig = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
	"authDomain": "ubiku3.firebaseapp.com",
	"databaseURL": "https://ubiku3.firebaseio.com",
	"projectId": "ubiku3",
	"storageBucket": "ubiku3.appspot.com",
	"messagingSenderId": "946813430025",
	"appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(function (user) {
	if (user) {
		// User is signed in.

		console.log("Logged in to firebase as uid " + user.uid);
		setTimeout(function () {
			boot();
		}, 2000);

	} else {
		// User is signed out login again
		console.log("Session expired");
		loginTofirebase();
	}
	// ...
});

function loginTofirebase() {
	console.log("Login to firebase");
	firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
		var errorCode = error.code;
		var errorMessage = error.message;
		console.log(error.code + ": " + error.message);
	});
}

function boot() {
	if (!booted) {
		booted = true;
		//load lines
		loadAccount(function (ob) {
			if (ob) {
				accountOb = JSON.parse(JSON.stringify(ob));
				loadLines(function () {
					//start API
					if (!ssl) {
						http.createServer(app).listen(port);
						console.log("FBRIDGE HTTP Server started on port", port)
					} else {
						var httpsOptions = {
							cert: fs.readFileSync('/etc/letsencrypt/live/' + serverDomain + '/fullchain.pem'),
							ca: fs.readFileSync('/etc/letsencrypt/live/' + serverDomain + '/chain.pem'),
							key: fs.readFileSync('/etc/letsencrypt/live/' + serverDomain + '/privkey.pem'),
							requestCert: false,
							rejectUnauthorized: false
						};

						https.createServer(httpsOptions, app).listen(port);
						console.log("FBRIDGE HTTPS Server started on port", port)
					}
					//start mt and purge workers
					setTimeout(function () {
						console.log("Starting mt workers")
						for (var i = 0; i < lines.length; i++) {
							mtWorker(lines[i].id);
						}
						console.log("Starting purge worker")
						purgeWorker();
					}, 5000)
				});

			} else {
				console.log("ERROR", account, "account not found")
			}
		});
	}
}

function loadAccount(cb) {
	firebase.database().ref('/accounts/' + account + '/config').once('value', function (snapshot) {
		if (snapshot.val()) {
			cb(JSON.parse(JSON.stringify(snapshot.val())));
		} else {
			cb(null);
		}
	});
}
function loadLines(cb) {
	firebase.database().ref('/lines/').orderByChild("account").equalTo(account).once('value', function (snapshot) {
		var arr = _.map(snapshot.val(), function (value, key) {
			value.id = key;
			return value;
		});
		for (var i = 0; i < arr.length; i++) {
			if (arr[i].isWAPIFB) {
				lines.push(arr[i]);
				apiTokens.push(arr[i].apiToken);
				loadToken(arr[i].id);
				//create assets dir for this line
				fs.mkdir('./fbAssets/media/' + arr[i].id, { recursive: true }, (err) => {
					if (err) throw err;
				});
			}
		}
		console.log(lines.length + " line(s) loaded for " + account);
		//initializq mtq arrays
		for (var i = 0; i < lines.length; i++) {
			mtq[lines[i].id] = [];
		}
		//load mtq
		loadMTQ(account);
		if (cb) { cb(); }
	});
}

function loadToken(line) {
	firebase.database().ref('fbTokens/' + line).once('value', function (tkn) {
		if (tkn.val()) {
			console.log("Token found for line", line)
			fbTokens[line] = tkn.val()
			//getMediaURL( '15550236912' , '533752868475555' , function(b64) { console.log (b64) });
		} else {
			console.log("Token NOT found for line", line)
			fbTokens[line] = ""
		}
	});
}

function loadMTQ(account) {
	firebase.database().ref("/accounts/" + account + "/mtq/").orderByChild("st").equalTo("qed").on('child_added', function (res) {
		var msg = res.val();
		msg.id = res.key;
		if (mtq[msg.line]) {
			var door = Date.now() - accountOb.misc.purgeAge;
			if (msg.ts > door) {
				//check if contact blocked
				if (!accountOb.blockedList) { accountOb.blockedList = "" }
				if (accountOb.blockedList.indexOf(msg.to) >= 0) {
					console.log(new Date(), "Purged blocked contact MT", msg.to);
					purgeQ.push(msg.id);
				} else {
					mtq[msg.line].push(msg);
					console.log(new Date(), "NEW MT FOR", account, msg.line, mtq[msg.line].length);
				}
			} else {
				console.log(new Date(), "Purged for time expiration", msg.id)
				purgeQ.push(msg.id);
			}
		} else {
			console.log(new Date(), "Line NOT VALID will purge", msg);
			purgeQ.push(msg.id);
		}
	});
}

function mtWorker(lineId) {

	if (mtq[lineId]) {
		if (mtq[lineId].length > 0) {
			console.log(new Date(), "Run MT Worker for", lineId, " l =", mtq[lineId].length)
			var mt = mtq[lineId].shift();
			if (mt.m) { console.log("m", mt.m.substring(0, 25)) }
			postToFB(mt, function (res) {
				console.log(res)
				if (res.error) {
					console.log(res.error)
					if (notRetryCodes.indexOf(res.error.code) >= 0) {
						console.log("Do not retry")
						//Update mtq
						firebase.database().ref("/accounts/" + mt.account + "/mtq/" + mt.id).update({ line: lineId, lu: Date.now(), st: 'err', mid: "na", error: res.error }).then(function () {
							console.log("ERR Sent to FB");
						}).catch(function (error) {
							console.log(new Date(), "Error updating MTQ", error);
						});
						setTimeout(function () { mtWorker(lineId) }, mtITV);
					} else {
						if (!tries[mt.id]) { tries[mt.id] = 0; }
						tries[mt.id] += 1;
						if (tries[mt.id] >= 10) {
							//Update mtq
							firebase.database().ref("/accounts/" + mt.account + "/mtq/" + mt.id).update({ line: lineId, lu: Date.now(), st: 'err', mid: "na", error: res.error }).then(function () {
								console.log("ERR Sent to FB");
							}).catch(function (error) {
								console.log(new Date(), "Error updating MTQ", error);
							});
							setTimeout(function () { mtWorker(lineId) }, mtITV);
						} else {
							//requeue wait and call again
							console.log("Retry MT", mt.id, tries[mt.id]);
							mtq[lineId].push(mt);
							setTimeout(function () { mtWorker(lineId) }, 5000)
						}
					}
				} else {
					var mid = res.messages[0].id.replace(/\./g, "_");
					console.log("Sent MT with ID", mid)
					//Update mtq
					firebase.database().ref("/accounts/" + mt.account + "/mtq/" + mt.id).update({ line: lineId, lu: Date.now(), st: 'snt', mid: mid }).then(function () {
						console.log("MT Sent to FB");
					}).catch(function (error) {
						console.log(new Date(), "Error updating MTQ", error);
					});
					//save ACK Ref
					var ack = { account: mt.account, contact: mt.to, msgId: mt.id }
					firebase.database().ref("/ack/" + mid).set(ack, function () {
						console.log(new Date(), "ACK Ref saved");
					}).catch(function (error) {
						console.log(new Date(), "Error saving ACK", error);
					});
					setTimeout(function () { mtWorker(lineId) }, mtITV);
				}
			})
		} else {
			setTimeout(function () { mtWorker(lineId) }, mtITV);
		}
	} else {
		setTimeout(function () { mtWorker(lineId) }, mtITV);
	}
}

function postToFB(mt, cb) {
	//console.log (mt)
	var lineOb = _.findWhere(lines, { id: mt.line });
	if (lineOb) {
		var ob = {
			"messaging_product": "whatsapp",
			"recipient_type": "individual",
			"to": mt.to
		}
		if (mt.isTemplate) {
			var paramOb = []
			if (mt.params.length > 0) {
				var params = mt.params.split("|");

				for (var i = 0; i < params.length; i++) {
					paramOb.push({
						"type": "text",
						"text": params[i]
					})
				}
			}
			ob.type = "template"
			ob.template = {
				"name": mt.templateName,
				"language": {
					"code": mt.lang
				},
				"components": [
					{
						"type": "body",
						"parameters": paramOb
					}
				]
			}
			if (mt.imageHeaderURL != undefined) {
				var header = {
					"type": "header",
					"parameters": [{
						"type": "image",
						"image": { "link": mt.imageHeaderURL }
					}]
				}
				ob.template.components.push(header)
			}
			if (mt.urlButton != undefined) {
				ob.template.components.push(mt.urlButton)
			}
		} else {
			switch (mt.type) {
				case "txt":
					ob.type = "text";
					ob.text = {
						preview_url: false,
						body: mt.m
					}
					break;
				case "img":
					ob.type = "image";
					var caption = mt.url;
					if (mt.m) { caption = mt.m }
					if (mt.desc) { caption += "\n" + mt.desc }
					ob.image = {
						link: mt.url,
						caption: caption
					}
					break;
				case "doc":
					ob.type = "document";
					var caption = mt.url;
					var filename = "Documento"
					if (mt.m) { caption = mt.m }
					if (mt.desc) { filename = mt.desc }
					ob.document = {
						link: mt.url,
						caption: caption,
						filename: filename
					}
					break;
				case "btn":
					var caption = mt.url;
					if (mt.m) { caption = mt.m }
					var buttons = []
					for (var i = 0; i < mt.desc.length; i++) {
						var btn = mt.desc[i]
						var button = {
							"type": "reply",
							"reply": {
								"id": btn.id,
								"title": btn.title
							}
						}
						buttons.push(button)
					}
					ob.type = "interactive";
					ob.interactive = {
						"type": "button",
						"body": {
							"text": caption
						},
						"action": {
							"buttons": buttons
						}
					}
					break;
				case "opt":
					var caption = mt.url;
					if (mt.m) { caption = mt.m }
					var opts = []
					for (var i = 0; i < mt.desc.options.length; i++) {
						var opt = mt.desc.options[i]
						var item = {
							"id": opt.id,
							"title": opt.title,
							"description": opt.desc
						}
						opts.push(item)
					}
					ob.type = "interactive";
					ob.interactive = {
						"type": "list",
						"body": {
							"text": caption
						},
						"action": {
							"button": mt.desc.buttonCaption,
							"sections": [
								{
									"title": mt.desc.title,
									"rows": opts
								}
							]
						}
					}
					break;
			}
		}
		console.log(JSON.stringify(ob))
		fetch(graphURL + lineOb.wabid + '/messages', {
			method: 'post',
			body: JSON.stringify(ob),
			headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + fbTokens[lineOb.id] },
		})
			.then(res => res.text())
			.then(function (body) {
				var res = JSON.parse(body)
				cb(res);
			});
	} else {
		cb({ error: 'Missing line ob' });
	}
}

function getMediaURL(line, mediaId, cb) {
	var lineOb = _.findWhere(lines, { id: line });
	if (lineOb) {
		fetch(graphURL + mediaId, {
			method: 'get',
			headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + fbTokens[lineOb.id] },
		})
			.then(res => res.text())
			.then(function (body) {
				var res = JSON.parse(body)
				console.log(JSON.stringify(res))
				if (res.url) {
					downloadMedia(line, mediaId, res.url, cb);
				} else {
					cb("error");
				}
			});
	} else {
		cb("error")
	}
}

function getTemplates(line, cb) {
	var lineOb = _.findWhere(lines, { id: line });
	if (lineOb) {
		if (lineOb.wabusid) {
			var params = '/message_templates?fields=name,language,status,id,quality_score,components+&limit=20'
			var url = graphURL + lineOb.wabusid + params

			console.log(url)
			fetch(url, {
				method: 'get',
				headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + fbTokens[lineOb.id] },
			})
				.then(res => res.text())
				.then(function (body) {
					var res = JSON.parse(body)
					if (res.data) {
						cb(res.data)
					} else {
						console.log(res)
						cb([])
					}
				});
		} else {
			console.log("Account missing wabusid param")
			cb([])
		}
	} else {
		cb([])
	}
}

function downloadMedia(line, mediaId, url, cb) {
	console.log("Download media", url);
	var lineOb = _.findWhere(lines, { id: line });
	if (lineOb) {
		fetch(url, {
			method: 'get',
			headers: { 'Authorization': 'Bearer ' + fbTokens[lineOb.id] },
		})
			.then(function (res) {
				var fileStream = fs.createWriteStream("./temp/" + mediaId);
				res.body.pipe(fileStream);
				fileStream.on("finish", function (file) {
					console.log("Download completed")
					fs.readFile("./temp/" + mediaId, "base64", function (err, b64) {
						if (err) {
							console.log(err)
							cb("error")
						} else {
							cb(b64);
							//save for cache use
							fs.writeFile('./fbAssets/media/' + line + '/' + mediaId, b64, 'utf8', function (err, data) {
								if (err) throw err;
							});
						}
					});
				});
			});
	} else {
		cb("error")
	}
}

function runOp(post) {
	var line = post.line;
	var lineOb = _.findWhere(lines, { id: line });

	if (lineOb) {
		switch (post.op) {
			case "reset-bot":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.contact,
					st: 'new',
					ts: Date.now(),
					isFB: true,
					line: line
				}
				firebase.database().ref("/accounts/" + lineOb.account + "/botJobs/").push(job);
				break;
			case "finish-order":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.contact,
					st: 'new',
					ts: Date.now(),
					isFB: true,
					line: line
				}
				firebase.database().ref("/accounts/" + lineOb.account + "/botJobs/").push(job);
				break;
			case "os-request-completed":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.ref,
					requestId: post.requestId,
					documentId: post.documentId,
					st: 'new',
					ts: Date.now(),
					isFB: true,
					line: line
				}
				firebase.database().ref("/accounts/" + lineOb.account + "/botJobs/").push(job);
				break;
			case "os-signed-pdf":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.ref,
					pdfURL: post.pdfURL,
					st: 'new',
					ts: Date.now(),
					isFB: true,
					line: line
				}
				firebase.database().ref("/accounts/" + lineOb.account + "/botJobs/").push(job);
				break;
			case "os-error":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.ref,
					errorCode: post.errorCode,
					errorMsg: post.errorMsg,
					st: 'new',
					ts: Date.now(),
					isFB: true,
					line: line
				}
				firebase.database().ref("/accounts/" + lineOb.account + "/botJobs/").push(job);
				break;
			case "payment-success":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.contact,
					st: 'new',
					ts: Date.now(),
					isFB: true,
					line: line
				}
				firebase.database().ref("/accounts/" + lineOb.account + "/botJobs/").push(job);
				break;
			case "payment-error":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				var job = {
					type: post.op,
					contact: post.contact,
					st: 'new',
					ts: Date.now(),
					isFB: true,
					line: line
				}
				firebase.database().ref("/accounts/" + lineOb.account + "/botJobs/").push(job);
				break;
			case "send-template":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				pushTemplate(post.params, post.templateName, post.contact, post.line, post.lang, post.imageHeaderURL, post.urlButton)
				break;
			case "set-flow":
				console.log(new Date(), "Run OP OK", post.op, post.contact);
				setFlow(post)
				break;
			default:
				console.log(new Date(), "Unknown OP", post.op, post.contact);
		}
	} else {
		console.log(new Date(), "Error running op line not found", line);
	}
}

function setFlow(post) {
	firebase.database().ref("/accounts/" + account + "/chats/" + post.contact).update({ sso: post.sso, module: post.module, step: post.step }, function (res) {
		console.log("Contact updated with flow dets")
	})
}

function pushMT(post) {

	var type = 'txt';
	if (post.type) { type = post.type }
	var rep = 'wapi';
	if (post.rep) { rep = post.rep }

	var line = post.line;
	var lineOb = _.findWhere(lines, { id: line });

	if (lineOb) {
		var msgId = Date.now() + "-" + line + "-" + post.to;
		var chn = "wsp"
		if (post.chn) { chn = post.chn }
		var mt = { g: 0, chn: chn, m: post.body, to: post.to, rep: rep, t: "mt", ts: Date.now(), type: type, st: 'qed', lu: Date.now() };
		if (post.desc) { mt.desc = post.desc; }
		if (post.url) { mt.url = post.url; }
		if (post.thumb) { mt.thumb = post.thumb; }
		if (post.cid) { mt.cid = post.cid; }
		if (chn == "wsp" && post.isTemplate) {
			mt.isTemplate = post.isTemplate;
			mt.lang = post.lang;
			mt.params = post.params;
			mt.templateName = post.templateName;
		}
		if (post.imageHeaderURL != undefined) { mt.imageHeaderURL = post.imageHeaderURL }

		firebase.database().ref("/accounts/" + lineOb.account + "/conves/" + post.to + "/msgs/" + msgId).set(mt).then(function () {
			var ob = _.clone(mt);
			ob.line = line;
			ob.to = post.to;
			ob.account = lineOb.account;
			//push to account MT queue
			firebase.database().ref("/accounts/" + lineOb.account + "/mtq/" + msgId).set(ob).then(function () {
				console.log(new Date(), "MT POSTED OK TO MTQ");
			}).catch(function (error) {
				console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function (error) {
			console.log(new Date(), "Save MT to chat Error", error);
		});
	} else {
		console.log(new Date(), "Error pushing line not found", line);
	}
}

function pushTemplate(params, templateName, contact, line, lang, imageHeaderURL, urlButton) {
	var lineOb = _.findWhere(lines, { id: line });

	if (lineOb) {
		var msgId = Date.now() + '-' + line + '-' + contact
		var mt = {
			g: 0,
			m: 'Template ' + templateName,
			rep: 'bot',
			t: 'mt',
			ts: Date.now(),
			type: 'txt',
			st: 'qed',
			lu: Date.now()
		}
		mt.isTemplate = true
		mt.lang = 'es' //todo read from DB
		if (lang) { mt.lang = lang }
		if (params) {
			mt.params = params.join('|')
		} else {
			mt.params = ""
		}
		if (imageHeaderURL) {
			mt.imageHeaderURL = imageHeaderURL
		}
		if (urlButton) {
			mt.urlButton = urlButton
		}
		mt.templateName = templateName
		firebase
			.database()
			.ref('/accounts/' + lineOb.account + '/conves/' + contact + '/msgs/' + msgId)
			.set(mt)
			.then(function () {
				var ob = _.clone(mt)
				ob.line = line
				ob.to = contact
				ob.account = lineOb.account
				//push to account MT queue
				firebase
					.database()
					.ref('/accounts/' + lineOb.account + '/mtq/' + msgId)
					.set(ob)
					.then(function () {
						console.log(new Date(), 'Template MT Queued OK')
					})
					.catch(function (error) {
						console.log(new Date(), 'Push Template MTQ Error', error)
					})
			})
			.catch(function (error) {
				console.log(new Date(), 'Push MT to Conve Error', error)
			})
	} else {
		console.log(new Date(), "Error pushing template line not found", line);
	}
}

function processFBChanges(post) {
	//console.log (JSON.stringify(post))
	var entries = post.entry;
	for (var i = 0; i < entries.length; i++) {
		var changes = entries[i].changes;
		for (var j = 0; j < changes.length; j++) {
			var change = changes[j];
			switch (change.field) {
				case 'messages':
					if (change.value.messages) { //MOs
						var msgs = change.value.messages;
						//console.log (msgs)
						//console.log (change.value.metadata)
						for (var m = 0; m < msgs.length; m++) {
							var msg = msgs[m];
							msg.nick = msg.from;
							var contact = _.findWhere(change.value.contacts, { wa_id: msg.from });
							if (contact) { msg.nick = contact.profile.name };
							msg.line = change.value.metadata.display_phone_number;
							msg.chn = change.value.messaging_product
							processMO(msg)
						}
					}
					if (change.value.statuses) { //ACKs
						var sts = change.value.statuses;
						for (var m = 0; m < sts.length; m++) {
							processACK(sts[m])
						}
					}
					break;
				default:
					console.log("Unprocessed change")
					console.log(change.field)
					console.log(change.value)
			}
		}
	}
}

function isMODuped(moID) {
	if (moIDs.indexOf(moID) >= 0) {
		return true;
	} else {
		moIDs.push(moID);
		//limit size
		if (moIDs.length >= 100) { moIDs.shift() }
		//console.log (moIDs)
		return false;
	}
}

function processMO(msg) {
	if (!isMODuped(msg.id.replace(/\./g, "_"))) {
		console.log(new Date(), "Processing MO", msg.from, msg.type, msg.line, msg.id.replace(/\./g, "_"));
		//is conact blocked?
		if (!accountOb.blockedList) { accountOb.blockedList = "" }
		if (accountOb.blockedList.indexOf(msg.from) < 0) {
			var line = msg.line;
			var lineOb = _.findWhere(lines, { id: line });
			if (lineOb) {

				//build mo from type
				switch (msg.type) {
					case 'text':
						var mo = { g: 3, m: msg.text.body, rep: 'wapi', t: "mo", ts: Date.now(), type: 'txt' };
						break;
					case 'image':
						var caption = "";
						if (msg.image.caption) { caption = msg.image.caption }
						var mo = { g: 3, m: caption, rep: 'wapi', t: "mo", ts: Date.now(), type: 'img', image: msg.image };
						break;
					case 'video':
						var caption = "";
						if (msg.video.caption) { caption = msg.video.caption }
						var mo = { g: 3, m: "", rep: 'wapi', t: "mo", ts: Date.now(), type: 'vid', video: msg.video };
						break;
					case 'audio':
						var caption = "";
						if (msg.audio.caption) { caption = msg.audio.caption }
						var mo = { g: 3, m: caption, rep: 'wapi', t: "mo", ts: Date.now(), type: 'aud', audio: msg.audio };
						break;
					case 'document':
						var caption = "";
						if (msg.document.caption) { caption = msg.document.caption }
						var mo = { g: 3, m: caption, rep: 'wapi', t: "mo", ts: Date.now(), type: 'doc', document: msg.document };
						break;
					case 'location':
						var mo = { g: 3, m: '[location]', rep: 'wapi', t: "mo", ts: Date.now(), type: 'loc', lng: msg.location.longitude, lat: msg.location.latitude };
						break;
					default:
						var mo = { g: 3, m: '[' + msg.type + ']', rep: 'wapi', t: "mo", ts: Date.now(), type: msg.type, dets: msg };
				}
				console.log(mo.m)
				var chn = "wsp"; //todo select from post
				if (lineOb.chn) { chn = lineOb.chn }
				var user = { chn: chn, id: msg.from, line: line, nick: msg.nick, lu: Date.now() }
				var msgId = msg.id.replace(/\./g, "_")
				//create or update user
				firebase.database().ref("/accounts/" + lineOb.account + "/chats/" + msg.from).update(user).then(function () {
					firebase.database().ref("/accounts/" + lineOb.account + "/conves/" + msg.from + "/msgs/" + msgId).set(mo).then(function () {

						//run bot
						if (lineOb.hasBot) {
							var job = {
								msg: msg,
								contact: msg.from,
								st: 'new',
								ts: Date.now(),
								isFB: true,
								type: 'process-mo'
							}
							firebase.database().ref("/accounts/" + lineOb.account + "/botJobs/" + msgId).set(job);
							console.log(new Date(), "MO Job Created", msgId)
						} else {
							firebase.database().ref("/accounts/" + lineOb.account + "/chats/" + msg.from + "/tso").once("value", function (uat) {
								var tso = uat.val();
								//start ticket					   
								if (tso) {
									//ticket already started
								} else {
									firebase.database().ref("/accounts/" + lineOb.account + "/chats/" + msg.from).update({ tso: Date.now(), status: "active" });
									console.log(new Date(), "Ticket started");
									logStartTicket(lineOb.account, msg.from, 'wsp')
								}
							});
						}
						firebase.database().ref("/accounts/" + lineOb.account + "/chats/" + msg.from + "/wsc").once("value", function (uat) {
							//reset whastapp 24h clock
							var wsc = uat.val();
							if (wsc) {
								//has 24h passed?
								if ((Date.now() - wsc) > (24 * 60 * 60 * 1000)) {
									firebase.database().ref("/accounts/" + lineOb.account + "/chats/" + msg.from).update({ wsc: Date.now() });
								}
							} else {
								firebase.database().ref("/accounts/" + lineOb.account + "/chats/" + msg.from).update({ wsc: Date.now() });
							}
						});
						console.log(new Date(), "MO Processed OK");
					}).catch(function (error) {
						console.log(new Date(), "Error posting MO to Firebase", line);
						console.log(error)
						console.log(mo)
						//TODO: persist this msg in local file to resend MOs
					});
				}).catch(function (error) {
					console.log(new Date(), "Error posting User to Firebase", line);
					console.log(error)
					console.log(sender)
					console.log(user)
					//TODO: persist this msg in local file to resend MOs
				});
			} else {
				console.log(new Date(), "Error processing MO line not found", line);
			}
		} else { //check if Contact is blocked
			console.log(new Date(), "Contact is blocked");
		}
	} else { //check if MO is duped
		console.log(new Date(), "MO is Duped");
	}

}

function logStartTicket(account, contact, chn) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact + "",
		chn: chn
	}
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
}

function processACK(ack) {
	//console.log (ack)
	var ob = { g: 0, lu: Number(ack.timestamp) * 1000 }
	switch (ack.status) {
		case "sent":
			ob.g = 1
			break;
		case "delivered":
			ob.g = 2
			break;
		case "read":
			ob.g = 3
			break;
		default:
			ob.g = ack.status
	}
	if (ack.id) {
		//get ack ref
		var mid = ack.id.replace(/\./g, "_")
		firebase.database().ref("/ack/" + mid).once('value', function (res) {
			if (res.val()) {
				var ref = res.val();
				firebase.database().ref("/accounts/" + ref.account + "/conves/" + ref.contact + "/msgs/" + ref.msgId).update(ob).then(function () {
					console.log(new Date(), "ACK updated for", mid);
				});
			} else {
				console.log(new Date(), "ACK not found for", mid);
			}
		});

	}

}

function purgeWorker() {
	if (purgeQ.length > 0) {
		var msgId = purgeQ.shift();
		//set mtq status to purged
		firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).update({ lu: Date.now(), st: 'pur' });
		console.log("Purged", msgId, purgeQ.length)
		setTimeout(function () { purgeWorker }, mtITV);
	} else {
		setTimeout(function () { purgeWorker }, 10000);
	}
}

function processPost(req, res, callback) {
	var queryData = "";
	if (typeof callback !== 'function') return null;

	req.on('data', function (data) {
		queryData += data;
		if (queryData.length > 10000000) {
			queryData = "";
			res.writeHead(413, { 'Content-Type': 'text/plain' }).end();
			req.connection.destroy();
		}
	});

	req.on('end', function () {
		if (req.method == 'GET') {
			req.post = qs.parse(req.url.replace(/^.*\?/, ''));
		} else {
			req.post = queryData;
		}
		callback();
	});
}


function cancelTrans(tid) {
	console.log("Cancel payphone trans id", tid)
	if (tid) {
		var tidParts = tid.split("_")
		var newJob = { type: 'cancel-pf-trans', contact: tidParts[0], tid: tid, st: 'new', tsc: Date.now() }
		firebase.database().ref("/accounts/" + account + "/botJobs/").push(newJob);
	} else {
		console.log("Invalid TID value")
	}
}


function confirmTrans(tid, payphoneId) {
	console.log("Confirm payphone trans id", tid, payphoneId)
	if (tid && payphoneId) {
		var tidParts = tid.split("_")
		var newJob = { type: 'confirm-pf-trans', contact: tidParts[0], tid: tid, payphoneId: payphoneId, st: 'new', tsc: Date.now() }
		firebase.database().ref("/accounts/" + account + "/botJobs/").push(newJob);
	} else {
		console.log("Invalid TID or payphoneId value")
	}
}

function cancelTransSop(tid) {
	console.log("Cancel payphone trans id", tid)
	if (tid) {
		var tidParts = tid.split("_")
		var newJob = { type: 'cancel-pf-trans-sop', contact: tidParts[0], tid: tid, st: 'new', tsc: Date.now() }
		firebase.database().ref("/accounts/" + account + "/botJobs/").push(newJob);
	} else {
		console.log("Invalid TID value")
	}
}


function confirmTransSop(tid, payphoneId) {
	console.log("Confirm payphone trans id", tid, payphoneId)
	if (tid && payphoneId) {
		var tidParts = tid.split("_")
		var newJob = { type: 'confirm-pf-trans-sop', contact: tidParts[0], tid: tid, payphoneId: payphoneId, st: 'new', tsc: Date.now() }
		firebase.database().ref("/accounts/" + account + "/botJobs/").push(newJob);
	} else {
		console.log("Invalid TID or payphoneId value")
	}
}


function confirmDeunaTrans(tx) {
	console.log("Confirm deuna trans id", tx.internalTransactionReference)
	if (tx.internalTransactionReference) {
		var tidParts = tx.internalTransactionReference.split("_")
		var newJob = { type: 'confirm-deuna-trans', contact: tidParts[1], tid: tx.internalTransactionReference, tx: tx, st: 'new', tsc: Date.now() }
		firebase.database().ref("/accounts/" + account + "/botJobs/").push(newJob);
	} else {
		console.log("Invalid TID")
	}
}

function confirmDeunaTransSop(tx) {
	console.log("Confirm deuna trans id", tx.internalTransactionReference)
	if (tx.internalTransactionReference) {
		var tidParts = tx.internalTransactionReference.split("_")
		var newJob = { type: 'confirm-deuna-trans-sop', contact: tidParts[1], tid: tx.internalTransactionReference, tx: tx, st: 'new', tsc: Date.now() }
		firebase.database().ref("/accounts/" + account + "/botJobs/").push(newJob);
	} else {
		console.log("Invalid TID")
	}
}

var app = function (req, res) {

	if (cors.apply(req, res)) {
		res.end();
	}
	if (req.method == "GET" || req.method == "POST") {
		processPost(req, res, function () {
			console.log("URL", req.url);
			if (req.url.indexOf("hub.challenge") > 0) {
				//FB challenge call
				//TO DO VALIDATE TOKEN
				res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
				res.write(req.post['hub.challenge']);
				res.end();
				//~ {
				//~ 'hub.mode': 'subscribe',
				//~ 'hub.challenge': '744944504',
				//~ 'hub.verify_token': 'a469d9a15841a469d915841a74a13ca15841a74a13c7c0' }

			}

			else if (req.url.indexOf("paylink-ok-sop") >= 0) {
				console.log(new Date(), req.url)
				//paylink-ok?id=18153803&clientTransactionId=593984252217_1689019958771
				var arr = req.url.split("&clientTransactionId=");
				var sarr = arr[0].split("=")
				confirmTransSop(arr[1], sarr[1]);
				res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
				res.write("Estamos procesando tu pago, ya puedes cerrar esta ventana.");
				res.end();
			} else if (req.url.indexOf("paylink-cancel-sop") >= 0) {
				console.log(new Date(), req.url)
				//paylink-cancel?id=0 &clientTransactionId= 593984252217_1689018679747
				var arr = req.url.split("&clientTransactionId=");
				cancelTransSop(arr[1]);
				res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
				res.write("Has cancelado tu pago, ya puedes cerrar esta ventana.");
				res.end();
			}


			else if (req.url.indexOf("paylink-ok") >= 0) {
				console.log(new Date(), req.url)
				//paylink-ok?id=18153803&clientTransactionId=593984252217_1689019958771
				var arr = req.url.split("&clientTransactionId=");
				var sarr = arr[0].split("=")
				confirmTrans(arr[1], sarr[1]);
				res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
				res.write("Estamos procesando tu pago, ya puedes cerrar esta ventana.");
				res.end();
			} else if (req.url.indexOf("paylink-cancel") >= 0) {
				console.log(new Date(), req.url)
				//paylink-cancel?id=0 &clientTransactionId= 593984252217_1689018679747
				var arr = req.url.split("&clientTransactionId=");
				cancelTrans(arr[1]);
				res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
				res.write("Has cancelado tu pago, ya puedes cerrar esta ventana.");
				res.end();
			}




			else if (req.url.indexOf("deuna-callback-payment") >= 0) {
				var authHeader = req.headers['authorization'];
				if (req.method == "POST") {
					if ((authHeader && authHeader.startsWith('Bearer '))) {
						if (authHeader.split(' ')[1] == accountOb.deunaToken) {
							var datos = ''
							if (req.post) { datos = JSON.parse(req.post) }
							console.log('DAAAAAATOS', datos)
							if (datos.internalTransactionReference.substring(0, 2) == 'lv') { confirmDeunaTrans(datos) }
							if (datos.internalTransactionReference.substring(0, 2) == 'mp') { confirmDeunaTransSop(datos) }
							res.writeHead(200, { "Content-Type": "text/plain" });
							res.write("OK");
							res.end();
						} else {
							console.log('Unauthorized, invalid token')
							res.writeHead(401, { "Content-Type": "text/plain" });
							res.write("Unauthorized");
							return res.end();
						}
					} else {
						console.log('Unauthorized, without token')
						res.writeHead(401, { "Content-Type": "text/plain" });
						res.write("Unauthorized");
						return res.end();
					}
				} else {
					//method not allowed
					console.log(new Date(), "method not allowed " + req.url, req.method);
					res.writeHead(405, { 'Content-Type': 'text/plain' });
					res.write("Method not allowed " + req.method);
					res.end()
				}
			}


			else {
				//process route
				var parts = req.url.split("/");
				//router
				switch (parts[1]) {
					case 'send':
						//process MT received from API call
						console.log(new Date(), "MT received");
						if (req.method == "POST") {
							if (typeof req.post === "string") {
								var post = _.clone(qs.parse(req.post));
								if (post.apiToken == undefined) {
									post = _.clone(JSON.parse(req.post));
								}
							} else {
								var post = _.clone(req.post);
							}
							console.log(post)
							if (apiTokens.indexOf(post.apiToken) >= 0) {
								var numberCheck = tools.numberFormat(post.to, post.isResponse)
								if (numberCheck.valid) {
									post.to = numberCheck.to;
									if (post.body) {
										if (post.body.length > 0) {
											pushMT(post)
											res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
											res.write("mt-pushed-ok");
											res.end();
										} else {
											res.writeHead(403, "ERROR", { 'Content-Type': 'text/plain' });
											res.write("invalid-message");
											res.end();
										}
									} else {
										res.writeHead(403, "ERROR", { 'Content-Type': 'text/plain' });
										res.write("invalid-message");
										res.end();
									}
								} else {
									res.writeHead(403, "ERROR", { 'Content-Type': 'text/plain' });
									res.write("invalid-number");
									res.end();
								}
							} else {
								res.writeHead(401, "ERROR", { 'Content-Type': 'text/plain' });
								res.write("invalid-token");
								res.end();
							}

						} else {
							res.writeHead(405, { 'Content-Type': 'text/plain' });
							res.write("Invalid request method");
							res.end();
						}

						break;
					case 'run-op':
						console.log(new Date(), "OP received");
						if (req.method == "POST") {

							if (typeof req.post === "string") {
								var post = JSON.parse(req.post);
							} else {
								var post = req.post;
							}
							console.log(post)
							if (apiTokens.indexOf(post.apiToken) >= 0) {
								runOp(post);
								res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
								res.write("run-op-ok");
								res.end();

							} else {
								res.writeHead(401, "ERROR", { 'Content-Type': 'text/plain' });
								res.write("invalid-token");
								res.end();
							}

						} else {
							res.writeHead(405, { 'Content-Type': 'text/plain' });
							res.write("Invalid request method");
							res.end();
						}

						break;
					case 'fbhook':
						console.log(new Date(), "FB Hook received");
						//console.log (req.post)
						processFBChanges(JSON.parse(req.post));
						res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
						res.write("hook-ok");
						res.end();
						break;
					case 'getmediaurl':

						console.log(new Date(), "Get Media URL", parts[2], parts[3]);
						if (req.method == "GET") {
							//check if media in assets
							fs.readFile('./fbAssets/media/' + parts[2] + '/' + parts[3], 'utf8', function (err, data) {
								if (err) {
									getMediaURL(parts[2], parts[3], function (b64) {
										res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
										res.write(b64);
										res.end();
									});

								} else {
									console.log("Media loaded from assets cache");
									res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
									res.write(data);
									res.end();
								}
							});

						} else {
							res.writeHead(405, { 'Content-Type': 'text/plain' });
							res.write("Invalid request method");
							res.end();
						}
						break;
					case 'getTemplates':

						console.log(new Date(), "Get Templates", parts[2]);
						if (req.method == "GET") {
							if (parts[2]) {
								//get templates from FB
								getTemplates(parts[2], function (arr) {
									res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
									res.write(JSON.stringify(arr));
									res.end();
								});
							} else {
								res.writeHead(400, "ERROR", { 'Content-Type': 'text/plain' });
								res.write("missing-line");
								res.end();
							}

						} else {
							res.writeHead(405, { 'Content-Type': 'text/plain' });
							res.write("Invalid request method");
							res.end();
						}
						break;
					case "ping":
						res.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
						res.write("pong");
						res.end();
						break;
					default:
						console.log(new Date(), "unknown path " + req.url);
						res.writeHead(404, { 'Content-Type': 'text/plain' });
						res.write("unknown path " + req.url);
						res.end();
				}
			}
		}); //end process post
	}
}
