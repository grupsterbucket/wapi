//https://developers.facebook.com/apps/745136353572824/whatsapp-business/wa-settings/?business_id=514393120290055
var fetch = require('node-fetch');
var tools = require('./tools');
var http = require("http");
var https = require("https");
var fs = require('fs');
var qs = require('querystring');
var _ = require('underscore');
var HttpCors = require('http-cors');
var cors = new HttpCors();
var accountOb = {}
var mtq = {}
var booted = false;
var apiTokens = []
var fbTokens = {}
var graphURL = 'https://graph.facebook.com/v13.0/'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs

//global scope to access from modules
global.lines = [];
global.firebase = require('firebase');

//read port and account from command line
var account = "demo" //default
account = process.argv[2];	
phone = process.argv[3];	
pin = process.argv[4];	


//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            boot();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function boot() {
  if (!booted) {
    booted = true;
    //load lines
    loadAccount(function (ob) {
		if (ob) {
			accountOb = JSON.parse( JSON.stringify(ob) );
				loadLines(function () {
					  setTimeout( function() { 
						  //verifyCode()
						  //requestCode()
						  register()
					  }, 2000);
				});
			
		} else {
			console.log ("ERROR",account,"account not found")	
		}
	});
  }
}

function loadAccount(cb) {
	firebase.database().ref('/accounts/'+account+'/config').once('value', function(snapshot) {
		if (snapshot.val()) {
			cb( JSON.parse( JSON.stringify(snapshot.val()) ) );	
		} else {
			cb(null);	
		}
	});
}
function loadLines(cb) {
	firebase.database().ref('/lines/').orderByChild("account").equalTo(account).once('value', function(snapshot) {		
		var arr = _.map (snapshot.val(), function(value, key) {
			value.id = key;
			return value;
		});
		for (var i=0; i<arr.length; i++) {
			if (arr[i].isWAPIFB) {
				lines.push( arr[i] );
				apiTokens.push(arr[i].apiToken);
				loadToken(arr[i].id);
			}
		}
		console.log (lines.length + " line(s) loaded for "+account);
		if (cb) { cb(); }
	});
}

function loadToken(line) {
	firebase.database().ref('fbTokens/'+line).once('value', function(tkn) {
		if (tkn.val()) {
			console.log ("Token found for line", line)
			fbTokens[line] = tkn.val()
		} else {
			console.log ("Token NOT found for line", line)
			fbTokens[line] = ""
		}
	});
}	

function requestCode() {
	console.log ("request code", phone)
	var lineOb = _.findWhere( lines, {id: phone} );
	if (lineOb) {
		var ob = {
		  "messaging_product": "whatsapp",
		  "code_method": "SMS",
		  "locale": "en_US",
		  "language": "en"
		}
		
		console.log ( JSON.stringify(ob) )
		fetch(graphURL+lineOb.wabid+'/request_code', {
			method: 'post',
			body:    JSON.stringify(ob),
			headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + fbTokens[lineOb.id] },
		})
		.then(res => res.text())
		.then(function(body) {
			var res = JSON.parse(body)
			console.log (res)
			process.exit(0)
		});
	} else {
		cb ({error: 'Missing line ob'});
	}
}

function verifyCode() {
	console.log ("verify code", phone, pin)
	var lineOb = _.findWhere( lines, {id: phone} );
	if (lineOb) {
		var ob = {
		  "messaging_product": "whatsapp",
		  "code": pin,
		  "locale": "en_US",
		  "language": "en"
		}
		
		console.log ( JSON.stringify(ob) )
		fetch(graphURL+lineOb.wabid+'/verify_code', {
			method: 'post',
			body:    JSON.stringify(ob),
			headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + fbTokens[lineOb.id] },
		})
		.then(res => res.text())
		.then(function(body) {
			var res = JSON.parse(body)
			console.log (res)
			process.exit(0)
		});
	} else {
		cb ({error: 'Missing line ob'});
	}
}

function register() {
	console.log ("register", phone, pin)
	var lineOb = _.findWhere( lines, {id: phone} );
	if (lineOb) {
		var ob = {
		  "messaging_product": "whatsapp",
		  "pin": pin
		}
		
		console.log ( JSON.stringify(ob) )
		fetch(graphURL+lineOb.wabid+'/register', {
			method: 'post',
			body:    JSON.stringify(ob),
			headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + fbTokens[lineOb.id] },
		})
		.then(res => res.text())
		.then(function(body) {
			var res = JSON.parse(body)
			console.log (res)
			process.exit(0)
		});
	} else {
		cb ({error: 'Missing line ob'});
	}
}

