
var firebase = require('firebase');

//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            boot();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function boot() {
	if (process.argv[2]) {
		console.log ("You have 30 seconds to change your mind and cancel process");
		console.log ("Deleting data from", process.argv[2]);
		setTimeout(function() {
			console.log ("Start delete...");
			firebase.database().ref('/accounts/'+process.argv[2]+"/mtq").set({}, function(res) {
				console.log ("mtq deleted");
			});
			
			//~ firebase.database().ref('/accounts/'+process.argv[2]+"/media").set({}, function(res) {
				//~ console.log ("media deleted");
			//~ });
			
			//~ firebase.database().ref('/accounts/'+process.argv[2]+"/chats").set({}, function(res) {
				//~ console.log ("chats deleted");
			//~ });
		}, 1000*32);
		var s = 30
		var itv = setInterval(function() {
			console.log (s)
			s -= 1;
			if (s<=0) {
				clearInterval(itv);
				}
			}, 1000);
	}
}
