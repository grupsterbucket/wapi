var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "udla-bulk";
var booted = false;
var jobs = []


//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	//get user step in bot
	firebase.database().ref("/accounts/"+client+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var group = "main";
		if (user.step) {step = user.step};
		if (user.group) {group = user.group};
		
		console.log ("User "+job.contact+" is at step:", step, "group:", group);
		
		pushText ("Gracias por escribirnos, para mayor información por favor escríbenos a https://bit.ly/3pvtiqZ", job.contact, user.line);
		finishJob(job);
		
		if (false) {//temp out
		//check if user has a campusers entry
		firebase.database().ref("/backroute/").orderByChild("to").equalTo(job.contact).once('value', function(results) {
			var campId = "0"
			var clienteFinal = "na"
			if (results.val()) {
				var arr = _.map(results.val(), function(val, key){
					val.id = key
					return val
					})
				arr = _.sortBy(arr, function(ob) { return -ob.tsc });
				var lastCamp = arr[0];
				//get last sent	camp ID
				campId = lastCamp.campId;
				clienteFinal = lastCamp.clienteFinal;
			} 
			if (step != 'idle') {
					console.log ("Process MO ", job.msg.type);
					switch (job.msg.type) {
						case 'txt':
							var body = job.msg.m;
							if (body.length>0) {
								console.log ("Mo: ",body)
								//trim spaces
								body = body.trim();
								if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
									var msg = "Gracias por usar este canal. Adios!";
									pushText (msg, job.contact, user.line);
									resetUserParam("step", job.contact, user.line);
								} if (clienteFinal == "loader") {
									console.log ("MO is a load test response", body)
									var parts = body.split("|");
									firebase.database().ref("/accounts/"+client+"/tests/"+parts[0]).push( {line: job.line, tsr: job.ts, ref: parts[1], tss: Number(parts[2])} );
								} else {
									//process step
									switch (step) {
										case "0":
											var kw = body.toLowerCase()
											if ( kw == "si" ) {
												var msg = "Gracias por tu respuesta te adjuntamos el link de pago https://bit.ly/3j0jG3y  y te recuerdo que debes realizar el pago y enviar la documentación hasta el 08 de octubre";
												pushText (msg, job.contact, user.line);
											} else if ( kw == "no" ) {
												var msg = "Agradecemos tu participación y te esperamos en una próxima ocasión";
												pushText (msg, job.contact, user.line);
											} else if ( kw.indexOf("ok")<0 ) {
												var msg = "La UDLA te agradece por preferirnos. Un consultor se pondrá en contacto en la brevedad posible.\n\n";
												msg += "Escoge una de las siguientes opciones:\n\n";
												msg += "1. Pregrado\n";
												msg += "2. Posgrado\n";
												msg += "3. Diplomados y Certificaciones\n";
												msg += "4. Homologación\n";
												msg += "5. Arizona\n\n";
												msg += "Por favor responde con el número de la opción que deseas seleccionar\n";
												setUserParam("step","1", job.contact, user.line);
												pushText (msg, job.contact, user.line);
											}
											break;
											//end step 0
										case "1":
											var kw = body.toLowerCase()
											var group = ""
											if (["1","1.","uno"].indexOf(kw)>=0) {
												group = "pregrado"
											}
											if (["2","2.","dos"].indexOf(kw)>=0) {
												group = "posgrado"
											}
											if (["3","3.","tres"].indexOf(kw)>=0) {
												group = "ec"
											}
											if (["4","4.","cuatro"].indexOf(kw)>=0) {
												group = "homologación"
											}
											if (["5","5.","cinco"].indexOf(kw)>=0) {
												group = "arizona"
											}
											if (group.length>0) {
												setUserParam("step","idle", job.contact, user.line);
												setUserParam("status","active", job.contact, user.line);
												setUserParam("group", group, job.contact, user.line); 
												setUserParam("x-camp", campId, job.contact, user.line);
												var msg = "Enseguida te atendemos. Gracias por tu espera."
												pushText (msg, job.contact, user.line);
											} else {
												if ( kw.indexOf("ok")<0 ) {
													var msg = "La UDLA te agradece por preferirnos. Un consultor se pondrá en contacto en la brevedad posible.\n\n";
													msg += "Escoge una de las siguientes opciones:\n\n";
													msg += "1. Pregrado\n";
													msg += "2. Posgrado\n";
													msg += "3. Diplomados y Certificaciones\n";
													msg += "4. Homologación\n";
													msg += "5. Arizona\n\n";
													msg += "Por favor responde con el número de la opción que deseas seleccionar\n";
													pushText (msg, job.contact, user.line);
												} else {
													var msg = "Por favor responde solo con el número de la opción que deseas para poder continuar o responde *salir* para terminar la sesión";
													pushText (msg, job.contact, user.line);
												}
											}
										break;
									}//end switch step
								}
							} //end if body length > 0
							//job is completed
							finishJob(job);
							break; 
						default:
							
							//finish jobs
							finishJob(job);
							break;
					} //end switch mo type
			} else { //end idle check
				console.log ('user '+job.contact+'@'+user.line+' is idle');
				//job is completed
				finishJob(job);
			}
		}); //end campusers check
		}//end temp out
	});
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(1000));
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		console.log (new Date(), "Job completed");
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}


function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).set(val);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).remove();
}

