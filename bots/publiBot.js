var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var fetch = require('node-fetch');
var client = "publi";
var booted = false;
var jobs = []
var config = {}
var token = "";
//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	loadAccountConfig(function() {
		if (!booted) {
			booted = true;
			getPubliToken();
			setTimeout(function() {
				listenToJobs();
				processJobQueue();
			},2000);
		}
	});
}

function loadAccountConfig(cb) {
	firebase.database().ref("/accounts/"+client+"/config/").on('value', function(res) {
		config = res.val();
		console.log (new Date(), "Publi config loaded")
		cb();
	});
}
function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function getPubliToken() {
	fetch( config.creds.publi.tokenURL, {
		method: 'post',
		headers: { 
				   'Accept': 'application/json',
				   'Content-Type': 'application/json'
		},
		body: JSON.stringify({Username: config.creds.publi.user, Password: config.creds.publi.pwd})
	}).then(function(response) {
		if (response.status !== 200 ) {
			console.log(new Date(),'Publi API error',response.status);
			response.json().then(function(data) {
				console.log(data);
			});
			process.exit(0);
		} else {
			response.text().then(function(data) {
				token = data;
				console.log (new Date(), "Publi token updated")
				setTimeout(function() { getPubliToken(); }, config.creds.publi.tokenRefresh)
			});
		}
	}).catch(function(err) {
		console.log(new Date(),'Publi API request error',err.message);
		process.exit(0);
	});	
}

function postMOtoPubli(job, mt,  cb) {
	
	var camp = ""
	var extId = ""
	console.log (mt)
	if (mt) {
		if (mt.camp) { camp = mt.camp}
		if (mt.extId) { extId = mt.extId}	
	}
	
	var mo = {
	  "Campania": camp,
	  "Mensaje_id": extId,
	  "Fecha": new Date(job.msg.ts),
	  "Telefono": job.contact,
	  "Contenido": job.msg.m,
	  "Nic": job.user.nick
	}
	if (job.msg.url) { mo.Contenido = mo.Contenido + " " + job.msg.url }
	console.log (mo)
	fetch( config.creds.publi.postMOURL, {
		method: 'post',
		headers: { 
				   'Accept': 'application/json',
				   'Content-Type': 'application/json',
				   'Authorization': 'Bearer ' + token.replace(/\"/g,"")
		},
		body: JSON.stringify(mo)
	}).then(function(response) {
		if (response.status !== 200 ) {
			console.log(new Date(),'Publi API error',response.status);
			response.json().then(function(data) {
				console.log(data);
			});
			cb(false)
		} else {
			response.text().then(function(data) {
				console.log (data)
				console.log (new Date(), "MO Posted")
				cb(true)
			});
		}
	}).catch(function(err) {
		console.log(new Date(),'Publi API request error',err.message);
		cb(false)
	});	
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	console.log (job)
	//get user conve to find last MT sent
	firebase.database().ref("/accounts/"+client+"/mtq/").orderByChild("to").equalTo(job.contact).once('value', function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(val, key){
					val.id = key
					return val
					})
			arr = _.sortBy(arr, function(ob) { return -ob.ts });
			var mt = arr[0];
			postMOtoPubli(job, mt, function(posted) {
				if (posted) {
					finishJob(job);
				} else {
					requeueJob(job);
				}
			});
		} else {
			postMOtoPubli(job, null, function(posted) {
				if (posted) {
					finishJob(job);
				} else {
					requeueJob(job);
				}
			});
		}
	});	
}



function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		console.log (new Date(), "Job completed");
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}

function requeueJob(job) {
	jobs.push(job)
	setTimeout(function() {
		processJobQueue();
	}, tools.getRndMS(5000) );
}
		

