global.fetch = require('node-fetch');
global.tools = require('./tools');
var fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
global.envBot = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
var soporte = require('./uanatacaPluginSoporte');
var sac = require('./uanatacaPlugin');
global.utapi = require('./uanatacaAPI');
global.ftapi = require('./faceTecAPI');
global.distriapi = require('./distribuidoresAPI');
global.deuna = require('./deuna');
global.payphone = require('./payphone');
utapi.initialize({ account: "ut" });
global._ = require('underscore');
global.firebase = require('firebase');
var CryptoJS = require("crypto-js");
global.account = "uanataca";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 250;
global.config = {}
global.calendars = []
global.lines = {}

//firebase init
var firebaseConfig = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
	"authDomain": "ubiku3.firebaseapp.com",
	"databaseURL": "https://ubiku3.firebaseio.com",
	"projectId": "ubiku3",
	"storageBucket": "ubiku3.appspot.com",
	"messagingSenderId": "946813430025",
	"appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(function (user) {
	if (user) {
		// User is signed in.
		console.log("Logged in to firebase as uid " + user.uid);
		setTimeout(function () {
			bootApp();
		}, 2000);

	} else {
		// User is signed out login again
		console.log("Session expired");
		loginTofirebase();
	}
	// ...
});

function loginTofirebase() {
	console.log("Login to firebase");
	firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
		var errorCode = error.code;
		var errorMessage = error.message;
		console.log(error.code + ": " + error.message);
	});
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		loadAccountConfig();
		setTimeout(function () {
			listenToJobs();
			processJobQueue();
		}, 1000);
	}
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/' + account + '/config/misc').on('value', function (snapshot) {
		config = snapshot.val();
		console.log("Config fetched for account", account);
	});
	firebase.database().ref('accounts/' + account + '/config/calendars').on('value', function (snapshot) {
		calendars = _.map(snapshot.val(), function (ob, key) {
			ob.id = key;
			return ob;
		});
		console.log("Calendars fetched for account", account);
	});
}
function listenToJobs() {
	console.log(new Date(), "Start listen jobs", account);
	firebase.database().ref("/accounts/" + account + "/botJobs/").orderByChild("st").equalTo("new").on('child_added', function (res) {
		var job = res.val();
		job.id = res.key;
		var line = "none"
		if (job.msg) { if (job.msg.line) { line = job.msg.line } }
		if (job.line) { line = job.line }
		if (line == "593990202594" || job.type == "confirm-deuna-trans-sop" || job.type == "confirm-pf-trans-sop" || job.type == "cancel-pf-trans-sop") {
			jobs.push(job)
			console.log(new Date(), 'New job queued', job.id, job.type)
		}
	});
}

function processJobQueue() {
	if (jobs.length > 0) {
		processJob(jobs.shift());
	} else {
		setTimeout(function () {
			processJobQueue();
		}, qitv);
	}
}

function processJob(job) {
	firebase.database().ref('/accounts/' + account + '/chats/' + job.contact).once('value', function (snapshot) {
		var user = snapshot.val()

		var step = '0';
		if (user.step) {
			step = user.step
		}

		var group = 'main'
		if (user.group) {
			group = user.group
		}


		switch (job.type) {
			case "check-liveness-status4D":
				console.log('IGNORE JOB TYPE FOR NEXXIT')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "check-biometry":
				console.log('IGNORE JOB TYPE FOR NEXXIT')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "check-transcribe":
				console.log('IGNORE JOB TYPE FOR NEXXIT')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "check-liveness-status":
				console.log('IGNORE JOB TYPE FOR NEXXIT')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "os-request-completed":
				console.log('IGNORE JOB TYPE FOR NEXXIT')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "os-signed-pdf":
				console.log('IGNORE JOB TYPE FOR NEXXIT')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "os-error":
				console.log('IGNORE JOB TYPE FOR NEXXIT')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "confirm-pf-trans":
				console.log('IGNORE JOB PAYPHONE')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "confirm-deuna-trans":
				console.log('IGNORE JOB DEUNA')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "cancel-pf-trans":
				console.log('IGNORE JOB PAYPHONE')
				setTimeout(function () { processJobQueue(); }, qitv);
				break
			case "confirm-pf-trans-sop":
				soporte.job(job, user, step, group, function (resp) {
					if (resp.st == "done") {
						//job is completed
						finishJob(job)
					} else {
						requeueJob(job, resp.ts)
					}
				})
				break
			case "confirm-deuna-trans-sop":
				soporte.job(job, user, step, group, function (resp) {
					if (resp.st == "done") {
						//job is completed
						finishJob(job)
					} else {
						requeueJob(job, resp.ts)
					}
				})
				break
			case "cancel-pf-trans-sop":
				soporte.job(job, user, step, group, function (resp) {
					if (resp.st == "done") {
						//job is completed
						finishJob(job)
					} else {
						requeueJob(job, resp.ts)
					}
				})
				break
			case 'send-notification':
				firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
					var user = snapshot.val();
					pushText(job.str, job.contact, user.line);
					//job is completed
					finishJob(job);
				});
				break;
			case 'reset-bot':
				firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
					var user = snapshot.val();
					resetBot(job.contact, user);
					//job is completed
					finishJob(job);
				});
				break;
			case 'booking-completed':
				firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
					var user = snapshot.val();
					console.log("booking completed")
					//return main menu to user
					//runBot("", job, user, "999", user.group, account);
					//job is completed
					finishJob(job);
				});
				break;
			default:
				firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
					var user = snapshot.val();
					var step = "0";
					var group = "main";

					if (user.step) { step = user.step };
					if (user.group) { group = user.group };

					//!Pruebas JAGM: poner grupo en user 
					if (config.whiteList.includes(user.id)) {
						group = 'sod'
						updateUser({ group: "sod" }, job.contact, user.line);
					}
					if (config.whiteListPiccolos.includes(user.id)) {
						group = 'pic'
						updateUser({ group: "pic" }, job.contact, user.line);
					}
					//!FIN Pruebas JAGM: poner grupo en user 


					if (user.tsso) {
						console.log("User " + job.contact + " is at step:", step, "group:", group);
					} else {
						startSession(job.contact, user);
						console.log("New Session for " + job.contact + "");
					}
					if (step.indexOf("nxt") == 0) {
						console.log("Ignored nexxit job")
						setTimeout(function () { processJobQueue(); }, qitv);
					} else if (step.indexOf("svf") == 0) {
						console.log("Ignored servifirma job")
						setTimeout(function () { processJobQueue(); }, qitv);
					} else {
						if (step != 'idle') {
							var msgType = job.msg.type
							if (msgType == "chat") { msgType = "text" }
							if (msgType == "") { msgType = "text" }
							console.log("Process MO ", msgType, "Line", user.line);
							switch (msgType) {
								case 'text':

									if (job.msg.m) {
										var body = job.msg.m;
									} else {
										var body = job.msg.text.body;
									}

									console.log("Body", body);

									if (body.length > 0) {
										//trim spaces
										body = body.trim();

										//catch operations
										if (body.toLowerCase().indexOf("#") == 0) {
											var parts = body.toLowerCase().split(" ");
											//match direct operation
											switch (parts[0]) {
												case "#salir":
													salir(job, user);
													break;
												case "#ping":
													var str = "#pong " + new Date();
													pushText(str, job.contact, user.line);
													break;
												case "#server":
													var str = "Server data:\n";
													str += "Datetime: " + new Date() + "\n";
													str += "Ram: " + Math.round(os.totalmem() / 1000000) + "MB\n";
													str += "Free: " + Math.round(os.freemem() / 1000000) + "MB\n";
													pushText(str, job.contact, user.line);
													break;
												case "#nexxit":
													//switch user to nexxit steps
													setUserParam('step', 'nxt-start', job.contact, user.line)
													//repost job
													var newJob = JSON.parse(JSON.stringify(job));
													delete newJob.id
													newJob.msg.text.body = "Hola Nexxit"
													firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)
													break;
												case "#nda":
													//switch user to nexxit steps
													setUserParam('step', 'nxt-menu', job.contact, user.line)
													//repost job
													var newJob = JSON.parse(JSON.stringify(job));
													delete newJob.id
													newJob.msg.text.body = "4"
													firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)
													break;
												case "#simple":
													//switch user to nexxit steps
													setUserParam('step', 'nxt-menu', job.contact, user.line)
													//repost job
													var newJob = JSON.parse(JSON.stringify(job));
													delete newJob.id
													newJob.msg.text.body = "9"
													firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)
													break;
												case "#certificada":
													//switch user to nexxit steps
													setUserParam('step', 'nxt-menu', job.contact, user.line)
													//repost job
													var newJob = JSON.parse(JSON.stringify(job));
													delete newJob.id
													newJob.msg.text.body = "9"
													firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)
													break;
												case "#avanzada":
													//switch user to nexxit steps
													setUserParam('step', 'nxt-menu', job.contact, user.line)
													//repost job
													var newJob = JSON.parse(JSON.stringify(job));
													delete newJob.id
													newJob.msg.text.body = "9"
													firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)
													break;
												case "#4d":
													//switch user to nexxit steps
													setUserParam('step', 'nxt-4d0', job.contact, user.line)
													//repost job
													var newJob = JSON.parse(JSON.stringify(job));
													delete newJob.id
													newJob.msg.text.body = "Prueba Liveness 4D"
													firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)
													break;
												case "#nda4":
													//switch user to nexxit steps
													setUserParam('step', 'nxt-4a0', job.contact, user.line)
													//repost job
													var newJob = JSON.parse(JSON.stringify(job));
													delete newJob.id
													newJob.msg.text.body = "Liveness 4D + NDA"
													firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)
													break;
												case "#equifax":
													//switch user to nexxit steps
													setUserParam('step', 'nxt-eq0', job.contact, user.line)
													//repost job
													var newJob = JSON.parse(JSON.stringify(job));
													delete newJob.id
													newJob.msg.text.body = "Liveness 4D + Carta Equifax"
													firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)
													break;
												case "#ayuda-servifirma":
													//abre ticket de servcio de servifirma
													var msg = "Gracias por escribirnos , te atenderemos lo más pronto posible\n";
													updateUser({ step: "idle", status: "active", group: "svf", tso: Date.now() }, job.contact, user.line);
													pushText(msg, job.contact, user.line);
													logStartTicket(job.contact, user.chn, user, "svf");
													break;
											} //end switch parts operation
										} else {
											if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
												salir(job, user);
											} else if (body.toLowerCase() == "servifirma" || body.toLowerCase() == "hola! quiero sacar mi firma electrónica") {
												//switch user to nexxit steps
												setUserParam('step', 'svf-start', job.contact, user.line)
												//repost job
												var newJob = JSON.parse(JSON.stringify(job));
												delete newJob.id
												newJob.msg.text.body = "Servifirma Demo"
												firebase.database().ref('/accounts/' + account + '/botJobs/').push(newJob)

											} else {
												//chat is not operation, run bot
												runBot(body.toLowerCase(), job, user, step, group, account);
											}
										} //end if operation
									} //body is empty
									//job is completed
									finishJob(job);
									break;
								case 'image':
									console.log(job.msg)
									if (job.msg.image.caption == "#l2d") {
										//run liveness 2D test
										if (lines[user.line]) {
											getMediaB64(lines[user.line], job.msg.image.id, function (b64) {
												if (b64) {
													ftapi.liveness2D(b64, function (results) {
														console.log(results)
														var imageStatus = ["FACE_FOUND_WITH_SUFFICIENT_QUALITY", "INVALID_IMAGE", "VALID_IMAGE_BUT_DETECTION_ISSUE", "QUALITY_TOO_LOW"]
														if (results.success) {
															var msg = results.liveness2DStatusMessage + " (" + imageStatus[results.liveness2DStatusEnumInt] + ")"
															//~ isLikelyRealPerson: true,
															//~ liveness2DStatusEnumInt: 0,
															//~ liveness2DStatusMessage: 'The subject in the submitted Image is likely a Real Human.',

															pushText(msg, job.contact, user.line);
														} else {
															pushText("La imagen no parece de una persona viva (" + imageStatus[results.liveness2DStatusEnumInt] + ")", job.contact, user.line);
														}
													})
												} else {
													pushText("Error interno cargando imagen desde FB", job.contact, user.line);
												}
											});
										} else {
											loadLineObject(user.line, function (loaded) {
												if (loaded) {
													getMediaB64(lines[user.line], job.msg.image.id, function (b64) {
														if (b64) {
															ftapi.liveness2D(b64, function (results) {
																console.log(results)
																var imageStatus = ["FACE_FOUND_WITH_SUFFICIENT_QUALITY", "INVALID_IMAGE", "VALID_IMAGE_BUT_DETECTION_ISSUE", "QUALITY_TOO_LOW"]
																if (results.success) {
																	var msg = results.liveness2DStatusMessage + " (" + imageStatus[results.liveness2DStatusEnumInt] + ")"
																	//~ isLikelyRealPerson: true,
																	//~ liveness2DStatusEnumInt: 0,
																	//~ liveness2DStatusMessage: 'The subject in the submitted Image is likely a Real Human.',

																	pushText(msg, job.contact, user.line);
																} else {
																	pushText("La imagen no parece de una persona viva (" + imageStatus[results.liveness2DStatusEnumInt] + ")", job.contact, user.line);
																}
															})
														} else {
															pushText("Error interno cargando imagen desde FB", job.contact, user.line);
														}
													});
												} else {
													pushText("Error interno cargando detalles de la línea " + user.line, job.contact, user.line);
												}
											})
										}
									} else {
										runBot("[" + job.msg.type + "]", job, user, step, group, account);
									}
									//job is completed
									finishJob(job);
									break;


								case 'interactive':
									console.log(job.msg.interactive)
									//*Verificar si son botones de WhatsApp
									if (job.msg.interactive.button_reply) {
										runBot(job.msg.interactive.button_reply.id, job, user, step, group, account);
									}
									//*Verificar si son opciones de WhatsApp
									if (job.msg.interactive.list_reply) {
										runBot(job.msg.interactive.list_reply.id, job, user, step, group, account);
									}
									finishJob(job);
									break;

								case 'button':
									runBot(job.msg.button.payload, job, user, step, group, account);
									//job is completed
									finishJob(job)
									break


								default:
									runBot("[" + job.msg.type + "]", job, user, step, group, account);
									//job is completed
									finishJob(job);
									break;
							} //end switch mo type
						} else { //end idle check
							console.log('user ' + job.contact + '@' + user.line + ' is idle');
							//is a reset operation?
							var body = job.msg.body;
							if (body) {
								if (body.length > 0) {
									//trim spaces
									body = body.trim();
									if (body.toLowerCase().indexOf("#") == 0) {
										if (body.toLowerCase() == "#reset") {
											resetBot(job.contact, user, "Reset Session OK");
										}
									}
								}
							}
							//job is completed
							finishJob(job);
						}
					} //end check if job is nexxit
				});
		}//end switch job type
	})
}

function runBot(body, job, user, step, group, account) {
	//soporte distris 593990202594
	//soporte sac 593968720200

	if (user.line == "593990202594") {
		console.log(new Date(), user.line, step, step.indexOf("sop"))
		if (step.indexOf("sop") == 0 || step == "0" || step == "go-back") {
			soporte.bot(body, job, user, step, group, account)
		} else {
			//start channel
			setUserParam("step", "0", user.id)
			soporte.bot(body, job, user, "0", group, account)
		}
	}
}

function finishJob(job) {
	//remove job from q
	firebase.database().ref("/accounts/" + account + "/botJobs/" + job.id).remove();
	//dump locally
	console.log(new Date(), "JOB Completed");
	//console.log (JSON.stringify(job))
	//process next job
	setTimeout(function () { processJobQueue(); }, qitv);
}

function finishJobOLD(job) {
	firebase.database().ref("/accounts/" + account + "/botJobs/" + job.id + "/st/").set('done', function (res) {
		setTimeout(function () {
			processJobQueue();
		}, qitv);
	});
}

global.pageBreak = function () {
	return '\n\n--------------------------oooo--------------------------\n\n'
}

global.pushText = function (body, contact, line, rep, type, desc, url, b64, cb) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	var msgId = Date.now() + "-" + line + "-" + contact;
	var mt = { g: 0, m: body, rep: rep, t: "mt", ts: Date.now(), type: type, st: 'qed', lu: Date.now() };
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	firebase.database().ref("/accounts/" + account + "/conves/" + contact + "/msgs/" + msgId).set(mt).then(function () {
		if (cb) { cb(); }
		var ob = _.clone(mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).set(ob).then(function () {
			console.log(new Date(), "MT Response POSTED OK");
		}).catch(function (error) {
			console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function (error) {
		console.log(new Date(), "Push MTQ Error", error);
	});

}

global.pushTemplate = function (params, templateName, contact, line) {

	var msgId = Date.now() + "-" + line + "-" + contact;
	var mt = { g: 0, m: "Nuevo ticket en Textcenter", rep: "bot", t: "mt", ts: Date.now(), type: "txt", st: "qed", lu: Date.now() };
	mt.isTemplate = true
	mt.lang = "es"; //todo read from DB
	mt.params = params.join("|");
	mt.templateName = templateName;
	firebase.database().ref("/accounts/" + account + "/conves/" + contact + "/msgs/" + msgId).set(mt).then(function () {
		var ob = _.clone(mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).set(ob).then(function () {
			console.log(new Date(), "Template Response POSTED OK");
		}).catch(function (error) {
			console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function (error) {
		console.log(new Date(), "Push MTQ Error", error);
	});

}


global.setUserParam = function (key, val, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/" + key).set(val);
}

global.updateUser = function (ob, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/").update(ob);
}

function startSession(contact, user) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/tsso").set(Date.now());
	logStartSession(contact, user.chn);
}

global.logStartSession = function (contact, chn) {
	var log = {
		type: "session-start",
		ts: Date.now(),
		ref: contact + "",
		chn: chn
	}
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
}

global.logEndSession = function (contact, chn, tsso, op) {
	var log = {
		type: "session-end",
		tssx: Date.now(),
		ref: contact + "",
		ts: Date.now(),
		op: op
	}
	if (tsso) { log.tsso = tsso; }
	if (chn) { log.chn = chn; }
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
}

global.logStartTicket = function (contact, chn, user, group) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact + "",
		chn: chn,
		group: group
	}
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
	//notificar
	if (config.notifyTickets) {
		if (config.notifyTickets[group]) {
			var arr = config.notifyTickets[group].split(",");
			for (var i = 0; i < arr.length; i++) {
				pushTemplate(["https://v3.textcenter.net/#inbox?c=" + contact], "new_ticket_created", arr[i], user.line);
			}
		} else if (config.notifyTickets.main) {
			var arr = config.notifyTickets.main.split(",");
			for (var i = 0; i < arr.length; i++) {
				pushTemplate(["https://v3.textcenter.net/#inbox?c=" + contact], "new_ticket_created", arr[i], user.line);
			}
		}

	}

}

global.resetUserParam = function (key, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/" + key).remove();
}

global.saveForm = function (ob, cb) {
	firebase.database().ref("/accounts/" + account + "/forms").push(ob).then(function (res) {
		cb(res.key);
	});
}

global.salir = function (job, user) {
	var self = this;
	logEndSession(job.contact, user.chn, user.tsso, "s");
	var str = "Gracias por usar este servicio. Adios!"
	pushText(str, job.contact, user.line);
	resetUserParam('step', job.contact);
	resetUserParam('pin', job.contact);
	resetUserParam('group', job.contact);
	resetUserParam('account', job.contact);
	resetUserParam('tsso', job.contact);
	resetUserParam('ced', job.contact);
	resetUserParam('statusSolicitud', job.contact);
	resetUserParam('tipo_perfil', job.contact);
	resetUserParam('fechaNacimientoRC', job.contact);
	resetUserParam('perfil', job.contact);
	resetUserParam('firmas', job.contact);
	resetUserParam('solicitudes', job.contact);
	resetUserParam('solicitudCancelar', job.contact);
	resetUserParam('solicitudesConEstado', job.contact);
	resetUserParam('payloadCT', job.contact);
	resetUserParam('valorRecargaC', job.contact);
	resetUserParam('cedVN', job.contact);
	resetUserParam('nombreVN', job.contact);
	resetUserParam('mailVN', job.contact);
	resetUserParam('validezVN', job.contact);
	resetUserParam('formatoVN', job.contact);
	resetUserParam('contenedorVN', job.contact);
	resetUserParam('celVN', job.contact);
	resetUserParam('precioVenta', job.contact);
	resetUserParam('partnerType', job.contact);
	resetUserParam('partnerUid', job.contact);
	resetUserParam('cedulaMp', job.contact);
	resetUserParam('solicitudesMp', job.contact);
	resetUserParam('tokensDistri', job.contact);
	resetUserParam('fechasDistri', job.contact);
	resetUserParam('uidsDistri', job.contact);
	resetUserParam('statusRequest', job.contact);
	setUserParam('status', 'reset', job.contact);

}

global.resetBot = function (contact, user, msg) {
	logEndSession(contact, user.chn, user.tsso, "r");

	if (msg) { pushText(msg, contact, user.line); }
	resetUserParam('step', contact);
	resetUserParam('pin', contact);
	resetUserParam('group', contact);
	resetUserParam('account', contact);
	resetUserParam('tsso', contact);
	resetUserParam('ced', job.contact);
	resetUserParam('statusSolicitud', job.contact);
	resetUserParam('tipo_perfil', job.contact);
	resetUserParam('perfil', job.contact);
	resetUserParam('firmas', job.contact);
	resetUserParam('solicitudes', job.contact);
	resetUserParam('solicitudCancelar', job.contact);
	setUserParam('status', 'reset', contact);
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()
}

global.getContact = function (phone, cb) {
	console.log("Searching for", phone);
	firebase.database().ref("/accounts/" + account + "/contacts").orderByChild("phone").equalTo(phone + '').once("value", function (res) {
		if (res.val()) {
			var arr = _.map(res.val(), function (ob, key) {
				ob.id = key;
				return ob;
			});
			console.log("Contact found", arr.length);
			var ob = arr[0];
			if (ob.group) {
				cb(ob)
			} else {
				cb(null);
			}
		} else {
			cb(null);
		}
	}).catch(function (error) {
		console.log(error)
		cb(null);
	});
}

global.getBalance = function (accId, cb) {
	console.log("Checking balance for", accId);
	firebase.database().ref("/accounts/" + account + "/trans").orderByChild("account").equalTo(accId).once("value", function (res) {
		if (res.val()) {
			var b = 0;
			var arr = _.map(res.val(), function (ob, key) {
				ob.id = key;
				return ob;
			});
			for (var i = 0; i < arr.length; i++) {
				if (arr[i].type == "deb") { b -= arr[i].value; }
				if (arr[i].type == "cre") { b += arr[i].value; }
			}
			cb(b);
		} else {
			cb(0);
		}
	});
}

global.createTask = function (task, cb) {
	firebase.database().ref("/accounts/" + account + "/tasks").push(task).then(function (res) {
		task.id = res.key;
		if (cb) { cb(task); }
	}).catch(function (error) {
		console.log(error)
		console.log(task);
		if (cb) { cb(null); }
	});
}

global.isShopOpen = function () {
	var d = new Date();
	var strDate = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear()
	var day = d.getDay();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (config.holidays.indexOf(strDate) >= 0) {
		return false;
	} else {
		if (config.workDays[day]) {
			if (t >= config.officeOpen && t < config.officeClose) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}

global.makeShortURL = function (authStr, cb) {
	firebase.database().ref('/shorts/').push(authStr).then(function (res) {
		console.log("Short Pushed", res.key)
		cb(res.key)
	})
		.catch(function (error) {
			cb("err")
		});
}

global.loadLineObject = function (line, cb) {
	console.log("Loading line object", line)
	firebase.database().ref('/lines/' + line).once('value').then(function (res) {
		if (res.val()) {
			console.log("Loaded Line Object", res.key)
			lines[res.key] = res.val();
			lines[res.key].id = res.key;
			cb(true)
		} else {
			cb(false)
		}
	})
		.catch(function (error) {
			console.log(error)
			cb(false)
		});
}

global.getMediaB64 = function (lineOb, imgId, cb) {
	console.log(lineOb.apiURL + "getmediaurl/" + lineOb.id + "/" + imgId)
	fetch(lineOb.apiURL + "getmediaurl/" + lineOb.id + "/" + imgId, {
		method: 'get',
		headers: {}
	}).then(function (response) {
		if (response.status !== 200) {
			console.log(new Date(), 'Get Media API error', response.status);
			response.json().then(function (data) {
				console.log(data);
			});
			cb(false);
		} else {
			response.text().then(function (data) {
				console.log("Get Media response length", data.length)
				//console.log (data)
				if (data == "error") {
					cb(false)
				} else {
					cb(data)
				}
			});
		}
	}).catch(function (err) {
		console.log(new Date(), 'Get Media API request error', err.message);
		cb(false);
	});
}

global.mapaEstadoFirma = function (str, obs) {
	switch (str) {
		case "NUEVO":
			return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
		case "ASIGNADO":
			return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
		case "EN VALIDACION":
			return 'Tu firma ya se encuentra en nuestro sistema, por favor espera un momento un operador la aprobara pronto';
		case "RECHAZADO":
			return 'Tu firma no cumple con los requisitos para ser aprobada.'
		case "ELIMINADO":
			return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
		case "APROBADO":
			return 'Tu firma ya fue aprobada, por favor revisa tu correo electrónico.'
		case "EMITIDO (VALIDO)":
			return 'Tu firma ya ha sido descargada.'
		case "EMITIDO (SUSPENDIDO)":
			return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
		case "EMITIDO (REVOCADO)":
			return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
		case "EMITIDO (CADUCADO)":
			return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
		default:
			return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
	}
	//ACT SOLICITADA
	//Se necesita corrección de datos en tu solicitud, por favor revise tu correo electrónico 	
}
