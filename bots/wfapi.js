
module.exports = {

  getFlow: function (kw, account, cb) {
    fetch(envBot[account].flowAPIURL + '/flow-public/' + kw, {
      method: 'get',
      headers: { 'X-Nexxit-Key': envBot[account].flowAPIKey }
    }).then(function (response) {
      if (response.status !== 200) {
        console.log(new Date(), 'WF API getFlow error', response.status);
        response.json().then(function (data) {
          console.log(data);
          cb(false);
        });
        
      } else {
		console.log ("Flow get ok")
        response.text().then(function (data) {
          if (data.indexOf("<!DOCTYPE") == 0) {
            cb(false)
          } else {
            if (tools.isJsonString(data)) {
              cb(JSON.parse(data))
            } else {
              cb(false)
            }
          }
        });
      }
    })
  },

  searchFlows: function (kw, account, cb) {
    fetch(envBot[account].flowAPIURL + '/flows-public/' + kw, {
      method: 'get',
      headers: { 'X-Nexxit-Key': envBot[account].flowAPIKey }
    }).then(function (response) {
      if (response.status !== 200) {
        console.log(new Date(), 'WF API searchFlows error', response.status);
        response.json().then(function (data) {
          console.log(data);
          cb(false);
        });
        
      } else {
		console.log ("Flows search ok")
        response.text().then(function (data) {
          if (data.indexOf("<!DOCTYPE") == 0) {
            cb(false)
          } else {
            if (tools.isJsonString(data)) {
              cb(JSON.parse(data))
            } else {
              cb(false)
            }
          }
        });
      }
    })
  }

}
