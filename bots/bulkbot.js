var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "wapi-bulk";
var booted = false;
var jobs = []
var os = require('os');
var ignoreList = ['593983893607']

//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	//we catch MOs from bulk messages and forward to clients bots if required
	//we only process txt MOs
	//check ignore list
	if (ignoreList.indexOf(job.contact)<0) {
		if (job.msg.type == "txt") {
			//find contact in backroute
			//var rt = { clienteFinal: clienteFinal, to: post.to, tsc: Date.now() }
			console.log (new Date(), "Processing new job");
			firebase.database().ref("/backroute/").orderByChild("to").equalTo(job.contact).once('value', function(res) {
				console.log (new Date(), "Backroute search done");
				if (res.val()) {
					var arr = _.map(res.val(), function(val, key){
						val.id = key
						val.sentDate = new Date(val.tsc)
						return val
						})
					arr = _.sortBy(arr, function(ob) { return -ob.tsc });
					var br = arr[0];
					//who was the last client
					console.log ("Last backroute",br.clienteFinal,job.contact);
					switch (br.clienteFinal) {
						case "tventas": 
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								//var msg = "Estimado usuario, por favor escriba directamente al Whatsapp de TVentas 0939220047 para solucionar cualquier requerimiento."
								var msg = "Estimado usuario, por favor cualquier requerimiento escriba directamente a nuestro Whatsapp de TVentas https://wa.link/kcqdvu"
								//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("tventas");
								pushText(msg, job.contact, job.line)
							}
							finishJob(job);	
							break;
						case "nova":
							var ignoreWords = ["gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "Para comunicarse con un Asesor de NOVA Ecuador, por favor haga click en el siguiente enlace de Whatsapp: https://wa.me/message/VYOY3P4CHDLDL1"
								//pushText(msg, job.contact, job.line)
							}

							if( ["ok"].indexOf(job.msg.m.toLowerCase())  >= 0){
								var msg = "Por favor haga click en el siguiente link, para recibir su cotización\n\n wa.link/kgmvhm"
								pushText(msg, job.contact, job.line);
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("nova");
							finishJob(job);
							break;
						case "novaCorp":
							var msg = "Por favor haga click en el siguiente link, para recibir su cotización\n\n wa.link/kgmvhm"
							pushText(msg, job.contact, job.line);
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("novaCorp");
							finishJob(job);
							break;
						case "mapia":
							var msg = "Para más información comuníquese por WhatsApp al link https://api.whatsapp.com/send?phone=593997084013"
							pushText(msg, job.contact, job.line);
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("mapia");
							finishJob(job);
							break;
						case "legalinea":
							var msg = "Estás a tan solo un paso de obtener tu firma electrónica. Aquí mismo, por whatsapp. Haz click en este enlace https://wa.me/593995945725"
							pushText(msg, job.contact, job.line);
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("legalinea");
							finishJob(job);
							break;
						case "asisken":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "*Gracias por responder.*\n*Aquí nuestros números de atención por WHATSAPP*\n\n📲 0997790096\n📲 0993236799\n📲 0993347976\n📲 0997790321\n\nTambién haciendo clic en el siguiente enlace👇👇👇\nhttp://bit.ly/ServicioDeAtencionAsisken\nle ayudamos con mas información por whatsapp"
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("asisken");
							finishJob(job);
							break;
						case "asiskenIG":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "*Gracias por comunicarte con ASISKEN, haz clic en este enlace http://bit.ly/5PasosASISKEN y descubre el proceso completo en nuestra cuenta oficial de Instagram*"
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("asiskenIG");
							finishJob(job);	
							break;
						case "asiskenMED":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "*Gracias por responder. Haciendo clic en el siguiente enlace http://bit.ly/AtencionASISKEN le ayudamos con más información por Whatsapp*"
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("asiskenMED");
							finishJob(job);	
							break;
						case "bwise":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "Por el momento este es un canal de comunicaciones uni-direccional. Pronto le podremos atender también por esta vía.\n\nSaludos,\nBusiness Wise."
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("bwise");
							finishJob(job);	
							break;
						case "marbox":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "Para comunicarse con MARBOX Pescadería y Marisquería! Pídelos a domicilio 0959053131 o en el siguiente link https://wa.me/593959053131"
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("marbox");
							finishJob(job);	
							break;
						case "cima":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "*Gracias. Somos CIMA BROKERS, clic aquí para atención personalizada 👉  http://bit.ly/SegurosParaCarros*"
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("cima");
							finishJob(job);	
							break;
						case "william":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "Por favor haga click en el link"
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("william");
							finishJob(job);	
							break;
						case "padelcity":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "Gracias por comunicarte con Padel City.\n"
								msg += "\n"
								msg += "Para mayor información sobre la promoción por favor haz click en el siguiente enlace\n"
								msg += "\n"
								msg += "https://wa.me/593987994235"
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("padelcity");
							finishJob(job);	
							break;
						case "loader":
							var parts = job.msg.m.split("|");
							firebase.database().ref("/accounts/"+client+"/tests/"+parts[0]).push( {line: job.line, tsr: job.ts, ref: parts[1], tss: Number(parts[2])} );
							finishJob(job);	
							break;	
						case "bkids":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "*BUSINESS KIDS ONLINE*\n\n\n";
								msg += "Síguenos en *Facebook*: https://www.facebook.com/BusinessKids-Quito-Ecuador-117538413384984\n\n";
								msg += "Síguenos en *Instagram*: https://www.instagram.com/businesskidsquitoec/\n\n";
								msg += "Para mayor información, escríbenos a nuestro *Whatsapp*: https://wa.link/cg9vre"
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("bkids");
							finishJob(job);	
							break;
						case "hughes":
							//create chat user for Hughes
							var user = { chn: 'wsp', id: job.contact, line: job.line, lu: Date.now() }
							var mo = { g:3, m: job.msg.m, rep: 'bot', t:"mo", ts: Date.now(), type: 'txt' };
							firebase.database().ref("/accounts/hughes/chats/"+job.contact).update(user).then(function() {
								firebase.database().ref("/accounts/hughes/conves/"+job.contact+"/msgs/"+Date.now()).set(mo).then(function() {
									//create botJob for Tventas
									var j = {
									   msg: job.msg,
									   contact: job.contact,
									   line: job.line,
									   st: 'new',
									   ts: Date.now(),
									   isBulk: true
									   }
									firebase.database().ref("/accounts/hughes/botJobs").push(j);
								});
							});
							finishJob(job);	
							break;
						case "CCcasas":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "Gracias por comunicarte con *Costa Centinela*. _Este es un mensaje automático._\n\nPara poder ser atendido por uno de nuestros asesores, por favor haz click en este enlace de Whatsapp: https://n9.cl/b3bi \n\nMuchas gracias."
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("CCcasas");
							finishJob(job);	
							break;
						case "CCclub":
							var ignoreWords = ["ok", "gracias", "bueno", "okey", "oki", "muchas gracias", "listo", "👍"]
							if ( ignoreWords.indexOf( job.msg.m.toLowerCase() ) < 0  ) {
								//send MT with autoresponder
								var msg = "Gracias por comunicarte con *Costa Centinela*. _Este es un mensaje automático._\n\nPara poder ser atendido por uno de nuestros asesores, por favor haz click en este enlace de Whatsapp: https://n9.cl/b3bi \n\nMuchas gracias."
								pushText(msg, job.contact, job.line)
							}
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("CCclub");
							finishJob(job);	
							break;
						case "Radisson":
							var msg = "Hola, gracias por interesarte en nuestro proyecto. Pronto nos comunicaremos con usted.\n\n";
							msg += "Nuestro celular 0995918196 y nuestro Whatsapp https://wa.me/593995918196 están a su disposición.";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("Radisson");
							finishJob(job);	
							break;
						case "amigui":
							var msg = "Gracias por tu interés, en este link http://wa.me/593998197180 puedes agendar tu cita y en este link https://amigui.com.ec/pages/vende-tu-ropa puedes saber más sobre el estado de las prendas que compramos y nuestra política de compras.";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("amigui");
							finishJob(job);	
							break;
						case "factureasy":
							var msg = "Gracias por tu interés, revisa más información en este enlace https://factureasy.com/?ref=WSPC1";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("factureasy");
							finishJob(job);	
							break;
						case "Melchiade":
							var msg = "¡Hola #coffeelover☕! 👋 Gracias por comunicarte con nosotros. Por favor, haz click en el enlace de abajo para poder atenderte mediante nuestro canal de WhatsApp.\n\n";
							msg += "👉 https://bit.ly/Melchiade_WA\n\n";
							msg += "Muchas Gracias😊.";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("Melchiade");
							finishJob(job);	
							break;	
						case "UISEK":
							var msg = "¡En la Universidad SEK garantizas tus prácticas médicas!\n\n";
							msg += "Nuestros graduados tienen un 94% de empleabilidad, aprendes en los mejores laboratorios del país con tecnología de punta y simuladores innovadores.  Te preparamos para Ser Mejor en el campo de la salud con grandes aportes a la sociedad.\n\n";
							msg += "𝐔𝐧𝐢𝐯𝐞𝐫𝐬𝐢𝐝𝐚𝐝 𝐒𝐄𝐊, la mejor universidad, para el mejor momento de tu vida.\n\n";
							msg += "Contáctanos, tenemos una 𝐁𝐄𝐂𝐀 𝐏𝐀𝐑𝐀 𝐓𝐈!\n\n";
							msg += "📲 Whatsapp: https://bit.ly/2Q6Sbcl\n\n";
							msg += "🌐 https://uisek.edu.ec/\n\n";
							msg += "📧 admisiones@uisek.edu.ec\n\n";
							var msg = "Para mayor información, favor comunícate a nuestro Whatsapp https://bit.ly/uisek-whatsapp";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("UISEK");
							finishJob(job);	
							break;
						case "UISEKSINRESPUESTA":
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("UISEK");
							finishJob(job);	
							break;
					    case "FARMAMIA":
					        var msg = "Gracias, hemos registrado tu respuesta";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("FARMAMIA");
							finishJob(job);	
							break;
						case "COSMERA":
					        var msg = "Para más información por favor ingresar al link https://wa.me/593984529853";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("COSMERA");
							finishJob(job);	
							break;
						case "YOUPL":
					        var msg = "Gracias, hemos registrado tu respuesta";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("YOUPL");
							finishJob(job);	
							break;
						case "DOMINIA":
					        var msg = "Gracias, hemos registrado tu respuesta";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("DOMINIA");
							finishJob(job);	
							break;
						case "otro":
							if (job.line=="593969891860") {
								var msg = "Recuerda que, si tienes alguna duda, puedes contactarnos al 1800 - EQUINOCCIAL (378466) o escribir a nuestro WhatsApp +593-96-2996666";
								pushText(msg, job.contact, job.line)
								//update cliente final for this contact
								firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("SESA");
							}
							finishJob(job);	
							break;
						case 'Montecarlo_Club_Residencial':
					        var msg = "Gracias, hemos registrado tu respuesta";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("Montecarlo_Club_Residencial");
							finishJob(job);	
							break;
						case 'MERAKI':
					        var msg = "¡Gracias por comunicarte con Meraki Club! ✨ Hemos recibido tu mensaje y nos pondremos en contacto contigo enseguida. 🕊️ ¡Esperamos poder ayudarte pronto!";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("MERAKI");
							finishJob(job);	
							break;
						case 'AUSTRO':
					        var msg = "¡Gracias por tu respuesta!";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("AUSTRO");
							finishJob(job);	
							break;
						case 'HLBEcuador':
					        var msg = "¡Gracias por tu respuesta!";
							pushText(msg, job.contact, job.line)
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set("HLBEcuador");
							finishJob(job);	
							break;
						default:
							//update cliente final for this contact
							firebase.database().ref("/accounts/"+client+"/chats/"+job.contact+"/clienteFinal").set(br.clienteFinal);
							finishJob(job);	
					}
				} else {
					finishJob(job);
				}
			});
		} else {
			console.log ("Not a TXT message")
			finishJob(job);
		}
	} else {
		console.log (job.contact,"is in ignoreList")
		finishJob(job);
	}
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(1000));
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		console.log (new Date(), "Job completed");
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}

//create job		

