// SOP Bot as module
module.exports = {
	bot: function (body, job, user, step, group, account) {
		console.log("Processing uanataca sop bot", step, group)
		switch (step) {
			case "0":
				if (config.whiteList.includes(user.id) || config.whiteListPiccolos.includes(user.id)) {
					updateUser({ step: "sop1" }, job.contact, user.line);
					pushText(mainMenu(true), job.contact, user.line);
				} else {
					distriapi.findByPhone(user.id).then((res) => {
						console.log(res) //TODO JAGM: quitar despues de las pruebas
						if (res.success) {
							updateUser({ partnerUid: res.data.uuid, partnerType: res.data.type }, job.contact, user.line);
							if (res.data.type == 'Correcamino') {
								setUserParam("step", "sop.correcaminos", job.contact, user.line);
								pushText(correcaminos(), job.contact, user.line);
							} else if (res.data.type == 'Tienda') {
								setUserParam("step", "sop.tienda", job.contact, user.line);
								pushText(tienda(), job.contact, user.line);
							} else if (res.data.type == 'Piccolo' && user.id == '593998131170') {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText(piccolosa(), job.contact, user.line);
							} else {
								updateUser({ step: "sop1" }, job.contact, user.line);
								pushText(mainMenu(true), job.contact, user.line);
							}
						} else {
							pushText('Tu número telefónico no tiene acceso a este chat.\nMayor información contacta a tu asesor.', job.contact, user.line);
							salir(job, user);
						}
					}).catch((err) => {
						console.error('Error verificando acceso:', err);
						pushText('Hubo un problema al verificar tu acceso. Inténtalo más tarde.', job.contact, user.line);
						salir(job, user);
					})
				}
				break;
			//end step 0
			case "sop1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					if (config.whiteList.includes(user.id)) {
						setUserParam("step", "sop1.1", job.contact, user.line);
						pushText(distri(), job.contact, user.line);
					} else if (config.whiteListPiccolos.includes(user.id)) {
						setUserParam("step", "sop1.1.piccolos", job.contact, user.line);
						pushText(piccolos(), job.contact, user.line);
					} else {
						pushText('Tu número telefónico no tiene acceso a este chat.\nMayor información contacta a tu asesor.', job.contact, user.line);
						salir(job, user);
					}
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "sop3.1", job.contact, user.line);
					pushText(oneShot(), job.contact, user.line);

				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "sop1.1":
				var kw = body.toLowerCase()
				var group = ""
				//reenvio de email de descarga automatico
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {

					var msg = "El mail de descarga se reenvía solo en status *APROBADO*\n\nIngrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop-rcorreo-descarga", job.contact, user.line);

					//cancelación automatica
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {

					var msg = "Solo en los siguientes status podrá cancelar una solicitud,\nescoja una opcion:\n\n1. Nuevo\n2. Aprobado\n3. Regresar al Menú anterior"
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop-cancel", job.contact, user.line);

					//reenvio de credenciales automatico
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {

					var msg = "Solo en status *EMITIDO* se puede reenviar credenciales:\n\nIngrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop1.1.1", job.contact, user.line);

					//Revocar
				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {

					setUserParam("step", "sop1.1.3", job.contact, user.line);
					pushText(revocar(), job.contact, user.line);

					//Estado solicitud
				} else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {

					var msg = "Ingrese el número de cédula o pasaporte, sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop-estado-solicitud", job.contact, user.line);

					//Contactar asesor
				} else if (["6", "6.", "seis"].indexOf(kw) >= 0) {

					if (isShopOpen()) {
						var msg = "Por favor indícanos cuál es tu requerimiento"
						updateUser({ step: "idle", status: "active", group: "sod", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "sod");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}
				} else if (["7", "7.", "siete"].indexOf(kw) >= 0) {

					// main menu
					setUserParam("step", "sop1", job.contact, user.line);
					pushText(mainMenu(false), job.contact, user.line);

				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;

			// !Reenvio de credenciales automatico
			//*Llega numero de cedula
			case "sop1.1.1":
				var kw = body.toLowerCase()
				pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
				updateUser({ step: "sop1.1.1-0", ced: kw }, job.contact, user.line);
				break

			//*Llega el tipo de perfil
			case "sop1.1.1-0":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					utapi.consultarFirmas(user.ced, '1', user.id).then((res) => {
						if (res.result) {
							pushText(fechasFirmas(res.firmas, 'Persona Natural', [], job, user), job.contact, user.line);
							updateUser({ step: "sop1.1.1-1", tipo_perfil: "1", perfil: 'Persona Natural' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-rcredenciales-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					utapi.consultarFirmas(user.ced, '2', user.id).then((res) => {
						if (res.result) {
							pushText(fechasFirmas(res.firmas, 'Representante Legal', [], job, user), job.contact, user.line);
							updateUser({ step: 'sop1.1.1-1', tipo_perfil: '2', perfil: 'Representante Legal' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-rcredenciales-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					utapi.consultarFirmas(user.ced, '3', user.id).then((res) => {
						if (res.result) {
							pushText(fechasFirmas(res.firmas, 'Miembro de empresa', [], job, user), job.contact, user.line);
							updateUser({ step: "sop1.1.1-1", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-rcredenciales-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//*llega la firma escogida
			case "sop1.1.1-1":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.firmas.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.firmas.length + 1) {
							// main menu
							setUserParam("step", "sop1", job.contact, user.line);
							pushText(mainMenu(false), job.contact, user.line);
						} else {
							//reenvio de credenciales
							utapi.reenvioCredenciales(user.ced, user.firmas[Number(kw) - 1], user.id).then((res) => {
								if (res.result) {
									//credenciales reenviadas
									setUserParam("step", "sop-rcredenciales-menu-final", job.contact, user.line);
									pushText(res.resultado + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
								} else {
									//resultado negativo para la cosulta
									pushText(res.resultado, job.contact, user.line);
									salir(job, user);
								}
							}).catch((err) => {
								console.log(err)
								pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
								salir(job, user);
							})
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			case 'sop-rcredenciales-wait':
				var kw = body.toLowerCase()
				//volver a ingresar cedula
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Ingrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop1.1.1", job.contact, user.line);
					//regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1.1", job.contact, user.line);
					pushText(distri(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			case 'sop-rcredenciales-menu-final':
				var kw = body.toLowerCase()
				//Regresar al menu principal
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1.1", job.contact, user.line);
					pushText(distri(), job.contact, user.line);
					//Salir
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//! Fin reenvio de credenciales automatico


			// !Cancelacion de solicitud
			//*LLega estado de la solicitud 1.Nuevo / 2.Aprobado / 3.Regresar al menu principal
			case "sop-cancel":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText("Ingrese el número de cédula o pasaporte sin guiones ni espacios", job.contact, user.line);
					updateUser({ step: "sop-cancel-0", statusSolicitud: "1" }, job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					pushText("Ingrese el número de cédula o pasaporte sin guiones ni espacios", job.contact, user.line);
					updateUser({ step: "sop-cancel-0", statusSolicitud: "3" }, job.contact, user.line);
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					// main menu
					setUserParam("step", "sop1", job.contact, user.line);
					pushText(mainMenu(false), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//*Llega numero de cedula
			case "sop-cancel-0":
				var ced = body.toLowerCase()
				utapi.consultarSolicitudes(ced, user.statusSolicitud, user.id).then((res) => {
					// console.log(res)
					if (res.result) {
						//si hay mas una solicitud no se pide el perfil
						if (res.solicitudes.length == 1) {
							pushText(fechasSolicitudes(res.solicitudes, [], job, user), job.contact, user.line);
							updateUser({ step: "sop-cancel-2" }, job.contact, user.line);

							// si hay mas de una solicitud se pide el perfil para solo mostrar dichas solicitudes
						} else {
							pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
							updateUser({ step: "sop-cancel-1", ced: ced }, job.contact, user.line);
						}
					} else {
						setUserParam("step", "sop-cancel-wait", job.contact, user.line);
						pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
					}
				}).catch((err) => {
					console.log(err)
				})
				break

			//*Llega el tipo de perfil
			case "sop-cancel-1":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) { // Persona natural
					utapi.consultarSolicitudes(user.ced, user.statusSolicitud, user.id).then((res) => {
						if (res.result) {
							if (res.solicitudes.some(item => item.tipo === 'PERSONA NATURAL')) {
								pushText(fechasSolicitudes2(res.solicitudes, 'Persona Natural', [], 0, job, user), job.contact, user.line);
								updateUser({ step: "sop-cancel-2", tipo_perfil: "1", perfil: 'Persona Natural' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-cancel-wait", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud con ese perfil\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-cancel-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) { //Representante legal
					utapi.consultarSolicitudes(user.ced, user.statusSolicitud, user.id).then((res) => {
						if (res.result) {
							if (res.solicitudes.some(item => item.tipo === 'REPRESENTANTE LEGAL')) {
								pushText(fechasSolicitudes2(res.solicitudes, 'Representante legal', [], 0, job, user), job.contact, user.line);
								updateUser({ step: 'sop-cancel-2', tipo_perfil: '2', perfil: 'Representante Legal' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-cancel-wait", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud con ese perfil\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-cancel-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) { //Miembro de empresa
					utapi.consultarSolicitudes(user.ced, user.statusSolicitud, user.id).then((res) => {
						if (res.result) {
							if (res.solicitudes.some(item => item.tipo === 'MIEMBRO DE EMPRESA')) {
								pushText(fechasSolicitudes2(res.solicitudes, 'Miembro de empresa', [], 0, job, user), job.contact, user.line);
								updateUser({ step: "sop-cancel-2", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-cancel-wait", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud con ese perfil\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-cancel-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//*llega la firma escogida
			case "sop-cancel-2":
				var kw = body.toLowerCase()
				var msg = ''
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.solicitudes.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.solicitudes.length + 1) {
							// main menu
							setUserParam("step", "sop1", job.contact, user.line);
							pushText(mainMenu(false), job.contact, user.line);
						} else {
							updateUser({ step: "sop-cancel-3", solicitudCancelar: user.solicitudes[Number(kw) - 1] }, job.contact, user.line);
							if (user.statusSolicitud == '1') {//Nuevo
								msg = 'Escoja un motivo para cancelar la solicitud:\n\n'
								msg += '1. No cumple requisitos\n'
								msg += '2. Duplicada\n'
								msg += '3. Cliente desiste solicitud\n'
								msg += '4. Otro\n'
							}
							if (user.statusSolicitud == '3') {//Aprobado
								msg = 'Escoja un motivo para cancelar la solicitud:\n\n'
								msg += '1. Correo Incorrecto\n'
								msg += '2. Celular incorrecto\n'
								msg += '3. Duplicada\n'
								msg += '4. Perfil Incorrecto\n'
								msg += '5. Vigencia incorrecta\n'
								msg += '6. Formato incorrecto\n'
								msg += '7. Otro\n'
							}
							pushText(msg, job.contact, user.line);
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			//En este paso se cancela luego de haber escogido el motivo de cancelacion
			case 'sop-cancel-3':
				var kw = body.toLowerCase()
				if (user.statusSolicitud == '1') { //Nuevo
					if (["1", "2", "3", "4"].indexOf(kw) >= 0) {
						//cancelar solicitud
						utapi.cancelarSolicitud(user.solicitudCancelar, user.statusSolicitud, user.id).then((res) => {
							if (res.result) {
								//solicitud cancelada
								setUserParam("step", "sop-cancel-menu-final", job.contact, user.line);
								pushText(res.message + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
							} else {
								//resultado negativo para la consulta
								pushText(res.message, job.contact, user.line);
								salir(job, user);
							}
						}).catch((err) => {
							console.log(err)
							pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
							salir(job, user);
						})
					} else {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					}
				}

				if (user.statusSolicitud == '3') { //Aprobado
					if (["1", "2", "3", "4", "5", "6", "7"].indexOf(kw) >= 0) {
						//cancelar solicitud
						utapi.cancelarSolicitud(user.solicitudCancelar, user.statusSolicitud, user.id).then((res) => {
							if (res.result) {
								//solicitud cancelada
								setUserParam("step", "sop-cancel-menu-final", job.contact, user.line);
								pushText(res.message + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
							} else {
								//resultado negativo para la consulta
								pushText(res.message, job.contact, user.line);
								salir(job, user);
							}
						}).catch((err) => {
							console.log(err)
							pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
							salir(job, user);
						})
					} else {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					}
				}
				break

			case 'sop-cancel-wait':
				var kw = body.toLowerCase()
				//volver a ingresar cedula
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Ingrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop-cancel-0", job.contact, user.line);
					//regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1.1", job.contact, user.line);
					pushText(distri(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			case 'sop-cancel-menu-final':
				var kw = body.toLowerCase()
				//Regresar al menu principal
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1.1", job.contact, user.line);
					pushText(distri(), job.contact, user.line);
					//Salir
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//! Fin cancelacion de solicitud

			//! Reenvio de correo de descarga
			//*Llega numero de cedula
			case 'sop-rcorreo-descarga':
				var kw = body.toLowerCase()
				pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
				updateUser({ step: "sop-rcorreo-descarga-0", ced: kw }, job.contact, user.line);
				break

			case 'sop-rcorreo-descarga-0':
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					utapi.consultarFirmasAprobadas(user.ced, user.id, '1').then((res) => {
						if (res.result) {
							pushText(fechasFirmas2(res.firmas, 'Persona Natural', [], job, user), job.contact, user.line);
							updateUser({ step: "sop-rcorreo-descarga-1", tipo_perfil: "1", perfil: 'Persona Natural' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-rcorreo-descarga-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					utapi.consultarFirmasAprobadas(user.ced, user.id, '2').then((res) => {
						if (res.result) {
							pushText(fechasFirmas2(res.firmas, 'Representante Legal', [], job, user), job.contact, user.line);
							updateUser({ step: 'sop-rcorreo-descarga-1', tipo_perfil: '2', perfil: 'Representante Legal' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-rcorreo-descarga-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					utapi.consultarFirmasAprobadas(user.ced, user.id, '3').then((res) => {
						if (res.result) {
							pushText(fechasFirmas2(res.firmas, 'Miembro de empresa', [], job, user), job.contact, user.line);
							updateUser({ step: "sop-rcorreo-descarga-1", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-rcorreo-descarga-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break


			case "sop-rcorreo-descarga-1":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.firmas.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.firmas.length + 1) {
							// main menu
							setUserParam("step", "sop1", job.contact, user.line);
							pushText(mainMenu(false), job.contact, user.line);
						} else {
							//reenvio de mail de descarga
							utapi.reenvioCorreoDescarga(user.firmas[Number(kw) - 1], user.id).then((res) => {
								if (res.result) {
									//email de descarga reenviado
									setUserParam("step", "sop-rcorreo-descarga-menu-final", job.contact, user.line);
									pushText(res.resultado + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
								} else {
									//resultado negativo para la consulta
									pushText(res.resultado, job.contact, user.line);
									salir(job, user);
								}
							}).catch((err) => {
								console.log(err)
								pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
								salir(job, user);
							})
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			case 'sop-rcorreo-descarga-wait':
				var kw = body.toLowerCase()
				//volver a ingresar cedula
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "El mail de descarga se reenvía solo en status *APROBADO*\n\nIngrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop-rcorreo-descarga", job.contact, user.line);
					//regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1.1", job.contact, user.line);
					pushText(distri(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			case 'sop-rcorreo-descarga-menu-final':
				var kw = body.toLowerCase()
				//Regresar al menu principal
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1.1", job.contact, user.line);
					pushText(distri(), job.contact, user.line);
					//Salir
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//! Fin reenvio de correo de descarga

			//!consultar estado de solicitud
			//*Llega numero de cedula
			case 'sop-estado-solicitud':
				var kw = body.toLowerCase()
				pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
				updateUser({ step: "sop-estado-solicitud-0", ced: kw }, job.contact, user.line);
				break

			case 'sop-estado-solicitud-0':
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					utapi.consultarSolicitudesConEstado(user.ced, user.id, '1').then((res) => {
						console.log(res.result)
						if (res.result) {
							pushText(fechasSolicitudesConEstado(res.solicitudes, [], job, user, 0), job.contact, user.line);
							updateUser({ step: "sop-estado-solicitud-1", tipo_perfil: "1", perfil: 'Persona Natural' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-estado-solicitud-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					utapi.consultarSolicitudesConEstado(user.ced, user.id, '2').then((res) => {
						if (res.result) {
							pushText(fechasSolicitudesConEstado(res.solicitudes, [], job, user, 0), job.contact, user.line);
							updateUser({ step: 'sop-estado-solicitud-1', tipo_perfil: '2', perfil: 'Representante Legal' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-estado-solicitud-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					utapi.consultarSolicitudesConEstado(user.ced, user.id, '3').then((res) => {
						if (res.result) {
							pushText(fechasSolicitudesConEstado(res.solicitudes, [], job, user, 0), job.contact, user.line);
							updateUser({ step: "sop-estado-solicitud-1", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
						} else {
							setUserParam("step", "sop-estado-solicitud-wait", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break


			case "sop-estado-solicitud-1":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.solicitudesConEstado.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.solicitudesConEstado.length + 1) {
							// main menu
							setUserParam("step", "sop1", job.contact, user.line);
							pushText(mainMenu(false), job.contact, user.line);
						} else {
							utapi.consultarSolicitudesConEstado(user.ced, user.id, user.tipo_perfil).then((res) => {
								if (res.result) {
									const ob = res.solicitudes.find(ob => ob.token_uid === user.solicitudesConEstado[Number(kw) - 1]);
									pushText(`El status del cliente ${user.ced} con perfil ${ob.tipo} creado el ${Object.values(ob)[0]} es: ${ob.estado}` + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
									setUserParam("step", "sop-estado-solicitud-menu-final", job.contact, user.line);
								} else {
									setUserParam("step", "sop-estado-solicitud-wait", job.contact, user.line);
									pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
								}
							}).catch((err) => {
								console.log(err)
							})
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			case 'sop-estado-solicitud-wait':
				var kw = body.toLowerCase()
				//volver a ingresar cedula
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Ingrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop-estado-solicitud", job.contact, user.line);
					//regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1.1", job.contact, user.line);
					pushText(distri(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			case 'sop-estado-solicitud-menu-final':
				var kw = body.toLowerCase()
				//Regresar al menu principal
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1.1", job.contact, user.line);
					pushText(distri(), job.contact, user.line);
					//Salir
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//!Fin consultar estado de solicitud

			case "sop1.1.3":
				//REVOCAR
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText("En el siguiente link puede descargar el formato y enviar firmado por el titular con firma electrónica o manuscrita mediante correo a info@uanataca.ec\n\nhttps://uanataca.ec/descargas/SOLICITUD_REVOCATORIA_UT.docx", job.contact, user.line);
					salir(job, user);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					// main menu
					setUserParam("step", "sop1", job.contact, user.line);
					pushText(mainMenu(false), job.contact, user.line);
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					// salir
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;

			case "sop3.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					if (isShopOpen()) {
						var msg = "Por favor coméntanos como podemos ayudarte, en un momento un asistente se comunicará contigo\n";
						updateUser({ step: "idle", status: "active", group: "sos", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "sos");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}

				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					if (isShopOpen()) {
						var msg = "Por favor coméntanos como podemos ayudarte, en un momento un asistente se comunicará contigo\n";
						updateUser({ step: "idle", status: "active", group: "sos", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "sos");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					if (isShopOpen()) {
						var msg = "Por favor coméntanos como podemos ayudarte, en un momento un asistente se comunicará contigo\n";
						updateUser({ step: "idle", status: "active", group: "sos", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "sos");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}
				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
					setUserParam("step", "sop1", job.contact, user.line);
					pushText(mainMenu(false), job.contact, user.line);
				} else {

					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;

			case "go-back":
				var kw = body.toLowerCase()
				var group = ""
				if (["salir", "*salir*"].indexOf(kw) >= 0) {
					salir(job, user);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					setUserParam("step", "sop1", job.contact, user.line);
					pushText(mainMenu(false), job.contact, user.line);
				}
				break;

			//!Inicio flujo Piccolos
			case "sop1.1.piccolos":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {

					//reenvio de credenciales original
					if (isShopOpen()) {
						pushText('Por favor ingresa el número de cédula y el email del solicitante', job.contact, user.line);
						updateUser({ step: "idle", status: "active", group: "pic", tso: Date.now() }, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "pic");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}

				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {

					//cancelación
					setUserParam("step", "sop1.1.2.piccolos", job.contact, user.line);
					pushText(estadoMenu(), job.contact, user.line);

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {

					//revocar
					setUserParam("step", "sop1.1.3.piccolos", job.contact, user.line);
					pushText(estadoMenu(), job.contact, user.line);

				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {

					//estado solicitud
					var msg = "Por favor ingresa el numero de cédula de la solicitud que quieres revisar?\n\n"
					setUserParam("step", "sop1.1.4.piccolos", job.contact, user.line);

					pushText(msg, job.contact, user.line);
				} else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {

					//hablar operador
					if (isShopOpen()) {
						var msg = "Por favor indícanos cuál es tu requerimiento"
						updateUser({ step: "idle", status: "active", group: "pic", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "pic");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}
				} else if (["6", "6.", "seis"].indexOf(kw) >= 0) {

					// main menu
					setUserParam("step", "sop1", job.contact, user.line);
					pushText(mainMenu(false), job.contact, user.line);

				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;

			case "sop1.1.2.piccolos":
				//cancelar
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Si el estado de la solicitud es NUEVO, se puede cancelar la solicitud en su portal dentro del menú de solicitudes. \n\n"
					setUserParam("step", "sop1", job.contact, user.line);
					pushText(msg + mainMenu(false), job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					if (isShopOpen()) {
						var msg = "Por favor ayúdanos con la fecha de la solicitud y el número de cédula, la cancelación se hará lo más pronto posible \n";
						updateUser({ step: "idle", status: "active", group: "pic", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "pic");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					if (isShopOpen()) {
						var msg = "Por favor ayúdanos con la fecha de la solicitud y el número de cédula, la revocación se hará lo más pronto posible \n";
						updateUser({ step: "idle", status: "active", group: "pic", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "pic");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;

			case "sop1.1.3.piccolos":
				//REVOCAR
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Si el estado de la solicitud es NUEVO, se puede cancelar la solicitud en su portal dentro del menú de solicitudes. \n\n"
					setUserParam("step", "sop1", job.contact, user.line);
					pushText(msg + mainMenu(false), job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					if (isShopOpen()) {
						var msg = "Por favor ayúdanos con la fecha de la solicitud y el número de cédula, la cancelación se hará lo más pronto posible\n";
						updateUser({ step: "idle", status: "active", group: "pic", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "pic");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					if (isShopOpen()) {
						var msg = "Por favor ingresar el número de cédula del solicitante, la fecha de emisión del certificado y las razones por las cuales se está requiriendo esta revocación\n";
						updateUser({ step: "idle", status: "active", group: "pic", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "pic");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;

			case "sop1.1.4.piccolos":
				//consulta estado solicitud en el API
				var kw = body.toLowerCase()
				utapi.checkStatus(kw.substring(0, 13), function (arr) {
					if (arr) {
						if (arr.length == 1) {
							var reg = arr[0]
						} else {
							//sort and use last
							arr = _.sortBy(arr, function (item) { return item.fecha_registro });
							var reg = arr[arr.length - 1]
						}
						msg = "El estado de tu solicitud es *" + reg.estado + "*\nObservaciones: _" + mapaEstadoFirma(reg.estado.toUpperCase(), reg.observacion) + "_"
						setUserParam("step", "sop1", job.contact, user.line);
						pushText(msg + "\n\n" + mainMenu(false), job.contact, user.line);
					} else {
						setUserParam("step", "sop1", job.contact, user.line);
						pushText("No hemos podido validar tu solicitud con el número de cédula ingresado\n\n" + mainMenu(false), job.contact, user.line);
					}
				});
				break

			//!Inicio flujo piccolos Automatico
			case "sop.piccolos":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					//Reenvio de email de descarga
					setUserParam("step", "sop.piccolos.1", job.contact, user.line);
					pushText('El mail de descarga se reenvía solo en status *APROBADO*\n\nIngrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					//Cancelación de una solicitud
					setUserParam("step", "sop.piccolos.2", job.contact, user.line);
					pushText('Solo en los siguientes status podrá cancelar una solicitud, escoja una opción:\n\n1. Nuevo\n2. En validación\n3. Aprobado\n4. Regresar al menú anterior', job.contact, user.line);
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					//Reenvio de credenciales
					setUserParam("step", "sop.piccolos.3", job.contact, user.line);
					pushText('Solo en status *EMITIDO* se puede reenviar credenciales:\n\nIngrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
					//Revocar una solicitud
					pushText('Para revocar una solicitud puede seguir dos pasos:\n- El titular puede revocar en línea mediante el mail que recibió de Uanataca luego de descargar la firma electrónica en la opción "REVOCAR" (Servicio 24/7)\n- Enviar la carta de solicitud de revocatoria firmada por el cliente por mail.\n\n1. Descargar formato de revocatoria\n2. Regresar al menu principal\n3. Salir', job.contact, user.line);
					setUserParam("step", "sop.piccolos.4", job.contact, user.line);
				} else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
					//Consultar estado de solicitud
					setUserParam("step", "sop.piccolos.5", job.contact, user.line);
					pushText('Ingrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
				} else if (["6", "6.", "seis"].indexOf(kw) >= 0) {
					//consultar Saldo Uanacredits
					distriapi.getBalance(user.partnerUid).then((res) => {
						console.log(res)
						if (res.success) {
							pushText(`Tu saldo en este momento es de: $${res.data.balance.toFixed(2)}\n\nRecuerda siempre recargar con anticipación para que tus solicitudes sean procesadas de manera inmediata`, job.contact, user.line);
							salir(job, user);
						} else {
							pushText("Ha ocurrido un error, inténtelo de nuevo más tarde", job.contact, user.line);
							salir(job, user);
						}
					}).catch((err) => {
						console.log(err)
						pushText("Ha ocurrido un error, inténtelo de nuevo más tarde", job.contact, user.line);
						salir(job, user);
					})
				} else if (["7", "7.", "siete"].indexOf(kw) >= 0) {
					//Verificar devolucion de uanacredits
					setUserParam("step", "sop.piccolos.7", job.contact, user.line);
					pushText('Ingrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
				} else if (["8", "8.", "ocho"].indexOf(kw) >= 0) {
					//Tiempo de acreditacion de una recarga
					pushText('Una vez realizada la recarga, dependerá de la forma de pago:\n1. Tarjeta de crédito/débito: Inmediato\n2. Depósito y Transferencia: Una vez cargado el comprobante en el módulo, el pago será validado por el área de contabilidad en máximo 1 hora laborable.\n\nTener en cuenta que si son transferencias interbancarias, dependerá del tiempo de acreditación y convenio que mantengan entre entidades financieras.', job.contact, user.line);
					salir(job, user);
				} else if (["9", "9.", "nueve"].indexOf(kw) >= 0) {
					//Tiempo de devolucion de uanacredits
					pushText('Una vez cancelada o rechazada una solicitud, los créditos son devueltos de manera automática e inmediata', job.contact, user.line);
					salir(job, user);
				} else if (["10", "10.", "diez"].indexOf(kw) >= 0) {
					//Contactar con un asesor
					if (isShopOpen()) {
						var msg = "Por favor indícanos cuál es tu requerimiento"
						updateUser({ step: "idle", status: "active", group: "pic", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "pic");
					} else {
						pushText(config.horarioStr, job.contact, user.line);
						setUserParam("step", "0", job.contact, user.line);
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//*Inicio reenvio de correo de descarga piccolos
			//llega el numero de cedula
			case "sop.piccolos.1":
				var kw = body.toLowerCase()
				updateUser({ step: "sop.piccolos.1.1", ced: kw }, job.contact, user.line);
				pushText('Escoja el tipo de perfil:\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa', job.contact, user.line);
				break

			//llega el tipo de perfil
			case "sop.piccolos.1.1":
				var kw = body.toLowerCase()
				//Persona Natural
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('APPROVED')) &&
									valores.some(valor => valor.includes('Persona natural'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.1.2", tipo_perfil: "1", perfil: 'Persona natural' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-cdescarga", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
					//Representante Legal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('APPROVED')) &&
									valores.some(valor => valor.includes('Representante legal'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.1.2", tipo_perfil: "2", perfil: 'Representante legal' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-cdescarga", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
					//Miembro de Empresa
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('APPROVED')) &&
									valores.some(valor => valor.includes('Miembro de empresa'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.1.2", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-cdescarga", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega la firma a reenviar
			case "sop.piccolos.1.2":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.tokensDistri.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.tokensDistri.length + 1) {
							// main menu
							setUserParam("step", "sop.piccolos", job.contact, user.line);
							pushText(piccolosa(), job.contact, user.line);
						} else {
							//reenvio de mail de descarga
							utapi.reenvioCorreoDescarga(user.tokensDistri[Number(kw) - 1], user.id).then((res) => {
								if (res.result) {
									//email de descarga reenviado
									setUserParam("step", "sop-piccolos-menu-final", job.contact, user.line);
									pushText(res.resultado + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
								} else {
									//resultado negativo para la consulta
									pushText(res.message, job.contact, user.line);
									salir(job, user);
								}
							}).catch((err) => {
								console.log(err)
								pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
								salir(job, user);
							})
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break


			case "sop-piccolos-wait-cdescarga":
				var kw = body.toLowerCase()
				//volver a ingresar cedula
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Ingrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop.piccolos.1", job.contact, user.line);
					//regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "sop.piccolos", job.contact, user.line);
					pushText(piccolosa(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//*Fin reenvio de correo de descarga piccolos

			//*Inicio cancelar una solicitud piccolos
			case "sop.piccolos.2":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					//Estado nuevo
					updateUser({ step: "sop.piccolos.2.1", statusRequest: 'NEW', statusSolicitud: "1" }, job.contact, user.line);
					pushText('Ingrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					//Estado en validacion
					updateUser({ step: "sop.piccolos.2.1", statusRequest: 'IN_VALIDATION', statusSolicitud: "2" }, job.contact, user.line);
					pushText('Ingrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					//Estado aprobado
					updateUser({ step: "sop.piccolos.2.1", statusRequest: 'APPROVED', statusSolicitud: "3" }, job.contact, user.line);
					pushText('Ingrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
					//Regresar al menu principal
					setUserParam("step", "sop.piccolos", job.contact, user.line);
					pushText(piccolosa(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			case "sop.piccolos.2.1":
				var kw = body.toLowerCase()
				updateUser({ step: "sop.piccolos.2.2", ced: kw }, job.contact, user.line);
				pushText('Escoja el tipo de perfil:\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa', job.contact, user.line);
				break

			case "sop.piccolos.2.2":
				var kw = body.toLowerCase()
				//Persona Natural
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes(user.statusRequest)) &&
									valores.some(valor => valor.includes('Persona natural'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.2.3", tipo_perfil: "1", perfil: 'Persona natural' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-cancelar", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
					//Representante Legal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes(user.statusRequest)) &&
									valores.some(valor => valor.includes('Representante legal'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.2.3", tipo_perfil: "2", perfil: 'Representante legal' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-cancelar", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
					//Miembro de Empresa
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes(user.statusRequest)) &&
									valores.some(valor => valor.includes('Miembro de empresa'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.2.3", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-cancelar", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			case "sop.piccolos.2.3":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.tokensDistri.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.tokensDistri.length + 1) {
							// main menu
							setUserParam("step", "sop.piccolos", job.contact, user.line);
							pushText(piccolosa(), job.contact, user.line);
						} else {
							updateUser({ step: "sop.piccolos.2.4", solicitudCancelar: user.tokensDistri[Number(kw) - 1] }, job.contact, user.line);
							if (user.statusSolicitud == '1' || user.statusSolicitud == '2') {//Nuevo o en validacion
								msg = 'Escoja un motivo para cancelar la solicitud:\n\n'
								msg += '1. No cumple requisitos\n'
								msg += '2. Duplicada\n'
								msg += '3. Cliente desiste solicitud\n'
								msg += '4. Otro\n'
							}
							if (user.statusSolicitud == '3') {//Aprobado
								msg = 'Escoja un motivo para cancelar la solicitud:\n\n'
								msg += '1. Correo Incorrecto\n'
								msg += '2. Celular incorrecto\n'
								msg += '3. Duplicada\n'
								msg += '4. Perfil Incorrecto\n'
								msg += '5. Vigencia incorrecta\n'
								msg += '6. Formato incorrecto\n'
								msg += '7. Otro\n'
							}
							pushText(msg, job.contact, user.line);
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break


			//En este paso se cancela luego de haber escogido el motivo de cancelacion
			case 'sop.piccolos.2.4':
				var kw = body.toLowerCase()
				if (user.statusSolicitud == '1' || user.statusSolicitud == '2') { //Nuevo o en validacion
					if (["1", "2", "3", "4"].indexOf(kw) >= 0) {
						//cancelar solicitud
						utapi.cancelarSolicitud(user.solicitudCancelar, user.statusSolicitud, user.id).then((res) => {
							if (res.result) {
								//solicitud cancelada
								setUserParam("step", "sop-piccolos-menu-final", job.contact, user.line);
								pushText(res.message + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
							} else {
								//resultado negativo para la consulta
								pushText(res.message, job.contact, user.line);
								salir(job, user);
							}
						}).catch((err) => {
							console.log(err)
							pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
							salir(job, user);
						})
					} else {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					}
				}

				if (user.statusSolicitud == '3') { //Aprobado
					if (["1", "2", "3", "4", "5", "6", "7"].indexOf(kw) >= 0) {
						//cancelar solicitud
						utapi.cancelarSolicitud(user.solicitudCancelar, user.statusSolicitud, user.id).then((res) => {
							if (res.result) {
								//solicitud cancelada
								setUserParam("step", "sop-piccolos-menu-final", job.contact, user.line);
								pushText(res.message + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
							} else {
								//resultado negativo para la consulta
								pushText(res.message, job.contact, user.line);
								salir(job, user);
							}
						}).catch((err) => {
							console.log(err)
							pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
							salir(job, user);
						})
					} else {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					}
				}
				break

			case "sop-piccolos-wait-cancelar":
				var kw = body.toLowerCase()
				//volver a ingresar cedula
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					setUserParam("step", "sop.piccolos.2", job.contact, user.line);
					pushText('Solo en los siguientes status podrá cancelar una solicitud, escoja una opción:\n\n1. Nuevo\n2. En validación\n3. Aprobado\n4. Regresar al menú anterior', job.contact, user.line);
					//regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "sop.piccolos", job.contact, user.line);
					pushText(piccolosa(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//*Fin cancelar una solicitud piccolos


			//*Inicio reenvio de credenciales piccolos
			case "sop.piccolos.3":
				var kw = body.toLowerCase()
				updateUser({ step: "sop.piccolos.3.1", ced: kw }, job.contact, user.line);
				pushText('Escoja el tipo de perfil:\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa', job.contact, user.line);
				break

			case "sop.piccolos.3.1":
				var kw = body.toLowerCase()
				//Persona Natural
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('ISSUED')) &&
									valores.some(valor => valor.includes('Persona natural'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.3.2", tipo_perfil: "1", perfil: 'Persona natural' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-rcredenciales", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
					//Representante Legal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('ISSUED')) &&
									valores.some(valor => valor.includes('Representante legal'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.3.2", tipo_perfil: "2", perfil: 'Representante legal' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-rcredenciales", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
					//Miembro de Empresa
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('ISSUED')) &&
									valores.some(valor => valor.includes('Miembro de empresa'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.3.2", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop.piccolos", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n" + piccolosa(), job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-rcredenciales", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break


			case "sop.piccolos.3.2":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.tokensDistri.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.tokensDistri.length + 1) {
							// main menu
							setUserParam("step", "sop.piccolos", job.contact, user.line);
							pushText(piccolosa(), job.contact, user.line);
						} else {
							//reenvio de credenciales
							utapi.reenvioCredencialesPiccolos(user.tokensDistri[Number(kw) - 1], user.id).then((res) => {
								if (res.result) {
									//credenciales reenviadas
									setUserParam("step", "sop-piccolos-menu-final", job.contact, user.line);
									pushText(res.resultado + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
								} else {
									//resultado negativo para la cosulta
									pushText(res.message, job.contact, user.line);
									salir(job, user);
								}
							}).catch((err) => {
								console.log(err)
								pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
								salir(job, user);
							})
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break


			case "sop-piccolos-wait-rcredenciales":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					//volver a ingresar cedula
					var msg = "Ingrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop.piccolos.3", job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					//regresar al menu principal
					setUserParam("step", "sop.piccolos", job.contact, user.line);
					pushText(piccolosa(), job.contact, user.line);
				}
				else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
			//*Fin reenvio de credenciales piccolos


			//*Inicio revocar una solicitud piccolos
			case "sop.piccolos.4":
				var kw = body.toLowerCase()
				//Descargar formato de revocatoria
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText('En el siguiente link puede descargar el formato y enviar firmado por el titular con firma electrónica o manuscrita mediante correo a info@uanataca.ec\n\nhttps://uanataca.ec/descargas/SOLICITUD_REVOCATORIA_UT.docx', job.contact, user.line);
					salir(job, user);
					//Regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					pushText(piccolosa(), job.contact, user.line);
					setUserParam("step", "sop.piccolos", job.contact, user.line);
					//Salir
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break
			//*Fin revocar una solicitud piccolos 


			//*Consultar estado de una solicitud piccolos
			//*Llega numero de cedula
			case 'sop.piccolos.5':
				var kw = body.toLowerCase()
				pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
				updateUser({ step: "sop.piccolos.5.1", ced: kw }, job.contact, user.line);
				break

			case 'sop.piccolos.5.1':
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					//Persona Natural
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('Persona natural'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.5.2", tipo_perfil: "1", perfil: 'Persona natural' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-piccolos-wait-esolicitud", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-esolicitud", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					//Representante Legal
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('Representante legal'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.5.2", tipo_perfil: "2", perfil: 'Representante legal' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-piccolos-wait-esolicitud", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-esolicitud", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					//Miembro de Empresa
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('Miembro de empresa'));
							});
							if (solicitudes.length > 0) {
								pushText(tokenSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.5.2", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-piccolos-wait-esolicitud", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-esolicitud", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break


			case "sop.piccolos.5.2":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.tokensDistri.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.tokensDistri.length + 1) {
							// main menu
							setUserParam("step", "sop.piccolos", job.contact, user.line);
							pushText(piccolosa(), job.contact, user.line);
						} else {
							distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
								if (res.success) {
									var solicitudes = res.data.requests.filter(obj => {
										var valores = Object.values(obj);
										return valores.some(valor => valor.includes(user.perfil));
									});
									if (solicitudes.length > 0) {
										const ob = solicitudes.find(ob => ob.token === user.tokensDistri[Number(kw) - 1]);
										pushText(`El status del cliente ${user.ced} con perfil ${ob.type} creado el ${ob.requestDate} es: ${ob.status.replace('NEW', 'NUEVA').replace('IN_VALIDATION', 'EN VALIDACION').replace('UPDATE_REQUESTED', 'ACTUALIZACION SOLICITADA').replace('UPDATED', 'ACTUALIZADA').replace('APPROVED', 'APROBADA').replace('REJECTED', 'RECHAZADA').replace('ISSUED', 'EMITIDA').replace('CANCELED', 'CANCELADA')}` + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
										setUserParam("step", "sop-piccolos-menu-final", job.contact, user.line);
									} else {
										setUserParam("step", "sop-piccolos-wait-esolicitud", job.contact, user.line);
										pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
									}
								} else {
									setUserParam("step", "sop-piccolos-wait-esolicitud", job.contact, user.line);
									pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
								}
							}).catch((err) => {
								console.log('ERR:', err);
								pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
								salir(job, user);
							})
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			case 'sop-piccolos-wait-esolicitud':
				var kw = body.toLowerCase()
				//volver a ingresar cedula
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					var msg = "Ingrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop.piccolos.5", job.contact, user.line);
					//regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "sop.piccolos", job.contact, user.line);
					pushText(piccolosa(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//*Fin consultar estado de una solicitud piccolos

			//*Consultar reverso uanacredits piccolos
			case 'sop.piccolos.7':
				var kw = body.toLowerCase()
				pushText("Escoja el tipo de perfil:\n\n1. Persona Natural\n2. Representante Legal\n3. Miembro de Empresa", job.contact, user.line);
				updateUser({ step: "sop.piccolos.7.1", ced: kw }, job.contact, user.line);
				break

			case 'sop.piccolos.7.1':
				var kw = body.toLowerCase()
				//Persona Natural
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('Persona natural'));
							});
							if (solicitudes.length > 0) {
								pushText(uidSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.7.2", tipo_perfil: "1", perfil: 'Persona natural' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-piccolos-wait-duanacredits", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line)
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-duanacredits", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
					//Representante Legal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('Representante legal'));
							});
							if (solicitudes.length > 0) {
								pushText(uidSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.7.2", tipo_perfil: "2", perfil: 'Representante legal' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-piccolos-wait-duanacredits", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-duanacredits", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
					//Miembro de Empresa
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					distriapi.findRequests(user.partnerUid, user.ced).then((res) => {
						if (res.success) {
							var solicitudes = res.data.requests.filter(obj => {
								var valores = Object.values(obj);
								return valores.some(valor => valor.includes('Miembro de empresa'));
							});
							if (solicitudes.length > 0) {
								pushText(uidSolicitudesDistri(solicitudes, [], job, user, 0), job.contact, user.line);
								updateUser({ step: "sop.piccolos.7.2", tipo_perfil: "3", perfil: 'Miembro de empresa' }, job.contact, user.line);
							} else {
								setUserParam("step", "sop-piccolos-wait-duanacredits", job.contact, user.line);
								pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
							}
						} else {
							setUserParam("step", "sop-piccolos-wait-duanacredits", job.contact, user.line);
							pushText("La cédula o pasaporte ingresada, no registra ninguna solicitud\n\n1. Ingresar nuevamente\n2. Regresar al menú principal", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('ERR:', err);
						pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break


			case "sop.piccolos.7.2":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.uidsDistri.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.uidsDistri.length + 1) {
							// main menu
							setUserParam("step", "sop.piccolos", job.contact, user.line);
							pushText(piccolosa(), job.contact, user.line);
						} else {
							//consultar devolucion uanacredits
							distriapi.findReverseUanacredits(user.uidsDistri[Number(kw) - 1]).then((res) => {
								if (res.success) {
									if (res.data.uanacredits.length > 0) {
										if (res.data.uanacredits.length == 1) {
											pushText(`La devolución de los uancredits del cliente ${user.ced} fue procesada el ${res.data.uanacredits[0].date}` + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line);
											setUserParam("step", "sop-piccolos-menu-final", job.contact, user.line);
										} else {
											var msg = ''
											for (let i = 0; i < res.data.uanacredits.length; i++) {
												msg += `La devolución de los uancredits del cliente ${user.ced} fue procesada el ${res.data.uanacredits[i].date}\n`
											}
											pushText(msg + '\n\n1. Regresar al menú principal\n2. Salir', job.contact, user.line)
											setUserParam("step", "sop-piccolos-menu-final", job.contact, user.line);
										}
									} else {
										pushText(`No existe solicitud de devolución de el cliente ${user.ced}`, job.contact, user.line);
										salir(job, user);
									}
								} else {
									pushText(`No existe solicitud de devolución de el cliente ${user.ced}`, job.contact, user.line);
									salir(job, user);
								}
							}).catch((err) => {
								console.log('ERR:', err);
								pushText('Ha ocurrido un error, inténtelo de nuevo más tarde', job.contact, user.line);
								salir(job, user);
							})
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break


			case "sop-piccolos-wait-duanacredits":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					//volver a ingresar cedula
					var msg = "Ingrese el número de cédula sin guiones ni espacios."
					pushText(msg, job.contact, user.line);
					setUserParam("step", "sop.piccolos.7", job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					//regresar al menu principal
					setUserParam("step", "sop.piccolos", job.contact, user.line);
					pushText(piccolosa(), job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//*Fin consultar reverso uanacredits piccolos

			case 'sop-piccolos-menu-final':
				var kw = body.toLowerCase()
				//Regresar al menu principal
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					setUserParam("step", "sop.piccolos", job.contact, user.line);
					pushText(piccolosa(), job.contact, user.line);
					//Salir
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break

			//!Fin flujo piccolos Automatico


			//!Fin flujo Piccolos

			//!Inicio flujo Correcaminos
			case "sop.correcaminos":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText('Digite la cédula', job.contact, user.line);
					setUserParam("step", "sop.correcaminos.1", job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					// pushText("Seleccione el formato:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "FORMATO", options: [{ id: "archivo", title: "Archivo" }, { id: "nube", title: "Nube" }, { id: "combo", title: "Combo" }, { id: "mp", title: "Menú principal" }] })
					// setUserParam("step", "sop.correcaminos.2", job.contact, user.line);


					distriapi.consultarPrecios().then((res) => {
						// console.log(res)
						if (res.result) {
							const preciosArchivo = res.precios.filter(obj => Object.values(obj).some(valor => valor.includes('ARCHIVO')));
							updateUser({ step: "sop.correcaminos.2.1", formatoVN: 'archivo', contenedorVN: "0" }, job.contact);
							pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + Object.values(preciosArchivo[0])[2] }, { id: "vigencia2", title: "2 años", desc: '$' + Object.values(preciosArchivo[1])[2] }, { id: "vigencia3", title: "7 días", desc: '$' + Object.values(preciosArchivo[2])[2] }, { id: "vigencia4", title: "30 días", desc: '$' + Object.values(preciosArchivo[3])[2] }, { id: "mp", title: "Menú Principal"}] })
						} else {
							pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
							salir(job, user);
						}
					}).catch((err) => {
						console.log(err)
						pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
						salir(job, user);
					})




				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					pushText(montoRecargaCyT(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
					distriapi.getBalance(user.partnerUid).then((res) => {
						console.log(res)
						if (res.success) {
							pushText(`Tu saldo actual es $${res.data.balance.toFixed(2)}\n\n1. Recargar\n2. Regresar al menú principal`, job.contact, user.line);
							setUserParam("step", "sop.correcaminos-wait", job.contact, user.line);
						} else {
							pushText("Ha ocurrido un error, inténtelo de nuevo más tarde", job.contact, user.line);
							salir(job, user);
						}
					}).catch((err) => {
						console.log(err)
					})
				} else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {
					pushText("Por favor ingrese el número de cédula del solicitante", job.contact, user.line);
					setUserParam("step", "sop.correcaminos.5", job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//*Captar Tienda
			//llega la cedula
			case "sop.correcaminos.1":
				var kw = body.toLowerCase()
				if (kw.length == 10 || kw.length == 13) {
					if (!user.payloadCT) { user.payloadCT = {} }
					user.payloadCT.cedula = kw
					if (kw.length == 13) { user.payloadCT.idType = 'R' }
					if (kw.length == 10) { user.payloadCT.idType = 'C' }
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
					updateUser({ step: "sop.correcaminos.1c", payloadCT: user.payloadCT }, job.contact, user.line);
				} else {
					pushText(`${kw} No parece un número de cédula o RUC correcto, por favor envíe su número de identificación nuevamente`, job.contact, user.line)
				}
				break

			//Confirmacion de la cedula
			case "sop.correcaminos.1c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Digite el nombre completo', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.1" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite la cédula', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega el nombre
			case "sop.correcaminos.1.1":
				var kw = body.toLowerCase()
				if ((kw.length < 5 || kw.length > 100)) {
					pushText("Por favor envía tu nombre completo", job.contact, user.line)
				} else {
					user.payloadCT.nombre = kw
					updateUser({ step: "sop.correcaminos.1.1c", payloadCT: user.payloadCT }, job.contact, user.line);
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
				}
				break

			//Confirmacion del nombre
			case "sop.correcaminos.1.1c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Digite el Email', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.2" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite el nombre completo', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.1" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega el email
			case "sop.correcaminos.1.2":
				var kw = body.toLowerCase()
				if (tools.isEmail(kw)) {
					user.payloadCT.email = kw
					updateUser({ step: "sop.correcaminos.1.2c", payloadCT: user.payloadCT }, job.contact, user.line);
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
				} else {
					pushText("Por favor revisa y envia tu correo electrónico correctamente", job.contact, user.line);
				}
				break

			//Confirmacion del email
			case "sop.correcaminos.1.2c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Digite el número de celular', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.3" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite el Email', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.2" }, job.contact, user.line);
				}
				break

			//llega el numero de celular
			case "sop.correcaminos.1.3":
				var kw = body.toLowerCase()
				if (kw.length != 10 || !kw.startsWith("09")) {
					pushText(`${kw} No parece un número de celular correcto, por favor envía tu celular nuevamente`, job.contact, user.line)
				} else {
					user.payloadCT.celular = kw
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
					updateUser({ step: "sop.correcaminos.1.3c", payloadCT: user.payloadCT }, job.contact, user.line);

				}
				break

			//Confirmacion del celular
			case "sop.correcaminos.1.3c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Digite el nombre de la tienda o local', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.4" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite el número de celular', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.3" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega el nombre de la tienda
			case "sop.correcaminos.1.4":
				var kw = body.toLowerCase()
				user.payloadCT.nombreTienda = kw
				pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
				updateUser({ step: "sop.correcaminos.1.4c", payloadCT: user.payloadCT }, job.contact, user.line);
				break

			//Confirmacion del nombre de la tienda
			case "sop.correcaminos.1.4c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Digite la ciudad', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.5" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite el nombre de la tienda o local', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.4" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega la ciudad
			case "sop.correcaminos.1.5":
				var kw = body.toLowerCase()
				user.payloadCT.ciudad = kw
				pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
				updateUser({ step: "sop.correcaminos.1.5c", payloadCT: user.payloadCT }, job.contact, user.line);
				break

			//Confirmacion de la ciudad
			case "sop.correcaminos.1.5c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Escribe la provincia', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.5.1" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite la ciudad', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.5" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega la provincia
			case "sop.correcaminos.1.5.1":
				var kw = body.toUpperCase().replace('BOLIVAR', 'BOLÍVAR').replace('CANAR', 'CAÑAR').replace('RIOS', 'RÍOS').replace('GALAPAGOS', 'GALÁPAGOS').replace('MANABI', 'MANABÍ').replace('TSACHILAS', 'TSÁCHILAS').replace('SUCUMBIOS', 'SUCUMBÍOS')
				if (config.provincias.indexOf(kw) >= 0) {
					user.payloadCT.provincia = kw
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
					updateUser({ step: "sop.correcaminos.1.5.1c", payloadCT: user.payloadCT }, job.contact, user.line);
				} else {
					pushText('Esa provincia no se encuentra registrada.\n\nEscribe la provincia', job.contact, user.line)
				}
				break

			//Confirmacion de la provincia
			case "sop.correcaminos.1.5.1c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Escribe la dirección del comercio', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.6" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Escribe la provincia', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.5.1" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break


			//llega la direccion
			case "sop.correcaminos.1.6":
				var kw = body.toLowerCase()
				if (kw.length < 10 || kw.length > 200) {
					pushText("Por favor envía la dirección completa del comercio", job.contact, user.line)
				} else {
					user.payloadCT.direccion = kw
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
					updateUser({ step: "sop.correcaminos.1.6c", payloadCT: user.payloadCT }, job.contact, user.line);
				}
				break


			//Confirmacion de la direccion
			case "sop.correcaminos.1.6c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Comparte la ubicación del comercio', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.7" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Escribe la dirección del comercio', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.1.6" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega la ubicacion
			case "sop.correcaminos.1.7":
				//TODO: ver como llega la ubicacion y guardar en el payload (lat, long)
				var kw = body.toLowerCase()
				distriapi.createStore(user.partnerUid, user.payloadCT.idType, user.payloadCT.cedula, user.payloadCT.nombre, user.payloadCT.celular, user.payloadCT.email, user.payloadCT.direccion, user.payloadCT.nombreTienda, user.payloadCT.ciudad, user.payloadCT.provincia).then((res) => {
					console.log(res)
					if (res.success) {
						pushText('Tienda creada exitosamente', job.contact, user.line);
						salir(job, user);
					} else {
						var msg = ''
						if (res.error.message) { msg = res.error.message }
						if (res.error.messages) { msg = res.error.messages[0] }
						pushText(msg, job.contact, user.line)
						salir(job, user);
					}
				}).catch((err) => {
					console.log(err)
				})
				break

			//*Fin Captar Tienda


			//*Venta Nueva
			//llega el formato
			// case "sop.correcaminos.2":
			// 	var kw = body.toLowerCase()
			// 	console.log('KW', kw)
			// 	distriapi.consultarPrecios().then((res) => {
			// 		console.log(res)
			// 		if (res.result) {
			// 			if (kw == 'archivo') {
			// 				const preciosArchivo = res.precios.filter(obj => Object.values(obj).some(valor => valor.includes('ARCHIVO')));
			// 				updateUser({ step: "sop.correcaminos.2.1", formatoVN: 'archivo', contenedorVN: "0" }, job.contact);
			// 				pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + Object.values(preciosArchivo[0])[2] }, { id: "vigencia2", title: "2 años", desc: '$' + Object.values(preciosArchivo[1])[2] }, { id: "vigencia3", title: "7 días", desc: '$' + Object.values(preciosArchivo[2])[2] }, { id: "vigencia4", title: "30 días", desc: '$' + Object.values(preciosArchivo[3])[2] }] })
			// 			} else if (kw == 'nube') {
			// 				const preciosNube = res.precios.filter(obj => Object.values(obj).some(valor => valor.includes('EN NUBE')));
			// 				updateUser({ step: "sop.correcaminos.2.1", formatoVN: 'nube', contenedorVN: "2" }, job.contact);
			// 				pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + Object.values(preciosNube[0])[2] }, { id: "vigencia2", title: "2 años", desc: '$' + Object.values(preciosNube[1])[2] }, { id: "vigencia3", title: "3 años", desc: '$' + Object.values(preciosNube[2])[2] }, { id: "vigencia4", title: "4 años", desc: '$' + Object.values(preciosNube[3])[2] }] })
			// 			} else if (kw == 'combo') {
			// 				const preciosCombo = res.precios.filter(obj => Object.values(obj).some(valor => valor.includes('COMBO p12+nube')));
			// 				updateUser({ step: "sop.correcaminos.2.1", formatoVN: 'combo', contenedorVN: "3" }, job.contact);
			// 				pushText("*Tiempo de vigencia*\nSelecciona una opción:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "TIEMPO DE VIGENCIA", options: [{ id: "vigencia1", title: "1 año", desc: '$' + Object.values(preciosCombo[0])[2] }, { id: "vigencia2", title: "2 años", desc: '$' + Object.values(preciosCombo[1])[2] }, { id: "vigencia3", title: "3 años", desc: '$' + Object.values(preciosCombo[2])[2] }, { id: "vigencia4", title: "4 años", desc: '$' + Object.values(preciosCombo[3])[2] }] })
			// 			} else if (kw == 'mp') {
			// 				pushText(correcaminos(), job.contact, user.line);
			// 				setUserParam("step", "sop.correcaminos", job.contact, user.line);
			// 			} else {
			// 				pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
			// 			}
			// 		} else {
			// 			pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
			// 			salir(job, user);
			// 		}
			// 	}).catch((err) => {
			// 		console.log(err)
			// 		pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
			// 		salir(job, user);
			// 	})
			// 	break

			//llega la vigencia
			case "sop.correcaminos.2.1":
				var kw = body.toLowerCase()
				distriapi.getBalance(user.partnerUid).then((response) => {
					// console.log(response)
					if (response.success) {
						let balance = response.data.balance;

						distriapi.consultarPrecios().then((res) => {
							const preciosArchivo = res.precios.filter(obj => Object.values(obj).some(valor => valor.includes('ARCHIVO')));
							// const preciosNube = res.precios.filter(obj => Object.values(obj).some(valor => valor.includes('EN NUBE')));
							// const preciosCombo = res.precios.filter(obj => Object.values(obj).some(valor => valor.includes('COMBO p12+nube')));
							let precio = 0
							if (kw == 'vigencia1') {

								if (user.formatoVN == 'archivo') { precio = parseFloat(Object.values(preciosArchivo[0])[2]) }
								// if (user.formatoVN == 'nube') { precio = parseFloat(Object.values(preciosNube[0])[2]) }
								// if (user.formatoVN == 'combo') { precio = parseFloat(Object.values(preciosCombo[0])[2]) }
								updateUser({ validezVN: '1 año' }, job.contact, user.line);
							} else if (kw == 'vigencia2') {

								if (user.formatoVN == 'archivo') { precio = parseFloat(Object.values(preciosArchivo[1])[2]) }
								// if (user.formatoVN == 'nube') { precio = parseFloat(Object.values(preciosNube[1])[2]) }
								// if (user.formatoVN == 'combo') { precio = parseFloat(Object.values(preciosCombo[1])[2]) }
								updateUser({ validezVN: '2 años' }, job.contact, user.line);
							} else if (kw == 'vigencia3') {

								if (user.formatoVN == 'archivo') { precio = parseFloat(Object.values(preciosArchivo[2])[2]) }
								// if (user.formatoVN == 'nube') { precio = parseFloat(Object.values(preciosNube[2])[2]) }
								// if (user.formatoVN == 'combo') { precio = parseFloat(Object.values(preciosCombo[2])[2]) }
								updateUser({ validezVN: '7 días' }, job.contact, user.line);
							} else if (kw == 'vigencia4') {

								if (user.formatoVN == 'archivo') { precio = parseFloat(Object.values(preciosArchivo[3])[2]) }
								// if (user.formatoVN == 'nube') { precio = parseFloat(Object.values(preciosNube[3])[2]) }
								// if (user.formatoVN == 'combo') { precio = parseFloat(Object.values(preciosCombo[3])[2]) }
								updateUser({ validezVN: '30 días' }, job.contact, user.line);
							} else if (kw == 'mp') {
								pushText(correcaminos(), job.contact, user.line);
								setUserParam("step", "sop.correcaminos", job.contact, user.line);
								return;
							} else {
								pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
								return;
							}


							//control precio-balance
							if (balance >= precio) {
								updateUser({ step: "sop.correcaminos.2.2", precioVenta: precio }, job.contact);
								pushText('Digite cédula que va a obtener firma electrónica', job.contact, user.line);
							} else {
								updateUser({ step: "sop.correcaminos.2.2i" }, job.contact);
								pushText('Su saldo es insuficiente:\n\n1. Recargar\n2. Regresar al menú principal', job.contact, user.line)
							}

							//catch de consultar precios
						}).catch((err) => {
							console.log(err)
							pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
							salir(job, user);
						})
					} else {
						pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
						salir(job, user);
					}

					//catch de getBalance
				}).catch((err) => {
					console.log(err)
					pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
					salir(job, user);
				})
				break

			//llega la cedula
			case "sop.correcaminos.2.2":
				var kw = body.toLowerCase()
				if (kw.length != 10) {
					pushText(`${kw} no parece una cédula correcta, vuelva a ingresar el número de cédula.`, job.contact, user.line);
				} else {
					updateUser({ step: "sop.correcaminos.2.2c", cedVN: kw }, job.contact, user.line);
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
				}
				break

			//confirmacion de la cedula
			case "sop.correcaminos.2.2c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Digite nombre completo', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.2.3" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite cédula que va a obtener firma electrónica', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.2.2" }, job.contact, user.line);
				}
				break

			//llega el nombre
			case "sop.correcaminos.2.3":
				var kw = body.toLowerCase()
				if (kw.length < 5 || kw.length > 100) {
					pushText("Por favor envía el nombre completo", job.contact, user.line)
				} else {
					updateUser({ step: "sop.correcaminos.2.3c", nombreVN: kw }, job.contact, user.line);
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
				}
				break

			//confirmacion del nombre
			case "sop.correcaminos.2.3c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Digite mail', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.2.4" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite nombre completo', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.2.3" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega el mail
			case "sop.correcaminos.2.4":
				var kw = body.toLowerCase()
				if (tools.isEmail(kw)) {
					updateUser({ step: "sop.correcaminos.2.4c", mailVN: kw }, job.contact, user.line);
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
				} else {
					pushText("Por favor revisa y envia tu correo electrónico correctamente", job.contact, user.line);
				}
				break

			//confirmacion del mail
			case "sop.correcaminos.2.4c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					pushText('Digite el número de celular', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.2.5" }, job.contact, user.line);
				} else if (kw == 'no') {
					pushText('Digite mail', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.2.4" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//llega el numero de celular
			case "sop.correcaminos.2.5":
				var kw = body.toLowerCase()
				if (kw.length != 10 || !kw.startsWith("09")) {
					pushText(`${kw} No parece un número de celular correcto, por favor envía tu celular nuevamente`, job.contact, user.line)
				} else {
					updateUser({ step: "sop.correcaminos.2.5c", celVN: kw }, job.contact, user.line);
					pushText(`¿Este dato enviado es correcto: ${kw.toUpperCase()}?`, job.contact, user.line, null, "btn", [{ id: "si", title: "SI" }, { id: "no", title: "NO" }])
				}
				break

			//confirmacion del numero de celular
			case "sop.correcaminos.2.5c":
				var kw = body.toLowerCase()
				if (kw == 'si') {
					distriapi.generarLink(user.partnerUid, user.partnerType, user.cedVN, user.celVN, user.mailVN, user.validezVN, user.contenedorVN).then((res) => {
						// console.log('RES generar link',res)
						if (res.result) {
							distriapi.debit(user.partnerUid, user.precioVenta).then((response) => {
								if (response.success) {
									pushText(`Venta registrada. Comparte este link con el cliente para que pueda completar el formulario\n\n${res.link}`, job.contact, user.line);
									salir(job, user);
								} else {
									//TODO: NO SE DEBITO EL VALOR VOLVER AL INICIO DE VENTA
									console.log('NO SE DEBITO EL VALOR')
								}
							}).catch((err) => {
								console.log(err)
								//TODO: NO SE DEBITO EL VALOR VOLVER AL INICIO DE VENTA
								console.log('NO SE DEBITO EL VALOR AL INICIO DE LA VENTA')
							})

						} else {
							pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
							salir(job, user);
						}
					}).catch((err) => {
						console.log(err)
					})
				} else if (kw == 'no') {
					pushText('Digite el número de celular', job.contact, user.line);
					updateUser({ step: "sop.correcaminos.2.5" }, job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			case "sop.correcaminos.2.2i":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText(montoRecargaCyT(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					pushText(correcaminos(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos", job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break
			//*Fin Venta Nueva


			//*Recargar Saldo
			case "sop.correcaminos.3":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 10) {
						pushText("El monto mínimo de recarga es de $10\n\n" + montoRecargaCyT(), job.contact, user.line);
					} else {
						pushText(recargaCyT(), job.contact, user.line);
						updateUser({ step: "sop.correcaminos.3.1", valorRecargaC: kw }, job.contact, user.line);
					}
				} else {
					pushText("Por favor ingrese un valor numérico o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break


			//llega metodo de pago
			case "sop.correcaminos.3.1":
				var kw = body.toLowerCase()
				//De UNA
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					makeLinkDeuna(job, parseFloat(user.valorRecargaC), 0, function (dataDeuna) {
						// console.log('DATA DEUNA', dataDeuna)
						if (dataDeuna) {
							setUserParam('step', 'sop.correcaminos.3.deuna', job.contact, user.line)
							pushText(`Ingresa en el siguiente link para poder realizar el pago con tu APP DEUNA: \n\n${dataDeuna.deeplink}\n\nUna vez que validemos tu pago, seguiremos con el proceso.`, job.contact, user.line)
						} else {
							pushText('Ocurrió un error con el botón de pagos, por favor inténtalo nuevamente.\n\n' + montoRecargaCyT(), job.contact, user.line);
							setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
						}
					})

					//Tarjeta de crédito o débito
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					makeLink(job, parseFloat(user.valorRecargaC), 0, function (link) {
						if (link) {
							pushText("Utiliza el siguiente enlace para realizar tu pago:\n\n" + link, job.contact, user.line);
							updateUser({ step: "sop.correcaminos.3.payphone" }, job.contact);
						} else {
							pushText('Ocurrió un error con el botón de pagos, por favor inténtalo nuevamente.\n\n' + montoRecargaCyT(), job.contact, user.line);
							setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
						}
					})
					//Regresar al menu principal
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					if (user.partnerType == 'Correcamino') {
						pushText(correcaminos(), job.contact, user.line);
						setUserParam("step", "sop.correcaminos", job.contact, user.line);
					}
					if (user.partnerType == 'Tienda') {
						pushText(tienda(), job.contact, user.line);
						setUserParam("step", "sop.tienda", job.contact, user.line);
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//*Inicio pago DEUNA
			case 'sop.correcaminos.3.deuna':
				var kw = body.toLowerCase()
				updateUser({ step: "sop.correcaminos.3.deuna.1" }, job.contact);
				pushText(`Estamos esperando tu pago.\n\n¿Deseas cambiar el método de pago?`, job.contact, user.line, null, "btn", [{ id: "cambiar-metodo", title: "Cambiar método" }, { id: "esperar", title: "Pagar con DEUNA" }])
				break

			case 'sop.correcaminos.3.deuna.1':
				var kw = body.toLowerCase()
				if (kw === 'cambiar-metodo') {
					pushText(montoRecargaCyT(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
				} else if (kw === 'esperar') {
					updateUser({ step: "sop.correcaminos.3.deuna" }, job.contact);
					pushText("Puedes realizar el pago en el link que te enviamos anteriormente\n\nUna vez que validemos tu pago, seguiremos con el proceso.", job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break
			//*Fin PAGO deuna


			//*Inicio pago payphone
			case "sop.correcaminos.3.payphone":
				var kw = body.toLowerCase()
				updateUser({ step: "sop.correcaminos.3.payphone.1" }, job.contact);
				pushText(msgPayphone(), job.contact, user.line);
				break

			case "sop.correcaminos.3.payphone.1":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText(montoRecargaCyT(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					updateUser({ step: "sop.correcaminos.3.payphone" }, job.contact);
					pushText("Listo, esperemos a terminar el proceso de pago", job.contact, user.line);
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;

			case "sop.correcaminos.3.payphone.wait":
				var kw = body.toLowerCase()
				pushText("Un momento por favor, estamos validando tu pago", job.contact, user.line);
				break;
			//*Fin pago payphone

			//*Fin Recargar Saldo


			//*Consulta Saldo
			//Este proceso esta en el menu principal
			//*Fin Consulta Saldo

			//*Consultar el estado de una solicitud
			//llega el numero de cedula
			case "sop.correcaminos.5":
				var kw = body.toLowerCase()
				updateUser({ step: 'sop.correcaminos.5.1', cedulaMp: kw }, job.contact, user.line);
				pushText('Escoja el tipo de perfil:\n\n1.Persona Natural\n2. Representante Legal\n3. Miembro de Empresa', job.contact, user.line);
				break

			//llega el tipo de perfil
			case "sop.correcaminos.5.1":
				var kw = body.toLowerCase()
				//Persona Natural
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					distriapi.consultarEstadoSolicitudMp(user.cedulaMp, 'PERSONA NATURAL').then((res) => {
						console.log(res)
						if (res.result) {
							pushText(fechasSolicitudesMp(res.data.solicitudes, [], 0, job, user), job.contact, user.line);
							updateUser({ step: "sop.correcaminos.5.2" }, job.contact, user.line);
						} else {
							pushText(res.responce + '\n\n' + 'Ingrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
							setUserParam("step", "sop.correcaminos.5", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('Error: ', err)
						pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
						salir(job, user);
					})
					//Representante Legal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					distriapi.consultarEstadoSolicitudMp(user.cedulaMp, 'REPRESENTANTE LEGAL').then((res) => {
						console.log(res)
						if (res.result) {
							pushText(fechasSolicitudesMp(res.data.solicitudes, [], 0, job, user), job.contact, user.line);
							updateUser({ step: "sop.correcaminos.5.2" }, job.contact, user.line);
						} else {
							pushText(res.responce + '\n\n' + 'Ingrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
							setUserParam("step", "sop.correcaminos.5", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('Error: ', err)
						pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
						salir(job, user);
					})

					//Miembro de Empresa
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					distriapi.consultarEstadoSolicitudMp(user.cedulaMp, 'MIEMBRO DE EMPRESA').then((res) => {
						console.log(res)
						if (res.result) {
							pushText(fechasSolicitudesMp(res.data.solicitudes, [], 0, job, user), job.contact, user.line);
							updateUser({ step: "sop.correcaminos.5.2" }, job.contact, user.line);
						} else {
							pushText(res.responce + '\n\n' + 'Ingrese el número de cédula o pasaporte sin guiones ni espacios', job.contact, user.line);
							setUserParam("step", "sop.correcaminos.5", job.contact, user.line);
						}
					}).catch((err) => {
						console.log('Error: ', err)
						pushText('Ocurrio un error, intenta de nuevo más tarde.', job.contact, user.line)
						salir(job, user);
					})
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}


			//llega la firma para consultar el estado
			case "sop.correcaminos.5.2":
				var kw = body.toLowerCase()
				if (!isNaN(Number(kw))) {
					if (Number(kw) < 1 || Number(kw) > user.solicitudesMp.length + 1) {
						pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
					} else {
						if (Number(kw) == user.solicitudesMp.length + 1) {
							// main menu
							if (user.partnerType == 'Correcamino') {
								pushText(correcaminos(), job.contact, user.line);
								setUserParam("step", "sop.correcaminos", job.contact, user.line);
							}
							if (user.partnerType == 'Tienda') {
								pushText(tienda(), job.contact, user.line);
								setUserParam("step", "sop.tienda", job.contact, user.line);
							}
						} else {
							pushText(`El status de su firma creada el ${user.solicitudesMp[Number(kw) - 1].fecha} es: ${user.solicitudesMp[Number(kw) - 1].estado}\n\n1. Regresar al menú principal\n2. Salir`, job.contact, user.line);
							setUserParam("step", "sop.correcaminos-w", job.contact, user.line);
						}
					}
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//*Fin consultar el estado de una solicitud

			case "sop.correcaminos-wait":
				var kw = body.toLowerCase()
				//Recargar
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText(montoRecargaCyT(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
					//Regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					pushText(correcaminos(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos", job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			case "sop.correcaminos-w":
				var kw = body.toLowerCase()
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					// main menu
					if (user.partnerType == 'Correcamino') {
						pushText(correcaminos(), job.contact, user.line);
						setUserParam("step", "sop.correcaminos", job.contact, user.line);
					}
					if (user.partnerType == 'Tienda') {
						pushText(tienda(), job.contact, user.line);
						setUserParam("step", "sop.tienda", job.contact, user.line);
					}
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión.", job.contact, user.line);
				}
				break
			//!Fin flujo Correcaminos


			//!Inicio flujo Tienda
			case "sop.tienda":
				var kw = body.toLowerCase()
				//Venta nueva
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText("Seleccione el formato:", job.contact, user.line, null, "opt", { buttonCaption: "OPCIONES", title: "FORMATO", options: [{ id: "archivo", title: "Archivo" }, { id: "nube", title: "Nube" }, { id: "combo", title: "Combo" }, { id: "mp", title: "Menú principal" }] })
					setUserParam("step", "sop.correcaminos.2", job.contact, user.line);
					//Recargar saldo
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					pushText(montoRecargaCyT(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
					//Consulta saldo
				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
					distriapi.getBalance(user.partnerUid).then((res) => {
						console.log(res)
						if (res.success) {
							pushText(`Tu saldo actual es ${res.data.balance.toFixed(2)}\n\n1. Recargar\n2. Regresar al menú principal`, job.contact, user.line);
							setUserParam("step", "sop.tienda-wait", job.contact, user.line);
						} else {
							pushText("Ha ocurrido un error, inténtelo de nuevo más tarde", job.contact, user.line);
							salir(job, user);
						}
					}).catch((err) => {
						console.log(err)
					})
				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
					pushText("Por favor ingrese el número de cédula del solicitante", job.contact, user.line);
					setUserParam("step", "sop.correcaminos.5", job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break

			//*Venta Nueva
			//Este proceso es igual al de correcaminos
			//*Fin Venta Nueva


			//*Recargar Saldo
			//Este proceso es igual al de correcaminos
			//*Fin Recargar Saldo


			// //*Consulta Saldo
			// Este proceso esta en el menu principal
			//*Fin Consulta Saldo


			case "sop.tienda-wait":
				var kw = body.toLowerCase()
				//Recargar
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					pushText(recargaCyT(), job.contact, user.line);
					setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
					//Regresar al menu principal
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					pushText(tienda(), job.contact, user.line);
					setUserParam("step", "sop.tienda", job.contact, user.line);
				} else {
					pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break
			//!Fin flujo Tienda

		}//end switch step

		function mainMenu(greet) {
			if (greet) {
				var msg = "Gracias por comunicarte con el canal de soporte de UANATACA EC por favor selecciona la opción de tu interés, será un gusto poder ayudarte 🙂\n\n";
			} else {
				var msg = "Por favor selecciona la opción de tu interés, será un gusto poder ayudarte 🙂\n\n";
			}
			msg += "1. Distribuidores\n";
			msg += "2. OneShot\n";
			return msg;
		}

		function GoBackMenu() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Regresar al menú principal\n";
			msg += "2. Salir";
			return msg;
		}


		function oneShot() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. El documento no obtiene la firma OneShot\n";
			msg += "2. No tengo acceso al servidor\n";
			msg += "3. Necesito soporte directo\n";
			msg += "4. Regresar al menú anterior\n";
			return msg;
		}

		function piccolos() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Reenvío de credenciales\n";
			msg += "2. Cancelación de una solicitud\n";
			msg += "3. Revocar una solicitud\n";
			msg += "4. Cuál es el estado de una solicitud?\n";
			msg += "5. Necesito hablar con un operador\n";
			msg += "6. Regresas al menú anterior\n";
			return msg;
		}

		function piccolosa() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Reenvío email para descarga de firma\n";
			msg += "2. Cancelar una solicitud\n";
			msg += "3. Reenvío de credenciales\n";
			msg += "4. Revocar una solicitud\n";
			msg += "5. Cuál es el estado de una solicitud\n";
			msg += "6. Saldo Uanacredits\n";
			msg += "7. Verificar devolución de Uanacredits\n";
			msg += "8. Tiempo de acreditación de una recarga\n";
			msg += "9. Tiempo de devolución Uanacredits\n";
			msg += "10. Contactar con un asesor\n";
			return msg;
		}

		function distri() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Reenvío email para descarga de firma\n";
			msg += "2. Cancelar una solicitud\n";
			msg += "3. Reenvío de credenciales\n";
			msg += "4. Revocar una solicitud\n";
			msg += "5. Cuál es el estado de una solicitud?\n";
			msg += "6. Contactar con un asesor\n";
			msg += "7. Regresar al menú principal\n";
			return msg;
		}

		function correcaminos() {
			var msg = "Menú principal\n\n"
			msg += "1. Captar Tienda\n"
			msg += "2. Venta Nueva\n"
			msg += "3. Recargar Saldo\n"
			msg += "4. Consulta tu saldo\n"
			return msg;
		}

		function msgPayphone() {
			var msg = 'Aún está pendiente completar tu pago con PayPhone o es posible que haya expirado el link de pago, selecciona una opción:\n\n'
			msg += '1. Intentar nuevamente el pago\n'
			msg += '2. Esperar a completar el pago\n'
			msg += '3. Salir de la sesión (esta opción cancela tu proceso de solicitud)'
			return msg
		}

		function vigenciaCyT() {
			var msg = "Seleccione Vigencia:\n\n"
			msg += "1. Un año\n"
			msg += "2. Dos años\n"
			msg += "3. Tres años\n"
			msg += "4. Cuatro años\n"
			msg += "5. Regresar al menú principal\n"
			return msg;
		}

		function formatoCyT() {
			var msg = "Seleccione Formato\n\n"
			msg += "1. Archivo\n"
			msg += "2. Nube\n"
			msg += "3. Token\n"
			msg += "4. Combo\n"
			msg += "5. Regresar al menú principal\n"
			return msg;
		}

		function recargaCyT() {
			var msg = "Escoja como recargar:\n"
			msg += "1. De UNA\n"
			msg += "2. Tarjeta de crédito o débito\n"
			msg += "3. Regresar al menú principal\n"
			return msg;
		}

		function montoRecargaCyT() {
			var msg = "Ingrese el monto que desea recargar (en números), el valor mínimo de recarga es de $10:\n"
			return msg;
		}

		function tienda() {
			var msg = "Menú principal\n\n"
			msg += "1. Venta nueva\n"
			msg += "2. Recargar saldo\n"
			msg += "3. Consulta tu saldo\n"
			return msg;
		}

		function estadoMenu() {
			var msg = "El estado de la solicitud es:\n\n";
			msg += "1. Nuevo\n";
			msg += "2. Aprobado\n";
			msg += "3. Emitido\n";
			return msg;
		}

		function revocar() {
			var msg = "Para revocar una solicitud puede seguir dos pasos:\n";
			msg += '- El titular puede revocar en línea mediante el mail que recibió de Uanataca luego de descargar la firma electrónica en la opción "REVOCAR" (Servicio 24/7)\n';
			msg += "- Enviar la carta de solicitud de revocatoria firmada por el cliente por mail.\n";
			msg += "1. Descargar formato de revocatoria\n";
			msg += "2. Regresar al menu principal\n";
			msg += "3. Salir\n";
			return msg;
		}


		function fechasFirmas(firmas, perfil, arreglo, job, user) {
			var msg = `Escoja el certificado del que desea recuperar la clave en el perfil: ${perfil}\n\n`;

			firmas.map((p, index) => {
				for (let i = 0; i <= firmas.length; i++) {
					if (p[`firma_${i}`]) {
						msg += `${i}. ` + 'Aprobada: ' + p[`firma_${i}`].replace(/-/g, "/") + '\n';
						arreglo.push(p[`firma_${i}`])
						if (index === firmas.length - 1) {
							msg += `${i + 1}.` + ' Menú Principal';
						}
					}
				}
			})
			updateUser({ firmas: arreglo }, job.contact, user.line);

			return msg;
		}


		function fechasFirmas2(firmas, perfil, arreglo, job, user) {
			var msg = `Escoja el certificado del que desea reenviar el email de descarga en el perfil: ${perfil}\n\n`;

			firmas.map((p, index) => {
				for (let i = 0; i <= firmas.length; i++) {
					if (p[`solicitud_${i}`]) {
						msg += `${i}. ` + 'Creada: ' + p[`solicitud_${i}`].replace(/-/g, "/") + '\n';
						arreglo.push(p.uid)
						if (index === firmas.length - 1) {
							msg += `${i + 1}.` + ' Menú Principal';
						}
					}
				}
			})
			updateUser({ firmas: arreglo }, job.contact, user.line);

			return msg;
		}

		function fechasSolicitudes(solicitudes, arreglo, job, user) {
			var msg = `Escoja el certificado del que desea cancelar\n\n`;

			solicitudes.map((p, index) => {
				for (let i = 0; i <= solicitudes.length; i++) {
					if (p[i]) {
						msg += `${i}. ` + 'Creada: ' + p[i].replace(/-/g, "/") + '\n';
						arreglo.push(p.token_uid)
						if (index === solicitudes.length - 1) {
							msg += `${i + 1}.` + ' Menú Principal';
						}
					}
				}
			})
			updateUser({ solicitudes: arreglo }, job.contact, user.line);

			return msg;
		}

		function fechasSolicitudes2(solicitudes, perfil, arreglo, count, job, user) {
			var msg = `Escoja el certificado del que desea cancelar en el perfil: ${perfil}\n\n`;

			solicitudes.map((p, index) => {
				if (p.tipo === perfil.toUpperCase()) {
					msg += `${count + 1}. ` + 'Creada: ' + p[index + 1].replace(/-/g, "/") + '\n';
					arreglo.push(p.token_uid);
					count++;
				}
			});

			msg += `${count + 1}. Menú Principal\n`;

			updateUser({ solicitudes: arreglo }, job.contact, user.line);

			return msg;
		}

		function fechasSolicitudesConEstado(solicitudes, arreglo, job, user, count) {
			var msg = `Escoja el certificado que desea conocer el status por fecha de creación:\n\n`;

			solicitudes.map((p, index) => {
				msg += `${count + 1}. ` + 'Creada: ' + p[index + 1].replace(/-/g, "/") + '\n';
				arreglo.push(p.token_uid);
				count++;
			});

			msg += `${count + 1}. Menú Principal\n`;

			updateUser({ solicitudesConEstado: arreglo }, job.contact, user.line);

			return msg;
		}


		function fechasSolicitudesMp(solicitudes, arreglo, count, job, user) {
			var msg = `Escoja el certificado que desea conocer el status por fecha:\n`;

			solicitudes.map((p, index) => {
				if (p.tipo === perfil.toUpperCase()) {
					msg += `${count + 1}. ` + 'Creada: ' + p[fecha_registro].replace(/-/g, "/") + '\n';
					arreglo.push({ estado: p[estado], fecha: p[fecha_registro] });
					count++;
				}
			});

			msg += `${count + 1}. Menú Principal\n`;

			updateUser({ solicitudesMp: arreglo }, job.contact, user.line);

			return msg;
		}


		function tokenSolicitudesDistri(solicitudes, arreglo, job, user, count) {
			var msg = `Escoja el certificado por fecha de creación:\n\n`;

			solicitudes.map((p, index) => {
				msg += `${count + 1}. ` + 'Creada: ' + p.requestDate.replace(/-/g, "/") + '\n';
				arreglo.push(p.token);
				count++;
			});

			msg += `${count + 1}. Menú Principal\n`;

			updateUser({ tokensDistri: arreglo }, job.contact, user.line);

			return msg;
		}

		function uidSolicitudesDistri(solicitudes, arreglo, job, user, count) {
			var msg = `Escoja el certificado por fecha de creación:\n\n`;

			solicitudes.map((p, index) => {
				msg += `${count + 1}. ` + 'Creada: ' + p.requestDate.replace(/-/g, "/") + '\n';
				arreglo.push(p.uuid);
				count++;
			});

			msg += `${count + 1}. Menú Principal\n`;

			updateUser({ uidsDistri: arreglo }, job.contact, user.line);

			return msg;
		}

	},

	job: function (job, user, step, group, cb) {
		console.log("Entra un job de uanatacaPluginSoporte")
		switch (job.type) {
			case "cancel-pf-trans-sop":
				firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).once('value', function (resp) {
					console.log(resp.val())
					var tx = resp.val();
					if (tx) {
						if (tx.st == "new") {
							//ponemos la trans en cancel
							firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).update({ st: "cancel", tsu: Date.now() }); //todo: control if this fails
							//Le regresamos al step de pagar
							pushText('Has cancelado tu transacción de pago. Puedes intentar nuevamente.\n\n' + montoRecargaCyT(), job.contact, user.line);
							setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
							cb({ st: "done" })
						} else {
							console.log("Trans already confirmed");
							cb({ st: "done" });
						}
					} else {
						console.log("Trans not found", job.tid)
						cb({ st: "requeue", ts: Date.now() + requeueInt })
					}
				})
				break;


			case "confirm-pf-trans-sop":
				console.log('JOB confirm-pf-trans-sop en uanatacaPluginSoporte', job)
				//obtenemos la trans par validar si ya fue confirmada
				firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).once('value', function (resp) {
					console.log(resp.val())
					var tx = resp.val();
					if (tx) {
						if (tx.st == "new") {

							updateUser({ step: "sop.correcaminos.3.payphone.wait" }, job.contact);
							pushText("Un momento por favor mientras validamos tu pago", job.contact, user.line);
							confirmPayment(job.tid, job.payphoneId, function (res) {
								switch (res.status) {
									case 'fail':
										pushText('Ocurrió un error con el botón de pagos, por favor inténtalo nuevamente.\n\n' + montoRecargaCyT(), job.contact, user.line);
										setUserParam("step", "sop.correcaminos.3", job.contact, user.line);
										firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).update({ st: "cancel", tsu: Date.now() }); //todo: control if this fails
										cb({ st: "done" });
										break;
									case 'pending':
										//not ready
										cb({ st: "requeue", ts: Date.now() + requeueInt })
										break;
									case 'ok':
										//payment completed
										firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).update({ st: "ok", tsu: Date.now(), results: res.results, payphoneId: job.payphoneId }); //todo: control if this fails

										distriapi.recharge(user.partnerUid, parseFloat(user.valorRecargaC), 'CREDIT_CARD', job.payphoneId).then((res) => {
											if (res.success) {
												distriapi.getBalance(user.partnerUid).then((response) => {
													console.log(response)
													if (response.success) {
														if (user.partnerType === 'Correcamino') {
															pushText(`Recarga exitosa, su nuevo saldo es: ${response.data.balance}\n\n` + correcaminos(), job.contact, user.line);
															setUserParam("step", "sop.correcaminos", job.contact, user.line);
														}
														if (user.partnerType === 'Tienda') {
															pushText(`Recarga exitosa, su nuevo saldo es: ${response.data.balance}\n\n` + tienda(), job.contact, user.line);
															setUserParam("step", "sop.tienda", job.contact, user.line);
														}
													} else {
														//TODO JAGM: No se pudo consultar el saldo pero ya se hizo la recarga. Ver que hacer
													}
													//catch getBalance
												}).catch((err) => {
													console.log(err)
												})
											} else {
												//TODO JAGM: no se pudo hacer la recarga pero ya esta hecho el pago. Ver que hacer
											}
											//catch recharge
										}).catch((err) => {
											console.log(err)
										})

										cb({ st: "done" });
								}
							})
						} else {
							console.log("Trans already confirmed");
							cb({ st: "done" });
						}
					} else {
						console.log("Trans not found", job.tid)
						cb({ st: "requeue", ts: Date.now() + requeueInt })
					}
				})
				break;

			case 'confirm-deuna-trans-sop':

				console.log('JOB confirm DEUNA en uanatacaPluginSoporte', job)

				firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).once('value', function (resp) {
					console.log(resp.val())
					var trans = resp.val();
					if (trans) {
						//payment completed
						firebase.database().ref("/accounts/" + account + "/tx/" + job.tid).update({ st: "ok", tsu: Date.now(), results: job.tx });

						distriapi.recharge(user.partnerUid, parseFloat(user.valorRecargaC), 'DEUNA', job.tid).then((res) => {
							if (res.success) {
								distriapi.getBalance(user.partnerUid).then((response) => {
									console.log(response)
									if (response.success) {
										if (user.partnerType === 'Correcamino') {
											pushText(`Recarga exitosa, su nuevo saldo es: ${response.data.balance}\n\n` + correcaminos(), job.contact, user.line);
											setUserParam("step", "sop.correcaminos", job.contact, user.line);
										}
										if (user.partnerType === 'Tienda') {
											pushText(`Recarga exitosa, su nuevo saldo es: ${response.data.balance}\n\n` + tienda(), job.contact, user.line);
											setUserParam("step", "sop.tienda", job.contact, user.line);
										}
									} else {
										//TODO JAGM: No se pudo consultar el saldo pero ya se hizo la recarga. Ver que hacer
									}
									//catch getBalance
								}).catch((err) => {
									console.log(err)
								})
							} else {
								//TODO JAGM: no se pudo hacer la recarga pero ya esta hecho el pago. Ver que hacer
							}
							//catch recharge
						}).catch((err) => {
							console.log(err)
						})


						cb({ st: "done" });
					} else {
						console.log("Trans not found", job.tid)
						cb({ st: "requeue", ts: Date.now() + requeueInt })
					}

				})
				break

			default:
				cb({ st: "requeue", ts: Date.now() + requeueInt })
		}


		function montoRecargaCyT() {
			var msg = "Ingrese el monto que desea recargar (en números), el valor mínimo de recarga es de $10:\n"
			return msg;
		}

		function correcaminos() {
			var msg = "Menú principal\n\n"
			msg += "1. Captar Tienda\n"
			msg += "2. Venta Nueva\n"
			msg += "3. Recargar Saldo\n"
			msg += "4. Consulta tu saldo\n"
			msg += "5. Consulta estado de una solicitud\n"
			return msg;
		}

		function tienda() {
			var msg = "Menú principal\n\n"
			msg += "1. Venta nueva\n"
			msg += "2. Recargar saldo\n"
			msg += "3. Consulta tu saldo\n"
			msg += "4. Consulta estado de una solicitud\n"
			return msg;
		}
	}
}


//setTimeout(function() { makeLink({contact:'593984252217'}, 10, function(link) { console.log ("link", link) } ) } , 3000)

function makeLink(job, pvp, ivac, cb) {
	var precio = Math.round(pvp * 100) / 100
	var iva = 0
	if (ivac == null) {
		iva = config.iva
	} else {
		iva = ivac
	}
	var tax = Math.round((precio * iva) * 100) / 100

	var transId = job.contact + "_" + Date.now()
	var payload = {
		amountWithoutTax: Math.round(precio * 10000) / 100,
		tax: Math.round(tax * 10000) / 100,
		amount: Math.round((precio + tax) * 10000) / 100,
		clientTransactionId: transId,
		currency: "USD",
		Reference: "Pago " + envBot[account].name,
		storeId: envBot[account].pf_mp.store,
		responseUrl: envBot[account].TCAPIURL + 'paylink-ok-sop',
		cancellationUrl: envBot[account].TCAPIURL + 'paylink-cancel-sop'
	}

	payphone.prepareButton(payload, envBot[account].pf_mp.token, function (data) {
		if (data) {
			var tx = {
				vendor: 'pf',
				data: data,
				contact: job.contact,
				st: 'new',
				tsc: Date.now()
			}
			firebase.database().ref("/accounts/" + account + "/tx/" + transId).set(tx); //todo: control if this fails
			//cb (data.payWithCard)
			cb("https://coniski.com/payphone/pago.html?ref=" + data.paymentId)
		} else {
			cb(false)
		}
	});
}

function makeLinkDeuna(job, pvp, ivac, cb) {
	var precio = Math.round(pvp * 100) / 100
	var iva = 0
	if (ivac == null) {
		iva = config.iva
	} else {
		iva = ivac
	}
	var tax = Math.round((precio * iva) * 100) / 100
	var amount = precio + tax
	var transId = 'mp' + '_' + job.contact + "_" + generarValorAleatorio()


	deuna.linkPagoDEUNA(amount.toFixed(2), transId, 'RECARGA MULTIPLAZA').then(function (deunaResponse) {


		console.log('DEUNAAAA', deunaResponse)
		if (deunaResponse.status === '1') {
			//TODO JAGM: antes de guardar los datos de la transaccion borrar el qr por que esta en b64, creo que seria aqui el qr no es necesario para el bot, otra opcio es una vez realizado el pago borrarlo pero que pasa si no hacen el pago?
			var tx = {
				vendor: 'deuna',
				data: deunaResponse,
				contact: job.contact,
				st: 'new',
				tsc: Date.now()
			}
			firebase.database().ref("/accounts/" + account + "/tx/" + transId).set(tx)
			cb(deunaResponse)
		} else {
			cb(false)
		}
	})
}

function confirmPayment(transId, paymentId, cb) {
	var paymentOb = {
		id: paymentId,
		clientTxId: transId
	}

	payphone.confirmPayment(paymentOb, envBot[account].pf_mp.token, function (tx) {
		if (tx) {
			if (tx.statusCode == 2 || tx.statusCode == 3) {

				if (tx.statusCode == 2) { //pago cancelado o rechazado
					cb({ status: "fail" })
				}

				if (tx.statusCode == 3) { //pago completado
					cb({ status: "ok", results: tx })
				}
			} else {
				cb({ status: "pending" })
			}
		} else {
			cb({ status: "pending" })
		}
	});
}



function generarValorAleatorio() {
	const caracteresValidos = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	const longitud = 4;
	let valorAleatorio = '';

	for (let i = 0; i < longitud; i++) {
		const indiceAleatorio = Math.floor(Math.random() * caracteresValidos.length);
		valorAleatorio += caracteresValidos.charAt(indiceAleatorio);
	}

	return valorAleatorio;
}
