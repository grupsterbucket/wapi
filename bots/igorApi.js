module.exports = {
  autenticacion: async function () {
    var options = {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "contrato": envBot[account].contrato,
        "interfase": envBot[account].interfase,
        "usuario": envBot[account].userAPI,
        "password": envBot[account].pwdAPI
      })
    }

    const respuestaAuth = await fetch(envBot[account].URBAPARKAPIURL + '/login/authenticate', options)
    const respuestaFinalAuth = await respuestaAuth.text()

    if (tools.isJsonString(respuestaFinalAuth)) {
      return JSON.parse(respuestaFinalAuth)
    } else {
      return { status: 502 }
    }
  },

  consultarUsuario: async function (tipoIdentidad, numCedula, token) {
    var options = {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    }


    const respuestaUsuario = await fetch(envBot[account].URBAPARKAPIURL + `/admin/ConsultarUsuario?tipoIdentidad=${tipoIdentidad}&identidad=${numCedula}`, options)
    const respuestaFinalUsuario = await respuestaUsuario.text()


    if (tools.isJsonString(respuestaFinalUsuario)) {
      return JSON.parse(respuestaFinalUsuario)
    } else {
      return { status: 502 }
    }
  },

  consultarConsumo: async function (numCedula, token) {
    var options = {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    }

    const respuestaConsumo = await fetch(envBot[account].URBAPARKAPIURL + `/admin/ConsultarConsumos?tipoIdentidad=2&identidad=${numCedula}&numeroRegistros=3`, options)
    const respuestaFinalConsumo = await respuestaConsumo.text()

    if (tools.isJsonString(respuestaFinalConsumo)) {
      return JSON.parse(respuestaFinalConsumo)
    } else {
      return { status: 502 }
    }
  },

}