var QRCode = require('qrcode');
var QRReader = require('qrcode-reader');
var Jimp = require('jimp');
let env = require('./env.json'); 
var path = require('path');
module.exports = {
	scanImage: function(url, cb) {
		console.log ("Scan QR", url);
		Jimp.read(url)
		.then(image => {
			var qr = new QRReader();
			qr.callback = function(err, value) {
				if (err) {
					console.error(err);
					cb("error");
				} else { 
					if (value.result) {
						cb(value.result);
					} else {
						cb("error");		
					}
				}
			};
			qr.decode(image.bitmap);
		  })
		  .catch(err => {
			console.log (err)
			cb("error");
		  });
	},
	
	generateCodeFromStringToAWS: function(fname, str, cb) {
		QRCode.toDataURL(str, {
		  color: {
			dark: '#000',  // Blue dots
			light: '#FFF' // Transparent background
		  }
		}, function (err, uri) {
		  if (err) {
			  console.log(new Date(), 'ERROR QR CODE');
			  console.log (err)
			  cb (null);
		  } else {
			  console.log(new Date(), 'QR Code generated');
			    var parts = uri.split(";base64,");
				var contentType = parts[0].replace("data:","");
				aws.putObject("v3/cuponfi/qrs/" +fname, dataUriToBuffer(uri), contentType, function(res){
					if (res.status=="ok") {
						console.log (new Date(), "Media saved on AWS for", fname);
						cb("https://s3.amazonaws.com/wsp2crmcdn/v3/cuponfi/qrs/" +fname);
					} else {
						cb(null);
					}	
				});
		  }
		  
		});
	},
	
	generateCodeFromStringToFile: function(fname, str, cb) {
		QRCode.toFile(path.resolve(__dirname, '/usr/share/nginx/html/textcenter3/qrs/'+fname), str, {
		  color: {
			dark: '#000',  // Blue dots
			light: '#FFF' // Transparent background
		  }
		}, function (err) {
		  if (err) {
			  console.log(new Date(), 'ERROR QR CODE');
			  console.log (err)
			  cb (false);
		  } else {
			  console.log(new Date(), 'QR Code generated');
			  cb (true);
		  }
		  
		});
	},
	
	generateCodeFromStringToURI: function(str, cb) {
		QRCode.toDataURL(str, {
		  color: {
			dark: '#000',  // Blue dots
			light: '#FFF' // Transparent background
		  }
		}, function (err, uri) {
		  if (err) {
			  console.log(new Date(), 'ERROR QR CODE');
			  console.log (err)
			  cb (null);
		  } else {
			  console.log(new Date(), 'QR Code generated');
			  cb (uri);
		  }
		  
		});
	}
}
