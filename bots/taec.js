var request = require('request');
global.tools = require('./tools');
var fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
var plugin = require('./taecPlugin');
global._ = require('underscore');
global.firebase = require('firebase');
var CryptoJS = require("crypto-js");
var account = "taec";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 500;
global.config = {}
global.calendars = []
global.tipoVehs = [ 'Sedán', 'SUV', 'Camioneta', 'Hatchback', 'Van', 'Camión' ]
global.uid = "na"
//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        uid = user.uid
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		loadAccountConfig();
		setTimeout(function(){
			listenToJobs();
			processJobQueue();
			//setInterval(function() { getRepRoundRobin(function(rep){ console.log (rep) }) },1000)
		}, 2000);
	}	
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/'+account+'/config/misc').on('value', function(snapshot) {		  
		config = snapshot.val();
		console.log ("Config fetched for account",account);
	});
	firebase.database().ref('accounts/'+account+'/config/calendars').on('value', function(snapshot) {		  
		calendars = _.map(snapshot.val(), function(ob, key){
			ob.id = key;
			return ob;
		});
		console.log ("Calendars fetched for account",account);
	});
}
function listenToJobs() {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued", job.id);
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	}
}

function processJob(job) {
	switch (job.type) {
		case 'send-notification':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				pushText (job.str, job.contact, user.line);
				//job is completed
				finishJob(job);
			});
			break;
		case 'reset-bot':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				resetBot (job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
		case 'booking-completed':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				//return main menu to user
				plugin.bot("", job, user, "999", user.group, account);
				//job is completed
				finishJob(job);
			});
			break;
			
		default:
			if (job.contact == "593991307152") {
				//ignore
				finishJob(job);
				console.log ("bot bot convo ignored")
			} else {
				firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
					var user = snapshot.val();
					var step = "0";
					var lastStep = "0";
					var group = "main";
					if (user.step) {step = user.step};
					if (user.lastStep) {lastStep = user.lastStep};
					if (user.group) {group = user.group};
					
					if (user.tsso) {
						console.log ("User "+job.contact+" is at step:", step, "group:", group);
					} else {
						startSession (job.contact, user);	
						console.log ("New Session for "+job.contact+"");
					}
					
					if (step != 'idle') {
						var msgType = job.msg.type
						if (msgType == "chat") { msgType = "text"}
						if (msgType == "") { msgType = "text"}
						console.log ("Process MO ", msgType);		
							switch (msgType) {
								case 'text':
									if (job.msg.m) {
										var body = job.msg.m;
									} else {
										var body = job.msg.text.body;
									}
									
									console.log ("Body", body);
							
						
									if (body.length>0) {
										//trim spaces
										body = body.trim();

										//catch operations
										if (body.indexOf("#") == 0) {
											var parts = body.split(" ");
											//match direct operation
											switch ( parts[0] ) {
												case "#salir":
													salir(job, user);
													break;
												case "#ping":
													var str = "#pong "+ new Date();
													pushText (str, job.contact, user.line);
													break;
												case "#reservas":
													getReservas(job.contact, user.line, parts[1]);
													break;
												case "#server":
													var str = "Server data:\n";
													str += "Datetime: "+ new Date()+"\n";
													str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
													str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
													pushText (str, job.contact, user.line);
													break;
												default:
													var str = "Operación desconocida";
													pushText (str, job.contact, user.line);
											} //end switch parts operation
										} else {
											if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
												salir(job, user);
											} else {
	
												//bot is enabled?
												if (config.botEnabled) {
													//chat is not operation, run bot
													plugin.bot(body, job, user, step, group, account);
												} else {
													if (config.autoMessage) {
														pushText (config.autoMessage, job.contact, user.line);
													} else {
														pushText ("Servicio en mantenimiento", job.contact, user.line);
													}
												}
												
											}
										} //end if operation
									} //body is empty
									//job is completed
									finishJob(job);
									break;
								default:
									plugin.bot("["+job.msg.type+"]", job, user, step, group, account);
									//job is completed
									finishJob(job);
									break;
							} //end switch mo type
					} else { //end idle check
						console.log ('user '+job.contact+'@'+user.line+' is idle');
						//is a reset operation?
						if (job.msg.text) { 
							var body = job.msg.text.body;
							if (body.length>0) {
								//trim spaces
								body = body.trim();
								if (body.toLowerCase().indexOf("#") == 0) {
									if (body.toLowerCase() == "#reset") {
										resetBot(job.contact, user, "Reset Session OK");
									}
								}
							}
						}
						//job is completed
						finishJob(job);
					}
				});
			} //end bot bot
		}//end switch job type
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	});
}

global.pageBreak = function() {
	return '\n\n--------------------------oooo--------------------------\n\n'
	}

global.pushText = function  (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	firebase.database().ref("/accounts/"+account+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.getReservas = function(contact, line, kw) {
	if (kw) {
		getPropId(kw, function(prop) {
			if (prop) {
				//get future events for pid
				getFutureEvents(prop.id, function(evs) {
					if (evs) {
						var evHoy = ""
						var evMan = ""
						var evFut = ""
						var cHoy = 0;
						var cMan = 0;
						var cFut = 0;
						
						for (var i=0; i<evs.length; i++) {
							var ev = evs[i];
							if (ev.st != "del") {
								 var hoy = new Date();
								 hoy.setHours(11);
								 hoy.setMinutes(59);
								 var manana = new Date();
								 manana.setDate(hoy.getDate()+1);
								 manana.setHours(11);
								 manana.setMinutes(59);
								 var row = "<tr class='event-row'>";
								 //row += "<td>"+ev.ymd+"</td>";
								 row += "<td>"+formatHours(ev.timeStart)+" - "+formatHours(ev.timeEnd)+"</td>";
								 var cal =  _.findWhere(calendars, {id: ev.resource});
								 if (cal) {
									row += "<td>"+cal.caption+"</td>";
								 } else {
									row += "<td>"+ev.resource+"</td>"; 
								 }
								 row += "<td>"+ev.pax+"</td>";
								 if (ev.st == 'blk') {
									row += "<td>"+ev.notes+"</td>";
								 } else {
									row += "<td>"+ev.nick+"</td>";	 
								 }
								 //row += "<td>"+getEventStatusBadge(ev.st)+"</td>";
								 row += "</tr>";
								 if (ev.ymd == hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate()) {
									evHoy += row;
									cHoy += 1;
								 } else if (ev.ymd == manana.getFullYear()+"-"+(manana.getMonth()+1)+"-"+manana.getDate()) {
									evMan += row;
									cMan += 1;
								 } else if (ev.dateStart > manana.getTime()) {
									evFut += row;
									cFut += 1;
								 }
							}
						}
						
						if (cHoy>0) {
							var html = "<h5>Reporte Diario de Reservas</h5>"
							html += "Edificio: <strong>"+prop.name+"</strong><br>"
							html += "Fecha: <strong>"+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+"</strong><br><br>"
							html += "<table class='pdf-table'>"
							html += "<tr class='event-row-header'><td>HORA</td><td>ÁREA</td><td>PAX</td><td>CONTACTO</td></tr>"
							html += evHoy
							html += "</table>"
							//console.log (html)
							printPDF("Reporte Reservas Diarias", html, "reporte_reservas.pdf", contact, line, cHoy, hoy, prop.name);
						} else {
							pushText ("No existen reservas para hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear(), contact, line);
						}
					} else  {
						pushText ("No existen reservas", contact, line);
					}
				});	
			} else  {
				pushText ("Propiedad no encontrada "+prop, contact, line);
			}
		});
		
	} else {
		getUser(contact, function(user) {
			if (user) {
				var gs = user.group.split(",");
				//get future events for first group on array
				getFutureEvents(gs[0], function(evs) {
					if (evs) {
						var evHoy = ""
						var evMan = ""
						var evFut = ""
						var cHoy = 0;
						var cMan = 0;
						var cFut = 0;
						
						for (var i=0; i<evs.length; i++) {
							var ev = evs[i];
							if (ev.st != "del") {
								 var hoy = new Date();
								 hoy.setHours(11);
								 hoy.setMinutes(59);
								 var manana = new Date();
								 manana.setDate(hoy.getDate()+1);
								 manana.setHours(11);
								 manana.setMinutes(59);
								 var row = "<tr class='event-row'>";
								 //row += "<td>"+ev.ymd+"</td>";
								 row += "<td>"+formatHours(ev.timeStart)+" - "+formatHours(ev.timeEnd)+"</td>";
								 var cal =  _.findWhere(calendars, {id: ev.resource});
								 if (cal) {
									row += "<td>"+cal.caption+"</td>";
								 } else {
									row += "<td>"+ev.resource+"</td>"; 
								 }
								 row += "<td>"+ev.pax+"</td>";
								 if (ev.st == 'blk') {
									row += "<td>"+ev.notes+"</td>";
								 } else {
									row += "<td>"+ev.nick+"</td>";	 
								 }
								 //row += "<td>"+getEventStatusBadge(ev.st)+"</td>";
								 row += "</tr>";
								 if (ev.ymd == hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate()) {
									evHoy += row;
									cHoy += 1;
								 } else if (ev.ymd == manana.getFullYear()+"-"+(manana.getMonth()+1)+"-"+manana.getDate()) {
									evMan += row;
									cMan += 1;
								 } else if (ev.dateStart > manana.getTime()) {
									evFut += row;
									cFut += 1;
								 }
							}
						}
						
						if (cHoy>0) {
							var html = "<h5>Reporte Diario de Reservas</h5>"
							html += "Edificio: <strong>"+user.condo.name+"</strong><br>"
							html += "Fecha: <strong>"+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+"</strong><br><br>"
							html += "<table class='pdf-table'>"
							html += "<tr class='event-row-header'><td>HORA</td><td>ÁREA</td><td>PAX</td><td>CONTACTO</td></tr>"
							html += evHoy
							html += "</table>"
							//console.log (html)
							printPDF("Reporte Reservas Diarias", html, "reporte_reservas.pdf", contact, line, cHoy, hoy, user.condo.name);
						} else {
							pushText ("No existen reservas para hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear(), contact, line);
						}
					} else  {
						pushText ("No existen reservas", contact, line);
					}
				});
			} else {
				pushText ("Operación no autorizada", contact, line);
			}
		});
	}
}

global.getFutureEvents = function(gid, cb) {
	var d = new Date();
	d.setHours(0);
	d.setMinutes(0);
	var sAt = d.getTime();
	d.setDate(d.getDate()+3);
	var eAt = d.getTime();
	console.log ("Getting event for next 3 days", new Date(sAt), new Date(eAt), gid )
	firebase.database().ref('accounts/'+account+'/events/').orderByChild("dateStart")
	.startAt(sAt).endAt(eAt).once('value', function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			arr = _.sortBy(arr, function(ob, key) {
				return ob.dateStart;
			});
			console.log ("Found",arr.length,"total future events")
			var evs = _.where(arr, {group: gid});
			if (evs.length>0) {
				console.log ("Found",evs.length,"future events for group")
				cb(evs);
			} else {
				cb(null);
			}
		} else {
			cb(null);	
		}	
	});	
}

global.formatHours = function(n) {
	if (n<1000) {
		n+=""
		return n.substring(0,1) + "h" + n.substring(1,3);	
	} else {
		n+=""
		return n.substring(0,2) + "h" + n.substring(2,4);
	}
}

global.printPDF = function (title, body, fname, contact, line, cHoy, hoy, condoName) {
	var html = '<html><head><title>' + title + '</title>';
	html += '<meta http-equiv="content-type" content="text/html; charset=UTF-8" />';
	html += '<meta charset="utf-8" />';
	html += '<link rel="stylesheet" href="https://v3.textcenter.net/css/styles-pdf.css" type="text/css" />';
	html += "<style>@page {size: 21cm 29.7cm; margin: 5mm 5mm 5mm 5mm;}</style>";
	html += '</head><body>';
	html += body;
	html += '</body></html>';
	//console.log (html)
	var data = {
		html: html,
		fname: fname,
		orientation: "portrait"
	};
	
	var args = {
		'form': data,
		'headers': { 'Content-Type': 'application/json'},
		'url':'https://pedefer.com:7172/make-pdf-html'
		
	}
	request.post(args, function (err, response) {
		if (!err) {
			var ob = JSON.parse(response.body)
			
			if (ob.result == "success") {
				console.log ("PDF created OK")
				var segs = ob.msg.split("/");
				console.log ("Link de reservas", "https://cdn.pedefer.com"+"/"+segs[segs.length-1]);
				var msg = "Hay "+cHoy+" reserva(s) para  hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+" en "+condoName+"\n\nDescargar PDF: https://cdn.pedefer.com"+"/"+segs[segs.length-1]
				pushText (msg, contact, line);	
			} else {
				pushText ("Hay "+cHoy+" reserva(s) para hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+" en "+condoName+"\n\nError generando PDF", contact, line);	
				console.log ("Error Pedefer")
				console.log (ob)	
			}
		} else {
			console.log ("Error Pedefer", err)
			pushText ("Hay "+cHoy+" reserva(s) para hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+" en "+user.condo.name+"\n\nError generando PDF", contact, line);	
		}
	});
}

global.getEventStatusBadge = function (st) {
		var stName = st;
		 switch (st) {
			case "new":
				stName = "Por confirmar";
				break;
			case "acc":
				stName = "Confirmado";
				break;
			case "rec":
				stName = "Rechazado";
				break;
			case "del":
				stName = "Eliminado";
				break;
			case "fin":
				stName = "Finalizado";
			case "blk":
				stName = "Bloqueado";
				break;
		 }
		return '<span style="color: '+getEventStatusColor(st)+';">'+stName+'</span>';
}

global.getEventStatusColor = function(st) {
	 var color = "";
	 switch (st) {
		case "new":
			color = "#415663";
			break;
		case "acc":
			color = "#28a745";
			break;
		case "rec":
			color = "#dc3545";
			break;
		case "del":
			color = "#dc3545";
			break;
		case "fin":
			color = "#6dad7c";
			break;
		case "blk":
			color = "#28a745";
			break;
	 }
	return color;
}
  
global.setUserParam = function(key, val, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).set(val);
}

global.updateUser = function(ob, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob );
}

function startSession (contact, user) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/tsso").set( Date.now() );
	logStartSession(contact, user.chn);
}

global.logStartSession = function(contact, chn) {
	var log = {
		type: "session-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logEndSession = function(contact, chn, tsso, op) {
	var log = {
		type: "session-end",
		tssx: Date.now(),
		ref: contact+"",
		ts: Date.now(),
		op: op
	}
	if (tsso) { log.tsso = tsso; }
	if (chn) { log.chn = chn; }
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logStartTicket = function(contact, chn, user, group) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn,
		group: group
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
	//send notification to agents
	if ( config.notifyTo[group] ) {
		var sendTos = config.notifyTo[group].split(",");	
		for (var i=0; i<sendTos.length; i++) {
			var to = sendTos[i].trim();
			if (to.length == 10 || to.length == 12) {
				if (to.length==10) { to = to.replace("09","5939") }
				pushTemplate(["https://v3.textcenter.net/#inbox?c="+contact], "new_ticket_created", to, user.line);
			}
		}
	}
	
}

global.logAssignedTicket = function(contact, rep, group) {
	var log = {
		type: "ticket-assign",
		ts: Date.now(),
		ref: contact,
		chn: "wsp",
		rep: rep,
		group: group,
		createdBy: uid
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.pushTemplate = function  (params, templateName, contact, line) {
	
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:"Nuevo ticket en Textcenter", rep: "bot", t:"mt", ts: Date.now(), type: "txt", st:"qed", lu: Date.now()};
	mt.isTemplate = true	
	mt.lang = "es"; //todo read from DB
	mt.params = params.join("|");
	mt.templateName = templateName;
	firebase.database().ref("/accounts/"+account+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "Template Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.resetUserParam = function(key, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).remove();
}

global.saveForm = function(ob, cb) {
	firebase.database().ref("/accounts/"+account+"/forms").push(ob).then(function(res){
		cb(res.key);
	});
}

global.salir = function(job, user) {
	var self = this;
	logEndSession(job.contact, user.chn, user.tsso, "s");	
	var str = "Gracias por usar este servicio.\nAdios!"
	pushText (str, job.contact, user.line);
	resetUserParam('step', job.contact);
	resetUserParam('pin', job.contact);
	resetUserParam('group', job.contact);
	resetUserParam('account', job.contact);
	resetUserParam('tsso', job.contact);
	resetUserParam('x-nombre', job.contact);
	resetUserParam('x-tipo', job.contact);
	resetUserParam('x-presupuesto', job.contact);
	setUserParam('status', 'reset', job.contact);
	
}

global.resetBot = function(contact, user, msg) {
	logEndSession(contact, user.chn, user.tsso, "r");
	if (msg) { pushText (msg, contact, user.line); }
	resetUserParam('step', contact);
	resetUserParam('pin', contact);
	resetUserParam('group', contact);
	resetUserParam('account', contact);
	resetUserParam('tsso', contact);
	resetUserParam('x-nombre', contact);
	resetUserParam('x-tipo', contact);
	resetUserParam('x-presupuesto', contact);
	setUserParam('status', 'reset', contact);
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);	
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()	
}

global.getContact = function(phone, cb) {
	console.log ("Searching for",phone);
	firebase.database().ref("/accounts/"+account+"/contacts").orderByChild("phone").equalTo(phone+'').once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			console.log ("Contact found", arr.length);
			cb (arr[0])
		} else {
			cb (null);
		}
	}).catch(function(error) {
		console.log (error)
		cb (null);
	});
}

global.isShopOpen = function() {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (config.workDays[d.getDay()]) {
		if (t>=config.officeOpen && t<config.officeClose) {
			return true;
		} else {
			return false;		
		}	
	} else {
		return false;	
	}
}

global.getRepRoundRobin = function(cb) {
	console.log ("Load reps");
	firebase.database().ref("/users").orderByChild("account").equalTo(account).once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			var reps = _.where(arr, {role: 'agent', status: 'active'});
			if (reps.length>0) {
				var currentPointer = config.repPointer;
				var nextPointer = currentPointer += 1;
				if (reps[nextPointer]) {
					firebase.database().ref('accounts/'+account+'/config/misc/repPointer').set(nextPointer);
					console.log(reps[nextPointer].name, nextPointer)
					cb(reps[nextPointer].id);
				} else {
					//restart round
					firebase.database().ref('accounts/'+account+'/config/misc/repPointer').set(0);
					console.log(reps[0].name, 0)
					cb(reps[0].id);
				}
			} else {
				cb (null);
			}
		} else {
			cb (null);
		}
	}).catch(function(error) {
		console.log (error)
		cb (null);
	});
}
