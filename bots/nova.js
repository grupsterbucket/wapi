var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
let jsonData = require('../nova.json');
var client = "nova";
var booted = false;
var jobs = []
var zohoAccessToken = '';
const moment = require('moment-timezone');
moment.tz.setDefault('America/Guayaquil');

//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
		getZohoToken();
		setInterval(() => {
			getZohoToken();
		}, 50 * 60 * 1000);
        loginTofirebase();
    }
    // ...
});

function getZohoToken(){

	var zohoRefreshToken = '1000.4375c24ce3ef75ef53c981676dfd2603.ad3c3d38eb06e2349df0a53ed2cae66c';
	var zohoClientId = '1000.5NABWJGKAXO7PKGOJ88EXHS985HV9F';
	var zohoClientSecret = '68a4446fb6d823bd76d462ee18d07c4389aac0d96b';

	var options = {
		'method': 'POST',
		'url': 'https://accounts.zoho.com/oauth/v2/token?refresh_token=' + zohoRefreshToken + '&client_id=' + zohoClientId + '&client_secret=' + zohoClientSecret + '&grant_type=refresh_token',
		'headers': {
		}
	  };
	  request(options, function (error, response) {
		if (error) throw new Error(error);
		console.log(response.body);
		var res = JSON.parse(response.body);
		zohoAccessToken = res.access_token;
		console.log(zohoAccessToken);
		/*getContact('1858185000256332175', function(){
		})*/
		//getChatHistory('593995453544', function(chat){
		//	console.log(chat);
			
		//});
		//getUserByEmail('sgomezjurado@novaseguroslatam.com', function(){
		//});
		/*searchContact2('0999631911', function(contact){
			console.log(contact);
			
		});*/
		//insertPossibleContactNote('1858185000245329001', '1858185000245283074');
	  });
}


function getUserByEmail(email, callback){

	var options = {
		'method': 'GET',
		'url': 'https://www.zohoapis.com/crm/v2.1/users?type=ActiveUsers&page=1',
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + zohoAccessToken
		  }
	  };
	  request(options, function (error, response) {
		if (error) throw new Error(error);
		var data1 = JSON.parse(response.body);
		
		var options = {
			'method': 'GET',
			'url': 'https://www.zohoapis.com/crm/v2.1/users?type=ActiveUsers&page=2',
			'headers': {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + zohoAccessToken
			  }
		  };
		  request(options, function (error, response) {
			if (error) throw new Error(error);
			var data2 = JSON.parse(response.body);
			
			var data = data1.users.concat(data2.users);

			var found = _.findWhere(data, {email: email});
			if(!found){
				console.log("No user");
				callback({id: '1858185000006000021'});
			}else{
				console.log(found);
				callback(found);
			}
		  });
	  });
}

function searchContact(phone, callback){

	phone = phone.replace('593', '0');

	var options = {
		'method': 'GET',
		'url': 'https://www.zohoapis.com/crm/v2.1/Contacts/search?criteria=Mobile:equals:+' + phone,
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + zohoAccessToken
		  }
	  };
	  request(options, function (error, response) {
		if (error) throw new Error(error);
		console.log(response.body);
		

		if(!response.body){
			console.log("No contact");
			callback();
		}

		if(response.body){
			var data = JSON.parse(response.body);
			var contact = data.data;
			console.log(contact[0]);
			callback(contact[0]);
		}
	  });
}

function searchContact2(phone, callback){

	phone = phone.replace('593', '0');
	
	var contact  = _.findWhere(jsonData["Hoja1"], {' Celular ': phone});


	if(!contact){
		console.log("No contact");
		callback();
	}

	if(contact){
		callback(contact);
	}
	
}


function insertPossibleContact(data, callback){

	var data = {
		"data": [
			{
				"Owner": data.Owner,
				"Last_Name": data.Last_Name,
				"First_Name": data.First_Name,
				"Email": data.Email,
				"Ejecutivo": data.Ejecutivo,
				"Lead_Status": data.Lead_Status,
				"Lead_Source": data.Lead_Source,
				"Mobile": data.Mobile,
				"Provincia": data.Provincia,
				"Canton": data.Canton,
				"Campana_ID" : "1858185000247072064",
				"Description": "AIGMED",
				"Company": data.Company,
				"RUC": data.RUC,
				"Designation": data.Designation,
				"Industry": data.Industry,
				//New keys,
				"Producto":[
					"Asistencia médica"
				 ]
			}
		],
		"trigger": [
			"approval",
			"workflow",
			"blueprint"
		]
	}


	var options = {
		'method': 'POST',
		'url': 'https://www.zohoapis.com/crm/v2.1/Leads',
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + zohoAccessToken
		},
		body: JSON.stringify(data)
	  };

	  request(options, function (error, response) {
		if (error) throw new Error(error);
		console.log(response.body);

		if(response.body){
			var data = JSON.parse(response.body);
			var user = data.data[0];
			var id = user.details.id;
			callback(id);
		}
	  });
}


function updatePossibleContact(data, callback){

	var data = {
		"data": [
			{
				"id": data.id,
				"Owner": data.Owner,
				"Last_Name": data.Last_Name,
				"First_Name": data.First_Name,
				"Email": data.Email,
				"Ejecutivo": data.Ejecutivo,
				"Lead_Status": data.Lead_Status,
				"Lead_Source": data.Lead_Source,
				"Mobile": data.Mobile,
				"Provincia": data.Provincia,
				"Canton": data.Canton,
				"Campana_ID" : "1858185000247072064",
				"Description": "AIGMED",
				"Company": data.Company,
				"RUC": data.RUC,
				"Designation": data.Designation,
				"Industry": data.Industry,
				//New keys
				"Producto":[
					"Asistencia médica"
				 ]
			}
		],
		"trigger": [
			"approval",
			"workflow",
			"blueprint"
		],
		"lar_id": "1858185000034919435"
	}

	var options = {
		'method': 'PUT',
		'url': 'https://www.zohoapis.com/crm/v2.1/Leads',
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + zohoAccessToken
		},
		body: JSON.stringify(data)
	  
	  };
	  request(options, function (error, response) {
		if (error) throw new Error(error);
		console.log(response.body);

		if(response.body){
			var data = JSON.parse(response.body);
			var res = data.data[0];
			if(res.status == 'success'){
				console.log("success");
				console.log(res);
				callback("success");
			}

			if(res.status == 'error'){
				console.log("error");
				console.log(res);
				callback();
			}
		}
	  });

}

function getContact(id, callback){
	console.log("Here");
	
	var options = {
		'method': 'GET',
		'url': 'https://www.zohoapis.com/crm/v2.1/Leads/' + id,
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + zohoAccessToken
		}
	  
	  };
	  request(options, function (error, response) {
		  console.log(error);
		  
		if (error) throw new Error(error);
		console.log(response.body);
		if(response.body){
			
			var data = JSON.parse(response.body);
			var res = data.data[0];
			console.log(res);
			
			if(res.status == 'success'){
				console.log("success");
				console.log(res);
				callback(res);
			}

			if(res.status == 'error'){
				console.log("error");
				console.log(res);
				callback();
			}
		}
	  });

}

function insertPossibleContactNote(id, note, callback){


	var data = {
		"data": [
		   {
				"Note_Title": "Conversación WhatsApp Text Center",
				"Note_Content": note,
				"Parent_Id": id,
				"se_module": "Leads"
			}
		]
	}

	var options = {
		'method': 'POST',
		'url': 'https://www.zohoapis.com/crm/v2.1/Leads/' + id + '/Notes',
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + zohoAccessToken
		},
		body: JSON.stringify(data)
	  
	  };
	  request(options, function (error, response) {
		if (error) throw new Error(error);
		if(response.body){
			var data = JSON.parse(response.body);
			var res = data.data[0];
			if(res.status == 'success'){
				console.log("success");
				console.log(res);
				callback(res.details.id);
			}

			if(res.status == 'error'){
				console.log("error");
				console.log(res);
				callback();
			}
		}
	  });

}


function updateContactNote(userId, noteId, note){

	var data = {
		"data": [
		   {
				"Note_Title": "Actualización conversación WhatsApp Text Center",
				"Note_Content": note,
				"Parent_Id": userId,
				"se_module": "Leads"
			}
		]
	}

	return;

	var options = {
		'method': 'PUT',
		'url': 'https://www.zohoapis.com/crm/v2.1/Notes/' + noteId,
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + zohoAccessToken
		},
		body: JSON.stringify(data)
	  };

	  request(options, function (error, response) {
		if (error) throw new Error(error);
		console.log(response.body);
	  });

}

function updateFirebaseContact(contact, data){
	firebase.database().ref("/accounts/"+client+"/chats/"+contact).update(data);

}

function getRep(repId, callback){
	firebase.database().ref("/users/"+repId).once("value", function(snapshot) {
		var user = snapshot.val();
		callback(user);
	});

}


function getChatHistory(contact, callback){

	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/msgs/").once("value", function(snapshot) {
		var msgs = snapshot.val();

		msgs = _.map(msgs, function(v, k) {
			return v;
		});

		msgs = _.sortBy(msgs, function(m){
			return m.ts;
		})

		var chat = "";

		msgs.forEach(m => {
			var from = "";
			if(m.rep == "wapi") from = "User: ";
			if(m.rep != "wapi") from = "Agent: ";

			chat += from + m.m + " - " + moment(m.ts).format("LLL") + "\n";
		});

		callback(chat);
	});
}

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listenToJobs();
		processJobQueue();
		console.log(jsonData['Hoja1'].length);
	}	
}

function listenToJobs() {
	firebase.database().ref("/accounts/"+client+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued");
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	}
}

function processJob(job) {
	//get user step in bot
	//if(job.contact != '593995453544') return;

	firebase.database().ref("/accounts/"+client+"/chats/"+job.contact).once("value", function(snapshot) {
		var user = snapshot.val();
		var step = "0";
		var group = "main";
		if (user.step) {step = user.step};
		if (user.group) {group = user.group};

	
		console.log ("User "+job.contact+" is at step:", step, "group:", group);

		if(group == 'uio' || group == 'gye' || group == 'man' || group == 'cue'){
			searchContact2(job.contact, function(contact, err){
				console.log("Updating");
				if(contact){
					//Update database
					var data ={
						"x-email": contact[' Email '],
						"x-rucempresa": contact['RUC EMPRESA'],
						"x-empresa": contact['NOMBRE EMPRESA'],
						"nick": contact['NOMBRE'],
						//New keys
					}
					console.log(data);
					
					updateFirebaseContact(job.contact, data);
				}
			})
		}

		if (step != 'idle') {
			console.log("Review", job);
				if(!job.msg){
					finishJob(job);
					return;
				}
				switch (job.msg.type) {
					case 'chat':
						var body = job.msg.body;	

						if (body.length>0) {
							console.log ("Mo: ",body)
							body = body.replace('.','');
							var kmarr = ["kilometro","kilómetro","kilometros","kilómetros", "km", "kms"]
							var emparr = ["empresa","empresas"]

							//trim spaces
							body = body.trim();
							if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*" || body.toLowerCase() == "#salir") {
								var msg = "Gracias por usar este canal. Adios!";
								pushText (msg, job.contact, user.line);
								resetUserParam("step", job.contact, user.line);
							} else if (kmarr.indexOf( body.toLowerCase() )>=0 ) {
								var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
								setUserParam("step","idle", job.contact, user.line);
								setUserParam("status","active", job.contact, user.line);
								setUserParam("group", "km", job.contact, user.line); 
								pushText (msg, job.contact, user.line);
							} else if (_.intersection(body.toLowerCase().split(" "), emparr).length > 0 ) {
								var msg = "Bienvenid@ a NOVAECUADOR.\n\nPor favor escoje una de las siguientes ciudades para poder atenderte de mejor manera:\n\n";
								msg += "1. Quito\n";
								msg += "2. Guayaquil\n";
								msg += "3. Manta\n";
								msg += "4. Cuenca";
								setUserParam("step","4", job.contact, user.line);
								pushText (msg, job.contact, user.line);
							} else {
								//process step
								switch (step) {
									case "0":
										var kw = body.toLowerCase()
										var msg = "Hola\n";
										msg += "Bienvenido a NOVA!\n\n";
										msg += "¿En qué te puedo servir?\n";
										msg += "(selecciona una opción)\n\n";
										msg += "1. Deseo contratar un seguro o asistencia.\n";
										msg += "2. Requiero asesoría sobre mis productos\n";
										msg += "3. Quiero reportar un siniestro\n";
										msg += "4. Renovaciones\n";
										setUserParam("step","1", job.contact, user.line);
										pushText (msg, job.contact, user.line);
										break;
										//end step 0
									case "1":
										var kw = body.toLowerCase()
										var group = ""
										if (["1","1.","uno"].indexOf(kw)>=0) {
											var msg = "Qué producto estas interesado:\n\n";
											msg += "1. Seguro de Salud\n";
											msg += "2. Seguro de Vida\n";
											msg += "3. Seguro Vehicular\n";
											msg += "4. Seguro de Hogar\n";
											msg += "5. Otro Seguro\n";
											setUserParam("step","2", job.contact, user.line);
											group = "com"
										}
										if (["2","2.","dos"].indexOf(kw)>=0) {
											var msg = "Qué tipo de asesoría requieres:\n\n";
											msg += "1. Conocer características de mi producto\n";
											msg += "2. Como usar mi producto\n";
											msg += "3. Deseo presentar un requerimiento\n";
											msg += "4. Otros\n";
											setUserParam("step","2", job.contact, user.line);
											group = "ser"
										}
										if (["3","3.","tres"].indexOf(kw)>=0) {
											var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
											setUserParam("step","idle", job.contact, user.line);
											setUserParam("status","active", job.contact, user.line);
											group = "sin"
										}
										if (["4","4.","cuatro"].indexOf(kw)>=0) {
											var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
											setUserParam("step","idle", job.contact, user.line);
											setUserParam("status","active", job.contact, user.line);
											group = "ser"
										}
										if (group.length>0) {
											setUserParam("group", group, job.contact, user.line); 
										} else {											
											var msg = "Por favor selecciona una de las siguientes opciones o escribe *salir*:\n\n";
											msg += "1. Deseo contratar un seguro o asistencia.\n";
											msg += "2. Requiero asesoría sobre mis productos.\n";
											msg += "3. Quiero reportar un siniestro.\n";
											msg += "4. Renovaciones\n";
										}
										pushText (msg, job.contact, user.line);
										break;
								    case "2":
										var kw = body.toLowerCase()
										if (["3","3.","tres"].indexOf(kw)>=0) {
											var msg = "Por favor selecciona una de las siguientes opciones o escribe *salir*:\n\n";
											msg += "1. Seguro por *Kilómetro*.\n";
											msg += "2. Seguro tradicional.\n";
											setUserParam("step","3", job.contact, user.line);
											pushText (msg, job.contact, user.line);
										} else {
											var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
											setUserParam("step","idle", job.contact, user.line);
											setUserParam("status","active", job.contact, user.line);
											pushText (msg, job.contact, user.line);
										}
										break;
									case "3":		
										if (["1","1.","uno"].indexOf(kw)>=0) {
											setUserParam("group", "km", job.contact, user.line); 
										}
										var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
										setUserParam("step","idle", job.contact, user.line);
										setUserParam("status","active", job.contact, user.line);
										pushText (msg, job.contact, user.line);
										break;
									case "4":
										var kw = body.toLowerCase()
										
										var group = "";
										var provincia = "";
										var canton = "";
										var uioArr = ["1","1.","uno", "quito", "pichincha"];
										var gyeArr = ["2","2.","dos", "guayaquil", "guayas"]
										var manArr = ["3","3.","tres", "manabi", "manta", "portoviejo"]
										var cueArr = ["4","4.","cuatro", "cuenca", "azuay", "loja"]

										if (_.intersection(kw.split(" "), uioArr).length > 0 ) {
											group = "uio"
											provincia = "17 PICHINCHA";
											canton = "1 QUITO";
										}
										if (_.intersection(kw.split(" "), gyeArr).length > 0 ) {
											group = "gye"
											provincia = "9 GUAYAS"
											canton = "1 GUAYAQUIL"
										}
										if (_.intersection(kw.split(" "), manArr).length > 0 ) {
											group = "man"
											provincia = "13 MANABI"
											canton = "1 PORTOVIEJO"
										}
										if (_.intersection(kw.split(" "), cueArr).length > 0 ) {
											group = "cue"
											provincia = "1 AZUAY"
											canton = "1 CUENCA"
										}
										if (group.length>0) {
											var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
											let params = {
												"step": "idle",
												"status": "active",
												"group": group,
												"x-provincia": provincia,
												"x-canton": canton
											}
											updateFirebaseContact(job.contact, params);
											console.log("Set ", provincia, canton);

											searchContact2(job.contact, function(contact, err){
												console.log("Updating");
												if(contact){
													//Update database
													var data ={
														"x-email": contact[' Email '],
														"x-rucempresa": contact['RUC EMPRESA'],
														"x-empresa": contact['NOMBRE EMPRESA'],
														"nick": contact['NOMBRE']
													}
													console.log(data);
													
													updateFirebaseContact(job.contact, data);
												}
											})

											pushText (msg, job.contact, user.line);
										} else {											
											var msg = "Por favor selecciona una de las siguientes opciones o escribe *salir*:\n\n";
											msg += "1. Quito\n";
											msg += "2. Guayaquil\n";
											msg += "3. Manta\n";
											msg += "4. Cuenca";
											pushText (msg, job.contact, user.line);
										}
										
										break;
								}//end switch step
							}
						} //end if body length > 0
						//job is completed
						finishJob(job);
						break; 
					
					default:
						//finish jobs
						finishJob(job);
						break;
				} //end switch mo type
		} else { //end idle check
			console.log ('user '+job.contact+'@'+user.line+' is idle');
			//job is completed
			if(job.type){

				switch (job.type) {
					case 'lead':
		
						if(user.rep){
							console.log("Finding user rep", user.rep);
							
							getRep(user.rep, function(rep){
								
								getUserByEmail(rep.email, function(novaUser){
									console.log(novaUser);
									
									if(novaUser){
											
										var name = 'Unknown';
										var lastname = 'Unknown';
										var mobile = job.contact.replace("593", "0");
										if(user.nick){
											var parts = user.nick.split(' ');
											if(parts.length == 1){
												name = parts[0];
											}

											if(parts.length == 2){
												name = parts[0];
												lastname = parts[1];
											}

											if(parts.length == 3){
												name = parts[0]
												lastname = parts[1] + " " + parts[2]; 
											}

											if(parts.length == 4){
												name = parts[0] + " " + parts[1]; 
												lastname = parts[2] + " " + parts[3]; 
											}
										}

										var data ={
											"Last_Name": lastname,
											"First_Name": name,
											"Email": user["x-email"],
											"Ejecutivo": "COMERCIAL CORPORATIVO",
											"Lead_Status": "No contactado",
											"Lead_Source":"Text Center",
											"Mobile": mobile,
											"Provincia": user["x-provincia"],
											"Canton":  user["x-canton"],
											"Company": user['x-empresa'],
											"RUC": user['x-rucempresa'],
											"Designation": 'Ejecutivo',
											"Industry": 'Otros'
											//New keys
										}
										
										console.log(user);
										
										if(user['x-leadId']){
											//Update database
											data.id = user["x-leadId"];
											data.Owner = user["x-ownerId"];
											console.log("Updating", data);
											var now = Date.now();
											setUserParam("logs/" + now, data, job.contact);
											setUserParam("logs/" + now + '/op', 'update', job.contact);
											updatePossibleContact(data, function(res){
												setUserParam("logs/" + now + '/res', res, job.contact);
												//getChatHistory(job.contact, function(chat){
													//updateContactNote(user["x-leadId"], user['x-noteId'], chat);
												//});
											});
										}else{
											
											data.Owner = novaUser.id;
											data.Ejecutivo = "COMERCIAL CORPORATIVO";
											data.Lead_Status = "No contactado";
											
											console.log("Inserting", data);
											
											var now = Date.now();
											setUserParam("logs/" + now, data, job.contact);
											setUserParam("logs/" + now + '/op', 'insert', job.contact);
											insertPossibleContact(data, function(id){
												console.log("Inserted lead", id);
												setUserParam("x-leadId", id, job.contact);
												setUserParam("x-ownerId",novaUser.id, job.contact);
												setUserParam("logs/" + now + '/res', id, job.contact);

												if(id){
													getChatHistory(job.contact, function(chat){
														console.log("Got chat", chat);
														
														insertPossibleContactNote(id, chat, function(noteId){
															setUserParam("x-noteId", noteId, job.contact);
														});
													});
												}
											});
										}
									}
								})
							})
		
						}
		
						finishJob(job);
						break;
				
					default:
						//finish jobs
						finishJob(job);
						break;
				}
			
			}else{
				finishJob(job);
			}
		}

		
	});
}

function pushText (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	//send after random ms
	setTimeout(function() {
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.client = client;
			//push to client MT queue
			firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}, tools.getRndMS(1000));
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+client+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		console.log (new Date(), "Job completed");
		setTimeout(function() {
			processJobQueue();
		}, tools.getRndMS(500) );
	});
}


function setUserParam(key, val, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).set(val);
}

function resetUserParam(key, contact, line) {
	firebase.database().ref("/accounts/"+client+"/chats/"+contact+"/"+key).remove();
}

