var fs = require('fs');
var path = require('path');
var _ = require('underscore');
var firebase = require('firebase');
var accounts = {};
var booted = false;
var os = require('os');

//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function bootApp() {
	if (!booted) {
		booted = true;
		//load api accounts 
		loadAccounts();
		
	}	
}

function loadAccounts() {
	var self = this;
	firebase.database().ref('apiAccounts').on('value', function(snapshot) {		  
		var arr = snapshot.val();
		console.log ("API Accounts fetched");
		for (var i=0; i<arr.length; i++) {
			if (accounts[arr[i]]) {
				console.log ("Listeners already started for", arr[i])	
			} else {
				if (fs.existsSync(path.resolve(__dirname, './dumps/jobs/'+arr[i]))) {
					console.log ("Directory",path.resolve(__dirname, './dumps/jobs/'+arr[i]),"exists")
				} else {
					fs.mkdirSync(path.resolve(__dirname, './dumps/jobs/'+arr[i]));
					console.log ("Directory",path.resolve(__dirname, './dumps/jobs/'+arr[i]),"created")
				}
				accounts[arr[i]] = {started: true, q: []}	
				listenToJobs(arr[i]);
				console.log ("Listeners started for", arr[i])
				//start worker
				startWorker(arr[i]);
			}
		}
	});

}

function startWorker(account) {
	setTimeout(function() {
		worker(account);
		console.log("Worker started for",account)
	}, 2000)	
}
function listenToJobs(account) {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("done").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		console.log (new Date(), "New "+account+" job queued", accounts[account].q.length);
		accounts[account].q.push(job);
	});
}

function worker(account) {
	if (accounts[account].q.length<=0) {
		setTimeout(function() {
			worker(account);
		}, 10000)	
	} else {
		console.log ("Processing", account, accounts[account].q.length)
		processJob(accounts[account].q.shift(), account);
	}	
}
function processJob(job, account) {
	dumpJSON('./dumps/jobs/'+account+'/'+job.id+'.json', job, function(dumped){
		if (dumped) {
			firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id).remove();	
			setTimeout(function() {worker(account)}, 10)
		}	
	});	
}

function dumpJSON(url, ob, cb) {
	fs.writeFile(path.resolve(__dirname, url), JSON.stringify(ob), 'utf8', function(err) {
		if (!err) {
			cb(true);
		} else {
			console.log (err);
			process.exit(0)
			cb(false)
		}	
	});
}


