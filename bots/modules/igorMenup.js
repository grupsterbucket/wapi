
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' igorMenup', user.id, step, group)
    console.log(job)
    switch (step) {
      // Me llega el saludo y el menu principal
      case "0":
        updateUser({ step: '1', module: "main" }, job.contact);
        pushText(mainMenu(true), job.contact, user.line)
        break;

      // Me llega que opcion del menu principal selecciono y se redirije
      case "1":
        var kw = body.toLowerCase()
        if (["1", "1.", "uno", "soy cliente"].indexOf(kw) >= 0) {
          mods["igorMenu1"].bot('Swtich Module', job, user, "0")
        } else if (["2", "2.", "dos", "descargar"].indexOf(kw) >= 0) {
          mods["igorMenu2"].bot('Swtich Module', job, user, "0")
        } else if (["3", "3.", "tres", "informacion", "mas informacion"].indexOf(kw) >= 0) {
          mods["igorMenu3"].bot('Swtich Module', job, user, "0")
        } else if (["4", "4.", "cuatro", "validar factura"].indexOf(kw) >= 0) {
          //abrir ticket
          if (isShopOpen()) {
            msg = 'Pronto un especialista te contactará.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line);
          logStartTicket(job.contact, user.chn, user, "sac");
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo igorMenup")
    console.log(job)

    cb({ st: "done" })
  }
}

function mainMenu(greet) {
  if (greet) {
    var msg = '*¡Hola! Soy Igor, tu asistente virtual.!*\n\n'
    msg += '*Estoy aquí para hacer tu experiencia de movilidad mucho más fácil. Por favor, selecciona una de las opciones:*\n\n'
  } else {
    var msg = 'Por favor, selecciona una de las opciones:\n\n'
  }
  msg += "1. ¡Soy cliente! Quiero explorar lo que puedo hacer contigo.\n";
  msg += "2. ¡Quiero descargar la app! \n";
  msg += "3. Busco más información. \n";
  msg += "4. ¡Valida tu factura! \n";
  return msg

}

