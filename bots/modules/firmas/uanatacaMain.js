// SAC Bot as module
module.exports = {
	bot: function(body, job, user, step, group) {
		console.log ("Processing uanataca sac bot", step, group)
		console.log('Processing ' + account + ' main', user.id, step, group)
		switch (step) {
			case "0":
				setUserParam("step","sac1", job.contact, user.line);
				pushText (mainMenu(true), job.contact, user.line);
				break;
				//end step 0
			case "sac1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","firma","solcitar"].indexOf(kw)>=0) {
					pushText (firma()+mainMenu(), job.contact, user.line);
				} else if (["2","2.","dos","trabaja","distribuidor","vender"].indexOf(kw)>=0) {
					setUserParam("step","sac2.1", job.contact, user.line);
					pushText (trabaja(), job.contact, user.line);
				} else if (["3","3.","tres","ayuda","servicio"].indexOf(kw)>=0) {
					setUserParam("step","sac3.1", job.contact, user.line);
					pushText (sac(), job.contact, user.line);
				} else if (["9","9.","nueve"].indexOf(kw)>=0) {
            mods["mainFirmas"].bot('Swtich Module', job, user, "0")
				} 
				else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "sac2.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					if (isShopOpen()) {
						var msg = "Gracias por contactarte con nosotros, te contactaremos lo más pronto posible.\n";
						updateUser({step:"idle", status: "active", group: "fac", tso: Date.now()}, job.contact);
						pushText (msg, job.contact, user.line);
						logStartTicket2(job.contact, user.chn, user, "fac");
					} else {
						pushText (config.horarioStr, job.contact, user.line);
						setUserParam("step","0", job.contact, user.line);
					}
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					setUserParam("step","sac2.1.2", job.contact, user.line);
					pushText (serDistri(), job.contact, user.line);
					
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					setUserParam("step","sac1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				} else {											
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "sac2.1.2":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					if (isShopOpen()) {
						var msg = "Te contactará un agente comercial pronto, muchas gracias por utilizar este servicio \n";
						updateUser({step:"idle", status: "active", group: "sod", tso: Date.now()}, job.contact);
						pushText (msg, job.contact, user.line);
						logStartTicket2(job.contact, user.chn, user, "sod");
					} else {
						pushText (config.horarioStr, job.contact, user.line);
						setUserParam("step","0", job.contact, user.line);
					}
					
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "Gracias por tu interés en trabajar con nosotros pero al momento no cumples los requerimientos.\n\n"
					setUserParam("step","sac1", job.contact, user.line);
					pushText (msg + mainMenu(false), job.contact, user.line);
				} else {	
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			
			case "sac3.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText (tipos()+GoBackMenu(), job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					setUserParam("step","sac3.2.1", job.contact, user.line);
					pushText ("Por favor envía tu número de cédula", job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText ("Por favor ingresa a los siguientes links para saber como firmar con tu firma electrónica de UANATACA:\n\nFIRMA EC https://youtu.be/X3M4L0u57U4 \nADOBE READER https://youtu.be/-txoPSL7Fus \n\n"+GoBackMenu(), job.contact, user.line);
				} else if (["4","4.","cuatro"].indexOf(kw)>=0) {
					setUserParam("step","go-back", job.contact, user.line);
					pushText ("Revisa un video tutorial en el siguiente enlace: https://youtu.be/CJhXMDa8kUk \n\n"+GoBackMenu(), job.contact, user.line);
				} else if (["5","5.","cinco"].indexOf(kw)>=0) {
					if (isShopOpen()) {
						var msg = "Tienes una sugerencia? Por favor envíanos los detalles y te responderemos lo antes posible. Gracias!\n";
						updateUser({step:"idle", status: "active", group: "sug", tso: Date.now()}, job.contact);
						pushText (msg, job.contact, user.line);
						logStartTicket2(job.contact, user.chn, user, "sug");
					} else {
						pushText (config.horarioStr, job.contact, user.line);
						setUserParam("step","0", job.contact, user.line);
					}
					
				} else if (["6","6.","seis"].indexOf(kw)>=0) {
					if (isShopOpen()) {
						var msg = "Necesitas ayuda? Por favor envíanos los detalles y te responderemos lo antes posible. Gracias!\n";
						updateUser({step:"idle", status: "active", group: "sac", tso: Date.now()}, job.contact);
						pushText (msg, job.contact, user.line);
						logStartTicket2(job.contact, user.chn, user, "sac");
					} else {
						pushText (config.horarioStr, job.contact, user.line);
						setUserParam("step","0", job.contact, user.line);
					}
					
				} else if (["7","7.","siete"].indexOf(kw)>=0) {
					setUserParam("step","sac1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				} else {
					pushText ("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "sac3.2.1":
				var kw = body.toLowerCase()
				utapi.checkStatus(kw.substring(0,13), function(arr){
					if (arr) {
						if (arr.length==1) {
							var reg = arr[0]	
						} else {
							//sort and use last
							arr = _.sortBy(arr, function(item) { return item.fecha_registro});
							var reg = arr[arr.length-1]
						}
						msg = "El estado de tu solicitud es *"+reg.estado+"*\nObservaciones: _" + mapaEstadoFirma(reg.estado.toUpperCase(), reg.observacion) + "_"
						setUserParam("step","sac1", job.contact, user.line);
						pushText (msg+"\n\n"+mainMenu(false), job.contact, user.line);
					} else {
						setUserParam("step","sac1", job.contact, user.line);
						pushText ("No hemos podido validar tu solicitud con el número de cédula ingresado\n\n"+mainMenu(false), job.contact, user.line);
					}	
				});
				break
			case "go-back":
				var kw = body.toLowerCase()
				var group = ""
				if (["salir","*salir*"].indexOf(kw)>=0) {
					salir(job, user);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					salir(job, user);
				} else {											
					setUserParam("step","sac1", job.contact, user.line);
					pushText (mainMenu(false), job.contact, user.line);
				}
				break;
		}//end switch step
		
		function mainMenu(greet) {
			if (greet) {
				var msg = "Gracias por comunicarte con UANATACA EC por favor selecciona la opción de tu interés, será un gusto poder ayudarte 🙂\n\n";
			} else {
				var msg = "Por favor selecciona la opción de tu interés, será un gusto poder ayudarte 🙂\n\n";
			}
			msg += "1. Solicita tu firma\n";
			msg += "2. Trabaja con nosotros\n";
			msg += "3. Servicio al cliente\n";;
			return msg;
		}
		
		function GoBackMenu() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Regresar al menú principal\n";
			msg += "2. Salir";
			return msg;
		}
		
		
		function trabaja() {
		    var msg = "";
			msg += "1. Soy facturador electrónico\n";
			msg += "2. Quiero ser Distribuidor\n";
			msg += "3. Regresar al menú principal\n";
			return msg;
		}

		
		function firma() {
		    var msg = "Para solicitar tu firma electrónica utiliza nuestro formulario de solicitud en línea: https://store.uanataca.ec\n\n";
			return msg;
		}
		

		function sac() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Tipos de firma\n";
			msg += "2. Estado de solicitud\n";
			msg += "3. Cómo firmar un documento\n";
			msg += "4. Cómo descargar mi firma\n";
			msg += "5. Déjanos tus sugerencias\n";
			msg += "6. Contacta a un agente de servicio\n";
			msg += "7. Menú anterior\n";
			
			return msg;
		}
		
		function tipos() {
			var msg = "";
			msg += "*_Persona Natural_*\n";
			msg += "Es un certificado electrónico emitido a un ciudadano. Garantiza su identidad y le permite realizar trámites que requieran autenticarse en portales web o firmar electrónicamente documentos con plenas garantías legales.\n";
			msg += "INGRESA AQUÍ PARA MÁS INFORMACIÓN\n";
			msg += "https://bit.ly/3TCk1MN \n\n";

			msg += "*_Miembro de empresa_*\n";
			msg += "Es un certificado electrónico emitido a un empleado de una entidad. Lo solicita el representante legal o autorizado de la empresa para sus empleados y lo vincula como empleado de dicha entidad.\n";
			msg += "INGRESA AQUÍ PARA MÁS INFORMACIÓN\n";
			msg += "https://bit.ly/3TCk1MN \n\n";

			msg += "*_Representante legal_*\n";
			msg += "Es un certificado electrónico emitido al representante legal de una entidad para sus relaciones con organismos y entidades públicas. Lo puede solicitar el representante legal siempre que acredite su identidad y acredite sus poderes de representación.\n";
			msg += "INGRESA AQUÍ PARA MÁS INFORMACIÓN\n";
			msg += "https://bit.ly/3TCk1MN  \n\n";

			
			return msg;
		}		
		
		function serDistri() {
			var msg = "Te comentamos que para ser distribuidor necesitas ser una empresa y tener desarrollado un sistema de facturación propio ¿Cumples con los requisitos?\n\n";
			msg += "1. Si\n";
			msg += "2. No\n";
			return msg;
		}
	}
}
