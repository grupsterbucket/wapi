global.fetch = require('node-fetch');
global.tools = require('./tools');
//~ for (var i=0; i<1000000;i++) {
//~ console.log (tools.generateALfaPin())	
//~ }
global.qr = require('./qr');
global.aws = require('../aws');
global.dataUriToBuffer = require('data-uri-to-buffer');
global.extractFrames = require('ffmpeg-extract-frames');
global.extractFrame = require('ffmpeg-extract-frame');

//~ extractFrame({
//~ input: './tempVids/593984252217_1673910320380',
//~ output: './tempVids/frame.jpg',
//~ quality: 2,
//~ offset: 1250
//~ })

global.fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
var bot = require('./botPlugin');
var botPC = require('./padelcityPlugin');
global._ = require('underscore');
global.firebase = require('firebase');
var CryptoJS = require("crypto-js");
var account = "coniski";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 150;
global.config = {}
global.calendars = []
global.lines = {}
global.requeueInt = 5000
//firebase init
var firebaseConfig = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
	"authDomain": "ubiku3.firebaseapp.com",
	"databaseURL": "https://ubiku3.firebaseio.com",
	"projectId": "ubiku3",
	"storageBucket": "ubiku3.appspot.com",
	"messagingSenderId": "946813430025",
	"appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(function (user) {
	if (user) {
		// User is signed in.
		console.log("Logged in to firebase as uid " + user.uid);
		setTimeout(function () {
			bootApp();
		}, 2000);

	} else {
		// User is signed out login again
		console.log("Session expired");
		loginTofirebase();
	}
	// ...
});

function loginTofirebase() {
	console.log("Login to firebase");
	firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
		var errorCode = error.code;
		var errorMessage = error.message;
		console.log(error.code + ": " + error.message);
	});
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		initAWS(function () {
			loadAccountConfig();
			setTimeout(function () {
				listenToJobs();
				processJobQueue();
				//getDataRegCivil('1707784839','E1131I1222')
			}, 1000);
		});
	}
}

function initAWS(cb) {
	firebase.database().ref('/accounts/coniski/config/creds/aws').once('value', function (creds) {
		if (creds.val()) {
			aws.init(creds.val().accessKeyId, creds.val().secretAccessKey);
			//aws.transcribeVideo("wsp2crmcdn","v3/coniski/demos/liveness3dplus/593984252217_1673572201336.mp4")
			cb();
		} else {
			console.log("AWS credentials not found");
		}
	});
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/' + account + '/config/misc').on('value', function (snapshot) {
		config = snapshot.val();
		console.log("Config fetched for account", account);
	});
	firebase.database().ref('accounts/' + account + '/config/calendars').on('value', function (snapshot) {
		calendars = _.map(snapshot.val(), function (ob, key) {
			ob.id = key;
			return ob;
		});
		console.log("Calendars fetched for account", account);
	});
}
function listenToJobs() {
	firebase.database().ref("/accounts/" + account + "/botJobs/").orderByChild("st").equalTo("new").on('child_added', function (res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log(new Date(), "New job queued", job.id);
	});
}

function processJobQueue() {
	if (jobs.length > 0) {
		var job = jobs.shift();
		if (job.runAfter) {
			if (job.runAfter > Date.now()) {
				//console.log (new Date(), "Job not ready for run")
				jobs.push(job)
				setTimeout(function () {
					processJobQueue();
				}, qitv);
			} else {
				processJob(job)
			}
		} else {
			processJob(job)
		}
	} else {
		setTimeout(function () {
			processJobQueue();
		}, qitv);
	}
}

function processJob(job) {
	switch (job.type) {
		case "check-liveness-status":
			firebase.database().ref('/accounts/' + account + '/chats/' + job.contact.msisdn).once('value', function (snapshot) {
				var user = snapshot.val()
				console.log(job)

				verificarLiveness(job.analyse_id, job.api_evidence, function (resolve) {
					console.log(resolve.details.data.results_media)
					if (resolve.status === 200) {

						if (resolve.details.data.state == "FINISHED") {
							var score = 100 - (resolve.details.data.results_media[0].results_data.confidence_spoofing * 100)

							//update evidence con resultado de spoof
							if (job.nextStep == "finish") {
								//termina prueba rapida de liveness
								updateUser({ step: "0" }, job.contact.msisdn, user.line, user.chn);
								var msg = "Resultado de prueba Liveness 4D:\n\n";
								msg += "Clave segura por audio *✔*\n";
								msg += "Registro Civil *✔*\n";
								msg += "Biometría Facial ( confianza: " + parseFloat(job.FaceMatches[0].Similarity).toFixed(4) + "% ) *✔*\n";
								if (score > 80) {
									msg += "Anti-spoof Liveness ( confianza: " + parseFloat(score).toFixed(4) + "% ) *✔*\n\n";
								} else {
									msg += "Anti-spoof Liveness ( confianza: " + parseFloat(score).toFixed(4) + "% ) *❌*\n\n";
								}
								msg += "La prueba ha terminado, gracias por usar este servicio. Adios!";
								pushText(msg, job.contact.msisdn, user.line, user.chn);
							} else {
								//pide direccion al user
								setUserParam('step', job.nextStep, job.contact, user.line)
								pushText("Next step", job.contact.msisdn, user.line)
							}
							//job is completed
							finishJob(job)
						} else {
							//requeue job	
							requeueJob(job, Date.now() + requeueInt)
						}


					} else {
						//requeue job	
						requeueJob(job, Date.now() + requeueInt)
					}
				})
			})

			break;
		case 'check-biometry':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				console.log(job)
				//extract frames from video
				var fname = job.transJob.replace("TJ_", "")
				extractFrame({
					input: './tempVids/' + fname,
					output: './tempVids/' + fname + '-1.jpg',
					offset: tools.getRndMS(1234),
					quality: 2
				});
				var msg = "Listo, hemos validado correctamente tu clave segura, por favor ahora envíanos tu número de cédula, vamos a validar tu identidad";
				updateUser({ step: "3d3", videoFile: fname }, job.contact, user.line, user.chn);
				pushText(msg, job.contact, user.line, user.chn);
				//job is completed
				finishJob(job);
			});
			break;
		case 'check-transcribe':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				aws.checkTranscribe(job.transJob, function (data) {
					console.log(data)
					if (data.TranscriptionJob.TranscriptionJobStatus == "COMPLETED") {
						aws.getObject('v3/coniski/demos/liveness3dplus/' + job.transJob, function (res) {
							console.log(res.file.Body.toString())
							var obj = JSON.parse(res.file.Body.toString())
							var valid = validateAlfaPIN(job, obj, user);
							if (valid) {
								//create biometry job
								var job2 = {
									type: "check-biometry",
									contact: user.id,
									fromJob: job.id,
									transJob: job.transJob,
									st: 'new',
									ts: Date.now()
								}
								firebase.database().ref("/accounts/" + account + "/botJobs/").push(job2);
							} else {
								console.log(new Date(), "alfa pin validation failed")
								sendNewPin(job, user)
							}
							//job is completed
							finishJob(job);
						})
					} else {
						//requeue job	
						requeueJob(job, Date.now() + requeueInt);
					}
				})


			});
			break;
		case 'send-notification':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				pushText(job.str, job.contact, user.line, user.chn);
				//job is completed
				finishJob(job);
			});
			break;
		case 'reset-bot':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				resetBot(job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
		case 'booking-completed':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				if (user.step == "pc2") {
					//return to bot control
					setTimeout(function () { botPC.bot("", job, user, "pc3", user.group, account); }, 500);
				} else {
					resetBot(job.contact, user);
				}
				//job is completed
				finishJob(job);
			});
			break;
		case 'finish-order':
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact).once("value", function (snapshot) {
				var user = snapshot.val();
				//job is completed
				finishJob(job);
			});
			break;
		default:
			firebase.database().ref("/accounts/" + account + "/chats/" + job.contact.msisdn).once("value", function (snapshot) {
				var user = snapshot.val();
				var step = "0";
				var group = "main";
				if (user.step) { step = user.step };
				if (user.group) { group = user.group };

				if (user.tsso) {
					console.log("User " + job.contact.msisdn + " is at step:", step, "group:", group);
				} else {
					startSession(job.contact.msisdn, user);
					console.log("New Session for " + job.contact.msisdn + "");
				}

				if (step != 'idle') {
					console.log("Process MO ", job.msg.type);
					switch (job.msg.type) {
						case 'text':
							console.log("Body", job.msg.content.text);
							var body = job.msg.content.text;
							if (body.length > 0) {
								//trim spaces
								body = body.trim();

								//catch operations
								if (body.toLowerCase().indexOf("#") == 0) {
									var parts = body.toLowerCase().split(" ");
									//match direct operation
									switch (parts[0]) {
										case "#salir":
											salir(job, user);
											break;
										case "#ping":
											var str = "#pong " + new Date();
											pushText(str, job.contact.msisdn, user.line, user.chn);
											break;
										case "#test":
											var str = parts[1] + "|" + parts[2] + "|" + Date.now();
											pushText(str, job.contact.msisdn, user.line, user.chn);
											break;
										case "#btn":
											var ob = {
												"type": "button",
												"header": {
													"type": "image",
													"image": {
														"url": "https://static1.bigstockphoto.com/0/4/1/large2/140286470.jpg"
													}
												},
												"body": {
													"text": "Te gustan las burgers?"
												},
												"action": {
													"buttons": [{
														"id": "op1",
														"type": "reply",
														"title": "Si de ley!"
													},
													{
														"id": "op2",
														"type": "reply",
														"title": "Nop :("
													},
													{
														"id": "op3",
														"type": "reply",
														"title": "Solo las de veggies"
													}
													]
												},
												"footer": {
													"text": "We have the best ones!"
												}
											}
											pushButtons(ob, job.contact.msisdn, user.line, user.chn);
											break;

										case "#persona":
											var cedula = parts[1];
											if (tools.isCedula(cedula)) {
												//call datofast and get data
												tools.datoFastHook(cedula, function (str, raw) {
													pushText(str, job.contact.msisdn, user.line, user.chn)
												});
											} else {
												pushText("No encontramos una persona con la cédula " + cedula, job.contact.msisdn, user.line, user.chn)
											}
											break;
										case "#empresa":
											var ruc = parts[1];
											//call datofast and get data
											tools.datoFastHookEmpresa(ruc, function (str, raw) {
												pushText(str, job.contact.msisdn, user.line, user.chn)
											});
											break;
										case "#placa":
											var placa = parts[1];
											//call datofast and get data
											tools.datoFastHookVehiculo(placa, function (str, raw) {
												pushText(str, job.contact.msisdn, user.line, user.chn)
											});
											break;
										case "#server":
											var str = "Server data:\n";
											str += "Datetime: " + new Date() + "\n";
											str += "Ram: " + Math.round(os.totalmem() / 1000000) + "MB\n";
											str += "Free: " + Math.round(os.freemem() / 1000000) + "MB\n";
											pushText(str, job.contact.msisdn, user.line, user.chn);
											break
										case "#menta":
											updateUser({ step: "b0" }, job.contact.msisdn);
											bot.segurosBot("na", job, user, "b0", group, account);
											break;
										case "#libera":
											updateUser({ step: "l0" }, job.contact.msisdn);
											bot.liberaBot("na", job, user, "l0", group, account);
											break;
										case "#padel":
											updateUser({ step: "pc0" }, job.contact.msisdn);
											botPC.bot("na", job, user, "pc0", group, account);
											break;
										default:
											var str = "Operación desconocida";
											pushText(str, job.contact.msisdn, user.line, user.chn);
									} //end switch parts operation
								} else {
									if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
										salir(job, user);
									} else {
										//chat is not operation, run bot
										runMainBot(body.toLowerCase(), job, user, step, group)
									}
								} //end if operation
							} //body is empty
							//job is completed
							finishJob(job);
							break;
						case 'image':
							runMainBot("[image]", job, user, step, group)
							//job is completed
							finishJob(job);
							break;
						case 'video':
							runMainBot("[video]", job, user, step, group)
							//job is completed
							finishJob(job);
							break;
						case 'interactive':
							console.log(job.msg.content.interactive)
							//job is completed
							finishJob(job);
							break;
						//!Pruebas ubicacion
						case 'location':
							runMainBot("[location]", job, user, step, group)
							//job is completed
							finishJob(job);
							break
						default:
							console.log(job.msg)
							//job is completed
							finishJob(job);
							break;
					} //end switch mo type
				} else { //end idle check
					console.log('user ' + job.contact.msisdn + '@' + user.line + ' is idle');
					//is a reset operation?
					if (job.msg.content.text) {
						var body = job.msg.content.text;
						if (body.length > 0) {
							//trim spaces
							body = body.trim();
							if (body.toLowerCase().indexOf("#") == 0) {
								if (body.toLowerCase() == "#reset") {
									resetBot(job.contact.msisdn, user, "Reset Session OK");
								}
							}
						}
					}
					//job is completed
					finishJob(job);
				}
			});
	} //end switch job type
}

function requeueJob(job, ts) {
	firebase.database().ref('/accounts/' + account + '/botJobs/' + job.id + '/runAfter').set(ts)
	job.runAfter = ts
	jobs.push(job)
	setTimeout(function () {
		processJobQueue();
	}, qitv);
}

function finishJob(job) {
	firebase.database().ref("/accounts/" + account + "/botJobs/" + job.id + "/st/").set('done', function (res) {
		setTimeout(function () {
			processJobQueue();
		}, qitv);
	});
}

function runMainBot(kw, job, user, step, group) {

	//process step
	switch (step) {
		case "0":
			pushText(mainMenu(true), job.contact.msisdn, user.line, user.chn);
			setUserParam("step", "1", job.contact.msisdn);
			break;
		//end step 0
		case "1":
			if (["1", "1.", "uno", "soporte", "servicio"].indexOf(kw) >= 0) {
				if (isShopOpen()) {
					pushText("Listo, en un momento un agente de servicio le atenderá.", job.contact.msisdn, user.line, user.chn);
				} else {
					pushText("Hemos recibido su solicitud.\n\nLe recordamos que nuestro horario de atención por este canal es " + config.horarioStr + ". Un agente de servicio le responderá apenas retornemos a las actividades. Gracias por su comprensión!", job.contact.msisdn, user.line, user.chn);
				}
				updateUser({ step: "idle", group: "ser", status: "active", tso: Date.now() }, job.contact.msisdn);
				logStartTicket(job.contact.msisdn, user.chn);
			} else if (["2", "2.", "dos", "contacto"].indexOf(kw) >= 0) {
				if (isShopOpen()) {
					pushText("Listo, en un momento un especialista le atenderá.", job.contact.msisdn, user.line, user.chn);
				} else {
					pushText("Hemos recibido su solicitud.\n\nLe recordamos que nuestro horario de atención por este canal es " + config.horarioStr + ". Un agente de servicio le responderá apenas retornemos a las actividades. Gracias por su comprensión!", job.contact.msisdn, user.line, user.chn);
				}
				updateUser({ step: "idle", group: "com", status: "active", tso: Date.now() }, job.contact.msisdn);
				logStartTicket(job.contact.msisdn, user.chn);
			} else if (["3", "3.", "tres", "productos"].indexOf(kw) >= 0) {
				pushText("Por favor visite nuestro website para mayor información: https://coniski.com/" + pageBreak() + mainMenu(false), job.contact.msisdn, user.line, user.chn);
			} else if (["4", "4.", "cuatro", "demo"].indexOf(kw) >= 0) {
				pushText(demosMenu(), job.contact.msisdn, user.line, user.chn);
				setUserParam("step", "2", job.contact.msisdn);
			} else {
				pushText("Por favor digite una de las opciones o escriba *salir* para terminar la sesión", job.contact.msisdn, user.line, user.chn);
			}

			break;
		//end step 1
		case "2":
			if (["1", "1.", "uno", "carrito", "compras", "pedido", "orden"].indexOf(kw) >= 0) {
				updateUser({ step: "c0" }, job.contact.msisdn);
				bot.cartBot(kw, job, user, "c0", group, account);
			} else if (["2", "2.", "dos", "firma"].indexOf(kw) >= 0) {
				updateUser({ step: "f0" }, job.contact.msisdn);
				bot.firmaBot(kw, job, user, "f0", group, account);
			} else if (["3", "3.", "tres", "cita", "agenda"].indexOf(kw) >= 0) {
				updateUser({ step: "a0" }, job.contact.msisdn);
				bot.citaBot(kw, job, user, "a0", group, account);
			} else if (["4", "4.", "cuatro", "banca"].indexOf(kw) >= 0) {
				updateUser({ step: "b0" }, job.contact.msisdn);
				bot.segurosBot(kw, job, user, "b0", group, account);
			} else if (["5", "5.", "cinco", "acceso"].indexOf(kw) >= 0) {
				updateUser({ step: "q0" }, job.contact.msisdn);
				bot.accessBot(kw, job, user, "q0", group, account);
			} else if (["6", "6.", "seis", "liveness", "3d"].indexOf(kw) >= 0) {
				updateUser({ step: "3d0" }, job.contact.msisdn);
				bot.l3dpBot(kw, job, user, "3d0", group, account);
			} else if (["7", "7.", "siete", "courier"].indexOf(kw) >= 0) {
				updateUser({ step: "cr0" }, job.contact.msisdn);
				bot.courrierBot(kw, job, user, "cr0", group, account);
			}
			else {
				pushText("Por favor digite una de las opciones o escriba *salir* para terminar la sesión", job.contact.msisdn, user.line, user.chn);
			}

			break;
		//end step 1
		default:
			if (step.indexOf("c") == 0) {
				bot.cartBot(kw, job, user, step, group, account);
			}
			if (step.indexOf("f") == 0) {
				bot.firmaBot(kw, job, user, step, group, account);
			}
			if (step.indexOf("a") == 0) {
				bot.citaBot(kw, job, user, step, group, account);
			}
			if (step.indexOf("b") == 0) {
				bot.segurosBot(kw, job, user, step, group, account);
			}
			if (step.indexOf("q") == 0) {
				bot.accessBot(kw, job, user, step, group, account);
			}
			if (step.indexOf("l") == 0) {
				bot.liberaBot(kw, job, user, step, group, account);
			}
			if (step.indexOf("pc") == 0) {
				botPC.bot(kw, job, user, step, group, account);
			}
			if (step.indexOf("3d") == 0) {
				bot.l3dpBot(kw, job, user, step, group, account);
			}
			if (step.indexOf("cr") == 0) {
				bot.courrierBot(kw, job, user, step, group, account);
			}
	}//end switch step
}

global.mainMenu = function (greet) {
	if (greet) {
		var str = "Gracias por contactar al chatbot de Coniski.\nPor favor digite una opción:\n\n";
	} else {
		var str = "Por favor digite una opción:\n\n";
	}
	str += "1. Servicio al cliente\n";
	str += "2. Contactarnos\n";
	str += "3. Información sobre nuestros productos\n";
	str += "4. Correr un demo\n\n";
	str += "Escriba *salir* si desea terminar la sesión";
	return str;
}

global.demosMenu = function () {
	var str = "Por favor digite una opción para correr un demo:\n\n";
	str += "1. Carrito de compras\n";
	str += "2. Firma electrónica\n";
	str += "3. Agendar una cita médica\n";
	str += "4. Servicios aseguradora\n";
	str += "5. Acceso QR\n";
	str += "6. Liveness 4D\n";
	str += "7. Courier\n\n";
	str += "Escriba *salir* si desea terminar la sesión";
	return str;
}

global.pageBreak = function () {
	return '\n\n--------------------------oooo--------------------------\n\n'
}

global.pushText = function (body, contact, line, chn, rep, type, desc, url, b64) {
	if (!chn) { chn = 'wsp'; }
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	var msgId = Date.now() + "-" + line + "-" + contact;
	var mt = { g: 0, chn: chn, m: body, rep: rep, t: "mt", ts: Date.now(), type: type, st: 'qed', lu: Date.now() };
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	firebase.database().ref("/accounts/" + account + "/conves/" + contact + "/msgs/" + msgId).set(mt).then(function () {
		var ob = JSON.parse(JSON.stringify(mt));
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).set(ob).then(function () {
			console.log(new Date(), "MT Response POSTED OK");
		}).catch(function (error) {
			console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function (error) {
		console.log(new Date(), "Push MTQ Error", error);
	});

}

global.pushButtons = function (buttons, contact, line) {

	var msgId = Date.now() + "-" + line + "-" + contact;
	var mt = { g: 0, m: "Buttons MT", rep: "bot", t: "mt", ts: Date.now(), type: "btn", st: "qed", lu: Date.now() };
	mt.buttons = buttons;

	firebase.database().ref("/accounts/" + account + "/conves/" + contact + "/msgs/" + msgId).set(mt).then(function () {
		var ob = _.clone(mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).set(ob).then(function () {
			console.log(new Date(), "MT Response POSTED OK");
		}).catch(function (error) {
			console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function (error) {
		console.log(new Date(), "Push MTQ Error", error);
	});

}

global.pushTemplate = function (params, templateName, contact, line) {

	var msgId = Date.now() + "-" + line + "-" + contact;
	var mt = { g: 0, m: "Nuevo ticket", rep: "bot", t: "mt", ts: Date.now(), type: "txt", st: "qed", lu: Date.now() };
	mt.isTemplate = true
	mt.lang = "es"; //todo read from DB
	mt.params = params.join("|");
	mt.templateName = templateName;
	firebase.database().ref("/accounts/" + account + "/conves/" + contact + "/msgs/" + msgId).set(mt).then(function () {
		var ob = _.clone(mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).set(ob).then(function () {
			console.log(new Date(), "MT Response POSTED OK");
		}).catch(function (error) {
			console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function (error) {
		console.log(new Date(), "Push MTQ Error", error);
	});

}

global.setUserParam = function (key, val, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/" + key).set(val);
}

global.updateUser = function (ob, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/").update(ob);
}

function startSession(contact, user) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/tsso").set(Date.now());
	logStartSession(contact, user.chn);
}

global.logStartSession = function (contact, chn) {
	var log = {
		type: "session-start",
		ts: Date.now(),
		ref: contact + "",
		chn: chn
	}
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
}

global.logEndSession = function (contact, chn, tsso, op) {
	var log = {
		type: "session-end",
		tssx: Date.now(),
		ref: contact + "",
		ts: Date.now(),
		op: op
	}
	if (tsso) { log.tsso = tsso; }
	if (chn) { log.chn = chn; }
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
}

global.logStartTicket = function (contact, chn) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact + "",
		chn: chn
	}
	firebase.database().ref("/accounts/" + account + "/logs").push(log);
	//send notification 
	pushTemplate(["Elias", "http://v3.textcenter.net/"], "new_task_update", "593999703430", "593939397007");
	pushTemplate(["Ricardo", "http://v3.textcenter.net/"], "new_task_update", "593984252217", "593939397007");
}

global.resetUserParam = function (key, contact) {
	firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/" + key).remove();
}

global.saveForm = function (ob, cb) {
	firebase.database().ref("/accounts/" + account + "/forms").push(ob).then(function (res) {
		cb(res.key);
	});
}

global.salir = function (job, user) {
	var self = this;
	logEndSession(job.contact.msisdn, user.chn, user.tsso, "s");
	var str = "Gracias por usar este servicio. Adios!"
	pushText(str, job.contact.msisdn, user.line, user.chn);
	resetUserParam('step', job.contact.msisdn);
	resetUserParam('pin', job.contact.msisdn);
	resetUserParam('condos', job.contact.msisdn);
	resetUserParam('group', job.contact.msisdn);
	resetUserParam('account', job.contact.msisdn);
	resetUserParam('tsso', job.contact.msisdn);
	setUserParam('status', 'reset', job.contact.msisdn);

}

global.resetBot = function (contact, user, msg) {
	logEndSession(contact, user.chn, user.tsso, "r");

	if (msg) { pushText(msg, contact, user.line, user.chn); }
	resetUserParam('step', contact);
	resetUserParam('pin', contact);
	resetUserParam('condos', contact);
	resetUserParam('group', contact);
	resetUserParam('account', contact);
	resetUserParam('tsso', contact);
	setUserParam('status', 'reset', contact);
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()
}

global.createTask = function (task, cb) {
	firebase.database().ref("/accounts/" + account + "/tasks").push(task).then(function (res) {
		task.id = res.key;
		if (cb) { cb(task); }
	}).catch(function (error) {
		console.log(error)
		console.log(task);
		if (cb) { cb(null); }
	});
}

global.isShopOpen = function () {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (t >= config.officeOpen && t < config.officeClose) {
		return true;
	} else {
		return false;
	}
}

global.getContact = function (phone, cb) {
	console.log("Searching for contact", phone);
	firebase.database().ref("/accounts/" + account + "/contacts").orderByChild("phone").equalTo(phone + '').once("value", function (res) {
		if (res.val()) {
			var arr = _.map(res.val(), function (ob, key) {
				ob.id = key;
				return ob;
			});
			console.log("Contact found", arr.length);
			cb(arr[0])
		} else {
			cb(null);
		}
	}).catch(function (error) {
		console.log(error)
		cb(null);
	});
}

global.loadLine = function (id, cb) {
	firebase.database().ref("/lines" + id).once("value", function (res) {
		if (res.val()) {
			lines[id] = res.val()
			cb(res.val())
		} else {
			cb(true)
		}
	})
}

global.validateAudioPIN = function (url, user) {
	fetch(url, {
		method: 'get',
		headers: {}
	}).then(function (response) {
		if (response.status !== 200) {
			console.log(new Date(), 'Get Media url error', response.status);

		} else {
			response.buffer().then(function (buffer) {
				console.log("video downloaded")
				//save locally for liveness check
				var fname = user.id + "_" + Date.now()
				fs.writeFile('./tempVids/' + fname, buffer, {}, (err, res) => {
					if (err) {
						console.error(err)
						return
					}
					console.log('video saved')
				})
				//save buffer to AWS
				aws.putObject("v3/coniski/demos/liveness3dplus/" + fname + ".mp4", buffer, "video/mp4", function (res) {
					if (res.status == "ok") {
						console.log(new Date(), "Media saved on AWS for", fname);
						console.log("https://s3.amazonaws.com/wsp2crmcdn/v3/coniski/demos/liveness3dplus/" + fname);
						//create transcribe job
						aws.transcribeVideo("wsp2crmcdn", "v3/coniski/demos/liveness3dplus", fname, "mp4", function (data) {
							if (data) {
								console.log(data)
								//create check transcribe status job
								var job = {
									type: "check-transcribe",
									contact: user.id,
									transJob: "TJ_" + fname,
									st: 'new',
									ts: Date.now(),
									runAfter: Date.now() + (4 * requeueInt)
								}
								firebase.database().ref("/accounts/" + account + "/botJobs/").push(job);
							}
						})
					} else {

					}
				});
			});
		}
	}).catch(function (err) {
		console.log(new Date(), 'Get Media API request error', err.message);
		console.log(err)
	});
}

global.validateAlfaPIN = function (job, trans, user) {
	console.log(job)
	console.log(JSON.stringify(trans))
	var arr = _.where(trans.results.items, { type: 'pronunciation' })
	if (arr.length >= 4) {
		var keys = []
		keys.unshift(arr[arr.length - 1].alternatives[0].content.toUpperCase())
		keys.unshift(arr[arr.length - 2].alternatives[0].content.toUpperCase())
		keys.unshift(arr[arr.length - 3].alternatives[0].content.toUpperCase())
		keys.unshift(arr[arr.length - 4].alternatives[0].content.toUpperCase())
		//keys.unshift(arr[arr.length-5].alternatives[0].content.toUpperCase())
		console.log(keys)
		//loop and convert to numbers and letters
		var key = ""
		if (numberMapES[keys[0]]) {
			key = numberMapES[keys[0]]
			console.log(key)
		} else {
			return false
		}
		if (numberMapES[keys[1]]) {
			key = key + numberMapES[keys[1]]
			console.log(key)
		} else {
			return false
		}
		//~ if (letts.indexOf(keys[1])>=0 || lettsMapES[keys[1]]) {
		//~ key = key + lettsMapES[keys[1]]	
		//~ console.log (key)
		//~ } else {
		//~ return false	
		//~ }
		if (numberMapES[keys[2]]) {
			key = key + numberMapES[keys[2]]
			console.log(key)
		} else {
			return false
		}
		if (numberMapES[keys[3]]) {
			key = key + numberMapES[keys[3]]
			console.log(key)
		} else {
			return false
		}
		//~ if (letts.indexOf(keys[3])>=0 || lettsMapES[keys[3]]) {
		//~ key = key + lettsMapES[keys[3]]
		//~ console.log (key)
		//~ } else {
		//~ return false	
		//~ }
		//~ if (numberMapES[keys[4]]) {
		//~ key = key + numberMapES[keys[4]]	
		//~ console.log (key)
		//~ } else {
		//~ return false	
		//~ }
		//so far we found all matches
		console.log(key, user.pin)
		if (key == user.pin) {
			return true
		} else {
			return false
		}

	} else {
		//test wont pass we need at least 4 items
		//push text validation failed return user to previos step
		return false;
	}
}

global.sendNewPin = function (job, user) {
	var pin = tools.generateNumberPin();
	var msg = "Lo sentimos, no hemos podido escuchar la clave correctamente\n\n";
	msg += "Recuerda pronuncia los números de tu clave de *uno en uno*:\n\n";
	msg += "Probemos una *clave nueva*. Envía un video selfie diciendo la siguiente frase:\n\n";
	msg += "_Mi clave segura es " + (pin + "").split("").join(" ") + "_";
	updateUser({ step: "3d1", pin: pin, tspin: Date.now() }, job.contact, user.line, user.chn);
	pushText(msg, job.contact, user.line, user.chn);
}

global.getDataRegCivil = function (ced, dacti, cb) {
	//check if we have this info cached
	firebase.database().ref('accounts/' + account + '/rc/' + ced).once('value', function (res) {
		if (res.val()) {
			console.log("Loaded RC data from cache")
			cb(res.val());
		} else {
			//fetch from UT API
			var body = {
				"apikey": env.apikey_RC,
				"uid": env.uid_RC,
				"cedula": ced,
				"coddact": dacti
			}
			console.log(body)
			fetch(env.url_apiRC, {
				method: 'post',
				body: JSON.stringify(body),
				headers: { "Content-Type": "application/json" }
			}).then(function (response) {
				if (response.status !== 200) {
					console.log(new Date(), 'Get RegCivil Data error', response.status);
					cb(false);
				} else {
					response.json().then(function (data) {
						//console.log (data)
						if (data.result) {
							//persist in cache
							data.tsc = Date.now()
							data.status = 'active'
							firebase.database().ref('accounts/' + account + '/rc/' + ced).set(data);
							cb(data);
						} else {
							cb(false);
						}
					});
				}
			}).catch(function (err) {
				console.log(new Date(), 'Get RegCivil Data error', err.message);
				console.log(err)
				cb(false);
			});
		}
	});
}

global.compareFaces = function (source, target, contact, cb) {
	//console.log (source.substring(0,25))
	//console.log (target.substring(0,25))
	//use Nexxit API to compare two faces
	var body = {
		"source": source,
		"target": target,
		"userRef": contact
	}

	fetch(env.nexxitAPIURL + '/compare-faces', {
		method: 'post',
		body: JSON.stringify(body),
		headers: { 'Content-Type': 'application/json', 'x-nexxit-key': env.nexxitAPIToken }
	}).then(function (response) {
		if (response.status !== 200) {
			console.log(new Date(), 'Compare faces error', response.status);
			response.text().then(function (data) {
				console.log(data)
			});
			cb({ status: 502 });
		} else {
			response.json().then(function (data) {
				cb(data)
			});
		}
	}).catch(function (err) {
		console.log(new Date(), 'Compare faces error', err.message);
		console.log(err)
		cb({ status: 502 });
	});
}

global.verificarLiveness = function (analyse_id, api_evidence, cb) {
	//use Nexxit API to check results of 2d liveness
	var body = {
		"analyze_id": analyse_id,
		"api_evidence": api_evidence
	}

	fetch(env.nexxitAPIURL + '/liveness/check', {
		method: 'post',
		body: JSON.stringify(body),
		headers: { 'Content-Type': 'application/json', 'x-nexxit-key': env.nexxitAPIToken }
	}).then(function (response) {
		if (response.status !== 200) {
			console.log(new Date(), 'check analyze error', response.status);
			response.text().then(function (data) {
				console.log(data)
			});
			cb({ status: 502 });
		} else {
			response.json().then(function (data) {
				cb(data)
			});
		}
	}).catch(function (err) {
		console.log(new Date(), 'check analyze error', err.message);
		console.log(err)
		cb({ status: 502 });
	});
}


global.checkLiveness2D = function (source, contact, cb) {
	//use Nexxit API to check 2D liveness
	var body = {
		"image": source,
		"userRef": contact
	}

	fetch(env.nexxitAPIURL + '/liveness/request', {
		method: 'post',
		body: JSON.stringify(body),
		headers: { 'Content-Type': 'application/json', 'x-nexxit-key': env.nexxitAPIToken }
	}).then(function (response) {
		if (response.status !== 200) {
			console.log(new Date(), '2d Liveness error', response.status);
			response.text().then(function (data) {
				console.log(data)
			});
			cb({ status: 502 });
		} else {
			response.json().then(function (data) {
				cb(data)
			});
		}
	}).catch(function (err) {
		console.log(new Date(), '2d Liveness error', err.message);
		console.log(err)
		cb({ status: 502 });
	});
}
