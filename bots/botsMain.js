// Common bots functions
var qs = require('querystring');
module.exports = {
	pushText: function  (body, contact, line, rep, type, desc, url, b64) {
		if (!rep) { rep = 'bot'; }
		if (!type) { type = 'txt'; }
		var msgId = Date.now()+"-"+line+"-"+contact;
		var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
		if (desc) { mt.desc = desc }
		if (url) { mt.url = url }
		if (b64) { mt.thumb = b64; }
		firebase.database().ref("/accounts/"+account+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = contact;
			ob.account = account;
			//push to account MT queue
			firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT Response POSTED OK");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});

	},

	setUserParam: function(key, val, contact) {
		firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).set(val);
	},

	updateUser: function(ob, contact) {
		firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob );
	},

	startSession: function (contact, user) {
		firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/tsso").set( Date.now() );
		this.logStartSession(contact, user.chn);
	},

	logStartSession: function(contact, chn) {
		var log = {
			type: "session-start",
			ts: Date.now(),
			ref: contact+"",
			chn: chn
		}
		firebase.database().ref("/accounts/"+account+"/logs").push(log);
	},

	logEndSession: function(contact, chn, tsso, op) {
		var log = {
			type: "session-end",
			tssx: Date.now(),
			ref: contact+"",
			ts: Date.now(),
			op: op
		}
		if (tsso) { log.tsso = tsso; }
		if (chn) { log.chn = chn; }
		firebase.database().ref("/accounts/"+account+"/logs").push(log);
	},

	logStartTicket: function(contact, chn) {
		var log = {
			type: "ticket-start",
			ts: Date.now(),
			ref: contact+"",
			chn: chn
		}
		firebase.database().ref("/accounts/"+account+"/logs").push(log);
	},

	resetUserParam: function(key, contact) {
		firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).remove();
	},

	saveForm: function(ob, cb) {
		firebase.database().ref("/accounts/"+account+"/forms").push(ob).then(function(res){
			cb(res.key);
		});
	},

	salir: function(job, user) {
		var self = this;
		if (user.tsso) { this.logEndSession(job.contact, user.chn, user.tsso, "s"); }
		var str = "Gracias por usar este servicio. Adios!";
		this.pushText (str, job.contact, user.line);
		this.resetUserParam('step', job.contact);
		this.resetUserParam('pin', job.contact);
		this.resetUserParam('condos', job.contact);
		this.resetUserParam('group', job.contact);
		this.resetUserParam('account', job.contact);
		this.resetUserParam('tsso', job.contact);
		this.resetUserParam('status', 'reset', job.contact);
	},

	resetBot: function(contact, user, msg) {
		if (user.tsso) { this.logEndSession(contact, user.chn, user.tsso, "r"); }
		if (msg) { this.pushText (msg, contact, user.line); }
		this.resetUserParam('step', contact);
		this.resetUserParam('pin', contact);
		this.resetUserParam('condos', contact);
		this.resetUserParam('group', contact);
		this.resetUserParam('account', contact);
		this.resetUserParam('tsso', contact);
		this.resetUserParam('status', 'reset', contact);
	},

	decr: function (str) {
		var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
		return decrypted.toString(CryptoJS.enc.Utf8);
	},

	encr: function (str) {
		return CryptoJS.AES.encrypt(str, env.hasherPwd).toString();
	},

	createTask: function(task, cb) {
		firebase.database().ref("/accounts/"+account+"/tasks").push(task).then(function(res) {
			task.id = res.key;
			if (cb) { cb(task); }
		}).catch(function(error){
			console.log (error)
			console.log (task);
			if (cb) { cb(null); }
		});
	},

	isShopOpen: function() {
		var d = new Date();
		var t = (d.getHours() * 100) + d.getMinutes();
		if (t>=config.officeOpen && t<config.officeClose) {
			return true;
		} else {
			return false;
		}
	}
}
