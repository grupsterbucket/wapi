// UT API plugin
module.exports = {
	initialize: function (options) {
		this.options = options;
		this.env = env[env.env][this.options.account]
	},

	checkStatus: function (dni, cb) {
		console.log("Checking UT Status", dni);
		var ob = {
			apikey: env.prod.ut.uanatacaApiKey,
			uid: env.prod.ut.uanatacaUID,
			numerodocumento: dni,
			tipo_solicitud: "PERSONA NATURAL"
		}

		fetch(env.prod.ut.uanatacaApiURL + '/v4/consultarEstado_BotWA002', {
			method: 'post',
			body: JSON.stringify(ob)
		}).then(function (response) {
			if (response.status !== 200) {
				console.log(new Date(), 'UT API error', response.status);
				response.json().then(function (data) {
					console.log(data);
				});
				cb(false);
			} else {
				response.text().then(function (data) {
					console.log("UT response length", data.length)
					if (tools.isJsonString(data)) {
						var ob = JSON.parse(data)
						if (ob.result) {
							//console.log (data.data.solicitudes)
							cb(ob.data.solicitudes);
						} else {
							cb(false)
						}
					} else {
						cb(false)
					}
				});
			}
		}).catch(function (err) {
			console.log(new Date(), 'UT API request error', err.message);
			cb(false);
		});
	},

	consultarFirmas: async function (numCedula, perfil, celular) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"apikey": env.prod.ut.uanatacaApiKey,
				"uid": env.prod.ut.uanatacaUID,
				"cedula": numCedula,
				"perfil": perfil,
				"celular": celular
			})
		}

		const respuestaFirmas = await fetch(env.prod.ut.uanatacaApiURL + '/v4/consultaXcedulaYperfil', options)
		const respuestaFinalFirmas = await respuestaFirmas.text()

		if (tools.isJsonString(respuestaFinalFirmas)) {
			return JSON.parse(respuestaFinalFirmas)
		} else {
			return { status: 502 }
		}
	},

	consultarFirmasAprobadas: async function (numCedula, celular, perfil) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"apikey": env.prod.ut.uanatacaApiKey,
				"uid": env.prod.ut.uanatacaUID,
				"cedula": numCedula,
				"celular": celular,
				"perfil": perfil
			})
		}

		const respuestaFirmas = await fetch(env.prod.ut.uanatacaApiURL + '/v4/consultaAprobadasXcedulaYperfil', options)
		const respuestaFinalFirmas = await respuestaFirmas.text()

		if (tools.isJsonString(respuestaFinalFirmas)) {
			return JSON.parse(respuestaFinalFirmas)
		} else {
			return { status: 502 }
		}
	},

	consultarSolicitudes: async function (numCedula, estado, celular) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"apikey": env.prod.ut.uanatacaApiKey,
				"uid": env.prod.ut.uanatacaUID,
				"estado": estado,
				"cedula": numCedula,
				"celular": celular
			})
		}

		const respuestaFirmas = await fetch(env.prod.ut.uanatacaApiURL + '/v4/consultaXcedulaYestado', options)
		const respuestaFinalFirmas = await respuestaFirmas.text()

		if (tools.isJsonString(respuestaFinalFirmas)) {
			return JSON.parse(respuestaFinalFirmas)
		} else {
			return { status: 502 }
		}
	},

	consultarSolicitudesConEstado: async function (numCedula, celular, perfil) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"apikey": env.prod.ut.uanatacaApiKey,
				"uid": env.prod.ut.uanatacaUID,
				"perfil": perfil,
				"cedula": numCedula,
				"celular": celular
			})
		}

		const respuestaFirmas = await fetch(env.prod.ut.uanatacaApiURL + '/v4/consultarESTADOxCedulaYperfil', options)
		const respuestaFinalFirmas = await respuestaFirmas.text()

		if (tools.isJsonString(respuestaFinalFirmas)) {
			return JSON.parse(respuestaFinalFirmas)
		} else {
			return { status: 502 }
		}
	},

	reenvioCredenciales: async function (numCedula, fecha, celular) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"apikey": env.prod.ut.uanatacaApiKey,
				"uid": env.prod.ut.uanatacaUID,
				"cedula": numCedula,
				"fecha": fecha,
				"celular": celular,
			})
		}
		

		const respuestaCred = await fetch(env.prod.ut.uanatacaApiURL + '/v4/reenviar_credencialesXcedulaYfecha', options)
		const respuestaFinalCred = await respuestaCred.text()

		if (tools.isJsonString(respuestaFinalCred)) {
			return JSON.parse(respuestaFinalCred)
		} else {
			return { status: 502 }
		}
	},

	reenvioCredencialesPiccolos: async function (token, celular) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"apikey": env.prod.ut.uanatacaApiKey,
				"uid": env.prod.ut.uanatacaUID,
				"token": token,
				"celular": celular,
			})
		}
		

		const respuestaCred = await fetch(env.prod.ut.uanatacaApiURL + '/v4/reenviar_credencialesXtoken', options)
		const respuestaFinalCred = await respuestaCred.text()

		if (tools.isJsonString(respuestaFinalCred)) {
			return JSON.parse(respuestaFinalCred)
		} else {
			return { status: 502 }
		}
	},


	reenvioCorreoDescarga: async function (token, celular) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"apikey": env.prod.ut.uanatacaApiKey,
				"uid": env.prod.ut.uanatacaUID,
				"token": token,
				"celular": celular
			})
		}
		

		const respuestaCred = await fetch(env.prod.ut.uanatacaApiURL + '/v4/reenviar_CorreoDeDescarga', options)
		const respuestaFinalCred = await respuestaCred.text()

		if (tools.isJsonString(respuestaFinalCred)) {
			return JSON.parse(respuestaFinalCred)
		} else {
			return { status: 502 }
		}
	},

	cancelarSolicitud: async function (token, estado, celular) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"apikey": env.prod.ut.uanatacaApiKey,
				"uid": env.prod.ut.uanatacaUID,
				"token": token,
				"estado": estado,
				"celular": celular
			})
		}
		

		const respuestaCred = await fetch(env.prod.ut.uanatacaApiURL + '/v4/cancelarXtokenYestado', options)
		const respuestaFinalCred = await respuestaCred.text()

		if (tools.isJsonString(respuestaFinalCred)) {
			return JSON.parse(respuestaFinalCred)
		} else {
			return { status: 502 }
		}
	},


	FirmasLargaDuracion: async function (opciones) {
		const respuestaFirmas = await fetch(envBot[account].UTAPIURL + '/solicitud', opciones)
		const respuestaFinalFirmas = await respuestaFirmas.text()

		if (tools.isJsonString(respuestaFinalFirmas)) {
			return JSON.parse(respuestaFinalFirmas)
		} else {
			return { status: 502 }
		}
	},

	consultarEstadoSolicitud: async function (numCedula) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].UTUid,
				"apikey": envBot[account].UTKey,
				"numerodocumento": numCedula,
				"tipo_solicitud": "PERSONA NATURAL"
			})
		}

		const respuestaEstado = await fetch(envBot[account].UTAPIURL + '/consultarEstado', options)
		const respuestaFinalEstado = await respuestaEstado.text()

		if (tools.isJsonString(respuestaFinalEstado)) {
			return JSON.parse(respuestaFinalEstado)
		} else {
			return { status: 502 }
		}
	},

	consultarPrecios: async function () {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].UTUid,
				"apikey": envBot[account].UTKey
			})
		}
		const response = await fetch('https://uanataca.ec/api_wa_ua/v1/consultar_precios', options)
		let data = await response.text()

		if (tools.isJsonString(data)) {
			return JSON.parse(data)
		} else {
			return { status: 502 }
		}
	},

	consultarTodosPrecios: async function () {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].UTUid,
				"apikey": envBot[account].UTKey
			})
		}
		const response = await fetch('https://uanataca.ec/api_wa_ua/v1/consultar_preciosTodos', options)
		let data = await response.text()

		if (tools.isJsonString(data)) {
			return JSON.parse(data)
		} else {
			return { status: 502 }
		}
	},

	consultarPreciosFac: async function () {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].certiUID,
				"apikey": envBot[account].certiAPIKey
			})
		}
		const response = await fetch('https://certificada.ec/api_wa_ua/v1/consultar_precios', options)
		let data = await response.text()

		if (tools.isJsonString(data)) {
			return JSON.parse(data)
		} else {
			return { status: 502 }
		}
	},

	consultarLinkPagos: async function (token) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].UTUidWeb,
				"apikey": envBot[account].UTKeyWeb,
				"token": token
			})
		}

		const respuestaPagos = await fetch('https://uanataca.ec/api_wa_ua/v1/consultar_link_pago', options)
		const respuestaFinalPagos = await respuestaPagos.text()

		if (tools.isJsonString(respuestaFinalPagos)) {
			return JSON.parse(respuestaFinalPagos)
		} else {
			return { status: 502 }
		}

	},

	consultarEstado: async function (numCedula) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].UTUid,
				"apikey": envBot[account].UTKey,
				"numerodocumento": numCedula,
				"tipo_solicitud": "PERSONA NATURAL"
			})
		}

		const respuestaEstado = await fetch(envBot[account].UTAPIURL + '/consultarEstado_BotWA002', options)
		const respuestaFinalEstado = await respuestaEstado.text()

		if (tools.isJsonString(respuestaFinalEstado)) {
			return JSON.parse(respuestaFinalEstado)
		} else {
			return { status: 502 }
		}
	},

	consultarVendedor: async function (codigoSM) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].UTUidWeb,
				"apikey": envBot[account].UTKeyWeb,
				"codigoDist": codigoSM
			})
		}

		const respuestaVendedor = await fetch('https://uanataca.ec/api_wa_ua/v1/consultar_vendedor', options)
		const respuestaFinalVendedor = await respuestaVendedor.text()

		if (tools.isJsonString(respuestaFinalVendedor)) {
			return JSON.parse(respuestaFinalVendedor)
		} else {
			return { status: 502 }
		}
	},

	consultarCodDescuento: async function (codigoDescuento) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].UTUidWeb,
				"apikey": envBot[account].UTKeyWeb,
				"coddescuento": codigoDescuento
			})
		}

		const respuestaCodDescuento = await fetch('https://uanataca.ec/api_wa_ua/v1/consultar_cod_descuento', options)
		const respuestaFinalCodDescuento = await respuestaCodDescuento.text()

		if (tools.isJsonString(respuestaFinalCodDescuento)) {
			return JSON.parse(respuestaFinalCodDescuento)
		} else {
			return { status: 502 }
		}
	},

	enviarComprobantePago: async function (token, fotoComprobante) {
		var options = {
			method: 'post',
			body: JSON.stringify({
				"uid": envBot[account].UTUid,
				"apikey": envBot[account].UTKey,
				"token": token,
				"f_comprobante": fotoComprobante
			})
		}

		const respuestaPago = await fetch('https://uanataca.ec/api_wa_ua/v1/enviar_comprobantePago', options)
		const respuestaFinalPago = await respuestaPago.text()

		if (tools.isJsonString(respuestaFinalPago)) {
			return JSON.parse(respuestaFinalPago)
		} else {
			return { status: 502 }
		}
	}
}
