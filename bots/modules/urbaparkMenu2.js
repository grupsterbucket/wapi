
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' urbaparkMenu2', user.id, step, group)
    console.log(job)
    switch (step) {
      // Me llega el saludo y el menu principal
      case "0":
        updateUser({ step: '1', module: "urbaparkMenu2" }, job.contact);
        pushText(mainMenu(), job.contact, user.line)
        break;

      // Me llega que opcion del menu principal selecciono y se redirije
      case "1":
        var kw = body.toLowerCase()
        var msg = ''
        if (["1", "1.", "uno"].indexOf(kw) >= 0) {
          //Abrir ticket
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
          //Abrir ticket
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["3", "3.", "tres"].indexOf(kw) >= 0) {
          //Abrir ticket
          if (isShopOpen()) {
            msg = 'Uno de nuestros especialistas se comunicará contigo.'
          } else {
            msg = config.horarioStr
          }
          updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
          pushText(msg, job.contact, user.line)
          logStartTicket(job.contact, user.chn, user, "sac");
        } else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {
          mods["main"].bot('Volver menu principal', job, user, "0.1")
        } else {
          pushText("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line);
        }
        break;
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo igorMenup")
    console.log(job)

    cb({ st: "done" })
  }
}

function mainMenu() {
  var msg = "Contamos con Planes Prepago de parqueo mensuales tanto para usuarios individuales como para empresas. Estos planes te brindan la comodidad de contar siempre con un espacio de estacionamiento seguro durante todo el mes, sin tener que preocuparte por realizar pagos diarios.\n\n"
  msg += "Por favor, selecciona una de las siguientes opciones:\n";
  msg += "1. Planes Prepago para usuarios individuales\n";
  msg += "2. Planes Prepago para empresas\n";
  msg += "3. Pre-compra de horas de parqueo \n";
  msg += "4. Volver al Menú Principal\n";
  return msg
}

function datos11() {
  var msg = "¡Perfecto! Si estás interesado en nuestros Planes Prepago para usuarios individuales, has tomado una excelente decisión.  Para obtener más información o adquirir uno de nuestros Planes Prepago para usuarios individuales, por favor proporciona los siguientes datos:\n\n"
  msg += "Nombre completo:\n";
  msg += "Correo electrónico:\n";
  msg += "Número de teléfono:\n";
  msg += "Sector:\n\n";
  msg += "Uno de nuestros representantes se comunicará contigo pronto para brindarte todos los detalles y ayudarte con el proceso de adquisición. ¡Gracias por elegir nuestros Planes Prepago y esperamos poder ofrecerte un excelente servicio de estacionamiento mensual!\n";
  return msg
}

function datos12() {
  var msg = "¡Excelente elección! Nuestros Planes Prepago para empresas te ofrecen una solución conveniente y personalizada para satisfacer las necesidades de estacionamiento de tu equipo.\n"
  msg += "Para obtener más información o adquirir uno de nuestros Planes Prepago para empresas, por favor proporciona los siguientes datos:\n\n";
  msg += "Nombre de la empresa:\n";
  msg += "Nombre del contacto:\n";
  msg += "Correo electrónico:\n";
  msg += "Número de teléfono:\n";
  msg += "Ciudad de ubicación:\n\n";
  msg += "Uno de nuestros representantes se pondrá en contacto contigo a la brevedad para brindarte todos los detalles y asistirte en el proceso de adquisición. ¡Gracias por elegir nuestros Planes Prepago para empresas y confiar en nuestros servicios de estacionamiento!\n";
  return msg
}

function datos13() {
  var msg = "¡Excelente elección! La pre-compra de horas de parqueo es una opción conveniente para asegurarte un tiempo determinado de estacionamiento para ti o tus clientes, sin tener que preocuparte por pagar cada vez que ingreses al parqueadero. \n\n"
  msg += "Para brindarte más información y asistirte con la pre-compra de horas de parqueo, necesitamos algunos detalles adicionales. Por favor, proporciona la siguiente información:\n\n";
  msg += "•	Cantidad de horas de parqueo que deseas pre-comprar.\n";
  msg += "•	Ubicación del parqueadero deseado \n";
  msg += "•	Método de pago preferido (tarjeta de crédito, transferencia bancaria, etc\n\n";
  msg += "Una vez que recibamos estos detalles, nuestro equipo te guiará a través del proceso de pre-compra de horas de parqueo y te proporcionará los pasos necesarios para completar la transacción. Si tienes alguna pregunta adicional o necesitas asistencia adicional, no dudes en hacerlo. ¡Estamos aquí para ayudarte!\n";
  return msg
}



