// FaceTec API plugin
module.exports = {

	liveness2D: function(b64, cb) {
		console.log ("Checking Liveness 2D");
		
		fetch(env.FaceTecBaseURL+'/liveness-2d', {
			method: 'post',
			headers: { 'X-Device-Key' : ''+env.DeviceKeyIdentifier+'' ,
					   'Accept': 'application/json',
					   'Content-Type': 'application/json'
			},
			body: JSON.stringify({image: b64})
		}).then(function(response) {
			if (response.status !== 200 ) {
				console.log(new Date(),'FaceTec API error',response.status);
				response.json().then(function(data) {
					console.log(data);
				});
				cb (false);
			} else {
				response.text().then(function(data) {
					console.log ("FaceTec response length", data.length)
					if (tools.isJsonString(data)) {
						cb (JSON.parse(data))	
					} else {
						cb (false)	
					}
				});
			}
		}).catch(function(err) {
			console.log(new Date(),'FaceTec API request error',err.message);
			cb (false);
		});
	}
}
