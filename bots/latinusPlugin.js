// Bot as module
module.exports = {
	bot: function (body, job, user, step, group, errCount) {
		console.log("Processing latinus sac bot", step, group)
		switch (step) {
			case "0":
				setUserParam("step", "lns1", job.contact, user.line);
				pushText(mainMenu(true), job.contact, user.line);
				break;
			//end step 0
			case "lns1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {

					//ventas
					setUserParam("step", "lns1.1", job.contact, user.line);
					pushText(Ventas(), job.contact, user.line);

				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {

					//uso
					setUserParam("step", "lns2.1", job.contact, user.line);
					pushText(Uso(), job.contact, user.line);

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {

					//confirmar pago
					var msg = "Si deseas revisar el estado de tu pago por favor ingresa el ID del trámite. El ID del trámite está en el correo electrónico que recibiste al momento de registrar tu solicitud."
					setUserParam("step", "lns3.1", job.contact, user.line);
					pushText(msg, job.contact, user.line);

				} else if (["4", "4.", "cuatro"].indexOf(kw) >= 0) {

					//estado proceso
					var msg = "Si deseas revisar el estado de la solicitud por favor ingresa el ID del trámite. El ID del trámite está en el correo electrónico que recibiste al momento de registrar tu solicitud."
					setUserParam("step", "lns4.1", job.contact, user.line);
					pushText(msg, job.contact, user.line);


				} else if (["5", "5.", "cinco"].indexOf(kw) >= 0) {

					//olvide clave
					var msg = 'En el caso que se haya olvidado su clave debe realizar el proceso de "Recuperación por Olvido de Clave" en el link https://signer.latinus.net/latinus-app/#/login \nDebe tener el número de serie para acceder; está en su contrato en la sección vigésima.\nSi es primera vez que realiza el proceso no tiene costo si es una segunda vez tiene un costo de $24,64. En la contraseña digite solo números, letras (una mayúscula por lo menos y evitar usar la letra ñ). Caracteres especiales solo le permite @  #  $  %'
					setUserParam("step", "lns-back", job.contact, user.line);
					pushText(msg + "\n\n" + GoBackMenu(), job.contact, user.line);

				} else if (["6", "6.", "seis"].indexOf(kw) >= 0) {

					//revocatoria
					var msg = 'Debes ingresar a https://signer.latinus.net/latinus-app/#/login y escoger la opción de "Revocación De Firma Electrónica". \nDebe tener el número de serie para acceder, está en su contrato en la sección vigésima. Si no dispone de su contrato puede comunicarse con un operador (opción 8 del menú principal).'
					setUserParam("step", "lns-back", job.contact, user.line);
					pushText(msg + "\n\n" + GoBackMenu(), job.contact, user.line);

				} else if (["7", "7.", "siete"].indexOf(kw) >= 0) {

					//distris
					//~ pushText (Distris(), job.contact, user.line);
					setUserParam("step", "lns-back", job.contact, user.line);
					pushText(Distris() + "\n\n" + GoBackMenu(), job.contact, user.line);

				} else if (["8", "8.", "ocho"].indexOf(kw) >= 0) {

					//validacion de documentos
					if (isShopOpen()) {
						var msg = "Un momento por favor, te atenderemos lo más pronto posible"
						updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "sac");
					} else {
						setUserParam("step", "lns1", job.contact, user.line);
						pushText(config.horarioStr + "\n\n" + mainMenu(false), job.contact, user.line);
					}

				} else if (["9", "9.", "nueve"].indexOf(kw) >= 0) {

					//operador
					if (isShopOpen()) {
						var msg = "Un momento por favor, te atenderemos lo más pronto posible"
						updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "sac");
					} else {
						setUserParam("step", "lns1", job.contact, user.line);
						pushText(config.horarioStr + "\n\n" + mainMenu(false), job.contact, user.line);
					}

				} else if (["0", "0.", "cero"].indexOf(kw) >= 0) {

					salir(job, user);

				} else {

					stepError("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line, errCount, user);

				}
				break;

			case "lns1.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {

					//comprar firma
					var msg = "La Firma Electrónica en formato archivo (.p12) es para personas naturales (No para personas jurídicas), todo el trámite se lo realiza en línea, el costo es de $22.00 + IVA ($25.30) por dos años. Es necesario disponer de un dispositivo con cámara y de tu cédula física. El pago lo puedes hacer mediante tarjeta de débito o crédito para mayor rapidez. Si realizas el pago con transferencia o depósito debes esperar que el pago se efectivice en unas horas. Debes ingresar al link https://signer.latinus.net/latinus-app/#/login para que puedas iniciar el proceso."
					setUserParam("step", "lns-back", job.contact, user.line);
					pushText(msg + "\n\n" + GoBackMenu(), job.contact, user.line);

				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {

					//renovar firma
					var msg = "Se renovará la firma si fue emitida como archivo para persona natural y por el BCE.\nEl costo es $25.30 por dos años. Para la RENOVACIÓN DE FIRMA ELECTRÓNICA debes ingresar en el link https://signer.latinus.net/latinus-app/#/login y seleccionar RENOVACIÓN DE FIRMA ELECTRÓNICA. El número de serie está en la sección vigésima de su contrato entregado con la firma que desea renovar. Si no dispone de su contrato puede comunicarse con un operador (opción 8 del menú principal)."
					setUserParam("step", "lns-back", job.contact, user.line);
					pushText(msg + "\n\n" + GoBackMenu(), job.contact, user.line);

				} else {

					stepError("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line, errCount, user);

				}
				break;

			case "lns2.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {

					//descarga
					setUserParam("step", "lns-back", job.contact, user.line);
					pushText(Descarga() + "\n\n" + GoBackMenu(), job.contact, user.line);

				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {

					//firma EC
					setUserParam("step", "lns-back", job.contact, user.line);
					pushText(FirmaEC() + "\n\n" + GoBackMenu(), job.contact, user.line);

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {

					//facturación
					var msg = "Si no cuentas con un sistema de facturación electrónico privado, puedes utilizar el sistema de facturación gratuito del SRI: https://sriyyoenlinea.sri.gob.ec/portal-sriyyo-internet/login.html \n Para conocer cómo utilizarlo puedes ingresar al siguiente tutorial link: https://www.youtube.com/watch?v=cKQQ5wDmw6A "
					setUserParam("step", "lns-back", job.contact, user.line);
					pushText(msg + "\n\n" + GoBackMenu(), job.contact, user.line);

				} else {

					stepError("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line, errCount, user);

				}
				break;

			case "lns3.1":

				//check status pago
				var kw = body.toLowerCase()
				latinusAPI.checkStatusPago(kw.substring(0, 13), function (data) {
					console.log(data)
					if (data) {
						if (data.codigoStatus == "200") {
							var msg = data.mensaje.replace("encontro", "encontró") + "\n\n*Estado:* " + mapPagoEstado(data.data.estado) + "\n*Medio:* " + data.data.medio + "\n";
							if (data.data.estado == "Pendiente" || data.data.estado == "PENDING") {
								msg += "Recuerda que el proceso de tu solicitud puede demorar hasta una hora y si pagaste con transferencia debemos primero validar tu pago\n"
								updateUser({ step: "lns-open-ticket", group: "pag" }, job.contact, user.line);
								pushText(msg + "\n\n" + openTicket(), job.contact, user.line);
							} else if (data.data.estado == "REJECTED" || data.data.estado == "ERROR" || data.data.estado == "Reembolsado") {
								updateUser({ step: "lns-open-ticket", group: "sac" }, job.contact, user.line);
								pushText(openTicket(), job.contact, user.line);
							} else {
								setUserParam("step", "lns1", job.contact, user.line);
								pushText(msg + "\n\n" + mainMenu(false), job.contact, user.line);
							}
						} else {
							operador
							setUserParam("step", "lns1", job.contact, user.line);
							pushText("No hemos podido validar tu solicitud con el ID de trámite enviado.\n\n" + mainMenu(false), job.contact, user.line);
						}
					} else {
						setUserParam("step", "lns1", job.contact, user.line);
						pushText("No hemos podido validar tu solicitud con el ID de trámite enviado.\n\n" + mainMenu(false), job.contact, user.line);
					}
				});
				break;

			case "lns4.1":

				//check status solicitud
				var kw = body.toLowerCase()
				latinusAPI.checkStatus(kw.substring(0, 13), function (data) {
					console.log(data)
					if (data) {
						if (data.codigoStatus == "200") {
							var msg = data.mensaje.replace("encontro", "encontró") + "\n\n*Estado:* " + mapEstado(data.data.estadoSolicitud) + "\n";
							if (data.data.estadoSolicitud == "Pendiente" || data.data.estadoSolicitud == "Pagado" || data.data.estadoSolicitud == "Archivado") {
								msg += "Recuerda que el proceso de tu solicitud puede demorar hasta una hora y si pagaste con transferencia debemos primero validar tu pago.\n"
								updateUser({ step: "lns-open-ticket", group: "sac" }, job.contact, user.line);
								pushText(msg + "\n\n" + openTicket(), job.contact, user.line);
							} else {
								setUserParam("step", "lns1", job.contact, user.line);
								pushText(msg + "\n\n" + mainMenu(false), job.contact, user.line);
							}
						} else {
							setUserParam("step", "lns1", job.contact, user.line);
							pushText("No hemos podido validar tu solicitud con el ID de trámite enviado.\n\n" + mainMenu(false), job.contact, user.line);
						}
					} else {
						setUserParam("step", "lns1", job.contact, user.line);
						pushText("No hemos podido validar tu solicitud con el ID de trámite enviado.\n\n" + mainMenu(false), job.contact, user.line);
					}
				});
				break;

			case "lns7.1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {

					//si no distri
					updateUser({ step: "lns-open-ticket", group: "dis" }, job.contact, user.line);
					pushText(openTicket(), job.contact, user.line);

				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {

					//valida manual
					if (isShopOpen()) {
						var msg = "Por favor envía la siguiente información del cliente:\n\n"
						msg += "- Número de cédula\n"
						msg += "- Email\n"
						msg += "- Número de celular\n"
						msg += "- Enviar video del cliente diciendo\n"
						msg += "Hoy ... (fecha) ... yo ... (nombres completos) ... con cédula de identidad ... autorizo a Latinus SA la emisión de mi firma electrónica con vigencia de dos años\n"
						msg += "- Fotos de la cédula por ambos lados de manera horizontal lo más claro posible\n"
						updateUser({ step: "idle", status: "active", group: "dis", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, "dis");
					} else {
						setUserParam("step", "lns1", job.contact, user.line);
						pushText(config.horarioStr + "\n\n" + mainMenu(false), job.contact, user.line);
					}

				} else if (["3", "3.", "tres"].indexOf(kw) >= 0) {

					//estado proceso
					var msg = "Por favor ingresa el ID del trámite que deseas revisar el estado de la solicitud. El ID del trámite está en el correo electrónico enviamos al cliente."
					setUserParam("step", "lns4.1", job.contact, user.line);
					pushText(msg, job.contact, user.line);

				} else {

					stepError("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line, errCount, user);

				}
				break;

			case "lns-back":
				var kw = body.toLowerCase()
				var group = ""
				if (["salir", "*salir*"].indexOf(kw) >= 0) {
					salir(job, user);
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					salir(job, user);
				} else {
					setUserParam("step", "lns1", job.contact, user.line);
					pushText(mainMenu(false), job.contact, user.line);
				}
				break;
			case "lns-open-ticket":
				var kw = body.toLowerCase()
				var group = ""
				if (["1", "1.", "uno"].indexOf(kw) >= 0) {
					if (isShopOpen()) {
						var msg = "Un momento por favor, en seguida te atendemos"
						updateUser({ step: "idle", status: "active", tso: Date.now() }, job.contact, user.line);
						pushText(msg, job.contact, user.line);
						logStartTicket(job.contact, user.chn, user, user.group);
					} else {
						setUserParam("step", "lns1", job.contact, user.line);
						pushText(config.horarioStr + "\n\n" + mainMenu(false), job.contact, user.line);
					}
				} else if (["2", "2.", "dos"].indexOf(kw) >= 0) {
					setUserParam("step", "lns1", job.contact, user.line);
					pushText(mainMenu(false), job.contact, user.line);
				} else {
					stepError("Por favor selecciona una de las opciones del menú o escribe *salir* para terminar la sesión", job.contact, user.line, errCount, user);
				}
				break;
		}//end switch step

		function mainMenu(greet) {
			if (greet) {
				var msg = "Gracias por comunicarte con el canal de servicio de *LATINUS* por favor selecciona una opción:\n\n";
			} else {
				var msg = "Por favor selecciona una opción:\n\n";
			}
			msg += "1. COMPRA/RENOVACIÓN DE FIRMA\n";
			msg += "2. CÓMO DESCARGAR Y USAR MI FIRMA\n";
			msg += "3. QUIERO CONFIRMAR MI PAGO\n";
			msg += "4. ESTADO DE MI PROCESO\n";
			msg += "5. OLVIDÉ MI CLAVE\n";
			msg += "6. REVOCATORIA\n";
			msg += "7. DISTRIBUIDORES\n";
			msg += "8. VALIDACIÓN DE DOCUMENTOS\n";
			msg += "9. OPERADOR\n";
			msg += "0. SALIR\n";
			return msg;
		}

		function GoBackMenu() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Regresar al menú principal\n";
			msg += "2. Salir";
			return msg;
		}

		function openTicket() {
			var msg = "Deseas comunicarte con un operador?\n\n";
			msg += "1. Si\n";
			msg += "2. No";
			return msg;
		}

		function TipoPago() {
			var msg = "Cómo realizaste tu pago?\n\n";
			msg += "1. Transferencia o depósito\n";
			msg += "2. Tarjeta de crédito o débito";
			return msg;
		}

		function Distris() {
			var msg = "Si quieres ser distribuidor por favor envía la siguiente información al correo karina.suarez@latinus.net:\n\n";
			msg += "- Nombres completos\n";
			msg += "- Cedula de identidad\n";
			msg += "- Correo electrónico\n";
			msg += "- Celular de contacto\n";
			msg += "\n";
			msg += "Adjuntar:\n";
			msg += "- Fotografía de la cedula\n";
			msg += "- RUC en caso de tener\n";
			//~ var msg = "Selecciona una opción:\n\n";
			//~ msg += "1. Quieres ser distribuidor?\n";
			//~ msg += "2. Solicitar validación manual\n";
			//~ msg += "3. Consultar estado de solicitud";
			return msg;
		}

		function Ventas() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. COMPRA TU FIRMA\n";
			msg += "2. QUIERO RENOVAR MI FIRMA";
			return msg;
		}

		function Uso() {
			var msg = "Selecciona una opción:\n\n";
			msg += "1. Cómo descargar mi firma?\n";
			msg += "2. Firma de documentos con FirmaEC\n";
			msg += "3. Facturación Electrónica SRI y YO en linea\n";
			return msg;
		}

		function Descarga() {
			var msg = "¿Como descargar correctamente mi firma desde mi mail?\n\n";
			msg += "- Abrir el correo emitido por parte de ecienlinea@bce.ec con asunto *Notificaciones Certificados. Certificado Creado*\n";
			msg += "- Podrás visualizar tres archivos\n";
			msg += "- Visualizar un botón que dice descargar todo*\n";
			msg += "- Le das clic en el mismo y a los pocos segundos se descargará\n";
			msg += "- Le das clic derecho sobre la carpeta obtenida y luego en extraer aquí\n";
			msg += "- Ya podrás usar tu firma, la cual es el archivo Firma_ tu_número_de_CI.p12\n";
			return msg;
		}

		function FirmaEC() {
			var msg = "Guía para el uso de FirmaEC\n\n";
			msg += "- Descargar aplicativo para firmar https://www.firmadigital.gob.ec/descargar-firmaec/  \n";
			msg += "- Manual de usuario pág. 20 instrucciones para firmar documentos https://www.firmadigital.gob.ec/wp-content/uploads/2021/08/Manual-Usuario-FirmaEC-v2.8.0.pdf  \n";
			msg += "- Video Manual como usar firma EC https://www.youtube.com/watch?v=QnBdX4FFTEM&ab_channel=GobiernoElectr%C3%B3nicoEcuador  \n";
			return msg;
		}

		function mapPagoEstado(str) {
			switch (str) {
				case "APPROVED":
					return "Aprobado";
				case "REJECTED":
					return "Rechazado";
				case "ERROR":
					return "Error";
				case "PENDING":
					return "Pendiente";
				default:
					return str;
			}
		}

		function mapEstado(str) {
			switch (str) {
				case "Pendiente":
					return "La solicitud esta pendiente de aprobar el pago";
				case "Archivado":
					return "La solicitud esta pendiente de aprobar el pago.";
				case "Pagado":
					return "La solicitud se encuentra pagada, pendiente por emitirse la firma.";
				case "Emitido":
					return "La firma electrónica fue emitida.";
				case "Finalizada":
					return "La firma electrónica fue emitida y la solicitud fue finalizada exitosamente.";
				default:
					return str;
			}
		}

	}
}
