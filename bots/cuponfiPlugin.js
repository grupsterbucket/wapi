// Cuponfi Bot as module
module.exports = {
	bot: function(body, job, user, step, group, account, contact) {
		console.log ("Processing cuponfi mo", step, group)
		//only run if not a group chat
		if (job.contact.indexOf("-")<0) {
			switch (step) {			
				case "0":
					var kw = body.toLowerCase()
					pushText (cuponfiWelcome(), job.contact, user.line);
					break;
					//end step 0
				case "c1":
					//find matches 
					var parts = body.split("@")
					pushText ("Un momento por favor...", job.contact, user.line);
					var matches = geo.findCupons(Number(parts[0]),Number(parts[1]), config.radioSearch);
					if (matches.length>0) {
						//sort by distance
						var matches = _.sortBy(matches, function(ob, key) { return ob.dist });
						var ob = { step:"c2", cupones:matches, page:1 }
						updateUser(ob, job.contact, user.line);
						if (matches.length>10) {
							pushText ("Digita una opción para imprimir tu cupón o escribe *más* para ver más opciones:\n\n"+buildCuponesMenu(matches, 1), job.contact, user.line);
						} else {
							pushText ("Digita una opción para imprimir tu cupón:\n\n"+buildCuponesMenu(matches, 1), job.contact, user.line);
						}
					} else {
						pushText ("Lo sentimos, no existen resultados para tu ubicación.", job.contact, user.line);
					}
					break;
				case "c2":
					//find cupon by idx and page
					var kw = body.toLowerCase();
					if (kw == "mas" || kw == "más") {
						if (user.cupones.length>10) {
							var ob = { page: page+1 }
							updateUser(ob, job.contact, user.line);
							pushText ("Digita una opción para imprimir tu cupón:\n\n"+buildCuponesMenu(matches, page+1), job.contact, user.line)
						} else {
							pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
						}	
					} else {
						if (isNaN(kw)) {
							pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
						} else {
							var idx = (parseInt(kw)-1) + ((user.page-1)*10);
						}
						console.log ("selected cupon", idx);
						if (user.cupones[idx]) {
							var cupon = user.cupones[idx];
							printCupon(cupon, job.contact, function(cuprint) {
								var fname = cuprint.id+".png"
								qr.generateCodeFromStringToAWS(fname, "cuponfi:"+cuprint.id, function(url) {
									if (url) {
										console.log (new Date(), "Media saved on server for", fname);
										var msg = "*"+cupon.local.name+"*\n";
										msg += ""+cupon.name+"\n";
										if (cupon.notes.length>0) { msg += "_"+cupon.notes+"_\n"; }
										msg += 'Ver Mapa:\nhttps://maps.google.com/maps?q='+cupon.local.lat+'%2C'+cupon.local.lon+'&z=17&hl=es\n';
										msg += "\n";
										msg += "*Instrucciones:* presenta este código QR para canjear tu cupón."
										console.log (url)
										pushText (msg, job.contact, user.line, "bot", "img", null, url, null);
										setTimeout( function() { salir(job, user); }, 3000 );
									} else {
										pushText ("Mil disculpas, este servicio está en mantenimiento.", job.contact, user.line);
										setTimeout( function() { salir(job, user); }, 3000 );
									}
								});
							})
							
						} else {
							pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
						}
					}
					break;
				case "u1":
					var kw = body.toLowerCase()
					pushText ("Solo para usuarios registrados", job.contact, user.line);
					break;
				case "r1":
					var kw = body.toLowerCase();
					if (kw.indexOf("cuponfi")>=0) {
						//valid cupon
						var parts = kw.split(":");
						var cid = parts[1];
						var ref = parts[2];
						if (!ref) { ref = ""}
						if (ref == "[imagen]") { ref = "N/A"}
						if (ref.length == 0) { ref = "N/A"; }
						console.log (cid, ref);
						//get cuprint record
						getCuprint(cid, function(cuprint) {
							if (cuprint) {
								//todo: chech multi uso
								if (cuprint.isCan) {
									pushText ("Este cupón ya fue canjeado", job.contact, user.line);	
								} else {
									//get cupon for this cuprint
									getCupon(cuprint.cid, function(cupon) {
										if (cupon) {
											//is contact group same as cupon group
											if (cupon.group == contact.group) {
												//redem cupon
												var ob = {contCan: job.contact, isCan: true, tsCan: Date.now(), canRef: ref }
												updateCuprint(cid, ob);
												pushText ("Cupón redimido con éxito. Ref: "+ref, job.contact, user.line);	
											} else {
												pushText ("Este cupón no corresponde a la cuenta del contacto registrado", job.contact, user.line);		
											}
										} else {
											pushText ("No existe este cupón", job.contact, user.line);		
										}
									});
								}
							} else {
								pushText ("No existe este cupón", job.contact, user.line);	
							}
						});
					} else {
						pushText ("La imagen no corresponde a un QR Cuponfi", job.contact, user.line);
					}
					
					break;
				
			} //send switch steps
		}//end check if group
		
		function cuponfiWelcome() {
			var msg = "";
			msg += "¡Hola, bienvenid@ a Cuponfi!\n\n"; 
			msg += "Por favor comparte tu ubicación para recibir cupones de ofertas cercanas a ti."
			return msg;	
			
		}
		
		function buildCuponesMenu(arr, page) {
			var str = "";
			var from = (page - 1) * 10;
			var to = from+9;
			if (to>arr.length) { to = arr.length; }
			for (var i=from; i<to; i++) {
				str += (i+1) + ". " + arr[i].name + " en " + arr[i].local.name ;	
			}
			return str;
		}
		
		function printCupon(cupon, contact, cb) {
			var cuprint = {
				"tsc": Date.now(), 
				"tsCan": 0,
				"qrStr": cupon.qrString,
				"cid": cupon.id,
				"group": cupon.group,
				"local": cupon.local.id,
				"isCan": false,
				"contPnt": contact,
				"contCan": ""
			}
			var cid = Date.now()+"-"+contact;
	
			firebase.database().ref("/accounts/cuponfi/cuprints/"+cid).set(cuprint).then(function() {
				cuprint.id = cid;
				cb(cuprint);
			});
		}
	}
}
