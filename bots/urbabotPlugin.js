// Urbabot Bot as module
var qs = require('querystring');
module.exports = {
	bot: function(body, job, user, step, group, account) {
		console.log ("Processing condo sub bot", step, group, job.contact, "job ID", job.id)
		switch (step) {		
			case "0":
				var kw = body.toLowerCase()
				//find contact in DB
				getContact(job.contact, function(contact) {
					if (contact) {
						console.log (job.contact , "contact found", contact.id)
						if (contact.status == "active") {
							var gs = []
							if (contact.group) {
								if (contact.group.length>0) {
									gs = contact.group.split(",");
								}
							}
							if (gs.length>0) {
								var condos = []
								loadCondos(gs, condos, function(condos) {
									if (condos.length == 1) {
										getContactUnitsInGroup(contact.id, condos[0].id, function(units) {
											if (units.length>0) {
												if (units.length==1) {
													var msg = welcomeMessage(true, contact.name);
													var ob = { step:"1", group: condos[0].id, condos: condos, name: contact.name, account: contact.id, units: units, unit: units[0] }
													if ( contact["x-dni"] ) { ob.dni = contact["x-dni"] }
													updateUser(ob, job.contact, user.line);
													pushText (msg, job.contact, user.line);
												} else {
													//need to select unit
													var ob = { step:"2u", group: condos[0].id, condos: condos, name: contact.name, account: contact.id, units: units }
													var msg = "Hola! Por favor digita una opción:\n\n";	
													for (var i=0; i<units.length; i++) {
														var parts = units[i].split("@")
														if (parts[0] == "all") {
															msg += (i+1)+". Unidad "+parts[0]+"\n"
														} else {
															msg += (i+1)+". Unidad "+parts[0]+" torre "+parts[1]+"\n"
														}
													}
													updateUser(ob, job.contact, user.line);
													pushText (msg, job.contact, user.line);
												}
											} else {
												var msg = "Hola! Tu contacto no ha sido asignada a un rol en el inmueble, por favor contacta con administración para actualizar tu información.\nGracias!";
												pushText (msg, job.contact, user.line);
												salir(job, user);	
											}										
										});
									} else {
										//need to select building
										updateUser({step:"2", condos: condos, name: contact.name, account: contact.id}, job.contact, user.line);
										var msg = "Hola! Por favor escoge un condomino:\n\n";	
										for (var i=0; i<condos.length; i++) {
											msg += (i+1)+". "+condos[i].name+"\n"
										}
										pushText (msg, job.contact, user.line);
									}
								});
							} else {
								var msg = "Hola! Tu contacto no ha sido asignada a un inmueble, por favor contacta con administración para actualizar tu información.\nGracias!";
								pushText (msg, job.contact, user.line);
							} 
						} else {
							var msg = "Hola! Tu cuenta está desactivada, por favor contacta con administración.\nGracias!";
							pushText (msg, job.contact, user.line);
						}
					} else {
						setUserParam("step","888", job.contact, user.line);
						pushText (menuVisitors(), job.contact, user.line);
					}
				});
				break;
				//end step 0
			case "1":
				var kw = body.toLowerCase()
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Digita una opción:\n\n";
					msg += "1. ESTADO DE CUENTA\n";
					msg += "2. CONFIRMAR TU PAGO DE ALICUOTA\n";
					msg += "3. PROBLEMA CON MI ESTADO DE CUENTA\n";
					msg += "4. REGRESAR\n";
					setUserParam("step","1.1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var condo = _.findWhere(user.condos, {id: user.group});
					if (condo.hasRes) {
						if (!condo.config) { condo.config = {
						  "acom" : "",
						  "conta" : "no",
						  "linkres" : "",
						  "msgres" : "",
						  "normas" : "",
						  "notif" : "0984252217",
						  "reco" : "",
						  "regla" : "",
						  "sos" : ""
						}}
						if (!condo.config.linkres) { condo.config.linkres = "" }
						if (!condo.config.msgres) { condo.config.msgres = "" }
						if (condo.config.linkres.length>0 && condo.config.linkres.indexOf("http")==0) {
							var msg = "Por favor utiliza el siguiente enlace para gestionar tus reservas:\n\n";
							msg += condo.config.linkres;
							msg += welcomeMessage(false);
							updateUser({step:"1"}, job.contact, user.line);
							pushText (msg, job.contact, user.line);
						} else {
							var msg = "Digita una opción:\n\n";
							msg += "1. HACER UNA RESERVA\n";
							msg += "2. VER/CANCELAR TUS RESERVAS\n";
							msg += "3. REGRESAR\n";
							console.log (condo.config)
							if (condo.config.msgres.length>0) {
								msg += "\n";
								msg += condo.config.msgres;
							}
							setUserParam("step","1.2", job.contact, user.line);
							pushText (msg, job.contact, user.line);
						}
					} else {
						var msg = "Al momento no se pueden realizar reservas en este inmueble";
						pushText (msg, job.contact, user.line);
					}
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = "Digita una opción:\n\n";
					msg += "1. ADMINISTRACIÓN\n";
					msg += "2. TARJETAS DE INGRESO\n";
					msg += "3. OTROS\n";
					setUserParam("step","1.3", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["4","4.","cuatro"].indexOf(kw)>=0) {
					var msg = "Digita una opción:\n\n";
					msg += "1. REGLAMENTO INTERNO DEL CONDOMINIO\n";
					msg += "2. ACOMETIDAS DEL CONDOMINIO\n";
					msg += "3. BASURA Y RECICLAJE\n";
					msg += "4. REGRESAR\n";
					setUserParam("step","1.4", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["5","5.","cinco"].indexOf(kw)>=0) {
					salir(job, user);
				} else {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "2":
				// select condo if  multiple
				var kw = body.toLowerCase()
				if (isNaN(kw)) {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				} else {
					var idx = parseInt(kw)-1;
					if (user.condos[idx]) {
						getContactUnitsInGroup(user.account, user.condos[idx].id, function(units) {
							if (units.length>0) {
								if (units.length==1) {
									var msg = welcomeMessage(true, user.name);
									var ob = { step:"1", group: user.condos[idx].id, units: units, unit: units[0] }
									updateUser(ob, job.contact, user.line);
									pushText (msg, job.contact, user.line);
								} else {
									//need to select unit
									var ob = { step:"2u", group: user.condos[idx].id, units: units }
									var msg = "Hola! Por favor escoge una opción:\n\n";	
									for (var i=0; i<units.length; i++) {
										var parts = units[i].split("@")
										if (parts[0] == "all") {
											msg += (i+1)+". Depto "+parts[0]+"\n"
										} else {
											msg += (i+1)+". Depto "+parts[0]+" torre "+parts[1]+"\n"
										}
									}
									updateUser(ob, job.contact, user.line);
									pushText (msg, job.contact, user.line);
								}
							} else {
								var msg = "Hola! Tu cuenta no ha sido asignada a un rol en el inmueble, por favor contacta con administración para actualizar tu información.\nGracias!";
								pushText (msg, job.contact, user.line);		
								salir(job, user);	
							}
							//~ if (units.length==0) { units.push("N/A") }
							//~ var msg = welcomeMessage(true, user.name);
							//~ updateUser({step:"1", group: user.condos[idx].id, units: units}, job.contact, user.line);
							//~ pushText (msg, job.contact, user.line);
						});
						
					} else {
						pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
					}
				}
				break;
			case "2u":
				// select condo if  multiple
				var kw = body.toLowerCase()
				if (isNaN(kw)) {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				} else {
					var idx = parseInt(kw)-1;
					if (user.units[idx]) {
						var msg = welcomeMessage(true, user.name);
						updateUser({step:"1", unit: user.units[idx]}, job.contact, user.line);
						pushText (msg, job.contact, user.line);
					} else {
						pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
					}
				}
				break;
			case "1.1":
				var kw = body.toLowerCase()
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var condo = _.findWhere(user.condos, {id: user.group});
					if (!condo.config) {
						//user default
						condo.config = {
						  "acom" : "",
						  "conta" : "no",
						  "linkres" : "",
						  "msgres" : "",
						  "normas" : "",
						  "notif" : "0984252217",
						  "reco" : "",
						  "regla" : "",
						  "sos" : ""
						}
					}
					if (condo.config.conta == "si") { //this condo's conta is managed with Andamio
						pushText ("Un momento por favor", job.contact, user.line);
						getBalance(user.account, user.group,  function(balance) {
							if (balance==0) {
								var msg = "No tienes un saldo pendiente de pago.\nGracias por estar al día en tus cuotas!";
							} else if (balance>0) {
								var msg = "Al momento tienes un saldo a favor de *$"+(balance.toFixed(2))+"*. Este valor será ajustado con tus futuros cargos.";
							} else {
								var msg = "A la fecha tienes un saldo pendiente por pagar de *$"+((balance*-1).toFixed(2))+"*\n\n";
								msg += "Por favor manten al día tus pagos para así ayudarnos a brindarte un mejor servicio.\n\nGracias!"	
							}
							msg += welcomeMessage(false);
							setUserParam("step","1", job.contact, user.line);
							pushText (msg, job.contact, user.line);
						});
					} else {
						//create task
						var name = "El contacto";
						if (user.name) { name = user.name; }
						createTask(user, {
							group: user.group,
							account: user.account,
							status: 'new',
							dura: 10,
							type: 'contact-request',
							title: 'Consulta de Saldo',
							needsReply: true,
							body: name+' solicita su saldo a la fecha.', tsc: Date.now(), createdBy: 'bot'}
						);
						var msg = "Listo, la administración ha recibido tu solicitud y te enviará una respuesta.";
						msg += welcomeMessage(false);
						setUserParam("step","1", job.contact, user.line);
						pushText (msg, job.contact, user.line);
					}
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "Por favor envianos por aquí una foto del comprobante de la transferencia o depósito\n\n";
					setUserParam("step","1.1.3", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					contactAdmin(job, user);
				} else if (["4","4.","cuatro","regresar","volver"].indexOf(kw)>=0) {
					updateUser({step:"1"}, job.contact, user.line);
					pushText (welcomeMessage(false), job.contact, user.line);
				} else {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "1.1.3":
				if (job.msg.type == 'image') {
					var name = "Se";
					if (user.name) { name = user.name; }
					//create task
					createTask(user, {
						group: user.group,
						account: user.account,
						status: 'new',
						dura: 10,
						type: 'contact-request',
						title: 'Validar Comprobante',
						needsReply: true,
						body: name+' solicita la validación del comprobante de depósito/transferencia:<br><br><span class="async-media-link" data-id="'+job.msg.image.id+'"></span>',
						tsc: Date.now(),
						createdBy: 'bot',
						image: job.msg.image
					});
					var msg = "Listo, la administración va a revisar el comprobante y te enviará una notificación.";
					msg += welcomeMessage(false);
					setUserParam("step","1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else {
					pushText ("Por favor envia una foto o escribe *salir* para terminar la sesión", job.contact, user.line);	
				}
				break;
			case "1.2": //reservas
				var kw = body.toLowerCase()
				if (["1","1.","uno"].indexOf(kw)>=0) {
					
					getBalance(user.account, user.group, function(balance) {
						var cals = _.where(calendars, {group: user.group, type: 'res', status: "open"});
						cals = _.sortBy(cals, function(ob) { return ob.caption; });
						buildCalOptions(user, balance, cals, welcomeMessage(false), function(opts, msg) {
							if (opts.length>0) {
								updateUser({step:"1.2.1", calopts: opts}, job.contact, user.line);
								pushText (msg, job.contact, user.line);
							} else {
								updateUser({step:"1", calopts: opts}, job.contact, user.line);
								pushText (msg, job.contact, user.line);
							}
						});
					});
					
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					getEventListPerContact(user, function(arr) {
						if (arr.length>0) {
							var activeRes = []
							var msg = "A continuación están tus reservas activas, digita la opción que deseas cancelar\n\n";
							msg += "*Importante:* Las reservas canceladas no se pueden reactivar.  Escribe *volver* para regresar al menú o escribe *salir* si deseas terminar la sesión\n\n";
							for ( var i=0; i<arr.length; i++) {
								var cal = _.findWhere(calendars, {id: arr[i].resource});
								if (cal) {
									msg += (i+1) + ". " + cal.caption + " Fecha: *" + arr[i].ymd + "* Hora: *" + formatHours(arr[i].timeStart) + "*";
									var st = arr[i].st;
									if (arr[i].st == "acc") { st = "Confirmada" }
									if (arr[i].st == "new") { st = "_Pendiente por confirmar_" }
									msg += ' ('+st+')';
									msg +='\n';
									activeRes.push(arr[i].id)
								}
							}
							updateUser({step:"1.2.3", activeRes: activeRes}, job.contact, user.line);
						} else {
							var msg = "No tienes reservas activas al momento\n";
							msg += welcomeMessage(false);
							updateUser({step:"1"}, job.contact, user.line);
						}
						pushText (msg, job.contact, user.line);
					});
				} else if (["3","3.","tres","regresar","volver"].indexOf(kw)>=0) {
					updateUser({step:"1"}, job.contact, user.line);
					pushText (welcomeMessage(false), job.contact, user.line);
				} else {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "1.2.1": //select resource to make reservation
				var kw = body.toLowerCase().trim();
				
				if (isNaN(Number(kw))) {
					if (["anterior","volver","regresar","menu"].indexOf(kw)>=0) {
						updateUser({step:"1"}, job.contact, user.line);
						pushText (welcomeMessage(false), job.contact, user.line);
					} else { 
						pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
					}
				} else {
					var idx = Number(kw) - 1;
					if (user.calopts[idx]) {
						var pin = Math.floor(Math.random() * 625500) + 100000;
						var msg = buildResourceDets(user.calopts[idx]);
						var condo = _.findWhere(user.condos, {id: user.group});
						var logo = "nologo";
						if (!condo.config) { condo.config = {
							  "acom" : "",
							  "conta" : "no",
							  "linkres" : "",
							  "msgres" : "",
							  "normas" : "",
							  "notif" : "0984252217",
							  "reco" : "",
							  "regla" : "",
							  "sos" : ""
						}}
						if (condo.config.logo) { logo = condo.config.logo; }
						var authStr = encr(job.contact+"|"+account+"|"+user.calopts[idx].id+"|"+user.calopts[idx].type+"|"+pin+"|"+Date.now()+"|"+logo+"|FB");
						makeShortURL(authStr, function(str) {
							msg += "Usa este enlance para hacer tu reserva:\nhttps://events.textcenter.net/#home/SC@"+str;
							pushText (msg, job.contact, user.line);
							updateUser({step:"1.2.2", pin: pin}, job.contact, user.line);	
						});	
					} else {
						pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
					}
				}
				break;
			case "1.2.2": //waiting for event
				pushText ("Por favor termina de agendar tu reserva en nuestro app de reservas o escribe *salir* para terminar la sesión", job.contact, user.line);
				break;
			case "1.2.3": //check if option is in list of active reservas
				var kw = body.toLowerCase().trim();
				
				if (isNaN(Number(kw))) {
					if (["anterior","volver","regresar","menu"].indexOf(kw)>=0) {
						updateUser({step:"1"}, job.contact, user.line);
						pushText (welcomeMessage(false), job.contact, user.line);
					} else { 
						pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
					}
				} else {
					var idx = Number(kw) - 1;
					if (user.activeRes[idx]) {
						updateEvent(user.activeRes[idx], {st: 'del', obs:'Cancelado por el contacto', tsu: Date.now()}, function(updated) {
							if (updated) {
								var msg = "Listo! Tu reserva fue cancelada\n";
								msg += welcomeMessage(false);
							} else {
								var msg = "Hubo un error al cancelar el evento, por favor comunícante con la administración\n";
								msg += welcomeMessage(false);
							}
							updateUser({step:"1"}, job.contact, user.line);
							pushText (msg, job.contact, user.line);
						});
						
					} else {
						pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
					}
				}
				break;
			case "1.3": //administracion
				var kw = body.toLowerCase()
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Deseas contactar con la administración?\n\n"
					msg += "1. SI\n";
					msg += "2. NO\n";
					setUserParam("step","1.3.1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "Por favor digita una opción:\n\n";
					msg += "1. SOLICITAR tarjeta de acceso\n";
					msg += "2. BLOQUEAR tarjeta de acceso\n";
					msg += "3. REGRESAR\n";
					setUserParam("step","1.3.2", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					contactAdmin(job, user);
				} else {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "1.3.1":
				var kw = body.toLowerCase()
				if (["1", "1.", "si","ok","yes","bueno","aja","gracias","okey","sii","siii", "porfa", "porfavor",  "por favor", "si, gracias", "si gracias"].indexOf(kw)>=0) {
					contactAdmin(job, user);
				} else {
					salir(job, user);
				}
				break
			case "1.3.2":
				var kw = body.toLowerCase()
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var name = "Se";
					if (user.name) { name = user.name; }
					//create task
					createTask(user, {
						group: user.group,
						account: user.account,
						status: 'new',
						dura: 10,
						type: 'contact-request',
						title: 'Tarjeta de Acceso - Nueva',
						needsReply: true,
						body: name+' solicita una nueva tarjeta de acceso', tsc: Date.now(), createdBy: 'bot'}
					);
					var msg = "Listo, la administración va a revisar tu solicitud y te enviará una notificación.";
					msg += welcomeMessage(false);
					setUserParam("step","1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var name = "Se";
					if (user.name) { name = user.name; }
					//create task
					createTask(user, {
						group: user.group,
						account: user.account,
						status: 'new',
						dura: 10,
						type: 'contact-request',
						title: 'Tarjeta de Acceso - Bloqueo',
						needsReply: true,
						body: name+' solicita BLOQUEAR una tarjeta de acceso', tsc: Date.now(), createdBy: 'bot'}
					);
					var msg = "Listo, la administración va a revisar tu solicitud y te enviará una notificación.";
					msg += welcomeMessage(false);
					setUserParam("step","1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
					
				} else if (["3","3.","regresar","volver"].indexOf(kw)>=0) {	
					updateUser({step:"1"}, job.contact, user.line);
					pushText (welcomeMessage(false), job.contact, user.line);
				} else {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break
			case "1.4": //CONDOMINIO
				var kw = body.toLowerCase()
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var condo = _.findWhere(user.condos, {id: user.group});
					if (condo.config) {
						if (condo.config.regla) {
							var msg = "Por favor utiliza el siguiente enlace para descargar el reglamento interno:\n\n";
							msg += condo.config.regla+"\n";
						} else {
							var msg = "La administración no ha actualizado aún el reglamento interno\n\n";	
						}  
					} else {
						var msg = "La administración no ha actualizado aún el reglamento interno\n\n";
					}
					msg += welcomeMessage(false);
					setUserParam("step","1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var condo = _.findWhere(user.condos, {id: user.group});
					if (condo.config) {
						var msg = "El inmueble tiene las siguientes acometidas:\n\n";
						msg += condo.config.acom+"\n";
					} else {
						var name = "Se";
						if (user.name) { name = user.name; }
						//create task
						createTask(user, {
							group: user.group,
							account: user.account,
							status: 'new',
							dura: 10,
							type: 'contact-request',
							title: 'Consulta Acometidas',
							needsReply: true,
							body: name+' requiere información sobre las acometidas del edificio', tsc: Date.now(), createdBy: 'bot'}
						);
						var msg = "La administración va a revisar tu inquietud y te enviará una notificación.";
					
					}
					msg += welcomeMessage(false);
					setUserParam("step","1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var condo = _.findWhere(user.condos, {id: user.group});
					if (condo.config) {
						var msg = "Este es el horario de recolección de desechos:\n\n";
						msg += condo.config.reco+"\n";
					} else {
						var name = "Se";
						if (user.name) { name = user.name; }
						//create task
						createTask(user, {
							group: user.group,
							account: user.account,
							status: 'new',
							dura: 10,
							type: 'contact-request',
							title: 'Consulta Recolección Desechos',
							needsReply: true,
							body: name+' requiere información sobre la recolección de desechos', tsc: Date.now(), createdBy: 'bot'}
						);
						var msg = "La administración va a revisar tu inquietud y te enviará una notificación.";
					
					}
					msg += welcomeMessage(false);
					setUserParam("step","1", job.contact, user.line);
					pushText (msg, job.contact, user.line);
				} else if (["4","4.","cuatro","regresar","volver"].indexOf(kw)>=0) {
					updateUser({step:"1"}, job.contact, user.line);
					pushText (welcomeMessage(false), job.contact, user.line);
				} else {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "888":
				//visitors menu
			
				var kw = body.toLowerCase()
				if (["1","1.","uno","comprar","contratar"].indexOf(kw)>=0) {
					var msg = "Excelente! En un momento atendemos tu requerimiento";
					updateUser({step:"idle", group: "sales", status: "active", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "sales");
					pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos","ayuda","help","servicio","cliente"].indexOf(kw)>=0) {
					var msg = "Un momento por favor, en breve atendemos tu requerimiento";
					updateUser({step:"idle", group: "support", status: "active", tso: Date.now()}, job.contact, user.line);
					logStartTicket(job.contact, user.chn, user, "support");
					pushText (msg, job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					salir(job, user);
				} else {
					pushText ("Por favor digita una opción o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
			case "999": //main menu
				msg = welcomeMessage(false);
				setUserParam("step","1", job.contact, user.line);
				resetUserParam('pin', job.contact);
				resetUserParam('calopts', job.contact);
				pushText (msg, job.contact, user.line);
				break;
			
		}
	}	
}

function menuVisitors () {
	var msg = ""
	msg += "Hola, gracias por contactar a Urbabot!\n\n";
	msg += "Por favor *digita* una opción:\n\n";
	msg += "1. Quiero contratar Urbabot para mi condominio\n";
	msg += "2. Contactar con servico al cliente\n";
	msg += "3. Salir\n";	
	return msg;
}

function welcomeMessage (greet, name) {
	var msg = ""
	if (greet) {
		msg += "Hola, bienvenid@ al chatbot de Urbabot!\n\n";
		msg += "*MENU PRINCIPAL*\n\n";
	} else {
		msg += "\n\n*MENU PRINCIPAL*\n\n";
	}
	msg += "Digita una opción:\n\n";
	msg += "1. ALICUOTAS\n";
	msg += "2. RESERVAS\n";
	msg += "3. SERVICIO AL CLIENTE\n";	
	msg += "4. CONDOMINIO\n";
	msg += "5. SALIR\n";
	return msg;
}

function contactAdmin(job, user) {
	if (isShopOpen()) {
		var msg = "Un momento por favor, el administrador atenderá tu requerimiento";
		updateUser({step:"idle", status: "active", tso: Date.now()}, job.contact, user.line);
		logStartTicket(job.contact, user.chn, user, "admin");
		pushText (msg, job.contact, user.line);
	} else {
		var condo = _.findWhere(user.condos, {id: user.group});
		var msg = "En este momento la oficina del administrador está cerrada.\n\n";
		msg += "Si se trata de una emergencia por favor comunícate al "+condo.config.sos;
		msg += welcomeMessage(false);
		setUserParam("step","1", job.contact, user.line);
		pushText (msg, job.contact, user.line);
	}	
}

function buildResourceDets(res) {
	var days = ["LUN","MAR","MIE","JUE","VIE","SAB","DOM"]
	var daysOpen = []
	if (res.schedule) {
		for (var i=0; i<res.schedule.length; i++) {
			if (res.schedule[i].isOpen) {
				daysOpen.push(days[i]);
			}
		}
		var str = "*"+res.caption+'*\n';
		str += '*Horario de servicio:*\n';
		str += 'Abierto '+daysOpen.join('-')+' de '+formatHours(res.schedule[0].hourStart)+' a '+formatHours(res.schedule[0].hourEnd)+''
		if (res.obs.length>0) {
			str += '\n*Observaciones:* \n';
			str += res.obs
		}
		str += '\n\n';
	} else {
		for (var i=0; i<res.daysOpen.length; i++) {
			if (res.daysOpen[i]) {
				daysOpen.push(days[i]);
			}
		}
		var str = "*"+res.caption+'*\n';
		str += '*Horario de servicio:*\n';
		str += 'Abierto '+daysOpen.join('-')+' de '+formatHours(res.hourStart)+' a '+formatHours(res.hourEnd)+''
		if (res.obs.length>0) {
			str += '\n*Observaciones:* \n';
			str += res.obs
		}
		str += '\n\n';
	}
	return str;
}


