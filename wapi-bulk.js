var tools = require('./tools');
var http = require("http");
var https = require("https");
var fs = require('fs');
var qs = require('querystring');
var _ = require('underscore');
var HttpCors = require('http-cors');
var cors = new HttpCors();
var mtq = [];
var purge = []
var mtqr = {}
var load = {};
var d = new Date();
var loadDate = d.getFullYear() + "" + format2D(d.getMonth()+1) + "" + format2D(d.getDate())
var maxLoad = 125; //default
var booted = false;
var ssl = true;
var users = []
var assetsCache = {}
var campsCache = {}
var config = { hourOpen: 800, hourClose: 2200, mtDoor: 10800000 } //default
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs
//global scope to access from modules
global.lines = [];
global.firebase = require('firebase');

//read port from command line
var port = 9305 //default
if (process.argv[2]) {
	var portNumber = parseInt(process.argv[2]);
	if (portNumber >= 9000 && portNumber <= 9999) { port = portNumber; }
}

//read port from command line
var client = "wapi-bulk"
if (process.argv[3]) {
	client = process.argv[3];	
}
//firebase init
var fbconfig = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(fbconfig);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootWAPI();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function bootWAPI() {
  if (!booted) {
    //listen to config
    firebase.database().ref("/accounts/"+client+"/config/").on('value', function(resp) {
		if (resp.val()) {
			config = resp.val();
			console.log (new Date(), "Config Reloaded for", client);	
			console.log (config);
			console.log ("Working Hours", isWorkingHours());
		}	
	});
    
    //save boot record
    firebase.database().ref('/procs/').push({proc: 'wapi-bulk', port: port, client: client, ts: Date.now()});
	//load users
	loadUsers(function () {
		//load lines
		loadLines(function () {
			//load lines load
			getLoad(function () {
				
				  booted = true;
				  	
				  //load mtq
				  loadMTQ();
				  
				  //start purge worker
				  setTimeout(function() { purgeWorker() }, 2000);	

				  //start API
				  if (!ssl) {
					http.createServer(app).listen(port);
					console.log("WAPI BULK HTTP Server started port/client", port, client)
				  } else {
					var httpsOptions = {
						cert: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/fullchain.pem'),
						ca: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/chain.pem'),
						key: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/privkey.pem'),
						requestCert: false,
						rejectUnauthorized: false
					};

					https.createServer(httpsOptions, app).listen(port);
					console.log("WAPI BULK HTTPS Server started port/client", port, client)
					}
			});
		});
	});
  }
}

function checkLoad(line) {
	var validLoad = false
	//if current date not the same as loadDate we need to init load
	var d = new Date();
	var dstr = d.getFullYear() + "" + format2D(d.getMonth()+1) + "" + format2D(d.getDate())
	if (loadDate!=dstr) {
		//init new load for today
		loadDate = dstr;
		console.log ("New Load Init for ",new Date());
		initLoad();
		load[line] = 1;
		saveLoad();
		validLoad = true;
	} else {
		if ( !load.hasOwnProperty(line) ) {
			load[line] = 1;
			validLoad = true;
			saveLoad();
		} else {
			load[line] += 1;
			var lineMaxLoad = maxLoad //default
			var lineOb = _.findWhere( lines, {id: line} );
			if (lineOb) {
				if (lineOb.config.maxLoad) {
					lineMaxLoad	= Number(lineOb.config.maxLoad);
				}
			}
			if (load[line] > lineMaxLoad) {
				load[line] = lineMaxLoad;
				validLoad = false
			} else {
				validLoad = true;
				saveLoad();
			}
		}
	}	
	
	return validLoad;
}

function getLoad(cb) {
	
	console.log ("Getting load for", loadDate)
	firebase.database().ref('/loads/'+loadDate).once('value', function(resp) {
		if (resp.val()) {
			load = JSON.parse(JSON.stringify(resp.val()));
			console.log ("Load loaded from DB");
		} else {
			console.log ("New Load Init for ",new Date());
			initLoad();	
			saveLoad();
		}
		if (cb) { cb(); }
	});	
}

function initLoad() {
	load = { tsc: Date.now() }
}

function saveLoad(cb) {
	firebase.database().ref('/loads/'+loadDate).set(load);
	if (cb) { cb(); }
}

function format2D(v) {
	if (v<10) {
		return "0" + v;
	} else {
		return "" + v;
	}	
}

function loadLines(cb) {
	firebase.database().ref('/lines/').orderByChild('client').equalTo(client).on('child_added', function(snapshot) {
		var line = snapshot.val()
		line.id = snapshot.key
		if (line.status == "active") {
			lines.push(line)	
		}
		console.log (lines.length + " line(s) loaded");
	});
	if (cb && !booted ) { cb(); }
}

function loadUsers(cb) {
	firebase.database().ref('/users/').orderByChild("client").equalTo(client).once('value', function(snapshot) {
		var arr = _.map (snapshot.val(), function(value, key) {
			value.uid = key;
			return value;});
		for (var i=0; i<arr.length; i++) {
			if (arr[i].bulkToken) {
				users.push ( arr[i] )
			}
		}
		console.log (users.length + " user(s) loaded");
		if (cb && !booted ) { cb(); }
	});
}

function loadMTQ() {
	firebase.database().ref("/accounts/"+client+"/mtq/").orderByChild("st").equalTo("qed").on('child_added', function(resp) {
		var msg = resp.val();
		msg.id = resp.key;	
		var door = Date.now() - config.mtDoor;
		if (msg.ts>door) {
			if (msg.line == "bulk") {	
				//console.log ("PRIO", msg.prio)
				if (msg.prio == "top") {
					mtq.unshift(msg);
					console.log (new Date(), "NEW BULK MT QUEUED TOP", mtq.length);	
				} else {		
					mtq.push(msg);
					console.log (new Date(), "NEW BULK MT QUEUED", mtq.length);	
				}
			} else {
				if (!mtqr[msg.line]) {
					mtqr[msg.line] = []
				}
				if (msg.prio == "top") {
					mtqr[msg.line].unshift(msg);
					console.log (new Date(), "NEW RESPONSE TO "+msg.line+" MT QUEUED TOP", mtqr[msg.line].length, msg.id);
				} else {		
					mtqr[msg.line].push(msg);
					console.log (new Date(), "NEW RESPONSE TO "+msg.line+" MT QUEUED", mtqr[msg.line].length, msg.id);
				}	
			}
		} else {
			console.log (new Date(), "Purge MT", msg.id)	
			purge.push(msg);
		}
	});
}

function purgeWorker() {
	if (purge.length>0) {
		purgeMT( purge.shift() ); 
	} else {
		setTimeout(function() { purgeWorker() }, 60000);	
	}	
}
function purgeMT(msg) {
	//set mtq status to purged
	firebase.database().ref("/accounts/"+client+"/mtq/"+msg.id).update({ lu: Date.now(), st: 'pur' });
	
	//increment purge count in camp
	if (msg.camp) {
		firebase.database().ref('accounts/'+client+'/camps/'+msg.camp+'/count/p').set(firebase.database.ServerValue.increment(1));
		firebase.database().ref('accounts/'+client+'/camps/'+msg.camp+'/count/q').set(firebase.database.ServerValue.increment(-1));
	}
	console.log (new Date(),  "Purged MT", msg.id, purge.length);
	setTimeout(function() { purgeWorker() }, 100);	
}

function pushMT(post, user) {
	
	var type = 'txt';
	if ( post.type ) { type = post.type }
	var rep = "wapi";
	if ( post.rep ) { rep = post.rep }
	var clienteFinal = 'otro';
	if (post.clienteFinal) { clienteFinal = post.clienteFinal }

		var msgId = Date.now()+"-bulk-"+post.to;
		var d = new Date()
		var mt = { uid: user.uid, my: d.getFullYear()+"-"+(d.getMonth()+1),g:0, m:post.body, to: post.to, rep: rep, t:"mt", ts: Date.now(), type: type, client: client, st: 'qed', lu: Date.now(), clienteFinal: clienteFinal, line: post.line};
		if (post.camp) { mt.camp = post.camp}
		if (post.desc) { mt.desc = post.desc }
		if (post.url) { mt.url = post.url }
		if (post.thumb) { mt.thumb = post.thumb; }
		if (post.extId) { mt.extId = post.extId; }
		if (post.mediaURL) { mt.mediaURL = post.mediaURL; }
		if (post.prio) { mt.prio = post.prio; }
		if (post.isResponse) { mt.prio = 'top'; }
		if (post.createTicket) { mt.forceAck = true }
		//console.log (post)
		//push to client MT queue
		firebase.database().ref("/accounts/"+client+"/mtq/"+msgId).set(mt).then(function() {
		   console.log(new Date(), "MT POSTED OK TO BULK MTQ");
		   if (post.createTicket) {
			    //update / create chat
			    var user = { chn: 'wsp', id: post.to, line: post.line, nick: post.to, lu: Date.now() }
			    if (post.rep) { user.rep = post.rep; }
			    firebase.database().ref("/accounts/"+client+"/chats/"+post.to).update(user)
				//create MT in chat
				firebase.database().ref("/accounts/"+client+"/conves/"+post.to+"/msgs/"+msgId).update(mt);
		   } else if (post.isResponse) {
			    //create MT in chat
				firebase.database().ref("/accounts/"+client+"/conves/"+post.to+"/msgs/"+msgId).update(mt);
		   }
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
		
		//push to back routing
		var rt = { clienteFinal: clienteFinal, to: post.to, tsc: Date.now()}
		if (post.camp) { rt.campId = post.camp}
		firebase.database().ref("/backroute/").push(rt)

}

function processMO( post ) {
	var line = post.me.user;
	var msg = post.msg.msg;
	var sender = msg.senderObj;
	
	//build mo from type
	switch (msg.type) {
		case 'chat':
			var mo = { g:3, m: msg.body, rep: 'wapi', t:"mo", ts: Date.now(), type: 'txt' };
			break;
		case 'image':
			var strBody = "[imagen]";
			if (msg.caption) { strBody = msg.caption }
			var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'img', mimetype: msg.mimetype, size: msg.size, thumb: msg.body, url: post.msg.fn };
			break;
		case 'document':
			var strBody = "[document]";
			if (msg.caption) { strBody = msg.caption }
			var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'doc', mimetype: msg.mimetype, size: msg.size, thumb: msg.body, url: post.msg.fn };
			break;
		case 'location':
			var mo = { g:3, m:'[location]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'loc', lng: msg.lng, lat: msg.lat, thumb: msg.body };
			break;
		case 'vcard':
			var mo = { g:3, m: msg.body, rep: 'wapi', t:"mo", ts: Date.now(), type: 'vcd' };
			break;
		case 'video':
			var strBody = "[video]";
			if (msg.caption) { strBody = msg.caption }
			var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'vid', mimetype: msg.mimetype, size: msg.size, thumb: msg.body, url: post.msg.fn };
			break;
		case 'audio':
			var mo = { g:3, m: '[audio]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'aud', mimetype: msg.mimetype, size: msg.size, url: post.msg.fn };
			break;
		case 'ptt':
			var mo = { g:3, m: '[audio]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'aud', mimetype: msg.mimetype, size: msg.size, url: post.msg.fn };
			break;
		default:
			var mo = { g:3, m: msg.type, rep: 'wapi', t:"mo", ts: Date.now(), type: msg.type };
	}
	var name = sender.id.user;
	if ( sender.pushname ) { name = sender.pushname; }
	if ( sender.name ) { name = sender.name; }
	var user = { chn: 'wsp', id: sender.id.user, line: line, nick: name, lu: Date.now() }

	var contact = sender.id.user;
	if (contact.indexOf('@g.us')>0) { contact = contact.replace('@g.us','-group') } //MO is from group
	
	//create or update user
	firebase.database().ref("/accounts/"+client+"/chats/"+contact).update(user).then(function() {
		firebase.database().ref("/accounts/"+client+"/conves/"+contact+"/msgs/"+msg.id.id).set(mo).then(function() {
			console.log (new Date(), "MO saved to conve")
			//create job		
			var job = {
			   msg: mo,
			   user: user,
			   contact: contact,
			   line: line,
			   st: 'new',
			   ts: Date.now()
			   }
			firebase.database().ref("/accounts/"+client+"/botJobs").push(job);
			console.log (new Date(), "Bot Job pushed")
		}).catch(function(error) {
		   console.log (new Date(), "Error posting MO to Firebase", line);
		   console.log (error)
		   console.log (mo)
		   //TODO: persist this msg in local file to resend MOs
		});
	}).catch(function(error) {
		console.log (new Date(), "Error posting User to Firebase", line);
		console.log (error)
		console.log (sender)
		console.log (user)
		//TODO: persist this msg in local file to resend MOs
	});
}

function pushWatcher (line, watch ) {
	firebase.database().ref("/lines/"+line+"/watch").update( watch );
}

function saveMedia(post) {
	var client = 'demo'
	if (post.client) { client = post.client }
	firebase.database().ref("/accounts/"+client+"/media/"+post.fn.replace(/\./g,"_")).set (post.blob).then(function() {
	}).catch(function(error) {
		   console.log(new Date(), "Error saving MEDIA", error);
	});
}

function ackMTSent(msg, line) {
	firebase.database().ref("/accounts/"+client+"/conves/"+msg.to+"/msgs/"+msg.id).update({ line: line, lu: Date.now(), st: 'snt', g: 1 })
	firebase.database().ref("/accounts/"+client+"/mtq/"+msg.id).update({ line: line, lu: Date.now(), st: 'snt' }).then(function() {
		//increment sent count in camp
		if (msg.camp) {
			if (msg.camp != "bulk-response") {
				firebase.database().ref('accounts/'+client+'/camps/'+msg.camp+'/count/s').set(firebase.database.ServerValue.increment(1));
				firebase.database().ref('accounts/'+client+'/camps/'+msg.camp+'/count/q').set(firebase.database.ServerValue.increment(-1));
			}
		}
	}).catch(function(error) {
		   console.log(new Date(), "Error setting ACK MT SENT", error);
	});
}

function getLink(contact, camp, cb) {
	//is camp in cache?
	if (campsCache[camp]) {
		pushTrack(contact, camp);
		cb(campsCache[camp].link);
	} else {
		firebase.database().ref("accounts/"+client+"/camps/"+camp).once('value', function(resp) {
			if (resp.val()) {
				campsCache[resp.key] = resp.val();
				pushTrack(contact, camp);
				cb(campsCache[resp.key].link);
			} else {
				cb(false);
			}
		}).catch(function(error) {
			console.log (error)
			cb(false);
		});
	}
}

function pushTrack(contact, camp) {
	var track = {
		ref: contact,
		camp: camp,
		ts: new Date()
		}
	firebase.database().ref("/tracks/").push(track);	
}

function hookAsset(mt) {
	if (mt.thumb) {
		if (mt.thumb.indexOf('b64Asset')>=0) {
			var asset = JSON.parse(mt.thumb);
			//check if loaded on cache
			
			if ( assetsCache[asset.code] ) {
				console.log ("Hooked asset from cache");
				mt.thumb = assetsCache[asset.code];
				return mt;
			} else {
				var content = fs.readFileSync(asset.path, 'utf8');
				if (content) {
					console.log ("Hooked asset from file");
					assetsCache[asset.code] = content;
					mt.thumb = content;
				} else {
					delete mt.thumb
					mt.type = "txt";	
				}
				return mt;
			}
			
		} else {
			return mt;	
		}
	} else {
		return mt;
	}	
}

function processPost(req, res, callback) {
	var queryData = "";
	if(typeof callback !== 'function') return null;

	req.on('data', function(data) {
		queryData += data;
		if(queryData.length > 10000000) {
			queryData = "";
			res.writeHead(413, {'Content-Type': 'text/plain'});
			res.end();
			req.connection.destroy();
		}
	});

	req.on('end', function() {
		if (req.method == 'GET') {
			req.post = qs.parse(req.url.replace(/^.*\?/, ''));
		} else {
			req.post = queryData;
		}
		callback();
	});
}

var app = function(req, res) {

     if(cors.apply(req, res)) {
		 res.end();
		 return;
	 }

	 processPost(req, res, function() {
		//console.log ("URL", req.url);
		var parts = req.url.split("/");
		//router
        switch (parts[1]) {
			case 'send':
                //process MT received from API call
				   console.log (new Date(), "MT received");
				   if (req.method == "POST") {
						if (typeof req.post === "string") {
							var post = _.clone ( qs.parse(req.post)	);
							if (!post.apiToken) {
								post = _.clone ( JSON.parse(req.post)	);
							}
						} else {
							var post = _.clone ( req.post );
						}
						//find user with token
						var user = _.findWhere(users, {bulkToken: post.apiToken})
						if (user) {
							var mtValid = isValidMT(post);
							if (mtValid.valid) {
								pushMT (post, user)
								res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
								res.write("mt-pushed-ok");
								res.end();
								return;
							} else {
								console.log ("403", mtValid.msg)
								res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
								res.write(mtValid.msg);
								res.end();
								return;
							}
						} else {
							console.log ("401", "Invalid Token")
							res.writeHead(401, "ERROR", {'Content-Type': 'text/plain'});
							res.write("invalid-token");
							res.end();
							return;
						}

				   } else {
					   console.log ("405")
					   res.writeHead(405, {'Content-Type': 'text/plain'});
					   res.write("invalid-request-method");
					   res.end();
					   return;
				   }

                break;
            case 'pushMO':                
			    //~ console.log (new Date(), "MO received");
			    //~ console.log (req.post);
			    if (req.post) {
					var postObj = JSON.parse(req.post);
					if (postObj.me) {
						processMO ( postObj );
					}
				}
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("mo-pushed-ok");
				res.end();
				return;
                break;
            case 'pullMT':
                //console.log (new Date(), "Pulling MT for",parts[2]);
				var line = parts[2];
				if (line) {
					if (isWorkingHours()) {
						if ( mtqr[line] ) {
							if ( mtqr[line].length>0 ) {
								console.log (new Date(), "RESPONSE MT PULL OK LENGTH = " + mtq.length, line);
								var mt = mtqr[line].shift();
								msg = JSON.stringify( hookAsset(mt) );
								ackMTSent(mt, line);
								
							} else {
								
								if ( mtq.length>0 ) {
									if ( checkLoad(line) ) {
										console.log (new Date(), "MT PULL OK LENGTH = " + mtq.length, line);
										var mt = mtq.shift();
										msg = JSON.stringify( hookAsset(mt) );
										ackMTSent(mt, line);
									} else {
										console.log (new Date(), "MAX LOAD", line);
										msg = "mtq-empty";
									}
								} else {
									//console.log (new Date(), "MT PULL MTQ EMPTY", line);
									msg = "mtq-empty";
								}
								
							}	
						} else {	
							
							if ( mtq.length>0 ) {
								if ( checkLoad(line) ) {
									console.log (new Date(), "MT PULL OK LENGTH = " + mtq.length, line);
									var mt = mtq.shift();
									msg = JSON.stringify( hookAsset(mt) );
									ackMTSent(mt, line);
								} else {
									console.log (new Date(), "MAX LOAD", line);
									msg = "mtq-empty";
								}
							} else {
								//console.log (new Date(), "MT PULL MTQ EMPTY", line);
								msg = "mtq-empty";
							}
							
						}
					} else {
						//console.log (new Date(), "OFF HOURS", line);
						msg = "mtq-empty";	
					}
				} else {
					console.log (new Date(), "MT PULL INVALID LINE", line);
					msg = "invalid-line";	
				}
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write( msg );
			    res.end();
			    return;
                break;
            case 'upload':                
			    //~ console.log (new Date(), "UPLOAD received");
			    if (IsJsonString(req.post)) {
					//saveMedia ( JSON.parse(req.post) ); 
					console.log (new Date(), "Upload Error - Save Media Disabled")	
				} else {
					console.log (new Date(), "Upload Error")	
				}
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("upload-ok");
			    res.end();
			    return;
                break;
            case 'watcher':                
			    //console.log (new Date(), "Watcher received");
			    //console.log (req.post)
			    if (req.post) {
					var obj = JSON.parse(req.post); //To Do catch error SyntaxError: Unexpected end of JSON input
					if (obj.me) {
						var line = obj.me.user;
						//console.log (new Date(), "Watcher for", line);
						var watch = {
							phoneAuthed: obj.msg.stream.phoneAuthed,
							info: obj.msg.stream.info,
							mode: obj.msg.stream.mode,
							connected: obj.msg.conn.connected,
							battery: obj.msg.conn.battery,
							plugged: obj.msg.conn.plugged,
							ts: Date.now(),
							load: 0,
							ql: 0
							
						}
						if (load[line] != undefined) { watch.load = load[line] }
						if (!watch.connected) { watch.connected = false }
						if (!watch.plugged) { watch.plugged = false }
						if (!watch.battery) { watch.battery = false }
						if (obj.msg.mtql) { watch.ql = obj.msg.mtql }
						
						//push watcher
						pushWatcher(line, watch );
					}
					
				}
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("watch-ok");
				res.end();
				return;
				break;
			case 'secure':                
			    console.log (new Date(), "secure", parts[2], parts[3]);
			    if (parts[2] && parts[3]) {
					getLink(parts[2], parts[3], function (link) {
						if (link) {
							res.writeHead(301,
								{Location: link}
							);
							res.end(); 
							return;
						} else {
							res.writeHead(404, {'Content-type':'text/html'})
							res.end("Campaign not found :(");	
							return;
						}
					});
				} else {
					res.writeHead(404, {'Content-type':'text/html'})
					res.end("Campaign not found :(");	
					return;
				}
                break;
            case 'err':                
			    console.log (new Date(), "ERR received");
			    console.log (req.post)
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("err-ok");
			    res.end();
			    return;
                break;
            case "ping":
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("pong");
				res.end();
				return;
				break;
			case "status":
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write(JSON.stringify({ql: mtq.length}));
				res.end();
				return;
				break;
            default:
			   console.log(new Date(), "unknown path "+req.url);
			   res.writeHead(404, {'Content-Type': 'text/plain'});
			   res.write("unknown path "+req.url);
			   res.end();
			   return;
        }
    });
}

function isValidMT(post) {
	var ob = {valid: true, msg:''}	
	if (!post.type) { post.type = 'txt' }
	var numberCheck = tools.numberFormat(post.to);
	var validTypes = ["txt","img","vid","mix","stk","tmp","btn","lst","doc"] //todo> move to account config
	if (numberCheck.valid) {
		
		post.to = numberCheck.to;
		//if (post.body) {
			//~ if (post.body.length > 0) {
				if (validTypes.indexOf(post.type)>=0) {
					//all is good
				} else {
					ob.valid = false;
					ob.msg = "invalid-message-type";	
				}
			//~ } else {
				//~ ob.valid = false;
				//~ ob.msg = "missing-body-attribute";	
			//~ }
		//} else {
			//ob.valid = false;
			//ob.msg = "missing-body-attribute";
		//}
	} else {
		ob.valid = false;
		ob.msg = "invalid-destination-number";
	}
	
	return ob;
}

function IsJsonString(str) {
	if (str.indexOf("[")==0 || str.indexOf("{")==0) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	} else {
		return false;	
	}
}

function isWorkingHours() {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (t>=config.hourOpen && t<config.hourClose) {
		return true;
	} else {
		return false;		
	}	
}
