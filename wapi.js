var tools = require('./tools');
var http = require("http");
var https = require("https");
var fs = require('fs');
var qs = require('querystring');
var _ = require('underscore');
var request = require('request');
var HttpCors = require('http-cors');
var cors = new HttpCors();
var mtq = {};
var mtqLoaded = false;
var clientQLoaded = {}
var booted = false;
var ssl = true;
var tokens = []
var purge = []
var assetsCache = {}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs
//global scope to access from modules
global.lines = [];
global.firebase = require('firebase');

//read port from command line
var port = 9304 //default
if (process.argv[2]) {
	var portNumber = parseInt(process.argv[2]);
	if (portNumber >= 9000 && portNumber <= 9999) { port = portNumber; }
}

//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootWAPI();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function bootWAPI() {
  if (!booted) {
    booted = true;
    //push proc
    firebase.database().ref('/procs/').push({proc: 'wapi', port: port, ts: Date.now()});
    //load lines
    loadLines(function () {
		  loadBotAssets();
		  //start API
		  if (!ssl) {
			http.createServer(app).listen(port);
			console.log("WAPI HTTP Server started on port", port)
		  } else {
			var httpsOptions = {
				cert: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/fullchain.pem'),
				ca: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/chain.pem'),
				key: fs.readFileSync('/etc/letsencrypt/live/textcenter.net/privkey.pem'),
				requestCert: false,
				rejectUnauthorized: false
			};

			https.createServer(httpsOptions, app).listen(port);
			console.log("WAPI HTTPS Server started on port", port)
			}
    });
  }
}

function loadLines(cb) {
	firebase.database().ref('/lines/').once('value', function(snapshot) {
		var arr = _.map (snapshot.val(), function(value, key) {
			value.id = key;
			return value;});
		for (var i=0; i<arr.length; i++) {
			if (arr[i].isWAPI && arr[i].client != "wapi-bulk" && arr[i].client != "udla-bulk" ) {
				lines.push( arr[i] );
			}
		}
		console.log (lines.length + " line(s) loaded");
		//initializq mtq arrays
		for (var i=0; i<lines.length; i++) {
			mtq[lines[i].id] = [];
			tokens.push(lines[i].apiToken);
			//load MTs queue from database if not loaded already
			if (clientQLoaded[lines[i].client]) {
				console.log ("mtq already loaded for "+lines[i].client) 	
			} else {
				console.log ("loading mtq for "+lines[i].client) 	
				clientQLoaded[lines[i].client] = true;
				loadMTQ(lines[i].client);
			}
		}
		//start purge worker
		setTimeout(function() { purgeWorker() }, 2000);
		if (cb) { cb(); }
	});
}


function loadMTQ(client) {
	firebase.database().ref("/accounts/"+client+"/mtq/").orderByChild("st").equalTo("qed").on('child_added', function(res) {
		var msg = res.val();
		msg.id = res.key;
		if (mtq[msg.line]) {			
			var door = Date.now() - (1*60*60*1000); //only log if recent as last hour
			
			if (msg.ts>door) {					
					mtq[msg.line].push(msg);
					console.log (new Date(), "NEW MT FOR", client, msg.line, mtq[msg.line].length);					
			} else {
				console.log (new Date(), "Purge", msg.id)	
				purge.push(msg);
			}
		} else {
		    console.log (new Date(), "Line NOT VALID", msg);
		    console.log (new Date(), "Purge", msg.id)	
			purge.push(msg);
		}
	});
}

function pushMT(post) {
	
	var type = 'txt';
	if ( post.type ) { type = post.type }
	var rep = 'wapi';
	if ( post.rep ) { rep = post.rep }
	
	var line = post.line;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
		var msgId = Date.now()+"-"+line+"-"+post.to;
		var mt = { g:0, m:post.body, to: post.to, rep: rep, t:"mt", ts: Date.now(), type: type, st: 'qed', lu: Date.now()};
		if (post.desc) { mt.desc = post.desc }
		if (post.url) { mt.url = post.url }
		if (post.thumb) { mt.thumb = post.thumb; }
		if (post.camp) { mt.camp = post.camp; }
		if (post.isSticker) { mt.isSticker = true; }
		firebase.database().ref("/accounts/"+lineOb.client+"/conves/"+post.to+"/msgs/"+msgId).set(mt).then(function() {
			var ob = _.clone (mt);
			ob.line = line;
			ob.to = post.to;
			ob.client = lineOb.client;
			console.log(new Date(), "MT Saved to /"+lineOb.client+"/conves/"+post.to+"/msgs/"+msgId);
			//push to client MT queue
			firebase.database().ref("/accounts/"+lineOb.client+"/mtq/"+msgId).set(ob).then(function() {
			   console.log(new Date(), "MT POSTED OK TO MTQ");
			}).catch(function(error) {
			   console.log(new Date(), "Push MTQ Error", error);
			});
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	} else {
		console.log (new Date(), "Error pushing line not found", line);
	}
}

function processMO( post ) {
	var line = post.me.user;
	var msg = post.msg.msg;
	var sender = msg.senderObj;
	var chat = msg.chat;
	var lineOb = _.findWhere( lines, {id: line} );
	
	if (lineOb) {
	
		//build mo from type
		switch (msg.type) {
			case 'chat':
				var mo = { g:3, m: msg.body, rep: 'wapi', t:"mo", ts: Date.now(), type: 'txt' };
				break;
			case 'image':
				var strBody = "[imagen]";
				if (msg.caption) { strBody = msg.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'img', mimetype: msg.mimetype, size: msg.size, thumb: msg.body, url: post.msg.fn };
				break;
			case 'document':
				var strBody = "[document]";
				if (msg.caption) { strBody = msg.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'doc', mimetype: msg.mimetype, size: msg.size, thumb: msg.body, url: post.msg.fn };
				break;
			case 'location':
				var mo = { g:3, m:'[location]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'loc', lng: msg.lng, lat: msg.lat, thumb: msg.body };
				break;
			case 'vcard':
				var mo = { g:3, m: msg.body, rep: 'wapi', t:"mo", ts: Date.now(), type: 'vcd' };
				break;
			case 'video':
				var strBody = "[video]";
				if (msg.caption) { strBody = msg.caption }
				var mo = { g:3, m: strBody, rep: 'wapi', t:"mo", ts: Date.now(), type: 'vid', mimetype: msg.mimetype, size: msg.size, thumb: msg.body, url: post.msg.fn };
				break;
			case 'audio':
				var mo = { g:3, m: '[audio]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'aud', mimetype: msg.mimetype, size: msg.size, url: post.msg.fn };
				break;
			case 'ptt':
				var mo = { g:3, m: '[audio]', rep: 'wapi', t:"mo", ts: Date.now(), type: 'aud', mimetype: msg.mimetype, size: msg.size, url: post.msg.fn };
				break;
			default:
				var mo = { g:3, m: msg.type, rep: 'wapi', t:"mo", ts: Date.now(), type: msg.type };
		}
		var name = sender.id.user;
		if ( sender.pushname ) { name = sender.pushname; }
		if ( sender.name ) { name = sender.name; }
		var user = { chn: 'wsp', id: sender.id.user, line: line, nick: name, lu: Date.now() }
		if (lineOb.hasBot) {
			//bot will process after save
		} else {
			//no bots line chat must open in textcenter
			user.status = "active";	
		}
		if (chat.id.user) {
			var contact = chat.id.user;
		} else {
			var contact = sender.id.user;
		}
		
		if ( contact.indexOf ("-")>0 ) { //is group
			mo.contact = sender.id.user;
			mo.nick = sender.id.user;
			if (sender.pushname) {
				mo.nick = sender.pushname;
			}
			if (chat.formattedTitle) {
				user.nick = chat.formattedTitle;
			}
		}

		//create or update user
		firebase.database().ref("/accounts/"+lineOb.client+"/chats/"+contact).update(user).then(function() {
			firebase.database().ref("/accounts/"+lineOb.client+"/conves/"+contact+"/msgs/"+msg.id.id).set(mo).then(function() {
			   //run bot
			   if (lineOb.hasBot) {
				   var job = {
					   msg: msg,
					   contact: contact,
					   st: 'new',
					   ts: Date.now()
					   }
				   firebase.database().ref("/accounts/"+lineOb.client+"/botJobs").push(job);
			   }
			}).catch(function(error) {
			   console.log (new Date(), "Error posting MO to Firebase", line);
			   console.log (error)
			   console.log (mo)
			   //TODO: persist this msg in local file to resend MOs
			});
		}).catch(function(error) {
			console.log (new Date(), "Error posting User to Firebase", line);
			console.log (error)
			console.log (sender)
			console.log (user)
			console.log (mo)
			console.log (post)
			//TODO: persist this msg in local file to resend MOs
		});
	} else {
		console.log (new Date(), "Error processing MO line not found", line);
	}

}
function pushWatcher (line, watch ) {
	firebase.database().ref("/lines/"+line+"/watch").set( watch  );
}

function processACK(post) {
	if (post.me) {
		var lineOb = _.findWhere( lines, {id: post.me.user} );
		
		if (lineOb) {
			//update ACK and LU of MT
			if (post.msg.muid) {
				var parts = post.msg.muid.split("-");
				firebase.database().ref("/accounts/"+lineOb.client+"/conves/"+parts[2]+"/msgs/"+post.msg.muid+"/g").set(post.msg.ack);
				firebase.database().ref("/accounts/"+lineOb.client+"/conves/"+parts[2]+"/msgs/"+post.msg.muid+"/lu").set(Date.now());
				console.log (new Date(), "ACK updated for |"+post.msg.muid+"|");
			}
		} else {
			console.log (new Date(), "Error process ACK line not found", post.me.user);
		}
	} else {
		console.log (new Date(), "Error process ACK line not found", post);
	}
	
}

function ackMTSent(msgId, line) {
	var lineOb = _.findWhere( lines, {id: line} );
	firebase.database().ref("/accounts/"+lineOb.client+"/mtq/"+msgId).update({ line: line, lu: Date.now(), st: 'snt' }).then(function() {
		console.log ("MT Sent", lineOb.client, msgId);
	}).catch(function(error) {
		console.log(new Date(), "Error setting ACK MT SENT", error);
	});
}

function purgeWorker() {
	if (purge.length>0) {
		purgeMT( purge.shift() ); 
	} else {
		setTimeout(function() { purgeWorker() }, 60000);	
	}	
}

function purgeMT(msg) {	
	//set mtq status to purged
	firebase.database().ref("/accounts/"+msg.client+"/mtq/"+msg.id).update({ lu: Date.now(), st: 'pur' })
	console.log ("Purged", msg.id, msg.client, " purge q is", purge.length);
	setTimeout(function() { purgeWorker() }, 50)
}

function loadBotAssets() {
	var content = fs.readFileSync('./bots/assets/ads/hornero/congelados.txt', 'utf8');
	if (content) {
		console.log ("Loaded bot asset from file");
		assetsCache['hornero_congelados'] = content;
	}
	var content = fs.readFileSync('./bots/assets/ads/hornero/promo.txt', 'utf8');
	if (content) {
		console.log ("Loaded bot asset from file");
		assetsCache['hornero_promo'] = content;
	}
	var content = fs.readFileSync('./bots/assets/ads/hornero/martes.txt', 'utf8');
	if (content) {
		console.log ("Loaded bot asset from file");
		assetsCache['hornero_martes'] = content;
	}
	var content = fs.readFileSync('./bots/assets/ads/hornero/nino2020.txt','utf8');
	if (content) {
		console.log ("Loaded bot asset from file");
		assetsCache['hornero_nino2020'] = content;
	}
}

function saveAsset(post) {
	fs.writeFileSync('./assets/'+post.client+'_'+post.id+'.txt', post.b64, 'utf8');
	console.log ("asset saved")
}

function saveMedia(post) {
	var client = 'demo'
	if (post.client) { client = post.client }
	fs.writeFileSync('./assets/media/'+client+'_'+post.fn, post.blob, 'utf8');
	console.log ("media save for", client);
}

function getAsset(code, res) {
	console.log ("Getting asset", code)
	var content = fs.readFile('./assets/'+code+'.txt', 'utf8', function(err, data) {
			if (!err) {
				assetsCache[code] = data;
				var msg = data;
			} else {
				console.log (err);
				var msg = "nuay";
			}
			res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			res.write(msg);
			res.end();
			return;
	});
}

function getMedia(code, res) {
	console.log ("Getting media", code)
	var parts = code.split("_");
	var content = fs.readFile('./assets/media/'+parts[0]+"_"+parts[1]+"."+parts[2], 'utf8', function(err, data) {
			if (!err) {
				assetsCache[code] = data;
				var msg = data;
			} else {
				console.log (err);
				var msg = "nuay";
			}
			res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			res.write(msg);
			res.end();
			return;
	});
}

function hookAsset(mt) {
	if (mt.thumb) {
		if (mt.thumb.indexOf('b64Asset')>=0) {
			var asset = JSON.parse(mt.thumb);
			//check if loaded on cache
			
			if ( assetsCache[asset.code] ) {
				console.log ("Hooked asset from cache");
				mt.thumb = assetsCache[asset.code];
				return mt;
			} else {
				var content = fs.readFileSync(asset.path, 'utf8');
				if (content) {
					console.log ("Hooked asset from file");
					assetsCache[asset.code] = content;
					mt.thumb = content;
				} else {
					delete mt.thumb
					mt.type = "txt";	
				}
				return mt;
			}
			
		} else {
			return mt;	
		}
	} else {
		return mt;
	}	
}

function ackToTextotal(msgId, status, datetime) {
	if (!status) {status = 'SENT'}
	var url = "http://admin:pinguino2020@textotal.com/api/messages/"+msgId+"/status";
	var postData = {status: status}
	if (datetime) { postData.datetime = datetime }
	var options = { 
			method: 'POST',
			url: url,
			json: postData
	};
	request(options, function (error, resp, body) {
		if (error) {
			console.log (new Date(), options.url, error)
		} else {
			console.log (new Date(), options.url, body)
		}
	});	
}

function ackToWhatssend(msgId, status, datetime) {
	if (!status) {status = 'SENT'}
	var url = "http://admin:Pinguino2020@whatssend.com/api/messages/"+msgId+"/status";
	var postData = {status: status}
	if (datetime) { postData.datetime = datetime }
	var options = { 
			method: 'POST',
			url: url,
			json: postData
	};
	request(options, function (error, resp, body) {
		if (error) {
			console.log (new Date(), options.url, error)
		} else {
			console.log (new Date(), options.url, body)
		}
	});	
}

function processPost(req, res, callback) {
	var queryData = "";
	if(typeof callback !== 'function') return null;

	req.on('data', function(data) {
		queryData += data;
		if(queryData.length > 10000000) {
			queryData = "";
			res.writeHead(413, {'Content-Type': 'text/plain'}).end();
			req.connection.destroy();
		}
	});

	req.on('end', function() {
		if (req.method == 'GET') {
			req.post = qs.parse(req.url.replace(/^.*\?/, ''));
		} else {
			req.post = queryData;
		}
		callback();
	});
}


var app = function(req, res) {

     if(cors.apply(req, res)) {
		 res.end();
		 return;
	 }

	 processPost(req, res, function() {
		//console.log ("URL", req.url);
		var parts = req.url.split("/");
		//router
        switch (parts[1]) {
			case 'send':
                //process MT received from API call
				//~ console.log (new Date(), "MT received");
				   if (req.method == "POST") {
	
						if (typeof req.post === "string") {
							var post = _.clone ( qs.parse(req.post)	);
						} else {
							var post = _.clone ( req.post );
						}

						if (tokens.indexOf(post.apiToken)>=0 ) {
							var numberCheck = tools.numberFormat(post.to, post.isResponse)
							if (numberCheck.valid) {
								post.to = numberCheck.to;
								if (post.body) {
									if (post.body.length > 0) {
										pushMT (post)
										res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
										res.write("mt-pushed-ok");
										res.end();
										return;
									} else {
										res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
										res.write("invalid-message");
										res.end();
										return;
									}
								} else {
									res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
									res.write("invalid-message");
									res.end();
									return;
								}
							} else {
								res.writeHead(403, "ERROR", {'Content-Type': 'text/plain'});
								res.write("invalid-number");
								res.end();
								return;
							}
						} else {
							res.writeHead(401, "ERROR", {'Content-Type': 'text/plain'});
							res.write("invalid-token");
							res.end();
							return;
						}

				   } else {
					   res.writeHead(405, {'Content-Type': 'text/plain'});
					   res.write("Invalid request method");
					   res.end();
					   return;
				   }

                break;
            case 'pushMO':                
			    //~ console.log (new Date(), "MO received");
			    //~ console.log (req.post);
			    if (req.post) {
					var postObj = JSON.parse(req.post);
					if (postObj.me) {
						processMO ( postObj );
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write("mo-pushed-ok");
						res.end();
						return;
					} else {
						//~ res.writeHead(500, "ERROR", {'Content-Type': 'text/plain'});
						//~ res.end();
						//~ return;
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write("mo-pushed-ok");
						res.end();
						return;
					}
				}
                break;
            case 'pullMT':
				var line = parts[2];
				if (line) {
					if ( mtq[line] ) {
						if ( mtq[line].length>0 ) {
							console.log (new Date(), "MT PULL OK LENGTH = " + mtq[line].length, line);
							var mt = mtq[line].shift();
							msg = JSON.stringify( hookAsset(mt) );
							ackMTSent(mt.id, line);
						} else {
							//console.log (new Date(), "MT PULL MTQ EMPTY", line);
							msg = "mtq-empty";
						}
					} else {
						console.log (new Date(), "MT PULL INVALID LINE", line);
						msg = "invalid-line";
					}
				} else {
					console.log (new Date(), "MT PULL INVALID LINE", line);
					msg = "invalid-line";	
				}
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write( msg );
			    res.end();
				return;
                break;
            case 'upload':                
			    //~ console.log (new Date(), "UPLOAD received");
			    //~ console.log (req.post);
			    if (IsJsonString(req.post)) {
					saveMedia ( JSON.parse(req.post) ); 
				} else {
					console.log (new Date(), "Upload Error")	
				}
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("upload-ok");
			    res.end();
				return;
                break;
            case 'ack':                
			    //~ console.log (new Date(), "ACK received");
			    //console.log (req.post)
			    if (req.post) {
					var postObj = JSON.parse(req.post);
					processACK ( postObj );
				}
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("ack-ok");
				res.end();
				return;
                break;
            case 'watcher':                
			    //console.log (new Date(), "Watcher received");
			    //console.log (req.post)
			    if (req.post) {
					var obj = JSON.parse(req.post); //To Do catch error SyntaxError: Unexpected end of JSON input
					if (obj.me) {
						var line = obj.me.user;
						var watch = {
							phoneAuthed: obj.msg.stream.phoneAuthed,
							info: obj.msg.stream.info,
							mode: obj.msg.stream.mode,
							connected: obj.msg.conn.connected,
							battery: obj.msg.conn.battery,
							plugged: obj.msg.conn.plugged,
							ts: Date.now(),
							ql: 0,
							moql: 0,
							tunnelStatus: "-"
						}
						if (mtq[line]) { watch.ql = mtq[line].length }
						if (!watch.connected) { watch.connected = false }
						if (!watch.plugged) { watch.plugged = false }
						if (!watch.battery) { watch.battery = false }
						if (obj.phone) { watch.phone = obj.phone }
						if (obj.moql) { watch.moql = obj.moql}
						if (obj.tunnelStatus) {watch.tunnelStatus = obj.tunnelStatus}
						//push watcher
						pushWatcher(line, watch );
					}
					
				}
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("watch-ok");
				res.end();
				return;
				break;
			    
            case 'err':                
			    console.log (new Date(), "ERR received");
			    console.log (req.post)
			    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
			    res.write("err-ok");
			    res.end();
				return;
                break;
            case 'save-asset':
			    //~ console.log (new Date(), "Save asset requested");
			    if (req.method == "POST") {
	
					if (typeof req.post === "string") {
						var post = _.clone ( qs.parse(req.post)	);
					} else {
						var post = _.clone ( req.post );
					}

					saveAsset(post)
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write("save-ok");
					res.end();
					return;

			   } else {
				   res.writeHead(405, {'Content-Type': 'text/plain'});
				   res.write("Invalid request method");
				   res.end();
				   return;
			   }
			    
                break;
            case 'asset':
			    //~ console.log (new Date(), "Asset requested");
			    if (parts[2]) {
					if (assetsCache[parts[2]]) {
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write(assetsCache[parts[2]]);
						res.end();
						return;
					} else {
						getAsset(parts[2],res);
					}
				} else {
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write('nuay');
					res.end();
					return;
				}
			    
                break;
            case 'media':
			    //~ console.log (new Date(), "Media requested");
			    if (parts[2]) {
					if (assetsCache[parts[2]]) {
						res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
						res.write(assetsCache[parts[2]]);
						res.end();
						return;
					} else {
						getMedia(parts[2],res);
					}
				} else {
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write('nuay');
					res.end();
					return;
				}
			    
                break;
            case 'track':                
			    //~ console.log (new Date(), "tracked", parts[2]);
			    fs.readFile('sample.jpeg', function (err, content) {
					if (err) {
						res.writeHead(400, {'Content-type':'text/html'})
						console.log(err);
						res.end("No such image");    
					} else {
						//specify the content type in the response will be an image
						res.writeHead(200,{'Content-type':'image/jpg'});
						res.end(content);
					}
				});
                break;
            case "fixWhatssendACK":
				if (req.method == "POST") {
					if (typeof req.post === "string") {
						var post = _.clone ( qs.parse(req.post)	);
					} else {
						var post = _.clone ( req.post );
					}
					//patch to fix Whatssend ACK
					console.log (post)
					ackToWhatssend(post.msgId, post.status, post.datetime);
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write("Fix sent OK");
					res.end();
					return;
				} else {
					res.writeHead(405, {'Content-Type': 'text/plain'});
					res.write("Invalid request method");
					res.end();
					return;
				}
			    break;  
			case "fixTextotalACK":
			    //patch to fix Textotal ACK
			    if (req.method == "POST") {
					if (typeof req.post === "string") {
						var post = _.clone ( qs.parse(req.post)	);
					} else {
						var post = _.clone ( req.post );
					}
					//patch to fix Whatssend ACK
					console.log (post)
					ackToTextotal(post.msgId, post.status, post.datetime);
					res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
					res.write("Fix sent OK");
					res.end();
					return;
				} else {
					res.writeHead(405, {'Content-Type': 'text/plain'});
					res.write("Invalid request method");
					res.end();
					return;
				}
				break;
            case "ping":
				res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
				res.write("pong");
				res.end();
				return;
				break;

            default:
			   console.log(new Date(), "unknown path "+req.url);
			   res.writeHead(404, {'Content-Type': 'text/plain'});
			   res.write("unknown path "+req.url);
			   res.end();
			   return;
        }
    });
}

function IsJsonString(str) {
	if (str.indexOf("[")==0 || str.indexOf("{")==0) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	} else {
		return false;	
	}
}
