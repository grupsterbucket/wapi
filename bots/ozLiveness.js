global.fetch = require('node-fetch');
var FormData = require('form-data');
global.fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));

var payload = {
  "media:tags": {s
    "video1": [
      "video_selfie_blank"
    ]
  }
}

var payloadPhoto = {
  "media:tags": {
    "photo1": [
      "video_selfie_blank"
    ]
  }
}

var formData = new FormData();
formData.append('payload', JSON.stringify(payload));
//formData.append('photo1', fs.createReadStream('./tempVids/593999703430_1673996305836-1.zip'));
formData.append('video1', fs.createReadStream('./tempVids/593984252217_1673910320380'));

fetch('https://api.sandbox.ozforensics.com/api/folders', {
	method: 'post',
	body: formData,
	headers: {
		'content-type': 'multipart/form-data',
		'Connection': 'keep-alive',
		'X-Forensic-Access-Token': env.ozAccessToken
	}
}).then(function(response) {
	if (response.status !== 200 ) {
		console.log(new Date(),'OZ upload Error',response.status);
		response.text().then(function(data) {
			console.log (data)
		});
	} else {
		response.json().then(function(data) {
			console.log (data)
		});
	}
}).catch(function(err) {
	console.log(new Date(),'OZ upload Error',err.message);
	console.log (err)
	
});		
