"use strict";
const { default: axios } = require('axios')

class Payphone {
  constructor() {}

  static getTransaction(transactionId, payphoneToken, cb) {
    axios( {
			url: 'https://pay.payphonetodoesposible.com/api/Sale/client/' + transactionId,
			method: 'get',
			headers: {
				'Authorization': 'Bearer ' +payphoneToken,
				'Content-Type': 'application/json'
			}
		}).then(function(response) {
			if (response.status !== 200 ) {
				console.log(new Date(),'Payphone API error GT',response.status);
				// Examine the text in the response
				console.log(response.data);
				cb (false)
			} else {
				// Examine the text in the response
				console.log ("PAYPHONE RESPONSE GET TRANSACTION")
				console.log(response.data);
				cb (response.data)  
			}
		}).catch(function(err) {
			console.log(new Date(),'Payphone API error GT',err.message);
			cb (false)
		});
  }

  static prepareButton(payload, payphoneToken, cb) {
	  console.log (payload)
	  console.log (payphoneToken)
    
	  axios( {
			url: 'https://pay.payphonetodoesposible.com/api/button/Prepare',
			method: 'post',
			data:    JSON.stringify(payload),
			headers: {
				'Authorization': 'Bearer ' +payphoneToken,
				'Content-Type': 'application/json'
			}
		}).then(function(response) {
			if (response.status !== 200 ) {
				console.log(new Date(),'Payphone API error PB',response.status);
				// Examine the text in the response
				console.log(response.data);
				cb (false)
			} else {
				// Examine the text in the response
				console.log ("PAYPHONE RESPONSE PREPARE BUTTON")
				console.log(response.data);
				cb (response.data)
				
			}
		}).catch(function(err) {
			console.log (err)
			console.log(new Date(),'Payphone API error PB');
			cb (false)
		});
  }

  static confirmPayment(payload, payphoneToken, cb) {
	    console.log ("Checking payment")
	    console.log (payload)
		axios( {
			url: 'https://pay.payphonetodoesposible.com/api/button/V2/Confirm',
			method: 'post',
			data:    JSON.stringify(payload),
			headers: {
				'Authorization': 'Bearer ' +payphoneToken,
				'Content-Type': 'application/json'
			}
		}).then(function(response) {
			if (response.status !== 200 ) {
				console.log(new Date(),'Payphone API error CP',response.status);
				// Examine the text in the response
				console.log(response.data);
				cb (false)
			} else {
				// Examine the text in the response
				console.log ("PAYPHONE RESPONSE CONFIRM PAYMENT")
				console.log(response.data);
				cb (response.data)
			}
		}).catch(function(err) {
			console.log(new Date(),'Payphone API error CP',err.message);
			cb (false)
		});
  }
}

module.exports = Payphone;
