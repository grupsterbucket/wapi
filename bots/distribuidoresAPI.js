// UT API plugin
module.exports = {

  findByPhone: async function (numTelf) {

    var options = {
      method: 'GET',
      headers: {
        'x-api-key': env.prod.ut.distribuidoresApiKey,
        'Content-Type': 'application/json',
      },
    }

    const respuesta = await fetch(env.prod.ut.distribuidoresApiURL + '/api/' + `${numTelf}`, options)
    const respuestaFinal = await respuesta.text()


    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

  getBalance: async function (partnerUid) {
    var options = {
      method: 'get',
      headers: {
        'x-api-key': env.prod.ut.distribuidoresApiKey,
        'Content-Type': 'application/json',
      }
    }

    const respuesta = await fetch(env.prod.ut.distribuidoresApiURL + '/api/balance/' + `${partnerUid}`, options)
    const respuestaFinal = await respuesta.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

  createStore: async function (partnerUid, identificationType, numIdentification, nombre, phone, email, address, businessName, city, province) {
    var options = {
      method: 'post',
      headers: {
        'x-api-key': env.prod.ut.distribuidoresApiKey,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "parent": partnerUid,
        "identificationType": identificationType,
        "identification": numIdentification,
        "name": nombre,
        "phone": phone,
        "email": email,
        "address": address,
        "businessName": businessName,
        "city": city,
        "province": province
      })
    }

    const respuesta = await fetch(env.prod.ut.distribuidoresApiURL + '/api/store', options)
    const respuestaFinal = await respuesta.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

  recharge: async function (partnerUid, amount, type, transactionNumber) {
    var options = {
      method: 'post',
      headers: {
        'x-api-key': env.prod.ut.distribuidoresApiKey,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "partnerUuid": partnerUid,
        "amount": amount,
        "type": type,
        "transactionNumber": transactionNumber
      })
    }

    const respuesta = await fetch(env.prod.ut.distribuidoresApiURL + '/api/recharge', options)
    const respuestaFinal = await respuesta.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

  debit: async function (partnerUid, amount) {
    var options = {
      method: 'post',
      headers: {
        'x-api-key': env.prod.ut.distribuidoresApiKey,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "partnerUuid": partnerUid,
        "amount": amount
      })
    }

    const respuesta = await fetch(env.prod.ut.distribuidoresApiURL + '/api/debit', options)
    const respuestaFinal = await respuesta.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

  consultarPrecios: async function () {
    var options = {
      method: 'post',
      body: JSON.stringify({
        "apikey": env.prod.ut.utKey,
        "uid": env.prod.ut.utUid
      })
    }


    const respuesta = await fetch('https://uanataca.ec/api_wa_ua/v1/consultar_preciosTodos', options)

    let respuestaFinal = await respuesta.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

  generarLink: async function (partnerUid, partnerType, cedulaCliente, celularCliente, correoCliente, validez, contenedor) {
    var options = {
      method: 'post',
      body: JSON.stringify({
        "apikey": env.prod.ut.utKey,
        "uid": env.prod.ut.utUid,
        "crc_id": partnerUid,
        "crc_tipo": partnerType,
        "cedulaCliente": cedulaCliente,
        "celularCliente": celularCliente,
        "correoCliente": correoCliente,
        "validez": validez,
        "contenedor": contenedor
      }),
    }

    const respuesta = await fetch('https://api.uanataca.ec/v4/CrearLinkCRC01', options)

    let respuestaFinal = await respuesta.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

  consultarEstadoSolicitud: async function (numCedula, perfil) {
    var options = {
      method: 'post',
      body: JSON.stringify({
        "uid": env.prod.ut.utUid,
        "apikey": env.prod.ut.utKey,
        "numerodocumento": numCedula,
        "tipo_solicitud": perfil
      })
    }

    const respuesta = await fetch(envBot[account].UTAPIURL + '/consultarEstado', options)
    const respuestaFinal = await respuesta.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: 502 }
    }
  },

  consultarEstadoSolicitudMp: async function (numCedula, perfil) {
    var options = {
      method: 'post',
      body: JSON.stringify({
        "uid": env.prod.ut.utUid,
        "apikey": env.prod.ut.utKey,
        "numerodocumento": numCedula,
        "tipo_solicitud": perfil
      })
    }

    const respuesta = await fetch(envBot[account].UTAPIURL + '/consultarEstado', options)
    const respuestaFinal = await respuesta.text()

    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: 502 }
    }
  },


  findRequests: async function (partnerUid, cedula) {

    var options = {
      method: 'GET',
      headers: {
        'x-api-key': env.prod.ut.distribuidoresApiKey,
        'Content-Type': 'application/json',
      },
    }

    const respuesta = await fetch(env.prod.ut.distribuidoresApiURL + '/api/stakeholder/' + `${partnerUid}` + '/requests/' + `${cedula}`, options)
    const respuestaFinal = await respuesta.text()


    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

  findReverseUanacredits: async function (solicitudUid) {

    var options = {
      method: 'GET',
      headers: {
        'x-api-key': env.prod.ut.distribuidoresApiKey,
        'Content-Type': 'application/json',
      },
    }

    const respuesta = await fetch(env.prod.ut.distribuidoresApiURL + '/api/uanacredits/' + `${solicitudUid}` + '/reverse', options)
    const respuestaFinal = await respuesta.text()


    if (tools.isJsonString(respuestaFinal)) {
      return JSON.parse(respuestaFinal)
    } else {
      return { status: respuesta.status, msg: respuesta.statusText }
    }
  },

}
