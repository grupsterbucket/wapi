var _ = require('underscore');
var firebase = require('firebase');
var booted = false;
var config = {}
var account = "demo"; //default
if (process.argv[2]) {
	account = process.argv[2];
}

//firebase init
var firebaseConfig = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
	"authDomain": "ubiku3.firebaseapp.com",
	"databaseURL": "https://ubiku3.firebaseio.com",
	"projectId": "ubiku3",
	"storageBucket": "ubiku3.appspot.com",
	"messagingSenderId": "946813430025",
	"appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(function (user) {
	if (user) {
		// User is signed in.
		console.log("Logged in to firebase as uid " + user.uid);
		setTimeout(function () {
			bootApp();
		}, 2000);

	} else {
		// User is signed out login again
		console.log("Session expired");
		loginTofirebase();
	}
	// ...
});

function loginTofirebase() {
	console.log("Login to firebase");
	firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
		var errorCode = error.code;
		var errorMessage = error.message;
		console.log(error.code + ": " + error.message);
	});
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		loadAccountConfig();
	}
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/' + account + '/config/').once('value', function (snapshot) {
		config = snapshot.val();
		console.log("Config fetched for account", account);
		loadEvents()
	});
}


function loadEvents() {
	var d = new Date();
	d.setDate(d.getDate() + 1); //tomorrow
	var ymd = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
	console.log("Searching events", account, ymd);
	firebase.database().ref("/accounts/" + account + "/events").orderByChild("ymd").equalTo(ymd).once("value", function (res) {
		if (res.val()) {
			var arr = _.map(res.val(), function (ob, key) {
				ob.id = key;
				return ob;
			});
			console.log("Events found", arr.length);
			for (var i = 0; i < arr.length; i++) {
				sendReminder(arr[i]);
			}
			setTimeout(function () { process.exit(0) }, 10000);
		} else {
			console.log("Events not found");
		}
	}).catch(function (error) {
		console.log(error)
	});
}

function sendReminder(ev) {
	console.log("Send reminder for", ev.account, ev.resource)
	var d = new Date(ev.dateStart)
	var cal = config.calendars[ev.resource]
	var param = "*" + d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + "* a las *" + formatHours(Number(d.getHours() + "" + (d.getMinutes() == 0 ? '00' : d.getMinutes()))) + "* en *" + cal.caption + "*";
	//set user to step reminder
	firebase.database().ref("/accounts/" + account + "/chats/" + ev.account + "/step").set("reminder")
	pushTemplate([param], 'event_reminder', ev.account)
}

function pushTemplate(params, templateName, contact) {
	firebase
		.database()
		.ref('/accounts/' + account + '/chats/' + contact)
		.once('value', function (snapshot) {
			var user = snapshot.val()

			var msgId = Date.now() + "-" + user.line + "-" + contact;
			var mt = { g: 0, m: "Enviado recordatorio de cita para el " + params[0], rep: "bot", t: "mt", ts: Date.now(), type: "txt", st: "qed", lu: Date.now() };
			mt.isTemplate = true
			mt.lang = "es"; //todo read from DB
			mt.params = params.join("|");
			mt.templateName = templateName;
			firebase.database().ref("/accounts/" + account + "/chats/" + contact + "/msgs/" + msgId).set(mt).then(function () {
				var ob = _.clone(mt);
				ob.line = user.line;
				ob.to = contact;
				ob.account = account;
				//push to account MT queue
				firebase.database().ref("/accounts/" + account + "/mtq/" + msgId).set(ob).then(function () {
					console.log(new Date(), "Template Response POSTED OK");
				}).catch(function (error) {
					console.log(new Date(), "Push MTQ Error", error);
				});
			}).catch(function (error) {
				console.log(new Date(), "Push MTQ Error", error);
			});
		})

}

function formatHours(n) {
	if (n < 1000) {
		n += ""
		return n.substring(0, 1) + "h" + n.substring(1, 3);
	} else {
		n += ""
		return n.substring(0, 2) + "h" + n.substring(2, 4);
	}
}
