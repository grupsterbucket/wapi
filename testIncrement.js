var _ = require('underscore');
var firebase = require('firebase');
var admin = require("firebase-admin");
var fs = require('fs');
var path = require('path');
//~ //firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

 
firebase.initializeApp(config);


//Initialize Firebase Admin
admin.initializeApp({
  credential: admin.credential.cert( JSON.parse(fs.readFileSync(path.resolve(__dirname, './whatabot-sa.json'), 'utf8')) ),
  databaseURL: "https://whatabot-ac0d8.firebaseio.com"
});


firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            boot();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function boot() {
	//test autoincrement
		 var sv = firebase.database.ServerValue;
         var ref = firebase.database().ref('/camps/test/count')
         ref.set(sv.increment(1));
}

//admin functions
function updateUserPassword(pwd, uid) {
	admin.auth().updateUser(uid, {password: pwd}).then(function(userRecord) {
		console.log("Successfully updated user password for ", uid);
	})
	.catch(function(error) {
		console.log("Error updating user password:", error);
	});
}
