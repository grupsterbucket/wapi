
module.exports = {
  bot: function (body, job, user, step, group) {
    console.log('Processing ' + account + ' call2bMain', user.id, step, group)
    console.log(job)
    switch (step) {
      //*Aqui me llega si acepta o no acepta o abro ticket si es default
      case "0":
        var kw = body.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "")
        console.log('KW', kw)
        switch (kw) {
          case 'renovar':
            updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
            pushText('Listo, en un momento te atendemos', job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sac");
            break

          case 'detener promociones':
            updateUser({ status: "closed" }, job.contact);
            var blacklistItem = {
              contacto: job.contact,
              tsc: Date.now(),
            }
            crearBlacklist(blacklistItem)
            pushText('Entiendo, gracias por tu atención', job.contact, user.line);
            break

          case 'ya renove':
            updateUser({ status: "closed" }, job.contact);
            var renewedItem = {
              contacto: job.contact,
              tsc: Date.now(),
            }
            createRenewed(renewedItem)
            pushText('Gracias por tu respuesta.', job.contact, user.line);
            break


          default:
            updateUser({ step: "idle", status: "active", group: "sac", tso: Date.now() }, job.contact);
            pushText('Buen día, un momento por favor en seguida te atendemos.', job.contact, user.line);
            logStartTicket(job.contact, user.chn, user, "sac");
        }
        break;
    }
  },

  job: function (job, user, step, group, flow, cb) {
    console.log("Entra un job para el módulo call2bMain")
    console.log(job)

    cb({ st: "done" })
  }
}


