var request = require('request');
global.tools = require('./tools');
var fs = require('fs');
var path = require('path');
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
var plugin = require('./condoPlugin');
global._ = require('underscore');
global.firebase = require('firebase');
var CryptoJS = require("crypto-js");
var account = "condo";
var booted = false;
var jobs = []
var os = require('os');
var qitv = 10;
global.config = {}
global.calendars = []

//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		//load account config
		loadAccountConfig();
		setTimeout(function(){
			listenToJobs();
			processJobQueue();
		}, 2000);
	}	
}

function loadAccountConfig() {
	var self = this;
	firebase.database().ref('accounts/'+account+'/config/misc').on('value', function(snapshot) {		  
		config = snapshot.val();
		console.log ("Config fetched for account",account);
	});
	firebase.database().ref('accounts/'+account+'/config/calendars').on('value', function(snapshot) {		  
		calendars = _.map(snapshot.val(), function(ob, key){
			ob.id = key;
			return ob;
		});
		console.log ("Calendars fetched for account",account);
	});
}
function listenToJobs() {
	firebase.database().ref("/accounts/"+account+"/botJobs/").orderByChild("st").equalTo("new").on('child_added', function(res) {
		var job = res.val();
		job.id = res.key;
		jobs.push(job);
		console.log (new Date(), "New job queued", job.id);
	});
}

function processJobQueue() {
	if (jobs.length>0) {
		processJob( jobs.shift() );
	} else {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	}
}

function processJob(job) {
	switch (job.type) {
		case 'send-notification':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				pushText (job.str, job.contact, user.line);
				if (job.context == 'new-event') {
					//~ //create task
					//~ createTask(user, {
						//~ group: user.group,
						//~ account: user.account,
						//~ status: 'new',
						//~ dura: 10,
						//~ type: 'contact-request',
						//~ title: 'Aprobar Reserva de '+job.calendar.caption,
						//~ needsReply: false,
						//~ linkTo: '#calendar',
						//~ body: user.name+' ha creado una reserva.', tsc: Date.now(), createdBy: 'bot'}
					//~ );	
					
					//send template to admin
					if (user.condos) {
						var condo = _.findWhere(user.condos, {id: user.group});
						if (condo) {
							if (condo.config) {
								if (condo.config.notif) {
									var sendTos = condo.config.notif.split(",");
									for (var i=0; i<sendTos.length; i++) {
										var to = sendTos[i].trim();
										if (to.length == 10 || to.length == 12) {
											if (to.length==10) { to = to.replace("09","5939") }
											//pushTemplate(["nueva reserva http://v3.textcenter.net/#calendar"], "new_ticket_created", to, user.line);
										}
									}
								}
							}
						}
					}
					
				}
				//job is completed
				finishJob(job);
			});
			break;
		case 'reset-bot':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				resetBot (job.contact, user);
				//job is completed
				finishJob(job);
			});
			break;
		case 'booking-completed':
			firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
				var user = snapshot.val();
				//return main menu to user
				plugin.bot("", job, user, "999", user.group, account);
				//job is completed
				finishJob(job);
			});
			break;	
			
		default:
			if (job.contact == "593991307152") {
				//ignore
				finishJob(job);
				console.log ("bot bot convo ignored")
			} else {
				firebase.database().ref("/accounts/"+account+"/chats/"+job.contact).once("value", function(snapshot) {
					var user = snapshot.val();
					var step = "0";
					var lastStep = "0";
					var group = "main";
					if (user.step) {step = user.step};
					if (user.lastStep) {lastStep = user.lastStep};
					if (user.group) {group = user.group};
					
					if (user.tsso) {
						console.log ("User "+job.contact+" is at step:", step, "group:", group);
					} else {
						startSession (job.contact, user);	
						console.log ("New Session for "+job.contact+"");
					}
					
					if (step != 'idle') {
							console.log ("Process MO ", job.msg.type);
							
							switch (job.msg.type) {
								case 'text':
									console.log ("Body", job.msg.text.body);
									var body = job.msg.text.body;
									if (body.length>0) {
										//trim spaces
										body = body.trim();

										//catch operations
										if (body.indexOf("#") == 0) {
											var parts = body.split(" ");
											//match direct operation
											switch ( parts[0] ) {
												case "#salir":
													salir(job, user);
													break;
												case "#ping":
													var str = "#pong "+ new Date();
													pushText (str, job.contact, user.line);
													break;
												case "#reservas":
													getReservas(job.contact, user.line, parts[1]);
													break;
												case "#server":
													var str = "Server data:\n";
													str += "Datetime: "+ new Date()+"\n";
													str += "Ram: "+ Math.round(os.totalmem()/1000000)+"MB\n";
													str += "Free: "+ Math.round(os.freemem()/1000000)+"MB\n";
													pushText (str, job.contact, user.line);
													break;
												default:
													var str = "Operación desconocida";
													pushText (str, job.contact, user.line);
											} //end switch parts operation
										} else {
											if (body.toLowerCase() == "salir" || body.toLowerCase() == "*salir*") {
												salir(job, user);
											} else if (body.toLowerCase() == "volver" || body.toLowerCase() == "*volver*") {
												setUserParam("step", lastStep, job.contact);
												step = lastStep;
												plugin.bot(body.toLowerCase(), job, user, step, group, account);
											} else {
												//save step as last step to implement volver
												setUserParam("lastStep", step, job.contact);
												//bot is enabled?
												if (config.botEnabled) {
													//chat is not operation, run bot
													if (isShopOpen()) {
														plugin.bot(body.toLowerCase(), job, user, step, group, account);
													} else {
														pushText (config.horarioStr, job.contact, user.line);
													}
												} else {
													if (config.autoMessage) {
														pushText (config.autoMessage, job.contact, user.line);
													} else {
														pushText ("Servicio en mantenimiento", job.contact, user.line);
													}
												}
												
											}
										} //end if operation
									} //body is empty
									//job is completed
									finishJob(job);
									break;
								case 'image':
									plugin.bot("[image]", job, user, step, group, account);
									//job is completed
									finishJob(job);
									break;
								case 'video':
									plugin.bot("[video]", job, user, step, group, account);
									//job is completed
									finishJob(job);
									break;
								case 'audio':
									plugin.bot("[audio]", job, user, step, group, account);
									//job is completed
									finishJob(job);
									break;
								case 'file':
									plugin.bot("[file]", job, user, step, group, account);
									//job is completed
									finishJob(job);
									break;
								case 'location':
									plugin.bot("[location]", job, user, step, group, account);
									//job is completed
									finishJob(job);
									break;
								default:
									//job is completed
									finishJob(job);
									break;
							} //end switch mo type
					} else { //end idle check
						console.log ('user '+job.contact+'@'+user.line+' is idle');
						//is a reset operation?
						if (job.msg.text) { 
							var body = job.msg.text.body;
							if (body.length>0) {
								//trim spaces
								body = body.trim();
								if (body.toLowerCase().indexOf("#") == 0) {
									if (body.toLowerCase() == "#reset") {
										resetBot(job.contact, user, "Reset Session OK");
									}
								}
							}
						}
						//job is completed
						finishJob(job);
					}
				});
			} //end bot bot
		}//end switch job type
}

function finishJob(job) {
	firebase.database().ref("/accounts/"+account+"/botJobs/"+job.id+"/st/").set('done', function(res) {
		setTimeout(function() {
			processJobQueue();
		}, qitv );
	});
}

global.pageBreak = function() {
	return '\n\n--------------------------oooo--------------------------\n\n'
	}

global.pushText = function  (body, contact, line, rep, type, desc, url, b64) {
	if (!rep) { rep = 'bot'; }
	if (!type) { type = 'txt'; }
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:body, rep: rep, t:"mt", ts: Date.now(), type: type, st:'qed', lu: Date.now()};
	if (desc) { mt.desc = desc }
	if (url) { mt.url = url }
	if (b64) { mt.thumb = b64; }
	firebase.database().ref("/accounts/"+account+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "MT Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.getReservas = function(contact, line, kw) {
	if (kw) {
		getPropId(kw, function(prop) {
			if (prop) {
				//get future events for pid
				getFutureEvents(prop.id, function(evs) {
					if (evs) {
						var evHoy = ""
						var evMan = ""
						var evFut = ""
						var cHoy = 0;
						var cMan = 0;
						var cFut = 0;
						
						for (var i=0; i<evs.length; i++) {
							var ev = evs[i];
							if (ev.st != "del") {
								 var hoy = new Date();
								 hoy.setHours(11);
								 hoy.setMinutes(59);
								 var manana = new Date();
								 manana.setDate(hoy.getDate()+1);
								 manana.setHours(11);
								 manana.setMinutes(59);
								 var row = "<tr class='event-row'>";
								 //row += "<td>"+ev.ymd+"</td>";
								 row += "<td>"+formatHours(ev.timeStart)+" - "+formatHours(ev.timeEnd)+"</td>";
								 var cal =  _.findWhere(calendars, {id: ev.resource});
								 if (cal) {
									row += "<td>"+cal.caption+"</td>";
								 } else {
									row += "<td>"+ev.resource+"</td>"; 
								 }
								 row += "<td>"+ev.pax+"</td>";
								 if (ev.st == 'blk') {
									row += "<td>"+ev.notes+"</td>";
								 } else {
									row += "<td>"+ev.nick+"</td>";	 
								 }
								 //row += "<td>"+getEventStatusBadge(ev.st)+"</td>";
								 row += "</tr>";
								 if (ev.ymd == hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate()) {
									evHoy += row;
									cHoy += 1;
								 } else if (ev.ymd == manana.getFullYear()+"-"+(manana.getMonth()+1)+"-"+manana.getDate()) {
									evMan += row;
									cMan += 1;
								 } else if (ev.dateStart > manana.getTime()) {
									evFut += row;
									cFut += 1;
								 }
							}
						}
						
						if (cHoy>0) {
							var html = "<h5>Reporte Diario de Reservas</h5>"
							html += "Edificio: <strong>"+prop.name+"</strong><br>"
							html += "Fecha: <strong>"+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+"</strong><br><br>"
							html += "<table class='pdf-table'>"
							html += "<tr class='event-row-header'><td>HORA</td><td>ÁREA</td><td>PAX</td><td>CONTACTO</td></tr>"
							html += evHoy
							html += "</table>"
							//console.log (html)
							printPDF("Reporte Reservas Diarias", html, "reporte_reservas.pdf", contact, line, cHoy, hoy, prop.name);
						} else {
							pushText ("No existen reservas para hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear(), contact, line);
						}
					} else  {
						pushText ("No existen reservas", contact, line);
					}
				});	
			} else  {
				pushText ("Propiedad no encontrada "+prop, contact, line);
			}
		});
		
	} else {
		getUser(contact, function(user) {
			if (user) {
				var gs = user.group.split(",");
				//get future events for first group on array
				getFutureEvents(gs[0], function(evs) {
					if (evs) {
						var evHoy = ""
						var evMan = ""
						var evFut = ""
						var cHoy = 0;
						var cMan = 0;
						var cFut = 0;
						
						for (var i=0; i<evs.length; i++) {
							var ev = evs[i];
							if (ev.st != "del") {
								 var hoy = new Date();
								 hoy.setHours(11);
								 hoy.setMinutes(59);
								 var manana = new Date();
								 manana.setDate(hoy.getDate()+1);
								 manana.setHours(11);
								 manana.setMinutes(59);
								 var row = "<tr class='event-row'>";
								 //row += "<td>"+ev.ymd+"</td>";
								 row += "<td>"+formatHours(ev.timeStart)+" - "+formatHours(ev.timeEnd)+"</td>";
								 var cal =  _.findWhere(calendars, {id: ev.resource});
								 if (cal) {
									row += "<td>"+cal.caption+"</td>";
								 } else {
									row += "<td>"+ev.resource+"</td>"; 
								 }
								 row += "<td>"+ev.pax+"</td>";
								 if (ev.st == 'blk') {
									row += "<td>"+ev.notes+"</td>";
								 } else {
									row += "<td>"+ev.nick+"</td>";	 
								 }
								 //row += "<td>"+getEventStatusBadge(ev.st)+"</td>";
								 row += "</tr>";
								 if (ev.ymd == hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate()) {
									evHoy += row;
									cHoy += 1;
								 } else if (ev.ymd == manana.getFullYear()+"-"+(manana.getMonth()+1)+"-"+manana.getDate()) {
									evMan += row;
									cMan += 1;
								 } else if (ev.dateStart > manana.getTime()) {
									evFut += row;
									cFut += 1;
								 }
							}
						}
						
						if (cHoy>0) {
							var html = "<h5>Reporte Diario de Reservas</h5>"
							html += "Edificio: <strong>"+user.condo.name+"</strong><br>"
							html += "Fecha: <strong>"+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+"</strong><br><br>"
							html += "<table class='pdf-table'>"
							html += "<tr class='event-row-header'><td>HORA</td><td>ÁREA</td><td>PAX</td><td>CONTACTO</td></tr>"
							html += evHoy
							html += "</table>"
							//console.log (html)
							printPDF("Reporte Reservas Diarias", html, "reporte_reservas.pdf", contact, line, cHoy, hoy, user.condo.name);
						} else {
							pushText ("No existen reservas para hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear(), contact, line);
						}
					} else  {
						pushText ("No existen reservas", contact, line);
					}
				});
			} else {
				pushText ("Operación no autorizada", contact, line);
			}
		});
	}
}

global.getFutureEvents = function(gid, cb) {
	var d = new Date();
	d.setHours(0);
	d.setMinutes(0);
	var sAt = d.getTime();
	d.setDate(d.getDate()+3);
	var eAt = d.getTime();
	console.log ("Getting event for next 3 days", new Date(sAt), new Date(eAt), gid )
	firebase.database().ref('accounts/'+account+'/events/').orderByChild("dateStart")
	.startAt(sAt).endAt(eAt).once('value', function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			arr = _.sortBy(arr, function(ob, key) {
				return ob.dateStart;
			});
			console.log ("Found",arr.length,"total future events")
			var evs = _.where(arr, {group: gid});
			if (evs.length>0) {
				console.log ("Found",evs.length,"future events for group")
				cb(evs);
			} else {
				cb(null);
			}
		} else {
			cb(null);	
		}	
	});	
}

global.formatHours = function(n) {
	if (n<1000) {
		n+=""
		return n.substring(0,1) + "h" + n.substring(1,3);	
	} else {
		n+=""
		return n.substring(0,2) + "h" + n.substring(2,4);
	}
}

global.printPDF = function (title, body, fname, contact, line, cHoy, hoy, condoName) {
	var html = '<html><head><title>' + title + '</title>';
	html += '<meta http-equiv="content-type" content="text/html; charset=UTF-8" />';
	html += '<meta charset="utf-8" />';
	html += '<link rel="stylesheet" href="https://v3.textcenter.net/css/styles-pdf.css" type="text/css" />';
	html += "<style>@page {size: 21cm 29.7cm; margin: 5mm 5mm 5mm 5mm;}</style>";
	html += '</head><body>';
	html += body;
	html += '</body></html>';
	//console.log (html)
	var data = {
		html: html,
		fname: fname,
		orientation: "portrait"
	};
	
	var args = {
		'form': data,
		'headers': { 'Content-Type': 'application/json'},
		'url':'https://pedefer.com:7172/make-pdf-html'
		
	}
	request.post(args, function (err, response) {
		if (!err) {
			var ob = JSON.parse(response.body)
			
			if (ob.result == "success") {
				console.log ("PDF created OK")
				var segs = ob.msg.split("/");
				console.log ("Link de reservas", "https://cdn.pedefer.com"+"/"+segs[segs.length-1]);
				var msg = "Hay "+cHoy+" reserva(s) para  hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+" en "+condoName+"\n\nDescargar PDF: https://cdn.pedefer.com"+"/"+segs[segs.length-1]
				pushText (msg, contact, line);	
			} else {
				pushText ("Hay "+cHoy+" reserva(s) para hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+" en "+condoName+"\n\nError generando PDF", contact, line);	
				console.log ("Error Pedefer")
				console.log (ob)	
			}
		} else {
			console.log ("Error Pedefer", err)
			pushText ("Hay "+cHoy+" reserva(s) para hoy "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+hoy.getFullYear()+" en "+user.condo.name+"\n\nError generando PDF", contact, line);	
		}
	});
}

global.getEventStatusBadge = function (st) {
		var stName = st;
		 switch (st) {
			case "new":
				stName = "Por confirmar";
				break;
			case "acc":
				stName = "Confirmado";
				break;
			case "rec":
				stName = "Rechazado";
				break;
			case "del":
				stName = "Eliminado";
				break;
			case "fin":
				stName = "Finalizado";
			case "blk":
				stName = "Bloqueado";
				break;
		 }
		return '<span style="color: '+getEventStatusColor(st)+';">'+stName+'</span>';
}

global.getEventStatusColor = function(st) {
	 var color = "";
	 switch (st) {
		case "new":
			color = "#415663";
			break;
		case "acc":
			color = "#28a745";
			break;
		case "rec":
			color = "#dc3545";
			break;
		case "del":
			color = "#dc3545";
			break;
		case "fin":
			color = "#6dad7c";
			break;
		case "blk":
			color = "#28a745";
			break;
	 }
	return color;
}
  
global.setUserParam = function(key, val, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).set(val);
}

global.updateUser = function(ob, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/").update( ob );
}

function startSession (contact, user) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/tsso").set( Date.now() );
	logStartSession(contact, user.chn);
}

global.logStartSession = function(contact, chn) {
	var log = {
		type: "session-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logEndSession = function(contact, chn, tsso, op) {
	var log = {
		type: "session-end",
		tssx: Date.now(),
		ref: contact+"",
		ts: Date.now(),
		op: op
	}
	if (tsso) { log.tsso = tsso; }
	if (chn) { log.chn = chn; }
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
}

global.logStartTicket = function(contact, chn, user, group) {
	var log = {
		type: "ticket-start",
		ts: Date.now(),
		ref: contact+"",
		chn: chn,
		group: group
	}
	firebase.database().ref("/accounts/"+account+"/logs").push(log);
	//send notification to agents
	if (user.condos && group == "admin") {
		var condo = _.findWhere(user.condos, {id: user.group});
		if (condo) {
			if (condo.config) {
				if (condo.config.notif) {
					var sendTos = condo.config.notif.split(",");
					for (var i=0; i<sendTos.length; i++) {
						var to = sendTos[i].trim();
						if (to.length == 10 || to.length == 12) {
							if (to.length==10) { to = to.replace("09","5939") }
							pushTemplate(["https://v3.textcenter.net/#inbox?c="+contact], "new_ticket_created", to, user.line);	
						}
					}
				}
			}
		}
	} else {
		console.log (group)
		console.log (config.notifyTo[group])
		if ( config.notifyTo[group] ) {
			var sendTos = config.notifyTo[group].split(",");	
			for (var i=0; i<sendTos.length; i++) {
				var to = sendTos[i].trim();
				if (to.length == 10 || to.length == 12) {
					if (to.length==10) { to = to.replace("09","5939") }
					pushTemplate(["https://v3.textcenter.net/#inbox?c="+contact], "new_ticket_created", to, user.line);
				}
			}
		}
	}
}

global.pushTemplate = function  (params, templateName, contact, line) {
	
	var msgId = Date.now()+"-"+line+"-"+contact;
	var mt = { g:0, m:"Nuevo ticket en Textcenter", rep: "bot", t:"mt", ts: Date.now(), type: "txt", st:"qed", lu: Date.now()};
	mt.isTemplate = true	
	mt.lang = "es"; //todo read from DB
	mt.params = params.join("|");
	mt.templateName = templateName;
	firebase.database().ref("/accounts/"+account+"/conves/"+contact+"/msgs/"+msgId).set(mt).then(function() {
		var ob = _.clone (mt);
		ob.line = line;
		ob.to = contact;
		ob.account = account;
		//push to account MT queue
		firebase.database().ref("/accounts/"+account+"/mtq/"+msgId).set(ob).then(function() {
		   console.log(new Date(), "Template Response POSTED OK");
		}).catch(function(error) {
		   console.log(new Date(), "Push MTQ Error", error);
		});
	}).catch(function(error) {
	   console.log(new Date(), "Push MTQ Error", error);
	});

}

global.resetUserParam = function(key, contact) {
	firebase.database().ref("/accounts/"+account+"/chats/"+contact+"/"+key).remove();
}

global.saveForm = function(ob, cb) {
	firebase.database().ref("/accounts/"+account+"/forms").push(ob).then(function(res){
		cb(res.key);
	});
}

global.salir = function(job, user) {
	var self = this;
	logEndSession(job.contact, user.chn, user.tsso, "s");	
	var str = "Gracias por usar este servicio.\nAdios!"
	pushText (str, job.contact, user.line);
	resetUserParam('step', job.contact);
	resetUserParam('lastStep', job.contact);
	resetUserParam('pin', job.contact);
	resetUserParam('condos', job.contact);
	resetUserParam('units', job.contact);
	resetUserParam('calopts', job.contact);
	resetUserParam('activeRes', job.contact);
	resetUserParam('group', job.contact);
	resetUserParam('account', job.contact);
	resetUserParam('tsso', job.contact);
	setUserParam('status', 'reset', job.contact);
	
}

global.resetBot = function(contact, user, msg) {
	logEndSession(contact, user.chn, user.tsso, "r");
	
	if (msg) { pushText (msg, contact, user.line); }
	resetUserParam('step', contact);
	resetUserParam('lastStep', contact);
	resetUserParam('pin', contact);
	resetUserParam('condos', contact);
	resetUserParam('units', contact);
	resetUserParam('calopts', contact);
	resetUserParam('activeRes', contact);
	resetUserParam('group', contact);
	resetUserParam('account', contact);
	resetUserParam('tsso', contact);
	setUserParam('status', 'reset', contact);
}

global.decr = function (str) {
	var decrypted = CryptoJS.AES.decrypt(str, env.hasherPwd);
	return decrypted.toString(CryptoJS.enc.Utf8);	
}

global.encr = function (str) {
	return CryptoJS.AES.encrypt(str, env.hasherPwd).toString()	
}

global.getPropId = function(kw, cb) {
	console.log ("Searching for prop", kw);
	firebase.database().ref("/accounts/"+account+"/props").orderByChild("name").equalTo(kw+'').once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			console.log ("Prop found", arr.length);
			cb (arr[0])
		} else {
			cb (null);
		}
	}).catch(function(error) {
		console.log (error)
		cb (null);
	});
}

global.getContact = function(phone, cb) {
	console.log ("Searching for",phone);
	firebase.database().ref("/accounts/"+account+"/contacts").orderByChild("phone").equalTo(phone+'').once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			console.log ("Contact found", arr.length);
			cb (arr[0])
		} else {
			cb (null);
		}
	}).catch(function(error) {
		console.log (error)
		cb (null);
	});
}

global.getUser = function(phone, cb) {
	console.log ("Searching user",phone);
	firebase.database().ref("users").orderByChild("phone").equalTo(phone+"").once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			console.log ("User found", arr.length);
			var ob = arr[0];
			if (ob.group) {
				firebase.database().ref("/accounts/"+account+"/props/"+ob.group).once("value", function(res) {
					if (res.val()) {
						ob.condo = res.val();
						cb (ob)
					} else {
						console.log ("User found but prop not found");
						cb (null);	
					}
				});
			} else {
				console.log ("User found but has no groups");
				cb (null);	
			}
		} else {
			console.log ("User not found");
			cb (null);
		}
	}).catch(function(error) {
		console.log ("User not found. Error:");
		console.log (error)
		cb (null);
	});
}

global.getEventListPerContact = function (user, cb) {
	console.log ("Searching events ",user.account);
	firebase.database().ref("/accounts/"+account+"/events").orderByChild("account").equalTo(user.account).once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			var activas = [];
			for (var i=0; i<arr.length; i++) {
				if ((arr[i].st == "acc" || arr[i].st == "new") && arr[i].dateStart >= (Date.now()-86400000)) {
					activas.push(arr[i])	
				} 
			}
			console.log ("Events found", activas.length);
			cb (activas);
		} else {
			cb ([]);
		}
	}).catch(function(error) {
		console.log (error)
		cb ([]);
	});
}

global.updateEvent = function (evId, ob, cb) {
	firebase.database().ref('accounts/'+account+'/events/'+evId).update(ob).then(function() {
		console.log (new Date(), "Event updated OK", evId);
		if (cb) { cb(true); }
	}).catch(function(error) {
		console.log (new Date(), "Update event error");
		console.log (error)
		if (cb) { cb(false); }
	}); 
}

global.loadCondos = function(arr, condos, cb) {
	if (arr.length>0) {
		firebase.database().ref("/accounts/"+account+"/props/"+arr.shift()).once("value", function(res) {
			if (res.val()) {
				var ob = res.val();
				ob.id = res.key;
				condos.push(ob);
			}
			loadCondos(arr, condos, cb);
		}).catch(function(error) {
			console.log (error)
			loadCondos(arr, condos, cb);
		});
	} else {
		cb (condos);	
	}
}

global.getBalance = function(accId, group, cb) {
	console.log ("Checking balance for", accId, group);
	firebase.database().ref("/accounts/"+account+"/trans").orderByChild("account").equalTo(accId).once("value", function(res) {
		if (res.val()) {
			var b = 0;
			var arr = _.map(res.val(), function(ob, key) {
				ob.id = key;
				return ob;
			});
			arr = _.where(arr, {group: group})
			for (var i=0; i<arr.length; i++) {
				if (arr[i].type=="deb") { b -= arr[i].value; } 
				if (arr[i].type=="cre") { b += arr[i].value; }
			}
			cb(b);
		} else {
			cb(0);	
		}	
	});
}

global.getContactUnitsInGroup = function(contactId, group ,cb) {
	firebase.database().ref("/accounts/"+account+"/roles").orderByChild("contact").equalTo(contactId).once("value", function(res) {
		if (res.val()) {
			var arr = _.map(res.val(), function(ob, key) { return ob });
			var roles = _.where(arr, {group: group});
			var units = []
			if (roles.length>0) {					
				for ( var i=0; i<roles.length; i++) {
					units.push( roles[i].unit + "@" + roles[i].torre );
				}
			}
			cb (units);
		} else {
			cb ([]);
		}
	});	
}
global.buildCalOptions = function (user, balance, cals, welcomeMessage, cb) {
	console.log ("Build calendar options for user", user.id);
	var condo = _.findWhere(user.condos, {id: user.group});
	if (condo) {
		if (!condo.config) { condo.config = {} }
		if (cals.length>0) {
			firebase.database().ref("/accounts/"+account+"/roles").orderByChild("contact").equalTo(user.account).once("value", function(res) {
				//get torres where this contact has roles
				var torres = ["all"]
				var uroles = []
				if (res.val()) {
					var arr = _.map(res.val(), function(ob, key) { return ob });
					var roles = _.where(arr, {group: user.group});
					if (roles.length>0) {					
						for ( var i=0; i<roles.length; i++) {
							torres.push(roles[i].torre)
							uroles.push(roles[i].unit+"@"+roles[i].torre)
						}
					}
				}
				console.log ("user roles", uroles);
				console.log ("user torres", torres);
				
				//~ //get active reservations for this user or units
				if (user.unit) {
					var dbRef = firebase.database().ref('accounts/'+account+'/events/').orderByChild("unit").equalTo(user.unit);
				} else { 
					var dbRef = firebase.database().ref('accounts/'+account+'/events/').orderByChild("account").equalTo(user.account);
				}
				dbRef.once('value', function (evs) {
					var activeRes = []
					if (evs.val()) {
						var arr = _.map(evs.val(), function(ob, key) { return ob });
					} else {
						var arr = []	
					}
					//get tomorrow ts
					var d = new Date();
					d.setHours(1);
					d.setMinutes(1);
					d.setDate(d.getDate()+1);
					var ts = d.getTime();
					for (var i=0; i<arr.length; i++) {
						if (arr[i].st == "acc" && arr[i].dateStart>=ts) {
							activeRes.push(arr[i].resource)
						}	
					}
					console.log ("Active events after",d)
					console.log (activeRes)
					
					//build cal options
					var calopts = []
					for ( var i=0; i<cals.length; i++) {
						if (balance<0 && cals[i].mora == "no") {
							//msg += "_Lo sentimos, a la fecha tienes un saldo pendiente por pagar de *$"+((balance*-1).toFixed(2))+"* y no puedes realizar la reserva de "+cals[i].caption+"_\n\n";
						} else {
							//~ console.log ("torres", cals[i].torre, torres)
							var torresMatch = false;
							for (var j=0; j<torres.length; j++) {
								var tarr = cals[i].torre.split(",")
								if (tarr.indexOf(torres[j])>=0) {
									torresMatch = true;
									break;
								}	
							}
							
							if (torresMatch) {
								//check if user has res
								if (activeRes.indexOf(cals[i].id)<0) {
									//first reservation for this cal
									calopts.push(cals[i]);
								} else {
									var count = _.countBy(activeRes);
									var max = 1;
									if (["2","3","4","5"].indexOf(cals[i].auto)>=0) {max=Number(cals[i].auto)}
									console.log ("max res for this cal", max, "current count", count[cals[i].id])
									if (count[cals[i].id]<max) {
										//can have another res	
										calopts.push(cals[i]);
									}
									
								}
							}
						}
					}
					
					//build message
					if (calopts.length>0) {
						if (condo.config.normas) {
							var msg = condo.config.normas + "\n\nDigita una opción hacer tu reserva:\n\n";
						} else {
							var msg = "Digita una opción hacer tu reserva:\n\n";	
						}
						for ( var i=0; i<calopts.length; i++) {
							msg += (i+1)+'. '+calopts[i].caption+'\n';
						}
						cb(calopts,msg)
					} else {
						var msg = "Al momento tienes reservas activas en tu cuenta y no puedes realizar nuevas reservas.\n";
						msg += welcomeMessage;
						cb([],msg)
					}
				}); //get events
			}); //get contact
		} else {
			var msg = "Lo sentimos, las reservas por este canal no han sido habilitadas\n";
			msg += welcomeMessage;
			cb([],msg)
		}
	} else {
		var msg = "Lo sentimos, las reservas por este canal no han sido habilitadas\n";
		msg += welcomeMessage;
		cb([],msg)
	}
}

global.createTask = function(user, task, cb) {
	var ref = "all"
	if (task.group) {
		var ref = "g/"+task.group;
		var isGroup = true;
		var group = task.group;
		delete task.group;
	}
	if (task.user) {
		var ref = "u/"+task.user;
		var isGroup = false;
		var contactId = task.user;
		delete task.user;
	}
	firebase.database().ref("/accounts/"+account+"/tasks/"+ref).push(task).then(function(res) {
		task.id = res.key;
		if (isGroup) {
			console.log ("Crear tarea group", group)
			if (["comercial", "corretaje"].indexOf(group)>=0) {
				console.log ("send to", config.notifyTo[group])
				if ( config.notifyTo[group] ) {
					var sendTos = config.notifyTo[group].split(",");	
					for (var i=0; i<sendTos.length; i++) {
						var to = sendTos[i].trim();
						if (to.length == 10 || to.length == 12) {
							if (to.length==10) { to = to.replace("09","5939") }
							pushTemplate([" ","https://v3.textcenter.net/#tasks?g="+group], "new_task_update", to, user.line);
						}
					}
				}
			} else {
				//send notification to group notif array
				if (user.condos) {
					var condo = _.findWhere(user.condos, {id: user.group});
					if (condo) {
						if (condo.config) {
							if (condo.config.notif) {
								var sendTos = condo.config.notif.split(",");
								for (var i=0; i<sendTos.length; i++) {
									if (sendTos[i].length == 10 || sendTos[i].length == 12 ) {
										var to = sendTos[i];
										if (to.length==10) { to = to.replace("09","5939") }
										console.log ("notificar tarea a",to);
										pushTemplate([" ","http://v3.textcenter.net/#tasks"], "new_task_update", to, user.line);	
									}
								}
							}
						}
						
					}
				} else {
					//find condo with task.group
					firebase.database().ref("/accounts/"+account+"/props/"+group).once("value", function(res) {
						if (res.val()) {
							var condo = res.val();
							if (condo.config) {
								if (condo.config.notif) {
									var sendTos = condo.config.notif.split(",");
									for (var i=0; i<sendTos.length; i++) {
										if (sendTos[i].length == 10 || sendTos[i].length == 12 ) {
											var to = sendTos[i];
											if (to.length==10) { to = to.replace("09","5939") }
											pushTemplate([" ","http://v3.textcenter.net/#tasks"], "new_task_update", to, user.line);	
										}
									}
								}
							}
						}
					}).catch(function(error) {
						console.log (error)
						//cannot notify
					});
				}
			} //task is group condo
		} else {
			//send notif to user	
		}
		if (cb) { cb(task); }
	}).catch(function(error){
		console.log (error)
		console.log (task);
		if (cb) { cb(null); }
	});
}

global.isShopOpen = function() {
	var d = new Date();
	var t = (d.getHours() * 100) + d.getMinutes();
	if (t>=config.officeOpen && t<config.officeClose) {
		return true;
	} else {
		return false;		
	}	
}

global.makeShortURL = function(authStr, cb) {
	firebase.database().ref('/shorts/').push(authStr).then(function(res) {
		console.log ("Short Pushed", res.key)
		cb(res.key)
	})
	.catch(function(error) {
		cb("err")
	});
}
