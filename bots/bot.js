global.fetch = require('node-fetch');
global._ = require('underscore')
global.tools = require('./tools')
global.fs = require('fs')
global.path = require('path')
global.envBot = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
global.env = JSON.parse(fs.readFileSync(path.resolve(__dirname, './env.json'), 'utf8'));
global.Jimp = require("jimp");
global.extractFrames = require('ffmpeg-extract-frames');
global.extractFrame = require('ffmpeg-extract-frame');
global.aws = require('../aws');
global.igorApi = require('./igorApi')
global.utapi = require('./uanatacaAPI');
global.nxtapi = require('./nexxitAPI');
global.ftapi = require('./faceTecAPI');
global.payphone = require('./payphone');
global.wfapi = require('./wfapi');

//establecer Zona horaria
process.env.TZ = 'America/Guayaquil';

//load bot config
if (process.argv[2]) {
  global.botConfig = JSON.parse(fs.readFileSync(path.resolve(__dirname, process.argv[2]), 'utf8'));
  //load modules
  global.account = botConfig.account;
  if (botConfig.modules) {
    global.mods = {}
    for (var i = 0; i < botConfig.modules.length; i++) {
      mods[botConfig.modules[i].name] = require(botConfig.modules[i].path)
      console.log("module loaded", botConfig.modules[i].name, botConfig.modules[i].path)
    }
  } else {
    console.log("Falta lista de modulos")
    process.exit(0);
  }
} else {
  console.log("Falta argumento con ruta del archivo de configuración")
  process.exit(0);
}

global.firebase = require('firebase')
global.aws = require('../aws');

var booted = false
var jobs = []

var qitv = 50 //todo: puede ser argumento al iniciar o puede ser variable de config
global.requeueInt = 1000 //todo: puede ser argumento al iniciar o puede ser variable de config
global.config = {}


//firebase init
const firebaseConfig = {
  apiKey: 'AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc',
  authDomain: 'ubiku3.firebaseapp.com',
  databaseURL: 'https://ubiku3.firebaseio.com',
  projectId: 'ubiku3',
  storageBucket: 'ubiku3.appspot.com',
  messagingSenderId: '946813430025',
  appId: '1:946813430025:web:4a3324c8d359bd444d4bd8',
  measurementId: 'G-L8H2NKX2EX'
}

firebase.initializeApp(firebaseConfig)

firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    console.log('Logged in to firebase as uid ' + user.uid)
    setTimeout(function () {
      bootApp()
    }, 2000)
  } else {
    // User is signed out login again
    console.log('Session expired')
    loginTofirebase()
  }
  // ...
})

function loginTofirebase() {
  console.log('Login to firebase')
  firebase
    .auth()
    .signInWithEmailAndPassword(envBot['fbuser'], envBot['fbpwd'])
    .catch(function (error) {
      var errorCode = error.code
      var errorMessage = error.message
      console.log(error.code + ': ' + error.message)
    })
}

//listen to new jobs
function bootApp() {
  if (!booted) {
    booted = true
    //load account config
    initAWS(function () {
      loadAccountConfig()
      setTimeout(function () {
        listenToJobs()
        processJobQueue()
      }, 1000)
    });
  }
}

function initAWS(cb) {
  firebase.database().ref('/accounts/' + account + '/config/creds/aws').once('value', function (creds) {
    if (creds.val()) {
      aws.init(creds.val().accessKeyId, creds.val().secretAccessKey);
      cb();
    } else {
      console.log("AWS credentials not found");
    }
  });
}

function loadAccountConfig() {
  var self = this
  firebase
    .database()
    .ref('accounts/' + account + '/config/misc')
    .on('value', function (snapshot) {
      config = snapshot.val()
      console.log('Config fetched for account', account)
    })
  firebase
    .database()
    .ref('accounts/' + account + '/config/calendars')
    .on('value', function (snapshot) {
      calendars = _.map(snapshot.val(), function (ob, key) {
        ob.id = key
        return ob
      })
      console.log('Calendars fetched for account', account)
    })
}

function listenToJobs() {
  firebase
    .database()
    .ref('/accounts/' + account + '/botJobs/')
    .orderByChild('st')
    .equalTo('new')
    .on('child_added', function (res) {
      var job = res.val()
      job.id = res.key
      var line = "none"
      if (job.msg) { if (job.msg.line) { line = job.msg.line}}
      if (job.line) { line = job.line }
      if (botConfig.lines) {
        if (botConfig.lines.indexOf(line) >= 0  || botConfig.types.indexOf(job.type) >= 0 ) {
          jobs.push(job)
          console.log(new Date(), 'New job queued', job.id, job.type)
		}
      } else {
        jobs.push(job)
        console.log(new Date(), 'New job queued', job.id, job.type)
      }
    })
}

function processJobQueue() {
  if (jobs.length > 0) {
    var job = jobs.shift();
    if (job.runAfter) {
      if (job.runAfter > Date.now()) {

        jobs.push(job)
        setTimeout(function () {
          processJobQueue();
        }, qitv);
      } else {
        processJob(job)
      }
    } else {
      processJob(job)
    }
  } else {
    setTimeout(function () {
      processJobQueue();
    }, qitv);
  }
}

//* Inicio de processJob
function processJob(job) {
  firebase.database().ref('/accounts/' + account + '/chats/' + job.contact).once('value', function (snapshot) {
    var user = snapshot.val()
    var step = '0';
    if (user.step) {
      step = user.step
    }

    var group = 'main'
    if (user.group) {
      group = user.group
    }

    var module = 'main'
    if (user.module) {
      module = user.module
    }


    //* Nota JAGM expirar sesion si es mayor a 24 hrs
    if ((Date.now() - user.tsso) / (1000 * 60 * 60) > 24) {
      resetBot(job.contact, user)
      requeueJob(job, Date.now())
    } else {
      if (job.type == 'process-mo') {

        if (!user.tsso) { startSession(job.contact, user) }


        if (step != 'idle') {
          console.log("User is in module", module, "step", step)
          if (mods[module] == undefined) {
            module = "main"
            step = "0"
            console.log("User module changed", module, "step", step)
          }
          var msgType = job.msg.type
          if (msgType == '') {
            msgType = 'text'
          }
          console.log('Process MO ', msgType)
          switch (msgType) {
            case 'text':
              if (job.msg.m) {
                var body = job.msg.m
              } else {
                var body = job.msg.text.body
              }
              console.log('Body', body)

              if (body.length > 0) {
                //trim spaces
                body = body.trim()

                //catch operations
                if (body.toLowerCase().indexOf('#') == 0) {
                  var parts = body.toLowerCase().split(' ')
                  //match direct operation
                  switch (parts[0]) {
                    case '#salir':
                      //salir(job, user)
                      break
                    case '#ping':
                      var str = '#pong ' + new Date()
                      pushText(str, job.contact, user.line)
                      break
                    case '#techrevo':
                      updateUser({ step: 'techrevo-1', module: 'mainFirmas' }, job.contact)
                      pushText("Bienvenido a Uanataca!\nPor ser parte de la Revolución Tecnológica, el día de hoy te *regalamos* tu firma electrónica.\nIngresa el código promocional *TECHREVO* al momento del pago y tu firma es gratis.\n\nQuieres obtener tu firma electrónica en minutos?", job.contact, user.line, null, "btn", [{ id: "aceptar", title: "Si quiero!" }, { id: "rechazar", title: "No gracias" }])
                      break
                    case '#stop':
                      if(body.toLowerCase().indexOf(config.exitprocess) >= 0) {
                        console.log('STOP PROCESS')
                        process.exit(0)
                      }
                      break
                  } //end switch parts operation
                } else {
                  if (
                    body.toLowerCase() == 'salir' ||
                    body.toLowerCase() == '*salir*'
                  ) {
                    if (!user.sso) {
                      salir(job, user)
                    } else {
                      //*Primero preguntar si esta seguro de cancelar el proceso
                      updateUser({ step: "0", module: 'cancel' }, job.contact);
                      loadFlow(user, function (flow) { mods['cancel'].bot('Move to cancel', job, user, "0", null, flow) })
                    }
                  } else if (body.indexOf('Bot disabled') >= 0) {
                    console.log(new Date(), 'Ignore')
                  } else {
                    loadFlow(user, function (flow) { mods[module].bot(body.toLowerCase(), job, user, step, group, flow) })
                  }
                } //end if operation
              } //body is empty
              //job is completed
              finishJob(job)
              break;

            case 'interactive':
              console.log(job.msg.interactive)
              //*Verificar si son botones de WhatsApp
              if (job.msg.interactive.button_reply) {
                loadFlow(user, function (flow) { mods[module].bot(job.msg.interactive.button_reply.id, job, user, step, group, flow) })
              }
              //*Verificar si son opciones de WhatsApp
              if (job.msg.interactive.list_reply) {
                loadFlow(user, function (flow) { mods[module].bot(job.msg.interactive.list_reply.id, job, user, step, group, flow) })
              }
              finishJob(job);
              break;

            case 'button':
              loadFlow(user, function (flow) { mods[module].bot(job.msg.button.payload, job, user, step, group, flow) })
              //job is completed
              finishJob(job)
              break

            default:
              loadFlow(user, function (flow) { mods[module].bot('[' + job.msg.type + ']', job, user, step, group, flow) })
              //job is completed
              finishJob(job)
              break
          } //end switch mo type
        } else {
          //end idle check
          console.log('user ' + job.contact + '@' + user.line + ' is idle')
          //is a reset operation?
          var body = job.msg.body
          if (body) {
            if (body.length > 0) {
              //trim spaces
              body = body.trim()
              if (body.toLowerCase().indexOf('#') == 0) {
                if (body.toLowerCase() == '#reset') {
                  resetBot(job.contact, user, 'Reset Session OK')
                }
              }
            }
          }
          //job is completed
          finishJob(job)
        }


      } else { //el job no es un process-mo
        //que hago con el job type???	
        loadFlow(user, function (flow) {
          mods[module].job(job, user, step, group, flow, function (resp) {
            if (resp.st == "done") {
              //job is completed
              finishJob(job)
            } else {
              requeueJob(job, resp.ts)
            }
          })
        })
      }
    }

  }) //carga user
}
//*Fin de processJob

global.loadFlow = function (user, cb) {
  if (user.sso) {
    wfapi.getSSO(user.sso, function (ssoOb) {
      if (ssoOb) {
        wfapi.getFlow(ssoOb.sessionId, ssoOb.apiKey, function (flowResponse) {
          if (flowResponse) {
            //Todo: if flow is st new o in_process continuo
            //else le mando al inicio del bot borrando el sso, etc
            var flowOb = flowResponse.details
            flowOb.sessionId = ssoOb.sessionId
            flowOb.apiKey = ssoOb.apiKey
            // console.log('FLOW DENTRO DE LOADFLOW', flowOb)
            // console.log('STEPS', flowOb.type.steps)
            // console.log('STEP 1 FIELDS', flowOb.type.steps.os8_001.custom.fields)
            // console.log('STEP 2 FIELDS', flowOb.type.steps.os8_002.custom.fields)
            // console.log('STEP 3 FIELDS', flowOb.type.steps.os8_003.custom.fields)
            // console.log('STEP 4 FIELDS', flowOb.type.steps.os8_004.custom.fields)
            // console.log('STEP 5 FIELDS', flowOb.type.steps.os8_005.custom.fields)
            // console.log('STEP 6 FIELDS', flowOb.type.steps.os8_006.custom.fields)
            // console.log('STEP 7 FIELDS', flowOb.type.steps.os8_007.custom.fields)
            // console.log('STEP 8 FIELDS', flowOb.type.steps.os8_008.custom.fields)
            // console.log('STEP 9 FIELDS', flowOb.type.steps.os8_009.custom.fields)
            cb(flowOb)
          } else {
            console.log("Failed to load flow")
            cb({})
          }
        })
      } else {
        console.log("Failed to load sso")
        cb({})
      }
    })
  } else {
    cb({})
  }
}
global.finishJob = function (job) {
  //update job to done
  firebase.database().ref('/accounts/' + account + '/botJobs/' + job.id).update({ st: 'done', tsu: Date.now() })
  setTimeout(function () {
    processJobQueue()
  }, qitv)
}

global.requeueJob = function (job, ts) {
  firebase.database().ref('/accounts/' + account + '/botJobs/' + job.id + '/runAfter').set(ts)
  job.runAfter = ts
  jobs.push(job)
  setTimeout(function () {
    processJobQueue();
  }, qitv);
}



//* Inicio pushText
global.pushText = function (body, contact, line, rep, type, desc, url, b64, cb) {
  if (!rep) {
    rep = 'bot'
  }
  if (!type) {
    type = 'txt'
  }
  var msgId = Date.now() + '-' + line + '-' + contact
  var mt = {
    g: 0,
    m: body,
    rep: rep,
    t: 'mt',
    ts: Date.now(),
    type: type,
    st: 'qed',
    lu: Date.now()
  }
  if (desc) {
    mt.desc = desc
  }
  if (url) {
    mt.url = url
  }
  if (b64) {
    mt.thumb = b64
  }
  firebase
    .database()
    .ref('/accounts/' + account + '/conves/' + contact + '/msgs/' + msgId)
    .set(mt)
    .then(function () {
      if (cb) {
        cb()
      }
      var ob = _.clone(mt)
      ob.line = line
      ob.to = contact
      ob.account = account
      //push to account MT queue
      firebase
        .database()
        .ref('/accounts/' + account + '/mtq/' + msgId)
        .set(ob)
        .then(function () {
          console.log(new Date(), 'MT Response POSTED OK')
        })
        .catch(function (error) {
          console.log(new Date(), 'Push MTQ Error', error)
        })
    })
    .catch(function (error) {
      console.log(new Date(), 'Push MTQ Error', error)
    })
}
// * Fin PushText

global.pushTemplate = function (params, templateName, contact, line) {
  var msgId = Date.now() + '-' + line + '-' + contact
  var mt = {
    g: 0,
    m: 'Nuevo ticket en Textcenter',
    rep: 'bot',
    t: 'mt',
    ts: Date.now(),
    type: 'txt',
    st: 'qed',
    lu: Date.now()
  }
  mt.isTemplate = true
  mt.lang = 'es' //todo read from DB
  mt.params = params.join('|')
  mt.templateName = templateName
  firebase
    .database()
    .ref('/accounts/' + account + '/conves/' + contact + '/msgs/' + msgId)
    .set(mt)
    .then(function () {
      var ob = _.clone(mt)
      ob.line = line
      ob.to = contact
      ob.account = account
      //push to account MT queue
      firebase
        .database()
        .ref('/accounts/' + account + '/mtq/' + msgId)
        .set(ob)
        .then(function () {
          console.log(new Date(), 'Template Response POSTED OK')
        })
        .catch(function (error) {
          console.log(new Date(), 'Push MTQ Error', error)
        })
    })
    .catch(function (error) {
      console.log(new Date(), 'Push MTQ Error', error)
    })
}

global.setUserParam = function (key, val, contact) {
  firebase
    .database()
    .ref('/accounts/' + account + '/chats/' + contact + '/' + key)
    .set(val)
}

global.updateUser = function (ob, contact, cb) {
  firebase
    .database()
    .ref('/accounts/' + account + '/chats/' + contact + '/')
    .update(ob, function (resp, err) {
      console.log(resp)
      console.log(err)
      if (cb) { cb(true) }
    })
}

function startSession(contact, user) {
  firebase
    .database()
    .ref('/accounts/' + account + '/chats/' + contact + '/tsso')
    .set(Date.now())
  logStartSession(contact, user.chn)
}

global.logStartSession = function (contact, chn) {
  var log = {
    type: 'session-start',
    ts: Date.now(),
    ref: contact + '',
    chn: chn
  }
  firebase
    .database()
    .ref('/accounts/' + account + '/logs')
    .push(log)
}

global.logEndSession = function (contact, chn, tsso, op) {
  var log = {
    type: 'session-end',
    tssx: Date.now(),
    ref: contact + '',
    ts: Date.now(),
    op: op
  }
  if (tsso) {
    log.tsso = tsso
  }
  if (chn) {
    log.chn = chn
  }
  firebase
    .database()
    .ref('/accounts/' + account + '/logs')
    .push(log)
}

global.logStartTicket = function (contact, chn, user, group) {
  var log = {
    type: 'ticket-start',
    ts: Date.now(),
    ref: contact + '',
    chn: chn,
    group: group
  }
  firebase
    .database()
    .ref('/accounts/' + account + '/logs')
    .push(log)
  //notificar
  if (config.notifyTickets) {
    if (config.notifyTickets[group]) {
      var arr = config.notifyTickets[group].split(',')
      for (var i = 0; i < arr.length; i++) {
        pushTemplate(
          ['https://v3.textcenter.net/#inbox?c=' + contact],
          'new_ticket_created',
          arr[i],
          user.line
        )
      }
    } else if (config.notifyTickets.main) {
      var arr = config.notifyTickets.main.split(',')
      for (var i = 0; i < arr.length; i++) {
        pushTemplate(
          ['https://v3.textcenter.net/#inbox?c=' + contact],
          'new_ticket_created',
          arr[i],
          user.line
        )
      }
    } else {
      var arr = config.notifyTickets.split(',')
      for (var i = 0; i < arr.length; i++) {
        pushTemplate(
          ['https://v3.textcenter.net/#inbox?c=' + contact],
          'new_ticket_created',
          arr[i],
          user.line
        )
      }
    }
  }
}

global.logStartTicket2 = function (contact, chn, user, group) {
  var log = {
    type: "ticket-start",
    ts: Date.now(),
    ref: contact + "",
    chn: chn,
    group: group
  }
  firebase.database().ref("/accounts/" + account + "/logs").push(log);
  //notificar
  if (config.notifyTickets) {
    if (config.notifyTickets[group]) {
      var arr = config.notifyTickets[group].split(",");
      for (var i = 0; i < arr.length; i++) {
        pushTemplate(["https://v3.textcenter.net/#inbox?c=" + contact], "new_ticket_created", arr[i], user.line);
      }
    } else if (config.notifyTickets.main) {
      var arr = config.notifyTickets.main.split(",");
      for (var i = 0; i < arr.length; i++) {
        pushTemplate(["https://v3.textcenter.net/#inbox?c=" + contact], "new_ticket_created", arr[i], user.line);
      }
    }

  }

}

global.resetUserParam = function (key, contact) {
  firebase
    .database()
    .ref('/accounts/' + account + '/chats/' + contact + '/' + key)
    .remove()
}


global.salir = function (job, user) {
  logEndSession(job.contact, user.chn, user.tsso, 's')
  var str = 'Gracias por usar este servicio. Adios!' //todo: deberia venir de account config
  pushText(str, job.contact, user.line)
  resetUserParam('ced', job.contact)
  resetUserParam('documentos', job.contact)
  resetUserParam('step', job.contact)
  resetUserParam('pin', job.contact)
  resetUserParam('group', job.contact)
  resetUserParam('account', job.contact)
  resetUserParam('tsso', job.contact)
  resetUserParam('documentos', job.contact)
  resetUserParam('module', job.contact)
  resetUserParam('videoFile', job.contact)
  actualizarProceso(user.currentProcess, { st: 'deleted', tsu: Date.now() });
  resetUserParam('currentProcess', job.contact)
  resetUserParam('sso', job.contact)
  setUserParam('status', 'deleted', job.contact)
}

global.salirQuiet = function (job, user) {
  logEndSession(job.contact, user.chn, user.tsso, 's')
  resetUserParam('ced', job.contact)
  resetUserParam('documentos', job.contact)
  resetUserParam('step', job.contact)
  resetUserParam('pin', job.contact)
  resetUserParam('group', job.contact)
  resetUserParam('account', job.contact)
  resetUserParam('tsso', job.contact)
  resetUserParam('documentos', job.contact)
  resetUserParam('module', job.contact)
  resetUserParam('videoFile', job.contact)
  actualizarProceso(user.currentProcess, { st: 'deleted', tsu: Date.now() });
  resetUserParam('currentProcess', job.contact)
  resetUserParam('sso', job.contact)
  setUserParam('status', 'deleted', job.contact)
}

global.cancelarFlow = function (flowOb, job, user) {
  wfapi.cancelFlow(flowOb.sessionId, flowOb.apiKey, function (res) {
    switch (res.status) {
      case 200:
        pushText('Proceso cancelado con éxito', job.contact, user.line);
        salir(job, user)
        break;
      case 406:
        //user has to correct input
        break;
      case 400:
        //error en el API, le mando a error interno
        pushText(msgErrorInterno(), job.contact, user.line);
        //salir(job, user);
        break;
      case 500:
        //error en el API, le mando a error interno
        pushText(msgErrorInterno(), job.contact, user.line);
        //salir(job, user);
        break;
      default:
    }
  })
}

global.resetBot = function (contact, user, msg) {
  logEndSession(contact, user.chn, user.tsso, 'r')
  if (msg) {
    pushText(msg, contact, user.line)
  }
  resetUserParam('step', contact)
  resetUserParam('module', contact)
  resetUserParam('ced', contact)
  resetUserParam('documentos', contact)
  resetUserParam('pin', contact)
  resetUserParam('group', contact)
  resetUserParam('account', contact)
  resetUserParam('tsso', contact)
  actualizarProceso(user.currentProcess, { st: 'deleted', tsu: Date.now() });
  setUserParam('status', 'reset', contact)
}

//*Funcion para acabar un proceso con exito
global.finishProcess = function (job, user) {
  if (!user.sso) {
    var str = 'Hemos enviado tus datos, para seguir con el proceso revisa tu correo electrónico. Gracias!'
    pushText(str, job.contact, user.line)
  }
  logEndSession(job.contact, user.chn, user.tsso, 's')
  resetUserParam('step', job.contact)
  resetUserParam('documentos', job.contact)
  resetUserParam('pin', job.contact)
  resetUserParam('group', job.contact)
  resetUserParam('account', job.contact)
  resetUserParam('tsso', job.contact)
  actualizarProceso(user.currentProcess, { st: 'sent', tsu: Date.now() });
  resetUserParam('currentProcess', job.contact)
  resetUserParam('module', job.contact)
  resetUserParam('videoFile', job.contact)
  resetUserParam('sso', job.contact)
  setUserParam('status', 'reset', job.contact)
}

global.getContact = function (phone, cb) {
  console.log('Searching for', phone)
  firebase
    .database()
    .ref('/accounts/' + account + '/contacts')
    .orderByChild('phone')
    .equalTo(phone + '')
    .once('value', function (res) {
      if (res.val()) {
        var arr = _.map(res.val(), function (ob, key) {
          ob.id = key
          return ob
        })
        console.log('Contact found', arr.length)
        var ob = arr[0]
        if (ob.group) {
          cb(ob)
        } else {
          cb(null)
        }
      } else {
        cb(null)
      }
    })
    .catch(function (error) {
      console.log(error)
      cb(null)
    })
}

global.createTask = function (task, cb) {
  firebase
    .database()
    .ref('/accounts/' + account + '/tasks')
    .push(task)
    .then(function (res) {
      task.id = res.key
      if (cb) {
        cb(task)
      }
    })
    .catch(function (error) {
      console.log(error)
      console.log(task)
      if (cb) {
        cb(null)
      }
    })
}

global.isShopOpen = function () {
  var d = new Date()
  var strDate = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear()
  var day = d.getDay()
  var t = d.getHours() * 100 + d.getMinutes()
  if (config.holidays.indexOf(strDate) >= 0) {
    return false
  } else {
    if (config.workDays[day]) {
      if (t >= config.officeOpen && t < config.officeClose) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }
}

global.isShopOpen2 = function () {
  var d = new Date()
  var day = d.getDay()
  var t = d.getHours() * 100 + d.getMinutes()

  if (config.workDays[day]) {
    if (t >= config.officeOpen && t < config.officeClose) {
      return true
    } else {
      return false
    }
  } else {
    return false
  }


}


//! Funciones de procesos
//* Funcion para crear un proceso
global.crearProceso = function (proceso, cb) {
  firebase
    .database()
    .ref('/accounts/' + account + '/procesos')
    .push(proceso)
    .then(function (res, error) {
      if (cb) {
        console.log('PROCESO CREADO')
        cb(res.key)
      } else {
        console.log(error)
        console.log(proceso)
        if (cb) {
          cb(null)
        }
      }
    })
}

//* Funcion para crear un blacklist
global.crearBlacklist = function (obj) {
  firebase
    .database()
    .ref('/accounts/' + account + '/blacklist')
    .push(obj)
}

//* Funcion para crear una lista de renovados
global.createRenewed = function (obj) {
  firebase
    .database()
    .ref('/accounts/' + account + '/renovados')
    .push(obj)
}

//* Funcion para obtener paycodes
global.obtenerPaycodes = function (cb) {
  firebase
    .database()
    .ref('/accounts/' + account + '/paycodes')
    .once('value')
    .then((snapshot) => {
      if (cb) {
        cb(snapshot.val())
      }
    })
    .catch(function (err) {
      console.log(err)
      if (cb) {
        cb(null)
      }
    })
}


//* Funcion para recuperar un proceso
global.obtenerProceso = function (key, cb) {
  firebase
    .database()
    .ref('/accounts/' + account + '/procesos/' + key)
    // .orderByChild('contacto')
    .once('value')
    .then((snapshot) => {
      if (cb) {
        cb(snapshot.val())
      }
    })
    .catch(function (err) {
      console.log(err)
      if (cb) {
        cb(null)
      }
    })
}

//* Funcion para actualizar un proceso
global.actualizarProceso = function (key, obj) {
  firebase
    .database()
    .ref('/accounts/' + account + '/procesos/' + key)
    .update(obj)
}

//* Funcion para actualizar un usedby de un paycode
global.actualizarUsedby = function (key, obj) {
  firebase
    .database()
    .ref('/accounts/' + account + '/paycodes/' + key + '/usedby')
    .push(obj)
}

//* Funcion para actualizar un paycode
global.actualizarPaycode = function (key, obj) {
  firebase
    .database()
    .ref('/accounts/' + account + '/paycodes/' + key)
    .update(obj)
}


//estas deberían irse a tools
global.decr = function (str) {
  var decrypted = CryptoJS.AES.decrypt(str, envBothasherPwd)
  return decrypted.toString(CryptoJS.enc.Utf8)
}

global.encr = function (str) {
  return CryptoJS.AES.encrypt(str, envBothasherPwd).toString()
}

global.makeShortURL = function (authStr, cb) {
  firebase
    .database()
    .ref('/shorts/')
    .push(authStr)
    .then(function (res) {
      console.log('Short Pushed', res.key)
      cb(res.key)
    })
    .catch(function (error) {
      cb('err')
    })
}

global.msgErrorInterno = function () {
  var msg = 'Lo sentimos, ha ocurrido un error. Por favor vuelve a intentar más tarde.\n\n'
  return msg
}

//*crear admin job para sync de files
global.syncFilesJob = function (user, files) {
  var adminJob = {
    type: "sync-files2",
    contact: user.id,
    processId: user.currentProcess,
    files: files,
    st: 'new',
    ts: Date.now()
  }
  firebase.database().ref("/accounts/" + account + "/adminJobs/").push(adminJob);
}


//!funciones bot de uanataca
global.getBalance = function (accId, cb) {
  console.log("Checking balance for", accId);
  firebase.database().ref("/accounts/" + account + "/trans").orderByChild("account").equalTo(accId).once("value", function (res) {
    if (res.val()) {
      var b = 0;
      var arr = _.map(res.val(), function (ob, key) {
        ob.id = key;
        return ob;
      });
      for (var i = 0; i < arr.length; i++) {
        if (arr[i].type == "deb") { b -= arr[i].value; }
        if (arr[i].type == "cre") { b += arr[i].value; }
      }
      cb(b);
    } else {
      cb(0);
    }
  });
}


global.loadLineObject = function (line, cb) {
  console.log("Loading line object", line)
  firebase.database().ref('/lines/' + line).once('value').then(function (res) {
    if (res.val()) {
      console.log("Loaded Line Object", res.key)
      lines[res.key] = res.val();
      lines[res.key].id = res.key;
      cb(true)
    } else {
      cb(false)
    }
  })
    .catch(function (error) {
      console.log(error)
      cb(false)
    });
}

global.getMediaB64 = function (lineOb, imgId, cb) {
  console.log(lineOb.apiURL + "getmediaurl/" + lineOb.id + "/" + imgId)
  fetch(lineOb.apiURL + "getmediaurl/" + lineOb.id + "/" + imgId, {
    method: 'get',
    headers: {}
  }).then(function (response) {
    if (response.status !== 200) {
      console.log(new Date(), 'Get Media API error', response.status);
      response.json().then(function (data) {
        console.log(data);
      });
      cb(false);
    } else {
      response.text().then(function (data) {
        console.log("Get Media response length", data.length)
        //console.log (data)
        if (data == "error") {
          cb(false)
        } else {
          cb(data)
        }
      });
    }
  }).catch(function (err) {
    console.log(new Date(), 'Get Media API request error', err.message);
    cb(false);
  });
}

global.mapaEstadoFirma = function (str, obs) {
  switch (str) {
    case "NUEVO":
      return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
    case "ASIGNADO":
      return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
    case "EN VALIDACION":
      return 'Tu firma ya se encuentra en nuestro sistema, por favor espera un momento un operador la aprobara pronto';
    case "RECHAZADO":
      return 'Tu firma no cumple con los requisitos para ser aprobada.'
    case "ELIMINADO":
      return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
    case "APROBADO":
      return 'Tu firma ya fue aprobada, por favor revisa tu correo electrónico.'
    case "EMITIDO (VALIDO)":
      return 'Tu firma ya ha sido descargada.'
    case "EMITIDO (SUSPENDIDO)":
      return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
    case "EMITIDO (REVOCADO)":
      return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
    case "EMITIDO (CADUCADO)":
      return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
    default:
      return (obs.length > 0 ? "_" + obs.trim().replace(/<br>/g, "") : 'Ninguna')
  }
  //ACT SOLICITADA
  //Se necesita corrección de datos en tu solicitud, por favor revise tu correo electrónico 	
}
