var request = require('request');
var tools = require('./tools');
var _ = require('underscore');
var firebase = require('firebase');
var client = "wapi-bulk";
var booted = false;
var jobs = []
var os = require('os');


//firebase init
var config = {
    apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    authDomain: "whatabot-ac0d8.firebaseapp.com",
    databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    projectId: "whatabot-ac0d8",
    storageBucket: "whatabot-ac0d8.appspot.com",
    messagingSenderId: "288379073650"
  };

firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

//listen to new jobs
function bootApp() {
	if (!booted) {
		booted = true;
		listBackroute('593980357252')
	}	
}

function listBackroute(num) {
	firebase.database().ref("/backroute/").orderByChild("to").equalTo(num).once('value', function(res) {
		console.log (new Date(), "Backroute search done");
		if (res.val()) {
			var arr = _.map(res.val(), function(val, key){
				val.id = key
				val.sentDate = new Date(val.tsc)
				return val
				})
			arr = _.sortBy(arr, function(ob) { return -ob.tsc });
			console.log (arr)
		} else {
			console.log ("No backroutes");	
		}
	});
}	

