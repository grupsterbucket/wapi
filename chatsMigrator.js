var _ = require('underscore');
var firebase = require('firebase');
var booted = false;
var account = "xyz" //default
var jobs = []
if (process.argv[2]) {
	account = process.argv[2];
}
//firebase init
var firebaseConfig  = {
	"apiKey": "AIzaSyDz1Bxiw1vCT1Umv3u0nnOtK3AmBJMUkBc",
    "authDomain": "ubiku3.firebaseapp.com",
    "databaseURL": "https://ubiku3.firebaseio.com",
    "projectId": "ubiku3",
    "storageBucket": "ubiku3.appspot.com",
    "messagingSenderId": "946813430025",
    "appId": "1:946813430025:web:4a3324c8d359bd444d4bd8"
}

//~ var firebaseConfig = {
    //~ apiKey: "AIzaSyCmvMd7X-draAGL59tXajZKAvnjUW5FBsc",
    //~ authDomain: "whatabot-ac0d8.firebaseapp.com",
    //~ databaseURL: "https://whatabot-ac0d8.firebaseio.com",
    //~ projectId: "whatabot-ac0d8",
    //~ storageBucket: "whatabot-ac0d8.appspot.com",
    //~ messagingSenderId: "288379073650"
  //~ };
  
firebase.initializeApp( firebaseConfig );

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
    
        console.log("Logged in to firebase as uid " + user.uid);
        setTimeout(function () {
            bootApp();
        }, 2000);

    } else {
        // User is signed out login again
        console.log("Session expired");
        loginTofirebase();
    }
    // ...
});

function loginTofirebase() {
    console.log("Login to firebase");
    //firebase.auth().signInWithEmailAndPassword(process.env['whatabot_user'], process.env['whatabot_pwd']).catch(function (error) {
    firebase.auth().signInWithEmailAndPassword(process.env['andamio_user'], process.env['andamio_pwd']).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code + ": " + error.message);
    });
}

function bootApp() {
	if (!booted) {
		booted = true;
		console.log ("Processing",account)
		setDates();
	}
}

function setDates() {
	var year = "2022"
	var dfrom = new Date( "01/01/"+year )
	dfrom.setHours(0);
	dfrom.setMinutes(0);
	var dto = new Date( "12/31/"+year )
	dto.setHours(23);
	dto.setMinutes(59);
	
	var pages = parseInt((dto - dfrom)/86400000)+1
	console.log('Cargando 0%');
	varchats = [];
	loadChats(dfrom.getTime(), 0, pages);			
}

function loadChats(from, page, pages) {
	var top = from + 86400001;//3600001;
	var ref = firebase.database().ref('accounts/'+account+'/chats')
	ref.orderByChild("lu").startAt(from).endAt(top).once("value", function(snapshot) {
		if (snapshot.val()) {
			var res = _.map( snapshot.val(), function(obj, key) { 
				obj.id = key;
				return obj} )
			for (var i=0; i<res.length; i++) {
				if (res[i].msgs) {
					if (res[i].msgs!="migrated") {
					jobs.push(res[i])
					}
				}
			}
			
		}
		console.log('Cargando Conversaciones '+parseInt((page/pages)*100)+'% '+page+'/'+pages);
		page += 1;
		if (page==pages) {
			//done	
			console.log ("found chats",jobs.length)
			setTimeout(function() { processJobs() }, 10000);
		} else {
			loadChats(top, page, pages);
		}
	});
}

function processJobs() {
	if (jobs.length>0) {
		migrate(jobs.shift())
	} else {
		console.log ("Done!")
		process.exit(0)	
	}
}

function migrate(chat) {
	if (chat.msgs) {
		var arr = _.map (chat.msgs, function (val, key) {
			val.id = key;
			return val;
			});
		console.log ("migrating",arr.length,"messages",chat.id, chat.lu)
		firebase.database().ref('accounts/'+account+'/conves/'+chat.id+'/msgs/').update(chat.msgs)
		//remove msgs from chat
		firebase.database().ref('accounts/'+account+'/chats/'+chat.id+'').update({m2ts: Date.now(), msgs:'migrated'});
		
		//if (false) {
		if (chat.lu) {
			if (((Date.now() - chat.lu) > (60*60*24*5*1000)) && chat.status != "reset") {
				console.log ("Reset old chat") 
				var job = {
					type: 'reset-bot',
					st: 'new',
					ts: Date.now(),
					contact: chat.id
				}
				firebase.database().ref('accounts/'+account+'/botJobs').push(job);	
			}
		}
	}
	setTimeout(function() { processJobs() }, 250);
}
