// Bot as module
var qs = require('querystring');
module.exports = {
	cartBot: function(body, job, user, step, group, account) {
		console.log ("Processing cart sub bot", step, group)
		switch (step) {
			case "c0":
				main.setUserParam("step","c1", job.contact, user.line);
				main.pushText (mainMenuCartDemo(true), job.contact, user.line);
				break;
				//end step 0
			case "c1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","pedido","comprar","compra","pedir","domicilio"].indexOf(kw)>=0) {
					var msg = "Excelente!\nSigue el siguiente enlace para realizar tu pedido en línea:\n\n";
					msg += "https://demo.factureasy.com/#shop?cr="+job.contact+"@wsp"+user.line;
					main.updateUser({step:"c2", group: "ven"}, job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "Muy bien, sigue el siguiente enlace para ver nuestro menú:\n\n";
					msg += "https://demo.factureasy.com/#shop?cr="+job.contact+"@wsp"+user.line;
					main.pushText (msg + pageBreak() + mainMenuCartDemo(false), job.contact, user.line);
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "ser", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else if (["4","4.","cuatro"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro ejecutivo especialista para que apoye con tu requerimiento";
					main.updateUser({step:"idle", status: "active", group: "ser", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else {											
					main.pushText (mainMenuCartDemo(false), job.contact, user.line);
				}
				break;
			case "c2":
				var kw = body.toLowerCase()
				if (kw == "salir" || kw == "*salir*") {
					var str = "Gracias por usar este servicio. Adios!"
					main.pushText (str, job.contact, user.line);
					main.resetUserParam("step", job.contact, user.line);
					main.resetUserParam("status", job.contact, user.line);
					main.resetUserParam("pin", job.contact, user.line);
				} else {
					main.pushText ("Por favor completa tu pedido en nuestro app o escribe *salir* para terminar  la sesión", job.contact, user.line);
				}
				break;
		}//end switch step
		
		function mainMenuCartDemo(greet) {
			if (greet) {
				var msg = "Hola!\nBienvenido a Burger Co.\n\n";
				msg += "¿En qué te puedo servir?\n";
				msg += "(digita una opción)\n\n";
			} else {
				var msg = "Por favor digita una opción:\n\n";
			}
			msg += "1. Deseo hacer un pedido a domicilio\n";
			msg += "2. Quiero ver su menú\n";
			msg += "3. Necesito ayuda con un pedido\n";
			msg += "4. Quiero hacer un reclamo\n";
			return msg;
		}
	},
	
	firmaBot: function(body, job, user, step, group, account) {
		console.log ("Processing Firma sub bot", step, group)
		switch (step) {
			case "f0":
				if (user["x-dniValid"]) {
					var form = {
						ts: Date.now(),
						dni: user["x-dni"],
						model: 'sample',
						st: 'new',
						signed: false,
						contact: job.contact,
						pin: Math.floor(Math.random() * 825500) + 100000  
					}
					saveForm(form, function(key) {
						var nombre = user.persona.nombre;
						if (user.persona.nombre1) { nombre = user.persona.nombre1 }
						var msg = "Hola "+nombre+", tu identidad ya fue validada, ahora solo debes llenar el contrato usando el siguiente link:\n\n";
						msg += "https://forms.textcenter.net/#form/"+key+"/edit\n\n"
						msg += "Utiliza este código para ingresar: *"+form.pin+"*"
						main.updateUser({ step:"f4" }, job.contact, user.line);
						main.pushText (msg, job.contact, user.line);
					});	
				} else {
					var msg = "Coniski SmartContracts V1.0 Demo\n";
					msg += "Vamos a seguir un proceso de *tres* pasos:\n\n";
					msg += "- Validar tu identidad\n";
					msg += "- Llenar los datos de un contrato\n";
					msg += "- Obtener tu contrato PDF firmado electrónicamente\n\n";
					msg += "Es *importante* que respondas las preguntas del bot una por una y correctamente\n"
					msg += "Necesitas tener un *documento de identidad* a la mano para tomarle una foto\n\n"
					msg += "Comencemos!\n\n";
					msg += "Cuál es tu número de cédula?";
					main.setUserParam("step","f1", job.contact, user.line);
					main.pushText (msg, job.contact, user.line); 
				}
				break;
				//end step 0
			case "f1":
				var kw = body.toLowerCase();
				kw = kw.replace(/-/g,"");
				//is cedula valid?
				if (tools.isCedula(kw)) {
					//call datofast and get data
					tools.datoFastHook(kw, function(str, ob) {
						if (ob) {
							//save in user
							main.updateUser({ persona: ob, step:"f2", "x-dni": kw }, job.contact, user.line);
							main.pushText ("Listo, por favor envía una foto de un documento de identidad. Puede ser cédula, pasaporte o licencia.", job.contact, user.line);
							
						} else {
							main.pushText ("Por favor revisa y envia tu cédula correctamente", job.contact, user.line);
						}
					});
				} else {
					main.pushText ("Por favor revisa y envia tu cédula correctamente", job.contact, user.line);
				}
				break;
			case "f2":
				if (job.msg.url) {
					main.updateUser({ step:"f3", "x-dniPhoto": job.msg.url }, job.contact, user.line);
					var msg = "Listo, por favor envía un *video* donde se vea claramente tu rostro y debes decir lo siguiente:\n\n";
					msg += "_Mi nombre completo es "+user.persona.nombre+"_\n\n";
					msg += "_Mi número de identidad es "+user.persona.cedula+"_\n\n";
					msg += "_Hoy es  "+dateStr()+"_\n\n";
					msg += "_Autorizo a CONISKI a emitir una firma electrónica con mi identidad_\n\n";
					main.pushText (msg, job.contact, user.line);
					function dateStr() {
						var d = new Date()
						var ms = ["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"]
						return d.getDate() + " de " + ms[d.getMonth()] + " de " + d.getFullYear();	
					}
				} else {
					main.pushText ("Por favor revisa y envia una foto de tu identificación", job.contact, user.line);
				}
				break;
			case "f3":
				if (job.msg.url) {
					main.updateUser({ step:"f4", "x-dniVideo": job.msg.url, "x-dniValid": true }, job.contact, user.line);
					var msg = "Listo, espera un momento mientras validamos tu identidad";
					main.pushText (msg, job.contact, user.line);
					fakeAuth(job, user);
					function fakeAuth(job, user) {
						setTimeout(function() {
							//process to create p12, ask the user for PON
							//todo
							//process to create form
							var form = {
								ts: Date.now(),
								dni: user["x-dni"],
								model: 'sample',
								st: 'new',
								signed: false,
								contact: job.contact,
								pin: Math.floor(Math.random() * 825500) + 100000  
							}
							main.saveForm(form, function(key) {
								var msg = "Muy bien, ya hemos completado el primer paso. Por favor llena el contrato usando el siguiente link:\n\n";
								msg += "https://forms.textcenter.net/#form/"+key+"/edit\n\n"
								msg += "Utiliza este código para ingresar: *"+form.pin+"*"
								main.pushText (msg, job.contact, user.line);
							});
							
						}, 5000);
					}
				} else {
					main.pushText ("Por favor revisa y envia un video con las instrucciones enviadas", job.contact, user.line);
				}
				break;
			case "f4":
				var kw = body.toLowerCase()
				if (kw == "salir" || kw == "*salir*") {
					var str = "Gracias por usar este servicio. Adios!"
					main.pushText (str, job.contact, user.line);
					main.resetUserParam("step", job.contact, user.line);
					main.resetUserParam("status", job.contact, user.line);
					main.resetUserParam("pin", job.contact, user.line);
				} else {
					main.pushText ("Por favor completa el formulario en nuestro app o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				
				break;
		}//end switch step
	},
	
	citaBot: function(body, job, user, step, group, account) {
		console.log ("Processing cita sub bot", step, group)
		switch (step) {
			case "a0":
				main.setUserParam("step","a1", job.contact, user.line);
				main.pushText (mainMenuCitasDemo(true), job.contact, user.line);
				break;
				//end step 0
			case "a1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno","agendar","reserva","cita"].indexOf(kw)>=0) {
					var msg = "Excelente!\nSigue el siguiente enlace para agendar tu cita:\n\n";
					var pin = Math.floor(Math.random() * 625500) + 100000;
					main.updateUser({step:"a2", group: "cit", pin: pin}, job.contact, user.line);
					var authStr = main.encr(job.contact+"|demo|res1|app|"+pin+"|"+Date.now())
					msg += "https://events.textcenter.net/#home/"+qs.escape(authStr);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					getCitas(job, user, account, function(arr) {
						arr = _.sortBy(arr, function(ob) { return arr.dateStart});
						//only report new and accepted and only future events
						var d = new Date();
						d.setHours(0);
						d.setMinutes(0);
						var ts = d.getTime();
						//console.log (calendars)
						evs = ""
						for (var i=0; i<arr.length; i++) {
							var e = arr[i];
							if (e.dateStart>=ts) {
								if (e.st == "new" || e.st == "acc") {
									var cal = _.findWhere(calendars, {id: e.resource})
									if (cal) {
										var ed = new Date
										evs += "*"+cal.eventTitle+"*\n"
										evs += ""+cal.caption+"\n"
										evs += "Fecha: *"+e.ymd+"*\n"
										evs += "Hora: *"+formatTS(e.timeStart)+"*\n"
										evs += "Estado: *"+mapEventST(e.st)+"*\n"
										var pin = Math.floor(Math.random() * 625500) + 100000;
										var authStr = main.encr(job.contact+"|demo|res1|"+e.id+"|"+pin+"|"+Date.now())
									    evs += "*Cancelar "+cal.caption+":*\nhttps://events.textcenter.net/#cancel/"+qs.escape(authStr);
										evs += "\n\n"
									}
								}	
							}	
						}
						if (evs.length>0) {
							main.updateUser({pin: pin}, job.contact, user.line);
							var msg = "A continuación tus citas pendientes.  Utiliza el enlace para cancelar la cita:\n\n";
							msg += evs;
						} else {
							var msg = "No tienes citas pendientes para cancelar.\n\n";
						}
						main.pushText (msg + pageBreak() + mainMenuCitasDemo(false), job.contact, user.line);
					});
					
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = "Te estamos direccionando con nuestro personal especializado";
					main.updateUser({step:"idle", status: "active", group: "ser", tso: Date.now()}, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else {											
					main.pushText (mainMenuCitasDemo(false), job.contact, user.line);
				}
				break;
			case "a2":
				var kw = body.toLowerCase()
				if (kw == "salir" || kw == "*salir*") {
					var str = "Gracias por usar este servicio. Adios!"
					main.pushText (str, job.contact, user.line);
					main.resetUserParam("step", job.contact, user.line);
					main.resetUserParam("status", job.contact, user.line);
					main.resetUserParam("pin", job.contact, user.line);
				} else {
					main.pushText ("Por favor termina de agendar tu cita en nuestro app o escribe *salir* para terminar la sesión", job.contact, user.line);
				}
				break;
		}//end switch step
		
		function mainMenuCitasDemo(greet) {
			if (greet) {
				var msg = "Hola!\nBienvenido a Consultorios Bienestar\n\n";
				msg += "¿En qué te puedo servir?\n";
				msg += "(digita una opción)\n\n";
			} else {
				var msg = "Por favor digita una opción:\n\n";
			}
			msg += "1. Agendar una cita\n";
			msg += "2. Cancelar una cita\n";
			msg += "3. Reportar una emergencia\n";
			return msg;
		}
		
		function getCitas(job, user, account, cb) {
			firebase.database().ref("/accounts/"+account+"/events/").orderByChild("ref").equalTo(user.id).once('value', function(res) {
				if (res.val()) {
					cb(_.map(res.val(), function(ob, key) {
						ob.id = key;
						return (ob);
					}))
				} else {
					cb([]);	
				}
			});
		}
		
		function formatTS(n) {
			if (n<1000) {
				n+=""
				return n.substring(0,1) + ":" + n.substring(1,3);	
			} else {
				n+=""
				return n.substring(0,2) + ":" + n.substring(2,4);
			}
		}
		
		function mapEventST(str) {
			switch (str) {
				case 'new':
					return "Por confirmar";
				case 'acc':
					return "Confirmado";
				default:
					return str.toUpperCase();
			}
		}
	},
	
	segurosBot: function(body, job, user, step, group, account) {
		console.log ("Processing seguro sub bot", step, group)
		switch (step) {			
			case "b0":
				var kw = body.toLowerCase()
				main.setUserParam("step","b1", job.contact, user.line);
				main.pushText (mentaWelcome(true), job.contact, user.line);
				break;
				//end step 0
			case "b1":
				var kw = body.toLowerCase()
				var group = ""
				if (["1","1.","uno"].indexOf(kw)>=0) {
					var msg = "Ingresa al siguiente link donde podrás adquirir tu seguro en línea: \n\n";
					msg += "https://coniski.com/";
					msg += "\n\n";
					msg += "Gracias por usar este servicio. Adios!";
					main.resetUserParam("step", job.contact);
					main.pushText (msg, job.contact, user.line);
				} else if (["2","2.","dos"].indexOf(kw)>=0) {
					var msg = "Qué tipo de asesoría requieres:\n\n";
					msg += "1. Conocer mis coberturas\n";
					msg += "2. Como usar mi seguro\n";
					msg += "3. Deseo presentar un requerimiento\n";
					msg += "4. Otros\n";
					var ob = {step:"b2", group:"sac"}
					main.updateUser(ob, job.contact, user.line);
					main.pushText (msg, job.contact, user.line);
					
				} else if (["3","3.","tres"].indexOf(kw)>=0) {
					var msg = "Estamos direccionado tu solicitud, un momento por favor";
					var ob = {step:"idle", group:"sin", status: "active", tso: Date.now()}
					main.updateUser(ob, job.contact, user.line);
					main.logStartTicket(job.contact, user.chn);
					main.pushText (msg, job.contact, user.line);
				} else {
					var msg = "Por favor digita una de las opciones";
					main.pushText (msg, job.contact, user.line);
				}
				
				break;
			case "b2":
				var kw = body.toLowerCase()
				var msg = "Estamos direccionado tu solicitud, un momento por favor";
				main.updateUser({step:"idle", status: "active", tso: Date.now()}, job.contact, user.line);
				main.logStartTicket(job.contact, user.chn);
				main.pushText (msg, job.contact, user.line);
				break;
			
		}
		
		function mentaWelcome(greet) {
			var msg = "";
			if (greet) { msg += "¡Hola, bienvenid@ a Seguros Bienestar!\n\n"; }
			msg += "Por favor selecciona una opción:\n\n";
			msg += "1. Deseo adquirir un seguro.\n";
			msg += "2. Necesito asesoría.\n";
			msg += "3. Quiero reportar un siniestro.\n";
			return msg;	
			
		}
	}
}
